<?php
function checkNumeric($str) {
    return (is_string($str) || is_int($str) || is_float($str)) && preg_match('/^\d+\z/', $str);
}

function createSalt() {
	$string = md5(uniqid(rand(), true));
	return substr($string, 0, 4);
}

function noFollowLink($link) {
	$link = preg_replace('/((?:http|https|ftp):\/\/(?:[A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?[^\s\"\']+)/i','<a href="$1" rel="nofollow" target="blank">$1</a>',$link);
	return $link;
}

function remWhiteSpace($str) {
	$str = str_replace('	','',$str);
	$str = str_replace('  ','',$str);
	return $str;
}

function stripUrl($name) {
	$name = preg_replace('/%[0-9A-F]{2}/', "", $name);
	$name = strtolower($name);
	$name = str_replace('+','-',$name);
	$name = str_replace('--','-',$name);
	return $name;
}
?>