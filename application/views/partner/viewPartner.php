<div style="padding:15px">
<?php if($msg = $this->session->flashdata('message')):?>
	    <div class="alert alert-<?=$msg['type']?>" style="margin-top: 10px">
	    	<a class="close" data-dismiss="alert" href="#">&times;</a>
		    <h4 class="alert-heading"><?=$msg['body']?></h4>
	    </div>
	    <?php endif;?>
	  
<img src="http://placehold.it/1200x200">
<div class="row-fluid" style="padding-top:25px;">

	<div class="span6">
		<?php if($this->tank_auth->get_usertype() == "Admin"):?>

		<a href="<?=site_url("admin/ngoUpdates/$ngoID")?>" class="btn pull-right btn-success"> <i class="icon-plus"></i> Add Updates</a>
		<?php endif;?>
		
		<h3>Updates with The NGO</h3>
		
		
		<?php if($query = $this->Ngo_model->get_ngo_updates($ngoID) ):?>
		<?php foreach($query as $row):?>
		<legend>
				<?php if($this->tank_auth->get_usertype() == "Admin"):?>
				<a href="<?=site_url("admin/editNgoUpdates/" . $row->id)?>" class="btn btn-success pull-right"> <i class="icon-pencil"></i> Edit </a>
				<?php endif;?>

		<?=date("M d, Y", strtotime($row->date))?></legend>
		<?=$row->notes?>
		<?php endforeach;?>
		<?php else:?>
		No Updates
		<?php endif;?>
		
	</div>
	<div class="span6">
		<h3>About <?=$ngoinfo->organization?></h3> 
		<div class="span4">
		<img src="http://placehold.it/200x200"> 
		</div>
		<div class="span6">
			<strong> Country:</strong> <?=$ngoinfo->country?>
		</div>

	<div class="clearfix"></div>
		<legend>NGO Mission</legend>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		
		
		<legend>Meet the Staff</legend>
		<table>
			<tr>
				<td style="height: 125px; width:125px;"><img src="http://placehold.it/100x100"></td>
				<td>
					<strong>Name</strong> Peter Yim  <br />
					<strong>Title</strong> Peter Yim  <br />
					
				</td>
				
			</tr>
		</table>		
		<legend> Country Info</legend>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			
		</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			
		</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			
		</p>
	</div>
</div>
</div>