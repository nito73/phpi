<div style="padding:5px;">

<h2>WHAT PHP INTERNATIONAL DOES</h2>
<p><span style="text-decoration: underline;">WE PROVIDE HELP TO THOSE IN NEED THROUGH MICROSEED LOANS</span></p>
<p>PHP International provides you with an opportunity to help the poor who have businesses which, with help from you, can become more successful. You can easily make a donation to help fund a loan to an individual, a group of entrepreneurs, or meso co-operative larger loans. When you donate, PHP International sets up a special "MicroSeed Donation Account" for you from which you may view all of your funding activities. 
</p>
<p>
<span style="text-decoration: underline;">WE PROVIDE HELP TO WORTHY PROJECTS IN NEED</span><br>
PHP International also identifies and posts projects such as building schools, medical clinics, or sports projects like Kids Play International, which  helps put fun back in the lives of orphans who lost their parents due to genocide. Donations to these projects are given out of unconditional love with no expectation of receiving anything in return. In other words, each tax-deductible donation is just that and is NOT expected to be paid back.
</p>
<p><span style="text-decoration: underline;">WE JOIN WITH MICROFINANCE NON-GOVERNMENT ORGANIZATIONS</span><br>
<br>
PHP International reaches out to NGOs which have experience in Third World countries identifying people and projects in need of funding. They have the infrastructure in place to provide support to each entrepreneur by providing business education which has proven to increase the payback success rate of the loans. These NGOs join hands with PHPI to fund a variety of group and individual loans</p>
<p><span style="text-decoration: underline;">WE HELP THE POOR EXPERIENCE THE JOY THAT YOUR DONATIONS BRING</span><br>
<br>
We find donors who want to grow the love within themselves through giving based on unconditional love with no expectation of anything in return. The people whom you help also have much unconditional love within them.  When they repay their loan to our NGO, it returns the principal to PHPI. PHPI then credits your MicroSeed Account and the money can be used to fund other people in need over and over. Through this process each business entrepreneur actually becomes a partner with you.  Thus, together, People Helping People International, NGOs, donors and entrepreneurs are truly able to realize what faith the size of a mustard seed can accomplish. With your help, we believe we can move mountains of need to mountains of joy.</p>


			</div>