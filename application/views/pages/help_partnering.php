 <div class="accordion" style="padding:10px">  
<h3>Partnering with PHPI</h3>
            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1">  
How can my organization become a PHPI Non Government Organization (NGO)?			</a>  
     		</div>  
              <div id="collapse1" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  

    <p>
Briefly, in order to become an NGO with PHPI, an organization must, at a minimum:<ul>
<li>Serve at least 1,000 borrowers with microfinance services
<li>Have a history (at least 2-3 years) of lending to poor, excluded, and/or vulnerable people for the purpose of alleviating poverty or reducing vulnerability
<li>Be registered as a legal entity in their country of operation
<li>Have at least 1 year of financial audits
</ul></p><p>
Please note - PHPI is unable to work with start-up microfinance institutions or organizations that do not provide microfinance services. Additionally, PHPI does not provide funding in the form of grants or donations to any organization.
</p><p>
If your organization does not fit the minimum criteria for partnership, we encourage you to:
<ul>
<li>Monitor the PHPI website for changes in our partnership criteria.
<li>Get in touch with us again once you meet the minimum criteria - we would love to hear from you!
</ul>    </a> </p>


			</div>  
              </div>
               </div>
               
                           <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1a">  
Can I start a Field Office or PHPI affiliate in XX country?
				</a>  
     		</div>  
              <div id="collapse1a" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  

    <p>
PHP will partner with microfinance institutions across the globe. At this time, we are focusing our limited resources on developing additional partnerships and do not have plans to open field offices and/or PHPI affiliates. However, below are a few other ways you can become involved with PHPI.<ul>
<li>Apply to join our Ambassador Program. 
<li>Become a PHPI donor and support entrepreneurs as they lift themselves out of poverty. 
<li>Keep an eye out for future volunteer opportunities with PHPI.
</ul>
    </a> </p>


			</div>  
              </div>
               </div>

            


			</div>