 <div class="accordion" style="padding:10px">  
<h3>Completing the Payment Process</h3>
            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1">  
Was my transaction successful?
				</a>  
     		</div>  
              <div id="collapse1" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  

    <p>
These are two great steps to take to ensure that your purchase was successful. When you successfully complete a transaction, you receive a receipt confirming your purchase from PayPal. You can log into your MicroSeed Account at any time to view the loans to which you contributed. Simply log into your MicroSeed Account by clicking "My Account" to view your donation history.

    </a> </p>


			</div>  
              </div>
               </div>
               
                           <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1a">  
Do I have to use Paypal?
				</a>  
     		</div>  
              <div id="collapse1a" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  

    <p>
No, we also accept checks and money orders for $500 or more.
    </a> </p>


			</div>  
              </div>
               </div>

            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1b">  
Can I pay by check?
				</a>  
     		</div>  
              <div id="collapse1b" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  

    <p>
PHP International's MicroSeed Program only accepts checks for amounts of $500 or more and also asks for a donation of 8% to help cover administrative costs. We maintain this policy in order to keep our operational expenses low. All checks valuing less than $1,000 will be destroyed to protect your financial information.
    </p>
<i>To pay by check:</i>
<p><ul>
<strong>Step 1:</strong> Register for a MicroSeed Account<br />
<strong>Step 2:</strong> Make your check payable to "People Helping People International, Inc."<br />
<strong>Step 3:</strong> On the back of the check, write the email address that you use to log in to your MicroSeed Account, the total of your check that you would like to be credited to your MicroSeed Account and the total of your check that is a donation to PHPI to cover administrative overhead (8% of the donation). Indicate on the check the loan/project for which the money is to be used.<br />
<strong>Step 4:</strong>  Mail the check to:<br /><ul>
- People Helping People International, Inc<br /> 
- PO Box 164<br />
- Chautauqua, NY  14722-0164<br />
</ul>
<strong>Step 5:</strong>  When your check payment has been processed, we will credit your account and notify you by email.
</ul></p>
</p></a> </p>


			</div>  
              </div>
               </div>

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1c">  
Will I get a receipt?
				</a>  
     		</div>  
              <div id="collapse1c" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  

    <p>
Yes, you will receive an email from PayPal confirming your donation. For tax purposes, the total amount donated through PayPal or by check is tax deductible.

    </a> </p>


			</div>  
              </div>
               </div>
               
                           <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1d">  
Why did I receive a receipt from PayPal when I paid with my credit card?
				</a>  
     		</div>  
              <div id="collapse1d" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  

    <p>
PayPal processes all of our MicroSeed Loan Program's financial transactions. For this reason, regardless of whether you pay for a transaction with funds from a personal credit card or with funds from a PayPal account, you receive a receipt from PayPal recording the purchase. If you have any further questions about your transaction, please do not hesitate to contact PayPal directly at 888-221-1161 (Toll Free US and International), Monday through Friday, 8 a.m. to 5 p.m. CST. </a> </p>
 
			</div>  
              </div>
               </div>

            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1e">  
What if PayPal will not accept my credit card?				</a>  
     		</div>  
              <div id="collapse1e" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  

    <p>
If you have trouble making your payment, please contact the PayPal Customer Service line created by PayPal specifically for PHP International MicroSeed Program users at 888-221-1161 (Toll Free US and International), Monday through Friday, 8 a.m. to 5 p.m. CST.    </a> </p>


			</div>  
              </div>
               </div>


			</div>