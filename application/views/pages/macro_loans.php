<div style="padding:10px">
<h2>MESO/MACRO LOANS</h2>
<p>
People Helping People International also provides funding for meso/macro cooperative ventures, e.g., funding a co-op of 20 fishermen to buy a diesel-powered trawler to enable them to quadruple the catch they can get from canoes. These projects range from $5000 to $100,000+. Meso/Macro projects may be funded by several large donations or by hundreds of smaller ones. These tax-deductible donations, up to $500, may be charged with a credit card through PayPal. Amounts above $500 should be paid by check. The money repaid is then available for you to make more loans.</p>

<a class="pull-right btn btn-large btn-success" href="/give/viewLoanType/Macro">View Loans</a>

</div>