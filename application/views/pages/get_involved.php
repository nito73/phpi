<div style="padding:15px">
<h1>Ambassador Program</h1> 
   <div class="tabbable"> <!-- Only required for left/right tabs -->
  <ul class="nav nav-tabs">
   
    <li class="active" ><a href="#tab2" data-toggle="tab">Q and A</a></li>
    <li><a href="#tab1" data-toggle="tab"> Kids Play Program</a></li>
     <li><a href="#tab3" data-toggle="tab">Meet the Ambassadors</a></li>
    
  </ul>
  <div class="tab-content">
    <div class="tab-pane" id="tab1">
<p> <img style="width:690px" src="<?=base_url()?>/images/kpiLogo.jpg"></p>
<strong>Athlete Ambassador Program</strong>
<p><img style="width:690px"  src="<?=base_url()?>/images/P6050393a.jpg"><br>
Tracy Evans, 3x Olympian and founder of Kids Play International, uses sport and the Olympic values of Excellence, Friendship and Fair Play to promote gender equity in communities impacted by genocide. Currently, KPI has a year round sport for development program, <b><i>Let's Play Fair!</i></b> in Gatagara, Rwanda. LPF has 112 children, ages 7-12 and 13-18 years old from 3 partner schools. Each group meets twice weekly along with a Sunday community day that is open to all.
</p>
<p><strong>About Athlete Ambassador Program</strong><br>
Kids Play Int'ls  athlete ambassadors inspire, share, educate, and challenge. They inspire others through their best practices and positive lifestyle. They share their passion and experience. They educate on the importance of promoting gender equity. Last but not least, they challenge others to do the same.  Kids Play Int'l is partnering with Olympic and professional athletes who understand how the power of sport can make a difference in the lives of kids in communities impacted by genocide.
<br>
<br>
The Athlete Ambassador program offers athletes the opportunity to travel with a purpose to help further Kids Play Int'ls mission, partner with other like-minded Olympic and professional athletes, and more!
<br>
<br>
For more information on Kids Play Int'l or how to become an Athlete Ambassador please visit our website at:<span style="text-decoration: underline;"> <a href="www.kidsplayintl.org">KidsPlayIntl.org</a></span>
<br>
<br>
<strong>
Not An Athlete?</strong><br>
Please visit our website at <span style="text-decoration: underline;"><a href="www.kidsplayintl.org">KidsPlayIntl</a></span> to see ways you can GET INVOLVED or Travel with a Purpose!
</p>


    </div>
    <div class="tab-pane active" id="tab2">
  
  <div class="accordion" style="padding:10px">  
<h3>Who can be an Ambassador?</h3>
            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1">  
                Who is your typical Ambassador?
                </a>  
              </div>  
              <div id="collapse1" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>Our Ambassador Program encourages applicants with backgrounds in business/finance/social development to apply. That said, PHP International Programs will accept applicants from a wide range of backgrounds: MBA retirees, university film students, professionals transitioning careers, law students, public policy researchers, graduate students, financial planners and engineers.  Ages range from 21 (our minimum age) all the way through 60+ years old.</a> </p>
    </li>
</ul>

			</div>  
              </div>
               </div>
               <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1a">  
                I am 20 years old and will be 21 in a few months can I be an Ambassador now?
                </a>  
              </div>  
              <div id="collapse1a" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>Happy almost-birthday! We will be delighted to receive your application after your 21st birthday. We cannot accept Ambassadors younger than 21 for legal reasons.</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  
            
            
<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1b">  
                What is the average age of an Ambassador?
                </a>  
              </div>  
              <div id="collapse1b" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>30 years old.</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1c">  
                Can I be an Ambassador with someone else?
                </a>  
              </div>  
              <div id="collapse1c" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>Yes. PHP will send pairs of Ambassadors out to the field. Both individuals will need to have been accepted into the program on their own merits and will need to fill out their own application. If you have been accepted into the program and would like to be paired with another Ambassador on an assignment, please discuss this in your interview.</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1d">  
                Can my husband/wife be an Ambassador with me?
                </a>  
              </div>  
              <div id="collapse1d" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>Yes. Husband/wife teams can participate in the program. Please see above answer to applying as a group/couple.</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1e">  
                I want to bring my kids with me when I travel - can I be an Ambassador?
                </a>  
              </div>  
              <div id="collapse1e" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>Our goal, always, is to strengthen our partnerships with our Microfinance and Kids Play International partners. PHP's partners are on average small to mid-level organizations that are growing and would not be able to accommodate children in their office or while out in the field. That said, this is something PHP will consider on a case-by-case basis depending on how self-sufficient the Ambassadors will be, the length of stay in the field and their plans for accommodating the children while the Ambassadors are at work.</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1f">  
               Can I go for less than 10 weeks?
                </a>  
              </div>  
              <div id="collapse1f" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>No. PHP does not encourage applicants to apply for less than ten weeks. That said, there may be a need we have at PHP for a shorter stay, and you may submit your application to be on file with us, provided that you are very flexible in regards to the timing of placement.</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1g">  
                Can I be an Ambassador for a year?
                </a>  
              </div>  
              <div id="collapse1g" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>Yes. PHP welcomes long term Ambassadors who would like to stay beyond the minimum 10 week period. This is because longer stays facilitate a deeper understanding of the local culture was well as our Partner Organization and you will be able to contribute on a deeper level to both our Partner Organization as well as PHP. You may even have the chance to be placed with multiple partners throughout your Ambassadorship.</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1h">  
                I am traveling for 2 weeks in a country where a PHP Partner Organization is located, can I be an Ambassador?
                </a>  
              </div>  
              <div id="collapse1h" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>The Ambassadors Program requires a minimum 10 week commitment. The program's focus is to develop solid relationships between the Ambassadors and our partners. Two weeks is not enough time for an Ambassador to establish a working relationship that would be beneficial to our partners unless you are participating on a scheduled kids play international trip.</a> </p>
    </li>
</ul>
			</div>  
              </div>    
            </div>  
<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1i">  
                I am going to be in a country doing a research project, can I be an Ambassador?                </a>  
              </div>  
              <div id="collapse1i" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>This depends entirely on the nature of the research project. Please keep in mind that the Ambassadors Program has very specific requirements of the participants regarding journalist and other responsibilities to our partners Organization. It is generally difficult to combine the program with an ongoing project because of the time commitment required of Ambassadors (40 + hours/week). If by being an Ambassador you are also doing research (using your interviews for academic research) then it may be a possibility.</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>            
</div>
  
  <div class="accordion" style="padding:10px">  
<h3>What Is The Application Process?</h3>
            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse2">  
                Do I need to fill out an application?
                </a>  
              </div>  
              <div id="collapse2" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>Yes. In order to be considered for the micro finance program, every Ambassador must fill out an application and send your completed packet (application, cover letter, resume) to phpintl@gmail.com. Incomplete application packets will NOT BE considered. If you sign up for Kids Play International trip no application is required.</a> </p>
    </li>
</ul>

			</div>  
              </div>
               </div>
               <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse2a">  
                I submitted my application, when will I hear from PHP International's Ambassadors Program?
                </a>  
              </div>  
              <div id="collapse2a" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>We are constantly reviewing applications and interviewing potential Ambassadors. Depending on our needs as well as our time frame for getting people to the field, it may be 4-6 weeks before you hear from PHP about your application.</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  
            
            
<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse2b">  
                What is the application procedure?
                </a>  
              </div>  
              <div id="collapse2b" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>The Ambassadors Program application is available for download from the PHP website. The application should be completed and submitted via email to peoplehelpingpeopleintl@gmail.com. We review applications constantly. We will set up an interview within 4-6 weeks of receipt of your application. Please note that if we have a large number of qualified applications, we may not be able to follow up with every application that we receive.
<br /><br />
If you indicate that you have a need to go through the interview process earlier (due to grant reasons, etc.), please plan on applying earlier than 8 weeks before the expected date of your event (grant due, vacation, etc.) to allow sufficient time for the application review and interview.
<br /><br />
After an initial interview, we will set up a second interview if we both decide to proceed towards confirming placement.</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  


<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse2d">  
                Can I talk to someone about the Ambassadors Program?
                </a>  
              </div>  
              <div id="collapse2d" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>The best way to learn more about the program is to go to the Ambassadors page to download the program description and application. If you have further questions, beyond this FAQ, please email your specific questions to peoplehelpingpeopleintl@gmail.com. Please include the date ranges you're interested in volunteering as an Ambassador. Your question will be answered in 2-3 weeks.</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse2e">  
                Why do I need to write a sample Journal entry for my application?
                </a>  
              </div>  
              <div id="collapse2e" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>The primary responsibility of each Ambassador is to chronicle the lives of the working poor through descriptive and engaging journals uploaded regularly to the PHP website. While on assignment, the Ambassadors are expected to conduct multiple interviews with PHP entrepreneurs, to capture and convey their lives and the impact of the PHP loan. The sample journal in the application allows us to see the quality of your writing and your ability to convey details about the entrepreneur and their business that would be of interest to our PHP donors.</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse2f">  
               How do I learn about your HHPOs?                </a>  
              </div>  
              <div id="collapse2f" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>Just click on the HHPO icon shown at the bottom right hand corner of each posted loan or project.</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  


</div>

  <div class="accordion" style="padding:10px">  
<h3>Housing, health & Saftey</h3>
            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse3">  
                What are my housing options?
                </a>  
              </div>  
              <div id="collapse3" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>This depends on what your needs, expectations and budgets are. You are responsible for arranging and covering the costs of your housing. As a favor, some of our HHPO partners are able to help direct you to housing options. Previous Ambassadors have lived with home stays, lived in their own private apartment, shared an apartment/house with other loan representative officers or friends, or stayed in a hotel close to our HHPO partner's office. Some Ambassadors arrange their lodging prior to leaving home, while others prefer to arrange these details once in the country.</a> </p>
    </li>
</ul>

			</div>  
              </div>
               </div>
               <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse3a">  
                Who pays for travel and medical insurance?
                </a>  
              </div>  
              <div id="collapse3a" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>Each Ambassador is responsible for the cost of travel insurance.</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  
            
            
<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse3b">  
                Do I need travel insurance?
                </a>  
              </div>  
              <div id="collapse3b" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>PHP strongly recommends Ambassadors purchase travel insurance. Please see http://www.fodors.com/wire/archives/002131.cfm for more advice on the topic.</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse3c">  
                What kind of Visa will I apply for?
                </a>  
              </div>  
              <div id="collapse3c" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>You will need to do research as to the type of Visa appropriate for your stay in your destination country. In general, as volunteers Ambassadors only require Tourist Visas.</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse3d">  
                What vaccinations will I need?
                </a>  
              </div>  
              <div id="collapse3d" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>Vaccination requirements vary from country to country. In addition to completing basic research on this topic, you will need to contact your local travel clinic for more information on their recommendations for vaccinations.</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse3e">  
                What should I do to prepare for being an Ambassador?
                </a>  
              </div>  
              <div id="collapse3e" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>Research-Read-Plan!!! Each Ambassador is responsible for learning about the country/region they are being sent to.  We expect you to proactively seek out information. The more you know prior to your arrival the better. This includes medical concerns, cultural preparedness, the political and economic situation, and so on. You may also want to consider doing a crash course in the country's language (if you do not already speak it fluently).</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse3f">  
               What are my contacts at PHP International while in the field?
                </a>  
              </div>  
              <div id="collapse3f" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>For any issues related to the Ambassadors program, HHPO, etc. please contact our Ambassadors Program Coordinator. There might be questions, or projects that our Partnerships Team will ask you to take on and they will contact you directly.</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse3g">  
                Can I contact the HHPO directly?
                </a>  
              </div>  
              <div id="collapse3g" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>You will be put in touch with the HHPO 1-2 weeks before your date of departure. This is necessary to 1) Give PHP and the HHPO enough time to work out any details and logistics and 2) to limit the admin burden for this program on the HHPO. If you have any questions please contact your PHP Ambassador Program Coordinator.

</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse3h">  
                Do I need to bring my laptop?
                </a>  
              </div>  
              <div id="collapse3h" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>Every Ambassador is required to take a laptop with them. It will lessen your use of your host HHPO's resources (they may not have a computer for you to use). The benefit in bringing your own laptop is that you can draft profile entries and emails from your home (offline) and then upload them later at an internet cafe.

</a> </p>
    </li>
</ul>
			</div>  
              </div>    
            </div>  
<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse3i">  
                Do I need a digital camera?              
                </a>  
              </div>  
              <div id="collapse3i" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>Having a digital camera that takes video's to take with you into the field is a requirement of the program. You should either include it in your budgeting or fundraising goals. From time to time, PHP does receive donated cameras which we may lend to you provided that you leave it (in good condition of course) as a gift to the HHPO you are working with. Decisions to place a camera with an Ambassador are made on a case-by-case basis and are primarily dependant on the availability of donated cameras.

</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>            

            
</div>




  <div class="accordion" style="padding:10px">  
<h3>What Does The Ambassadors Do</h3>
            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse4">  
What will my daily tasks be as an Ambassador?
                </a>  
              </div>  
              <div id="collapse4" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p><i>Microfinance:</i><br />

Your daily tasks include meeting with individual entrepreneurs, writing and uploading profile entries about the entrepreneurs to PHP for our Helping Hand Partner Organizations (with accompanying photos, photo journals, videos, etc.). On days that you are not in the field, you will work with our HHPO to help with projects as needed and depending on your own skills and qualifications.
<br /><br />
<i>Kids Play International Athlete Ambassador Program</i>
<br />
About Kids Play Int'ls athlete ambassadors inspire, share, educate, and challenge. They inspire others through their best practices and positive lifestyle. They share their passion and experience. They educate on the importance of promoting gender equity. Last but not least, they challenge others to do the same. Kids Play Int'l is partnering with Olympic and Professional athletes who understand how the power of sport can make a difference in the lives of kids in communities impacted by genocide.
<br /><br />
The Athlete Ambassador program offers athletes the opportunity to travel with a purpose to help further Kids Play Int'ls mission, partner with other like-minded Olympic and Professional athletes, and more!
<br /><br />
For more information on Kids Play Int'l or how to become an Athlete Ambassador please visit our website at: www.kidsplayintl.org
<br /><br />
Not An Athlete? Please visit our website at <a href="kidsplay.org">kidsplayintl.org</a> to see ways you can GET INVOLVED or Travel with a Purpose!</a> </p>
    </li>
</ul>

			</div>  
              </div>
               </div>
               <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse4a">  
What are the job requirements of the Ambassadorship?                </a>  
              </div>  
              <div id="collapse4a" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>The Ambassadors Program if fulltime: 5 days, 40+ hours/week During your assignment:<br />

<i>Micro finance:</i><ul>
<li>You will meet 15 individual entrepreneurs from your portfolio, on average, per week
<li>You will create/upload 15 profile entries, photos, photo journals or video clips per week
<li>You may be involved in developing basic business causes for borrowers.
<li>You may be involved in teaching basic business course to borrowers
<li>You will work with our HHPO to serve their needs as well as you can at times/days you are not meeting entrepreneurs
<li>You will communicate with the Ambassadors Program Manager as necessary throughout your stay to discuss with the HHPO, your experience, or to complete surveys as required.
</li></a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  
            
            
<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse4b">  
In regards to Micro Finance Will I be meeting entrepreneurs on my own?                </a>  
              </div>  
              <div id="collapse4b" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>No, you will be working with field representatives or HHPO's Officer, Loan officers at the HHPO to visit and interview entrepreneurs.

</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse4c">  
How will I upload videos or photos?
                </a>  
              </div>  
              <div id="collapse4c" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>Most likely you will be spending a few long hours in an internet cafe every other day uploading your profile entries and photos for micro finance loans in need of funding or projects such as Kids Play International.

</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse4d">  
What if I choose to stay longer in the country can I continue to be an Ambassador?
                </a>  
              </div>  
              <div id="collapse4d" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>Decisions regarding extension of the program are made on a case by case basis and are dependent on whether or not the Ambassador can continue to fund himself/herself. Both PHP and our partners would also need to evaluate your performance to date as an Ambassador before offering an extension of the program.

</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  
            
</div>




  <div class="accordion" style="padding:10px">  
<h3>What About Funding?</h3>
            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse5">  
                Where can I get funding to participate in the Ambassadors Program?
                </a>  
              </div>  
              <div id="collapse5" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>Current Ambassadors get funding from their universities, Rotary Clubs, grants they've found through internet searches, by fundraising through the family, friends and networks and through a company sponsorship. Research expat community groups as to what's available.

</a> </p>
    </li>
</ul>

			</div>  
              </div>
               </div>
               <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse5a">  
What grants do Ambassadors normally get?
                </a>  
              </div>  
              <div id="collapse5a" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>Typically, Ambassadors get grants through their university or graduate school. Note, that some schools have grants available for students about to enter graduate school as well. Students can try applying for Fulbright Scholarships to help fund their Ambassadorship.

</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  
            
            
<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse5b">  
What grants can I get if I'm not a student?
                </a>  
              </div>  
              <div id="collapse5b" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>Grants are available to individuals who are not students. However, these funding sources are often not as publicized as university affiliated grants and thus require more research to discover. For a monthly fee, you can sign up to have access to a comprehensive online database for individuals looking for funding from The Foundation Center. See <a href="http://gtionline.fdncenter.org">gtionline.fdcenter.org</a> for more information.

</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse5c">  
Is there funding available from PHP?
                </a>  
              </div>  
              <div id="collapse5c" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>At this time funding is not available from PHP. Currently our preference is to select people who are committed to funding the trip themselves. If you'd like to sponsor an Ambassador (roughly $5000 will cover flight, food and accommodation and stipend) please contact us.</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse5d">  
Can PHP help me get a grant?
                </a>  
              </div>  
              <div id="collapse5d" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>Each Ambassador is responsible for obtaining his/her own funding.

</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse5e">  
What will my costs be as an Ambassador?
                </a>  
              </div>  
              <div id="collapse5e" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>The total cost of the program varies depending on country assignment and how conservative you are in terms of traveling and living. You will need to research the cost of living and round-trip flights for the destination country. A good place to start is <a href="http://thorntree.lonelyplanet.com">Lonelyplanet.com</a> or check out a few guidebooks for the country you're interested in to get an idea of what your budget will look like. Most of the partners we work with are based or work in rural areas where cost of living is low.

</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse5f">  
How have other Ambassadors fundraised for their trip?
                </a>  
              </div>  
              <div id="collapse5f" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>Besides applying for grants and scholarships, Ambassadors have fundraised through a network of contacts including family, friends, colleagues, school mates, and so on. Some Ambassadors have received free flights from airlines once they have applied to them directly for assistance. Others have encouraged their employers to contribute matching donations to PHP to specifically fundraise for their Ambassadorship.

</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse5g">  
Is there a sample budget I can follow?
                </a>  
              </div>  
              <div id="collapse5g" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>Again, costs vary depending on the location you'll be living as well as your own personal travel/lifestyle. Roughly for a 3 month Ambassadorship, you can plan on spending $5000 (for flight, food, lodging, incidentals, travel insurance, visas, etc.) Depending on your own history and needs, this figure might go up or down. (i.e. If you already have a yellow-fever certificate, you won't have to pay for a new one, etc.) Note: a longer stay does not mean your costs necessarily go up proportionally, as normally your flight is the largest portion of your costs. Living and volunteering for an additional 2 months would only increase your costs by $400-$600, assuming you lived a very low-key lifestyle.

</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse5h">  
Can my company or organization sponsor me and if so can is the sponsorship tax-deductible?
                </a>  
              </div>  
              <div id="collapse5h" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>Yes. Your company can absolutely support you, and yes it is tax deductible.

</a> </p>
    </li>
</ul>
			</div>  
              </div>    
            </div>  
<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse5i">  
What is PHP's EIN number (for donations)?
                </a>  
              </div>  
              <div id="collapse5i" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>26-3911363 </a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>            

            
</div>





  <div class="accordion" style="padding:10px">  
<h3>Where does the Ambassadors Go?</h3>
            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse6">  
What are PHP's priorities in terms of placing Ambassadors?
                </a>  
              </div>  
              <div id="collapse6" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>PHP's priority is currently to place Ambassadors in Africa, because that is where our partners are. However, this should not deter you from applying to other areas as we add them.

</a> </p>
    </li>
</ul>

			</div>  
              </div>
               </div>
               <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse6a">  
Where can I learn more about the country I'm interested in?
                </a>  
              </div>  
              <div id="collapse6a" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>A very basic starting place is looking at the country facts on <a href="www.wikipedia.org">Wifipedia</a> for the country you are interested in. From there you will find links to books and other internet references. It is up to the Ambassador to do their own research for any country they are placed.

</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  
            
            
<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse6b">  
Can I be an Ambassador in India?
                </a>  
              </div>  
              <div id="collapse6b" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>Not at this time. PHP does not yet have a HHPO in India due to governmental restrictions.

</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse6c">  
Can I be an Ambassador where PHP does not yet have a partner?
                </a>  
              </div>  
              <div id="collapse6c" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>No</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse6d">  
Will I be able to meet other Ambassadors while in the field?
                </a>  
              </div>  
              <div id="collapse6d" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>Perhaps. If you are placed in a country where PHP has multiple partners you may be able to facilitate a meeting with another Ambassador. PHP tries to overlap Ambassador placements overseas so you may be working with another Ambassador at the beginning as well as the end of your assignment.

</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse6e">  
Can I donate to an entrepreneur through your partner and then meet them while there?
                </a>  
              </div>  
              <div id="collapse6e" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>Perhaps. If they are part of your portfolio of clients to visit.

</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse6f">  
Is it safe to volunteer in certain countries?
                </a>  
              </div>  
              <div id="collapse6f" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>This is a question you need to answer for yourself. Check out guidebooks, travel advisories, and talk with friends about whether you feel comfortable going to a particular country. PHP will never send you somewhere you aren't comfortable. We want this to be a great experience for you, our partner and PHP! We will work with you to help you feel comfortable. While we can put you in touch with previous Ambassadors and introduce you to local support, ultimately you will need to feel at ease with whatever issues a country may be facing before PHP agrees to place you with one of our partners.

</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse6g">  
Can I be an Ambassador in Afghanistan or Iraq?
                </a>  
              </div>  
              <div id="collapse6g" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>No. Due to safety concerns, PHP is not sending Ambassadors to these countries.

</a> </p>
    </li>
</ul>

			</div>  
              </div>    
            </div>  

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse6h">  
What happens if there is a crisis while I'm in the country?
                </a>  
              </div>  
              <div id="collapse6h" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>In difficult situations, there are resources available to help you, to the extent that they can. PHP is available to assist you as well as our HHPO partners on the ground. There may also be Ambassadors nearby (at least a bus ride away) to help or visit if needed. In extreme situations, as in any such situation, you are ultimately best served by your preparation beforehand. Buy travel insurance and register with the US Embassy in the country you'll be living. Each Ambassador is responsible for maintaining communication with the U.S. Embassy present in the country to which they are assigned. In fact, upon arrival, you should immediately be in contact with the embassy, notifying them of your presence. At a time of major crisis, your primary point of communication should be the U.S. Embassy as PHP's and our HHPO Partner's ability to help you in such situations may be limited.

</a> </p>
    </li>
</ul>
			</div>  
              </div>    
            </div>  
         

            
</div>
         </div>
    
        <div class="tab-pane" id="tab3">
  <h1>Meet the Ambassadors</h2>
  <img style="width:690px" src="<?=base_url()?>/images/TE_resa.jpg"><br>
<strong>Tracy Evans, Kids Play Int'l Founder and 3x Olympian</strong>
<br>
Three-time Winter Olympian Tracy Evans was inspired by a volunteer trip she took to Africa. She experienced first hand the positive impact sport had on the impoverished youth with whom she worked. Seeing how little access these children have to sports, she returned to the United States determined to create a year-round community sports education program for kids in developing countries.
<br>
<br>
Recognizing the power of sports from her own experience as an Olympic athlete, Evans wanted to start a nonprofit organization that would help educate, empower and teach life lessons in a fun and engaging setting. This desire led to the creation of Kids Play Int'l.
</p>

        </div>
     
  </div>
</div>
</div>

       <!-- Jumbotron -->
     