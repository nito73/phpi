<div style="padding:10px">
<h1>RISK AND DUE DILIGENCE</h1> 
   <div class="tabbable"> <!-- Only required for left/right tabs -->
  <ul class="nav nav-tabs">
    <li class="active"><a href="#tab1" data-toggle="tab">Risk Overview</a></li>
    <li><a href="#tab2" data-toggle="tab">NGO's Role</a></li>
     <li><a href="#tab3" data-toggle="tab">PHPI's Role</a></li>
      <li><a href="#tab4" data-toggle="tab">Your Role</a></li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="tab1">
    <h1>Risk Overview</h1>
		
<p><strong>Donating to the poor online involves 3 levels of risk</strong></p>
<p>You should be aware of the different types of risk in donating to help the poor and find the best donation option for you with respect to risk and social return. Since your donation is based on unconditional love with no expectation of receiving anything in return, you will have comfort in the fact that your donation(s) do help people in a significant way. </p>
<p><strong>Risk 1: Entrepreneur Risk</strong></p>
<p>Each entrepreneur is screened by a People Helping People International (PHPI) local partner Non-Government Microfinance Organization (NGO) before each loan is made and posted on PHPI's website. The NGO looks at a variety of factors (past loan history, village or group reputation, feasibility of business idea) before deeming the entrepreneur credit worthy. However, a number of factors can result in entrepreneurs defaulting:</p>
<ul>
    <li>Business issues (e.g. crop failure)</li>
    <li>Health issues (e.g. malaria, HIV/AIDS)</li>
    <li>Other issues (e.g. theft, paying for school fees before repayment, etc)</li>
</ul>
<p>If a default occurs, PHPI requires our NGOs to be fully transparent about the reason(s) behind it.<br />
* Learn more about the NGO's role in screening entrepreneurs and administering its loans at How PHPI Works on our website.
</p>
<p><strong>Risk 2: NGO Risk </strong></p>
<p>When you donate with the intention to help a specific micro or meso/macro loan business sector(s), PHPI donates the funds to the local NGO which then administers its loans. While this necessary intermediary increases the likelihood that the loan will be effectively used and repaid, new risks are introduced:</p>
<ul>
    <li>Bankruptcy (e.g. the NGO may go out of business and be unable to collect your loan)</li>
    <li>Fraud (e.g. staff members at the NGO may embezzle funds)</li>
    <li>Poor operations (e.g. The NGO may have poor methodologies for screening entrepreneurs or collecting repayments)</li>
</ul>
<p><strong>Risk 3: Country Risk</strong></p>
<p>When lending internationally, it is important to consider "macro-level" risks:</p>
<ul>
    <li>Economic (e.g. a large currency devaluation or the institution of exchange controlled by local governments may render the NGO's local currency collections valueless for you)</li>
    <li>Political (e.g. many PHPI loans are made in the developing world) policies can change regarding funds repatriation or even the requirement that microfinance borrowers have to repay their loans.</li>
    <li>Natural disasters such as a tsunami or drought may greatly reduce the likelihood of loan repayment from certain countries or from specific regions in a country.</li>
</ul>
<p><i>* Learn more about the NGO's role in dealing with severe currency devaluations. <br />
* Learn more about your role in understanding responsible lending practices when it comes to currency devaluations.</i></p>


    </div>
    <div class="tab-pane" id="tab2">
    

				<h1>The NGO Role</h1>
<strong>NGOs are a critical link to entrepreneurs and projects</strong>
<p>Each entrepreneur requesting a loan through PHPI's partner NGO has successfully completed an application process conducted by a local microfinance institution. These NGOs have entered into a partnership agreement with PHPI to become one of our support organizations, responsible for the distribution and collection of loans.  They facilitate the distribution of funds to projects such as schools, medical clinics, scholarships, etc. which are pure donations with no expectation of any repayment.  These donations are made by PHPI on your behalf through your MicroSeed Account.
</p>
<strong>NGO Role #1: Screen entrepreneurs and post loans on PHPI Website</strong>
<p>NGOs work in impoverished areas to screen and approve entrepreneurs. NGOs gauge whether the entrepreneur demonstrates a need for a loan and a reasonable likelihood of making payments. Over the last 30 years, 100 million entrepreneurs have been reached by such organizations and data suggests that the poor can be quite credit worthy (+95% repayment rates) if the NGO employs the proper screening methodology and education. One common methodology used by our NGOs is to lend to entrepreneurs who belong to a borrowing group (e.g. a group of 5 women from the same village who know each other well). Loans to one member of the group are contingent on the other group members repaying on time. Because each member's livelihood depends on other members' repayment, a form of peer monitoring and support develops which helps ensure high repayment rates. Loans directly to individual entrepreneurs are also common, especially as the entrepreneur proves their credit-worthiness in a group setting.</p>
<strong>NGO Role #2: Disburse loans and collect amount</strong>
<p>When you donate to a micro or meso/macro loan business sector through PHPI, we donate the funds to the local NGO which lends and administers their loan. NGOs disburse the loan to the entrepreneur and after a grace period begin collecting payment amounts. Typically, loan officers will travel out to the entrepreneur's location (e.g. rural village) and collect the pledge amount on a weekly or monthly basis. Most of the time, the borrowers are able to pay the loan officer the full amount due, on time, without any issues. On occasion, a borrower may be late in payments.  This is an indicator of how successful their collection efforts are. Once the total loan payments are collected, the NGO donates the principal amount back to PHPI and PHPI's software automatically distributes the repayments to each donor's MicroSeed Account on a proportionate basis less PHPI's 8% administrative/overhead charge.</p>
<strong>NGO Role #3: Report impact and issues surrounding your loan</strong>
<p>In addition to loan screening and administration, NGOs document the impact of their loans in the form of reports which may be typed or a video to provide insight into the entrepreneur's progress and challenges. It is a key point of connection between PHPI's donors and their business sector entrepreneur partner across the world. NGOs are required to provide at least one report per loan, so that the impact can be more transparent.</p>


      
    </div>
    
        <div class="tab-pane" id="tab3">
     <h1>PHPI's Role</h1>
     
     <strong>PHPI screens, rates and monitors each NGO</strong>
<p>PHPI's staff and volunteers work to create a highly transparent donating platform for the poor. PHPI's main role, in addition to operating a website, is to screen and monitor each NGO, who in turn is responsible for screening each entrepreneur listed on the site and administering loans via funding from PHPI's website donations. </p>
<p>Each NGO has a different risk profile. Some are highly established with a proven track record and others are young and unproven but with the potential to reach entrepreneurs not reached by more established NGOs. Like other online marketplaces, PHPI hopes that an online donating platform will let unproven, riskier NGOs build a greater reputation through long-term performance. In the process, they should be able to raise capital from other sources beyond PHPI to serve more of the poor in their area.</p>
<strong>PHP Role #1: Initially screen each NGO</strong>
<p>New NGOs must meet PHPI's Minimum Requirements in order to receive donations from PHPI for local entrepreneurs and projects identified by the NGO. The NGO must:</p>
<ol>
    <li>
    <p>Have a mission of lending to the poor for a social purpose with interest rates that are significantly discounted versus alternatives available to the local poor. </p>
    </li>
    <li>
    <p>Be able to accept U.S. dollar denominated debt capital from U.S. lenders and manage a reasonable degree of currency risk. </p>
    </li>
    <li>
    <p>Be cleared of the U.S. Department of Justice Terrorist Exclusion List and the Treasury Department's list of "Specially Designated Nationals and Blocked Persons." </p>
    </li>
    <li>
    <p>Provide PHPI with legal incorporation registration documents recognized by the local government.</p>
    </li>
</ol>
<strong>PHPI Role #2: Monitor NGOs</strong>
<p>The amount of money an NGO can lend or give each month depends on our donor's appetite for the entrepreneurs or projects the NGO has screened and approved. PHPI routinely audits and monitors each NGO. If a PHPI audit uncovers funds mismanagement, PHPI will make this transparent on the website and take appropriate actions to resolve the issue, up to and including partnership termination and legal action in the case of gross funds mismanagement.</p>

        </div>
        <div class="tab-pane" id="tab4">
    
<h1>Your Role</h1>
<strong>As a donor, there are things you can do to increase the likelihood of being repaid:</strong>
<ol>

    <li><strong>Diversify your Portfolio</strong><br>
    <p>
    The best way to reduce your exposure to any one entrepreneur is to diversify where you place your money. Instead of placing $100 with one entrepreneur, we recommend you place $25 with four entrepreneurs.</p>
    </li>
    <li><strong>Learn about the Meso/Macro Loans or Project</strong><br>
    <p>
    Look for the following data points in the posted description:</p>
    <ul>
        <li>
        <p>Is the Meso/Macro loans or project in an area prone to economic or political disruptions?  (e.g. Iraq)</p>
        </li>
        <li>
        <p>Does the profile appear feasible?</p>
        </li>
    </ul>
    </li>
</ol>



        </div>
  </div>
</div>
</div>

       <!-- Jumbotron -->
     