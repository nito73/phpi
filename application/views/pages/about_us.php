<div style="padding: 10px;">
<h1>About Microfinance</h1>
<p>We're glad you'd like to learn more about microfinance. This page contains information we have gathered from colleagues, friends, and microfinance organizations we respect to help answer some of your questions.</p>
<p>

<div class="accordion" style="padding:10px">  
<h3>FAQs about Microfinance</h3>
            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1">  
What is microfinance?
				</a>  
     		</div>  
              <div id="collapse1" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  

    <p>
<p>"Microfinance is the supply of loans, savings, and other basic financial services to the poor."</p>
    <p>As the financial services of microfinance usually involve small amounts of money - small loans, small savings etc. - the term "microfinance" helps to differentiate these services from those which formal banks provide.</p>
    <p> The reason they are small is because someone who doesn't have a lot of money isn't likely to want to take out a $5,000 loan, or be able to open a savings account with an opening balance of $1,000, therefore -"micro". </p>
        </a> </p>


			</div>  
              </div>
               </div>
               
                           <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1a">  
What is a HHPO?				</a>  
     		</div>  
              <div id="collapse1a" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  

    <p>
<p>A HHPO is a NGO microfinance institution that provides to poor entrepreneurs microfinance loan services </p>
    <p>"Historical context can help explain how specialized microfinance institutions developed over the last few decades. Between the 1950's and 1970's, governments and donors focused on providing subsidized agricultural credit to small and marginal farmers, in hopes of raising productivity and incomes. During the 1980's, microenterprise credit concentrated on providing loans to poor women to invest in tiny businesses, enabling them to accumulate assets and raise household income. These experiments resulted in the emergence of nongovernmental organizations (NGOs) that provided financial services for the poor. In the 1990's, many of these organizations transformed themselves into formal financial institutions in order to enhance their outreach. </p>
    <p>A microfinance institution or HHPO can be defined as any organization&mdash;credit union, small commercial bank, financial NGO, or credit cooperative&mdash;that provides financial loan services for the poor."</p>
    <p>"The World Bank estimates that there are now over 7000 microfinance institutions (MFI's), serving some 16 million poor people in developing countries. The total cash turnover of MFI's world-wide is estimated at greater than US$2.5 billion and the potential for new growth is outstanding." </p>
    </a> </p>

			</div>  
              </div>
               </div>

            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1b">  
Why would poor people need financial services?
				</a>  
     		</div>  
              <div id="collapse1b" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  

    <p>
 <p><img width="150" height="180" align="left" style="margin: 5px 20px 15px 0px;" src="/images/about_3.jpg">"The great challenge before us is to address what excludes people from full participation in the financial sector... Together, we can and must build inclusive financial sectors that help people improve their lives."</p>
    <p><em> - <a href="http://www.yearofmicrocredit.org/pages/whyayear/whyayear_quotecollection.asp#kofiannan">Kofi Annan</a>, UN Secretary General</em></p>
    <p>It's easy to imagine poor people don't need financial services; however, they are using these services already, although they might look a little different. </p>
    <p>"Poor people do save, although mostly in informal ways. They invest in assets such as jewelry, domestic animals, building materials, and things that can be easily exchanged for cash. They may set aside grain from their harvest to sell at a later date. They bury cash in the ground or stash it in a jar. They are active in informal savings groups where everyone contributes a small amount each day, week, or month, and is successively awarded the pot on a rotating basis. Some of these groups allow members to borrow from the pot as well. The poor also give their money to friends to hold or pay local cash collectors to keep it safe."</p>
    <p>"However widely used, informal savings mechanisms have serious limitations. It is not possible, for example, to cut a leg off a goat when the family suddenly needs a small amount of cash. In-kind savings are subject to fluctuations in commodity prices, destruction by insects, fire, thieves, or illness (in the case of livestock). Informal rotating savings groups tend to be small and rotate limited amounts of money. Moreover, these groups often require rigid amounts of money at set intervals and do not react to changes in their members' ability to save. Perhaps most importantly, the poor are more likely to lose their money through fraud or mismanagement in informal savings arrangements than are depositors in formal financial institutions." </p>
    </a> </p>


			</div>  
              </div>
               </div>

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1c">  
Why don't they just go to a bank?				</a>  
     		</div>  
              <div id="collapse1c" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  

    <p>
<p>"The poor rarely access services through the formal financial sector. They address their need for financial services through a variety of financial relationships, mostly informal."</p>
    <p>Why is this? For a moment pretend that you are a poor sheep herder walking into a bank:</p>
    <ul>
        <li>You don't have any money to open a savings account with
        </li>
        <li>
        You don't have any collateral to secure a micro loan with
        </li>
        <li>
        You don't have a credit record as you have never been formally employed and you've never taken out a micro loan before
        </li>
        <li>
        You might even be unable to complete the necessary paperwork as you are illiterate.
        </li>
    </ul>
    <p>Formal financial institutions were not designed to help those who don't already have financial assets - they were designed to help those who do. Imagine trying to get a loan in the United States without any savings, an employer or a credit report.</p>
    <p>So what do poor people do?</p>
    <p>"Credit is available from informal commercial and non-commercial money-lenders but usually at a very high cost to borrowers. Savings services are available through a variety of informal relationships like savings clubs, rotating savings and credit associations, and mutual insurance societies that have a tendency to be erratic and insecure."</p>
    <p><img align="left" style="margin: 5px 20px 15px 0px;" src="/images/about_4.jpg">"Poverty is not created by the poor. It is created by the structures of society and the policies pursued by society. Change the structure as we are doing in Bangladesh, and you will see that the poor change their own lives. Grameen's experience demonstrates that, given the support of financial capital, however small, the poor are fully capable of improving their lives." - Banker to the Poor</p>
    <p><em>- <a href="http://www.grameen-info.org/">Muhammad Yunus</a>, Grameen Bank, Founder</em></p>
    <div style="width: 100%; clear: both;"></div>
        </a> </p>


			</div>  
              </div>
               </div>
               
                           <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1d">  
Why don't banks accommodate poor people?
				</a>  
     		</div>  
              <div id="collapse1d" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  

    <p>
<p>Some do. <a href="http://www.grameen-info.org/" target="_blank">Grameen Bank</a> in Bangladesh was formed out of a project providing small loans to women in the village of Jobra. <a href="http://www.bancosol.com.bo/en/" target="_blank"> Bancosol</a>, a commercial bank in Bolivia, is also a bank which provides microfinance services for the poor of Bolivia.</p>
    <p>However, the majority of formal banks do not provide microfinance products as microfinance is an expensive enterprise - you can make a lot more money on a large loan than a small loan, and you won't make much money holding savings accounts with very little funds in them. Banks can make more money if they only provide financial services to those who already have money.</p>
    </a> </p>

			</div>  
              </div>
               </div>

            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1e">  
Why are microcredit interest rates so high?
				</a>  
     		</div>  
              <div id="collapse1e" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  

    <p>
<p>The nature of small loan microcredit - is such that interest rates need to be high to return the cost of the loan. </p>
    <p>"There are three kinds of costs our MFI or HHPO has to cover when it makes microloans. The first two, the cost of the money that it lends and the cost of loan defaults, are proportional to the amount lent. For instance, if the cost paid by the MFI for the money it lends is 10%, and it experiences defaults of 1% of the amount lent, then these two costs will total $11 for a loan of $100, and $55 for a loan of $500. An interest rate of 11% of the loan amount thus covers both these costs for either loan.</p>
    <p>The third type of cost, transaction costs, is not proportional to the amount lent. The transaction cost of the $500 loan is not much different from the transaction cost of the $100 loan. Both loans require roughly the same amount of staff time for meeting with the borrower to appraise the loan, processing the loan disbursement and repayments, and follow-up monitoring. Suppose that the transaction cost is $25 per loan and that the loans are for one year. To break even on the $500 loan, the MFI would need to collect interest of $50 + 5 + $25 = $80, which represents an annual interest rate of 16%. To break even on the $100 loan, the MFI would need to collect interest of $10 + 1 + $25 = $36, which is an interest rate of 36%. At first glance, a rate this high looks abusive to many people, especially when the clients are poor. But in fact, this interest rate simply reflects the basic reality that when loan sizes get very small, transaction costs loom larger because these costs can't be cut below certain minimums." </p>    </a> </p>


			</div>  
              </div>
               </div>

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1f">  
What are the effects of microfinance?
				</a>  
     		</div>  
              <div id="collapse1f" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  

    <p>
<p><img width="124" height="149" align="left" style="margin: 5px 20px 15px 0px;" src="/images/about_7.jpg">"Several years ago two friends of mine were speaking with a group of 40 clients at a micro-bank in South Asia. Through the translator, they asked the 40 women what impact the bank had had on the husbands of the non-borrowers; not their husbands, but the husbands of women who are not with the bank. The clients said, 'Before we took our loans, our husbands were day-laborers, working for others whenever they could find work. When we took our loans our husbands stopped being day-laborers and worked with us - bicycle rickshaw, husking rice, growing garlic on leased land. This caused a shortage of day-laborers in this area, so the husbands of the non-borrowers who were day-laborers-their wages went up.' That was the impact of this bank on the husbands of the non-borrowers."</p>
    <p><em>- <a href="http://www.unfpa.org/gender/micro.htm">Sam Daley-Harris</a>, Microcredit Summit Campaign, Director</em></p>
    <p>"Comprehensive impact studies have demonstrated that:</p>
    <ul>
        <li>
        Microfinance helps very poor households meet basic needs and protect against risks;
        </li>
        <li>
        The use of financial services by low-income households is associated with improvements in household economic welfare and enterprise stability or growth;
        </li>
        <li>
        By supporting women's economic participation, microfinance helps to empower women, thus promoting gender-equity and improving household well-being;
        </li>
        <li>
        For almost all significant impacts, the magnitude of impact is positively related to the length of time that clients have been in the program." (<a href="http://www.uncdf.org/english/microfinance/facts.php">UNCDF Microfinance</a>)
        </li>
    </ul>
    <p>"Poor people, with access to savings, credit, insurance, and other financial services, are more resilient and better able to cope with the everyday crises they face. Even the most rigorous econometric studies have proven that microfinance can smooth consumption levels and significantly reduce the need to sell assets to meet basic needs. With access to micro insurance, poor people can cope with sudden increased expenses associated with death, serious illness, and loss of assets.</p>
    <p>Access to credit allows poor people to take advantage of economic opportunities. While increased earnings are by no means automatic, clients have overwhelmingly demonstrated that reliable sources of credit provide a fundamental basis for planning and expanding business activities. Many studies show that clients who join and stay in programs have better economic conditions than non-clients, suggesting that programs contribute to these improvements. A few studies have also shown that over a long period of time many clients do actually graduate out of poverty. </p>
    <p>By reducing vulnerability and increasing earnings and savings, financial services allow poor households to make the transformation from "every-day survival" to "planning for the future." Households are able to send more children to school for longer periods and to make greater investments in their children's education. Increased earnings from financial services lead to better nutrition and better living conditions, which translates into a lower incidence of illness. Increased earnings also mean that clients may seek out and pay for health care services when needed, rather than go without or wait until their health seriously deteriorates."</p>
    <p>"Empirical evidence shows that, among the poor, those participating in microfinance programs who had access to financial services were able to improve their well-being&mdash;both at the individual and household level&mdash;much more than those who did not have access to financial services.</p>
    <ul>
        <li>
        In Bangladesh, Bangladesh Rural Advancement Committee (BRAC) clients increased household expenditures by 28% and assets by 112%. The incomes of Grameen members were 43% higher than incomes in non-program villages.
        </li>
        <li>
        In El Salvador, the weekly income of FINCA clients increased on average by 145%.
        </li>
        <li>
        In India, half of SHARE clients graduated out of poverty.
        </li>
        <li>
        In Ghana, 80% of clients of Freedom from Hunger had secondary income sources, compared to 50% for non-clients.
        </li>
        <li>
        In Lombok, Indonesia, the average income of Bank Rakyat Indonesia (BRI) borrowers increased by 112%, and 90% of households graduated out of poverty.
        </li>
        <li>
        In Vietnam, Save the Children clients reduced food deficits from three months to one month."
        </li>
    </ul>
    </a> </p>

			</div>  
              </div>
               </div>
               
                           <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1g">  
When is microcredit not appropriate?
				</a>  
     		</div>  
              <div id="collapse1g" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  

    <p>
<p>"Microcredit may be inappropriate where conditions pose severe challenges to loan repayment. For example, populations that are geographically dispersed or have a high incidence of disease may not be suitable microfinance clients. In these cases, grants, infrastructure improvements or education and training programs are more effective. For microcredit to be appropriate, the clients must have the capacity to repay the loan under the terms by which it is provided." (<a href="http://www.yearofmicrocredit.org/pages/whyayear/whyayear_quotecollection.asp#kofiannan">International Year of Microcredit</a>)</p>    </a> </p>


			</div>  
              </div>
               </div>

            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1h">  
Why do so many MFIs focus on women?
				</a>  
     		</div>  
              <div id="collapse1h" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  

    <p>
<p><img width="434" height="324" align="left" style="margin: 5px 20px 15px 0px;" src="/images/about_9.jpg">"Today I'm a very respected woman in the community. I have come out of the crowd of women who are looked down upon. Due to the loan that I received... you have made me to be a champion out of nobody." </p>
    <p><em>- Rose Athieno, Produce Reseller, Uganda</em></p>
    <p>"Microfinance programs have generally targeted poor women. By providing access to financial services only through women&mdash;making women responsible for loans, ensuring repayment through women, maintaining savings accounts for women, providing insurance coverage through women&mdash;microfinance programs send a strong message to households as well as to communities. </p>
    <p>Many qualitative and quantitative studies have documented how access to financial services has improved the status of women within the family and the community. Women have become more assertive and confident. In regions where women's mobility is strictly regulated, women have become more visible and are better able to negotiate the public sphere. Women own assets, including land and housing, and play a stronger role in decision making.</p>
    <p>In some programs that have been active over many years, there are even reports of declining levels of violence against women." </p>
    </a> </p>


			</div>  
              </div>
               </div>

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1i">  
Can microfinance be profitable?
				</a>  
     		</div>  
              <div id="collapse1i" class="accordion-body collapse" style="height: 0px; ">  

                <div class="accordion-inner">  

    <p>
<p>Yes.  "The November 2001 issue of the <a href="http://www.mixmbb.org/Templates/WelcomePage.aspx">MicroBanking Bulletin</a> includes data from 62 self-sufficient MFI's. The average return on assets for this group is 5.5%, which compares favorably to commercial-bank returns. Indeed, there are grounds for hope that microfinance can become attractive to mainstream retail bankers.</p>
    <p>At the same time, some worry that an excessive concern for profit in microfinance will lead MFI's away from poor clients to serve better-off clients who want larger loans. It is true that programs serving very poor clients are somewhat less profitable than those reaching better-off clients, but this may say more about managers' objectives than an inherent conflict between serving the very poor and profitability. MFI's serving the very poor are showing rapid financial improvement. Microfinance programs like Bangladesh Rural Advancement Committee and ASA in Bangladesh have already demonstrated that very poor clients can be reached profitably: both institutions had profits of more than 4% of assets in 2000.</p>
    <p>There are cases where microfinance cannot be made profitable, for example, where potential clients are extremely poor and risk-averse or live in remote areas with very low population density. In such settings, microfinance may require continuing subsidies. Whether microfinance is the best use of these subsidies will depend on evidence about its impact on the lives of these clients."  With our Micro Seed Loan Program the capability to earn profits for the donor does not exist.  All contributions to the Micro Seed Loan Program are donations. </p>
    </a> </p>


			</div>  
              </div>
               </div>
               
<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1j">  
Is microfinance the solution to poverty?
				</a>  
     		</div>  
              <div id="collapse1j" class="accordion-body collapse" style="height: 0px; ">  

                <div class="accordion-inner">  

    <p>
<p><img width="107" height="133" align="left" style="margin: 5px 20px 15px 0px;" src="/images/about_11.jpg">"My own view is that we have to approach extreme poverty a little like the way in which a doctor might approach a patient. By that I mean do a diagnosis and understand what is it that is really ailing the particular country, the particular region. Sometimes it's terrible governance and the question is how to improve the governance and the hope for the kind of change that is needed. In other places it's the terrible burden of disease that may be addressable by good public health measures. In other places it is to show how to grow more food. In other places it's how to get business going and microfinance has proven to be an incredibly powerful tool.  Once the basics are in place, the people are eating and can survive, then microfinance can play a huge role in helping a poor community find ways through the market to get new opportunities, to earn new income, to start saving, making investments and start the process of climbing the ladder of economic development in your children, in your business or your farm and continuing up the process of improving skills, specialization, new business ventures and so on. We've learnt that microfinance can be a wonderful tool for that." </p>
    <p>- Jeffrey Sachs, The Earth Institute at Columbia University, Director</p>
    <p>No. Microfinance is but one strategy battling an immense problem.</p>
    <p>"In the last two decades, substantial progress has been made in developing techniques to deliver financial services to the poor on a sustainable basis. Most donor interventions have concentrated on one of these services, microcredit. For microcredit to be appropriate however, the clients must have the capacity to repay the loan under the terms by which it is provided. Otherwise, clients may not be able to benefit from credit and risk being pushed into debt problems. This sounds obvious, but microcredit is viewed by some as "one size fits all." Instead, microcredit should be carefully evaluated against the alternatives when choosing the most appropriate intervention tool for a specific situation.</p>
    <p>Microcredit may be inappropriate where conditions pose severe challenges to standard microcredit methodologies. Populations that are geographically dispersed or nomadic may not be suitable microfinance candidates. Microfinance may not be appropriate for populations with a high incidence of debilitating illnesses (e.g., HIV/AIDS). Dependence on a single economic activity or single agricultural crop, or reliance on barter rather than cash transactions may pose problems. The presence of hyperinflation or absence of law and order may stress the ability of microfinance to operate. Microcredit is also much more difficult when laws and regulations create significant barriers to the sustainability of microfinance providers (for example, by mandating interest-rate caps).</p>
    </a> </p>


			</div>  
              </div>
               </div>               
 
</div>

<br>
<h2>Examples of Some Alternative Strategies</h2>
<p><span style="font-size: 15px; font-weight: bold;">Grants</span> can be used to help overcome the social isolation, lack of productive skills, and low self-confidence of the extreme poor, and to prepare them for eventual use of microcredit. Small grants and other financial entitlements can work well as first steps to "graduate" the poor from vulnerability to economic self-sufficiency. A successful example is the BRAC Income Generation for Vulnerable Groups Development program in Bangladesh. This program has graduated more than 660,000 destitute women through free food, training, health care, and savings to BRAC's mainstream microcredit program. </p>
<p><span style="font-size: 15px; font-weight: bold;">Investments in infrastructure</span>, such as roads, communications, and education, provide a foundation for economic activities. Community-level investments in commercial or productive infrastructure (such as market centers or small-scale irrigation schemes) also facilitate business activity. </p>
<p><span style="font-size: 15px; font-weight: bold;">Employment programs</span> prepare the poor for self-employment. Food-for-work programs and public works projects fit this model. In many cases, these programs may be out of reach for cash-strapped local governments but within the purview of donors.</p>
<p><span style="font-size: 15px; font-weight: bold;">Non-financial services</span> range from literacy classes and community development to market-based business-development services. While non-financial services should be provided by separate institutional providers, there are clear, complementary links with the demand for and impact of microcredit. For example, improved access to market opportunities stimulates - and depends on - securing credit to cover the costs (product design, transport, etc.) of taking advantage of those opportunities.</p>
<p><span style="font-size: 15px; font-weight: bold;">Legal and institutional reforms</span> can create incentives for microfinance by improving the operating environment for both microfinance providers and their clients. For example, streamlining microenterprise registration, abolishing caps on interest rates, loosening regulations governing non-mortgage collateral, strengthening the judicial system, and reducing the cost and time of property and asset registration can foster a supportive climate for micro loans."</p>

</p>




