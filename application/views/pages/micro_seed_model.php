<div style="padding:10px">

			<div align="center" ><a href="/pages/view/hhpo_info">Overview</a> |  <strong>The Micro Seed Model</strong>  |  <a href="/pages/view/become_a_hhpo">Become a NGO Partner</a></div>
			<br />

<p><h3>Non Government Organization (NGO) Selection Requirements:</h3></p><br>
<h3>Operational Requirements</h3>
<p>In order to work effectively with PHP International, a microfinance institution must be able to:</p>
<ul>
    <li>
    Take digital photographs of borrowers
    </li>
    <li>
    Write borrower and business descriptions 
    </li>
    <li>
    Write at least one journal entry per borrower (i.e. business update) 
    </li>
    <li>
    Upload photographs and business descriptions to the PHP International website 
    </li>
    <li>
    Communicate via email and access the Internet regularly 
    </li>
    <li>
    Speak English
    </li>
    <li>
    Legally accept US dollar debt from a foreign lender and be able to repatriate funds 
    </li>
    <li>
    Manage foreign exchange risk 
    </li>
</ul>
<h3>Benefits of Working with PHP International</h3>
<ul>
    <li>
    <strong>0% Interest Debt Capital</strong>
    <p>PHP International enables microfinance non-government organizations (NGOs) to raise 0% interest debt capital directly, worldwide, using the Internet.</p>
    </li>
</ul>
<ul>
    <li>
    <strong>Increased Exposure</strong>
    <p>PHP International's transparent online platform exposes your work to a worldwide audience of donors. </p>
    </li>
</ul>
<ul>
    <li>
    <strong>Improved Staff Morale</strong>
    <p>NGO staff will love using our MicroSeed computer program. It is easy to learn, reduces costs if you are using a paper system, and is not time consuming. Their work becomes appreciated on a global scale. </p>
    </li>
</ul>
<ul>
    <li>
    <strong>Website Activity</strong>
    <p>Our donors select a business sector(s) and donate through PHPI. Our NGOs in turn find entrepreneurs within the business sector who need financial help. Our NGO then posts a picture(s) of each entrepreneur and a description of what they need a loan for to PHP International's website. Our donor will then feel committed to the borrower(s) they selected.</p>
    </li>
</ul>
<ul>
    <li>
    <strong>Funds Transfer</strong>
    <p>PHP International aggregates the 0% interest U.S. dollar capital and wires it to the NGO on a monthly basis. NGOs use PHP International's monthly funding to re-finance their overall cost of capital and to increase their own operational sustainability. </p>
    </li>
</ul>
<ul>
    <li>
    <strong>Reporting</strong>
    <p>PHP's MicroSeed donors provide subsidized capital in exchange for impact transparency. NGOs must report on the social impact of their borrowers' enterprises at least once per loan term.  </p>
    </li>
</ul>
<ul>
    <li>
    <strong>Repayment</strong>
    <p>The NGO's software automatically tracks each borrower's repayment. Once a loan is repaid or the maximum amount possible is collected, the NGO will donate back, on a quarterly basis, the total principal collected less interest to PHP International. The funds will be credited to the donors' accounts and made available for each donor to re-donate again to the NGO borrowers.</p>
    </li>
</ul>
<h3>Costs Associated with Working with PHP International</h3>
<ul>
    <li>
    <strong>Interest Rate PHP International Charges</strong>
    <p>The MicroSeed Program adds an 8% charge to each donation to cover our PayPal and operational expenses and then passes 100% of the donation to the NGO. PHP International is a self-sufficient 501(c)(3) U.S. non-profit organization. </p>
    </li>
</ul>
<ul>
    <li>
    <strong>Cost of Staff Time to Post Borrower Data</strong>
    <p>PHPI's free software for loans/projects and savings accounts vastly reduces the time most of our NGOs spend using paper systems. These savings more than offsets the cost of posting and the cost of any additional hardware.   </p>
    </li>
</ul>
<ul>
    <li>
    <strong>Foreign Exchange Hedging Cost </strong>
    <p>This cost varies from country to country.  PHP International's MicroSeed donations are in U.S. dollars and our policy is for our NGOs to manage the currency risk. PHPI's Exchange Rate Fluctuation Reserve Account system helps our NGOs to manage this risk.  </p>
    </li>
</ul>
<ul>
    <li>
    <strong>Total Cost of PHP International Capital</strong>
    <p>For most NGOs, PHP International represents the cheapest U.S. dollar debt capital source available. </p>
    </li>
    <li><strong>PHPI's Free Loan/Project Management Software</strong>
    <p>PHPI'S free loan/project management software, coupled with our savings account system eliminates costly paper systems. The savings should be more than enough to offset the cost of using PHPI's website. 
</ul>


			</div>