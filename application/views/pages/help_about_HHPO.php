<div class="accordion" style="padding:10px">  
<h3>Working with Non-Government Organizations</h3>
            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1">  
What is a Non-Government Organization (NGO)?				</a>  
     		</div>  
              <div id="collapse1" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>
When we refer to our Non-Government Organizations (NGOs) we are referring to microfinance institutions in specific countries that we have partnered with to manage and administer the individual microloans, meso/macro loans and projects. We provide them with the capacity to post loans and projects to PHPI's website. We teach them how to use our computer programs and in some instances give them administrative access to the loans.</a> </p>
    </li>
</ul>

			</div>  
              </div>
               </div>
               
                           <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1a">  
Why does PHPI need NGOs?				</a>  
     		</div>  
              <div id="collapse1a" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>
The NGOs are critical to our ability to provide loans efficiently. By partnering with microfinance institutions located on the ground, PHPI leverages critical knowledge of the local population and years of experience managing loans. Were PHPI to operate as a microfinance institution ourselves, we would enter the market decades behind many well-equipped organizations with documented success. Partnering allows us to divide and conquer -- PHPI collects and distributes donations through the website and NGOs manage the loans, which they make from our donations, on the ground -- allowing us maximum efficiency at the lowest cost.
    </a> </p>
    </li>
</ul>

			</div>  
              </div>
               </div>

            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1b">  
How does PHPI choose its microfinance NGOs?
				</a>  
     		</div>  
              <div id="collapse1b" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>
PHP works with established microfinance institutions which have a social mission of lending to the poor. We are very careful with whom we partner. Our president and computer staff have spent time with the staff of the NGOs and government regulators to be sure that our goals are similar.

    </a> </p>
    </li>
</ul>

			</div>  
              </div>
               </div>

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1c">  
What are the NGO's responsibilities?				</a>  
     		</div>  
              <div id="collapse1c" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>
Our NGOs are responsible for vetting loan applications to ensure that loans go to businesses with a high chance of success. The NGO uploads (posts) the loan details, including a photo of the entrepreneur and a description of the business, to PHPI's website.</p><p>
Once a loan has been fully funded, our NGOs are responsible for managing the loan on the ground, including distributing loan funds, collecting repayments and providing reasonable educational support to entrepreneurs to ensure the highest chance of business success. NGOs are responsible for providing updates on the progress of the business through PHPI's software.</p><p>
Finally, NGOs are responsible for forwarding principle repayments to PHPI on a monthly or quarterly basis.
    </a> </p>
    </li>
</ul>

			</div>  
              </div>
               </div>
               
                           <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1d">  
Do NGOs charge interest to the entrepreneurs?
				</a>  
     		</div>  
              <div id="collapse1d" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>
Yes, self-sustainability is critical to creating long-term solutions to poverty, and charging interest to entrepreneurs is necessary for microfinance institutions to achieve this. NGOs are free to charge interest, but PHPI will not partner with an organization that charges exorbitant interest rates. We also require NGOs to fully disclose their interest rates. Microfinance is an expensive business, which is essentially the reason small loans are not provided by large banks. While PHPI's NGOs do not bear the cost of capital or the cost of default, they do bear transaction costs and currency risk. Charging interest to entrepreneurs enables our NGOs to bear these costs and achieve self-sustainability.   </a> </p>
    </li>
</ul>

			</div>  
              </div>
               </div>
</div>

