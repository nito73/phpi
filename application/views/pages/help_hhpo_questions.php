 <div class="accordion" style="padding:10px">  
<h3>General NGO Questions</h3>
            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1">  
Why don't you work with NGOs in XX country?
				</a>  
     		</div>  
              <div id="collapse1" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>
A list of PHPI's current NGO's can be found when you click on "Plant a Seed" on our homepage. We are working hard to expand our partnerships to all regions across the globe at a pace that is healthy for both our current partners and for PHPI. 
    </p><p>
If we do not partner with an NGO in your area of interest, you can check out the Mix Market, a great website that collects data on microfinance institutions around the world. You may be able to find a microfinance institution in your area of interest on this site. Please refer to their website at http://www.mixmarket.org.    </a> </p>
    </li>
</ul>

			</div>  
              </div>
               </div>
               
                           <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1a">  
Does PHPI facilitate loans to entrepreneurs in the United States?
				</a>  
     		</div>  
              <div id="collapse1a" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>
Eventually PHPI would love to work with partners in the United States. For now, we are not currently facilitating loans in the U.S., as it is more complex legally to facilitate peer-to-peer lending in the United States. By focusing on lending in the developing world, more social good can be created for less money; it is cheaper to start a business in the developing world than in the developed world. Finally, please know that there is already an organization which facilitates peer-to-peer online lending in the US - check them out at <a href="http://www.prosper.com">http://www.prosper.com</a>.

    </a> </p>
    </li>
</ul>

			</div>  
              </div>
               </div>

            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1b">  
Can I contact a NGO?				</a>  
     		</div>  
              <div id="collapse1b" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>
PHPI posts all available contact information for its NGOs on our "Plant a Seed" page. If you click on the name of an NGO, you will be taken to the partner's Profile Page where you will find its contact information.
    </p><p>
Please note that many of the NGOs that work with PHPI have limited access to the Internet, may not speak English, and have a very small number of staff members. For these reasons, it may be difficult for a partner to respond to your inquiry.  </a> </p>
    </li>
</ul>

			</div>  
              </div>
               </div>

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1c">  
Can I contact the entrepreneur?
				</a>  
     		</div>  
              <div id="collapse1c" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>
PHPI and its NGOs cannot facilitate direct communication between donors and entrepreneurs at this time. Many entrepreneurs do not have access to the Internet, they speak another language than you do, and PHPI's NGOs do not have enough time to facilitate this kind of communication.    </a> </p>
    </li>
</ul>

			</div>  
              </div>
               </div>
               
                           <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1d">  
Can I Visit an individual entrepreneur?				</a>  
     		</div>  
              <div id="collapse1d" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>
PHPI and its NGOs cannot facilitate visits at this time. If even a fraction of PHPI's donors visited the entrepreneurs they support, our NGOs would be swamped and unable to proceed with their primary function of working with entrepreneurs.

    </a> </p>
    </li>
</ul>

			</div>  
              </div>
               </div>

            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1e">  
There are no businesses from country XX on your site right now, why?
				</a>  
     		</div>  
              <div id="collapse1e" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>
PHPI posts new loans on its website every day, so if you don't see a loan that meets your criteria today, please check back again soon. Loans do move through the site quickly, as the average time it takes for a loan posted on PHPI's website to receive full funding is quite short.    </a> </p>
    </li>
</ul>

			</div>  
              </div>
               </div>

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1f">  
Can I recommend an NGO to PHPI?			</a>  
     		</div>  
              <div id="collapse1f" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>
If you know of a microfinance institution (MFI) that fits PHP's criteria, please contact the MFI directly and ask them to review our minimum criteria to ensure they are a good fit and, if so, to submit an MFI profile using our "Become a Partner Organization" page.

    </a> </p>
    </li>
</ul>

			</div>  
              </div>
               </div>
               
                           <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1g">  
I am traveling to XX country and would like to help find an NGO partner. Is there anything I can do?
				</a>  
     		</div>  
              <div id="collapse1g" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  

    <p>
Below are a few ways in which you can become involved with PHPI.<ul>
<li>Become a PHPI donor and support entrepreneurs as they lift themselves out of poverty.
<li>Join PHPI as a volunteer ambassador. Visit our "Get Involved" section and learn about our "Ambassador Program." 
<li>Keep an eye out for future positions and volunteer opportunities with PHPI on our Get Involved section.
</ul>    </a> </p>


			</div>  
              </div>
               </div>

            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1h">  
Why do some NGOs post group loans?
				</a>  
     		</div>  
              <div id="collapse1h" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>
Group loans are a powerful innovation in microfinance, because they are often less expensive for partners to manage in terms of time and resources. With a group, microfinance institutions can leverage the local knowledge of individuals to select good borrowers; disburse many loans at once; collect repayments in a group; and more easily follow up on delinquent loans, as group members experience peer pressure and have an incentive to work with each other to ensure on-time repayment. By using these efficient aspects of group lending, microfinance institutions can issue more loans in smaller amounts to the poorest.  </a> </p>
    </li>
</ul>

			</div>  
              </div>
               </div>

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1i">  
How does a group loan work?
				</a>  
     		</div>  
              <div id="collapse1i" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>
In a group loan, each member of the group receives an individual loan but is bound by a 'group guarantee' (sometimes called 'joint liability'). Under this arrangement, each member of the group supports the others for paying back the loans of their fellow group members using peer pressure, a very effective method if someone is delinquent or defaults.

    </a> </p>
    </li>
</ul>

			</div>  
              </div>
               </div>
 
</div>

