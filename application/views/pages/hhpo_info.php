<div style="padding:10px;">

			<div align="center" ><strong>Overview</strong> |  <a href="/pages/view/micro_seed_model">The Micro Seed Model</a>  |  <a href="/pages/view/become_a_hhpo">Become a NGO Partner</a></div>
			<br />
			
<h2>Information for Prospective NGOs</h2>

<strong>Our Work</strong>
<p>People Helping People International (PHPI) enables non-government organization (NGO) microfinance institutions to raise debt capital directly from our MicroSeed Program donors. PHPI provides NGOs with 0% interest US dollar debt capital from our MicroSeed donors. NGOs then lend this capital to poor entrepreneurs or groups of entrepreneurs at prevailing interest rates and keep the interest income. Losses arising from client default are borne by PHPI's MicroSeed loan donors. </p>
<strong>Our Growth</strong>
<p>
PHPI partners with highly reputable NGOs. Well-established NGOs use PHPI's online fund-raising platform to access low-cost debt financing from PHPI's donors through our MicroSeed Program via the Internet.
</p>
<p>All NGOs benefit significantly by reducing their cost of raising capital and by being able to exhibit their work to an international audience.
</p><p>
If your organization does not fit the criteria or operational requirements listed above, please check back with us again in the future when you do. 
</p><p><ul>
<i>
<li>Do you need additional information? </li>
<li>Do you have a recommendation for an NGO that would benefit from a partnership with PHPI? </li> 
<li>Are you wondering why our MicroSeed Program does not have a partner in XX country?</li>
<li> Are you an organization looking for a different type of partnership?</li> 
</i></ul></p>
<p>Please look at the Help Information or About section on our homepage.</p>
<p><h3>Interested in Partnering with PHP International?</h3> </p>
<p>
<strong>Level I Microfinance NGO Profile</strong><br /> 
To be considered for Level I PHPI Partnership, an NGO must submit, at a minimum:<br />
<ul>
<li>Brief Application (Apply now via Email) </li>
<li>Your mission statement </li>
<li>Show proof of being registered as a legal entity qualified to provide microloans in your country</li>
<li>Show proof of having a maximum U.S. dollar credit line of $10,000-$20,000</li>
<li>Internet access</li>
<li>Staff capable of posting loans to PHPI's website for funding</li>
<li>At least one member of your staff with proficient English communication skills</li>
<li>Willing to provide an adequate number of computers</li>
<li>Willing to use PHPI's free, transparent microloan management software program which includes: </li><ul>
<li>Loan/ Project application</li>
<li>Loan Repayment Tracking</li>
<li>Automatic Quarterly Loan Principal Repayment Deposit System provided by PHPI</li>
<li>Savings Account program (optional)</li>
</ul></ul>
<strong>Level II Microfinance NGO Profile </strong><br />
To be considered for Level II PHPI Partnership, an NGO must submit, at a minimum:<br /><ul>
<li>A more detailed application ( Apply now via Email)</li>
<li>Your mission statement</li>
<li>Serve at least 1000 active borrowers with microfinance services</li>
<li>Have a history (at least 2-3 years) of lending to the poor for the purpose of alleviating poverty</li>
<li>Show proof of being registered as a legal entity qualified to do microloans in your country </li>
<li>Proof of having a maximum U.S. dollar credit line of $100,000-$200,000</li>
<li>Adequate English communication skills necessary to post loans and/or projects to PHPI's website for funding</li>
<li>Have adequate number of in-house computers with internet access</li>
<li>Willing to use PHPI's free, transparent microloan management software program which includes: </li><ul>
<li>Loan/ Project application</li>
<li>Loan Repayment Tracking</li>
<li>Automatic Quarterly Loan Repayment Principle Deposit System provided by PHPI</li>
<li>Saving Account program (optional)</ul></ul></p> 

			</div>