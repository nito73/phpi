<div class="accordion" style="padding:10px">  
<h3>About Donating</h3>
            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1">  
How do I donate to a business?				</a>  
     		</div>  
              <div id="collapse1" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
    <p>
<ol>
<li>Click on the "Plant a Seed" button at the top of the PHP International home page.<br />
<li>You will automatically be taken to the <a href="http://www.phpintl.org/give" >"View All Activities Page."</a> From here you may explores the types of loans or projects to which you are interested in donating.<br />
</ol>    </a> </p>


			</div>  
              </div>
               </div>
               
                           <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1a">  
What happens after I donate?
				</a>  
     		</div>  
              <div id="collapse1a" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>
When a loan or project has been fully funded it is moved to the "Fully Funded Loans" 
or "Fully Funded Projects" section of our website. You may follow your loan or project's 
progress by viewing the "In Need of Funding" section. When it is fully funded it is 
moved to the "Approved Loan/Project" section. When the loan has been paid off the 
NGO will donate the principal amount the NGO was able to collect back to PHPI. PHP 
International will credit your proportionate amount to your "MicroSeed Account" less an 
8% administrative funding fee, making the remainder available for you to re-donate to 
other loans or projects.

    </a> </p>
    </li>
</ul>

			</div>  
              </div>
               </div>

            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1b">  
Can I deposit funds into my account now and donate them later?				</a>  
     		</div>  
              <div id="collapse1b" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>
Presently this option is not available via the website; however, we have plans to add this 
option at a later date. You may send us a check for amounts over $500 and we will make 
the entry for you into your MicroSeed Account. 
    </a> </p>
    </li>
</ul>

			</div>  
              </div>
               </div>

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1c">  
How do I donate to my MicroSeed Account?
				</a>  
     		</div>  
              <div id="collapse1c" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
    <p>
<ol>
<li>Log into PHP International.
<li>Click "Plant a Seed" to browse the microloan business sectors, meso/macro ventures and
projects in need of funding.
<li>Select the loan(s) or project(s) you wish to donate to and pay through PayPal.
<li>If you have money in your MicroSeed Account, it will reduce the amount you will need 
to contribute through Pay Pal.   </a> </p>
</ol>    


			</div>  
              </div>
               </div>
               
                           <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1d">  
Can we donate as a group?
				</a>  
     		</div>  
              <div id="collapse1d" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>
Many groups donate together through our MicroSeed Account program. 
However, we are still working on a formal group feature for the website -- 
so at this point, it is not possible for us to collect and aggregate payments for 
one account. However, you can designate one individual to be your group's 
MicroSeed Account administrator who manages the donation process. All 
donations and re-donations will be in your MicroSeed Account administrator's 
name. It is up to you how your group to decide how you want to pool your 
money.
<br /><br />
Once we have set up your MicroSeed Account, you can share your account 
activity report with your group members. This report will display the donations 
made by your group.

    </a> </p>
    </li>
</ul>

			</div>  
              </div>
               </div>

            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1e">  
Can my company create an account?
				</a>  
     		</div>  
              <div id="collapse1e" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>
There are many ways to personalize your company's account. For example, upon 
registration you can make your MicroSeed Account user name reflect the name of your 
company.

    </a> </p>
    </li>
</ul>

			</div>  
              </div>
               </div>

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1f">  
Can I donate or donate in someone's memory?
				</a>  
     		</div>  
              <div id="collapse1f" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>
At this time we do not have a feature on our website that allows users to make a loan or donation in another individual's name.

    </a> </p>
    </li>
</ul>

			</div>  
              </div>
               </div>
               
                           <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1g">  
Can I donate if I reside outside the United States?
				</a>  
     		</div>  
              <div id="collapse1g" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p>
PHP International donors reside all over the world. If you have trouble making your payment, please contact the PayPal Customer Service line created by PayPal specifically for PHP international users at 888-221-1161 (Toll Free US and International), Monday through Friday, 8 a.m. to 5 p.m. CST. This phone number is staffed by PayPal employees who are specially trained to help PHP International MicroSeed Account donors!  </a> </p>
    </li>
</ul>

			</div>  
              </div>
               </div>


</div>

			