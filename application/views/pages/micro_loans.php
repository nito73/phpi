<div style="padding:10px;">

				<h1>
MICRO LOAN BUSINESS SECTORS
</h1>
<br>
<p>
There are five distinct business sectors that PHPI and our NGOs have identified in which donors may have an interest. They are:
</p>
<ul>
    <li>
    <span style="font-size: 13px;"><span style="font-weight: bold;">Food Industry</span></span> - businesses that produce or sell packaged or manufactured food out of a store, e.g. restaurants, bakeries, ice cream shops, etc.</li>
    <li>
    <span style="font-size: 13px;"><span style="font-weight: bold;">Agriculture</span></span> - businesses that grow crops, raise animals, fish, etc. directly from the land or water.</li>
    <li>
    <span style="font-size: 13px;"><span style="font-weight: bold;">Service Industry</span></span> - businesses that provides services, e.g. repairs, hair dressers, communications centers, etc.</li>
    <li>
    <span style="font-size: 13px;"><span style="font-weight: bold;">Small Trade Industry</span></span> - any business that sells a product on a one-to-one basis, e.g. beans, t-shirts, yarn, jewelry, etc.</li>
    <li>
    <span style="font-size: 13px;"><span style="font-weight: bold;">Manufacturing</span></span> - businesses capable of producing a product, e.g. cement blocks, seamstresses, furniture makers, etc.</li>
</ul>
<p>
These business sectors have entrepreneurs who have the capability to repay the loan to our NGO who in turn pays back the principal to PHPI for re-funding to new businesses/groups. Through faith the size of a mustard seed, you become a part of moving mountains of need to mountains of joy! </p><br/>
<p>People Helping People International accepts donations from $25 U.S. or more through PayPal.  If you wish to make a donation in a large amount, $300 - $2000, you simply make choices from the five business sectors to diversify the amount you donate to each business. </p>
<p>
<a class="pull-right btn btn-large btn-success" href="/give/viewLoanType/Micro">View Loans</a>
</p>
			</div>