<div style="padding:10px;">
<h1>
PROJECTS</h1>
<p>Tax-deductible donations to PROJECTS are in the form of gifts, with no expectation of any monetary return to your MicroSeed Account.  You simply review the projects posted to our website by our NGO partners and select the one(s) you would like People Helping People International to fund for you.</p>

<a class="pull-right btn btn-large btn-success" href="/give/viewLoanType/Projects">View Projects</a>

</div>