
<div class="accordion" style="padding:10px">  
<h3>Donating</h3>
            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">  
                 About Donating
                </a>  
              </div>  
              <div id="collapseOne" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
            
<ul type="disc">
    <li><span style="font-size: 13px;"><a href="#question0">How do I donate to a business?</a></span></li>
    <li><span style="font-size: 13px;"><a href="#question1">What happens after I donate?</a></span></li>
    <li><span style="font-size: 13px;"><a href="#question2">How do I deposit funds into my account and donate them later?</a></span></li>
    <li><span style="font-size: 13px;"><a href="#question3">How do I donate with the "Micro Seed Program Credit" in my Founder's Mustard Seed account?</a></span></li>
    <li><span style="font-size: 13px;"><a href="#question4">Can we donate as a group?</a></span></li>
    <li><span style="font-size: 13px;"><a href="#question5">Can my company create an account?</a></span></li>
    <li><span style="font-size: 13px;"><a href="#question6">Can I donate or donate in someone's memory?</a></span></li>
    <li><span style="font-size: 13px;"><a href="#question7">Can I donate if I reside outside the United States?</a></span></li>
</ul>
<strong><a id="question0" name="question0"></a><span class="no_u">How do I donate to a business?</span></strong>
<p>
</p>
<ol style="color: rgb(62, 33, 15);">
    <li><span style="font-size: 13px;">Click on the "Dona<span style="font-size: 13px;">tion</span>" button at the top of the PHP International home page.</span></li>
    <li><span style="font-size: 13px;">You will automatically be taken to the micro loan, macro/meso loan and project explanation page.&nbsp; From here you may view the types of loans or projects that you are interested in donating to.</span></li>
</ol>
<p></p>
<strong><a id="question1" name="question1"></a><span class="no_u">What happens after I donate?</span></strong>
<p><span style="font-size: 13px;">When a loan or project has been fully funded it is moved to the "Funded Loans" or "Funded Projects" section of our website.&nbsp; You ma<span style="font-size: 13px;">y follow your loa<span style="font-size: 13px;">n on project<span style="font-size: 13px;">s progr<span style="font-size: 13px;">ess by viewing the <span style="font-size: 13px;">"<span style="font-size: 13px;">In Need of <span style="font-size: 13px;">F</span>unding" section or when it is fully fund<span style="font-size: 13px;">ed it is moved to the "<span style="font-size: 13px;">A<span style="font-size: 13px;">pproved loan<span style="font-size: 13px;">/Project" section</span></span></span></span></span></span></span></span></span></span>.  When the loan has been paid off the HHPO will donate back to our Micro Seed Program the principle amount the HHPO was able to collect.&nbsp; PHP International will credit your proportionate amount to your "Founders Micro Seed Account" less an 8% administrati<span style="font-size: 13px;">ve funding fee, </span>making <span style="font-size: 13px;">the remainder</span> available for you to re-donate to other loans or projects.</span></p>
<strong><a id="question2" name="question2"></a><span class="no_u">How do I deposit funds into my account and donate them later?</span></strong>
<p></p>
<ol style="color: rgb(62, 33, 15);">
    <li><span style="font-size: 10px;"><span style="font-size: 13px;">Presently this option is not available <span style="font-size: 13px;">via the website</span>; however, we have plans to add this option at a later date. You may send us a check for <span style="font-size: 13px;">amo<span style="font-size: 13px;">unt<span style="font-size: 13px;">s over $<span style="font-size: 13px;">200 and we w<span style="font-size: 13px;">ill make the entry for you into your founders micro seed activity account. </span></span></span></span></span></span><br>
    </span></li>
</ol>
<p></p>
<strong><a id="question3" name="question3"></a><span class="no_u">How do I donate with the "Micro Seed Program Credit" in my Founder's Mustard Seed account?</span></strong>
<p>
</p>
<ol style="color: rgb(62, 33, 15);">
</ol>
<ol>
    <li><span style="font-size: 13px;">Log into PHP International.</span></li>
    <li><span style="font-size: 13px;">Click "Donat<span style="font-size: 13px;">ion</span>" to browse the micro loan business sectors, macro/meso ventures and projects in need of funding.</span></li>
    <li><span style="font-size: 13px;"><span style="font-size: 13px;">Select the loan(s)</span> you wish to donate and pay through "<span style="font-size: 13px;">P</span>ay<span style="font-size: 13px;">P</span>al".</span></li>
</ol>
<strong><span class="no_u">Can we donate as a group?</span></strong>
<p><span style="font-size: 13px;">Many groups donate together through our PHP International Micro Seed Program. However, we are still working on a formal group feature for the website -- so at this point, it is not possible for us to collect and aggregate payments for one account. However, you can designate one individual to be your group's Founders M<span style="font-size: 13px;">icro</span> Seed Account administrator who manages the donation process.&nbsp; All donations and re-donations will be in your account administrator's name.&nbsp; It is up to you how you want to pool your money together.</span></p>
<p><span style="font-size: 13px;">Once we have set up your Founders M<span style="font-size: 13px;">icro</span> Seed Account, you can share your account activity report with your group members. This report will display the donations made by your group. </span></p>
<strong><a id="question5" name="question5"></a><span class="no_u">Can my company create an account?</span></strong>
<p><span style="font-size: 13px;">There are many ways to personalize your company's account. For example, upon registration you can make your PHP International Micro Seed Program user name reflect the name of your company.</span></p>
<strong><a id="question6" name="question6"></a><span class="no_u">Can I donate or donate in someone's memory?</span></strong>
<p><span style="font-size: 13px;">At this time we do not have a feature on our website that allows users to make a loan or donation in another individual's name. </span></p>
<strong><a id="question7" name="question7"></a><span class="no_u">Can I donate if I reside outside the United States?</span></strong>
<p><span style="font-size: 13px;">PHP International donors reside all over the world.</span></p>
<p><span style="font-size: 13px;">If you have trouble making your payment, please contact the Paypal Customer Service line created by Paypal specifically for PHP International users at 		           	     (Toll Free US) or 				 (International), Monday through Friday, 8 a.m. to 5 p.m. CST. These phone numbers are staffed by Paypal employees who are specially trained to help PHP International Micro Seed Program donors!</span></p>


			</div>  
              </div>  
            </div>  
            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">  
            Completing the Payment Process
                </a>  
              </div>  
              <div id="collapseTwo" class="accordion-body collapse">  
                <div class="accordion-inner">  
                
<ul type="disc">
    <li><a href="#questiona">Was my transaction successful?</a> </li>
    <li><a href="#questionb">Do I have to use Paypal?</a> </li>
    <li><a href="#questionc">Can I pay by check?</a> </li>
    <li><a href="#questiond">Will I get a receipt?</a> </li>
    <li><a href="#questione">What if Paypal will not accept my credit card?</a> </li>
    <li><a href="#questionf">Why did I receive a receipt from Paypal when I       paid with my credit card?</a> </li>
</ul>
<strong><a id="questiona" name="questiona"></a><span class="no_u">Was  my transaction successful?</span></strong>
<p>When you successfully complete a transaction, you  receive a receipt confirming your purchase from Paypal. You can  log into your Founders Mustard Seed Account at any time to view the loans to which you contributed. These are two great steps to take to  ensure that your purchase was successful.<br>
<br>
Simply log into your Founders Mustard Seed Account by clicking "My Account" to view your donation history.</p>
<strong><a id="questionb" name="questionb"></a><span class="no_u">Do  I have to use Paypal?</span></strong>
<p>You can pay by credit card through "My Project  Donation Investments", even if you don't have a Paypal account. </p>
<strong><a id="questionc" name="questionc"></a><span class="no_u">Can I pay by check?</span></strong>
<p>PHP International Micro Seed Program only accepts checks for amounts of $1,000 or more  and also asks for a donation of 8% to help cover administrative costs. We  maintain this policy in order to keep our operational expenses low. All checks  valuing less than $1,000 will be destroyed to protect your financial  information.</p>
<h4>To pay by check:</h4>
<p><strong>Step 1:</strong> Register for a Founders Mustard Seed Account</p>
<p><strong>Step 2:</strong> Write your check payable to "People Helping People International, Inc."</p>
<p><strong>Step 3</strong>: On the back of the check, write the email  address that you use to log in to your
Founders  Mustard Seed Account, the total of your check that you would like to  be credited to your Founders Mustard Seed Account and the total of your  check that is a donation to PHP</p>
<p><strong>Step  4:</strong>&nbsp; Mail the check to:</p>
<p>- People Helping  People International, Inc</p>
<p>- Attn: Mary Kilsdonk</p>
<p>- 9 Church Street, Suite 203</p>
<p>- Hornell, NY&nbsp; 14843</p>
<p><strong>Step5:</strong>&nbsp; Indicate on the check the loan/project for which the money is to be used.</p>
<p>*When  your check payment has been processed, we will credit your account and notify<br>
you by email.</p>
<strong><a id="questiond" name="questiond"></a><span class="no_u">Will  I get a receipt?</span></strong>
<p>Yes, you will receive an email from Paypal confirming  your donation. And we will email a tax receipt to the email address you gave us  when you created your Founder's Mustard Seed Tree Account. </p>
<strong class="no_u"><a id="questione" name="questione"></a>What if Paypal  will not accept my credit card?</strong>
<p>PHP International does not have access to your payment details, as Paypal processes all of our financial transactions.
<br>
<br>
If you have trouble making your payment, please contact the Paypal Customer Service line created by Paypal specifically for PHP International Micro Seed Program users at 			(Toll Free US) or 		(International), Monday through Friday, 8 a.m. to 5 p.m. CST. These phone numbers are staffed by Paypal employees who are specially trained to help PHP International Micro Seed Program donors!
</p>
<strong><a id="questionf" name="questionf"></a><span class="no_u">Why  did I receive a receipt from Paypal when I paid with my credit card?</span></strong>
<p>Paypal processes all of our Micro Seed Loan Program's financial transactions. For this reason, regardless of whether you pay for a transaction with funds from a personal credit card or with funds from a Paypal account, you receive a receipt from Paypal recording the purchase. </p>
<p>If you have any further questions about your transaction, please do not hesitate to contact Paypal directly at 			(Toll Free US) or 			(International), Monday through Friday, 8 a.m. to 5 p.m. CST. These phone numbers are staffed by Paypal employees who are specially trained to help PHP International Micro Seed Program donors!</p>
</div>  
              </div>  
            </div>  
       </div>  
          
          
          
<div class="accordion" style="padding:10px">  
<h3>Keeping Track of Your Donations</h3>
            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">  
                 Tracking Repayments
                </a>  
              </div>  
              <div id="collapseThree" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p><a href="#question00">How       can I view the donations made in my Founders Mustard Seed Account?</a> </p>
    </li>
    <li>
    <p><a href="#question01">When       a business makes a partial repayment to the HHPO, can I re-donate my funds       immediately?</a> </p>
    </li>
    <li>
    <p><a href="#question02">What       if a loan is only partially repaid?</a> </p>
    </li>
    <li>
    <p><a href="#question03">When       is a loan considered to be in default?</a> </p>
    </li>
    <li>
    <p><a href="#question04">Can I       forgive a loan?</a> </p>
    </li>
</ul>
<strong><a id="question00" name="question00"></a>How can I view the donations in my Founder's  Mustard Seed Account?</strong>
<p>You can log into your PHP Founder's Mustard Seed Account  at any time to view the loans our HHPO has made or projects you have donated  to.&nbsp; Simply log into your account and  click "My Account" and login if you have not already done so and you will be taken directly to your "Founder's Mustard Seed  Account". </p>
<strong><a id="question01" name="question01"></a>When a business makes a partial  loan repayment, can I re-donate my funds immediately?</strong>
<p>No, your Founder's Mustard Seed Account will not be  credited with your loan funds until the total amount possible has been collected by our HHPO and they have deposited the principle amount back to PHP, at which time,  you may re-donate to a new entrepreneur, macro/meso venture or project. <br>
<br>
Currently PHP receives quarterly repayments from our HHPOs. The  funds from all partial repayment donations are then deposited into a U.S. bank  account where they earn an average interest rate. All funds generated from this  interest are used to help cover PHP's operational expenses.</p>
<strong><a id="question02" name="question02"></a>What if a loan is only partially  repaid?</strong>
<p>If a partially repaid loan is declared in default (it  is determined the entrepreneur cannot repay the rest of the loan), the principle  amount of repayments collected so far by the HHPO will be donated back to PHP  by the HHPO and distributed in proportion to each donors donation to the loan.  In short, PHP never keeps any of the funds, we always send 100% of funds and  100% of principle repaid to our HHPO by the entrepreneurs is credited to the donor's  Founder's Mustard Seed Account. </p>
<strong><a id="question03" name="question03"></a>When is a loan considered to be in  default?</strong>
<p>In micro finance, it is common for our HHPO's to try  to reschedule donations in delinquency and default in order to accommodate the  eventual repayment of the loan. At PHP, we define default (non-repayment) as 6  months after the official due date of the loan. At that point, we  "write-off" the defaulted amount on the loan, and it is moved from delinquency to default on  the balance sheet.</p>
<strong><a id="question04" name="question04"></a>Can I forgive a loan?</strong>
You cannot request that an  entrepreneur not be required to repay your donated portion of the HHPO loan he  or she received. If an entrepreneur defaults on his or her loan, the  outstanding payments must be forgiven by our HHPO. PHP does not facilitate  taking action against an entrepreneur whose loan has defaulted, so all default  costs are borne by those who request of PHP to donate to an entrepreneur selected by our HHPO.



			</div>  
              </div>  
            </div>  
            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">  
                 Re-Donating 
                </a>  
              </div>  
              <div id="collapseFour" class="accordion-body collapse">  
                <div class="accordion-inner">  
                
<ul type="disc">
    <li>
    <p><a href="#question0a">How can       I re-donate funds from a repaid loan?</a> </p>
    </li>
    <li>
    <p><a href="#question1a">How can       I donate funds from a repaid loan to PHP?</a> </p>
    </li>
    <li>
    <p><a href="#question2a">How can I withdraw funds from my Founders Mustard Seed Account?</a> </p>
    </li>
</ul>
<strong><a id="question0a" name="question0a"></a><span class="no_u">How  can I re-donate funds from a repaid loan?</span></strong>
<p><strong>*Step 1:</strong> Log into your Founders Mustard Seed Account through  the "My Account" button on our home page and see what your balance is.</p>
<p><strong>*Step 2:</strong> Determine how much of it you wish to  re-donate to a business sector or project and follow the normal donation  process.&nbsp; PHP will debit your account for  the amount.</p>
<strong><a id="question1a" name="question1a"></a><span class="no_u">How  can I donate funds from a repaid loan to PHP?</span></strong>
<p>At this time it is not possible to do so.&nbsp; We ask that you simply send us a check with the words "For PHP Administrative Expenses" written on the back. </p>
<strong><strong><a id="question2a" name="question2a"></a></strong><span class="no_u">How can I withdraw  funds from my Founders Mustard Seed Account?</span> </strong>
<p>You can't, as what you  donate via Paypal to PHP is considered a donation for which you get the benefit of a tax  deduction.&nbsp; The funds are credited to  your personal "Founder's Mustard Seed Account" and used to fund loans and  projects through our HHPO(s).</p>

 </div>  
              </div>  
            </div>  
             
          </div>  
          
          
                             
          
          
<div class="accordion" style="padding:10px">  
<h3>Managing Your Account</h3>
            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFive">  
                Changing Your Founders Mustard Seed Account Information
                </a>  
              </div>  
              <div id="collapseFive" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p><a href="#question0b">How       do I Change the email address registered with my Founder's Mustard Seed Account?</a> </p>
    </li>
    <li>
    <p><a href="#question1b">How       do I reset my Password</a> </p>
    </li>
</ul>
<strong><a id="question0b" name="question0b"></a><span class="no_u">How  do I Change the email address registered with my account?</span></strong>
<p>*Step 1:</p>
<p>*Step 2:</p>
<p>*Step 3:</p>
<p>*Step 4: </p>
<strong class="no_u"><a id="question1b" name="question1b"></a>How  do I reset my Password</strong>
<p>Select "Log In" at the top of the PHP home  page. Then click the "Forget Your Password?" link. Enter your email  address, and we'll send you instructions on how to reset your password.</p>




			</div>  
              </div>  
            </div>  
</div>
           
             
          
          <div class="accordion" style="padding:10px">  
<h3>Troubleshooting</h3>
            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseSix">  
                 Accessing Your Founders Mustard Seed Account
                </a>  
              </div>  
              <div id="collapseSix" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  

<ul type="disc">
    <li>
    <p><a href="#question0c">Why can't I log into my Founders Mustard Seed       Account?</a> </p>
    </li>
</ul>
<strong><a name="question0c" id="question0c"></a><span class="no_u">Why  can't I log into my Founders Mustard Seed Account?</span></strong>
<p>Account log in issues usually occur due to password  or user email address issues.&nbsp; First,  please be sure that the email address you are entering is the email address  that you registered with your account.&nbsp; If you are still having a problem contact us via email at <span style="text-decoration: underline;">peoplehelpingpeopleintl@gmail.com</span> and we will help you out.&nbsp; </p>



			</div>  
              </div>  
            </div>  
            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseSeven">  
               Other
                </a>  
              </div>  
              <div id="collapseSeven" class="accordion-body collapse">  
                <div class="accordion-inner">  
          
<p>A list of PHP's current HHPOs can be found on at <span style="text-decoration: underline;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span>. </p>
<p> We are working hard to expand our HHPOs at a pace that is healthy for both our  current HHPOs and for PHP.</p>


 </div>  
              </div>  
            </div>  
             
          </div>  
          
          <div class="accordion" style="padding:10px">  
<h3>About Our Organization</h3>
            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseEight">  
                 How Our Program Works
                </a>  
              </div>  
              <div id="collapseEight" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p><a href="#question0d">What       percentage of my donation goes to the entrepreneur?</a> </p>
    </li>
    <li>
    <p><a href="#question1d">Does       PHP charge interest?</a> </p>
    </li>
    <li>
    <p><a href="#question2d">Do       PHP's Helping Hand Partner Organizations (HHPO) charge interest to the       entrepreneurs?</a> </p>
    </li>
    <li>
    <p><a href="#question3d">Why       are your Helping Hand Partner Organizations (HHPO) interest rates so high?</a> </p>
    </li>
    <li>
    <p><a href="#question4d">How       does PHP fund its operations?</a> </p>
    </li>
    <li>
    <p><a href="#question5d">Is       there a "middle man"?</a> </p>
    </li>
    <li>
    <p><a href="#question6d">How       does the money get to the entrepreneur?</a> </p>
    </li>
    <li>
    <p><a href="#question7d">How       can I be sure of the integrity of the entrepreneur?</a> </p>
    </li>
    <li>
    <p><a href="#question8d">How       are the entrepreneurs chosen?</a> </p>
    </li>
    <li>
    <p><a href="#question9d">Do I       get interest on my donation?</a> </p>
    </li>
    <li>
    <p><a href="#question10d">Is my       donation tax-deductible?</a> </p>
    </li>
    <li>
    <p><a href="#question11d">Will       my Founders Mustard Seed Account be replenished?</a> </p>
    </li>
</ul>
<strong class="no_u"><a id="question0d" name="question0d"></a>What  percentage of my donation goes to the entrepreneur?</strong>
<p>100% as PHP asks you to add 8% on to cover the cost  of the Paypal Credit Card and part of our operational expenses. </p>
<strong class="no_u"><a id="question1d" name="question1d"></a>Does  PHP charge interest?</strong>
<p>No, as our HHPO makes the loan and charges interest  to the business entrepreneur to cover its cost of administering the loan and providing basic business courses to help ensure the success of the loan. </p>
<strong class="no_u"><a id="question2d" name="question2d"></a>Does  PHP's Helping Hand Partner Organization (HHPO) charge interest to the  entrepreneurs?</strong>
<p>Yes, self-sustainability is critical to creating  long-term solutions to poverty, and charging interest to entrepreneurs is  necessary for microfinance HHPO's to achieve this. Our Helping Hand Partner  Organizations (HHPO) are free to charge interest, but PHP will not partner with  an organization that charges exorbitant interest rates. We also require Helping  Hand Partner Organizations (HHPO) to fully disclose their interest rates. </p>
<p>Microfinance is an expensive business, which is  essentially the reason small donations are not provided by large banks. Unlike loans in our country which we collect on a monthly basis, micro loans are more costly as they are collected weekly.&nbsp; In addition, basic business courses are included to help ensure the entrepreneur's success.&nbsp; While  PHP's Helping Hand Partner Organizations (HHPO) do not bear the cost of capital  or the cost of default, they do bear transaction costs and currency risk.  Charging interest to entrepreneurs enables our Helping Hand Partner  Organizations (HHPO) to bear these costs and achieve self-sustainability. </p>
<strong class="no_u"><a id="question3d" name="question3d"></a>Why  are your Helping Hand Partner Organization (HHPO) interest rates so high?</strong>
<p>The average interest rate that a PHP Helping Hand  Partner Organization (HHPO) charges is about 21%, and PHP only partners with  microfinance institutions that have a social mission of donating to the poor.  Most of our HHPO's also provide education courses as a precursor to giving  donations.&nbsp; This has proven to improve  the success of our donations. <br>
<br>
The nature of microcredit "small donations" is such that interest  rates need to be high to return the cost of the donation to give. To quote  (CGAP) (Consultative Group to Assist the Poor):<br>
<br>
"There are three kinds of costs the HHPO has to cover when it makes  micro-donations. The first two, the cost of the money that it donates and the  cost of donation defaults, are proportional to the amount donated. For  instance, if the cost paid by the HHPO for the money it donates is 10%, and it  experiences defaults of 1% of the amount donated, then these two costs will  total $11 for a donation of $100, and $55 for a donation of $500. An interest  rate of 11% of the donation amount thus covers both these costs for either  donation.&nbsp; The third type of cost,  transaction costs, is not proportional to the amount donated. The transaction  cost of the $500 donation is not much different from the transaction cost of  the $100 donation. Both donations require roughly the same amount of staff time  for meeting with the borrower to appraise the donation, processing the donation  disbursement and repayments, and follow-up monitoring. Suppose that the  transaction cost is $25 per donation and that the donations are for one year.  To break even on the $500 donation, the HHPO would need to collect interest of  $50 + 5 + $25 = $80, which represents an annual interest rate of 16%. To break  even on the $100 donation, the HHPO would need to collect interest of $10 + 1 +  $25 = $36, which is an interest rate of 36%. At first glance, a rate this high  looks abusive to many people, especially when the clients are poor. But in  fact, this interest rate simply reflects the basic reality that when donation  sizes get very small, transaction costs loom larger because these costs can't  be cut below certain minimums." (CGAP)</p>
<strong class="no_u"><a id="question4d" name="question4d"></a>How  does PHP fund its operations?</strong>
<p>PHP currently sends 100% of your donations to our  HHPO's. However, self-sustainability is critical to us. To this end, we support  ourselves principally on the 8% Paypal Credit Card and operational fee/surcharge  plus optional donations our donors voluntarily pay to the organization in  addition to the donations they make to fund entrepreneurs and/or projects.&nbsp; We are currently self-sufficient.<br>
<br>
Lastly, we have raised growth capital from a small group including angel  donors, corporate sponsors and foundations. We are incredibly thankful for this  support.</p>
<strong class="no_u"><a id="question5d" name="question5d"></a>Is there a  "middle man"?</strong>
<p>Yes, in fact, there are two. The first is PHP. When  you donate through PHP's website, the funds are credited to a special Founders  Mustard Seed Account set up for you. From PHP the funds from the Founders Mustard Seed Accounts are donated to the  second middle man, our Helping Hand Partner Organization (HHPO), who will  manage the donation including distributing funds and collecting payments. When  the Helping Hand Partner Organization (HHPO) receives the funds, they are then  distributed to the entrepreneur.<br>
<br>
Payments are made from the entrepreneur to the  Helping Hand Partner Organization (HHPO) on a schedule determined by the  Helping Hand Partner Organization (HHPO).&nbsp;  As each payment is collected, the Helping Hand Partner Organization  (HHPO) enters the payment amount into our software over the internet, notifying  PHP and the donors that a payment has been made. When the donation has been  repaid in full, PHP credits the Founders Mustard Seed Account for the principle amount  our HHPO has deposited back to PHP.</p>
<strong><a id="question6d" name="question6d"></a><span class="no_u">How  does the money get to the entrepreneur?</span></strong>
<p>              Donations received by PHP are forwarded, by check or  international wire, to the respective Helping Hand Partner Organization (HHPO)  on a monthly basis. The Helping Hand Partner Organization (HHPO) then  distributes the funds to the entrepreneur.</p>
<p>&nbsp;</p>
<strong class="no_u"><a id="question7d" name="question7d"></a>How  can I be sure of the integrity of the entrepreneur?</strong>
<p>PHP does not send donations directly to the  entrepreneurs. Each donation is managed by a microfinance institution, our  HHPO, that we work with which administers the donations.<br>
<br>
Before an entrepreneur appears on our website they have first been screened by  our Helping Hand Partner Organization (HHPO) for loan application approval.  Each of our Helping Hand Partner Organizations (HHPO) uses their own  application procedures, which PHP has reviewed and approved in combination with our software. This ensures that  your donations are actually going to genuine entrepreneurs who will use the  donation for the purpose they specified.<br>
<br>
For more information, please visit our Risk and Due Diligence center.</p>
<strong class="no_u"><a id="question8d" name="question8d"></a>How  are the entrepreneurs chosen?</strong>
<p>Our Helping Hand Partner Organizations (HHPO) choose  the entrepreneurs whose approved loan applications are posted on our website, together with a picture and profile of what the loan is to be used for.&nbsp; Each  Helping Hand Partner Organization (HHPO) uses their own application vetting  process, reviewed by PHP to confirm that the organization has the ability to handle  PHP's funds responsibly, which are then entered into our system by the HHPO. </p>
<strong class="no_u"><a id="question9d" name="question9d"></a>Do  I get interest on my donation?</strong>
<p>The interest our donors receive is that of  unconditional love with no expectations of anything in return. </p>
<strong class="no_u"><a id="question10d" name="question10d"></a>Is my donation tax-deductible?</strong>
<p>Yes, all money donated is actually a gift to help the  poor and in turn help others.&nbsp; After the HHPO has collected the total amount possible on each loan thay you participate in, the principle portion is given back to PHP.&nbsp; Your Founders Mustard Seed Account is then credited with your proportionate amount of the principle so that you may use it to fund another loan and so the process repeats itself over and over so long as the loans are paid in full.</p>
<strong class="no_u"><a id="question11d" name="question11d"></a>Will My Founders Mustard Seed Account be Replenished?</strong>
<p>Your donation is not guaranteed, therefore there is a  chance that the entrepreneurs may not totally repay their loan so that your Founders  Mustard Seed account may be fully replenished.&nbsp;  Just like in stocks or mutual funds there are also different levels of  risk in funding a micro-seed loan.</p>
<p>Donating at PHP involves risk and PHP does not  guarantee donors that their donations will be repaid.<br>
<br>
To date, PHP's repayment rate is 92%. However, past repayment rates may not  necessarily reflect future repayment rates or the likelihood of repayment by  any particular entrepreneur.<br>
<br>
By donating to multiple entrepreneurs or groups, rather than simply donating to one  entrepreneur, you may be able to increase the likelihood of repayment. We call  this "donation diversification". &nbsp;</p>


	                

			</div>  
              </div>  
            </div>  
            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseNine">  
                 General Questions
                </a>  
              </div>  
              <div id="collapseNine" class="accordion-body collapse">  
                <div class="accordion-inner">  
<ul type="disc">
    <li>
    <p><a href="#question0e">What       kind of organization is PHP?</a> </p>
    </li>
    <li>
    <p><a href="#question1e">What       is PHP's EIN?</a> </p>
    </li>
    <li>
    <p><a href="#question2e">Is PHP       a religious organization?</a> </p>
    </li>
</ul>
<strong><a id="question0e" name="question0e"></a><span class="no_u">What  kind of organization is PHP?</span></strong>
<p>PHP is a 501 (c-3) non-profit public benefit corporation  registered in the state of Utah. </p>
<strong><a id="question1e" name="question1e"></a><span class="no_u">What  is PHP's EIN?</span></strong>
<p>26-3911363. </p>
<strong class="no_u"><a id="question2e" name="question2e"></a>Is  PHP a religious organization?</strong>
<p>No, however, we are spiritually motivated and are  open to partnering with religious organizations.</p>


 </div>  
              </div>  
            </div>  
             
          </div>  
          <div class="accordion" style="padding:10px">  
<h3>Our Helping Hand Partner Organizations (HHPO(s))</h3>
            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTen">  
                About our Helping Hand Partner Organizations (HHPO(s))
                </a>  
              </div>  
              <div id="collapseTen" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  

	             <ul type="disc">
    <li>
    <p><a href="#question0f">What       is a "Helping Hand Partner Organization (HHPO)"?</a> </p>
    </li>
    <li>
    <p><a href="#question1f">Why       does PHP use Helping Hand Partner Organizations (HHPO)?</a> </p>
    </li>
    <li>
    <p><a href="#question2f">How       does PHP choose its Helping Hand Partner Organizations (HHPO)?</a> </p>
    </li>
    <li>
    <p><a href="#question3f">What       are the Helping Hand Partner Organization's (HHPO) responsibilities?</a> </p>
    </li>
    <li>
    <p><a href="#question4f">Do       PHP's Helping Hand Partner Organization's (HHPO) charge interest to the       entrepreneurs?</a> </p>
    </li>
</ul>
<strong class="no_u"><a id="question0f" name="question0f"></a>What  is a "Helping Hand Partner Organization (HHPO)"?</strong>
<p>When we refer to our "Helping Hand Partner  Organizations (HHPO)" we are referring Non-Government Organizations (NGO's) which are microfinance institutions  that we have partnered with to manage and administer the individual micro loans, macro/meso loans and projects. </p>
<strong class="no_u"><a id="question1f" name="question1f"></a>Why  does PHP use Helping Hand Partner Organizations (HHPO)?</strong>
<p>PHP's Helping Hand Partner Organizations (HHPO) are  critical to our ability to provide loans efficiently. By partnering with  microfinance institutions located on the ground, PHP leverages critical  knowledge of the local population and years of experience managing loans. Were  PHP to operate as a microfinance institution ourselves, we would enter the  market decades behind many well-equipped organizations with documented success.  Partnering allows us to divide and conquer -- PHP collecting and distributing  donations through the website and Helping Hand Partner Organizations (HHPO)  managing the loans, which they make from our donations, on the ground --  allowing us maximum efficiency at the lowest cost. </p>
<strong class="no_u"><a id="question2f" name="question2f"></a>How  does PHP choose its Micro Finance Helping Hand Partner Organizations (HHPO)?</strong>
<p>PHP works with established microfinance institutions  with a social mission of lending to the poor.&nbsp;  We are very careful who we partner with.</p>
<br clear="all">
<strong class="no_u"><a id="question3f" name="question3f"></a>What  are the Helping Hand Partner Organization's (HHPO) responsibilities?</strong>
<p>Our Helping Hand Partner Organizations (HHPO) are  responsible for vetting loan applications to ensure that loans uploaded to the  site have a high chance of being a successful business. The Helping Hand  Partner Organization (HHPO) uploads (posts) the loan details, including a photo of the  entrepreneur and a description of the business, to PHP's website.<br>
<br>
Once a loan has been funded our Helping Hand Partner Organizations (HHPO) are  responsible for managing the loan on the ground, including distributing loan  funds, collecting repayments and providing reasonable educational support to entrepreneurs  to ensure the highest chance of business success. Helping Hand Partner  Organizations (HHPO) are responsible for providing updates on the progress of  the business through PHP's software.<br>
<br>
Finally, Helping Hand Partner Organizations (HHPO) are responsible for  forwarding principle repayments to PHP on a monthly or quarterly basis.</p>
<strong class="no_u"><a id="question4f" name="question4f"></a>Do  PHP's Helping Hand Partner Organizations (HHPO) charge interest to the entrepreneurs?</strong>
<p>Yes, self-sustainability is  critical to creating long-term solutions to poverty, and charging interest to  entrepreneurs is necessary for microfinance institutions to achieve this. Our  Helping Hand Partner Organizations (HHPO) are free to charge interest, but PHP  will not partner with an organization that charges exorbitant interest rates.  We also require Helping Hand Partner Organizations (HHPO) to fully disclose  their interest rates. You can find more information about the interest rates  that PHP's Helping Hand Partner Organizations (HHPO) charge.</p>
<p>              Microfinance is an expensive business, which is essentially the reason small  loans are not provided by large banks. While PHP's Helping Hand Partner  Organizations (HHPO) do not bear the cost of capital or the cost of default,  they do bear transaction costs and currency risk. Charging interest to  entrepreneurs enables our Helping Hand Partner Organizations (HHPO) to bear  these costs and achieve self-sustainability.</p>



			</div>  
              </div>  
            </div>  
            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseEleven">  
                Partnering with PHP International
                </a>  
              </div>  
              <div id="collapseEleven" class="accordion-body collapse">  
                <div class="accordion-inner">  
                
<ul type="disc">
    <li>
    <p><a href="#question0g">How       can my organization become a PHP Helping Hand Partner Organization (HHPO)?</a> </p>
    </li>
    <li>
    <p><a href="#question1g">Can I       start a Field Office or PHP affiliate in XX country?</a> </p>
    </li>
    <li>
    <p><a href="#question2g">I       want to start a Field Office in XX country. Does PHP provide funding?</a> </p>
    </li>
</ul>
<strong class="no_u"><a name="question0g" id="question0g"></a>How  can my organization become a PHP Helping Hand Partner Organization (HHPO)?</strong>
<p>Please visit our HHPO partner's overview for complete  information about becoming a PHP Helping Hand Partner Organization (HHPO).  Briefly, in order to become a Helping Hand Partner Organization (HHPO) with PHP,  an organization must, at a minimum:</p>
<p>&nbsp;</p>
<p>              *Serve at least 1,000 borrowers with microfinance services<br>
*Have a history (at least 2-3 years) of lending to poor, excluded, and/or  vulnerable people &nbsp;for the purpose of  alleviating poverty or reducing vulnerability <br>
*Be registered as a legal entity in their country of operation<br>
*Have at least 1 year of financial audits</p>
<p> </p>
<p>              Please note - PHP is unable to work with start-up  microfinance institutions or organizations that do not provide microfinance  services. Additionally, PHP does not provide funding in the form of grants or  donations to any organization. </p>
<p></p>
<p>              If your organization does not fit the minimum criteria for partnership, we  encourage you to:</p>
<p>              *Monitor the PHP website for changes in our partnership criteria.<br>
*Get in touch with us again once you meet the minimum criteria - we would love  to hear from you!</p>
<strong class="no_u"><a name="question1g" id="question1g"></a>Can  I start a Field Office or PHP affiliate in XX country?</strong>
<p>PHP will partner with microfinance institutions across  the globe. At this time, we are focusing our limited resources on developing  additional partnerships and do not have plans to open field offices and/or PHP  affiliates. <br>
<br>
However, below are a few other ways you can become involved with PHP.<br>
<br>
*Apply to join our Ambassador Program. <br>
*Become a PHP donor and support entrepreneurs as they lift themselves out of  poverty. <br>
*Keep an eye out for future volunteer opportunities with PHP.</p>
<strong class="no_u"><a name="question2g" id="question2g"></a>I  want to start a Field Office in XX country. Does PHP provide funding?</strong>
<p>PHP will partner with  microfinance institutions across the globe. At this time, we are focusing our  limited resources on developing additional partnerships and do not have plans  to open field offices and/or PHP affiliates.</p>


          
 </div>  
              </div>  
            </div> 
             <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwelve">  
                 General Helping Hand Partner Organization (HHPO) Questions
                </a>  
              </div>  
              <div id="collapseTwelve" class="accordion-body collapse">  
                <div class="accordion-inner">  
          
<ul type="disc">
    <li>
    <p><a href="#question0h">Why       don't you work with a Helping Hand Partner Organization (HHPO) in XX       country?</a> </p>
    </li>
    <li>
    <p><a href="#question1h">Does PHP       facilitate loans to entrepreneurs in the United States?</a> </p>
    </li>
    <li>
    <p><a href="#question3h">Can I       contact a Helping Hand Partner Organization (HHPO)?</a> </p>
    </li>
    <li>
    <p><a href="#question4h">Can I       contact the entrepreneur?</a> </p>
    </li>
    <li>
    <p><a href="#question5h">Can I       Visit a PHP Helping Hand Partner Organization (HHPO)/entrepreneur?</a> </p>
    </li>
    <li>
    <p><a href="#question6h">There       are no businesses from country XX on your site right now, why?</a> </p>
    </li>
    <li>
    <p><a href="#question7h">Can I       recommend a PHP Helping Hand Partner Organization (HHPO)?</a> </p>
    </li>
    <li>
    <p><a href="#question8h">I am       traveling/in the Peace Corp/living in XX country and would like to help. I       would like to help find a Helping Hand Partner Organization (HHPO). Is       there anything I can do?</a> </p>
    </li>
    <li>
    <p><a href="#question9h">Why       do some partners post group loans?</a> </p>
    </li>
    <li>
    <p><a href="#question10h">How       does a group loan work?</a> </p>
    </li>
</ul>
<strong class="no_u"><a id="question0h" name="question0h"></a>Why  don't you work with a Helping Hand Partner Organization (HHPO) in XX country?</strong>
<p>A list of PHP's current Helping Hand Partner  Organization (HHPO)s can be found on our Partner pages.
<br>
We are working hard to expand our partnerships to all regions across the globe  at a pace that is healthy for both our current partners and for PHPInternational.org. <br>
<br>
If we do not partner with an MFI in your area of interest, you can check out  the Mix Market, which is a great website that collects data on microfinance  institutions around the world. You may be able to find a microfinance  institution in your area of interest on this site. Please refer to their  website at http://www.mixmarket.org.</p>
<strong class="no_u"><a id="question1h" name="question1h"></a>Does  PHP facilitate loans to entrepreneurs in the United States?</strong>
<p>Eventually PHPInternational.org would love to work with partners  in the United States. However, for now, we are not currently facilitating loans  in the U.S., as it is more complex legally to facilitate peer-to-peer lending  in the United States. By focusing on lending in the developing world, more  social good can be created for less money; it is cheaper to start a business in  the developing world than in the developed world. Finally, please know that  there is already an organization which facilitates peer-to-peer online lending  in the US - check them out athttp://www.prosper.com. </p>
<strong class="no_u"><a id="question2h" name="question2h"></a></strong>
<strong class="no_u"><a id="question3h" name="question3h"></a>Can  I contact a Helping Hand Partner Organization (HHPO)?</strong>
<p>PHP posts all available contact information for its Helping  Hand Partner Organization (HHPO)s on our partner page. If you click on the name of a Helping Hand Partner Organization (HHPO), you  will be taken to the partner's Profile Page where you will find its contact  information. <br>
<br>
Please note that many of the Helping Hand Partner Organization (HHPO)s that  work with PHPInternational.org have limited access to the internet, may not speak English,  and have a very small number of staff members. For these reasons, it may be  difficult for a partner to respond to your inquiry.</p>
<strong class="no_u"><a id="question4h" name="question4h"></a>Can  I contact the entrepreneur?</strong>
<p>PHP and its Helping Hand Partner Organization (HHPO)s  cannot facilitate direct communication between donors and entrepreneurs at this  time. Many entrepreneurs do not have access to the internet, they speak another  language than you do, and PHP's Helping Hand Partner Organization (HHPO)s do  not have enough time to facilitate this kind of communication. </p>
<strong class="no_u"><a id="question5h" name="question5h"></a>Can I Visit a PHP  Helping Hand Partner Organization (HHPO)/entrepreneur?</strong>
<p>PHP and its Helping Hand Partner Organization (HHPO)s  cannot facilitate visits at this time. If even a fraction of PHP's donors  visited the entrepreneurs they support, our Helping Hand Partner Organization  (HHPO)s would be swamped and unable to proceed with their primary function of  working with entrepreneurs. </p>
<strong class="no_u"><a id="question6h" name="question6h"></a>There  are no businesses from country XX on your site right now, why?</strong>
<p>PHP posts new loans on its website everyday, so if  you don't see a loan that meets your criteria today, please check back again  soon. Loans do move through the site quickly, as the average time it takes for  a loan posted on PHP's website to receive full funding is quite short. </p>
<strong class="no_u"><a id="question7h" name="question7h"></a>Can I recommend  a PHP Helping Hand Partner Organization (HHPO)?</strong>
<p>If you know of a microfinance institution (MFI) that  fits PHP's criteria, please contact the MFI directly and ask them to review our  minimum criteria to ensure they are a good fit and, if so, to submit an MFI  profile using our Become a Helping Hand Partner Organization (HHPO) page. </p>
<strong class="no_u"><a id="question8h" name="question8h"></a>I  am traveling/in the Peace Corp/living in XX country and would like to help. I  would like to help find a Helping Hand Partner Organization (HHPO). Is there  anything I can do?</strong>
<p>Below are a few ways in which you can become involved  with PHP.<br>
<br>
*Become a PHP lender and support entrepreneurs as they lift themselves out of  poverty. <br>
<br>
*Join PHP as a volunteer ambassador. Visit our "Get Involved" section and learn about our "Ambassador Program." <br>
<br>
*Keep an eye out for future positions and volunteer opportunities with PHP on  our Get Involved section.</p>
<strong class="no_u"><a id="question9h" name="question9h"></a>Why  do some partners post group loans?</strong>
<p>Group loans are a powerful innovation in  microfinance, because they are often less expensive for partners to manage in  terms of time and resources. In a group, microfinance institutions can leverage  the local knowledge of individuals to select good borrowers; disburse many  loans at once; collect repayments in a group; and more easily follow up on  delinquent loans, as group members experience peer pressure and have an incentive to work with each other to  ensure on-time repayment. By using these efficient aspects of group lending,  microfinance institutions can issue more loans in smaller amounts to the  poorest. </p>
<strong class="no_u"><a id="question10h" name="question10h"></a>How does a group loan work?</strong>
<p>In a group loan, each member of the group receives an  individual loan but is part of a group of individuals bound by a 'group  guarantee' (sometimes called 'joint liability'). Under this arrangement, each  member of the group supports one another and is responsible for paying back the  loans of their fellow group members with peer pressure very effective if someone is delinquent or defaults.</p>


 </div>  
              </div>  
            </div>  
             
          </div>  
          