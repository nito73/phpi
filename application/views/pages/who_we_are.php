<div style="padding: 10px;">

				<h2>Who We Are</h2>
<br>
<br />
<strong>OUR VISION:</strong>
<br>
<span style="text-decoration: underline;">BECAUSE OUR MICRO SEED PROGRAM IS EMPOWERED BY DONORS WHO BELIEVE IN GIVING UNCONDITIONAL LOVE</span>
<br>
Matthew 6-3 Jesus said, "but when you give to the needy, do not let your left hand know what your right hand is doing, so that your giving may be in secret. Then your Father, who sees what is done in secret, will reward you." Thus, when our donors give to PHP we give their donation to our Helping Hand Partner Organization who makes the loan or funds a project enabling our donors to give unconditionally.
<br>
<span style="text-decoration: underline;">BECAUSE YOU MAY SEE WHAT THE SOWING OF YOUR MICRO SEEDS YIELD</span><br>
PHP believes in providing connectivity and transparency to our Donors. By posting pictures, videos and descriptions for our loans and projects so our donors are able to see and choose what they would like us to fund and sow micro seeds of love.<br>
<br>
<span style="text-decoration: underline;">BECAUSE YOUR MICRO SEED DONATIONS MAY LIVE ON<br>
<br>
</span>The management of the re-funding process from each Micro Seed Donation Account may be passed on to your heirs so that your giving may continue on and on.


			</div>