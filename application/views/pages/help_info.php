<div style="padding:10px;">

<h2>Help Information</h2>

<h3>Donating </h3>
<ul type="disc">
    <li><a href="/pages/view/about_donating/">About Donating</a> </li>
    <li>
    <a href="/pages/view/help_process/">Completing the Payment Process</a>
    </li>
</ul>
<h3>Keeping Track of Your Donations </h3>
<ul type="disc">
    <li>
    <a href="/pages/view/help_tracking/">Tracking Repayments</a>
    </li>
    <li>
    <a href="/pages/view/help_redonating/">Re-Donating</a> 
    </li>
</ul>
<h3>About Our Organization </h3>
<ul type="disc">
    <li>
    <a href="/pages/view/help_program_works">How Our Program Works</a> 
    </li>
</ul>
<h3>Our Non-Government Organizations </h3>
<ul type="disc">
    <li>
    <a href="/pages/view/help_about_HHPO/">Working with Non-Government Organizations</a>
    </li>
    <li>
    <a href="/pages/view/help_partnering/">Partnering with PHPI</a> 
    </li>
    <li>
    <a href="/pages/view/help_hhpo_questions/">General NGO Questions</a> 
    </li>
</ul>


			</div>