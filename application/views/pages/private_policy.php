<div style="padding:10px">

				<h2>PRIVACY POLICY</h2>


<ol>
<li>People Helping People International (PHPI) will not rent to, sell to or allow third parties to use your personal information.</li>
<li>By default, you will receive update emails on your donations that are sent by our Non-Government microfinance Organizations (NGOs) through the website. PHPI will not disclose your email address to our NGOs in any case -- our emails are sent through a web form so that third parties cannot learn your address. You can choose not to receive these emails through a preference on the website.</li>
<li>PHPI and Special Helping Hand Partner Organizations (HPPOs) who have their own sections on our website will not disclose your donating activity that is identifiable personally to any other party without your consent. PHPI reserves the right to record and display anonymous donating activity on the website and display the general regions where our donors are located.</li>
<li>We value your privacy and take it seriously.</li>
</ol>

			</div>