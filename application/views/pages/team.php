<div style="padding:10px;">
	<h2>Management Team</h2>
<p>

<p>
<strong> <a href="mailto:bobevansdct@hotmail.com"> Robert Evans Sr. | Founder</a> </strong><br />
Robert M. Evans is the founder of People Helping People International and also acts

as volunteer director of the PHPI. Prior to founding PHPI, he was chairman of the 

board and CEO of Dietary Consultants Inc. which provided management services to 

nursing homes, hospitals and colleges. He then established Data Control Technology 

International which is the leading provider of dietary software to nursing homes. 
</p>
<p>
<strong><a href="mailto:mltalbot@aol.com"> Mary Lee Talbot | Director of Administrative Services</a></strong>
<br />
Mary Lee Talbot is an author, editor, researcher and Presbyterian minister. She has a

Ph.D. in history and education from Columbia University, and a M. Div. from Andover 

Newton Theological School and B.A. from the College of Wooster. She has worked for 

the General Assembly staff of the Presbyterian Church (U.S.A.), Pittsburgh Theological 

Seminary, and is currently a free lance writer and editor.
</p>

<strong><a href="mailto:txn5473@gmail.com"> Tony Nguyen | Director of Website Development and Marketing</a></strong><br />
Tony Nguyen has a bachelor's degree from Rochester Institute of Technology in 

information technology and international marketing and is currently heading up PHPI's NGO Development Program
</p>
<p><strong>
<a href="mailto:john.n.doolittle@gmail.com"> John Doolittle | Director of NGO Development</a></strong><br />
John has been an independent development volunteer providing on-site planning

and operational financial assistance to NGO's in Thailand and South Africa. Upon his 

retirement from the business world he joined the U.S. Peace Corps, providing support 

services to an NGO and District Hospital in South Africa. He speaks Lao, French, Swazi 

and Thai. During John's business career he worked 16 years for Kaiser Permanent 

Medical Care, Performance Associates, Inc., Bechtel Corporation, USAID and the U.S. 

Export Import Bank.</p>
<p><strong><a href="mailto:support@phuconcepts.com"> Phu Concepts | Web and Computer Program Design</a>
</strong><br />
Phu Concepts was founded by Latrell Liptrot and Viet Nguyen in 2007 in Rochester, New York. Their partnership began when Liptrot needed a Web site for his modeling career. He hired Nguyen, then a 14-year-old high school student, to design it. Today they are serving over 3,500 clients world wide. Peter Yim has a bachelor's degree from Rochester Institute of Technology. While he was a student, he designed a web-based prototype for the RIT dining room to use to schedule student employees that is now being used throughout the university.
</p>
<p><strong><a href="mailto:tracy@kidsplayintl.org"> Tracy Evans | Kids Play International </a></strong><br />
Kids Play International is a partner of PHPI operated by Tracy Evans, a three-time 

Olympian and a member of the U.S. Ski Team, who owns Athlete Source Casting in 

Park City, Utah. She developed KPI out of her love of sports, her concern for Rwandans 

affected by the genocide 20 years ago and her desire to see girls develop strong and 

healthy attitudes toward life. Tracy received the Female Athlete Philanthropist of the 

Year award from United Athletes Foundation in 2011. Jerry Rice received the male award.</p>

			</div>