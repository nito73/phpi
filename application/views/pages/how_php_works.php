<div style="padding:10px">

				<h2>HOW PHP INTERNATIONAL WORKS</h2>
<h4>FIRST:</h4>
<ul><p>Our MicroSeed Non-Governmental Organization (NGO) finds grass root entrepreneurs who need help using two loan models which assure a very high payback rate. The first is the:<br><p>
<strong><span style="text-decoration: underline;">GROUP LOAN MODEL</span></strong><br>
The group loan involves a group of 5 to 20+ individuals, which becomes the basic unit of operation for our NGOs. The group methodology encourages a culture of financial responsibility where peer-support is an effective substitute for physical collateral and leads to a 99% rate of repayment. Some of our NGOs also require their borrowers to set up savings accounts and save a percentage of the loan amount before they qualify for a loan. The savings is then used as collateral for guaranteeing pay back of the loan. The advantages of the group methodology are:</p>
<ul>
<li>The group is trained to have joint responsibility for loans that are taken by individuals within the group.</li>
<li>The group approach ensures repayments from all individuals in the group in the case of defaults.</li>
<li>The group functions as a forum for discussions on credit discipline and other related issues led by the group's leader.</li>
<li>Each group elects a Group Loan Leader from the individuals within the group. This person's functions include collecting the payments from all the group members and applying peer pressure. </li>
<li>The group also agrees to take the responsibility for individual defaults and pay on behalf of the defaulting member.</li>
<li>Additionally the group can help do the credit appraisal and provide opinions on the credit worthiness of each individual member of the group.</li>
<li>Using a group methodology helps to control cost as one payment for a group of individuals takes the same amount of time as collecting a single individual loan.</li>
<li>NGOs provide the financial collection service at the group's location which could be a village in a rural area or a colony/slum urban area. Some of our NGOs employ group loan overseers in locations where there are enough group loans to warrant a person to work with the groups providing them with training and oversight. This has proven to increase the potential for loan repayment.</li>
</ul>
</p>
Our NGO plays a critical role in generating credit awareness, training and education among borrowers including, in some cases, the start of savings programs. These programs are geared toward raising the credit worthiness of the borrowers to a level sufficient enough to qualify for loans from our NGOs.
<p></p>
<p><strong><span style="text-decoration: underline;">INDIVIDUAL LOAN MODEL</span></strong><br>
This model is straight-forward credit lending to an individual who has demonstrated their ability to repay their loans first through the Group Loan Model. These individuals are categorized as not needing the group default guarantee or peer pressure element provided by group loans. The loans to these entrepreneurs are then posted to our website with a picture(s), written description and/ or personal video and are ready for funding. Loans are given directly to the borrowers and require significantly higher administrative costs than do group loans. It is very important that socioeconomic services such as skill development and business education are provided to help ensure the entrepreneur's successful repayment of the loan.
</p><p>
Projects such as building medical clinics, equipment for Kids Play International, schools, etc. are also posted and funded as a straight donation with no payback requirement. In other words, works of love with no expectations of receiving anything in return.
</p>
</ul>
<h4>SECOND:</h4>
<ul>
<p> You select a business sector loan or project. You simply go to our "Give" pages and view the loans and projects posted by our NGOs that fit your interest.</p>
</ul>
<h4>THIRD:</h4><ul>
<p>When you have chosen the business sector(s) micro, meso/macro loan and/or project(s) that you wish to fund, you can initiate a donation with your credit card through PayPal. The minimum loan is $25 toward any business sector or project that you wish to help with your fully tax-deductible donation.  Through PayPal, using your credit card, checking out is safe and easy. An 8% charge is added to your donation to coverage the cost of PayPal and PHPI administration. Donations of $500 or more should be paid by check to help us keep our overhead costs as low as possible.</p>
</ul>
<h4>FOURTH:</h4><ul>
<p>Once PHPI has received your tax-deductible donation, we will set up a MicroSeed Account in your name. The funds you wish to donate are aggregated with other funds and then forwarded to our NGO to fund your chosen loan or project.</p>
</ul>
<h4>FIFTH:</h4><ul>
<p>Over time our NGO collects the MicroSeed loan money and returns the principal amount to PHPI. PHPI keeps 8% of the principal returned to cover the administration of the program. The balance is deposited into your MicroSeed Account so that you may re-fund more loans or projects.</p>
</ul>
<h4>SIXTH:</h4><ul>
<p>As you re-fund more business sector micro, meso/macro loans out of your MicroSeed Account, the only cost you will experience is the 8% loan administrative fee.  It is PHPI's goal to keep our loan overhead costs at an extremely low level. We insure our donors that 100% of the money deposited to your MicroSeed Account goes to alleviating poverty.  Your unconditional love will grow larger and larger, turning mountains of need into mountains of joy. </p>
</ul>
</li>


			</div>