<div class="accordion" style="padding:10px">  
<h3>Keeping Track of Your Donations Tracking Repayments to NGOs</h3>
            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1">  
How can I view my MicroSeed Account?
				</a>  
     		</div>  
              <div id="collapse1" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">

    <p>
You can log into your PHPI MicroSeed Account at any time to view the loans our NGOs have made or projects to which you have donated. If you have not already done so, simply login your account and click "My Account" and you will be taken directly to your MicroSeed Account.    </a> </p>

</ul>

			</div>  
              </div>
               </div>
               
                           <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1a">  
When a business makes a partial loan repayment, can I re-donate my funds immediately?
				</a>  
     		</div>  
              <div id="collapse1a" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">

    <p>
No, your MicroSeed Account will not be credited with your proportionate share of the loan principal until the total amount possible has been collected by our NGO and they have deposited the principal amount back to PHPI. At that time, you may re-donate the portion of the principal due to you to a new entrepreneur, meso/macro venture or project. 
    </p><p>
Currently PHP receives quarterly repayments from our NGOs. The funds from all partial repayment donations are then deposited into PHPI's bank account where they earn an average interest rate. All funds generated from this interest are used to help cover PHPI's operational expenses.   </a> </p>

</ul>

			</div>  
              </div>
               </div>

            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1b">  
What if a loan is only partially repaid?
				</a>  
     		</div>  
              <div id="collapse1b" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">

    <p>
If a partially repaid loan is declared in default (it is determined the entrepreneur cannot repay the rest of the loan), the principal amount of repayments collected by the NGO will be donated back to PHPI by the NGO and distributed, in proportion, to each donor to that loan less our 8% administrative fee.

    </a> </p>

</ul>

			</div>  
              </div>
               </div>

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1c">  
When is a loan considered to be in default?
				</a>  
     		</div>  
              <div id="collapse1c" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">

    <p>
In microfinance, it is common for our NGO's to try to reschedule donations in delinquency and default in order to accommodate the eventual repayment of the loan. At PHPI, we define default (non-repayment) as 6 months after the official due date of the loan. At that point, we "write-off" the defaulted amount on the loan, and it is moved from delinquency to default on the balance sheet.
    </a> </p>

</ul>

			</div>  
              </div>
               </div>
               
                           <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1d">  
Can I forgive a loan?
				</a>  
     		</div>  
              <div id="collapse1d" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">

    <p>
You cannot request that an entrepreneur not be required to repay your donated portion
of the NGO loan he or she received. If an entrepreneur defaults on his or her loan, the 
outstanding payments must be forgiven by our NGO. PHPI does not facilitate taking action against an entrepreneur whose loan has defaulted. All risk resides with the donor.
    </a> </p>

</ul>

			</div>  
              </div>
               </div>




</div>