<div style="padding:10px;">

				<div align="center" ><a href="/pages/view/hhpo_info">Overview</a> |  <a href="/pages/view/hhpo_info">The Micro Seed Model</a>  |  <strong>Become a NGO Partner</strong></div>
			<br />
			<h3><p><span style="text-decoration: underline;">The process to become a PHP International Non-Government Microfinance Partner Organization.</span></h3>		</p>	
			<p>NGOs seeking to partner with PHP International are subject to PHP International's due diligence and approval process. </p>
<p><ol>
    <li>
    <strong>Getting started</strong>
    <ul>
        <li>Review the NGO Selection Requirements 
        </li>
        <li>Make sure you understand the MicroSeed Model 
        </li>
        <li>Ensure you are able to fulfill PHP International's operational requirements and fit the minimum partnership criteria.
        </li>
        <li>If your organization does not fit the criteria or operational requirements, please check back with us again in the future when you do. 
        </li>
        <li>If your organization fits the operational requirements and minimum partnership criteria, proceed to step 2. 
        </li>
    </ul>
    </li><br />
    <li>
    <strong>Submit your Application to PHP International's partnership team</strong>
    <ul>
        <li>Please submit  all requested information with your most recent organizational data. 
        </li>
        <li>Once you submit your profile, you will receive an automatic email confirming receipt. 
        </li>
    </ul>
    </li><br / >
    <li>
    <strong>PHP International review of NGO's Profile, determination of next steps</strong>
    				
    <ul>
        <li>Please allow up to six (6) weeks after your profile is submitted for PHP International to contact you. 
        <li>If your organization is not a good fit, we will contact you with information on how to proceed. 
        <li>To speed up the partnership review process, please make sure you have included all necessary documentation prior to sending the application to PHP International for review. 
        <li>If your organization is a good fit with PHP International's criteria and priorities, we will provide you with a copy of our NGO Partnership Agreement. 
        <li>If your NGO is approved as a partner, PHP International will work with you to finalize all partnership documentation and schedule a training session. 
        <li>You can expect up to six (6) weeks to finalize documentation.  
        <li>If your organization is not approved for partnership, PHPI will contact you with an explanation and recommended steps for approval.         
    </ul><br /> 
    <li>
    <strong>Pilot program begins</strong>
    <ul>
        <li>
        Please plan to spend up to four (4) months in the Pilot phase.  
        </li>
        <li>
        If approved for partnership, you will participate in a Pilot Program to become familiarized with the MicroSeed model, tools and resources.  
        </li>
        <li>
        You will be able to fundraise via PHP International's platform.  
        </li>
        <li>
        PHP International will provide ongoing assistance and guidance during the Pilot Program. </li>
    </ul>
    </li><br />
    <li>
    <strong>Welcome to PHP International!</strong>
    <ul>
        <li>
        After a successful Pilot phase, your organization will have full fundraising status via PHP International's platform! 
        </li>
        <li>
        Our NGO Manager will provide ongoing guidance and assistance during your relationship with PHP International. 
        </li>
    </ul>
    </li>
</ol>



			</div>