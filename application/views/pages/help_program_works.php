
<div class="accordion" style="padding:10px">  
<h3>How Our Program Works</h3>
            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1">  
What percentage of my donation goes to the entrepreneur?
				</a>  
     		</div>  
              <div id="collapse1" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">

    <p>
100% of you donation goes to the entrepreneur. PHPI adds 8% on to cover the cost of the use of PayPal and to cover part of our operational expenses.

    </a> </p>

</ul>

			</div>  
              </div>
               </div>
               
                           <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1a">  
Does PHPI charge interest?
				</a>  
     		</div>  
              <div id="collapse1a" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">

    <p>
No. Our NGOs make the loans and charge interest to the business entrepreneur in accord with local customs and law to cover its cost of administering the loans and providing basic business courses to help ensure the success of the loan.  </a> </p>

</ul>

			</div>  
              </div>
               </div>

            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1b">  
Why do PHPI's NGOs charge interest to the entrepreneurs?				</a>  
     		</div>  
              <div id="collapse1b" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <p>
Self-sustainability is critical to creating long-term solutions to poverty, and charging 
interest to entrepreneurs is necessary for microfinance NGOs to achieve this goal. Our NGOs are free to charge interest, but PHPI will not partner with an organization that charges exorbitant interest rates. We also require NGOs to fully disclose their interest rates.
    </p><p>
Microfinance is an expensive business, which is essentially the reason small loans are not provided by large banks. Unlike loans in the United States, which we collect on a monthly basis, microloans are more costly as they are collected weekly. In addition, basic business courses are included to help ensure the entrepreneur's success. While PHPI's NGOs do not bear the cost of capital or the cost of default, they do bear transaction costs and currency risk. Charging interest to entrepreneurs enables our NGOs to bear these costs and achieve self-sustainability.
     </a> </p>
</ul>

			</div>  
              </div>
               </div>

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1c">  
Why are NGO interest rates so high?
				</a>  
     		</div>  
              <div id="collapse1c" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <p>
The average interest rate that an NGO charges is about 21%. PHPI only partners with microfinance institutions that have a social mission of donating to the poor. Most of our NGOs also provide education courses as a precursor to giving loans. This has proven to improve the success of our funding. 
 
</p><p>
The nature of microcredit "small donations" is such that NGO interest rates need to be high to return the cost of the donation. To quote the Consultative Group to Assist the Poor (CGAP):
 <ul>"There are three kinds of costs the organization has to cover when it makes micro-
donations. The first two, the cost of the money that it donates and the cost of donation defaults, are proportional to the amount donated. For instance, if the cost paid by the organization for the money it donates is 10%, and it experiences defaults of 1% of the amount donated, then these two costs will total $11 for a donation of $100, and $55 for a donation of $500. An interest rate of 11% of the donation amount thus covers both these costs for either donation. The third type of cost, transaction costs, is not proportional to the amount donated. The transaction cost of the $500 donation is not much different from the transaction 
cost of the $100 donation. Both donations require roughly the same amount of staff time for meeting with the borrower to appraise the donation, processing the donation disbursement and repayments, and follow-up monitoring. Suppose that the transaction cost is $25 per donation and that the donations are for one year. To break even on the $500 donation, the organization would need to collect interest of $50 + 5 + $25 = $80, which represents an annual interest rate of 16%. To break even on the $100 donation, the organization would need to collect interest of $10 + 1 + $25 = $36, which is an interest rate of 36%. At first glance, a rate this high looks abusive to many people, especially when the clients are poor. But in fact, 
this interest rate simply reflects the basic reality that when donation sizes get very small, transaction costs loom larger because these costs can't be cut below certain minimums." (CGAP) 
</ul></p>
    </a> </p>
</ul>

			</div>  
              </div>
               </div>
               
                           <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1d">  
How does PHP fund its operations?
				</a>  
     		</div>  
              <div id="collapse1d" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <p>
PHPI currently sends 100% of your donations to our NGOs. However, self-sustainability is critical to us. To this end, we support ourselves principally on the 8% PayPal operational fee/surcharge plus voluntarily donations our donors make to the organization in addtion they make to fund entrepreneurs and/or projects. We are currently self-sufficient.
</p><p>
Lastly, we have raised growth capital from a small group including angel donors, corporate sponsors and foundations. We are incredibly thankful for this support.</a> </p>
</ul>

			</div>  
              </div>
               </div>

            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1e">  
Is there a "middle man"?
				</a>  
     		</div>  
              <div id="collapse1e" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <p>
Yes, in fact, there are two. The first is PHPI. When you donate through PHPI's website, the funds are credited to a special MicroSeed Account set up for you. PHPI then donates the funds to the second middle man, our NGO, who will manage the donation including distributing funds and collecting payments. When the NGO receives the funds, they are then distributed to the entrepreneur.</p><p>
Payments are made from the entrepreneur to the NGO on a schedule determined by the NGO. As each payment is collected, the NGO enters the amount into our software over the Internet, notifying PHPI and the donors (you) that a payment has been made. When the donation has been repaid in full, PHPI credits your MicroSeed Account for the principal amount our NGO has deposited back to PHPI minus an 8% administrative fee.   </a> </p>
</ul>

			</div>  
              </div>
               </div>

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1f">  
How does the money get to the entrepreneur?
				</a>  
     		</div>  
              <div id="collapse1f" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <p>
Donations received by PHPI are forwarded, by check or international wire, to the respective NGO on a monthly basis for all loans that are fully funded. The NGO then distributes the funds to the entrepreneur.

    </a> </p>
</ul>

			</div>  
              </div>
               </div>
               
                           <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1g">  
How can I be sure of the integrity of the entrepreneur?
				</a>  
     		</div>  
              <div id="collapse1g" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <p>
PHP does not send donations directly to the entrepreneurs. Each donation is managed by a microfinance institution, our NGO, which we work with to administer the donations. Before an entrepreneur appears on our website, they have first been screened by our NGO for loan application approval. Each of our NGOs use their own application procedures, which PHPI has reviewed and approved in combination with our software. This process ensures that your donations are actually going to genuine entrepreneurs who will use the donation for the purpose they specified. For more information, please visit our Risk and Due Diligence Section.

    </a> </p>
</ul>

			</div>  
              </div>
               </div>

            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1h">  
How are the entrepreneurs chosen?
				</a>  
     		</div>  
              <div id="collapse1h" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <p>
Our NGOs choose the entrepreneurs. Their approved loan applications are then posted on our website, together with a picture and profile of what the loan is to be used for. Each NGO uses its own application vetting process, reviewed by PHPI to confirm that the organization has the ability to handle PHPI's funds responsibly. Only then does an NGO begin to use the PHPI website and computer programs.
    </a> </p>
</ul>

			</div>  
              </div>
               </div>

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1i">  
Do I get interest on my donation?
				</a>  
     		</div>  
              <div id="collapse1i" class="accordion-body collapse" style="height: 0px; ">  

                <div class="accordion-inner">  
<ul type="disc">
    <p>
The interest our donors receive is that of unconditional love with no expectations of anything in return.

    </a> </p>
</ul>

			</div>  
              </div>
               </div>
               
<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1j">  
Is my donation tax-deductible?
				</a>  
     		</div>  
              <div id="collapse1j" class="accordion-body collapse" style="height: 0px; ">  

                <div class="accordion-inner">  
<ul type="disc">
    <p>
Yes, all money donated is actually a gift to help the poor and in turn help others. After the NGO has collected the total amount possible on each loan that you participate in, the principal portion is given back to PHPI. Your MicroSeed Account is then credited (minus PHPI's 8% administrative fee) with your proportionate amount of the principal so that you may use it to fund another loan and so the process repeats itself over and over so long as the loans are paid in full.
    </a> </p>
</ul>

			</div>  
              </div>
               </div>

<div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1k">  
Will my MicroSeed Account be replenished?				</a>  
     		</div>  
              <div id="collapse1k" class="accordion-body collapse" style="height: 0px; ">  

                <div class="accordion-inner">  
<ul type="disc">
    <p>
Your donation is not guaranteed, therefore there is a chance that the entrepreneurs may not totally repay their loan so that your MicroSeed Account will not be fully replenished. As in stocks or mutual funds, there are also different levels of risk in funding a microloan. Donating at PHPI involves risk and PHPI does not guarantee donors that their donations will be repaid.</p><p>
To date, PHPI's repayment rate is 92%. However, past repayment rates may not reflect future repayment rates or the likelihood of repayment by any particular entrepreneur. By donating to  multiple entrepreneurs or groups, rather than simply donating to one entrepreneur, you may be able to increase the likelihood of repayment. We call this "donation diversification".
    </a> </p>
</ul>

			</div>  
              </div>
               </div>                              
 
</div>

