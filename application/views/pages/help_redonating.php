<div class="accordion" style="padding:10px">  
<h3>Help Re-donation Information </h3>
            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1">  
How can I re-donate funds from a repaid loan?
				</a>  
     		</div>  
              <div id="collapse1" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  

    <p>
Log into your MicroSeed Account through the "My Account" button on our home page and see what your balance is. Then, determine how much of it you wish to re-donate to a business sector or project and follow the normal donation process. PHPI will debit your account for the amount.
   </a> </p>


			</div>  
              </div>
               </div>
               
                           <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1a">  
Can I donate funds from a repaid loan to PHPI?
				</a>  
     		</div>  
              <div id="collapse1a" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">

    <p>
At this time it is not possible to do so. We ask that you simply send us a check with the 
words "For PHPI Administrative Expenses" written on the back.
    </a> </p>

</ul>

			</div>  
              </div>
               </div>

            <div class="accordion-group">  
              <div class="accordion-heading">  
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1b">  
Can I withdraw funds from my MicroSeed Account?				</a>  
     		</div>  
              <div id="collapse1b" class="accordion-body collapse" style="height: 0px; ">  
                <div class="accordion-inner">  
<ul type="disc">
    <p>
You cannot withdraw the funds. Like any other charitable donation, once you have made your donation to PHPI, it is no longer available to you. However, you continue to participate in how your donation is used through your MicroSeed Account.
    </a> </p>

</ul>

			</div>  
              </div>
               </div>


			</div>