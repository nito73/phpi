
    <div class="span4">

      <form class="form-signin" method="post" action="/login">
        <h2 class="form-signin-heading">Please sign in</h2>
        <input name="email" type="text" class="input-block-level" placeholder="Email address">
        <input name="password" type="password" class="input-block-level" placeholder="Password">
        <label class="checkbox">
          <input type="checkbox" value="remember-me"> Remember me
        </label>
        <button class="btn btn-large btn-primary" type="submit">Sign in</button>
      </form>

    </div> <!-- /container -->
    <div class="span4">

      <form class="form-signin" method="post" action="/register">
        <h2 class="form-signin-heading">Register Now</h2>
       
        <input name="email" type="text" class="input-block-level" placeholder="Email address">
        <input name="firstname" type="text" class="input-block-level" placeholder="First Name">
        <input name="lastname" type="text" class="input-block-level" placeholder="Last Name">
        <input name="password" type="password" class="input-block-level" placeholder="Password">
        <input type="password" class="input-block-level" placeholder="Verify Password">
        <label> User Type 
        <select name="usertype">
        	<option value="">--- Select One ---</option>
        	<option value="indivisual">Individual</option>
        	<option value="NGO">NGO</option>
		  
		</select>
		</label>

      
               <button class="btn btn-large btn-primary" type="submit">Register</button>
      </form>

    </div> <!-- /container -->
