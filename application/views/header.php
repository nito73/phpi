<!DOCTYPE html>
<html>
  <head>
    <title>People Helping People</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
       
    <link href="<?=base_url()?>css/bootstrap.css" rel="stylesheet" media="screen">

    <link href="<?=base_url()?>css/bootstrap-responsive.css" rel="stylesheet" media="screen">
	    <link href="<?=base_url()?>css/bootstrap-formhelpers.css" rel="stylesheet">
    <link href="<?=base_url()?>css/bootstrap-formhelpers-countries.flags.css" rel="stylesheet">
    <link href="<?=base_url()?>css/bootstrap-formhelpers-currencies.flags.css" rel="stylesheet">

    <link href="<?=base_url()?>css/style.css" rel="stylesheet" media="screen">
    <script src="<?=base_url()?>js/jquery-2.0.3.min.js"></script>
    
    		<script src="<?=base_url()?>js/jquery.hotkeys.js"></script>
    <script src="<?=base_url()?>js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>js/prettify.js"></script>
      <script src="<?=base_url()?>js/bootstrap-wysiwyg.js"></script>
     
      <link href="<?=base_url()?>css/wysiwyg-index.css" rel="stylesheet">
      
       
       <script src="<?=base_url()?>js/jquery.dataTables.min.js"></script>
       <link href="<?=base_url()?>css/jquery.dataTables.css" rel="stylesheet">
       <script src="<?=base_url()?>js/accounting.min.js"></script>
       <link href="<?=base_url()?>css/datepicker.css" rel="stylesheet">
       <script src="<?=base_url()?>js/bootstrap-datepicker.js"></script>
       
       <script src="<?=base_url()?>js/select2.min.js"></script>
       <link href="<?=base_url()?>css/select2.css" rel="stylesheet">
       

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-37021208-2', 'phpintl.org');
  ga('send', 'pageview');

</script>

       <meta name="keywords" content="people helping people, international,  microfinance, microcredit, microloan,small loan, africa, business, philanthropy, international development, projects, corporate giving, corporate philanthropy, donors, donating, donation, charity, social development, economic, ecomony, development, grassroots, poverty alleviation, investments, entrepreneur, social, entrepreneurship, innovation, giving, give, gift, gifts, money, lending, helping, php, phpi, poor, poverty, ngo, non government organization, microseed, not for profit, nonprofit, community " />
  <meta name="description" content="Help make a difference in the world by help funding a microloan or donating to a project. We have a variety of microloans and macroloans that can benefit many people in the third world country. Join People Helping People International in ending poverty. Connect with our entrepreneurs across the globe." />

    
    <script>
    
  $(function(){
  var dateToday = new Date();
  dateToday.setDate(dateToday.getDate()-1);
    $('#editor').wysiwyg();
    $('.datepicker').datepicker({startDate: dateToday, format:'dd-mm-yyyy'});
    $('.currency').select2({allowClear: true, width:560, placeholder: "Currency used for Loan"});
    
    
    
/* Default class modification */
            $.extend( $.fn.dataTableExt.oStdClasses, {
                "sSortAsc": "header headerSortDown",
                "sSortDesc": "header headerSortUp",
                "sSortable": "header"
            } );

/* API method to get paging information */
            $.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
            {
                return {
                    "iStart":         oSettings._iDisplayStart,
                    "iEnd":           oSettings.fnDisplayEnd(),
                    "iLength":        oSettings._iDisplayLength,
                    "iTotal":         oSettings.fnRecordsTotal(),
                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                    "iPage":          Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
                    "iTotalPages":    Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
                };
            }


    /* Bootstrap style pagination control */
                $.extend( $.fn.dataTableExt.oPagination, {
                    "bootstrap": {
                        "fnInit": function( oSettings, nPaging, fnDraw ) {
                            var oLang = oSettings.oLanguage.oPaginate;
                            var fnClickHandler = function ( e ) {
                                e.preventDefault();
                                if ( oSettings.oApi._fnPageChange(oSettings, e.data.action) ) {
                                    fnDraw( oSettings );
                                }
                            };

                            $(nPaging).addClass('pagination').append(
                                '<ul>'+
                                    '<li class="prev disabled"><a href="#">&larr; '+oLang.sPrevious+'</a></li>'+
                                    '<li class="next disabled"><a href="#">'+oLang.sNext+' &rarr; </a></li>'+
                                '</ul>'
                            );
                            var els = $('a', nPaging);
                            $(els[0]).bind( 'click.DT', { action: "previous" }, fnClickHandler );
                            $(els[1]).bind( 'click.DT', { action: "next" }, fnClickHandler );
                        },

                        "fnUpdate": function ( oSettings, fnDraw ) {
                            var iListLength = 5;
                            var oPaging = oSettings.oInstance.fnPagingInfo();
                            var an = oSettings.aanFeatures.p;
                            var i, j, sClass, iStart, iEnd, iHalf=Math.floor(iListLength/2);

                            if ( oPaging.iTotalPages < iListLength) {
                                iStart = 1;
                                iEnd = oPaging.iTotalPages;
                            }
                            else if ( oPaging.iPage <= iHalf ) {
                                iStart = 1;
                                iEnd = iListLength;
                            } else if ( oPaging.iPage >= (oPaging.iTotalPages-iHalf) ) {
                                iStart = oPaging.iTotalPages - iListLength + 1;
                                iEnd = oPaging.iTotalPages;
                            } else {
                                iStart = oPaging.iPage - iHalf + 1;
                                iEnd = iStart + iListLength - 1;
                            }

                            for ( i=0, iLen=an.length ; i<iLen ; i++ ) {
                                // Remove the middle elements
                                $('li:gt(0)', an[i]).filter(':not(:last)').remove();

                                // Add the new list items and their event handlers
                                for ( j=iStart ; j<=iEnd ; j++ ) {
                                    sClass = (j==oPaging.iPage+1) ? 'class="active"' : '';
                                    $('<li '+sClass+'><a href="#">'+j+'</a></li>')
                                        .insertBefore( $('li:last', an[i])[0] )
                                        .bind('click', function (e) {
                                            e.preventDefault();
                                            oSettings._iDisplayStart = (parseInt($('a', this).text(),10)-1) * oPaging.iLength;
                                            fnDraw( oSettings );
                                        } );
                                }

                                // Add / remove disabled classes from the static elements
                                if ( oPaging.iPage === 0 ) {
                                    $('li:first', an[i]).addClass('disabled');
                                } else {
                                    $('li:first', an[i]).removeClass('disabled');
                                }

                                if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
                                    $('li:last', an[i]).addClass('disabled');
                                } else {
                                    $('li:last', an[i]).removeClass('disabled');
                                }
                            }
                        }
                    }
                } );



    });
function updateCart() {
	var carttotal = $('#carttotal');
	
	
	
	if(carttotal.text() == "Empty") {
		carttotal.text("1");
		
	} else {
		var item = parseInt(carttotal.text());
		item++;
		
		carttotal.text(item);
		
	}

 }

function removeCart() {
	var carttotal = $('#carttotal');
	
	
	
	if(carttotal.text() == "1") {
		carttotal.text("Empty");
		
	} else {
		var item = parseInt(carttotal.text());
		item--;
		
		carttotal.text(item);
		
	}

 }


</script>
  </head>
  <body >      
      <div class="header">
      	<div class="container">
	      	<div class="row">
	  			<div class="span12" style="float:right">
	      			<div class="login-top">
		      			<?php if($this->tank_auth->is_logged_in()):?> 
		      				Welcome <a href="/dashboard/"><?=$this->tank_auth->get_name();?>  </a>| <a href="/auth/logout">logout</a> | <a href="/settings/"> Account Settings </a> |
		      		<?php if($this->tank_auth->get_usertype() == "NGO" || $this->tank_auth->get_usertype() == "sNGO"|| $this->tank_auth->get_usertype() == "loan_officer") :?>
		                <a href="<?=base_url()?>clients">My Dashboard</a> | 
		                <?php elseif ($this->tank_auth->get_usertype() == "Admin"):?>
		                <a href="<?=base_url()?>admin">Admin</a> |
		                <?php else:?>
		               <a href="<?=base_url()?>dashboard">My Donations</a> |
		                <?php endif;?>
		      				
		      				
		      				 <a href="/give/cart"> View Seeds (<span id="carttotal"><?php if($this->cart->total_items() != 0) echo $this->cart->total_items(); else echo "Empty"?></span>)</a> 
		      				<?php else:?> <a href="/auth/login">Login</a> | <a href="/auth/register">Register</a> | <a href="/give/cart"> View Seeds (<span id="carttotal"><?php if($this->cart->total_items() != 0) echo $this->cart->total_items(); else echo "Empty"?></span>)</a> 
		      			<?php endif;?>
		      			
	      			</div>		      			
	  			</div>
	  		</div>

      	<div class="row">
	      	<div class="span4">
	        	<a id="logo" href="<?=base_url()?>"><img src="<?=base_url()?>images/logo.png" alt="php logo" /></a>
	        </div>
	        <div class="span8" style="float:right">
      			<div class="navbar">
		          <div class="navbar-inner">
		            <div class="container">
		              <ul class="nav">
		                <li <?php if($page == "Give") { echo 'class="active"';}?> ><a href="<?=base_url()?>give">Give</a></li>
		                <li <?php if($page == "whats_php") { echo 'class="active"';}?> ><a href="<?=base_url()?>pages/view/whats_php">What PHPI Does</a></li>
		                <li <?php if($page == "how_php_works") { echo 'class="active"';}?> ><a href="<?=base_url()?>pages/view/how_php_works">How PHPI works</a></li>
		                <li <?php if($page == "Funded") { echo 'class="active"';}?> ><a href="<?=base_url()?>give/funded">Fully Funded</a></li>
		                <li <?php if($page == "get_involved") { echo 'class="active"';}?>><a href="<?=base_url()?>pages/view/get_involved">Get Involved</a></li>
		                <li <?php if($page == "about_us") { echo 'class="active"';}?> ><a href="<?=base_url()?>pages/view/about_us">About</a></li>
		              </ul>
		            </div>
		          </div>
		        </div><!-- /.navbar -->
  			</div>
      	</div><!-- /.row -->
      </div><!-- /.container -->   
    </div>
  <div class="banner">
  	<div class="container">
  		<img src="<?=base_url()?>images/bannerforhomepage.jpg" alt="php logo" />
  	</div>
  </div>
	<div class="divider">
		<div class="container" style="background-color:#252323;">
		
		</div>
	</div>
 <div class="content">
     <div class="container">
     	<div>