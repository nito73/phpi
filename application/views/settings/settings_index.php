
	<div style="padding:10px;">
<h2>Account Settings</h2>
<?php if($msg = $this->session->flashdata('message')):?>
			
					
					<div class="alert alert-<?=$msg['type']?>">
						  <button type="button" class="close" data-dismiss="alert">&times;</button>
		               <?=$msg['body']?>
                	</div>
                <?php endif;?>
                
<form id="form" class="form-horizontal" method="post" action="<?=base_url("/settings/update/");?>" />
				
		<div class="control-group">
		    <label class="control-label" for="firstname">First Name</label>
		    <div class="controls">
	<input type="text" value="<?=$user->firstname;?>" class="input-xxlarge" id="name" name="firstname" placeholder="First Name">
		    </div>
		  </div>
	
		<div class="control-group">
		    <label class="control-label" for="lastname">Last Name</label>
		    <div class="controls">
				<input type="text" value="<?=$user->lastname;?>" class="input-xxlarge" id="name" name="lastname" placeholder="Last Name">
			    </div>
		  </div>
	

		<div class="control-group">
		    <label class="control-label" for="email">Email</label>
		    <div class="controls">
				<input type="text" value="<?=$user->email;?>" class="input-xxlarge" id="email" name="email" placeholder="Email">
			    </div>
		  </div>
		  
		  <div class="control-group">
		    <label class="control-label" for="phone">Phone</label>
		    <div class="controls">
				<input type="text" value="<?=$user->phone;?>" class="input-xxlarge" id="phone" name="phone" placeholder="Phone">
			    </div>
		  </div>
	



<h2>New Password</h2>
	<?php if($this->session->flashdata('password')):?>
			
					
					<div class="alert <?=$this->session->flashdata('password')?>">
						  <button type="button" class="close" data-dismiss="alert">&times;</button>
		               <?=$this->session->flashdata('password-msg')?>
                	</div>
                <?php endif;?>
                


		<div class="control-group">
		    <label class="control-label" for="oldpassword">Current Password</label>
		    <div class="controls">
				<input type="password" id="password" name="oldpassword" class="input-xxlarge">
			    </div>
		  </div>
		  
		  <div class="control-group">
		    <label class="control-label" for="password">New Password</label>
		    <div class="controls">
				<input type="password" id="password" name="password" class="input-xxlarge">
			    </div>
		  </div>

		  <div class="control-group">
		    <label class="control-label" for="confirmPassword">Confirm New Password</label>
		    <div class="controls">
				<input type="password" id="confirmPassword" name="confirmPassword" class="input-xxlarge" > <br />

			    </div>
		  </div>
	
<input type="submit" value="Update" class="btn btn-success pull-right">						
	<div class="clearfix"></div>
	






					
                </form>
<?php if(isset($organization) && $this->tank_auth->get_usertype() == "NGO"):?>
<hr />
<h2>NGO Settings</h2>
<form id="form" class="form-horizontal" method="post" action="<?=base_url("/settings/updateNGO/");?>" enctype="multipart/form-data" >
				
		<div class="control-group">
		    <label class="control-label" for="orgname">Organization Name</label>
		    <div class="controls">
	<input type="text" value="<?=$organization->organization;?>" class="input-xxlarge" id="orgname" name="orgname" placeholder="Organization Name">
		    </div>
		  </div>
	
		<div class="control-group">
		    <label class="control-label" for="Logo">Logo</label>
		    <div class="controls">
		    	<?php if($organization->logo):?>
			    	<img src="<?=$organization->logo;?>" style="width:100px"/>
		    	<?php endif;?>
		    		<input type="file" name="userfile" size="20" />
			    </div>
		  </div>
		  
		  <input name="orgID" value="<?=$organization->orgid?>" type="hidden" />
<input type="submit" value="Update NGO Settings" class="btn btn-success pull-right">						

</form>
<div class="clearfix"></div>
<?php endif;?>

	</div>
 