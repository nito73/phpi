	<div class="row" style="margin-bottom:20px;">
		<div class="span6" style="margin-left:25px;">
      <!-- Jumbotron -->
      <div>
        <h1>Who we are</h1>
        <p><img src="<?=base_url()?>images/logo_small.jpg" alt="PHP Logo" class="pull-left">People Helping People International (PHPI) is a non-profit organization that provides aid directly to people around the world who make two dollars or less per day through microseed loans. For them, help is not a welfare program but a loan which, when paid back, provides dignity and empowerment and can enable the poor to double their two-dollar-a-day income.<br /> </p><p>
        When you give through PHPI?s microseed loan program, you know your donation reaches the person or community you want to help. We work with non-governmental organizations (NGOs) to identify people in need. We post a picture of the person or group and a description of how the loan will be used on our website. You choose which project you want to support. Your donation, which is tax deductible, is directed to that person or group. When it is repaid, you redirect the principal to another person or group.</p>
<p><br />When you become a supporter of PHPI, your helping hands join together with many others to bring hope into the lives of the poor. It is through your faith, if only the size of a mustard seed, that you will help us move mountains of need to become mountains of joy. People Helping People International encourages you to explore our site and determine how you, too, can sow microseed loans of unconditional love.<br />
   </p>
        <a class="pull-right btn btn-warning" href="/pages/view/who_we_are/">Learn More</a>
      </div>
         <div style="margin-top:100px;">
        <h1>How PHPIntl. works</h1>
        <img src="<?=base_url()?>images/how_php_works.jpg" alt="How PHP works">
        <a class="pull-right btn btn-warning" href="/pages/view/how_php_works/">Learn More</a>
      </div>
   </div>
   <div class="span6" style="margin-left:5px;">
      <!-- Jumbotron -->
      <div>
       <div style="margin-bottom: 50px;">
        <h1>Our Global Impact</h1>
        <a class="pull-right btn-large btn-success" href="/give/">Plant A Seed</a>
       </div>
               <h3>What we do</h3>
        <div class="row">
 
        	
		<div class="span2">
			<div style="background-color:black; padding:10px;  height:150px;">
			<h4 style="color:white; text-align:center;">Micro Seed Loans</h4>
			<img src="<?=base_url()?>images/micro_seed.jpg" alt="How PHP works">
			</div>
			<a class="pull-right btn btn-warning" href="/pages/view/micro_loans/">Learn More</a>
		</div>
		<div class="span2">
			<div style="background-color:black; padding:10px;  height:150px;">
			<h4 style="color:white; text-align:center;">Macro Seed Loans</h4>
			<img src="<?=base_url()?>images/macro.jpg" alt="How PHP works">
			</div>
			<a class="pull-right btn btn-warning" href="/pages/view/macro_loans/">Learn More</a>
		</div>
		<div class="span2">
			<div style="background-color:black; padding:10px;  height:150px;">
		<h4 style="color:white; text-align:center;"><br />Projects</h4>
			<img src="<?=base_url()?>images/projects.jpg" alt="How PHP works">
			</div>
			<a class="pull-right btn btn-warning" href="/pages/view/projects/">Learn More</a>
		</div>
        </div>
      </div>
         <div>
             <h3>PHPIntl. Sponsors the following Organizations</h3>

             	<div class="row">
            
        		<div class="span2">
        		<img src="<?=base_url()?>images/kpiLogo.jpg" alt="How PHP works">
        		<img src="<?=base_url()?>images/kids_play.jpg" alt="Kids Play" class="border">
	        
        		</div>
        		<div class="span4">
        		<p>KPI uses sports and the Olympic values of Excellence, Friendship and Fair Play to promote gender equity in communities impacted by genocide. </p>
        		<a class="pull-right btn btn-success" href="http://www.kidsplayintl.org/" target="_blank">Read More</a>
        		</div>
        		
        		<div class="span2">
        		
        		<!-- <img src="<?=base_url()?>images/gglogo.jpg" alt="Golden Games Logo">
        		<img src="<?=base_url()?>images/GoldenGames.jpg" alt="Kids Play" class="border">
	        
        		</div>
        		<div class="span4">
        		<p>The Golden Games Program provides a healthy living activity program for seniors living in long term/assisted care facilities and retirement communities.   </p> 
        		
        	        		<a class="pull-right btn btn-success" href="http://www.goldenolympiangames.com/" target="_blank">More Info</a> -->
        		</div>
        		
        		
        	</div>
        </div>
   </div>
	</div>
      
      
<style>
	.border{
		border: white solid 5px;
		box-shadow: 3px 3px 5px 1px #9d9d9d;
		
		
	}
</style>

