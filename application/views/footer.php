</div> <!-- span12 -->
	 </div>
	 </div> <!-- /content -->
	 <!-- /container -->
      <div id="footer">
	      
	      <div class="container">
	      	<div class="map">
		      	<div class="row">
		      		<div class="span6" style="margin-left:25px">
		      			<strong style="color:#F6B417;">Our Mission</strong>
		      			<p style="color:white;">
			      			People Helping People was founded on the belief that every person is born of Love with a mission to grow and give their love unconditionally to others. Our mission is to provide the opportunity for our donors to fulfill their mission of giving a helping hand to people in need without changing their culture. In this way together we can with but the faith in a mustard seed move mountains of need to mountains of joy. 
		      			</p>
		      		</div>
		      		<div class="span6" style="margin-left:5px">
		      			<strong style="color:#F6B417;">Get Connected &amp; Involved</strong>
		      			<p style="color:white;">
			      			Get connected and make a differences. <br />
			      			
		      			</p>
		      			<div class="social" style="margin-top:25px;">
			      			<a href="https://www.facebook.com/PeopleHelpingPeopleInternational"><img src="<?=base_url()?>images/fb.png" alt=""></a>
			      			<a href="https://twitter.com/phpintl"><img src="<?=base_url()?>images/twitter.png" alt=""></a>
			      			<a href="#"><img src="<?=base_url()?>images/youtube.png" alt=""></a>
			      			<a href="#"><img src="<?=base_url()?>images/google.png" alt=""></a>
		      			</div>
		      		</div>
		      		
		      	</div>
	      	</div>
	        <div class="footer-navbar">
              <ul class="nav" style="float:left;">
                <li><a href="/pages/view/help_info">Help Info</a></li>
                <li><a href="/pages/view/hhpo_info">NGO Info</a></li>
                <li><a href="/pages/view/team">Team</a></li>
              
              </ul>     
              <ul class="nav">
               
                <li><a href="/pages/view/risk_and_due_diligence">Risk and Due Diligence</a></li>
                <li><a href="/pages/view/terms_of_use">Terms of Use</a></li>
                <li><a href="/pages/view/private_policy">Privacy Policy</a></li>
             </ul>     
		    </div><!-- /.navbar -->
	      </div>
	      <div class="container yellow">
	      	<p class="pull-right">Designed and Developed by <a style="color:white;"href="http://www.phuconcepts.com" alt="Web Design Rochester NY"><img src="http://www.phuconcepts.com/images/sm_logo.png" style="height:16px;" alt="Phuconcepts.com Web Design Rochester NY">PhuConcepts.com</a></p>
	        <p>Copyright <?=date('Y')?> People Helping People INTERNATIONAL</p>
	      </div>
	      
    </div>
  </body>
      <script src="<?=base_url()?>js/bootstrap-formhelpers-selectbox.js"></script>

   <script src="<?=base_url()?>js/bootstrap-formhelpers-currencies.en_US.js"></script>
    <script src="<?=base_url()?>js/bootstrap-formhelpers-currencies.js"></script>

</html>