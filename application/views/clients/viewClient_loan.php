<?php 
	$ngo_currency = "";
	$clientLoan = $this->Client_model->view_client_loan($clientID);
	$clientPayment = $this->Client_model->view_client_loan_payment($clientID);
	?>
	    <div class="tab-content" style="padding:10px;">
			
		    
		    
	 <?php if($clientLoan):?>

	
		    <?php 
	            		$ngo_currency = $clientLoan->currency;
		            	$loan_terms = $clientLoan->loanterms;
		            	$loan_type = $clientLoan->loantermstype;
		            	$loandate = $clientLoan->loandate;	
		            	$loaninterest = $clientLoan->loaninterest;
		            	$justloaninterest = $clientLoan->loaninterest;
		            	$totalrequested = $clientLoan->amount;
		            	$rate= $clientLoan->rate;
		            	$projectID = $clientLoan->id;
		            	$paymentduedate = "";
		            	$total = $this->Ngo_model->get_total_funded($projectID);
	            		?>
	            
	            		
		    <div class="row" style="padding:5px;">
    <div class="span3">
    <a href="#" class="thumbnail">
      <img src="<?php if($clientLoan->pics != "") { echo $clientLoan->pics;} else { echo base_url('images') . "/no_image.jpg";}?>">
    </a>
    </div>
    
    <div class="span8">
			
          <h1><?=$clientLoan->title?></h1>
          <h4><?=$clientLoan->location?> | <?=$clientLoan->category?></h4>
         
          
                   <dl class="dl-horizontal">
		
		  <dt>Posted Date</dt>
		  <dd><?=date('d M y' , strtotime($clientLoan->postdate))?></dd>
		  <dt>Exchange Rate</dt>
		  <dd>$ 1 USD -> <?=$rate?> <?=$ngo_currency?></dd>
		  
		  <?php if($repaymenttotal = $this->Ngo_model->total_repayment_amount($projectID)):?>
		  	<dt>Loan </dt>
		  		<dd>$<?=money_format('%(#10n', $clientLoan->amount)?> USD  / <?=money_format('%(#10n', $clientLoan->ngoamount)?> <?=$ngo_currency?>  (Principal)<br />
		  		$<?=money_format('%(#10n', $clientLoan->amount*(($loaninterest/100)+1))?> USD  / <?=money_format('%(#10n', ($clientLoan->amount*$rate)*($loaninterest/100)+1)?> <?=$ngo_currency?> (Interest <?=$justloaninterest?>%) 
		  		</dd>
		  	<dt>Total Due</dt>
		  	<dd>
		  				  <?php $percenttotal = floor(($repaymenttotal['phptotal']/($clientLoan->amount*(($loaninterest/100)+1)))*100);?>

			  $<?=money_format('%(#10n', $repaymenttotal['phptotal'])?> USD / $<?=money_format('%(#10n', $clientLoan->amount*(($loaninterest/100)+1))?> USD (<?=$percenttotal?>%)<br />
			  <?=money_format('%(#10n', $repaymenttotal['ngototal'])?> <?=$ngo_currency?> / <?=money_format('%(#10n', ($clientLoan->amount*$rate)*(($loaninterest/100)+1))?> <?=$ngo_currency?> (<?=$percenttotal?>%)
		


			   <div class="progress">
       
				<div class="bar" style="width: <?=$percenttotal?>%;"></div>
				   <label><?=$percenttotal?>% Returned</label>
				</div>
            </div>
            
			  </dd>
	
	        
		   
		  <?php endif;?>
		  
		  
  		</dl>
          
         
  </div>
   
		 <strong> Loan Payment</strong><br />
		    <table class="table table-hover">
	            <thead>
	            <tr>
	            	<td>Due Date</td>
	            	<td>Paid Date</td>

	            	<td><?=$ngo_currency?> Amount Due</td>
	            	<td><?=$ngo_currency?> Amount Paid</td>
	            	
	            	<td>Staff</td>
	            	<td>Print</td>
	            </tr>
	            </thead>
	            <tbody>
	            
			  <?php if($repaymentquery = $this->Ngo_model->view_repayment($clientLoan->id)):?>

	            		<?php $counter=0;
	            			$owe=0;
	            			$principalPaid = 0;
	            		?>

	            		<?php foreach($repaymentquery as $row):?>
	 						<?php $payment = $clientPayment[$counter];?>
	            		
	            	
	            		


	            		<tr>
	            			<td><?=date('d M y', strtotime($payment['date']))?></td>
	            			<td><?=date('d M y', strtotime($row->date))?></td>

	            			<td><?=money_format('%(#10n', $payment['unformated_ngo_payment']+$owe) . " " . $ngo_currency;?> </td>
	            			<td><?=money_format('%(#10n', $row->ngoamount) . " " . $ngo_currency;?> </td>
	            				<td><?=$this->Ngo_model->get_user_name($row->userid);?> </td>
	            					<td><a href="<?=site_url('clients/printLoanTransaction/' . $row->id)?>" class="btn btn-success" target="_blank"><i class="icon-print"></i> Print</a> </td>
	            				            			
	            		</tr>


	            		<?php 

	            			//check if $owe
	            				if(($payment['unformated_ngo_payment']+$owe) > $row->ngoamount ){
	            					//pay iterest first

	            					//pay what is owe first
	            					$row->ngoamount = $row->ngoamount  - $owe;
	            					$owe = 0;

	            					if($row->ngoamount > $payment['unformated_ngo_interest']) {
	            						$row->ngoamount = $row->ngoamount - $payment['unformated_ngo_interest'];
	            						$row->ngoamount = $payment['unformated_ngo_principal'] - $row->ngoamount;
	            						$owe = $row->ngoamount + ($row->ngoamount*.1);
				            			$principalPaid += $row->amount;
	            						

	            					} else {
	            						$owe = ($payment['unformated_ngo_interest'] - $row->ngoamount) + $payment['unformated_ngo_payment'] + ($payment['unformated_ngo_payment']*.1);

	            					}
	            				} else {
	            					if(strtotime($row->date) > strtotime($payment['date'] . " + 5 days") ){
	            						$owe = $payment['unformated_ngo_principal'] * .1;
	            					} else {
	            						$owe = 0;
		            				}

	            			$principalPaid += $payment['unformated_ngo_principal'];

	            				}


	            		$counter++;?>


	            		<?php endforeach;?>
	            		
	            			
            		<?php else:?>
            		<tr>
            			<td colspan="3">No Payments Made </td>
            		</tr>
            		<?php endif;?>
            		

	            </tbody>
	        </table>
	        <?php if (isset($counter) && isset($clientPayment[$counter])) :?>
	        <?php $payment = $clientPayment[$counter];?>

	        <strong>Loan Balance</strong> <?=money_format('%(#10n', $clientLoan->ngoamount-$principalPaid)?> <?=$ngo_currency?>
<br />
<br />
	            			
	        <strong>Payment Due Date</strong> <?=date('d M y', strtotime($payment['date']))?> <br />
	        <?php if($owe > 0):?>
	        	<strong>Past Balance </strong> <?=money_format('%(#10n', $owe) . " " . $ngo_currency;?> <br />
	    	<?php endif;?>
	        <strong>Minimum Payment Due </strong> <?=money_format('%(#10n', $payment['unformated_ngo_payment']+$owe) . " " . $ngo_currency;?> <br />
	      
	    <?php endif;?>

	        
	            <div class="well">
				    <p>By entering a payment,  Please enter monetary value in <?=$ngo_currency?></p>
				    <?php 
				    	$projectStatus = $this->Ngo_model->get_project_status($projectID);
				    if($projectStatus != 'Pending Closing' && $projectStatus != 'Closed' ):?>
				    <form method="post" action="<?=site_url('ngo/repaymentProcess')?>">
					    <label>Payment Date </label><input type="text" name="date" class="datepicker" value="<?=date('d-m-Y')?>"/>
						<input type="hidden" class="input-xxlarge" name="paymenttype" id="paymenttype" value="Loan" />
						<label>Amount <?=$ngo_currency?></label><input type="text" name="amount" />
					    <input type="hidden" name="projectid" value="<?=$projectID?>" />
					    <input type="hidden" name="clientid" value="<?=$clientID?>" />
					    <input type="hidden" name="rate" value="<?=$rate?>" />
					    <input class="btn btn-success"type="submit" value="Send" />
					</form>
				    
				    <?php else:?>
				    <h2>This Loan is <?=$projectStatus?></h2>
				    <?php endif;?>
				    <a href="<?=site_url('ngo/closeProject/' . $projectID)?>" class="btn btn-warning" > Close Loan</a>
			    </div>
		    
	   
				<?php else:?>
				<?php $ngo_currency = $organization->currency;?>
				<a class="btn btn-primary btn-large" href="<?=site_url('clients/addLoan/' . $clientID)?>" > Add Loan </a>
                	<p style="padding-top:20px;"><ul>
    <li>
    <span style="font-size: 13px;"><span style="font-weight: bold;">Food Industry Sector</span></span>- Businesses that produce or sell packaged manufactured food out of a store such as Restaurants, Bakery, Ice Cream, etc.</li>
    <li>
    <span style="font-size: 13px;"><span style="font-weight: bold;">Agriculture Sector</span></span>- Businesses that grow crops, raise animals, etc. off the land.</li>
    <li>
    <span style="font-size: 13px;"><span style="font-weight: bold;">Service Industry Sector</span></span> - Businesses that provide services such as repairs, hair dressers, communication centers, etc</li>
    <li>
    <span style="font-size: 13px;"><span style="font-weight: bold;">Small Trade Industry Sector</span></span> - Any type of product sold on a one-to-one such as beans, t-shirts, yams, jewelry, etc.</li>
    <li>
    <span style="font-size: 13px;"><span style="font-weight: bold;">Manufacturing Sector</span></span>- These businesses are defined as capable of producing a product such as cement block makers, seamstress, furniture, etc.</li>
</ul>
</p> 

<br />

<a class="btn btn-primary btn-large" href="<?=site_url('clients/addNonLoan/' . $clientID)?>" > Add Non-PHP Loan </a>
                	<p style="padding-top:20px;">
                	
               
                		Do you have a loan that is funded by another organization that you want our system to track payments?
                
</p> 

			    <?php endif;?>
			
		    </div>
		    
		 
    
                
      
      <script>
	      $(function(){
		      
		      $('.removeClient').click(function(event) { 
	    var note=confirm("Are you sure that you want to remove this Client");

		if(!note){
			event.preventDefault();
		}

   
    });
	      });
</script>

<?php $this->load->view('/dashboard/footer_view');?>