	<style type="text/css">
	#editor{
		overflow:scroll;
		max-height:300px;
	}
	.client img{
		height: 200px;
	}	
</style>

<link rel="stylesheet" href="/imageupload/css/jquery.fileupload-ui.css">


<div style="padding:10px;" class="client">
        <h1>New Client Application</h1>
        <p class="lead"></p>
        <div>
        <form id="form" method="post" action="<?=base_url('clients/addProcess');?>" class="form-horizontal">
			
  	
		
		
	    
	    
	    <h3>Client Information</h3>
	     <div class="control-group">
		    <label class="control-label" for="name">Client Name</label>
		    <div class="controls">
				<input class="input-xxlarge" type="text" name="name" id="name" placeholder="Client Name">
		    </div>
		  </div>
		   <div class="control-group">
		    <label class="control-label" for="uploadimage">Client Photo</label>
		    <div class="controls">
		 <img src="" class="uploadimage"/>
	    
	    
		<span class="btn btn-success fileinput-button">
	        <i class="icon-plus icon-white"></i>
	        <span>Select files...</span>
		    <input id="fileupload" type="file" name="files[]" multiple>
	    </span>
	    
	   	    <div id="progress" style="width:75%;" class="progress progress-success progress-striped">
	        <div class="bar" ></div>
	    </div>
	    
		    </div>
		  </div>
		  
		   <div class="control-group">
		    <label class="control-label" for="gov_id">National ID</label>
		    <div class="controls">
				<input class="input-xxlarge" type="text" name="gov_id"  placeholder="National ID">
		    </div>
		  </div>
 
		   <div class="control-group">
		    <label class="control-label" for="birthday">Birthday</label>
		    <div class="controls">
			<input class="input-xxlarge datepicker" type="text" name="birthday"  placeholder="Birthday">
		    </div>
		  </div>
 
		   <div class="control-group">
		    <label class="control-label" for="birthplace">Birthplace</label>
		    <div class="controls">
		<input class="input-xxlarge" type="text" name="birthplace"  placeholder="Birthplace"><br />
		    </div>
		  </div>
 
		   <div class="control-group">
		    <label class="control-label" for="gender">Gender</label>
		    <div class="controls">
				<label class="radio inline">
		<input type="radio" name="gender" value="male">Male</label>
		<label class="radio inline">
		<input type="radio" name="gender" value="female">Female
		</label>
<br />
		    </div>
		  </div>
		  <div class="control-group">
		    <label class="control-label" for="martial_status">Martial Status</label>
		    <div class="controls">
		<input class="input-xxlarge" type="text" name="martial_status"  placeholder="Martial Status"><br />
		    </div>
		  </div>
		  
		  <div class="control-group">
		    <label class="control-label" for="job">Occupation</label>
		    <div class="controls">
		<input class="input-xxlarge" type="text" name="job"  placeholder="Occupation/Profession"><br />
		    </div>
		  </div>
 
        <hr />
		   <div class="control-group">
		    <label class="control-label" for="address">Address</label>
		    <div class="controls">
	<input class="input-xxlarge" type="text" name="address" id="address" placeholder="Address">
		<br />
			<input class="span3" type="text" name="city" id="city" placeholder="City">
		<input class="span2" type="text" name="region" id="Region" placeholder="Region">
		<input class="span2" type="text" name="postalcode" id="Postal Code" placeholder="Postal Code">

		
		    </div>
		  </div>
 
	<div class="control-group">
		    <label class="control-label" for="cell">Mobile Phone</label>
		    <div class="controls">
		<input class="input-xxlarge" type="text" name="cell" id="cell" placeholder="Mobile Phone"> 

		    </div>
		  </div>
		  <div class="control-group">
		    <label class="control-label" for="collection_location">Collection Location</label>
		    <div class="controls">
		<input class="input-xxlarge" type="text" name="collection_location" id="collection_location" placeholder="Location of Collection"> 

		    </div>
		  </div>
		  <div class="control-group">
		    <label class="control-label" for="currency">Currency</label>
		    <div class="controls">
				<div class="bfh-selectbox bfh-currencies" data-currency="<?=$organization->currency?>" data-flags="true">
				  <input type="hidden" name="currency" value="">
				  	<a class="bfh-selectbox-toggle " role="button" data-toggle="bfh-selectbox" href="#">
					  		<span class="bfh-selectbox-option input-medium" data-option=""></span>
					  		<b class="caret"></b> 
					  </a>
					  <div class="bfh-selectbox-options">
					    <input type="text" class="bfh-selectbox-filter">
					   		 <div role="listbox">
							    <ul role="option">
							    </ul>
						    </div>
					  </div>
				</div>
		    </div>
		  </div>
		  <div class="control-group">
		    <label class="control-label" for="saving">Initial Savings Deposit<label>
		    <div class="controls">
		
		<input class="input-xxlarge" type="text" name="saving" id="saving" placeholder="Inital Savings Deposit">
		    </div>
		  </div>
			
		
					
		
		

	    
		<h3>Co-Signer Information</h3>
		
		
		
		   <div class="control-group">
		    <label class="control-label" for="coname">Co-Signer Name</label>
		    <div class="controls">
		
		<input class="input-xxlarge" type="text" name="coname"  placeholder="Co-Signer Name">
		
		    </div>
		  </div>
 
 
		
		   <div class="control-group">
		    <label class="control-label" for="copic">Co-Signer Picture</label>
		    <div class="controls">
				 <img src="" class="couploadimage"/>
			    
			    
				<span class="btn btn-success fileinput-button">
			        <i class="icon-plus icon-white"></i>
			        <span>Select files...</span>
				    <input id="cofileupload" type="file" name="files[]" multiple>
			    </span>
			    
			   	    <div id="coprogress" style="width:75%;" class="progress progress-success progress-striped">
			        <div class="bar" ></div>
			    </div>		
		    </div>
		  </div>
		  
		  
		   <div class="control-group">
		    <label class="control-label" for="cogov_id">National ID</label>
		    <div class="controls">
				<input class="input-xxlarge" type="text" name="cogov_id"  placeholder="National ID">
		    </div>
		  </div>
 
		   <div class="control-group">
		    <label class="control-label" for="cobirthday">Birthday</label>
		    <div class="controls">
			<input class="input-xxlarge datepicker" type="text" name="cobirthday"  placeholder="Birthday">
		    </div>
		  </div>
 
		   <div class="control-group">
		    <label class="control-label" for="cobirthplace">Birthplace</label>
		    <div class="controls">
		<input class="input-xxlarge" type="text" name="cobirthplace"  placeholder="Birthplace"><br />
		    </div>
		  </div>
 
		   <div class="control-group">
		    <label class="control-label" for="cogender">Gender</label>
		    <div class="controls">
				<label class="radio inline">
		<input type="radio" name="cogender" value="male">Male</label>
		<label class="radio inline">
		<input type="radio" name="cogender" value="female">Female
		</label>
<br />
		    </div>
		  </div>
		  <div class="control-group">
		    <label class="control-label" for="comartial_status">Martial Status</label>
		    <div class="controls">
		<input class="input-xxlarge" type="text" name="comartial_status"  placeholder="Martial Status"><br />
		    </div>
		  </div>
		  
		  <div class="control-group">
		    <label class="control-label" for="cojob">Occupation</label>
		    <div class="controls">
		<input class="input-xxlarge" type="text" name="cojob"  placeholder="Occupation/Profession"><br />
		    </div>
		  </div>
		
		<hr />
		   <div class="control-group">
		    <label class="control-label" for="coaddress">Address</label>
		    <div class="controls">
	<input class="input-xxlarge" type="text" name="coaddress" id="address" placeholder="Address">
		<br />
			<input class="span3" type="text" name="cocity" id="city" placeholder="City">
		<input class="span2" type="text" name="coregion" id="Region" placeholder="Region">
		<input class="span2" type="text" name="copostalcode" id="Postal Code" placeholder="Postal Code">

		
		    </div>
		  </div>
 
	<div class="control-group">
		    <label class="control-label" for="cocell">Mobile Phone</label>
		    <div class="controls">
		<input class="input-xxlarge" type="text" name="cocell" id="cell" placeholder="Mobile Phone"> 

		    </div>
		  </div>
		
		
	   
		<input type="hidden" name="pic" id="pic" placeholder="URL of Picture">
		<input type="hidden" name="copic" id="copic" placeholder="URL of Picture">

		<br />
    <input type="submit" class="btn" value="Add Client" />
  </div>
 </div>
  
  </form>      
        
		
        </div>
<script src="/imageupload/js/vendor/jquery.ui.widget.js"></script>
<script src="/imageupload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="/imageupload/js/jquery.fileupload.js"></script>
<script src="/imageupload/js/jquery.fileupload-image.js"></script>


<script>
$(function(){
	   	   'use strict';
    // Change this to the location of your server-side upload handler:
   var url = (window.location.hostname === 'blueimp.github.com' ||
                window.location.hostname === 'blueimp.github.io') ?
                '//jquery-file-upload.appspot.com/' : '<?=base_url();?>/imageupload/server/php/';

    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        disableImageResize: /Android(?!.*Chrome)|Opera/
        .test(window.navigator && navigator.userAgent),
    imageMaxWidth: 250,
    imageMaxHeight: 250,
    imageCrop: true, // Force cropped images
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
              	
                $('#pic').val(file.url);
                $('.uploadimage').attr('src', file.url);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .bar').css(
                'width',
                progress + '%'
            );
        }
    });
    
       $('#cofileupload').fileupload({
        url: url,
        dataType: 'json',
        disableImageResize: /Android(?!.*Chrome)|Opera/
        .test(window.navigator && navigator.userAgent),
    imageMaxWidth: 250,
    imageMaxHeight: 250,
    imageCrop: true, // Force cropped images
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
              	
                $('#copic').val(file.url);
                $('.couploadimage').attr('src', file.url);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#coprogress .bar').css(
                'width',
                progress + '%'
            );
        }
    });
    
    
  

  });
  
  


</script>
