<?php 
	$ngo_currency = "";
	$clientLoan = $this->Client_model->view_client_loan($clientID);
	?>
<style type="text/css">
	.total td{
		font-weight: bold;
	}
</style>
<?php $query = $this->Client_model->viewClient($clientID);?>

	  <?php   foreach($query as $row):?>
  <div class="pull-right"style="padding:5px;">
  
      <div class="btn-group">
    <button class="btn">Client Action</button>
    <button class="btn dropdown-toggle" data-toggle="dropdown">
    <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
	     <li>
	     	<a href="<?=site_url('clients/editClient/' . $clientID);?>" target="_blank">Edit Client</a>
	     </li>
	      <?php if($this->tank_auth->get_usertype() != "loan_officer"):?>
	      <?php if($row->status != "inActive"):?>
	      <li>
	     	<a href="<?=site_url('clients/removeClient/' . $clientID);?>" target="_blank" class="removeClient">Remove Client</a>
	     </li>
	     
	     <?php else:?>
	      <li>
	     	<a href="<?=site_url('clients/activateClient/' . $clientID);?>" target="_blank">Activate Client</a>
	     </li>
	     
	     
	     
	     <?php endif;?>
	     
	     
	     <?php if($clientLoan):?>
		     <li>
		     	  			<a href="<?=site_url('clients/editLoan/' . $clientLoan->id);?>" > Edit Loan Info </a>
		     </li>


  <?php if ($clientLoan):?>
    <li>
	  <a href="<?=site_url('/reports/repaymentReport/' . $clientLoan->id);?>" target="_blank">Print Client Loan Report</a>
	  
  </li>

<?php endif ;?>
		    <?php endif;?>
	     <?php endif;?>
	     <li>
		     <a href="<?=site_url('clients/printClient/' . $clientID);?>" target="_blank">Print Client Summery</a>
	     </li>
    </ul>
    </div>
    
	 
  </div>
  <div class="row" style="padding:10px;padding-top:40px;">
  
  	<div class="row">
  	<div class="span5">
	    <div class="span3">
    <a href="#" class="thumbnail">
      <img src="<?php if($row->pics != "") { echo $row->pics;} else { echo base_url('images') . "/no_image.jpg";}?>">
    </a>
    </div>
    <div class="span8">
			
<table>
	<tr>
		<td style="font-weight:bold;">Collection Location</td>
		  <td><?=$row->collectionlocation?></td>
	</tr>
	<tr>
		  <td style="font-weight:bold;">Name</td>
		  <td><?=$row->name?></td>
		  </tr>
	<tr>
		  <td style="font-weight:bold;">National ID</td>
		  <td><?=$row->gov_id?></td>
		  </tr>
	<tr>
		  <td style="font-weight:bold;">Gender</td>
		  <td><?=ucwords($row->gender)?></td>
		 
		  </tr>
	<tr>
		  <td style="font-weight:bold;">Birthday (Birthplace)</td>
		  <td><?=$row->birthdate?>(<?=$row->birthplace?>)</td>
		  </tr>
	<tr>
		  <td style="font-weight:bold;">Occupation</td>
		  <td><?=$row->job?></td>
		  </tr>
	<tr>
		  <td style="font-weight:bold;">Martial Status</td>
		  <td><?=$row->martial_status?></td>
		  
		  </tr>
	<tr>
		  <td style="font-weight:bold;">Address</td>
		  <td><?=$row->address?> <br/>
		  <?=$row->city?> <?=$row->region?>, <?=$row->postalcode?></td>
	 </tr>
		  
</table>
         
  </div>
	  	
  	</div>
  	<?php if($row->coname != "" && $row->coaddress != ""):?>
  	<div class="span6">
	    <div class="span3">
    <a href="#" class="thumbnail">
      <img src="<?php if($row->copics != "") { echo $row->copics;} else { echo base_url('images') . "/no_image.jpg";}?>">
    </a>
    </div>
    <div class="span8">
			
          
          <table>

	<tr>
		  <td style="font-weight:bold;">Co-Signer Name</td>
		  <td><?=$row->coname?></td>
		  </tr>
	<tr>
		  <td style="font-weight:bold;">National ID</td>
		  <td><?=$row->cogov_id?></td>
		  </tr>
	<tr>
		  <td style="font-weight:bold;">Gender</td>
		  <td><?=ucwords($row->gender)?></td>
		 
		  </tr>
	<tr>
		  <td style="font-weight:bold;">Birthday (Birthplace)</td>
		  <td><?=$row->cobirthdate?>(<?=$row->cobirthplace?>)</td>
		  </tr>
	<tr>
		  <td style="font-weight:bold;">Occupation</td>
		  <td><?=$row->cojob?></td>
		  </tr>
	<tr>
		  <td style="font-weight:bold;">Martial Status</td>
		  <td><?=$row->comartial_status?></td>
		  
		  </tr>
	<tr>
		  <td style="font-weight:bold;">Address</td>
		  <td><?=$row->coaddress?> <br/>
		  <?=$row->cocity?> <?=$row->coregion?>, <?=$row->copostalcode?></td>
	 </tr>
		  
</table>
         
  </div>
  	</div>
  	<?php endif;?>
  	</div>
<?php endforeach;?>

<style type="text/css">
	.total td{
		font-weight: bold;
	}
</style>
<?php $query = $this->Client_model->viewClient($clientID);?>
	 
            <?php 
            if($msg = $this->session->flashdata('message')):?>
            	    <div class="alert alert-<?=$msg['type']?>" style="margin-left: 20px;">
            	    	<a class="close" data-dismiss="alert" href="#">&times;</a>
            		    <strong class="alert-heading"><?=$msg['body']?></strong>
            	    </div>
            	    
            	    <?php endif;?>
            	  
    <div class="tabbable" style="padding:25px;"> <!-- Only required for left/right tabs -->
	    <ul class="nav nav-tabs">
			<li <?php if($view_type == "savings") :?>class="active"<?php endif;?> ><a href="<?php echo site_url('clients/viewClient/' . $clientID)?>/savings">Savings</a></li>
			<li <?php if($view_type == "collateral") :?>class="active"<?php endif;?> ><a href="<?php echo site_url('clients/viewClient/' . $clientID)?>/collateral">Collateral Savings</a></li>
			
		    <li <?php if($view_type == "loan") :?>class="active"<?php endif;?> ><a href="<?php echo site_url('clients/viewClient/' . $clientID)?>/loan">Loans</a></li>
			<?php if($clientLoan):?>
		    			<li <?php if($view_type == "loan_schedule") :?>class="active"<?php endif;?> ><a href="<?php echo site_url('clients/viewClient/' . $clientID)?>/loan_schedule">Payment Schedule</a></li>
			<?php endif;?>
			
	    </ul>
           

