<?php 
	$ngo_currency = $transaction->currency;
	?>
<style type="text/css">
	
	

	body{
		font-size: 18px;
		width: 300px;
	}
	h3{
		font-size: 18px;
	}
	label{
		font-weight: bold;
	}
		img{
		width: 290px;
		margin: 0 auto;
	}
	.pull-left{
		text-align: left;
	}
	.sign{
			border-bottom: 1px black solid;
			width: 100%;
			margin-top: 10px;
			margin-bottom: 10px;
			height: 20px;
			
	}
	body{
		padding-bottom: 30px;
	}
</style>
<body>

<img src="<?=$organization->logo?>" />

<?php $query = $this->Client_model->viewClient($clientID);

    foreach($query as $row):?>

     
  

     <label>Client Name:</label> <?=$row->name?> (<?=$row->gov_id?>) <br />
     <label>Client Account ID# </label>  <?=$row->id?> <br/>
	<label>Transaction Date: </label><?=date('d M y', strtotime($transaction->date))?> <br />
	<label>Transaction Recorded: </label><br />
	 <i>Savings     <?php if($transaction->amount > 0):?>Deposit<?php else:?> Withdraw<?php endif;?>  </i> <br />
<br />
<hr />

<?php endforeach;?>

		    
		    <?php 
		    	$coltotalCurrent = $this->Client_model->client_collateral_trans_total($clientID);
		    	
		    	$savtotalCurrent = $this->Client_model->client_savings_trans_total($clientID, $transaction->id);
		    	$savtotalPrior = $savtotalCurrent - $transaction->amount;
		    ?>
		   <table >
		   	<tr >
		   		<td  style="padding-bottom:20px;" class="pull-left"> <label> Collateral Savings Account:</label> <br /> Balance:  </td>
		   		<td><br /><?=money_format('%(#10n', $coltotalCurrent ) . " " . $ngo_currency;?> <br /></td>
		   	</tr>
		   	
		   	
		   	<tr>
		   		<td> <label> Savings Account:</label> <br /> Beginning Balance:  </td>
		   		<td><br /><?=money_format('%(#10n', $savtotalPrior ) . " " . $ngo_currency;?> </td>
		   	</tr>
		   	   	
	        <tr>
		   		<td > <label>     <?php if($transaction->amount > 0):?>Deposit<?php else:?> Withdraw<?php endif;?>:</label> </td>
		   		<td><?=money_format('%(#10n', $transaction->amount ) . " " . $ngo_currency;?> </td>
		   	</tr>
		   	<tr>
		   		<td> <label> Today Balance:</label> </td>
		   		<td><?=money_format('%(#10n', $savtotalCurrent ) . " " . $ngo_currency;?> </td>
		   	</tr>
		   </table>    				            			
	   
	   
 

		    <hr />
		       <label>Teller / Casher </label><br />
		      <?=$this->Ngo_model->get_user_name($transaction->userid);?> <br/>
		      <br />
		      <label>Cashier Signature and Stamp </label><br />
		      <div class="sign"></div>
		      
		       <label>Client Signature </label><br />
		      <div class="sign"></div>
</body>
		