<?php 
	$ngo_currency = "";
	$clientLoan = $this->Client_model->view_client_loan($clientID);
	$clientPayment = $this->Client_model->view_client_loan_payment($clientID);
	?>
<style type="text/css">
	

	body{
		font-size: 18px;
		width: 300px;
	}
	h3{
		font-size: 18px;
	}
	label{
		font-weight: bold;
	}
		img{
		width: 290px;
		margin: 0 auto;
	}
	tr td{
		text-align: right;
	}
	.sign{
			border-bottom: 1px black solid;
			width: 100%;
			margin-top: 10px;
			margin-bottom: 10px;
			height: 20px;
			
	}
	body{
		padding-bottom: 30px;
	}
</style>
<body>


   <h3>Loan Transaction Receipt</h3><br />
  <img src="<?=$organization->logo?>" />


<?php $query = $this->Client_model->viewClient($clientID);

    foreach($query as $row):?>

     
  

     <label>Client Name:</label> <?=$row->name?> (<?=$row->gov_id?>)  <br />
     <label>Client Account ID# </label>  <?=$row->id?> <br/>
	<label>Transaction Date: </label><?=date('d M y', strtotime($transaction->date))?> <br />
	<label>Transaction Recorded: </label><br />
	 <i>Loan Repayment</i> <br />
<br />
<hr />

<?php endforeach;?>
		    
		    <?php if($clientLoan):?>
		    <?php 
	            		$ngo_currency = $clientLoan->currency;
		            	$loan_terms = $clientLoan->loanterms;
		            	$loan_type = $clientLoan->loantermstype;
		            	$loandate = $clientLoan->loandate;	
		            	$loaninterest = $clientLoan->loaninterest;
		            	$totalrequested = $clientLoan->amount;
		            	$rate= $clientLoan->rate;
		            	$projectID = $clientLoan->id;
		            	
		            	$total = $this->Ngo_model->get_total_funded($projectID);
	            		?>
	            		
	         
	            		
	
	
		  
  		
   
	     
			  <br />
		    	  	
			 <div style="font-size:16px !important">
			 
			 
			  <?php if($repaymentquery = $this->Ngo_model->view_repayment($clientLoan->id, $transaction->id)):?>
			  
	            	
	            		<?php 
	            			$counter=0;
	            			$owe=0;
	            			$pastOwe = 0;
	            			$principalPaid = 0;
	            			$pastPaid =0;
	            			$remaingDue =0;
	            		foreach($repaymentquery as $row):?>
	            		
	            	 		
	            		 <?php $payment = $clientPayment[$counter];?>
	            		
	            	
	            		



	            		<?php 
	            					$pastOwe = $owe;

	            			//check if $owe
	            				if(($payment['unformated_ngo_payment']+$owe) > $row->ngoamount ){
	            					//pay iterest first

	            					//pay what is owe first
	            					$row->ngoamount = $row->ngoamount  - $owe;
	            					$owe = 0;
			            			$remaingDue =0;
			            			$paidToday = 0;


	            					if($row->ngoamount > $payment['unformated_ngo_interest']) {
	            						$row->ngoamount = $row->ngoamount - $payment['unformated_ngo_interest'];
	            						$paidToday = $row->ngoamount;
	            						$row->ngoamount = $payment['unformated_ngo_principal'] - $row->ngoamount;
	            						$owe = $row->ngoamount + ($row->ngoamount*.1);
				            			$remaingDue = $row->ngoamount;
	            						$principalPaid += $row->amount;
			            				$pastPaid = $principalPaid - $row->amount;

	            							

	            					} else {

	            						$owe = ($payment['unformated_ngo_interest'] - $row->ngoamount) + $payment['unformated_ngo_payment'] + ($payment['unformated_ngo_payment']*.1);
	            						$remaingDue = ($payment['unformated_ngo_interest'] - $row->ngoamount) + $payment['unformated_ngo_payment'];
	            						
	            					}

	            				} else {

	            					if(strtotime($row->date) > strtotime($payment['date'] . " + 5 days") ){
	            						$owe = $payment['unformated_ngo_principal'] * .1;
	            					} else {
	            						$owe = 0;
		            				}

			            			$remaingDue =0;
			            			$principalPaid += $payment['unformated_ngo_principal'];
			            			$pastPaid = $principalPaid - $payment['unformated_ngo_principal'];
	            						$paidToday = $payment['unformated_ngo_principal'];

	            				}


	            		$counter++;
	            		?>


	            			
	            		<?php endforeach;?>
	            	
	            <?php endif;?>
	            	
	            	<?php $thisPayment = $clientPayment[$counter - 1];?>
	        <label>Payment Due Date</label> <?=date('d M y', strtotime($thisPayment['date']))?><br />
	    
	         <label>Payment Amount Due</label> <?=money_format('%(#10n', $thisPayment['unformated_ngo_payment']+$pastOwe) . " " . $ngo_currency;?>
		
		 
	        </div>
	        
	       <br />
	
		<table>
			<tr>
				<td> <label>Begining Loan Balance</label></td>
				<td><?=money_format('%(#10n', $clientLoan->ngoamount-$pastPaid)?> <?=$ngo_currency?></td>
			</tr>
			<tr>
				<td>Amount Paid Today:</td>
				<td><?=money_format('%(#10n', $paidToday)?> <?=$ngo_currency?></td>
			</tr>
			<tr>
				<td><label>Today Loan Balance</label> </td>
				<td><?=money_format('%(#10n', $clientLoan->ngoamount-$principalPaid)?> <?=$ngo_currency?></td>
			</tr>
		</table>
		<br />
		<br />

	        	   <?php $payment = $clientPayment[$counter];?>

	     <?php if($remaingDue > 0):?>
	        <div style="font-size:20px; border: 2px solid black;">
		  <label>	Remaining Due: </label> <?=money_format('%(#10n', $remaingDue)?> <?=$ngo_currency?>
<br />  <br />
	        </div>
	       <?php endif;?>

	       <br />

	       <label>Paid to Date</label>	<?=money_format('%(#10n', $principalPaid)?> <?=$ngo_currency?>
<br /><br />
	       <label>Next Payment Due Date</label> <?=date('d M y', strtotime($payment['date']))?> <br />
	       	  <?php if($owe > 0):?>
	        	<strong>Fee Assess </strong> <?=money_format('%(#10n', $owe) . " " . $ngo_currency;?> <br />
	    	<?php endif;?>
	        <label>Next Payment Amount Due</label>  <?=money_format('%(#10n', $payment['unformated_ngo_payment']+$owe) . " " . $ngo_currency;?> <br />
	        <br />
	        	
	

<br />
<br />	  
	        
	        
<hr />

   
		      <label>Teller / Casher </label><br />
		      <?=$this->Ngo_model->get_user_name($transaction->userid);?> <br/>
		      <br />
		      <label>Cashier Signature and Stamp </label><br />
		      <div class="sign"></div>
		      
		       <label>Client Signature </label><br />
		      <div class="sign"></div>      	   
				
			    <?php endif;?>
		  </body>		    
		