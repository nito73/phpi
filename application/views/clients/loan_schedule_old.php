<?php 	$clientLoanPayment = $this->Client_model->view_client_loan_payment($clientID);
		$clientLoan = $this->Client_model->view_client_loan($clientID);

		$ngo_currency = $clientLoan->currency;
		$justinterest = $clientLoan->loaninterest/100;
		$totalrequested = $clientLoan->amount;
		$rate = $clientLoan->rate;
		$totalDue = $totalrequested;
		$totalDueNGO = $clientLoan->ngoamount;
		
			    		


	
?>
	    <div class="tab-content" style="padding:10px;">
			
		  
			<?php if($clientLoanPayment):?>
		
					  <h3>  Payment Schedule</h3>
					<legend><strong>Loan Amount</strong>
					<?=money_format('%(#10n', $totalDueNGO) . " " . $ngo_currency;?> / $ <?=money_format('%(#10n', $totalDue);?> USD </legend>
		    <table class="table table-hover">
		    <thead>
		    	<tr>
		    		<td>Date</td>
		    		<td>Principal</td>
					<td>Interest (<?=$justinterest*100?>%)</td>
		    		<td>Payment</td>
		    	</tr>
		    </thead>
		    <tbody>	
		    	
		    	<?php foreach($clientLoanPayment as $row):?>
		    	
		    	<tr>
		    		<td><?=$row['date'];?></td>
		    		<td><?=$row['ngo_principal'];?><br /><?=$row['principal'];?></td>

		    		<td><?=$row['ngo_interest'];?><br /><?=$row['interest'];?></td>
		    		<td><?=$row['ngo_payment'];?><br /><?=$row['payment'];?></td>

					
		    	</tr>
		    	<?php endforeach;?>
		    	</tbody>
		    </table>
		    
		    
		   
		    </div>
		    <?php endif;?>

	       </div>
    
    
                
      
      <script>
	      $(function(){
		      
		      $('.removeClient').click(function(event) { 
	    var note=confirm("Are you sure that you want to remove this Client");

		if(!note){
			event.preventDefault();
		}

   
    });
	      });
</script>

<?php $this->load->view('/dashboard/footer_view');?>