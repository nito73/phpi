<?php if($this->tank_auth->get_usertype() != "loan_officer" ):?>
<?php $this->load->view('/dashboard/sidebar_view');?>
<?php endif;?>


   <style type="text/css">
   
   </style>
       <!-- Jumbotron -->
           
      <div style="padding:10px;">
      	<div>
         <a href="<?=site_url("reports/dateProcess/daily");?>" class="btn pull-right btn-success" target="_blank">Print Daily Report</a>
         <a href="<?=site_url("clients/add");?>" class="btn pull-right btn-success">Add Client</a>

        </div>  
        <br />
        <br />
        <?php if($msg = $this->session->flashdata('message')):?>
        	    <div class="alert alert-<?=$msg['type']?>" style="margin-top: 10px">
        	    	<a class="close" data-dismiss="alert" href="#">&times;</a>
        		    <h4 class="alert-heading"><?=$msg['body']?></h4>
        	    </div>
        	    <?php endif;?>
        	  
        
            <div class="view_data well">
      <strong>View By</strong>
       City
        <select name="city" id="city">
        	<option value="All">All</option>
        	<?php $locquery = $this->Client_model->client_city($this->tank_auth->get_orgid());
    if ($locquery):?>
    <?php foreach($locquery as $row):?>
    	<option value="<?=$row->city?>"><?=$row->city?></option>
    <?php endforeach?>
    <?php endif;?>

        </select>
        
      Region
        <select name="region" id="region">
        	<option value="All">All</option>
        	<?php $locquery = $this->Client_model->client_region($this->tank_auth->get_orgid());
    if ($locquery):?>
    <?php foreach($locquery as $row):?>
    	<option value="<?=$row->region?>"><?=$row->region?></option>
    <?php endforeach?>
    <?php endif;?>

        </select>
        
        <br />
        Client Status
         
        <select name="Status" id="status">
        	<option value="Active">Active</option>
        	<option value="NEW">New Clients</option>
        	<option value="inActive">Deleted</option>
        </select>
			
      </div>
        
        <?php $query = $this->Dashboard_model->view_org_projects($this->tank_auth->get_orgid(), $location);
	            	$total = 0;
	            	?>

            <table class="table" id="myProjectsDataTable">
	            <thead>
	            <tr>
	            	<td>National ID</td>
	            	<td>Name</td>
	            	<td>Address</td>
	            	<td>Collection Location</td>
	            	<td>Phone</td>
	            	<td>View Activity</td>
	            </tr>
	            </thead>
	            <tbody>
	            
	            </tbody>
    </table>
    	            
    	     
    	     <form method="post" action="<?=site_url("ngo/print_collection");?>" target="_blank" id="printForm" >
    	     <input type="hidden" name="view" value="All" id="printView"/>
    	     </form>             
      </div>
      
      <script>
      
      $(function()
{
		$("#city").change(function(event){
			
			event.preventDefault();
			
			var viewLocation = $(this).val();
			$("#printView").val(viewLocation);
			oTable.fnDraw();
			
		});
		$("#region").change(function(event){
			
			event.preventDefault();
			
					oTable.fnDraw();
			
		});
		$("#status").change(function(event){
			
			event.preventDefault();
			
					oTable.fnDraw();
			
		});
		
		$("#print").click(function(event){
			
			event.preventDefault();
			$('#printForm').submit();
			
		});
		
		
        
 
        oTable = $('#myProjectsDataTable').dataTable( {
                "bProcessing": true,
                "bServerSide": true,
                "bJQueryUI": true,
                "sPaginationType": "bootstrap",
                "sAjaxSource": "<?php echo site_url('clients/myClientDatatable')?>",
                "fnServerData": function ( sSource, aoData, fnCallback ) {
                        $.ajax( {
                                "dataType": 'json',
                                "type": "POST",
                                "url": sSource,
                                "data": aoData,
                                "success": fnCallback
                        } );
                },
                "aaSorting": [[0,'DESC']],
                fnServerParams: function ( aoData ) {
                        aoData.push( { name: 'city', value: $('#city').val() } );
                        aoData.push( { name: 'region', value: $('#region').val() } );
                        aoData.push( { name: 'status', value: $('#status').val() } );
                },
                "bLengthChange": false
} );
       
 
});
      </script>
<?php //$this->load->view('/dashboard/footer_view');?>