<?php 
	$ngo_currency = "";
	$clientLoan = $this->Client_model->view_client_loan($clientID);
	$ngo_currency = (isset($clientLoan->currency)) ? $clientLoan->currency : $organization->currency;

	?>
		    <div class="tab-content" style="padding:10px;">
			

Collateral Account<br />
		    This account is only for Deposit to establish credit to apply for a loan
		    
		    
		    <?php 
		    if($repaymentquery = $this->Client_model->client_collateral_account($clientID)):?>
		    
		    <table class="table table-hover">
	            <thead>
	            <tr>
	            	<td>Date</td>
	            	<td>Type</td>
	            	<td><?=$ngo_currency?> Amount</td>
	            	<td>Staff</td>
	            	<td>Print</td>
	            	
	            </tr>
	            </thead>
	            <tbody>
	            	<?php
	            		$savingTotal = array(0);
	            		
	            	?>
	            		<?php foreach($repaymentquery as $row):?>
	            			
		            	
	            		
	            		<tr>
	            			<td><?=date('d M y', strtotime($row->date))?></td>
	            			<td><?php if($row->amount > 0):?>Deposit<?php else:?> Withdraw<?php endif;?></td>
	            			<td><?=money_format('%(#10n', $row->amount) . " " . $ngo_currency;?> </td>
	            			<td><?=$this->Ngo_model->get_user_name($row->userid);?> </td>
	            				<td><a href="<?=site_url('clients/printCollateralTransaction/' .$row->id)?>" class="btn btn-success" target="_blank"><i class="icon-print"></i> Print</a> </td>
	            			
	            		</tr>
	            		
	            		<?php 
	            			$savingTotal[0]+= $row->amount;
		            	 
	            		?>
	            		<?php endforeach;?>
	            		<tr class="total">
	            			<td colspan="2"style="text-align:right;">Saving Total</td>
	            			<td colspan="3"><?=money_format('%(#10n', $savingTotal[0]) . " " . $ngo_currency;?> </td>
	            				            			
	            		</tr>
	            			            

	            </tbody>
	        </table>
	   

		    <?php endif;?>
		    <div class="well">
			    <form method="post" action="<?=site_url('clients/collateralProcess')?>">
				    <label>Transaction Date </label>
				    <?php if($this->tank_auth->get_usertype() == "loan_officer"):?>
				    <?=date('d-m-Y')?>
					    <input type="hidden" name="date" class="datepicker" value="<?=date('d-m-Y')?>"/>

				    <?php else:?>
					    <input type="text" name="date" class="datepicker" value="<?=date('d-m-Y')?>"/>
				    <?php endif;?>
				    
					<input type="hidden" class="input-xxlarge" name="clientID" id="clientID" value="<?=$clientID?>" />
					<input type="hidden" class="input-xxlarge" name="currency" id="currency" value="<?=$ngo_currency?>" />
					
					<label>Amount <?=$ngo_currency?></label><input type="text" name="amount" />
				    
				    <input class="btn btn-success"type="submit" value="Send" />
				</form>
	
		    </div>

			      
      <script>
	      $(function(){
		      
		      $('.removeClient').click(function(event) { 
	    var note=confirm("Are you sure that you want to remove this Client");

		if(!note){
			event.preventDefault();
		}

   
    });
	      });
</script>

<?php $this->load->view('/dashboard/footer_view');?>