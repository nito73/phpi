<style type="text/css">
	#editor{
		overflow:scroll;
		max-height:300px;
	}
	.client img{
		height: 200px;
	}	
</style>

<link rel="stylesheet" href="/imageupload/css/jquery.fileupload-ui.css">

<?php $query = $this->Client_model->viewClient($clientID);

    foreach($query as $row):?>
<div style="padding:10px;" class="client">
        <h1>Edit Client Application</h1>
        <p class="lead"></p>
        <div>
        <form id="form" method="post" action="<?=base_url('clients/editProcess');?>" class="form-horizontal">
		<h3>Client Info</h3>	
  	   <div class="control-group">
		    <label class="control-label" for="name">Client Name</label>
		    <div class="controls">
				<input class="input-xxlarge" type="text" name="name" id="name" placeholder="Client Name" value="<?=$row->name?>">
		    </div>
		  </div>
		   <div class="control-group">
		    <label class="control-label" for="uploadimage">Client Photo</label>
		    <div class="controls">
		 <img src="<?=$row->pics?>" class="uploadimage"/>
	    
	    
		<span class="btn btn-success fileinput-button">
	        <i class="icon-plus icon-white"></i>
	        <span>Select files...</span>
		    <input id="fileupload" type="file" name="files[]" multiple>
	    </span>
	    
	   	    <div id="progress" style="width:75%;" class="progress progress-success progress-striped">
	        <div class="bar" ></div>
	    </div>
	    
		    </div>
		  </div>
		  
		   <div class="control-group">
		    <label class="control-label" for="gov_id">National ID</label>
		    <div class="controls">
				<input class="input-xxlarge" type="text" name="gov_id"  placeholder="National ID" value="<?=$row->gov_id?>">
		    </div>
		  </div>
 
		   <div class="control-group">
		    <label class="control-label" for="birthday">Birthday</label>
		    <div class="controls">
			<input class="input-xxlarge datepicker" type="text" name="birthday"  placeholder="Birthday" value="<?=($row->birthdate != "1969-12-31" && $row->birthdate != "0000-00-00") ? $row->birthdate : ''?>">
		    </div>
		  </div>
 
		   <div class="control-group">
		    <label class="control-label" for="birthplace">Birthplace</label>
		    <div class="controls">
		<input class="input-xxlarge" type="text" name="birthplace"  placeholder="Birthplace" value="<?=$row->birthplace?>"><br />
		    </div>
		  </div>
 
		   <div class="control-group">
		    <label class="control-label" for="gender">Gender</label>
		    <div class="controls">
				<label class="radio inline">
		<input type="radio" name="gender" value="male" <?php if($row->gender == "male"):?> checked="checked" <?php endif;?> >Male</label>
		<label class="radio inline">
		<input type="radio" name="gender" value="female" <?php if($row->gender == "female"):?> checked="checked" <?php endif;?>>Female
		</label>
<br />
		    </div>
		  </div>
		  <div class="control-group">
		    <label class="control-label" for="martial_status">Martial Status</label>
		    <div class="controls">
		<input class="input-xxlarge" type="text" name="martial_status"  placeholder="Martial Status" value="<?=$row->martial_status?>"><br />
		    </div>
		  </div>
		  
		  <div class="control-group">
		    <label class="control-label" for="job">Occupation</label>
		    <div class="controls">
		<input class="input-xxlarge" type="text" name="job"  placeholder="Occupation/Profession" value="<?=$row->job?>"><br />
		    </div>
		  </div>
 
        <hr />
		   <div class="control-group">
		    <label class="control-label" for="address">Address</label>
		    <div class="controls">
	<input class="input-xxlarge" type="text" name="address" id="address" placeholder="Address" value="<?=$row->address?>">
		<br />
			<input class="span3" type="text" name="city" id="city" placeholder="City" value="<?=$row->city?>">
		<input class="span2" type="text" name="region" id="Region" placeholder="Region" value="<?=$row->region?>">
		<input class="span2" type="text" name="postalcode" id="Postal Code" placeholder="Postal Code" value="<?=$row->postalcode?>">

		
		    </div>
		  </div>
 
	<div class="control-group">
		    <label class="control-label" for="cell">Mobile Phone</label>
		    <div class="controls">
		<input class="input-xxlarge" type="text" name="cell" id="cell" placeholder="Mobile Phone" value="<?=$row->cell?>"> 

		    </div>
		  </div>
		
		
		<h3>Co Signer Info</h3>
  <div class="control-group">
		    <label class="control-label" for="coname">Co-Signer Name</label>
		    <div class="controls">
				<input class="input-xxlarge" type="text" name="coname" id="name" placeholder="Client Name" value="<?=$row->coname?>">
		    </div>
		  </div>
		   <div class="control-group">
		    <label class="control-label" for="couploadimage">Co-Signer Photo</label>
		    <div class="controls">
		 <img src="<?=$row->copics?>" class="uploadimage"/>
	    
	    
		<span class="btn btn-success fileinput-button">
	        <i class="icon-plus icon-white"></i>
	        <span>Select files...</span>
		    <input id="fileupload" type="file" name="files[]" multiple>
	    </span>
	    
	   	    <div id="progress" style="width:75%;" class="progress progress-success progress-striped">
	        <div class="bar" ></div>
	    </div>
	    
		    </div>
		  </div>
		  
		   <div class="control-group">
		    <label class="control-label" for="cogov_id">National ID</label>
		    <div class="controls">
				<input class="input-xxlarge" type="text" name="cogov_id"  placeholder="National ID" value="<?=$row->cogov_id?>">
		    </div>
		  </div>
 
		   <div class="control-group">
		    <label class="control-label" for="cobirthday">Birthday</label>
		    <div class="controls">
			<input class="input-xxlarge datepicker" type="text" name="cobirthday"  placeholder="Birthday" value="<?=($row->cobirthdate != "1969-12-31" && $row->cobirthdate != "0000-00-00") ? $row->cobirthdate : ''?>">
		    </div>
		  </div>
 
		   <div class="control-group">
		    <label class="control-label" for="cobirthplace">Birthplace</label>
		    <div class="controls">
		<input class="input-xxlarge" type="text" name="cobirthplace"  placeholder="Birthplace" value="<?=$row->cobirthplace?>"><br />
		    </div>
		  </div>
 
		   <div class="control-group">
		    <label class="control-label" for="cogender">Gender</label>
		    <div class="controls">
				<label class="radio inline">
		<input type="radio" name="cogender" value="male" <?php if($row->cogender == "male"):?> checked="checked" <?php endif;?> >Male</label>
		<label class="radio inline">
		<input type="radio" name="cogender" value="female" <?php if($row->cogender == "female"):?> checked="checked" <?php endif;?>>Female
		</label>
<br />
		    </div>
		  </div>
		  <div class="control-group">
		    <label class="control-label" for="comartial_status">Martial Status</label>
		    <div class="controls">
		<input class="input-xxlarge" type="text" name="comartial_status"  placeholder="Martial Status" value="<?=$row->comartial_status?>"><br />
		    </div>
		  </div>
		  
		  <div class="control-group">
		    <label class="control-label" for="cojob">Occupation</label>
		    <div class="controls">
		<input class="input-xxlarge" type="text" name="cojob"  placeholder="Occupation/Profession" value="<?=$row->cojob?>"><br />
		    </div>
		  </div>
 
        <hr />
		   <div class="control-group">
		    <label class="control-label" for="coaddress">Address</label>
		    <div class="controls">
	<input class="input-xxlarge" type="text" name="coaddress" id="address" placeholder="Address" value="<?=$row->coaddress?>">
		<br />
			<input class="span3" type="text" name="cocity" id="city" placeholder="City" value="<?=$row->cocity?>">
		<input class="span2" type="text" name="coregion" id="Region" placeholder="Region" value="<?=$row->coregion?>">
		<input class="span2" type="text" name="copostalcode" id="Postal Code" placeholder="Postal Code" value="<?=$row->copostalcode?>">

		
		    </div>
		  </div>
 
	<div class="control-group">
		    <label class="control-label" for="cocell">Mobile Phone</label>
		    <div class="controls">
		<input class="input-xxlarge" type="text" name="cocell" id="cell" placeholder="Mobile Phone" value="<?=$row->cocell?>""> 

		    </div>
		  </div>  		
	   
		<input type="hidden" name="pic" id="pic" placeholder="URL of Picture" value="<?=$row->pics?>">
		<input type="hidden" name="copic" id="copic" placeholder="URL of Picture" value="<?=$row->copics?>">
		<input type="hidden" name="clientID" id="clientID" value="<?=$clientID?>">

		<br />
    <input type="submit" class="btn" value="Edit Client" />
  </div>
 </div>
  
  </form>      
        
		
        </div>
        
        <?php endforeach;?>
<script src="/imageupload/js/vendor/jquery.ui.widget.js"></script>
<script src="/imageupload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="/imageupload/js/jquery.fileupload.js"></script>
<script src="/imageupload/js/jquery.fileupload-image.js"></script>


<script>
$(function(){
	   	   'use strict';
    // Change this to the location of your server-side upload handler:
   var url = (window.location.hostname === 'blueimp.github.com' ||
                window.location.hostname === 'blueimp.github.io') ?
                '//jquery-file-upload.appspot.com/' : '<?=base_url();?>/imageupload/server/php/';

    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        disableImageResize: /Android(?!.*Chrome)|Opera/
        .test(window.navigator && navigator.userAgent),
    imageMaxWidth: 250,
    imageMaxHeight: 250,
    imageCrop: true, // Force cropped images
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
              	
                $('#pic').val(file.url);
                $('.uploadimage').attr('src', file.url);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .bar').css(
                'width',
                progress + '%'
            );
        }
    });
    
       $('#cofileupload').fileupload({
        url: url,
        dataType: 'json',
        disableImageResize: /Android(?!.*Chrome)|Opera/
        .test(window.navigator && navigator.userAgent),
    imageMaxWidth: 250,
    imageMaxHeight: 250,
    imageCrop: true, // Force cropped images
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
              	
                $('#copic').val(file.url);
                $('.couploadimage').attr('src', file.url);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#coprogress .bar').css(
                'width',
                progress + '%'
            );
        }
    });
    
    
  

  });
  
  


</script>
