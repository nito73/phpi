<?php 
	$ngo_currency = $transaction->currency;
	?>
<style type="text/css">
	

	body{
		font-size: 18px;
		width: 300px;
	}
	h3{
		font-size: 18px;
	}
	label{
		font-weight: bold;
	}
		img{
		width: 290px;
		margin: 0 auto;
	}
	tr td{
		text-align: right;
	}
	.sign{
			border-bottom: 1px black solid;
			width: 100%;
			margin-top: 10px;
			margin-bottom: 10px;
			height: 20px;
			
	}
	body{
		padding-bottom: 30px;
	}
</style>
<body>

 
  <img src="<?=$organization->logo?>" />


<?php $query = $this->Client_model->viewClient($clientID);

    foreach($query as $row):?>

     
  

     <label>Client Name:</label> <?=$row->name?> (<?=$row->gov_id?>) <br />
     <label>Client Account ID# </label>  <?=$row->id?> <br/>
	<label>Transaction Date: </label><?=date('d M y', strtotime($transaction->date))?> <br />
	<label>Transaction Recorded: </label><br />
	 <i>Collateral Deposited</i> <br />
<br />
<hr />

<?php endforeach;?>

		    
		    <?php 
		    	$totalCurrent = $this->Client_model->client_collateral_trans_total($clientID, $transaction->id);
		    	$totalPrior = $totalCurrent - $transaction->amount;
		    ?>
		   <table >
		   	<tr>
		   		<td> <label> Beginning Balance:</label> </td>
		   		<td><?=money_format('%(#10n', $totalPrior ) . " " . $ngo_currency;?> </td>
		   	</tr>
		   	
	        <tr>
		   		<td> <label> Deposit:</label> </td>
		   		<td><?=money_format('%(#10n', $transaction->amount ) . " " . $ngo_currency;?> </td>
		   	</tr>
		   	<tr>
		   		<td> <label> Today Balance:</label> </td>
		   		<td><?=money_format('%(#10n', $totalCurrent ) . " " . $ngo_currency;?> </td>
		   	</tr>
		   </table>    				            			
	   
	   

	
		    <hr />
		    
		      <label>Teller / Casher </label><br />
		      <?=$this->Ngo_model->get_user_name($transaction->userid);?> <br/>
		      <br />
		      <label>Cashier Signature and Stamp </label><br />
		      <div class="sign"></div>
		      
		       <label>Client Signature </label><br />
		      <div class="sign"></div>
</body>
		