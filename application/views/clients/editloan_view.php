<style type="text/css">
	#editor{
		overflow:scroll;
		max-height:300px;
	}
</style>
<link rel="stylesheet" href="/imageupload/css/jquery.fileupload-ui.css">

<?php $query = $this->Give_model->viewProject($projectID);?>
<?php foreach($query as $row):?>
<?php $clientID = $row->client_id;?>
<div style="padding:10px;">
		<h1>Edit Application</h1>
        <div>
        <form id="form">
        <label>Name of the Application</label>
		<input class="input-xxlarge" type="text" name="title" id="title" value="<?=$row->title;?>">
		<label>Amount Requested</label>
		<p><?=money_format('%(!#10n', $row->amount);?></p>
		
		<label>Type of Application</label>
			<select class="input-xxlarge" name="type" id="type">
			<option value="<?=$row->type;?>"> <?=$row->type?></option>
			  <option value="Micro">Micro</option>
			  <option value="Macro">Macro/Meso</option>
			  <option value="Project">Application</option>
			</select>
		
		<label>Type of Industry</label>
			<select class="input-xxlarge" name="category" id="category">
			  <option value="<?=$row->category;?>"> <?=$row->category?></option>
			  <option value="Food">Food</option>
			  <option value="Agriculture">Agriculture</option>
			  <option value="Service">Service</option>
			  <option value="Small Trade">Small Trade</option>
			  <option value="Manufacturing">Manufacturing</option>
			</select>		
			
  	
		<label>Name of the Borrower</label>
		<input class="input-xxlarge" type="text" name="name" id="name" value="<?=$row->name;?>">
		
		<label>Application Location</label>
		Location: <?=$row->location;?> <br />
		Country: <?=$row->country;?> <br />
		<br />
		
		
		<label>Application Photo</label>
		 <img src="<?=$row->pics;?>" class="uploadimage"/>
		<span class="btn btn-success fileinput-button">
	        <i class="icon-plus icon-white"></i>
	        <span>Select files...</span>
		    <input id="fileupload" type="file" name="files[]" multiple>
	    </span>
	    
	   	    <div id="progress" class="progress progress-success progress-striped">
	        <div class="bar"></div>
	    </div>
	   
	    
		<input class="input-xxlarge" type="hidden" name="pic" id="pic" placeholder="URL of Picture" value="<?=$row->pics;?>">
		
		<label>Application Video</label>
		<input class="input-xxlarge" type="text" name="video" id="video" placeholder="Youtube ID" value="<?=$row->video;?>">
		
		<label>Brief Application Use</label>
		<input class="input-xxlarge" type="text" name="use" id="use" value="<?=$row->use;?>">
		
		<label>Brief Application Description</label>
		<div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor">
      
<!--
	      <div class="btn-group">
	        <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="icon-text-height"></i>&nbsp;<b class="caret"></b></a>
	          <ul class="dropdown-menu">
	          <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
	          <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
	          <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
	          </ul>
	      </div>
	      
-->
	      <div class="btn-group">
	        <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="icon-bold"></i></a>
	        <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="icon-italic"></i></a>
	      </div>
      
      </div>
      
    

    <div id="editor" name="description">
      <?=$row->description?>
    </div>
    
    <?php endforeach;?>
    <input type="submit" class="btn" value="Submit" />
  </div>
 </div>
  
  </form>      
        
		
        </div>
<script src="/imageupload/js/vendor/jquery.ui.widget.js"></script>
<script src="/imageupload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="/imageupload/js/jquery.fileupload.js"></script>

<script>
$(function(){
	     	   'use strict';
    // Change this to the location of your server-side upload handler:
var url = (window.location.hostname === 'blueimp.github.com' ||
                window.location.hostname === 'blueimp.github.io') ?
                '//jquery-file-upload.appspot.com/' : 'http://php.phuyumyums.com/imageupload/server/php/';

    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
              	
                $('#pic').val(file.url);
                $('.uploadimage').attr('src', file.url);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .bar').css(
                'width',
                progress + '%'
            );
        }
    });
 	  
$("#form").submit(function(event) {
 var projectdesc = $('#editor').html();
    
	 $.ajax({
	  type: "POST",
	  url: '<?=base_url();?>clients/updateProcess',
	  dataType: 'JSON',
	  data: { title: $("#title").val(), name: $("#name").val(), use: $("#use").val(),  category: $("#category").val(),    description: projectdesc, type: $("#type").val(), pic: $("#pic").val(), video: $("#video").val(), projectID: "<?=$projectID?>" },
	  success: function(data) { 
	  	window.location = "<?=base_url();?>/clients/viewClient/<?=$clientID;?>";
	   },
	});
	
	 event.preventDefault();
});

  });
  
  


</script>
