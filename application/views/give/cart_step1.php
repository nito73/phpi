<div id="itemincart">
<?php echo form_open('/give/cartProcess'); ?>


<?php $i = 1; ?>
<?php if($this->cart->total_items() != 0):?>
<?php foreach ($this->cart->contents() as $items): ?>
<div id="projectitem<?=$items['rowid']?>">
<div class="row">
<div class="span8">
	<?php $query  = $this->Give_model->viewProject($items['id']);?>
		
		
		<?php foreach($query as $row):?>

  <div class="row">
    <div class="span2">
    <a href="#" class="thumbnail">
      <img src="<?php if($row->pics != "") { echo $row->pics;} else { echo base_url('images') . "/no_image.jpg";}?>">
    </a>
    </div>
    <div class="span6">
          <h3><a href="<?=site_url('/give/view/' . $row->id);?>"><?=$row->title?></a></h3>
          <strong><?=$row->location?> | <?=$row->category?></strong>
         <p><?=$row->use?></p>
       </div> 
  </div>
  
  	<?PHP $fullamount = $row->amount;?>
          <?php endforeach;?>

	</div>
	<div class="span2"><strong>Donation Amount</strong> 
	
		<span class="typeitemamount<?=$i?>" style="display:none">
			<input type="text" class="itemamount typeitemamountinput<?=$i?>" id="amount" value="0" />
		</span>
			
			
	<select class="itemamount" id="amount" name="price<?=$i?>" rel="<?=$i?>">
		<option value="<?=$items['price']?>" label="$<?=$items['price']?>">$<?=$items['price']?></option>
				<?php for($x=25; $x < $fullamount; $x = $x + 25):?>
				<option value="<?=$x?>" label="$<?=$x?>.00">$<?=$x?>.00</option>
				<?php endfor;?>
				
				<option value="<?=$fullamount?>" label="$<?=$fullamount?>.00">$<?=$fullamount?>.00</option>
			</select>
			<input type="hidden" name="id<?=$i?>" value="<?=$items['id']?>" />
			<input type="hidden" name="rowid<?=$i?>" value="<?=$items['rowid']?>" />
			
			<a href="#" class="offset2 remove" style="text-align:right" rel="<?=$items['rowid']?>">Remove</a>
			</div>


</div>
<hr/>
</div>
<?php $i++; ?>

<?php endforeach; ?>
				<input type="hidden" name="itemcount" value="<?=$i?>" />
<div class="row">

<div class="span4 offset6 php" rel="">
	<table style="width:100%;">

	
	
	
</div>
<div class="span4 offset6 total" rel="<?=$this->cart->total()?>">
<tr style="border-top:1px solid black;font-size:16px;">
	<td>
		<strong>Total Donation Amount</strong>
	</td>
	<td>
		$<span id="finaltotal"><?php echo $this->cart->format_number($this->cart->total()+($this->cart->total()*.08)); ?></span>
	</td>
</tr>
</table>	
<p class=""> Additonal Cost including Micro Seed account deduction will be done on the next step</p>
 <input class="btn btn-large btn-primary" type="submit" value="Checkout" />
</form>
</div>
</div>
<?php else:?>
<h2>Your Micro Seed cart is Empty</h2>
<p class="lead">To plant a micro seed you need to view loans and Applications that need your support!</p>
<p><a href="/give/">Click here to view different Loan/Project Activities that need your support</a></p>

<?php endif?>
</div>

<div id="emptycart" style="display:none;">
<h2>Your Seed cart has been Emptied</h2>
<p class="lead">You need to plant a seed by viewing the Applications to see them in your cart!</p>
<p><a href="/give/">Click here to view different Loan/Project Activity that need your support</a></p>

</div>



    <script>
    
     
     function calcfinaltotal(){
	     var finaltotal = 0;
	    $(".itemamount").each(function() { 
	    	var subtotal =  parseInt($(this).val());
	    	console.log(subtotal);
		    finaltotal = finaltotal + subtotal;
	    });
	       var phpcost = finaltotal*.08;
	       $("#phpcost").text(phpcost.toFixed(2));
	       finaltotal = finaltotal + phpcost;
	      $("#finaltotal").text(finaltotal.toFixed(2));
	      

	     
	     
     }
     
  $(function(){
  
    
    
        $(".itemamount").keydown(function(event) {
        // Allow: backspace, delete, tab, escape, and enter
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault(); 
            }   
        }
       
        calcfinaltotal();
    });

     $(".itemamount").click(function(event) {
               
         if($(this).val() == "MORE"){
         	var id = $(this).attr('rel');
	         
	         $(this).remove();
	         $(this).attr('name', '');
	         $('.typeitemamount'+id).show();
	         $('.typeitemamountinput'+id).attr('name', 'price'+id);
         } else {
	         calcfinaltotal();
        }
    });

 

         $(".itemamount").keyup(function(event) {
               
        
	         calcfinaltotal();
        
    });

    
    
    $(".remove").click(function(event){
	    var rowid = $(this).attr('rel');
	   
	    
	    
	    $.ajax({
		  type: "POST",
		  url: '<?=base_url();?>give/removeSeed',
		  data: { rowid: rowid },
		  dataType: "JSON",
		  success: function(data) { 
		  	 
		    
			 setTimeout(function(){ 
				$('#projectitem' + rowid).fadeOut('2000');
				calcfinaltotal();
		    removeCart();
		    
		    	  var carttotal = $('#carttotal');
	  	if(carttotal.text() == "Empty") {
	  	$("#itemincart").remove();
	  	$("#emptycart").fadeIn();
	  	
	  	}
		    }, 1500 ); 
		}
		
		
	});
	    
	    event.preventDefault();
	    
	  
	  

  
    });
    
    });
    
   


</script>