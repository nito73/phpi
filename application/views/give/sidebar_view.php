<div class="container-fluid" style="padding-top:10px;">
  <div class="row-fluid">
    <div class="span2">
     <ul class="nav nav-list">
     	<li class="nav-header">Loans </li>
     	<li <?php if ($viewLoanType == 'ALL') { echo 'class="active"';}?> ><a href="/give/viewLoanType/ALL">All Loan Activities</a></li>
	  <li <?php if ($viewLoanType == 'Micro') { echo 'class="active"';}?>><a href="/give/viewLoanType/Micro">Micro Loans</a></li>
	  <li <?php if ($viewLoanType == 'Macro') { echo 'class="active"';}?>><a href="/give/viewLoanType/Macro">Macro/Meso Loans</a></li>
	 </ul>
    <?php if ($viewLoanType != 'Projects'):?>
    <ul class="nav nav-list">
        	<li <?php if ($viewLoanCategory == 'ALL') { echo 'class="active"';}?> ><a href="/give/viewLoanCategory/ALL">All Business Sectors</a></li>
    	<li <?php if ($viewLoanCategory == 'Food') { echo 'class="active"';}?> ><a href="/give/viewLoanCategory/Food">Food</a></li>
		<li <?php if ($viewLoanCategory == 'Agriculture') { echo 'class="active"';}?> ><a href="/give/viewLoanCategory/Agriculture">Agriculture</a></li>
			  <li <?php if ($viewLoanCategory == 'Service') { echo 'class="active"';}?> ><a href="/give/viewLoanCategory/Service">Service</a></li>
			  <li <?php if ($viewLoanCategory == 'Small Trade') { echo 'class="active"';}?> ><a href="/give/viewLoanCategory/Small Trade">Small Trade</a></li>
			  <li <?php if ($viewLoanCategory == 'Manufacturing') { echo 'class="active"';}?> ><a href="/give/viewLoanCategory/Manufacturing">Manufacturing</a></li>
    </ul>
    <?php endif;?>
    	<ul class="nav nav-list">

    	<li class="nav-header">Projects </li>

	  <li <?php if ($viewLoanType == 'Project') { echo 'class="active"';}?>><a href="/give/viewLoanType/Project">All Projects</a></li>
	 </ul>
    <?php if(!$this->session->userdata('SpecialNGO')):?>
    
    <ul class="nav nav-list">
    	<li class="nav-header">NGOs</li>
    		<li <?php if ($orgID == "0") { echo 'class="active"';}?> ><a href="/give/viewOrg/0">All NGOs </a></li>	

    	      <?php foreach ($this->Give_model->viewOrgList() as $orgrow):?>

    	      <li <?php if ($orgID == $orgrow->id ) { echo 'class="active"';}?> ><a href="/give/viewOrg/<?=$orgrow->id?>"><?=$orgrow->organization?></a></li>
    	      <?php endforeach;?>
    </ul>
    
    <?php endif;?>
    </div>
    
    
     <div class="span10">