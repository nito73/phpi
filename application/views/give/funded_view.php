   
   <?php foreach($query as $row):?>
  <div class="row">
    <div class="span4">
    <a href="#" class="thumbnail">
      <img src="<?php if($row->pics != "") { echo $row->pics;} else { echo base_url('images') . "/no_image.jpg";}?>">
    </a>
    </div>
    <div class="span7">
          <h1><?=$row->title?>
          <?php if($this->tank_auth->get_usertype() == "Admin"):?>
          	<?php if($row->public == "Yes"):?>
	          	<a class="changeproject btn btn-success" rel="<?=$row->id?>">Live</a>
	        <?php else:?>
        	  	<a class="changeproject btn btn-danger" rel="<?=$row->id?>">Offline</a>
	        <?php endif;?>
          <?php endif;?>
          
          </h1>
          <h4><?=$row->location?> | <?=$row->category?></h4>
        <p class="lead"><?=$row->use?> <a href="/give/view/<?=$row->id;?>">Read More </a></p>
        <small>Funding By <a href="#"><?=$this->Give_model->getOrgByOrgID($row->organization_id)?></a></small>
      
    </div>
  </div>
    <hr>
    
    <?php endforeach;?>
<?php $this->load->view('/give/footer_view');?>


