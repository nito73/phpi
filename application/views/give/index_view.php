  <?php
    	$nextpage = $pagenum + 1;
    	$prevpage = $pagenum - 1;
    ?>

   <div class="plantcart alert alert-success">
    Your seed has be planted for <strong><span id="projectname"></span></strong>, <a href="/give/cart">Click here</a> to view to finish planting
    </div>
    <div style="padding-bottom:10px;">
      <?php if($query['total'] - ($pagenum*25) > 25  || ($pagenum == 0 && $query['total'] > 25) ):?>
      <a href="<?=base_url('give/moreLoans/' . $nextpage) ?>" class="pull-right btn btn-warning" >View Next Loans <i class="icon-chevron-right icon-white"></i> </a>
      <?php endif;?>
   
   <?php if($pagenum != 0):?>
    <a href="<?php if ($prevpage == 0 ) { echo base_url('give'); } else { echo base_url('give/moreLoans/' . $prevpage ); }?>" class="btn btn-warning"><i class="icon-chevron-left icon-white"></i>View Previous Loans</a>
	<?php endif;?>
    </div>
    <div class="clearfix"></div>
    <?php if($query['query']):?>
   <?php foreach($query['query'] as $row):?>
   <?php $total = $this->Give_model->getTotalFunded($row->id);
	   $percenttotal = floor(($total/$row->amount)*100);
	   
   ?>
  <div class="row">
    <div class="span4">
    <a href="/give/view/<?=$row->id;?>" class="thumbnail">
      <img style="width:300px;"src="<?php if($row->pics != "") { echo $row->pics;} else { echo base_url('images') . "/no_image.jpg";}?>">
    </a>
    </div>
    <div class="span7">
          <h3><?=$row->title?>
          <?php if($this->tank_auth->get_usertype() == "Admin"):?>
          	<?php if($row->public == "Approved"):?>
	          	<a class="changeproject btn btn-success" rel="<?=$row->id?>">Live</a>
	        <?php else:?>
        	  	<a class="changeproject btn btn-danger" rel="<?=$row->id?>">Offline</a>
	        <?php endif;?>
          <?php endif;?>
          
          </h3>
                    <strong>Location: </strong> <?=$row->country?> | <?=$row->location?> <br />
          <strong> Business sector: </strong> <?=$row->category?>
         	 <?php if($percenttotal > "99"):?>This has been is Fully Funded<?php endif;?>
			<div class="progress">
			      <div class="bar" style="width: <?=$percenttotal?>%;"></div>
					   <label><?=$percenttotal?>% Funded of $<?=$row->amount?></label>
			</div>
        <p class="lead"><?=$row->use?> <a href="/give/view/<?=$row->id;?>" class="btn btn-warning">Read More </a></p>
        <small>Funding By <a href="#<?php echo site_url();?>pages/readMore/<?=$row->organization_id?>" ><?=$this->Give_model->getOrgByOrgID($row->organization_id)?></a></small>
        <a class="plantseedmodal btn btn-large btn-success pull-right" href="#" rel="<?=$row->id?>">Plant a Seed</a>
    </div>
    
    
    <div class="modal fade modal<?=$row->id;?> hide">
  <div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <h3>Plant A Seed <?=$row->title?></h3>
  </div>
  <div class="modal-body">
     <div class="row">
    <div class="span4">
    <img  style="width:300px;" src="<?php if($row->pics != "") { echo $row->pics;} else { echo base_url('images') . "/no_image.jpg";}?>">
    </div>
    <div class="span7 loaninfo<?=$row->id;?>">
	   <?php if( $this->tank_auth->is_logged_in() && $seedaccount = $this->Give_model->get_seed_account()):?>
	   	<?php if($seedaccount != 0):?>
	   		 <strong>Currently in Seed Account<span style="float:right;">$<?=money_format('%(!#10n', $seedaccount)?></span></strong>	

		
	   	<?php endif;?>
	   <?php endif;?>

	   <?php if($total):?>
    	 <strong>Amount Raised <span style="float:right;">$<?=money_format('%(!#10n', $total)?></span></strong>
    	  <?php if($percenttotal > "99"):?>This has been is Fully Funded<?php endif;?>
			<div class="progress">
			      <div class="bar" style="width: <?=$percenttotal?>%;"></div>
					   <label><?=$percenttotal?>% Funded of $<?=$row->amount?></label>
			</div>
		 <strong>Amount Needed <span style="float:right;">$<?=money_format('%(!#10n', $row->amount-$total)?></span></strong>	
		<hr />
		<?php endif;?>
		

    	 <label>Select Seed Amount</label>
    	 <select class="itemamount" id="price<?=$row->id?>" name="price<?=$row->id?>">
				
				<?php for($x=25; $x < $row->amount; $x = $x + 25):?>
				<option value="<?=$x?>" label="$<?=$x?>.00">$<?=$x?>.00</option>
				<?php endfor;?>
				<option value="<?=$row->amount?>" label="$<?=$row->amount?>.00">$<?=$row->amount?>.00</option>
				
			</select> 
    </div>
    
    
     </div>
  </div>
  <div class="modal-footer">
    <a href="#" class="btn" data-dismiss="modal">Close</a>
    <a href="#" class="plantseed btn btn-success" rel="<?=$row->id?>">Plant Seed</a>
  </div>
</div>



  </div>
    <hr>
    
    <?php endforeach;?>
	    <div class="clearfix"></div>
<?php if($query['total'] - ($pagenum*25) > 25  || ($pagenum == 0 && $query['total'] > 25)):?>
      <a href="<?=base_url('give/moreLoans/' . $nextpage) ?>" class="pull-right btn btn-warning" >View Next Loans <i class="icon-chevron-right icon-white"></i> </a>
      <?php endif;?>
   
   <?php if($pagenum != 0):?>
    <a href="<?php if ($prevpage == 0 ) {echo base_url('give'); } else { echo base_url('give/moreLoans/' . $prevpage ); }?>" class="btn btn-warning"><i class="icon-chevron-left icon-white"></i> View Previous Loans</a>
	<?php endif;?>
    <?php else :?>
    	<h2>Please come back shortly to more <?=$viewLoanType?> Applications, Currently People Helping People staff are working on getting more Applications Posted</h2>
    <?php endif;?>
<?php $this->load->view('/give/footer_view');?>


<script>
$(function(){
	$(".plantcart").hide();
	
	$(".changeproject").click(function(){
		var button = $(this);
		var projectid = $(this).attr('rel');
		var currentStatus = $(button).text();
		if(currentStatus == "Live"){
		$.ajax({
		  type: "POST",
		  url: '<?=base_url();?>admin/projectpublic',
		  data: { projectpublic: 'no', project_id: projectid },
		  success: function(data) { 
			  $(button).removeClass('changeproject btn btn-success').addClass("changeproject btn btn-danger");
			  $(button).text("Offline");
		   }
		   
		});
		} else {
					$.ajax({
		  type: "POST",
		  url: '<?=base_url();?>admin/projectpublic',
		  data: { projectpublic: 'Approved', project_id: projectid },
		  success: function(data) { 
			  $(button).removeClass("changeproject btn btn-danger").addClass('changeproject btn btn-success');
			  $(button).text("Live");
		}
		   
		});
			
			
		}
	
	});		
	
	$(".plantseed").click(function(event){
	var plantseedbtn = $(this);
	var projectid = $(this).attr('rel');
	var amount = $("#price" + projectid).val();
	console.log(amount);
		$.ajax({
		  type: "POST",
		  url: '<?=base_url();?>give/plantseed',
		  data: { id: projectid, amount:amount},
		  dataType: "JSON",
		  success: function(data) { 
		  if(data.status == "error"){
				   $(".loaninfo" + projectid).append('<div class="alert alert-error">' + data.msg + '</div>');
				   $("#projectname").text(data.projectname);

				  
			  } else {
				  $(".loaninfo" + projectid).html('<div class="alert alert-success">This loan has been successfully added to your <a herf="<?=base_url()?>/give/cart" >Seeds</a></div>');
  			  	  $(plantseedbtn).remove();
				  updateCart();
				  $("#projectname").text(data.projectname);
		  	}
		}
		
		
	});
	
	});
	
	$(".plantseedmodal").click(function(event){
		var projectid = $(this).attr('rel');
		$('.modal'+ projectid).modal('show');
		
	});
	
});   
  
  


</script>