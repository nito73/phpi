
<?php if($msg = $this->session->flashdata('message')):?>
	    <div class="alert alert-<?=$msg['type']?>" style="margin-top: 10px">
	    	<a class="close" data-dismiss="alert" href="#">&times;</a>
		    <h4 class="alert-heading"><?=$msg['body']?></h4>
	    </div>
	    <?php endif;?>
	  
<div id="itemincart"style="padding:10px;">

<?php $i = 1; ?>
<?php if($this->cart->total_items() != 0):?>
<?php foreach ($this->cart->contents() as $items): ?>
<div id="projectitem<?=$items['rowid']?>">

<div class="row">
<div class="span8">
	<?php $query  = $this->Give_model->viewProject($items['id']);?>
		
		
		<?php foreach($query as $row):?>

  <div class="row">
    <div class="span2">
    <a href="#" class="thumbnail">
      <img src="<?php if($row->pics != "") { echo $row->pics;} else { echo base_url('images') . "/no_image.jpg";}?>">
    </a>
    </div>
    <div class="span6">
          <h3><?=$row->title?></h3>
          <strong ><?=$row->location?> | <?=$row->category?></strong>
         <p><?=$row->use?></p>
       </div> 
  </div>
          <?php endforeach;?>

	</div>
	<div class="span2"><strong>Donation Amount</strong> 
	
			$<?=$items['price'];?>
			<input type="hidden" name="id<?=$i?>" value="<?=$items['id']?>" />
			<input type="hidden" name="rowid<?=$i?>" value="<?=$items['rowid']?>" />
			
			
			
			</div>


</div>
<hr/>
</div>
<?php $i++; ?>

<?php endforeach; 
	$grandtotal = $this->cart->total();
	$seedaccount = $this->Give_model->get_seed_account();
	$seedused = 0;
	if($seedaccount > $grandtotal){
		$seedused = $grandtotal;
		$grandtotal = 0;
		
	} else {
		$seedused = $seedaccount;
		
		$grandtotal = $grandtotal - $seedaccount; 
		$grandtotal = $grandtotal * 1.08;
	}
?>

				<input type="hidden" name="itemcount" value="<?=$i?>" />
<div class="row">
<div class="span5  php" rel="">
	<table style="width:100%; font-size:14px;">
		

<tr style="">
	<td>
		<strong>Total Donation Amount</strong>
	</td>
	<td>
		$<span id="finaltotal"><?php echo $this->cart->format_number($this->cart->total()); ?></span>
	</td>
</tr>
<?php if($seedaccount != 0):?>
		<tr>
			<td><strong> Total Deducted from Mustard Seed Account</strong> </td>
			<td>$<?=$seedused?></td>
		</tr>

<tr style="border-top:1px solid black;">
	<td>
		<strong> Donation Balance</strong>
	</td>
	<td>
		$<span id="finaltotal"><?php echo ($this->cart->total() - $seedused) ? $this->cart->format_number($this->cart->total() - $seedused) : '0.00' ?></span>
	</td>
</tr>

<tr>
	<td>
		<strong> Microseed Account Balance</strong>
	</td>
	<td>
		$<span id="finaltotal"><?php echo ( ($seedused - $this->cart->total() ) < 0) ? "0.00" : $this->cart->format_number($seedused - $this->cart->total()) ?></span>
	</td>
</tr>

<?php endif;?>	
	</table>
</div>

<div class="span5  offset1 total" rel="<?=$this->cart->total()?>">
	<table style="width:100%;">
	
	<?php if($this->cart->total() - $seedused) :?>
		<tr>
	<td>
		<strong> Checkout Balance</strong>
	</td>
	<td>
		$<span id="finaltotal"><?php echo $this->cart->format_number($this->cart->total() - $seedused)?></span>
	</td>
</tr>
	<?php endif;?>
	<?php if($grandtotal):?>
		<tr>
			<td><strong>PHP Administrative / Overhead (8%)</strong></td>
			<td>$<span id="phpcost"><?php echo $this->cart->format_number(($this->cart->total()- $seedused)*.08); ?></span><br /></td>
		</tr>
<?php endif;?>

	<tr style="border-top:2px solid black;">
	<td>
		<strong>Grand Total</strong>
	</td>
	<td>
		$<span id="finaltotal"><?php if ($grandtotal != 0) echo $this->cart->format_number($grandtotal); else echo '0 Remainding' ?></span>
	</td>
</tr>
</table>	
	<?php if($grandtotal  > 500):?>
<br />
 <a href="/give/complete" class="btn btn-large btn-primary"> Confirm Donation</a>
 <p>Please help us reduce our overhead cost by donating via check, Instructions on the next page</p>
<?php elseif ($grandtotal == 0):?>
 <a href="/give/complete" class="btn btn-large btn-primary"> Confirm Donation</a>
 <p>You agree to process this order from your seed account</p>

<?php else:?>

 <a href="/give/completePaypal" class="btn btn-large btn-warning"> Donate by Paypal</a>
 <p>You will be asked to confirm again before sending your donation to Paypal</p>
<?php endif;?>
</form>
</div>
</div>
<?php else:?>
<h2>Your Seed Cart is Empty</h2>
<p class="lead">You need to plant a seed by viewing the Applications to see them in your cart!</p>
<p><a href="/give/">Click here to view different Application that need your support</a></p>

<?php endif?>
</div>
