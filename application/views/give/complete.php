<div style="padding:10px">
<p>

	Thank you for planting a seed with People Helping People International, please visit your user profile to view the progress of the funding
	<br />
</p>
<p><ul>	
<strong>Step 1:</strong> Make the check payable to: <i>People Helping People International, Inc.</i><br />

<strong>Step 2:</strong> Complete the check with the following information:<br />
<p>	<ul>
Amount of: <strong>$<?=money_format('%(!#10n',$grandtotal)?> </strong><br /> 
Invoice number:<strong> #<?=$orderid?>	</strong>
</ul></p>
<strong>Step 3:</strong> Please print your email address that you use to log in to your Mustard Seed Account.<br />
<strong>Step 4:</strong>&nbsp;Mail the check to:<br /><ul><br />
 People Helping  People International, Inc<br />
 PO Box 243<br />
 Chautauqua, New York 14722<br />
</p>
</ul>
<br />
<p>*When  your check payment has been processed, we will credit your account and notify you by email. If you have any more additional questions or concerns please refer to our <a href="/pages/view/FAQ/">FAQ page</a></p>
</div>