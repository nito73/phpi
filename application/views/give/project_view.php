<?php foreach($query as $row):?>
<?php $percenttotal = floor(($total/$row->amount)*100);?>
    <div class="plantcart alert alert-success">
    Your seed has be planted for <strong><span id="projectname"></span></strong>, <a href="/give/cart">Click here</a> to view to finish planting
    </div>
  <div class="row" style="padding:10px;">
    <div class="span3">
    <a href="#" class="thumbnail">
      <img src="<?php if($row->pics != "") { echo $row->pics;} else { echo base_url('images') . "/no_image.jpg";}?>">
    </a>
    </div>
    
    <div class="span8">
    <a class="plantseedmodal btn btn-large btn-success pull-right" href="#" rel="<?=$row->id?>">Plant a Seed</a>
          <h1><?=$row->title?></h1>
          <strong>Location: </strong> <?=$row->country?> | <?=$row->location?> <br />
          <strong> Business sector: </strong> <?=$row->category?>
         
          
                   <dl class="dl-horizontal">
                   
		  <dt>Name</dt>
		  <dd><?=$row->name?></dd>
		  <dt>Posted Date</dt>
		  <dd><?=date('m/d/y' , strtotime($row->postdate))?></dd>
		  
		  <dt>Amount</dt>
		 
		 <?php if($total):?>
			  <dd>$<?=money_format('%(!#10n', $total)?> USD Funded / $<?=money_format('%(!#10n', $row->amount)?> USD Loan</dd>
		 <?php else:?>
		 	<dd>$<?=money_format('%(!#10n', $row->amount)?> USD</dd>
		 
		 <?php endif;?>
		  
		  
  		</dl>
           <?php if($percenttotal > "99"):?>This has been is Fully Funded<?php endif;?>
          <div class="progress">
	         
			<div class="bar" style="width: <?=$percenttotal?>%;"></div>
			   <label><?=$percenttotal?>% Funded </label>
		</div>
    </div>
    <div class="row">
	    <div class="span11"  style="padding-left:35px;">
	        <?php if($row->video):?>

	        <div class="tabbable"> <!-- Only required for left/right tabs -->
						<ul class="nav nav-tabs">
				<li class="active"><a href="#tab1" data-toggle="tab">Application Description</a></li>
				
				<li><a href="#tab2" data-toggle="tab">Video</a></li>
			</ul>
			<div class="tab-content">
		    	<div class="tab-pane active" id="tab1">
		    		<p><?=$row->description?></p>
			    </div>
			    <div class="tab-pane" id="tab2">
			    	<p><iframe width="400" height="225" src="http://www.youtube.com/embed/<?=$row->video?>?rel=0" frameborder="0" allowfullscreen></iframe> </p>
			    </div>
			</div>
	    </div>
	    
	    <?php else:?>
	    	<legend>Application Description</legend>
	    	<p><?=$row->description?></p>
	    <?php endif;?>
    </div>
</div>    
	
</div>


  </div>



    <div class="modal fade modal<?=$row->id;?> hide">
  <div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <h3>Plant A Seed <?=$row->title?></h3>
  </div>
  <div class="modal-body">
     <div class="row-fluid">
    <div class="span4">
    <img src="<?php if($row->pics != "") { echo $row->pics;} else { echo base_url('images') . "/no_image.jpg";}?>">
    </div>
    <div class="span7 loaninfo<?=$row->id;?>">
    	
    	 <?php if( $this->tank_auth->is_logged_in() && $seedaccount = $this->Give_model->get_seed_account()):?>
	   	<?php if($seedaccount != 0):?>
	   		 <strong>Currently in Seed Account<span style="float:right;">$<?=money_format('%(!#10n', $seedaccount)?></span></strong>	

		
	   	<?php endif;?>
	   <?php endif;?>

	   <?php if($total):?>
    	 <strong>Amount Raised <span style="float:right;">$<?=money_format('%(!#10n', $total)?></span></strong>
    	  <?php if($percenttotal > "99"):?>This has been is Fully Funded<?php endif;?>
			<div class="progress">
			      <div class="bar" style="width: <?=$percenttotal?>%;"></div>
					   <label><?=$percenttotal?>% Funded of $<?=$row->amount?></label>
			</div>
		 <strong>Amount Needed <span style="float:right;">$<?=money_format('%(!#10n', $row->amount-$total)?></span></strong>	
		<hr />
		<?php endif;?>
    	 <label>Select Seed Amount</label>
    	 <select class="itemamount" id="price<?=$row->id?>" name="price<?=$row->id?>">
				
				<?php for($x=25; $x < 525; $x = $x + 25):?>
				<option value="<?=$x?>" label="$<?=$x?>.00">$<?=$x?>.00</option>
				<?php endfor;?>
				
			</select> 
    </div>
    
    
     </div>
  </div>
  <div class="modal-footer">
    <a href="#" class="btn" data-dismiss="modal">Close</a>
    <a href="#" class="plantseed btn btn-success" rel="<?=$row->id?>">Plant Seed</a>
  </div>
</div>



<?php endforeach;?>


<script>
$(function(){
		$(".plantseedmodal").click(function(event){
		var projectid = $(this).attr('rel');
		$('.modal'+ projectid).modal('show');
		
	});
	
	$(".plantcart").hide();
	
		$(".plantseed").click(function(event){
	var plantseedbtn = $(this);
	var projectid = $(this).attr('rel');
	var amount = $("#price" + projectid).val();
	console.log(amount);
		$.ajax({
		  type: "POST",
		  url: '<?=base_url();?>give/plantseed',
		  data: { id: projectid, amount:amount},
		  dataType: "JSON",
		  success: function(data) { 
			  if(data.status == "error"){
				   $(".loaninfo" + projectid).append('<div class="alert alert-error">' + data.msg + '</div>');
				   $("#projectname").text(data.projectname);

				  
			  } else {
				  $(".loaninfo" + projectid).html('<div class="alert alert-success">This loan has been successfully added to your <a herf="<?=base_url()?>/give/cart" >Seeds</a></div>');
  			  	  $(plantseedbtn).remove();
				  updateCart();
				  $("#projectname").text(data.projectname);
		  	}
			
		}

		
		
		
	});
	
	});
	
});   
  
  


</script>