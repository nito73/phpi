<?php 
setlocale(LC_MONETARY, 'en_US');
$currency = $this->tank_auth->get_orgCurrency();

$grandratetotal = 0;
$grandtotalNGO = 0;
$grandtotalphp = 0;

?>
<?php $todayEx = $this->Report_model->get_rate($currency) . " $currency -> " . '$1 USD ';?>
	<?php $todayRate = $this->Report_model->get_rate($currency);?>
	
	
<style type="text/css">
	.total td{
		font-weight: bold;
	}
	img{ height: 200px; float: left;}
	table tr td{border: 1px solid black; }
	thead tr td{border:2px solid black; font-weight: bold; background-color: yellow; text-align: center;}
	.moneycell {text-align: right;}
	.infocell { text-align: center;}
	
	table{ border-collapse: collapse; }	
	.grandtotal td{
		font-weight: bold;
		font-size: 18px;
		
	}
	@media print {
    	.pagebreak {page-break-after: always;}
	}

</style>
<?php if ($startdate == $enddate):?>
<h1><?=$startdate?> Daily Report </h1>
<?php else:?>
<h1>Quarterly Summary Deposit Report <?=$startdate?> to <?=$enddate?> </h1>
<?php endif;?>
<?php if($fundingsource != "All"):?>
	<h2>Funding Source: <?=$fundingsource?></h2>
<?php endif;?>
<?php if($query):?>
<?php 
	$returngrandtotal = 0;
	$interestgrandtotal = 0;
	$principalgrandtotal =0;
	$grandtotal = 0;
	$grandtotaldue = 0;
	$granddeposittotal = 0;

	$flux_total = 0;
	
	//What is collected from NGO by Project
	$projectDue = array();	

	//if the is a Closed loan
	$closedLoan = FALSE;
	$closeProjects = array();
?>
<h3>
	<?=date("d M Y")?> Exchange Rate <br />
	
	
	<?=$todayEx?>
</h3>

	<table>
		<thead>
			<tr>
				<td>ID</td>
			
				<td>Name of Borrower</td>
				<td># of Payments Made</td>
				<td>Payments Remaining</td>
				<td>Collection Location</td>
				<td>Total Principal Due This Quarter <br />(<?=$currency?>) </td>
				
				<td>Total Principal Collected This Quarter <br />(<?=$currency?>)  </td>
				<td>% Principal Return  This Quarter</td>
				<td>Principal to be Deposited to PHP This Quarter <br /> (<?=$currency?>) </td>
				
			</tr>
		</thead>
		<tbody>
		
	<?php foreach($query as $row):?>
	<?php 
	//payments that needed to be made
	$totalPayments = $this->Report_model->total_client_repayment($row->projectid);

			//precent Due to PHP During Time period
?>
		<tr>
			<td><?=$row->client_id?></td>
			<td><?=$row->clientname?></td>
			<td class="infocell"><?=$row->totaltransactions?></td>
			
			
			<td class="infocell"><?=$row->loanterms - $totalPayments;?></td>
		
		
			<td><?=$row->collectionlocation?></td>
			<?php 

				//what is due 
				 $actualDue = ($this->Report_model->roundUpToAny($row->ngoamount/$row->loanterms))* $row->totaltransactions;
			?>

			<td class="moneycell" ><?=money_format('%!.0n', $actualDue)?></td>
				

				<?php 
					
					 $totalPayDue =0;
					 $totalNGOPrincipal = 0;

					 //what the payment is
					 $pricipalPayement = $row->ngoamount/$row->loanterms;


					$repayment = $this->Report_model->view_client_repayment($row->projectid, $startdate, $enddate);


					$currentlyDue = $row->ngoamount;

				
				//was the payment short
				$paymentShort =0;
				$ngointrest = (($row->loaninterest/100)*$row->ngoamount)*$row->loanterms;
				$ngointrestCollected=0;
				$paymentPrincipal =0;

						$ngointrestCollected;
						foreach ($repayment as $rerow) {

							if($rerow->closepayment == 1){
								$closedLoan = TRUE;
								$closeProjects[] = $rerow->clientid;

							}

							if($paymentShort){

								$ngointrestCollected += ((2*$row->loaninterest)/100) * $currentlyDue;
							} else {
								$ngointrestCollected += ($row->loaninterest/100) * $currentlyDue;
							}
						


						
							$ngointrest = ($row->loaninterest/100) * $currentlyDue;
							

						$paymentIntrestCollected = 0; 
				


				
					$curentPaymentDue = $ngointrest+$pricipalPayement;
				
					$totalPayDue += $pricipalPayement+$ngointrest;
					


						$paymentIntrest = $ngointrest;
					if($rerow->ngoamount < $curentPaymentDue){
						$paymentIntrestCollected = ($rerow->ngoamount/$curentPaymentDue) * $paymentIntrest;
						$paymentShort = $curentPaymentDue - $rerow->ngoamount ;
					} else {
						$paymentShort = 0;
						$paymentIntrestCollected = $paymentIntrest;
					}
					$paymentIntrestCollected = $this->Report_model->roundUpToAny($paymentIntrestCollected);

							// $paymentPrincipal = $rerow->ngoamount - $paymentIntrestCollected;
							
							// echo $rerow->projectid . ' ' . $rerow->ngoamount . ' ' . $paymentIntrestCollected;
							// echo '<hr>';

					//temp

							$paymentPrincipal = $actualDue;


							$paymentPrincipal = number_format($paymentPrincipal, 2, '.', '');
					
							$currentlyDue = $currentlyDue - $paymentPrincipal;


							// $totalNGOPrincipal += $paymentPrincipal;
							// 	$interestgrandtotal += $paymentIntrestCollected;

//temp
							$totalNGOPrincipal = $paymentPrincipal;
								$interestgrandtotal += $paymentIntrestCollected;
				
						}
					
						//principal collected from the ngo
					 $principalNGO = number_format($totalNGOPrincipal, 2, '.', '');

					 //percent of what is collected in decimal
					 $percent_collected = ($totalNGOPrincipal/$row->ngoamount);


					 //what is remaing what as deposited
					 $php_remainding_owe = ($row->amount*$percent_collected) - $row->phpdepositamount;

					 //what is the current remaing from ngo flat;
					 $ngo_remaing = ($row->ngoamount*$percent_collected)- $row->ngodepositamount;
					 
					 //flux time calculate if there is a flux change based on if the project is closed
					//convert was is owed to php info ngo currency
					 $php_remaing_NGO = $php_remainding_owe*$todayRate;
					

					 if ($ngo_remaing == $php_remaing_NGO){
					 	$flux = 0;
					 } else {
					 	$flux = $php_remaing_NGO-$ngo_remaing;

					 }

					 $flux = $flux/$todayRate;

					 	$flux = number_format($flux, 2, '.', '');

					 	$flux = $flux*$todayRate;

					 //logic is if close find differance else nothing


					
					 		$principalgrandtotal += $principalNGO;
					 		$granddeposittotal += $principalNGO;
					 
					//TOTAL DATA

					//NGO collected principal divided total to get the percent collected * the USD provided to the NGO * current exchange rate
				 	$convertedAmount = (($principalNGO/$row->ngoamount)*$row->amount)*$this->Report_model->get_rate($row->currency);
						

					//find out how much intrest was collected
					$interestNGO = ($row->loaninterest/100 ) * 	$row->ngototal; 
					
					//veriable was redundent on purpose since we use to collect the 1% from non PHP loans
					
					//find the diffrence of what was collect and what is do after today exchange rate
					$phpreturnammount = $convertedAmount - $principalNGO;
					
					//used to count totals
					$returngrandtotal = $returngrandtotal + $convertedAmount;
					$grandtotal = $grandtotal + $principalNGO;
					$grandratetotal = $grandratetotal + ($principalNGO - $convertedAmount);
					
		?>
				
		

			<td  class="moneycell" ><?php echo money_format('%!.0n', $principalNGO);?>  </td>	

			<td  class="infocell" ><?=number_format(($principalNGO/$row->ngoamount)*100, 1, '.', '')?></td>
			
			
			<td  class="moneycell" ><?php echo  money_format('%!.0n', $principalNGO)?> </td>		
			
		</tr>	
	
	<?php endforeach;?>
			<?php $grandtotalNGO = $grandtotal?>
				<tr class="grandtotal">
			<td colspan="8" style="text-align:right">Grand Total</td>
			
			<td  class="moneycell" ><?=money_format('%!.0n', $principalgrandtotal)?></td>

	

		</tr>
		</tbody>
	</table>
	
	
	<strong>Total Principal Collected This Quarter:</strong> <?=money_format('%!.0n', $principalgrandtotal)?> <?=$row->currency?> | <strong>Total Interest Collected This Quarter:</strong> <?=money_format('%!.0n', $interestgrandtotal)?> <?=$row->currency?> <br />
	
	<?php if($closedLoan):?>
<div class="pagebreak"></div>
<h1>Closed Loan Exchange Rate Fluctuation Adjustment</h1>
<h3>
	<?=date("d M Y")?> Exchange Rate <br />
	
	
	<?=$todayEx?>
</h3>

<?php 
	$returngrandtotal = 0;
	$interestgrandtotal = 0;
	$principalgrandtotal =0;
	$grandtotal = 0;
	$grandtotaldue = 0;
	$granddeposittotal = 0;

	$flux_total = 0;
		$flux_total_NGO = 0;
	
	//What is collected from NGO by Project
	$projectDue = array();	
?>


	<table>
		<thead>
		
		<tr>
			<td>ID</td>
			
			<td>Name of Borrower</td>
			<td>Total Principal Due Per Initial Exchange Rate<br /> (USD) </td>
			<td>Total Adjusted Principal Due Per Current Exchange Rate <br />(USD)</td>
			<td>Initial Exchange Rate </td>
			<td>Current Exchange Rate </td>
			<td>Total Adjustment <br />(USD)</td>
			<td>% of Total Principal Repaid <br />(<?=$currency;?>)</td>
			<td>Total Currency Exchange Rate Adjustment <br />(<?=$currency;?>)</td>
			
	</thead>
		<tbody>
		
	


	<?php foreach($closeProjects as $clientid):?>
		<?php 
		$clientinfo = $this->Client_model->viewClient($clientid);
		$clientloaninfo = $this->Client_model->view_client_loan($clientid);
		$clientLoanDetails = $this->Client_model->view_client_loan_payment($clientid);
			$closingtotals = $this->Report_model->closingtotals($clientid);
		?>

<tr>
		<td><?=$clientid?></td>
		<td><?=$clientinfo[0]->name?></td>

		<td class="moneycell" ><?=money_format('%(!#10n',$clientLoanDetails['unformated_total_principal'])?></td>
		<?php
			//flux
			$due =($clientLoanDetails['unformated_total_principal'])- $closingtotals->ngototalpayment;
			$principalgrandtotal += $due;
			$flux =(($due*$todayRate)-$clientLoanDetails['unformated_total_ngo_principal'])/$todayRate;
			
			$flux_NGO = $this->Client_model->roundUpToAny($flux*$todayRate);
			
			$flux_total += $flux;

			$flux_total_NGO += $flux_NGO;
				//nowrate 
				?>
				<td class="moneycell" ><?=money_format('%(!#10n', $due+$flux)?></td>

				<td class="moneycell"><?=number_format($clientloaninfo->rate, 1, '.', '')?> <?=$currency;?> -> $1 USD</td>
				<td class="moneycell"><?=number_format($todayRate, 1, '.', '')?> <?=$currency;?> -> $1 USD</td>

				<td class="moneycell"><?=money_format('%(!#10n', $flux)?></td>
				<td class="moneycell"><?php $percent_collected = (($closingtotals->clienttotalpayment - $clientLoanDetails['unformated_total_ngo_interest'])/$clientLoanDetails['unformated_total_ngo_principal'])*100;
						echo number_format($percent_collected, 1, '.', '');
				?></td>
				

				<td class="moneycell"><?=money_format('%!.0n', $flux_NGO)?></td>

			</tr>
	<?php endforeach;?>
				
		
	
			<?php $grandtotalNGO = $grandtotal?>
				<tr class="grandtotal">
			<td colspan="6" style="text-align:right">Grand Total</td>

			<td class="moneycell"><?=money_format('%(!#10n', $flux_total)?> </td>
			
			<td class="moneycell"></td>

			<td class="moneycell"><?=money_format('%!.0n', $flux_total_NGO)?></td>
			
	

		</tr>
		</tbody>
	</table>
	

<?php else:?>
	
<?php endif;?>
<?php endif;?>




<?php if($intial_query):?>
<h2>Initial Deposit</h2>
<?php 
	
	
	$grandfluxtotal = 0;
	$granddeposittotal = 0;
	?>
	Rate Differance is the % Return to the Current Exchange Rate
		<table>
		<thead>
			<tr>
				<td>ID</td>
			
				
				<td>Name of Borrower</td>
				<td>Rate on Loan Start</td>
				<td>Total Principal</td>
				<td>Total Principal Collected</td>
				<td>Total Principal Return %</td>
				<td>Today Exchange Rate</td>
				<td>Exchange Rate Fluctuation Deposit (PHP)</td>
				<td>Principal Deposited to PHP</td>
				<td>Deposited to NGO</td>

			</tr>
		</thead>
		<tbody>
		
	<?php foreach($intial_query as $row):?>
	
	<?php 
		
	$grandfluxtotal += $row->difference;
	$granddeposittotal += ($row->amount*$row->intial_rate)+$row->difference;
	?>
		<tr>	
			<td><?=$row->phpid;?></td>
			<td><?=$row->name;?></td>
			<td><?=$row->intial_rate;?> -> $1 USD</td>
			<td>N/A</td>
			<td>N/A</td>
			<td>N/A</td>
			<td><?=$todayEx?></td>
			<td><?php echo money_format('%(!#10n', $row->difference); ?> <?=$row->currency?>  				</td>	
			<td>N/A</td>
			<td><?php echo money_format('%(!#10n', ($row->amount*$row->intial_rate)+$row->difference);?> <?=$row->currency?> </td>	
		</tr>		 	
			

	<?php endforeach;?>
		<?php $grandtotalphp = $grandtotal;?>
			<tr class="grandtotal">
			<td colspan="7" style="text-align:right">Grand Total</td>
		
			<td><?=money_format('%(!#10n', $grandfluxtotal)?><?=$row->currency?></td>
			<td><?=money_format('%(!#10n', $granddeposittotal)?><?=$row->currency?></td>
			<td></td>

		</tr>
		</tbody>
	</table>

<?php endif;?>
Note: All values round up to the nearest 5 (<?=$currency?>) <br/>

<br />

<br />
