
<?php 
setlocale(LC_MONETARY, 'en_US');
$currency = $this->tank_auth->get_orgCurrency();

$grandratetotal = 0;
$grandtotalNGO = 0;
$grandtotalphp = 0;

?>
<?php $todayEx = $this->Report_model->get_rate($currency) . " $currency -> " . '$1 USD ';?>
	<?php $todayRate = $this->Report_model->get_rate($currency);?>
	
	
<style type="text/css">
	.total td{
		font-weight: bold;
	}
	img{ height: 200px; float: left;}
	 table tr td{border: 1px solid black; }
	.reportTable, thead tr td{border:2px solid black; font-weight: bold; background-color: yellow; text-align: center;}
	
	table{ border-collapse: collapse; }	
	.grandtotal td{
		font-weight: bold;
		font-size: 18px;
		
	}
</style>
<?php if ($startdate == $enddate):?>
<h1><?=$startdate?> Daily Report </h1>
<?php else:?>
<h1>Loan Repayment Report <?=$startdate?> to <?=$enddate?> </h1>
<?php endif;?>
<?php if($fundingsource != "All"):?>
	<h2>Funding Source: <?=$fundingsource?></h2>
<?php endif;?>
<?php if($query):
	
	$returngrandtotal = 0;
	$interestgrandtotal = 0;
	$principalgrandtotal =0;
	$grandtotal = 0;
	$grandtotaldue = 0;
	$granddeposittotal = 0;
	$projectPrincipal = 0;
	
?>
<h3>
	<?=date("d M Y")?> Exchange Rate <br />
	
	
	<?=$todayEx?>
</h3>


<?php foreach ($query as $row):?>
<?php 
	$project = $this->Report_model->view_client_loan($row->projectid);

	$repayment = $this->Report_model->view_client_repayment($row->projectid);

?>
<table>
		
			<tr>
				<td class="reportTable">Loan Number</td>
				<td><?=$project->id?></td>
			</tr>
			<tr>
				<td class="reportTable">Collection Location</td>
				<td><?=$project->location?></td>
			</tr>
			<tr>
				<td class="reportTable">Client Name</td>
				<td><?=$project->clientname?></td>
			</tr>
			<tr>
				<td class="reportTable">Monthly % Intrest Charge</td>
				<td><?=$project->loaninterest?> %</td>
			</tr>
			<tr>
				<td class="reportTable">Penalty Intrest  Charge</td>
				<td><?=$project->loaninterest*2?> %</td>
			</tr>
			<tr>
				<td class="reportTable">Loan Start Date</td>
				<td><?=$project->startdate?></td>
			</tr>
			<tr>
				<td class="reportTable">Loan 1st Payment Date</td>
				<td><?=$project->loandate?></td>
			</tr>
			<tr>
				<td class="reportTable">Posted Date</td>
				<td><?=$project->postdate?></td>
			</tr>
			<tr>
				<td class="reportTable">Begining Exachange Rate</td>
				<td>$1 USD -> <?=$project->rate?> <?=$currency?></td>
			</tr>
			<tr>
				<td class="reportTable">Total Loan Princial</td>
				<td><?php echo money_format('%(!#10n', $project->ngoamount)?><?=$currency?> / $ <?php echo money_format('%(!#10n', $project->amount)?> USD</td>
			</tr>
			<tr>
				<td class="reportTable">Monthly Princial Payment</td>
				<td><?php echo money_format('%(!#10n', $project->ngoamount/$project->loanterms)?><?=$currency?></td>
			</tr>
			<tr>
				<td class="reportTable">Total Loan Interest </td>
				<td><?php $ngointrest = (($project->loaninterest/100)*$project->ngoamount)*$project->loanterms;
					echo money_format('%(!#10n',$ngointrest);
				?> <?=$currency?></td>
			</tr>
			<tr>
				<td class="reportTable">Total Loan Amount</td>
				<td><?echo money_format('%(!#10n', $project->ngoamount+$ngointrest)?> <?=$currency?></td>
			</tr>
			<tr>				
				<td class="reportTable">Loan Term</td>
				<td><?=$project->loanterms?> Payments 
			</td>
			</tr>
			<tr>
				<td class="reportTable">Loan Closed Date</td>
				<td>

					<?php 
						if ($project->loantermstype == 4){
								$dateMath = $project->loanterms-1 . ' Months';

							} else {
								$dateMath = $project->loanterms . ' Weeks';
		
							}
						
						$paymentDueDate = date('Y-m-d', strtotime($project->loandate . " +" . $dateMath));
					?>
<?=date("Y-m-d", strtotime($project->loandate . "+" . $dateMath))?></td>
			</tr>
			
	

	</table>
<br />

<table>
		<thead>
			<tr>
				<td>Payment #</td>
			
				<td>Payment Due Date</td>
				<td>Date Paid</td>
				<td>Principal Balance Due</td>

				<td>Total Payment Amount Due (RWF)</td>
				<td>Total Payment Amount Collected (RWF)</td>
				<td>Total Interest Payment Amount (RWF)</td>
				<td>Total Interest Payment Amount Collected (RWF)</td>
				<td>Total Principal Amount (RWF)</td>
				<td>Total YTD Principal Payment Amount Collected (RWF)</td>
				<td>Total Quartly Principal Payment Deposit Required (RWF)</td>

				<td>% of Total RWF Principal Repaid</td>
				<td>Deposited to PHP Date</td>
				<td>Exchange Rate</td>
				
				<td>Total Quarterly Principal Payment Amount Due (USD)</td>
				<td>% of Total U.S. Principal repaid</td>

			</tr>
		</thead>
		<tbody>
			<?php 
				$totalNGOPrincipal = 0;;
				$totalPrincipal =0;
				$totalInterest =0;
				$totalInterestCollected=0;
				$totalPayDue =0;
				$totalCollected = 0;
				$quarterNGOTotal = 0;
				$querterDeposit =0;
				$paymentCounter = 0;


				//Closing Info
				//last date
				$closingDate;
				$lastExRate;

				$currentlyDue = $project->ngoamount;

				//pricipalPayement
				$pricipalPayement = $project->ngoamount/$project->loanterms;

				//was the payment short
				$paymentShort =0;


			foreach ($repayment as $rerow):
				//latepayment
				if($paymentShort){

				$ngointrest = ((2*$project->loaninterest)/100) * $currentlyDue;
			} else {
				$ngointrest = ($project->loaninterest/100) * $currentlyDue;
			}

							if ($project->loantermstype == 4){
								$dateMath = $paymentCounter . ' Months';

							} else {
								$dateMath = $project->loantermstype*$paymentCounter . ' Weeks';
		
							}
						
						$paymentDueDate = date('Y-m-d', strtotime($project->loandate . " +" . $dateMath));





				?>


		
			<tr>
				<td><?php echo $paymentCounter+1;?></td>
			
				<td><?=$paymentDueDate;?></td>
				<td><?php echo $rerow->date;
						$closingDate = $rerow->date;
				?></td>

				<td><?php echo money_format('%(!#10n', $currentlyDue)?><?=$currency?></td>
				<td><?php


					if($rerow->closepayment){
						$curentPaymentDue = $ngointrest+$currentlyDue;
						$totalPayDue += $currentlyDue+$ngointrest;
					
					} else {
					$curentPaymentDue = $ngointrest+$pricipalPayement+$paymentShort;
					$totalPayDue += $pricipalPayement+$ngointrest+$paymentShort;
					}
					
				echo money_format('%(!#10n',$curentPaymentDue)?>  <?=$currency?></td>
				
				<td><?php 
					
					$totalCollected += $rerow->ngoamount;
				echo money_format('%(!#10n',$rerow->ngoamount);?> <?=$currency?></td>
				
				<td><?php 
					$paymentIntrest = $ngointrest;
					$totalInterest += $paymentIntrest;
					 echo money_format('%(!#10n', $paymentIntrest);
					?> <?=$currency?></td>

					<td><?php
					$paymentIntrestCollected = 0; 
					if($rerow->ngoamount < $curentPaymentDue){
						$paymentIntrestCollected = ($rerow->ngoamount/$curentPaymentDue) * $paymentIntrest;
						$paymentShort = $curentPaymentDue - $rerow->ngoamount ;
					} else {
						$paymentShort = 0;
						$paymentIntrestCollected = $paymentIntrest;
					}

					$totalInterestCollected += $paymentIntrestCollected;
					 echo money_format('%(!#10n', $paymentIntrestCollected );
					


					?> <?=$currency?></td>

				<td><?php 

					$paymentPrincipal = $rerow->ngoamount - $paymentIntrestCollected;
					$quarterNGOTotal += number_format($paymentPrincipal,  2, '.', '');

					 echo money_format('%(!#10n', $paymentPrincipal);
					?> <?=$currency?></td>


				<td><?php $totalNGOPrincipal += $paymentPrincipal;
						 echo money_format('%(!#10n', $totalNGOPrincipal);
				?> <?=$currency?></td>
				<td><?php 
					//math to calculate what has been collected in percent;
					//needed if closing
					$totalNGOPrincipalPercent = $totalNGOPrincipal/$project->ngoamount*100;
				if(date("m", strtotime($rerow->date)) % 4 == 0 || $rerow->closepayment ):?>
			
						<?php 
							// if($rerow->closepayment){

							// //math
							// //NGO Deposit amount
							// // MATH : Amount (Closing Percentage collected) - Currently Deposited
							// $quarterNGOTotal =  $todayRate * (($project->amount*($totalNGOPrincipalPercent/100)) - $totalPrincipal);

							// } 

						echo money_format('%(!#10n', $quarterNGOTotal);	?> <?=$currency?>


				<?php endif;?>
			</td>
				<td><?php 
					if(date("m", strtotime($rerow->date)) % 4== 0 || $rerow->closepayment ){
						echo number_format($totalNGOPrincipalPercent, 3, '.', '') . ' %';
					}
					?>
			
				</td>
				<td>
				<?php 
				if(date("m", strtotime($rerow->date)) % 4== 0 || $rerow->closepayment ){
					if ($rerow->depositdate){
							echo $rerow->depositdate;			
					} else {
						echo "Not Deposited";
						}
				}
				?>
			</td>
						<td><?php 
					if(date("m", strtotime($rerow->date)) % 4== 0 || $rerow->closepayment ){
						$transRate = 0;
						if ($rerow->depositRate){
							$transRate = $rerow->depositRate;
						} else {
							$transRate = $todayRate;
						}
							echo "$transRate $currency ->" .'$1 USD';
							$lastExRate = "$transRate";
					}
				?>

			</td>
				<td> <?php 

				if (!$rerow->depositamount){

						$phpamount = $quarterNGOTotal/$todayRate;
					
				} else {
					if(!isset($phpamount)) {$phpamount=0;}
					$phpamount += $rerow->depositamount;
					$querterDeposit += $rerow->depositamount;

				
				}
						
					if(date("m", strtotime($rerow->date)) % 4== 0 || $rerow->closepayment ){
						if($rerow->closepayment){

							//math
							//PHP Amount (Closing Percentage collected) - Currently Deposited
							// $phpamount = ($project->amount*($totalNGOPrincipalPercent/100)) - $totalPrincipal;
							$phpamount = $quarterNGOTotal/$todayRate;
						} 
						

						$totalPrincipal += $phpamount;
						

						 echo '$' . money_format('%(!#10n', $phpamount) . 'USD' ;
					}

						$totalPrincipalPercent = $totalPrincipal/$project->amount*100;
				?></td>
	
				<td>
				<?php 
					if(date("m", strtotime($rerow->date)) % 4== 0 || $rerow->closepayment ){
						echo number_format($totalPrincipalPercent, 3, '.', '') . ' %';
					}
					?>

				</td>
				
		

			</tr>

				<?php 
					if(date("m", strtotime($rerow->date)) % 4 == 0 || $rerow->closepayment ){
						$quarterNGOTotal = 0;
						$quarterTotal = 0;
						//deposit based off per to get sum
						$querterDeposit =0;
					}
			?>
			<?php 
					$paymentCounter++;
					//update due
					$paymentPrincipal = number_format($paymentPrincipal, 2, '.', '');
					
					$currentlyDue = $currentlyDue - $paymentPrincipal;

					//removed the short logic;

			endforeach;?>
			<tr>
				<td colspan="3" class="reportTable" >Total</td>
				<td><?php  echo money_format('%(!#10n', $project->ngoamount);
					?> <?=$currency?></td>
				<td><?php  echo money_format('%(!#10n', $totalPayDue);
					?> <?=$currency?></td>
				<td><?php  echo money_format('%(!#10n', $totalCollected);
					?> <?=$currency?></td>
				
				<td><?php  echo money_format('%(!#10n', $totalInterest);
					?> <?=$currency?></td>
				<td><?php  echo money_format('%(!#10n', $totalInterestCollected);
					?> <?=$currency?></td>
				<td colspan="1"><?php echo money_format('%(!#10n', $totalNGOPrincipal);	?> <?=$currency?></td>
				<td colspan="1"><?php echo money_format('%(!#10n', $totalNGOPrincipal);	?> <?=$currency?></td>
				<td colspan="1"><?php echo number_format($totalNGOPrincipalPercent, 3, '.', '') . ' %';?>
				</td>
				<td></td>
				<td></td>
				<td colspan="1">$<?php echo money_format('%(!#10n', $totalPrincipal);?> USD</td>

				<td colspan="2">
					<?php echo number_format($totalPrincipalPercent, 3, '.', '') . ' %';?>

				</td>

			</tr>
		</tbody>

	</table>
	<br />
<div class="clearfix"></div>

<?php
endforeach;?>



<table>
	<thead>
		<tr>
			<td colspan="4">Closed Loan Exchange Rate Fluxation Adjustment</td>
		</tr>
		<tr>
			<td>Close Loan Date </td>
			<td>Total Loan Principal Amount Due USD </td>
			<td>% of Total Pricipal Repaid <?=$currency;?></td>
			<td>Total Adjust Principal Amount Due USD</td>
			<td>Total of Deposits Made USD</td>
			<td>Total Adjustment Amount USD</td>
			<td>Exhange Rate at Closing (<?=$currency;?>)</td>
			<td>Adjustment Amount Posted to Quarterly Loan Summary Report (<?=$currency;?>)</td>
			
	</thead>
	<tbody>
			<tr>
				<td><?=$closingDate;?> </td>
				<td>$ <?php echo  money_format('%(!#10n',$project->amount);?> USD</td>
				<td><?php echo number_format($totalNGOPrincipalPercent, 3, '.', '') . ' %';?></td>
				<td><?php $totalAdjustment = ($totalNGOPrincipalPercent/100)*$project->amount?>
						$ <?php echo  money_format('%(!#10n',$totalAdjustment);?> USD
				</td>
				<td>$<?php echo money_format('%(!#10n', $totalPrincipal);?> USD</td>
				<td>
					$<?php echo money_format('%(!#10n', $totalAdjustment-$totalPrincipal);?> USD
				</td>
				<td><?php echo "$lastExRate $currency -> " . '$1 USD';?> </td>
				<td>
						<?php
							
							$math =$totalPrincipal- $totalAdjustment;
								$math = number_format($math,2, '.', '')*$lastExRate;

								echo money_format('%(!#10n', $math ) . ' '. $currency;	
							?>

						</td>


				</td>

	</tbody>
</table>
<?php endif;?>

