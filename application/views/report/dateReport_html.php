<?php 
setlocale(LC_MONETARY, 'en_US');
$currency = $this->tank_auth->get_orgCurrency();
?>
<style type="text/css">
	.total td{
		font-weight: bold;
	}
	img{ height: 200px; float: left;}
	table tr td{border: 1px solid black; }
	thead tr td{border:2px solid black; font-weight: bold; background-color: yellow; }
	
	table{ border-collapse: collapse; }	
	.grandtotal td{
		font-weight: bold;
		font-size: 18px;
		
	}
</style>
<?php if ($startdate == $enddate):?>
<h1><?=$startdate?> Daily Report </h1>
<?php else:?>
<h1>Summary Report <?=$startdate?> to <?=$enddate?> </h1>
<?php endif;?>
<?php if($fundingsource != "All"):?>
	<h2>Funding Source: <?=$fundingsource?></h2>
<?php endif;?>
<?php if($query):
	
	$returngrandtotal = 0;
	$interestgrandtotal = 0;
	$principalgrandtotal =0;
	$grandtotal = 0;
	$grandtotaldue = 0;
	
?>
<h3>
	<?=date("d M Y")?> Exchange Rate <br />
	<?=$this->Report_model->get_rate($currency) . ' ' . $currency?> -> $1 USD 
</h3>

	<table>
		<thead>
			<tr>
				<td>ID</td>
			
				<td>Name of Applicant</td>
				<td># of Payments Made</td>
				<td>Payments Remaining</td>
				<td>Colletion Location</td>
				<td>Total Principal Due</td>
				<td>Total Principal Return</td>
				<?php if($fundingsource == "All"):?><td>Funding Source</td><?php endif;?>
			</tr>
		</thead>
		<tbody>
		
	<?php foreach($query as $row):?>
	<?php 

	//payments that needed to be made
				$paymentneedmade = 0;
				$loandate  = $row->loandate;
				
					for($x=1; $x < $row->loanterms+1; $x++){
						
						if((strtotime("$loandate +" . $x * $row->loantermstype . " Week" ) > strtotime($startdate)) && (strtotime("$loandate +" . $x * $row->loantermstype . " Week" ) <= strtotime($enddate))  ){
							$paymentneedmade++;
						}
					}
					
				?>
		<tr>
			<td><?=$row->id?></td>
			<td><?=$row->clientname?></td>
			<td><?=$row->totaltransactions?></td>
			
			
			<td><?=$row->loanterms - $row->totaltransactions?></td>
		
		
			<td><?=$row->collectionlocation?></td>

										<td><?php $totaldue = (($row->ngoamount * (($row->loaninterest)/100)+1)/$row->loanterms) * $paymentneedmade;
											$grandtotaldue = $grandtotaldue + $totaldue;
										?>
							<?=money_format('%(!#10n', $totaldue)?> <?=$row->currency?></td>
							<td><?=money_format('%(!#10n', $row->ngototal)?> <?=$row->currency?></td>
						
					<?php 
					
					if ($row->fundingsource == "PHP"){
				
					$principalNGO = ((100-$row->loaninterest)/100 ) * 	$row->ngototal; 
					// $convertedAmount = (($principalNGO/$row->ngoamount)*$row->amount)*$this->Report_model->get_rate($row->currency);
					$interestNGO = ($row->loaninterest/100 ) * 	$row->ngototal; 
					$principalCollected = ((100-$row->loaninterest)/100 ) * 	$row->ngototal; 

					} else {
						//1%
						$principalCollected = ((100-$row->loaninterest)/100 ) * 	$row->ngototal; 

						$principalNGO = .01 * 	$row->ngototal;
						$interestNGO = ($row->loaninterest/100 ) * 	$row->ngototal;  	
						 $convertedAmount = $principalNGO;
								}
					
					$returngrandtotal = $returngrandtotal + $principalNGO;
					//$returngrandtotal = $returngrandtotal + $convertedAmount;
					$principalgrandtotal+= $principalCollected;
					$interestgrandtotal += $interestNGO;
					$grandtotal = $grandtotal + $row->ngototal;
					?>
					 <?php //this is whats returned before
					 //	echo money_format('%(!#10n', $principalNGO) . ' ' . $row->currency?>
						
						
					
			
			
			<?php if($fundingsource == "All"):?><td><?=$row->fundingsource?></td><?php endif;?>
		</tr>	
	
	<?php endforeach;?>
			<?php $grandtotalNGO = $grandtotal?>
				<tr class="grandtotal">
			<td colspan="5" style="text-align:right">Grand Total</td>
			<td><?=money_format('%(!#10n', $grandtotaldue)?> <?=$row->currency?></td>

			<td colspan="1"><?=money_format('%(!#10n', $grandtotal)?> <?=$row->currency?></td>
	

		</tr>
		</tbody>
	</table>
	
	
	<strong>Principle Collected:</strong> <?=money_format('%(!#10n', $principalgrandtotal)?> <?=$row->currency?> <br />
	<strong>Interest Collected:</strong> <?=money_format('%(!#10n', $interestgrandtotal)?> <?=$row->currency?> <br />
	
	
	<?php 
	
		$returngrandtotal = 0;
	$interestgrandtotal = 0;
	$principalgrandtotal =0;
	$grandtotal = 0;
	$grandtotaldue = 0;
	$grandratetotal = 0;
	
	?>
	Rate Differance is the % Return to the Current Exchange Rate
		<table>
		<thead>
			<tr>
				<td>ID</td>
			
				<td>Name of Applicant</td>
				<td>Total Amount Return %</td>
				<td>Total Amount Return</td>
				<td>Rate on Loan Start</td>
				<td>Rate Differance</td>
				<td>Amount Deposited to PHP</td>

				<?php if($fundingsource == "All"):?><td>Funding Source</td><?php endif;?>
			</tr>
		</thead>
		<tbody>
		
	<?php foreach($query as $row):?>
	<?php 

	//payments that needed to be made
				$paymentneedmade = 0;
				$loandate  = $row->loandate;
				
					for($x=1; $x < $row->loanterms+1; $x++){
						
						if((strtotime("$loandate +" . $x * $row->loantermstype . " Week" ) > strtotime($startdate)) && (strtotime("$loandate +" . $x * $row->loantermstype . " Week" ) <= strtotime($enddate))  ){
							$paymentneedmade++;
						}
					}
					
				?>
		<tr>
			<td><?=$row->id?></td>
			<td><?=$row->clientname?></td>
			<?php 
					
					if ($row->fundingsource == "PHP"){
				//if php funded takes what was desposited and gets the principal collected
					 $principalNGO = ((100-$row->loaninterest)/100 ) * 	$row->ngototal; 
					 $convertedAmount = (($principalNGO/$row->ngoamount)*$row->amount)*$this->Report_model->get_rate($row->currency);
					$interestNGO = ($row->loaninterest/100 ) * 	$row->ngototal; 
					$principalCollected = ((100-$row->loaninterest)/100 ) * 	$row->ngototal; 

					
						$phpreturnammount = $convertedAmount - $principalNGO;
					$totaldue = $row->ngototal + ($phpreturnammount);
										
					
					} else {
						//1%
						$principalCollected = ((100-$row->loaninterest)/100 ) * 	$row->ngototal; 

						$principalNGO = .01 * 	$row->ngototal;
						$interestNGO = ($row->loaninterest/100 ) * 	$row->ngototal;  	
						 $convertedAmount = $principalNGO;
						 	$totaldue = $principalNGO;
						 	$phpreturnammount = 0;
					}
					
					$returngrandtotal = $returngrandtotal + $convertedAmount;
					$principalgrandtotal+= $principalCollected;
					$interestgrandtotal += $interestNGO;
					$grandtotal = $grandtotal + $row->ngototal  + ($convertedAmount - $principalNGO);
					$grandratetotal = $grandratetotal + ($convertedAmount - $principalNGO);
					?>
					 <?php //this is whats returned before
					 	//echo money_format('%(!#10n', $principalNGO) . ' ' . $row->currency . '/'?>
					<?php 
										
										?>
				<td><?=number_format(($principalNGO/$row->ngoamount)*100, 2, '.', '')?> %</td>
					
					<td><?=money_format('%(!#10n', $row->ngototal)?> <?=$row->currency?></td>
					<td><?=$row->rate;?> -> $1 USD</td>					
							
					<td>					 
					 <?php
					 	if ($phpreturnammount == 0){
						 	echo "N/A";
					 	} else {
							echo money_format('%(!#10n', $phpreturnammount) . ' ' . $row->currency; 
						}
					 ?>  				</td>

					<td>					 
					 <?php
						echo money_format('%(!#10n', $totaldue); 
					 ?> <?=$row->currency?>  				</td>
					
					
					<?php if($fundingsource == "All"):?><td><?=$row->fundingsource?></td><?php endif;?>

					
					
						
		</tr>	
	
	<?php endforeach;?>
		<?php $grandtotalphp = $grandtotal;?>
			<tr class="grandtotal">
			<td colspan="5" style="text-align:right">Grand Total</td>
			<td><?=money_format('%(!#10n', $grandratetotal)?><?=$row->currency?></td>
			<td colspan="2"><?=money_format('%(!#10n', $grandtotal)?> <?=$row->currency?></td>
			

		</tr>
		</tbody>
	</table>
	
<?php else:?>
	<?php if ($startdate == $enddate):?>
	<p>No Loan Transactions for  <?=$startdate?> </p>
<?php else:?>
	<p>No Loan Transactions for  <?=$startdate?> to <?=$enddate?></p>
	<?php endif;?>
<?php endif;?>
<br />

<?php if($savingsquery):
	$totalDeposit = 0;
	$totalWithdraw = 0;
	
?>

	<h2>Savings Transaction</h2>

	<table>
		<thead>
			<tr>
				<td>ID</td>
			
				<td>Name of Applicant</td>
				<td># of Transactions Made</td>
				<td>Total</td>
			</tr>
		</thead>
		<tbody>
		
	<?php foreach($savingsquery as $row):?>

		<tr>
			<td><?=$row->id?></td>
			<td><?=$row->clientname?></td>
			<td> <?=$row->type?> (<?=$row->totaltransactions?>)</td>
			<?php 
				if($row->type == "Deposit"){
					$totalDeposit += $row->total;
					
				} else {
					$totalWithdraw += $row->total;
				}
				$currency =$row->currency;
			?>
			<td><?=money_format('%(!#10n',$row->total)?> <?=$row->currency?></td>
			
				</tr>	
	
	<?php endforeach;?>
		
		</tbody>
	</table>
	
	<strong>Deposit Collected:</strong> <?=money_format('%(!#10n', $totalDeposit)?> <?=$currency?> <br />
	<strong>Withdraw Collected:</strong> <?=money_format('%(!#10n', $totalWithdraw)?> <?=$currency?> <br />
	
	
	
<?php else:?>
		<?php if ($startdate == $enddate):?>
	<p>No Savings Transactions for  <?=$startdate?> </p>
<?php else:?>
	<p>No Savings Transactions for  <?=$startdate?> to <?=$enddate?></p>
	<?php endif;?>

<?php endif;?>
<br />
<?php if($YTDQuery):?>
<h2>Year to date Info </h2>
<?php foreach($YTDQuery['savingQuery'] as $savingRow):?>
	<strong> Total YTD <?=$savingRow->type?>: </strong> <?=money_format('%(!#10n',$savingRow->total)?> <?=$currency?> <br />
<?php endforeach;?>

<?php

	$totalInterest=0;
	$totalPrincipal=0;
 foreach($YTDQuery['loanQuery'] as $loanRow):?>

	<?php
		$interest = $loanRow->loaninterest/100;
		$projectInterest = $interest * $loanRow->ngototal;
		$projectPrincipal = $loanRow->ngototal - $projectInterest;
		
		$totalInterest += $projectInterest;
		$totalPrincipal += $projectPrincipal;
		
		
	?>
<?php endforeach;?>
	<strong> Total YTD Interest Collected: </strong> <?=money_format('%(!#10n',$totalInterest)?> <?=$currency?> <br />

		<strong> Total YTD Principal Collected: </strong> <?=money_format('%(!#10n',$totalPrincipal)?> <?=$currency?> <br />
	
<?php endif;?>
