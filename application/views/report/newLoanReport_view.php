<?php 
setlocale(LC_MONETARY, 'en_US');
//$currency = $this->tank_auth->get_orgCurrency();


function roundUpToAny($n,$x=5) {
    return (ceil($n)%$x === 0) ? ceil($n) : round(($n+$x/2)/$x)*$x;
}

$grandratetotal = 0;
$grandtotalNGO = 0;
$grandtotalphp = 0;
//loan process
$loanstartPHP = 298.50;
$loanstartRATE = 670;

$loanAmount = 200000;
$loanDate = "2014-05-14";
$paymentsMade[1] = array('date' => '2014-06-14',	'payment' =>'21670');
$paymentsMade[2] = array('date' => '2014-07-14',	'payment' =>'21255');
$paymentsMade[3] = array('date' => '2014-08-14',	'payment' =>'20840');
$paymentsMade[4] = array('date' => '2014-09-14',	'payment' =>'20420');
// $paymentsMade[5] = array('date' => '2013-02-14',	'payment' =>'30000');
// $paymentsMade[6] = array('date' => '2014-03-14',	'payment' =>'3550');
// $paymentsMade[7] = array('date' => '2014-04-14',	'payment' =>'87250');
// // $paymentsMade[7] = array('date' => '2014-04-14',	'payment' =>'78529');
// $paymentsMade[8] = array('date' => '2014-05-14',	'payment' =>'27500');
// $paymentsMade[9] = array('date' => '2014-06-14',	'payment' =>'26875');
// $paymentsMade[10] = array('date' => '2014-07-14',	'payment' =>'26250');
// $paymentsMade[11] = array('date' => '2014-08-14',	'payment' =>'25390');

$loanAmountDue = $loanAmount;
$paymentTerms = 12;

$payment = $loanAmount/$paymentTerms;
$counter = 0;

$totalPrincipal = 0;
$totalInterest = 0;
$grandTotal = 0;
$owe = 0;
$totalOwe = 0;
$totalPaid = 0;
$totalPaidInterest = 0;
$totalPaidPayment = 0;
$fee =0;

$todayEX = 690;

$currency = 'RWF';

?>
<style type="text/css">
	/*.jan, .jan td{
		background-color: red;
	}*/
		table tr td{border: 1px solid black; text-align: right; }
	thead th, .reportTable {border:2px solid black; font-weight: bold; background-color: yellow; }
	
	table{ border-collapse: collapse; }	
	.grandtotal td{
		font-weight: bold;
		font-size: 18px;
		
	}
</style>

<h2>Sacco Borrower Repayment Report </h2>

<table>
	
		
			<tr>
				<td class="reportTable">Loan Number</td>
				<td>50</td>
			</tr>
			<tr>
				<td class="reportTable">Collection Location </td>
				<td>Rusororo Sacco</td>
			</tr>
			<tr>
				<td class="reportTable">Client Name</td>
				<td>NGENGIMAMA ISMAIL</td>
			</tr>
		
	
		
			<tr>
				<td class="reportTable">Monthly Interest Percent</td>
				<td>2.5</td>
			</tr>
			<tr>
				<td class="reportTable">Penalty Interest Percent </td>
				<td>10</td>
			</tr>
			<tr>
				<td class="reportTable">Loan Start Date</td>
				<td><?=$loanDate?></td>
			</tr>
			<tr>
				<td class="reportTable">Loan 1st Payment Date</td>
				<td><?=date("Y-m-d", strtotime($loanDate . "+1 month"))?></td>
			</tr>
		
			<tr>
				<td class="reportTable">Beginning Exchange Rate</td>
				<td>$1 USD -> <?=$loanstartRATE?> <?=$currency?></td>
			</tr>
			<tr>
				<td class="reportTable">Total Loan <?=$currency?> </td>
				<td><?php echo money_format('%(!#10n', $loanAmount)?></td>
			</tr>
			<tr>
				<td class="reportTable">Total Loan USD</td>
				<td>  <?php echo money_format('%(!#10n', $loanstartPHP)?></td>
			</tr>
			<tr>
				<td class="reportTable">Monthly Principal Payment <?=$currency?></td>
				<td><?php echo money_format('%(!#10n', $payment)?></td>
			</tr>
			
		
			<tr>				
				<td class="reportTable">Loan Term (Payments) </td>
				<td>12 </td>
			</tr>
		
	

	</table>
<br />
<br />

<table>
	<thead>
		<th>Payment Number </th>
		<th>Payment Due Date</th>
		<th>Date Paid</th>
		<th>Principal Balance Remaining (<?=$currency?>)</th>
		<th>Principal Due (Includes Late Fees) (<?=$currency?>)*</th>
		<th>Principal Paid (Includes Late Fees) (<?=$currency?>)</th>
		<th>Interest Due (<?=$currency?>)*</th>
		<th>Interest Paid (<?=$currency?>)</th>
		<th>Total Due (<?=$currency?>)</th>
		<th>Total Paid (<?=$currency?>)</th>
		<th>Late Total (<?=$currency?>)</th>
		<th>% of Total Principal Repaid</th>
		<th>Deposited to PHP Date</th>
		<th>Current Exchange Rate to USD</th>
		<th>Total Quarterly Principal Amount Due (USD)</th>
		<th>% of USD Principal Repaid</th>
	</thead>
	<?php while($loanAmountDue > 0):?>
	<?php $counter ++;
	
		$date = date('Y-m-d', strtotime($loanDate . "+ $counter Month"));

	?>

		<?php if(isset($paymentsMade[$counter] ) ):
					//thePayment
			$thePayment = $payment+$owe;//what should be paid

			$paid = $paymentsMade[$counter]['payment'];
			$totalPaid += $paid;
			 $paymentInterest = roundUpToAny($loanAmountDue * 0.025);
		?>

		<tr <?php if (date('m', strtotime($date)) == '01'):?> class="jan" <?php endif;?> >
		<td><?php echo $counter;?></td>
		
		<td><?php echo $date?></td>
		<td><?php echo $paymentsMade[$counter]['date'] ?></td>

		<td><?php echo money_format('%(!#10n', roundUpToAny($loanAmountDue));?></td>
		<td><?php echo money_format('%(!#10n', roundUpToAny($payment+$owe));?></td>
		<td><?php 
			if($paid >= $payment+$owe+$paymentInterest){
					$paidPayment = $paid - $paymentInterest;
				} else {
					$paidPayment = ($paid - $paymentInterest) - ($payment+$owe) ;
					if($paidPayment < 0){
						$paidPayment = 0;
					}
				}
				$totalPaidPayment += $paidPayment;
		echo money_format('%(!#10n', roundUpToAny($paidPayment));?></td>
		<td><?php $totalInterest += $paymentInterest;
			echo money_format('%(!#10n', $paymentInterest);?></td>
			<td><?php 
				if($paid > $paymentInterest){
					$paidPaymentInterest = $paymentInterest;
				} else {
					$paidPaymentInterest = $paid;
				}
				$totalPaidInterest += $paidPaymentInterest;
			echo money_format('%(!#10n', $paidPaymentInterest);?></td>
		<td><?php 
			$total = roundUpToAny($payment +$owe +  $paymentInterest);

		echo money_format('%(!#10n', $total);
		?>
	</td>
	<td><?php 
			$total = $paymentsMade[$counter]['payment'];

		echo money_format('%(!#10n', $total);
			$grandTotal += $total;
		?>
	</td>
	
		<?php 


		//latepayment
			if(strtotime($paymentsMade[$counter]['date']) < strtotime($date . ' + 5 Days')){

				if ($paid >= $payment+$owe+$paymentInterest){
						if($owe > 0){
							$owe = $paid - roundUpToAny($payment+$paymentInterest+$owe);

						} 

						 if ($payment+$paymentInterest+$owe < $paid){
							$owe = 0;
						}


						
					echo "<td> --- </td>";
						$PPaid = $paid - $paymentInterest;
					$loanAmountDue = $loanAmountDue - $PPaid;
				} else {
						//paying intrest first
						if($paymentInterest < $paid){ 
						$paid = $paid - $paymentInterest;
				}  else {
						$paid = $paymentInterest -$paid;
					
				}		$owe= ($owe+$payment);
						$fee = roundUpToAny($owe*.10 + $paid);
						$owe = $owe + $fee;
						// $owe = roundUpToAny($owe + $fee, -1, PHP_roundUpToAny_HALF_UP);
					echo "<td>$fee <small>10% late fee added </small></td>";

					$loanAmountDue = $loanAmountDue - $paid ;
				}
			}else {

				//late 10%fee
					if ($paid >= $payment+$paymentInterest){
					$loanAmountDue = $loanAmountDue - $payment;
					$fee = $payment*1.1+ $paymentInterest;
					$fee = money_format('%(!#10n', $fee);
					echo "<td>" . $fee . " <small>10% added to the principal </small></td>";

				} else {
						//paying intrest first 
						if($paid == 0){
						$owe= ($payment+$owe);
						$fee = ($owe*.10) + $paymentInterest;
						$owe = roundUpToAny($owe + $fee);

						}//nothing was paid 
						else {
						$paid = $paid - $paymentInterest;
						$owe= ($payment+$owe) - $paid;
						$fee = $owe*.10;
						$owe = roundUpToAny($owe + $fee);

						}
					echo "<td>$owe <small>10% late fee added </small></td>";

					$loanAmountDue = $loanAmountDue - $paid ;
				}
			}
			$totalOwe += $fee;
			$fee = 0;
					$totalPrincipal += roundUpToAny($payment+$owe);

			?>
		<td><?php echo number_format(($totalPrincipal/$loanAmount)*100,1);?></td>
		<?php if (date('m', strtotime($date)) % 3 == 0):?>
		<td>------</td>
		<td>690</td>
		<td><?php 

			$paymentPaidUSD = ($totalPrincipal/690)-$grandtotalphp;
			$grandtotalphp += $paymentPaidUSD;
			 echo money_format('%(!#10n', $paymentPaidUSD);?></td>
		<td><?php echo number_format(($paymentPaidUSD/$loanstartPHP)*100,1);?></td>
	<?php else:?>
		<td>------</td>
		<td>------</td>
		<td>------</td>
		<td>------</td>
	<?php endif;?>
			</tr>
	<?php else:?>
		
		<tr <?php if (date('m', strtotime($date)) == '01'):?> class="jan" <?php endif;?> >
		<td><?php echo $counter;?></td>

		<td><?php echo $date?></td>
		<td>N/A</td>
		<td><?php echo money_format('%(!#10n', roundUpToAny($loanAmountDue));?></td>
		<td>
			<?php //lastpayment
				if($loanAmountDue < $payment){
					$payment = $loanAmountDue;
				}?>
			<?php echo money_format('%(!#10n', roundUpToAny($payment+$owe));
			$totalPrincipal += roundUpToAny($payment+$owe);
		?></td>
	<td> --- </td>

		<td><?php $paymentInterest = roundUpToAny($loanAmountDue * 0.025);
			$totalInterest += $paymentInterest;
			echo money_format('%(!#10n', $paymentInterest);?></td>
	<td> --- </td>
		
		<td><?php 
			$total = roundUpToAny($payment+$owe + $paymentInterest);

		echo money_format('%(!#10n', $total);
		$grandTotal += $total;?>
	</td>
	<td> --- </td>
	<td> --- </td>
	<td> --- </td>
	<td> --- </td>
	<td> --- </td>
	<td> --- </td>
	<td> --- </td>
	</tr>
		<?php $loanAmountDue = $loanAmountDue - $payment;?>
<?php endif;?>
	<?php endwhile;?>
		
		<tr class="reportTable" style="vertical-align: text-bottom;">
		<td colspan="3">Grand Total</td>
		
		<td><?php echo money_format('%(!#10n', $totalPrincipal);?></td>
		<td><?php echo money_format('%(!#10n', $totalPrincipal);?></td>
		<td><?php echo money_format('%(!#10n', $totalPaidPayment);?></td>
		<td><?php echo money_format('%(!#10n', $totalInterest);?></td>
		<td><?php echo money_format('%(!#10n', $totalPaidInterest);?></td>
		<td><?php echo money_format('%(!#10n', $grandTotal);?></td>
		<td><?php echo money_format('%(!#10n', $totalPaid);?></td>
		<td><?php echo money_format('%(!#10n', $totalOwe);?></td>
		<td> --- </td>
		<td> --- </td>
	<td> --- </td>
	<td> --- </td>
	<td><?php echo money_format('%(!#10n', $grandtotalphp);?></td>
	</tr>
</table>
<br/>
* Rounds up to the nearest 5 (<?=$currency?>) <br/>
Note: Grand Total affected by rounding.