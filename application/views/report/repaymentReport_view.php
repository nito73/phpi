<?php
/**********************************
	Edited: 03SEPT2015
	Dev: William Essig
	Notes:
		Code Clean Up
**********************************/
	//echo $projectid.'<br />';
	$project = $this->Report_model->view_client_loan($projectid);
	$repayment = $this->Report_model->view_client_repayment($projectid); // Needs error catching for missing rows in TABLE: repayment
	$clientPayment = $this->Client_model->view_client_loan_payment($project->client_id);


	setlocale(LC_MONETARY, 'en_US');
	if($this->tank_auth->get_usertype() != "Admin"){
		$currency = $this->tank_auth->get_orgCurrency();
	} else {
		$currency = $project->currency;
	}
	$todayEx = $this->Report_model->get_rate($currency)." $currency -> ".'$1 USD ';
	$todayRate = $this->Report_model->get_rate($currency);
?>
<style type="text/css">
	.total td{font-weight:bold;}
	img{ height: 200px;float:left;}
	table tr td{border:1px solid black;text-align: right;}
	.reportTable, thead tr td{border:2px solid black;font-weight:bold;background-color:yellow;text-align:center;}
	.infocell{text-align:center;}
	table{border-collapse:collapse;}	
	.grandtotal td{font-weight:bold;font-size:18px;}
</style>
<?php
	$returngrandtotal = 0;
	$interestgrandtotal = 0;
	$principalgrandtotal =0;
	$grandtotal = 0;
	$grandtotaldue = 0;
	$granddeposittotal = 0;
	$projectPrincipal = 0;
?>
<h1>Rusororo SACCO  - Borrower's Loan Repayment Report - 
	<?=date("d M Y")?> - Exchange Rate - <?=$todayEx?>
</h1>
<table>
	<tr>
		<td class="reportTable">Loan Number</td>
		<td><?=$project->id?></td>
		<td class="reportTable">Loan Start Date</td>
		<td><?=$project->startdate?></td>
		<td class="reportTable">Total Loaned (USD)</td>
		<td><?php echo money_format('%!.0n', $project->amount)?></td>
		<td class="reportTable">Monthly Principal Payment (<?=$currency?>)</td>
		<td><?php echo money_format('%!.0n', $project->ngoamount/$project->loanterms)?></td>
	</tr>
	<tr>
		<td class="reportTable">Client Name</td>
		<td><?=$project->clientname?></td>
		<td class="reportTable">Loan Close Date</td>
		<td>
			<?php
				if ($project->loantermstype == 4){
					$dateMath = $project->loanterms-1 . ' Months';
				} else {
					$dateMath = $project->loanterms . ' Weeks';
				}
				$paymentDueDate = date('Y-m-d', strtotime($project->loandate . " +" . $dateMath));
			?>
			<?=date("Y-m-d", strtotime($project->loandate . "+" . $dateMath))?>
		</td>
		<td class="reportTable">Total Loaned (<?=$currency?>)</td>
		<td><?php echo money_format('%!.0n', $project->ngoamount)?></td>
		<td class="reportTable">Monthly Interest %</td>
		<td><?=$project->loaninterest?></td>
	</tr>
	<tr>
		<td class="reportTable">Collection Location</td>
		<td><?=$project->location?></td>
		<td class="reportTable">Posted to PHP Date</td>
		<td><?=$project->postdate?></td>
		<td class="reportTable">Estimated Total Loan Interest </td>
		<td>
			<?php
				echo money_format('~%!.0n',$clientPayment['unformated_total_ngo_interest']);
			?> <?=$currency?>
		</td>
		<td class="reportTable">Beginning Exchange Rate</td>
		<td><?=number_format($project->rate, 1, '.', '')?> <?=$currency?> -> $1 USD</td>
	</tr>
	<tr>
		<td class="reportTable">Loan Term</td>
		<td><?=$project->loanterms?>  </td>
		<td class="reportTable">Loan 1st Payment Date</td>
		<td><?=$project->loandate?></td>
		<td class="reportTable">Estimated Total Loan Amount (P+I)</td>
		<td><?echo money_format('~%!.0n', $project->ngoamount+$clientPayment['unformated_total_ngo_interest'])?> <?=$currency?></td>
		<td class="reportTable">Penalty Interest % </td>
		<td><?=$project->lateamount?></td>
	</tr>
</table>
<br />
<table>
	<thead>
		<tr>
			<td>Payment #</td>
			<td width="10%" style="width:10%">Payment Due Date</td>
			<td width="10%" style="padding:10px;">Date Paid</td>
			<td>Principal Balance Remaining (<?=$currency;?>)</td>
			<td>Principal Due (<?=$currency;?>) </td>
			<td>Principal In Arrears (<?=$currency;?>) </td>
			<td>Principal Paid (<?=$currency;?>) </td>
			<td>Interest Due (<?=$currency;?>) </td>
			<td>Interest In Arrears (<?=$currency;?>) </td>
			<td>Interest Paid (<?=$currency;?>) </td>
			<td>Total Payment Due (<?=$currency;?>) </td>
			<td>Total Payment Made (<?=$currency;?>) </td>
			<td>Penalty Fee + Int. in Arrears (<?=$currency;?>) </td>
			<td>% Total Principal Repaid (<?=$currency;?>)</td>
			<td>Deposited to PHP Date</td>
			<td>Total Quarterly Principal Payment Amount Due (<?=$currency;?>)</td>
			<td>Exchange Rate to USD</td>
			<td>Total Quarterly Principal Payment Amount Due (USD)</td>
			<td>% USD Principal Repaid</td>
		</tr>
	</thead>
	<tbody>
		<?php 
			$totalPrincipalDue=0;
			$totalPrincipalArrears = 0;
			$totalPrincipalPaid=0;
			$totalInterestDue=0;
			$totalInterestArrears=0;
			$totalInterestPaid =0;
			$grandtotalDue = 0;
			$grandtotalPaid = 0;
			$totalFees = 0;
			$totalRepaidPercent_ngo = 0;
			$totalQtrDeposit =0;
			$totalQtrDepositNGO = 0;
			$totalRepaidPercent_php =0;
			//Closing Info
			//last date
			$closingDate;
			$lastExRate;
			//was the payment short
			$counter=0;
			$owe=0;
			$oweRow=0;
			$principalPaidRow = 0;
			$principalPaidPeriod = 0;
			$feeRow=0;
			$paymentA = 0;
			$principalPaidPHP =0;
			// Error: Needs error catching
			// Probably better off encasing entire foreach within IF/ELSE statement
			if(!$repayment){$arrisnil=true;}else{$arrisnil=false;}
			if(!$arrisnil){ // Encased - Problem patched (for now)
			foreach ($repayment as $row):
				$payment = $clientPayment[$counter];
				$closingDate = date('d M y', strtotime($payment['date']));
				//echo $closingDate.'<br />';
		?>
		<tr>
			<td class="infocell" ><?=$counter+1;?></td>
			<td><?=date('d M y', strtotime($payment['date']))?></td>
			<td><?=date('d M y', strtotime($row->date))?></td>
			<td><?=money_format('%!.0n', $project->ngoamount-$principalPaidRow)?>
				<?php $decliningBalance = $project->ngoamount-$principalPaidRow;?>
			</td>
			<td>
				<?php
					if($payment['unformated_ngo_principal']+$oweRow > $decliningBalance ){
						$payment['unformated_ngo_principal'] = $decliningBalance;
					}
					echo money_format('%!.0n', $payment['unformated_ngo_principal']+$oweRow);
					$principalRow =  $payment['unformated_ngo_principal']+$oweRow;
					$totalPrincipalDue += $payment['unformated_ngo_principal']+$oweRow;
				?>
			</td>
			<?php $intrestRow = $this->Client_model->roundUpToAny($decliningBalance*($project->loaninterest/100));?>
			<td>
				<?php
					if( $row->ngoamount < ($payment['unformated_ngo_payment']+$oweRow) && $row->ngoamount != ($payment['unformated_ngo_payment']+$oweRow)){
						if($row->ngoamount > $payment['unformated_ngo_interest']) {
							$paymentRow = $row->ngoamount - $intrestRow;
							$intrestA = 0;
							$paymentA = $payment['unformated_ngo_principal'] - $paymentRow+$oweRow;
							$oweRow = $paymentA + ($paymentA*($project->lateamount/100));
							$feeRow = ($paymentA*($project->lateamount/100));
							$principalPaidRow += $principalRow - $paymentA;
						} else {
							$intrestA = $intrestRow- $row->ngoamount;
							$paymentA = $payment['unformated_ngo_principal'];
							$oweRow = ($intrestRow - $row->ngoamount) + $payment['unformated_ngo_principal'] + ($payment['unformated_ngo_principal']*($project->lateamount/100));
	            			$feeRow = $intrestA + ($paymentA*($project->lateamount/100));
						}
						echo money_format('%!.0n', $paymentA);
						$totalPrincipalArrears += $paymentA;
					} else {
						$paymentA =0;
	            		$intrestA =0;
						$pastOwe = $oweRow;
						if(strtotime($row->date) > strtotime($payment['date'] . " + 5 days") ){
							$oweRow = $principalRow * .1;
							$feeRow = $oweRow;
						} else {
							$oweRow = 0;
							$feeRow = 0;
						}
						$paymentRow = 0;
						if( $row->ngoamount > ($payment['unformated_ngo_payment']+$pastOwe)) {
							$payment['unformated_ngo_principal'] = $row->ngoamount - $payment['unformated_ngo_interest'];
							$principalPaidRow += $row->ngoamount - $intrestRow;
						} else {
							$principalPaidRow += $payment['unformated_ngo_principal']+$pastOwe;
						}
						echo "---";
					}
				?>
			</td>
			<td>
				<?php
					if(isset($paymentA) && $paymentA){
						if($paymentA >= $payment['unformated_ngo_principal']){
							echo money_format('%!.0n', '0');
							$principalPaidPeriod = 0;
						} else {
							echo money_format('%!.0n', $payment['unformated_ngo_principal']- $paymentA);
	            			$principalPaidPeriod = $payment['unformated_ngo_principal']-$paymentA;
	            		}
					} else {
						echo money_format('%!.0n', $principalRow);
	            		$principalPaidPeriod = ($principalRow);
					}
					$totalPrincipalPaid += $principalPaidPeriod;
				?>
			</td>
			<td>
				<?php money_format('%!.0n', $intrestRow );?> 
	            <?php $totalInterestDue += $intrestRow;?>
			</td>
			<td>
				<?php
					if (isset($intrestA) && $intrestA ){
						echo money_format('%!.0n', $intrestA);
						$totalInterestArrears += $intrestA;
					} else {
						echo '----';
					}
				?>
			</td>
			<td>
				<?php
					if (isset($intrestA) && $intrestA ){
						$totalInterestPaid += $intrestRow-$intrestA;
						echo money_format('%!.0n', $intrestRow-$intrestA);
					} else {
						$totalInterestPaid += $intrestRow;
						echo money_format('%!.0n', $intrestRow);
					}
				?>
			</td>
			<?php $payment['unformated_ngo_payment'] = $principalRow + $intrestRow;?>
			<td><?=money_format('%!.0n', $payment['unformated_ngo_payment']);?> 
				<?php $grandtotalDue += $payment['unformated_ngo_payment'];?>
			</td>
			<td><?=money_format('%!.0n', $row->ngoamount);?> 
				<?php $grandtotalPaid += $row->ngoamount;?>
			</td>
			<td><?=money_format('%!.0n', $feeRow);?> </td>
			<?php $totalFees += $feeRow;?>
			<td><?=number_format(($principalPaidRow/$project->ngoamount)*100,1);?> 
				<?php $totalRepaidPercent_ngo = number_format(($principalPaidRow/$project->ngoamount)*100,1);?>
			</td>
			<?php
				if(!$row->depositdate){
					$principalPaidPHP += $principalPaidPeriod/$todayRate;
				} else {
					$principalPaidPHP += $row->depositamount;
				}
				if (date('m', strtotime($payment['date'])) % 3 == 0):
			?>
			<td><?php if($row->depositdate):?>
				<?=date('d M y', strtotime($row->depositdate))?>
				<?php else :?>
				---
				<?php endif;?>
			</td>
			<td>
				<?php
					$pastPrincipalPaid = $totalQtrDeposit;
					echo money_format('%!.0n', ($principalPaidPHP - $pastPrincipalPaid) * $todayRate);
					$totalQtrDepositNGO = ($principalPaidPHP - $pastPrincipalPaid) * $todayRate;
				?>
				<?php $totalQtrDeposit = $principalPaidPHP; ?>
			</td>
			<td>
				<?php if($row->depositRate):?>
				<?=$row->depositRate . ' ' . $currency?> -> $1 USD
				<?php else :?>
				<?=$todayEx?>
				<?php endif;?>
			</td>
			<td>
				<?php
					echo money_format('%!.0n', $principalPaidPHP - $pastPrincipalPaid);
				?>
			</td>
				<?php else:?>
					<td>------</td>
					<td>------</td>
					<td>------</td>
					<td>------</td>
				<?php endif;?>
			<td><?=number_format(($principalPaidPHP/$project->amount)*100,1);?></td>
			<?php $totalRepaidPercent_php =number_format(($principalPaidPHP/$project->amount)*100,1);?>
		</tr>
		<?php $counter++;?>
		<?php
			endforeach;
			} // End Error encase
		?>
		<tr>
			<td colspan="4" class="reportTable" >Total</td>
			<td><?php  echo money_format('%!.0n', $totalPrincipalDue);?></td>
			<td><?php  echo money_format('%!.0n', $totalPrincipalArrears );?></td>
			<td><?php  echo money_format('%!.0n', $totalPrincipalPaid);?></td>
			<td><?php  echo money_format('%!.0n', $totalInterestDue);?></td>
			<td><?php  echo money_format('%!.0n', $totalInterestArrears);?></td>
			<td><?php  echo money_format('%!.0n', $totalInterestPaid );?></td>
			<td><?php  echo money_format('%!.0n', $grandtotalDue );?></td>
			<td><?php  echo money_format('%!.0n', $grandtotalPaid );?></td>
			<td><?php  echo money_format('%!.0n', $totalFees );?></td>
			<td><?php  echo $totalRepaidPercent_ngo;?></td>
			<td></td>
			<td><?php  echo money_format('%!.0n', $totalQtrDepositNGO );?></td>
			<td></td>
			<td><?php  echo money_format('%!.0n', $totalQtrDeposit );?></td>
			<td><?php  echo $totalRepaidPercent_php;?></td>
		</tr>
	</tbody>
</table>
<br />
<div class="clearfix"></div>
<?php 
	if($project->public == "Closed"):
?>
		<table>
			<thead>
				<tr>
					<td colspan="4">Closed Loan Exchange Rate Fluctuation Adjustment</td>
				</tr>
				<tr>
					<td>Close Loan Date </td>
					<td>Total Loan Principal Amount Due (USD) </td>
					<td>% of Total Repaid (<?=$currency;?>)</td>
					<td>Total Adjust Principal Amount Due (USD)</td>
					<td>Total of Deposits Made (USD)</td>
					<td>Total Adjustment Amount (USD)</td>
					<td>Exhange Rate at Closing (<?=$currency;?>)</td>
					<td>Adjustment Amount Posted to Quarterly Loan Summary Report (<?=$currency;?>)</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><?=$closingDate;?> </td>
					<td>$ <?php echo money_format('%!#10n',$project->amount);?> </td>
					<td><?php echo $totalRepaidPercent_ngo;?></td>
					<td>
						<?php
							if($totalPrincipalDue > $project->ngoamount){
								$totalAdjustment = $project->ngoamount/$todayRate;
							} else {
								$totalAdjustment = $totalPrincipalDue/$todayRate;
							}
							echo  money_format('%!.0n',$totalAdjustment);
						?> 
					</td>
					<td><?php echo money_format('%!.0n', $totalQtrDeposit);?></td>
					<td><?php echo money_format('%!.0n', $project->amount - $totalQtrDeposit);?> </td>
					<td><?php echo $todayEx;?> </td>
					<td>
						<?php
							$math =$project->amount - $totalAdjustment;
							$math = number_format($math,2, '.', '')*$todayEx;
							echo money_format('%!.0n', $this->Client_model->roundUpToAny($math) );	
						?>
					</td>
				</tr>
			</tbody>
		</table>
<?php
	endif;
?>
Note: All values round up to the nearest 5 (<?=$currency?>)<br/>

