<?php 
setlocale(LC_MONETARY, 'en_US');
//$currency = $this->tank_auth->get_orgCurrency();

$grandratetotal = 0;
$grandtotalNGO = 0;
$grandtotalphp = 0;
//loan process
$loanstartPHP = 500;
$loanstartRATE = 685;

$loanAmount = 300000;
$loanDate = "2013-09-14";
$paymentsMade[1] = array('date' => '2013-10-13',	'payment' =>'32500');
$paymentsMade[2] = array('date' => '2013-11-13',	'payment' =>'31875');
$paymentsMade[3] = array('date' => '2013-12-13',	'payment' =>'31250');
$paymentsMade[4] = array('date' => '2013-01-14',	'payment' =>'30625');
$paymentsMade[5] = array('date' => '2013-02-14',	'payment' =>'30000');
$paymentsMade[6] = array('date' => '2014-03-14',	'payment' =>'3550');
$paymentsMade[7] = array('date' => '2014-04-14',	'payment' =>'87250');
// $paymentsMade[7] = array('date' => '2014-04-14',	'payment' =>'78529');
$paymentsMade[8] = array('date' => '2014-05-14',	'payment' =>'27500');
$paymentsMade[9] = array('date' => '2014-06-14',	'payment' =>'26875');
$paymentsMade[10] = array('date' => '2014-07-14',	'payment' =>'26250');
$paymentsMade[11] = array('date' => '2014-08-14',	'payment' =>'25390');

$loanAmountDue = $loanAmount;
$paymentTerms = 12;

$payment = $loanAmount/$paymentTerms;
$counter = 0;

$totalPrincipal = 0;
$totalInterest = 0;
$grandTotal = 0;
$owe = 0;
$totalOwe = 0;
$totalPaid = 0;
$totalPaidInterest = 0;
$totalPaidPayment = 0;
$fee =0;

$todayEX = 685;

$currency = 'RWF';

?>

<h2>Saccoo Barrorer Repayment Report </h2>

<table>
	
		
			<tr>
				<td class="reportTable">Monthly Interest Percent</td>
				<td>2.5 %</td>
			</tr>
			<tr>
				<td class="reportTable">Penalty Interest Percent </td>
				<td>10 %</td>
			</tr>
			<tr>
				<td class="reportTable">Loan Start Date</td>
				<td><?=$loanDate?></td>
			</tr>
			<tr>
				<td class="reportTable">Loan 1st Payment Date</td>
				<td><?=date("Y-m-d", strtotime($loanDate . "+1 month"))?></td>
			</tr>
		
			<tr>
				<td class="reportTable">Beginning Exchange Rate</td>
				<td>$1 USD -> <?=$loanstartRATE?> <?=$currency?></td>
			</tr>
			<tr>
				<td class="reportTable">Total Loan </td>
				<td><?php echo money_format('%(!#10n', $loanAmount)?><?=$currency?> / $ <?php echo money_format('%(!#10n', $loanstartPHP)?> USD</td>
			</tr>
			<tr>
				<td class="reportTable">Monthly Principal Payment</td>
				<td><?php echo money_format('%(!#10n', $payment)?><?=$currency?></td>
			</tr>
			
		
			<tr>				
				<td class="reportTable">Loan Term</td>
				<td>12 Payments 
			</td>
			</tr>
		
	

	</table>
<style type="text/css">
	.jan, .jan td{
		background-color: red;
	}
		table tr td{border: 1px solid black; }
	thead th{border:2px solid black; font-weight: bold; background-color: yellow; }
	
	table{ border-collapse: collapse; }	
	.grandtotal td{
		font-weight: bold;
		font-size: 18px;
		
	}
</style>
<table>
	<thead>
		<th>Payment Date</th>
		<th>Date Paid</th>
		<th>Principal Balance Remaining</th>
		<th>Payment Number </th>
		<th>Principal Due (Includes Late Fees)</th>
		<th>Principal Paid (Includes Late Fees)</th>
		<th>Interest Due</th>
		<th>Interest Paid</th>
		<th>Total Due</th>
		<th>Total Paid</th>
		<th>Late Total </th>
		<th>% of Total Principal Repaid</th>
		<th>Deposited to PHP Date</th>
		<th>Exchange Rate to USD</th>
		<th>Total Quarterly Principal Payment Amount Due (USD)</th>
		<th>% of USD Principal repaid</th>
	</thead>
	<?php while($loanAmountDue > 0):?>
	<?php $counter ++;
	
		$date = date('Y-m-d', strtotime($loanDate . "+ $counter Month"));

	?>

		<?php if(isset($paymentsMade[$counter] ) ):
					//thePayment
			$thePayment = $payment+$owe;//what should be paid

			$paid = $paymentsMade[$counter]['payment'];
			$totalPaid += $paid;
			 $paymentInterest = round($loanAmountDue * 0.025);
		?>

		<tr <?php if (date('m', strtotime($date)) == '01'):?> class="jan" <?php endif;?> >
		<td><?php echo $date?></td>
		<td><?php echo $paymentsMade[$counter]['date'] ?></td>
		<td><?php echo money_format('%(!#10n', round($loanAmountDue));?></td>
		<td><?php echo $counter;?></td>
		<td><?php echo money_format('%(!#10n', round($payment+$owe));?></td>
		<td><?php 
			if($paid >= $payment+$owe+$paymentInterest){
					$paidPayment = $paid - $paymentInterest;
				} else {
					$paidPayment = ($paid - $paymentInterest) - ($payment+$owe) ;
					if($paidPayment < 0){
						$paidPayment = 0;
					}
				}
				$totalPaidPayment += $paidPayment;
		echo money_format('%(!#10n', round($paidPayment));?></td>
		<td><?php $totalInterest += $paymentInterest;
			echo money_format('%(!#10n', $paymentInterest);?></td>
			<td><?php 
				if($paid > $paymentInterest){
					$paidPaymentInterest = $paymentInterest;
				} else {
					$paidPaymentInterest = $paid;
				}
				$totalPaidInterest += $paidPaymentInterest;
			echo money_format('%(!#10n', $paidPaymentInterest);?></td>
		<td><?php 
			$total = round($payment +$owe +  $paymentInterest);

		echo money_format('%(!#10n', $total);
		?>
	</td>
	<td><?php 
			$total = $paymentsMade[$counter]['payment'];

		echo money_format('%(!#10n', $total);
			$grandTotal += $total;
		?>
	</td>
	
		<?php 


		//latepayment
			if(strtotime($paymentsMade[$counter]['date']) < strtotime($date . ' + 5 Days')){

				if ($paid >= $payment+$owe+$paymentInterest){
						if($owe > 0){
							$owe = $paid - round($payment+$paymentInterest+$owe);

						} 

						 if ($payment+$paymentInterest+$owe < $paid){
							$owe = 0;
						}


						
					echo "<td> --- </td>";
						$PPaid = $paid - $paymentInterest;
					$loanAmountDue = $loanAmountDue - $PPaid;
				} else {
						//paying intrest first
						if($paymentInterest < $paid){ 
						$paid = $paid - $paymentInterest;
				}  else {
						$paid = $paymentInterest -$paid;
					
				}		$owe= ($owe+$payment);
						$fee = round($owe*.10 + $paid);
						$owe = $owe + $fee;
						// $owe = round($owe + $fee, -1, PHP_ROUND_HALF_UP);
					echo "<td>$fee <small>10% late fee added </small></td>";

					$loanAmountDue = $loanAmountDue - $paid ;
				}
			}else {

				//late 10%fee
					if ($paid >= $payment+$paymentInterest){
					$loanAmountDue = $loanAmountDue - $payment;
					$fee = $payment*1.1+ $paymentInterest;
					$fee = money_format('%(!#10n', $fee);
					echo "<td>" . $fee . " <small>10% added to the principal </small></td>";

				} else {
						//paying intrest first 
						if($paid == 0){
						$owe= ($payment+$owe);
						$fee = ($owe*.10) + $paymentInterest;
						$owe = round($owe + $fee);

						}//nothing was paid 
						else {
						$paid = $paid - $paymentInterest;
						$owe= ($payment+$owe) - $paid;
						$fee = $owe*.10;
						$owe = round($owe + $fee);

						}
					echo "<td>$owe <small>10% late fee added </small></td>";

					$loanAmountDue = $loanAmountDue - $paid ;
				}
			}
			$totalOwe += $fee;
			$fee = 0;
					$totalPrincipal += round($payment+$owe);

			?>
		<td><?php echo floor(($totalPrincipal/$loanAmount)*100);?></td>
		<td>------</td>
		<td>685</td>
		<td>$<?php 
			$paymentPaidUSD = $totalPrincipal/685;

			 echo money_format('%(!#10n', $paymentPaidUSD);?></td>
		<td><?php echo floor(($paymentPaidUSD/$loanstartPHP)*100);?></td>
			</tr>
	<?php else:?>
		
		<tr <?php if (date('m', strtotime($date)) == '01'):?> class="jan" <?php endif;?> >
		<td><?php echo $date?></td>
		<td>N/A</td>
		<td><?php echo money_format('%(!#10n', round($loanAmountDue));?></td>
		<td><?php echo $counter;?></td>
		<td>
			<?php //lastpayment
				if($loanAmountDue < $payment){
					$payment = $loanAmountDue;
				}?>
			<?php echo money_format('%(!#10n', round($payment+$owe));
			$totalPrincipal += round($payment+$owe);
		?></td>
	<td> --- </td>

		<td><?php $paymentInterest = round($loanAmountDue * 0.025);
			$totalInterest += $paymentInterest;
			echo money_format('%(!#10n', $paymentInterest);?></td>
	<td> --- </td>
		
		<td><?php 
			$total = round($payment+$owe + $paymentInterest);

		echo money_format('%(!#10n', $total);
		$grandTotal += $total;?>
	</td>
	<td> --- </td>
	<td> --- </td>
	<td> --- </td>
	<td> --- </td>
	<td> --- </td>
	<td> --- </td>
	</tr>
		<?php $loanAmountDue = $loanAmountDue - $payment;?>
<?php endif;?>
	<?php endwhile;?>
		<tr>
		<td colspan="3">Grand Total</td>
		<td><?php echo $counter;?> </td>
		<td><?php echo money_format('%(!#10n', $totalPrincipal);?></td>
		<td><?php echo money_format('%(!#10n', $totalPaidPayment);?></td>
		<td><?php echo money_format('%(!#10n', $totalInterest);?></td>
		<td><?php echo money_format('%(!#10n', $totalPaidInterest);?></td>
		<td><?php echo money_format('%(!#10n', $grandTotal);?></td>
		<td><?php echo money_format('%(!#10n', $totalPaid);?></td>
		<td><?php echo money_format('%(!#10n', $totalOwe);?></td>
	</tr>
</table>