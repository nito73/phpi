<style type="text/css">
	.total td{
		font-weight: bold;
	}
	img{ height: 200px; float: left;}
	table tr td{border: 1px solid black; }
	thead tr td{border:2px solid black; font-weight: bold; background-color: yellow; }
	
	table{ border-collapse: collapse; }	
</style>
<h1>Quarterly Report for Q<?=$qtr?> </h1>
<?php if($fundingsource != "All"):?>
	<h2>Funding Source: <?=$fundingsource?></h2>
<?php endif;?>
<?php if($query = $this->Report_model->get_quarterly_report($qtr, $fundingsource)):?>
	<table>
		<thead>
			<tr>
				<td>ID</td>
			
				<td>Name of Applicant</td>
				<td>Colletion Location</td>
				<td>Transaction Type</td>
				<td>Amount</td>
				<?php if($fundingsource == "All"):?><td>Funding Source</td><?php endif;?>
			</tr>
		</thead>
		<tbody>
		
	<?php foreach($query as $row):?>
		<tr>
			<td><?=$row->id?></td>
	
			<td><?=$row->name?></td>
		
			<td><?=$row->collectionlocation?></td>
			<td><?=$row->paymenttype?> (<?=$row->totaltransactions?>)</td>
			<td>$<?=money_format('%(#10n', $row->total)?> USD <?php if($row->currency != "USD"):?>/ <?=money_format('%(#10n', $row->ngototal)?> <?=$row->currency?><?php endif;?></td>
			<?php if($fundingsource == "All"):?><td><?=$row->fundingsource?></td><?php endif;?>
		</tr>	
	<?php endforeach;?>
		</tbody>
	</table>
<?php else:?>
	<p>Nothing to report for <?=$qtr?></p>
<?php endif;?>