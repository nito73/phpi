<?php 
setlocale(LC_MONETARY, 'en_US');
$currency = $this->tank_auth->get_orgCurrency();

$grandratetotal = 0;
$grandtotalNGO = 0;
$grandtotalphp = 0;

?>
<?php $todayEx = $this->Report_model->get_rate($currency) . " $currency -> " . '$1 USD ';?>
	<?php $todayRate = $this->Report_model->get_rate($currency);?>
	
	
<style type="text/css">
	.total td{
		font-weight: bold;
	}
	img{ height: 200px; float: left;}
	table tr td{border: 1px solid black; }
	thead tr td{border:2px solid black; font-weight: bold; background-color: yellow; }
	
	table{ border-collapse: collapse; }	
	.grandtotal td{
		font-weight: bold;
		font-size: 18px;
		
	}
</style>
<?php if ($startdate == $enddate):?>
<h1><?=$startdate?> Daily Report </h1>
<?php else:?>
<h1>Summary Report <?=$startdate?> to <?=$enddate?> </h1>
<?php endif;?>
<?php if($fundingsource != "All"):?>
	<h2>Funding Source: <?=$fundingsource?></h2>
<?php endif;?>
<?php if($query):
	
	$returngrandtotal = 0;
	$interestgrandtotal = 0;
	$principalgrandtotal =0;
	$grandtotal = 0;
	$grandtotaldue = 0;
	$granddeposittotal = 0;
	
?>
<h3>
	<?=date("d M Y")?> Exchange Rate <br />
	
	
	<?=$todayEx?>
</h3>

	<table>
		<thead>
			<tr>
				<td>ID</td>
			
				<td>Name of Applicant</td>
				<td># of Payments Made</td>
				<td>Payments Remaining</td>
				<td>Collection Location</td>
				<td>Total Collected</td>
				<td>Total Principal Return %</td>
				<td>Total Principal Collected</td>
				<td>Exchange Rate Fluctuation Deposit (PHP) </td>
				<td>Principal Deposited to PHP</td>
			</tr>
		</thead>
		<tbody>
		
	<?php foreach($query as $row):?>
	<?php 

	//payments that needed to be made
	//creates a counter of payments that needs to be made
				$paymentneedmade = 0;
				$loandate  = $row->loandate;
				
					for($x=1; $x < $row->loanterms+1; $x++){
						
						if((strtotime("$loandate +" . $x * $row->loantermstype . " Week" ) > strtotime($startdate)) && (strtotime("$loandate +" . $x * $row->loantermstype . " Week" ) <= strtotime($enddate))  ){
							$paymentneedmade++;
						}
					}
					
				?>
		<tr>
			<td><?=$row->id?></td>
			<td><?=$row->clientname?></td>
			<td><?=$row->totaltransactions?></td>
			
			
			<td><?=$row->loanterms - $row->totaltransactions?></td>
		
		
			<td><?=$row->collectionlocation?></td>

										
							<td><?=money_format('%(!#10n', $row->ngototal)?> <?=$row->currency?></td>
						
								<?php 
				// no more 1%	
				//	if ($row->fundingsource == "PHP"){
				//if php funded takes what was desposited and gets the principal collected
					
					//principal collected from the ngo
					 $principalNGO = ((100-$row->loaninterest)/100 ) * 	$row->ngototal; 
					
					//NGO collected principal divided total to get the percent collected * the USD provided to the NGO * current exchange rate
				 $convertedAmount = (($principalNGO/$row->ngoamount)*$row->amount)*$this->Report_model->get_rate($row->currency);
					
					//find out how much intrest was collected
					$interestNGO = ($row->loaninterest/100 ) * 	$row->ngototal; 
					
					//veriable was redundent on purpose since we use to collect the 1% from non PHP loans
					//used to collect the total amount DUE
					$principalCollected = $principalNGO; 

					//find the diffrence of what was collect and what is do after today exchange rate
					$phpreturnammount = $convertedAmount - $principalNGO;
					
					//used to count totals
					$returngrandtotal = $returngrandtotal + $convertedAmount;
					$principalgrandtotal+= $principalCollected;
					$interestgrandtotal += $interestNGO;
					$grandtotal = $grandtotal + $principalNGO;
					$grandratetotal = $grandratetotal + ($principalNGO - $convertedAmount);
					$granddeposittotal += $principalNGO+$phpreturnammount; ?>
				
			<td><?=number_format(($principalNGO/$row->ngoamount)*100, 2, '.', '')?> %</td>
				
			<td><?php echo money_format('%(!#10n', $principalNGO);?> <?=$row->currency?>  </td>	
					
			<td><?php echo ($phpreturnammount == 0) ? "N/A" : money_format('%(!#10n', $phpreturnammount) . "$row->currency" ?> </td>

			<td> <?php echo money_format('%(!#10n', $principalNGO+$phpreturnammount); ?> <?=$row->currency?>  </td>	
		</tr>	
	
	<?php endforeach;?>
			<?php $grandtotalNGO = $grandtotal?>
				<tr class="grandtotal">
			<td colspan="7" style="text-align:right">Grand Total</td>
			
			<td><?=money_format('%(!#10n', $principalgrandtotal)?><?=$row->currency?></td>

			<td><?=money_format('%(!#10n', $grandratetotal)?><?=$row->currency?></td>
			<td><?=money_format('%(!#10n', $granddeposittotal)?><?=$row->currency?></td>
			
	

		</tr>
		</tbody>
	</table>
	
	
	<strong>Principle Collected:</strong> <?=money_format('%(!#10n', $principalgrandtotal)?> <?=$row->currency?> <br />
	<strong>Interest Collected:</strong> <?=money_format('%(!#10n', $interestgrandtotal)?> <?=$row->currency?> <br />
	
	
	<?php 
	$returngrandtotal = 0;
	$interestgrandtotal = 0;
	$principalgrandtotal =0;
	$grandtotal = 0;
	$grandtotaldue = 0;
	$grandratetotal = 0;
	$granddeposittotal = 0;
	?>
	Rate Differance is the % Return to the Current Exchange Rate
		<table>
		<thead>
			<tr>
				<td>ID</td>
			
				
				<td>Name of Applicant</td>
				<td>Rate on Loan Start</td>
				<td>Total Principal</td>
			
				<td>Total Principal Collected</td>
				
				
				
				<td>Total Principal Return %</td>
			
				<td>Today Exchange Rate</td>
				<td>Exchange Rate Fluctuation Deposit (PHP)</td>
				<td>Principal Deposited to PHP</td>
				<td>Deposited to NGO</td>

				<?php /*if($fundingsource == "All"):?><td>Funding Source</td><?php endif;?><?php */?>
			</tr>
		</thead>
		<tbody>
		
	<?php foreach($query as $row):?>
	<?php if ($row->fundingsource == "PHP"):?>
		<tr>
			<td><?=$row->id?></td>
			<td><?=$row->clientname?></td>
			
					<td><?=$row->rate;?> -> $1 USD</td>					
			

			<?php 
				// no more 1%	
				//	if ($row->fundingsource == "PHP"){
				//if php funded takes what was desposited and gets the principal collected
					
					//principal collected from the ngo
					 $principalNGO = ((100-$row->loaninterest)/100 ) * 	$row->ngototal; 
					
					//NGO collected principal divided total to get the percent collected * the USD provided to the NGO * current exchange rate
					 $convertedAmount = (($principalNGO/$row->ngoamount)*$row->amount)*$this->Report_model->get_rate($row->currency);
					
					//find out how much intrest was collected
					$interestNGO = ($row->loaninterest/100 ) * 	$row->ngototal; 
					
					
					//veriable was redundent on purpose since we use to collect the 1% from non PHP loans
					//used to collect the total amount DUE
					$principalCollected = $principalNGO; 

					//find the diffrence of what was collect and what is do after today exchange rate
						$phpreturnammount = $convertedAmount - $principalNGO;
					
					//add the diffrence to what was collected to get a total due for php
					$totaldue = $row->ngototal + ($phpreturnammount);
										
					
				//used to count totals
					$returngrandtotal = $returngrandtotal + $convertedAmount;
					$principalgrandtotal+= $principalCollected;
					$interestgrandtotal += $interestNGO;
					$grandtotal = $grandtotal + $principalNGO;
					$grandratetotal = $grandratetotal + ($principalNGO - $convertedAmount);
					$granddeposittotal += $principalNGO+$phpreturnammount;
					?>
					 <?php //this is whats returned before
					 	//echo money_format('%(!#10n', $principalNGO) . ' ' . $row->currency . '/'?>
					<?php 
										
										?>
				<td><?=money_format('%(!#10n', $row->ngoamount)?> <?=$row->currency?></td>					
				<td><?=money_format('%(!#10n', $principalNGO)?> <?=$row->currency?></td>
				<td><?=number_format(($principalNGO/$row->ngoamount)*100, 2, '.', '')?> %</td>
				<td><?=$todayEx?>		</td>
				<td><?php echo ($phpreturnammount == 0) ? "N/A" :  money_format('%(!#10n', -$phpreturnammount) . ' ' . $row->currency ?> </td>
				<td><?php echo money_format('%(!#10n', $principalNGO+$phpreturnammount); ?> <?=$row->currency?> </td>					
					<td></td>
					
					
						
		</tr>	
	<?php endif;?>
	<?php endforeach;?>
		<?php $grandtotalphp = $grandtotal;?>
			<tr class="grandtotal">
			<td colspan="7" style="text-align:right">Grand Total</td>
		
			<td><?=money_format('%(!#10n', $grandratetotal)?><?=$row->currency?></td>
			<td><?=money_format('%(!#10n', $granddeposittotal)?><?=$row->currency?></td>
			<td></td>

		</tr>
		</tbody>
	</table>
	
<?php else:?>
	<?php if ($startdate == $enddate):?>
	<p>No Loan Transactions for  <?=$startdate?> </p>
<?php else:?>
	<p>No Loan Transactions for  <?=$startdate?> to <?=$enddate?></p>
	<?php endif;?>
<?php endif;?>


<?php if($intial_query):?>
<h2>Initial Deposit</h2>
<?php 
	
	
	$grandfluxtotal = 0;
	$granddeposittotal = 0;
	?>
	Rate Differance is the % Return to the Current Exchange Rate
		<table>
		<thead>
			<tr>
				<td>ID</td>
			
				
				<td>Name of Applicant</td>
				<td>Rate on Loan Start</td>
				<td>Total Principal</td>
				<td>Total Principal Collected</td>
				<td>Total Principal Return %</td>
				<td>Today Exchange Rate</td>
				<td>Exchange Rate Fluctuation Deposit (PHP)</td>
				<td>Principal Deposited to PHP</td>
				<td>Deposited to NGO</td>

			</tr>
		</thead>
		<tbody>
		
	<?php foreach($intial_query as $row):?>
	
	<?php 
		
	$grandfluxtotal += $row->difference;
	$granddeposittotal += ($row->amount*$row->intial_rate)+$row->difference;
	?>
		<tr>	
			<td><?=$row->phpid;?></td>
			<td><?=$row->name;?></td>
			<td><?=$row->intial_rate;?> -> $1 USD</td>
			<td>N/A</td>
			<td>N/A</td>
			<td>N/A</td>
			<td><?=$todayEx?></td>
			<td><?php echo money_format('%(!#10n', $row->difference); ?> <?=$row->currency?>  				</td>	
			<td>N/A</td>
			<td><?php echo money_format('%(!#10n', ($row->amount*$row->intial_rate)+$row->difference);?> <?=$row->currency?> </td>	
		</tr>		 	
			

	<?php endforeach;?>
		<?php $grandtotalphp = $grandtotal;?>
			<tr class="grandtotal">
			<td colspan="7" style="text-align:right">Grand Total</td>
		
			<td><?=money_format('%(!#10n', $grandfluxtotal)?><?=$row->currency?></td>
			<td><?=money_format('%(!#10n', $granddeposittotal)?><?=$row->currency?></td>
			<td></td>

		</tr>
		</tbody>
	</table>

<?php endif;?>
<br />

<?php 
//checks if there was any savings during the month
if($savingsquery):
	$totalDeposit = 0;
	$totalWithdraw = 0;
	
?>

	<h2>Savings Transaction</h2>

	<table>
		<thead>
			<tr>
				<td>ID</td>
			
				<td>Name of Applicant</td>
				<td>Total # of Deposits</td>
				<td>Total Deposits</td>
				<td>Total # of Withdraw</td>
				<td>Total Withdraw</td>
				<td>Current Balance</td>
			</tr>
		</thead>
		<tbody>
		
		<?php $client = array();
		
	foreach($savingsquery as $row){
			$clientID = $row->id;
			
			$client[$clientID]['name'] = $row->clientname;
		
				if($row->type == "Deposit"){
					$totalDeposit += $row->total;
					$client[$clientID]['totalDepositCount'] = $row->totaltransactions;
					$client[$clientID]['totalDeposit'] = money_format('%(!#10n',$row->total) . ' ' . $row->currency;
				} else {
					$totalWithdraw += $row->total;
					$client[$clientID]['totalWithdrawCount'] = $row->totaltransactions;
					$client[$clientID]['totalWithdraw'] = money_format('%(!#10n',$row->total) . ' ' . $row->currency;
				}
				$currency =$row->currency;
	



	}

	?>
		
		
		<?php foreach($client as $clientID=>$v):?>
		<tr>
			<td><?=$clientID?></td>	
			<td><?=$v['name']?></td>
			<?php if(isset($v['totalDepositCount'])):?>
				<td> <?=$v['totalDepositCount']?></td>
				<td><?=$v['totalDeposit']?></td>
			<?php else:?>
				<td>0</td>
				<td>0 <?=$currency?> </td>
			<?php endif;?>
			<?php if(isset($v['totalWithdrawCount'])):?>
				<td> <?=$v['totalWithdrawCount']?></td>
				<td><?=$v['totalWithdraw']?></td>
			<?php else:?>
				<td>0</td>
				<td>0 <?=$currency?> </td>
			<?php endif;?>
			<td><?php $savtotalCurrent = $this->Client_model->client_savings_trans_total($clientID);?>
		    	<?=money_format('%(!#10n', $savtotalCurrent)?> <?=$currency?>
		    	</td>	
		</tr>
		<?php endforeach;?>
		</tbody>
	</table>
	
	<strong>Deposit Collected:</strong> <?=money_format('%(!#10n', $totalDeposit)?> <?=$currency?> <br />
	<strong>Withdraw Collected:</strong> <?=money_format('%(!#10n', $totalWithdraw)?> <?=$currency?> <br />
	
	
	
<?php else:?>
		<?php if ($startdate == $enddate):?>
	<p>No Savings Transactions for  <?=$startdate?> </p>
<?php else:?>
	<p>No Savings Transactions for  <?=$startdate?> to <?=$enddate?></p>
	<?php endif;?>

<?php endif;?>
<br />
<?php if($YTDQuery):?>
<h2>Year to date Info </h2>
<?php foreach($YTDQuery['savingQuery'] as $savingRow):?>
	<strong> Total YTD <?=$savingRow->type?>: </strong> <?=money_format('%(!#10n',$savingRow->total)?> <?=$currency?> <br />
<?php endforeach;?>

<?php

	$totalInterest=0;
	$totalPrincipal=0;
 foreach($YTDQuery['loanQuery'] as $loanRow):?>

	<?php
		$interest = $loanRow->loaninterest/100;
		$projectInterest = $interest * $loanRow->ngototal;
		$projectPrincipal = $loanRow->ngototal - $projectInterest;
		
		$totalInterest += $projectInterest;
		$totalPrincipal += $projectPrincipal;
		
		
	?>
<?php endforeach;?>

	<strong> Total YTD Interest Collected: </strong> <?=money_format('%(!#10n',$totalInterest)?> <?=$currency?>  / $<?=money_format('%(!#10n',$totalInterest/$todayRate)?> USD<br />

		<strong> Total YTD Principal Collected: </strong> <?=money_format('%(!#10n',$totalPrincipal)?> <?=$currency?> / $ <?=money_format('%(!#10n',$totalPrincipal/$todayRate)?> USD <br />
	
<?php endif;?>

<form class="" method="post" action="<?=site_url()?>/reports/deposit">
	<input type="hidden" name="startdate" value="<?=$startdate?>"/>
	<input type="hidden" name="enddate" value="<?=$enddate?>"/>
	<input type="hidden" name="ratedifferance" value="<?=$grandratetotal?>"/>
	<input type="hidden" name="ngoamount" value="<?=$grandtotalNGO?>"/>
	<input type="hidden" name="phpamount" value="<?=$grandtotalphp?>"/>
	<input type="hidden" name="currency" value="<?=$currency?>"/>
	<input type="hidden" name="rate" value="<?=$this->Report_model->get_rate($currency)?>"/>
	<input type="hidden" name="type" value="Quarterly Report Deposit"/>
	<input type="submit" value="Deposit" />
	<small> By clicking on the deposit you will have 5 business days to deposit this amount into the PHP account </small> 
</form>