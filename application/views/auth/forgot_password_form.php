

<?php
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'value' => set_value('login'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
if ($this->config->item('use_username', 'tank_auth')) {
	$login_label = 'Email or login';
} else {
	$login_label = 'Email';
}
?>
<div class="row" style="padding:5px">
	
	<div class="span6">
	<p>
		  <img src="<?=base_url()?>images/how_php_works.jpg" alt="How PHP works">
        <a class="pull-right btn btn-warning" href="/pages/view/how_php_works/">Learn More</a>
        <div class="clearfix"></div>
        <br />
		<div class="row">
 
        	
		<div class="span2">
			<div style="background-color:black; padding:10px">
			<h4 style="color:white; text-align:center;">Micro Seed Loans</h4>
			<img src="<?=base_url()?>images/micro_seed.jpg" alt="How PHP works">
			</div>
			<a class="pull-right btn btn-warning" href="/pages/view/micro_loans/">Learn More</a>
		</div>
		<div class="span2">
			<div style="background-color:black; padding:10px">
			<h4 style="color:white; text-align:center;">Macro Seed Loans</h4>
			<img src="<?=base_url()?>images/macro.jpg" alt="How PHP works">
			</div>
			<a class="pull-right btn btn-warning" href="/pages/view/macro_loans/">Learn More</a>
		</div>
		<div class="span2">
			<div style="background-color:black; padding:10px">
		<h4 style="color:white; text-align:center;">Projects</h4>
			<img src="<?=base_url()?>images/projects.jpg" alt="How PHP works">
			</div>
			<a class="pull-right btn btn-warning" href="/pages/view/projects/">Learn More</a>
		</div>
        </div>
        
		
	</div>
	<div class="span5">
		<h3>Password Reset</h3>
		<div class="alert">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  Please enter your email so that we can reset your password
</div>
<?php echo form_open($this->uri->uri_string()); ?>
<table>
	<tr>
		<td><?php echo form_label($login_label, $login['id']); ?></td>
		<td><?php echo form_input($login); ?></td>
		<td style="color: red;"><?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?></td>
	</tr>
</table>

		<input class="btn btn-primary" type="submit" value="Get a new password" name="reset">
		<?php echo form_close(); ?>
	</div>		
		
</div>
