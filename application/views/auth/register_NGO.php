<?php
if ($use_username) {
	$username = array(
		'name'	=> 'username',
		'id'	=> 'username',
		'value' => set_value('username'),
		'maxlength'	=> $this->config->item('username_max_length', 'tank_auth'),
		'size'	=> 30,
	);
}
$firstname = array(
	'name'	=> 'firstname',
	'id'	=> 'firstname',
	'value'	=> set_value('firstname'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
$lastname = array(
	'name'	=> 'lastname',
	'id'	=> 'lastname',
	'value'	=> set_value('lastname'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
$city = array(
	'name'	=> 'city',
	'id'	=> 'city',
	'value'	=> set_value('city'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
	'value'	=> set_value('email'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'value' => set_value('password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
);
$confirm_password = array(
	'name'	=> 'confirm_password',
	'id'	=> 'confirm_password',
	'value' => set_value('confirm_password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'maxlength'	=> 8,
);
?>


<div class="row">
<div class="span3">
<ul class="nav nav-list">
  <li class="nav-header">Registration</li>
  <li class="active"><a href="#">Basic Information</a></li>
  
</ul>
</div>
<div class="span8">
<?php echo form_open($this->uri->uri_string()); ?>
		<?php echo form_label('First Name', $firstname['id']); ?>
		<?php echo form_input($firstname); ?>
		
		<?php echo form_label('Last Name', $lastname['id']); ?>
		<?php echo form_input($lastname); ?>
		
		<label>Organization</label>
		<input type="text" name="organization" id="organization">
		
		<label>Currency Used</label>
		<select name="currency" class="currency">
			<option value="USD">(USD) US Dollar</option>
			<option value="AED">(AED) UAE Dirham</option>
			<option value="AFN">(AFN) Afghani</option>
			<option value="ALL">(ALL) Lek</option>
			<option value="AMD">(AMD) Armenian Dram</option>
			<option value="ANG">(ANG) Netherlands Antillean Guilder</option>
			<option value="AOA">(AOA) Kwanza</option>
			<option value="ARS">(ARS) Argentine Peso</option>
			<option value="AUD">(AUD) Australian Dollar</option>
			<option value="AWG">(AWG) Aruban Florin</option>
			<option value="AZN">(AZN) Azerbaijanian Manat</option>
			<option value="BAM">(BAM) Convertible Mark</option>
			<option value="BBD">(BBD) Barbados Dollar</option>
			<option value="BDT">(BDT) Taka</option>
			<option value="BGN">(BGN) Bulgarian Lev</option>
			<option value="BHD">(BHD) Bahraini Dinar</option>
			<option value="BIF">(BIF) Burundi Franc</option>
			<option value="BMD">(BMD) Bermudian Dollar</option>
			<option value="BND">(BND) Brunei Dollar</option>
			<option value="BOB">(BOB) Boliviano</option>
			<option value="BOV">(BOV) Mvdol</option>
			<option value="BRL">(BRL) Brazilian Real</option>
			<option value="BSD">(BSD) Bahamian Dollar</option>
			<option value="BTN">(BTN) Ngultrum</option>
			<option value="BWP">(BWP) Pula</option>
			<option value="BYR">(BYR) Belarussian Ruble</option>
			<option value="BZD">(BZD) Belize Dollar</option>
			<option value="CAD">(CAD) Canadian Dollar</option>
			<option value="CDF">(CDF) Congolese Franc</option>
			<option value="CHE">(CHE) WIR Euro</option>
			<option value="CHF">(CHF) Swiss Franc</option>
			<option value="CHF">(CHF) Swiss Franc</option>
			<option value="CHW">(CHW) WIR Franc</option>
			<option value="CLF">(CLF) Unidades de fomento</option>
			<option value="CLP">(CLP) Chilean Peso</option>
			<option value="CNY">(CNY) Yuan Renminbi</option>
			<option value="COP">(COP) Colombian Peso</option>
			<option value="COU">(COU) Unidad de Valor Real</option>
			<option value="CRC">(CRC) Costa Rican Colon</option>
			<option value="CUC">(CUC) Peso Convertible</option>
			<option value="CUP">(CUP) Cuban Peso</option>
			<option value="CVE">(CVE) Cape Verde Escudo</option>
			<option value="CZK">(CZK) Czech Koruna</option>
			<option value="DJF">(DJF) Djibouti Franc</option>
			<option value="DKK">(DKK) Danish Krone</option>
			<option value="DOP">(DOP) Dominican Peso</option>
			<option value="DZD">(DZD) Algerian Dinar</option>
			<option value="EGP">(EGP) Egyptian Pound</option>
			<option value="ERN">(ERN) Nakfa</option>
			<option value="ETB">(ETB) Ethiopian Birr</option>
			<option value="EUR">(EUR) Euro</option>
			<option value="EUR">(EUR) Euro</option>
			<option value="FJD">(FJD) Fiji Dollar</option>
			<option value="FKP">(FKP) Falkland Islands Pound</option>
			<option value="GBP">(GBP) Pound Sterling</option>
			<option value="GEL">(GEL) Lari</option>
			<option value="GHS">(GHS) Ghana Cedi</option>
			<option value="GIP">(GIP) Gibraltar Pound</option>
			<option value="GMD">(GMD) Dalasi</option>
			<option value="GNF">(GNF) Guinea Franc</option>
			<option value="GTQ">(GTQ) Quetzal</option>
			<option value="GYD">(GYD) Guyana Dollar</option>
			<option value="HKD">(HKD) Hong Kong Dollar</option>
			<option value="HNL">(HNL) Lempira</option>
			<option value="HRK">(HRK) Croatian Kuna</option>
			<option value="HTG">(HTG) Gourde</option>
			<option value="HUF">(HUF) Forint</option>
			<option value="IDR">(IDR) Rupiah</option>
			<option value="ILS">(ILS) New Israeli Sheqel</option>
			<option value="INR">(INR) Indian Rupee</option>
			<option value="IQD">(IQD) Iraqi Dinar</option>
			<option value="IRR">(IRR) Iranian Rial</option>
			<option value="ISK">(ISK) Iceland Krona</option>
			<option value="JMD">(JMD) Jamaican Dollar</option>
			<option value="JOD">(JOD) Jordanian Dinar</option>
			<option value="JPY">(JPY) Yen</option>
			<option value="KES">(KES) Kenyan Shilling</option>
			<option value="KGS">(KGS) Som</option>
			<option value="KHR">(KHR) Riel</option>
			<option value="KMF">(KMF) Comoro Franc</option>
			<option value="KPW">(KPW) North Korean Won</option>
			<option value="KRW">(KRW) Won</option>
			<option value="KWD">(KWD) Kuwaiti Dinar</option>
			<option value="KYD">(KYD) Cayman Islands Dollar</option>
			<option value="KZT">(KZT) Tenge</option>
			<option value="LAK">(LAK) Kip</option>
			<option value="LBP">(LBP) Lebanese Pound</option>
			<option value="LKR">(LKR) Sri Lanka Rupee</option>
			<option value="LRD">(LRD) Liberian Dollar</option>
			<option value="LSL">(LSL) Loti</option>
			<option value="LTL">(LTL) Lithuanian Litas</option>
			<option value="LVL">(LVL) Latvian Lats</option>
			<option value="LYD">(LYD) Libyan Dinar</option>
			<option value="MAD">(MAD) Moroccan Dirham</option>
			<option value="MAD">(MAD) Moroccan Dirham</option>
			<option value="MDL">(MDL) Moldovan Leu</option>
			<option value="MGA">(MGA) Malagasy Ariary</option>
			<option value="MKD">(MKD) Denar</option>
			<option value="MMK">(MMK) Kyat</option>
			<option value="MNT">(MNT) Tugrik</option>
			<option value="MOP">(MOP) Pataca</option>
			<option value="MRO">(MRO) Ouguiya</option>
			<option value="MUR">(MUR) Mauritius Rupee</option>
			<option value="MVR">(MVR) Rufiyaa</option>
			<option value="MWK">(MWK) Kwacha</option>
			<option value="MXN">(MXN) Mexican Peso</option>
			<option value="MYR">(MYR) Malaysian Ringgit</option>
			<option value="MZN">(MZN) Mozambique Metical</option>
			<option value="NAD">(NAD) Namibia Dollar</option>
			<option value="NGN">(NGN) Naira</option>
			<option value="NIO">(NIO) Cordoba Oro</option>
			<option value="NOK">(NOK) Norwegian Krone</option>
			<option value="NPR">(NPR) Nepalese Rupee</option>
			<option value="NZD">(NZD) New Zealand Dollar</option>
			<option value="OMR">(OMR) Rial Omani</option>
			<option value="PAB">(PAB) Balboa</option>
			<option value="PEN">(PEN) Nuevo Sol</option>
			<option value="PGK">(PGK) Kina</option>
			<option value="PHP">(PHP) Philippine Peso</option>
			<option value="PKR">(PKR) Pakistan Rupee</option>
			<option value="PLN">(PLN) Zloty</option>
			<option value="PYG">(PYG) Guarani</option>
			<option value="QAR">(QAR) Qatari Rial</option>
			<option value="RON">(RON) New Romanian Leu</option>
			<option value="RSD">(RSD) Serbian Dinar</option>
			<option value="RUB">(RUB) Russian Ruble</option>
			<option value="RWF">(RWF) Rwanda Franc</option>
			<option value="SAR">(SAR) Saudi Riyal</option>
			<option value="SBD">(SBD) Solomon Islands Dollar</option>
			<option value="SCR">(SCR) Seychelles Rupee</option>
			<option value="SDG">(SDG) Sudanese Pound</option>
			<option value="SEK">(SEK) Swedish Krona</option>
			<option value="SGD">(SGD) Singapore Dollar</option>
			<option value="SHP">(SHP) Saint Helena Pound</option>
			<option value="SLL">(SLL) Leone</option>
			<option value="SOS">(SOS) Somali Shilling</option>
			<option value="SRD">(SRD) Surinam Dollar</option>
			<option value="SSP">(SSP) South Sudanese Pound</option>
			<option value="STD">(STD) Dobra</option>
			<option value="SVC">(SVC) El Salvador Colon</option>
			<option value="SYP">(SYP) Syrian Pound</option>
			<option value="SZL">(SZL) Lilangeni</option>
			<option value="THB">(THB) Baht</option>
			<option value="TJS">(TJS) Somoni</option>
			<option value="TMT">(TMT) Turkmenistan New Manat</option>
			<option value="TND">(TND) Tunisian Dinar</option>
			<option value="TOP">(TOP) Pa?anga</option>
			<option value="TRY">(TRY) Turkish Lira</option>
			<option value="TTD">(TTD) Trinidad and Tobago Dollar</option>
			<option value="TWD">(TWD) New Taiwan Dollar</option>
			<option value="TZS">(TZS) Tanzanian Shilling</option>
			<option value="UAH">(UAH) Hryvnia</option>
			<option value="UGX">(UGX) Uganda Shilling</option>
			<option value="UYU">(UYU) Peso Uruguayo</option>
			<option value="UZS">(UZS) Uzbekistan Sum</option>
			<option value="VEF">(VEF) Bolivar </option>
			<option value="VND">(VND) Dong</option>
			<option value="VUV">(VUV) Vatu</option>
			<option value="WST">(WST) Tala</option>
			<option value="XAF">(XAF) CFA Franc BEAC</option>
			<option value="XCD">(XCD) East Caribbean Dollar</option>
			<option value="XDR">(XDR) SDR (Special Drawing Right)</option>
			<option value="XPF">(XPF) CFP Franc</option>
			<option value="XSU">(XSU) Sucre</option>
			<option value="XUA">(XUA) ADB Unit of Account</option>
			<option value="YER">(YER) Yemeni Rial</option>
			<option value="ZAR">(ZAR) Rand</option>
			<option value="ZMW">(ZMW) Zambian Kwacha</option>
			<option value="ZWL">(ZWL) Zimbabwe Dollar</option>
		</select>
	<?php if ($use_username) { ?>
	
		<?php echo form_label('Username', $username['id']); ?>
		<?php echo form_input($username); ?>
		<span style="color: red;"><?php echo form_error($username['name']); ?><?php echo isset($errors[$username['name']])?$errors[$username['name']]:''; ?>
		</span>
	
	<?php } ?>

	
		<?php echo form_label('Email Address', $email['id']); ?>
		<?php echo form_input($email); ?>
		<span style="color: red;"><?php echo form_error($email['name']); ?><?php echo isset($errors[$email['name']])?$errors[$email['name']]:''; ?></span>
	
		<?php echo form_label('Password', $password['id']); ?>
		<?php echo form_password($password); ?>
		<span style="color: red;"><?php echo form_error($password['name']); ?></span>
	
		<?php echo form_label('Confirm Password', $confirm_password['id']); ?>
		<?php echo form_password($confirm_password); ?>
		<span style="color: red;"><?php echo form_error($confirm_password['name']); ?></span>


		<label>Address</label>
		<input class="input-xxlarge" type="text" name="address" id="address" placeholder="Address">
		<input type="text" name="city" id="city" placeholder="City">
	
		<input type="text" name="state" id="state" placeholder="State">
	
		<input class="input-mini" type="text" name="zip" id="zip" placeholder="Postal">
		<input class="input-xxlarge" type="text" name="country" id="country" placeholder="Country">
		
		
<input type="hidden" name="usertype" value="NGO" />
<input type="submit" name="register" value="Register" class="btn btn-primary">
<?php echo form_close(); ?>
</div>
</div>
