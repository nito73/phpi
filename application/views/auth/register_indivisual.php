<?php
if ($use_username) {
	$username = array(
		'name'	=> 'username',
		'id'	=> 'username',
		'value' => set_value('username'),
		'maxlength'	=> $this->config->item('username_max_length', 'tank_auth'),
		'size'	=> 30,
	);
}
$firstname = array(
	'name'	=> 'firstname',
	'id'	=> 'firstname',
	'value'	=> set_value('firstname'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
$lastname = array(
	'name'	=> 'lastname',
	'id'	=> 'lastname',
	'value'	=> set_value('lastname'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
$city = array(
	'name'	=> 'city',
	'id'	=> 'city',
	'value'	=> set_value('city'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
$country = array(
	'name'	=> 'country',
	'id'	=> 'country',
	'value'	=> set_value('country'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
	'value'	=> set_value('email'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'value' => set_value('password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
);
$confirm_password = array(
	'name'	=> 'confirm_password',
	'id'	=> 'confirm_password',
	'value' => set_value('confirm_password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'maxlength'	=> 8,
);
?>


<div class="row">
<div class="span3">
<ul class="nav nav-list">
  <li class="nav-header">Registration</li>
  <li class="active"><a href="#">Basic Information</a></li>
  
</ul>
</div>
<div class="span8">
<?php echo form_open($this->uri->uri_string()); ?>
		<?php echo form_label('First Name', $firstname['id']); ?>
		<?php echo form_input($firstname); ?>
		
		<?php echo form_label('Last Name', $lastname['id']); ?>
		<?php echo form_input($lastname); ?>
		
		<?php echo form_label('Home City', $city['id']); ?>
		<?php echo form_input($city); ?>
		
		<?php echo form_label('Country', $country['id']); ?>
		<?php echo form_input($country); ?>
	<?php if ($use_username) { ?>
	
		<?php echo form_label('Username', $username['id']); ?>
		<?php echo form_input($username); ?>
		<span style="color: red;"><?php echo form_error($username['name']); ?><?php echo isset($errors[$username['name']])?$errors[$username['name']]:''; ?>
		</span>
	
	<?php } ?>

	
		<?php echo form_label('Email Address', $email['id']); ?>
		<?php echo form_input($email); ?>
		<span style="color: red;"><?php echo form_error($email['name']); ?><?php echo isset($errors[$email['name']])?$errors[$email['name']]:''; ?></span>
	
		<?php echo form_label('Password', $password['id']); ?>
		<?php echo form_password($password); ?>
		<span style="color: red;"><?php echo form_error($password['name']); ?></span>
	
		<?php echo form_label('Confirm Password', $confirm_password['id']); ?>
		<?php echo form_password($confirm_password); ?>
		<span style="color: red;"><?php echo form_error($confirm_password['name']); ?></span>

<input type="hidden" name="usertype" value="indivisual" />
<input type="submit" name="register" value="Register" class="btn btn-primary">
<?php echo form_close(); ?>
</div>
</div>