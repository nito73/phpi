<div style="padding:10px">
<p><h1>Register As a</h1></p><br />
<div class="row">
<div class="span4">
<a href="/auth/setRegisterType/Self" class="btn-primary btn-large ">Individual</a>
<p style="padding-top:20px;">PHPI would like to thank our donors for their funding support which helps multitudes of people to increase their income from $1 to $2 per day. Your MicroSeed gifts directed toward projects such as schools, medical clinics, etc are truly gifts of unconditional love without expectations of anything in return. Once you have registered, PHPI will setup a MicroSeed Account in your name so that you may see exactly where your money is going, what is paid back and how much principal you may use to support more loans or projects. It is PHPI’s goal to provide its donors with complete transparency concerning the use of their funds and PHPI’s low 8% overhead/administrative cost. </p>
</div>
<div class="span4">
<a href="/auth/setRegisterType/NGO" class="btn-primary btn-large">NGO</a>
<p style="padding-top:20px;">NGOs may apply for partner status to post microloans, meso/macro loans and Projects on PHPI’s website. Once you have requested partner status PHPI will forward to your organization an application to complete and returned to PHPI. The application will be reviewed and you will be contacted by a member of PHPI's staff to discuss your partnering request. A determination on partnering will be made within six (6) weeks from the date PHPI receives your application. Once approved, PHPI will work with you to train your staff on how to post loans and projects. </p>
</div>
<div class="span3">
<a href="/auth/setRegisterType/sNGO" class="btn-primary btn-large">Special HHPO's</a>
<p style="padding-top:20px;">Special HPPOs (Helping Hand Partner Organizations) may apply to PHPI for authorization to post microloans, meso/macro loans and projects to a special location on our website. Donors visiting your website may then travel to this special location via a link on your website provided by PHPI. Here they may view and fund ONLY your posted microloans, meso/macro loans and projects. Once you have registered PHPI will forward your organization an application to complete and return to PHPI. The application will be reviewed and you will be contacted by a member of PHPI's staff to discuss your partnering request. A determination on partnering will be made within 30 days from the date PHPI receives your application. After approval PHPI's donors together with your Special Donors will attempt to fund your postings.</p>
</div>

<div class="span4">
<a href="/auth/setRegisterType/sNGO" class="btn-primary btn-large">Special Funders</a>
<p style="padding-top:20px;">Large organizations such as banks and foundations interested in using PHPI to fund large numbers of loans will have the capacity to monitor and control the release of their funding or grants. The Special Funder will have its own MicroSeed Account. The Special Funder may elect to fund its own selections or have PHPI handle the selection process under their guidance. The MicroSeed Account will show the Special Funder the loans that they have funded together with the amounts repaid by the borrower to the NGO and deposited back into the Special Funder’s MicroSeed Account for re-lending. Thus, a Special Funder can see the ripple effect that its funding is creating. In addition, the Special Funder will be given administrative access to our website, enabling it to view the NGO’s actual loan repayment reports for all of their funding. We know of no other MFI that is providing this amount of transparency to large funders and foundations.
<br /><br />
With regard to grants or funding provided for projects where no return is expected, the Special Funder may use their MicroSeed Account to control the release of their funds on a multi-stage or phase-in basis pending satisfactory completion of each stage/phase. The project would be required to post pictures and a detailed description of each completed stage/phase for review by the Special Funder prior to release of any other funding.</p>
</div>
</div>

</div>