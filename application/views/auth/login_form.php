<?php
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'value' => set_value('login'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
if ($login_by_username AND $login_by_email) {
	$login_label = 'Email or login';
} else if ($login_by_username) {
	$login_label = 'Login';
} else {
	$login_label = 'Email';
}
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'size'	=> 30,
);
$remember = array(
	'name'	=> 'remember',
	'id'	=> 'remember',
	'value'	=> 1,
	'checked'	=> set_value('remember'),
	'style' => 'margin:0;padding:0',
);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'maxlength'	=> 8,
);
?>
<div class="row" style="padding:5px">
	
	<div class="span6">
	<p>
		  <img src="<?=base_url()?>images/how_php_works.jpg" alt="How PHP works">
        <a class="pull-right btn btn-warning" href="/pages/view/how_php_works/">Learn More</a>
        <div class="clearfix"></div>
        <br />
		<div class="row">
 
        	
		<div class="span2">
			<div style="background-color:black; padding:10px">
			<h4 style="color:white; text-align:center;">Micro Seed Loans</h4>
			<img src="<?=base_url()?>images/micro_seed.jpg" alt="How PHP works">
			</div>
			<a class="pull-right btn btn-warning" href="/pages/view/micro_loans/">Learn More</a>
		</div>
		<div class="span2">
			<div style="background-color:black; padding:10px">
			<h4 style="color:white; text-align:center;">Macro Seed Loans</h4>
			<img src="<?=base_url()?>images/macro.jpg" alt="How PHP works">
			</div>
			<a class="pull-right btn btn-warning" href="/pages/view/macro_loans/">Learn More</a>
		</div>
		<div class="span2">
			<div style="background-color:black; padding:10px">
		<h4 style="color:white; text-align:center;">Projects</h4>
			<img src="<?=base_url()?>images/projects.jpg" alt="How PHP works">
			</div>
			<a class="pull-right btn btn-warning" href="/pages/view/projects/">Learn More</a>
		</div>
        </div>
        
		
	</div>
	<div class="span5">
		<h3>Welcome to People Helping People</h3>
		<?php if($msg = $this->session->flashdata('message')):?>
			    <div class="alert alert-success" style="margin-top: 10px">
			    	<a class="close" data-dismiss="alert" href="#">&times;</a>
				    <h4 class="alert-heading"><?=$msg?></h4>
			    </div>
			    <?php endif;?>
			  
		<?php echo form_open($this->uri->uri_string()); ?>
			<?php if($warning = $this->session->flashdata('warning')):?>
		    <div class="alert alert-block">
		    <button type="button" class="close" data-dismiss="alert">&times;</button>
				<?=$warning?>
		    </div>
		    <?php endif;?>
			<label> <?php echo form_label($login_label, $login['id']); ?>
				<?php echo form_input($login); ?>
				<span style="color: red;"><?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?></span>
			</label>
			<label>
				<?php echo form_label('Password', $password['id']); ?>
				<?php echo form_password($password); ?>
				<span style="color: red;"><?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?></span>
			</label>
					
					
					
					<div class="clearfix"></div>
		<?php echo anchor('/auth/forgot_password/', 'Forgot password'); ?>
					
		<input class="btn btn-primary" type="submit" value="Sign In" name="submit">
		<?php echo form_close(); ?>
	</div>		
		
</div>
