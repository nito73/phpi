<style type="text/css">
	#editor{
		overflow:scroll;
		max-height:300px;
	}
	.client img{
		height: 200px;
	}	
</style>

<link rel="stylesheet" href="/imageupload/css/jquery.fileupload-ui.css">


<div style="padding:10px;" class="client">
        <h1>New Group/Co-Op Application</h1>
        <p class="lead"></p>
        <div>
        <form id="form" method="post" action="<?=base_url('groups/addProcess');?>">
			
  	
		
		<label>Group Photo</label>
		 <img src="" class="uploadimage"/>
	    
	    
		<span class="btn btn-success fileinput-button">
	        <i class="icon-plus icon-white"></i>
	        <span>Select files...</span>
		    <input id="fileupload" type="file" name="files[]" multiple>
	    </span>
	    
	   	    <div id="progress" style="width:75%;" class="progress progress-success progress-striped">
	        <div class="bar" ></div>
	    </div>
	    
	    <label>Group Information</label>
		<input class="input-xxlarge" type="text" name="name" id="name" placeholder="Group Name">
		
		<input class="input-xxlarge" type="text" name="location" id="location" placeholder="Location">
	</div>
		
	   
		<input type="hidden" name="pic" id="pic" placeholder="URL of Picture">
		<input type="hidden" name="copic" id="copic" placeholder="URL of Picture">

		<br />
    <input type="submit" class="btn" value="Add Group" />
  </div>
 </div>
  
  </form>      
        
		
        </div>
<script src="/imageupload/js/vendor/jquery.ui.widget.js"></script>
<script src="/imageupload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="/imageupload/js/jquery.fileupload.js"></script>
<script src="/imageupload/js/jquery.fileupload-image.js"></script>


<script>
$(function(){
	   	   'use strict';
    // Change this to the location of your server-side upload handler:
   var url = (window.location.hostname === 'blueimp.github.com' ||
                window.location.hostname === 'blueimp.github.io') ?
                '//jquery-file-upload.appspot.com/' : '<?=base_url();?>/imageupload/server/php/';

    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        disableImageResize: /Android(?!.*Chrome)|Opera/
        .test(window.navigator && navigator.userAgent),
    imageMaxWidth: 250,
    imageMaxHeight: 250,
    imageCrop: true, // Force cropped images
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
              	
                $('#pic').val(file.url);
                $('.uploadimage').attr('src', file.url);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .bar').css(
                'width',
                progress + '%'
            );
        }
    });
    

    
  

  });
  
  


</script>
