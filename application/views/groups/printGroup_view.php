<?php 
	$ngo_currency = "";
	?>
<?php 
	$groupLoan = $this->Group_model->view_group_loan($groupID);

?>

<style type="text/css">
	.total td{
		font-weight: bold;
	}
	img{ height: 100px; float: left;}
	table tr td{border: 1px solid black; }
	thead tr td{border:2px solid black; font-weight: bold; background-color: yellow; }
	
	table{ border-collapse: collapse; }	
	.client tr td{border: none;}
	.client img{ height: 150px;}
</style>
<?php $query = $this->Group_model->viewGroup($groupID);

    foreach($query as $row):?>
    <table class="client">
    <tr style="padding-top:10px;">
    	<td>
   <img src="<?php if($row->group_photo != "") { echo $row->group_photo;} else { echo base_url('images') . "/no_image.jpg";}?>">
   <br />
   
  
     <strong> Group Name</strong> <?=$row->name?><br />
     <strong> Location</strong> <?=$row->location?> <br />

    </tr>
    </table>
  
    <div class="clearfix"></div>
<?php endforeach;?>

<h2>Group Members</h2>
		
<table>	
	<thead>
		<tr>
			<td>Photo</td>
			<td>Name</td>
			<td>Group Role</td>
			<td>Mobile Phone</td>
			<td>Address </td>
		</tr>
	</thead>
	<tbody>			
<?php $query = $this->Group_model->viewGroupMembers($groupID);

    foreach($query as $row):?>
    <tr>
    	<td>
   <img src="<?php if($row->pics != "") { echo $row->pics;} else { echo base_url('images') . "/no_image.jpg";}?>">
   </td>
   	<td><?=$row->name?></td>
   		<td><?=$row->role?></td>
   		<td><?=$row->cell?></td>
   		<td><?=$row->address?></td>
   		   		
    </tr>
    
  
  <?php endforeach;?>
	</tbody>
	

	</table>			
	
	
	  
		    <?php if($groupLoan):?>
		    <h2>Group Loan</h2>
		    <?php 
	            		$ngo_currency = $groupLoan->currency;
		            	$loan_terms = $groupLoan->loanterms;
		            	$loan_type = $groupLoan->loantermstype;
		            	$loandate = $groupLoan->loandate;	
		            	$loaninterest = $groupLoan->loaninterest;
		            	$totalrequested = $groupLoan->amount;
		            	$rate= $groupLoan->rate;
		            	$projectID = $groupLoan->id;
		            	
		            	$total = $this->Ngo_model->get_total_funded($projectID);
	            		?>
	            		
	            <?php
		    		if($loaninterest){
		    			$loaninterest = ($loaninterest/100)+1;
		    			
		    			$ngoprincipal = ($totalrequested*$rate)/$loan_terms;
			    		$principal = ($totalrequested)/$loan_terms;
			    		
		    			
		    			$totalrequested = $totalrequested * $loaninterest;
		    		
		    			$justinterest = $loaninterest -1;	
		    		} 
			    		$ngopayments = ($totalrequested*$rate)/$loan_terms;
			    		$payments = ($totalrequested)/$loan_terms;
			    		
			    		$ngointerest = $ngoprincipal * $justinterest;
			    		$interest = $principal * $justinterest;
			    		
			    		?>

	            		
	
			
             <strong>Loan Name </strong><?=$groupLoan->title?><br />
          <strong>Location</strong> <?=$groupLoan->location?> <br />
          <strong>Catergoy</strong> <?=$groupLoan->category?><br />
		  <strong>Posted Date</strong> <?=date('d M y' , strtotime($groupLoan->postdate))?> <br />
		  <strong>Exchange Rate</strong> $ 1 USD -> <?=$rate?> <?=$ngo_currency?><br />
		  
		  <?php if($repaymenttotal = $this->Ngo_model->total_repayment_amount($projectID)):?>
		  	<strong> Loan </strong> <br />
		  	 $<?=money_format('%(#10n', $groupLoan->amount)?> USD  / <?=money_format('%(#10n', $groupLoan->amount*$rate)?> <?=$ngo_currency?>  (Principal)<br />
		  	 $<?=money_format('%(#10n', $groupLoan->amount*($loaninterest/100))?> USD  / <?=money_format('%(#10n', ($groupLoan->amount*$rate)*($loaninterest/100))?> <?=$ngo_currency?> (Interest <?=$loaninterest?>%) <br />		
		  	   <strong>Total Due</strong>
		  <br />
		  <?php $percenttotal = floor(($repaymenttotal['phptotal']/$groupLoan->amount)*100);?>

			  $<?=money_format('%(#10n', $repaymenttotal['phptotal'])?> USD / $<?=money_format('%(#10n', $groupLoan->amount*(($loaninterest/100)+1))?> USD (<?=$percenttotal?>%)<br />
			  <?=money_format('%(#10n', $repaymenttotal['ngototal'])?> <?=$ngo_currency?> / <?=money_format('%(#10n', ($groupLoan->amount*$rate)*(($loaninterest/100)+1))?> <?=$ngo_currency?> (<?=$percenttotal?>%)
			 <br />
		  <?php else:?>
			  <?php if ($groupLoan->fundingsource == "PHP"):?>
			  <?php $percenttotal = floor(($total/$groupLoan->amount)*100);?>
				  <strong>Amount</strong> $<?=$total?>/$<?=$groupLoan->amount?> USD  (<?=$percenttotal?>% Funded) <br />
	          <?php else:?>
	          	  <strong>Repayment Amount</strong>
			  <br />
			  $<?=money_format('%(#10n', $repaymenttotal['phptotal'])?> USD / $<?=$groupLoan->amount?> USD <br />
			  <?=money_format('%(#10n', $repaymenttotal['ngototal'])?> <?=$ngo_currency?> / <?=$groupLoan->amount*$rate?> <?=$ngo_currency?>
			  <?php $percenttotal = floor(($repaymenttotal['phptotal']/$groupLoan->amount)*100);?>
		


	          <?php endif;?>
	          
		   
		  <?php endif;?>
		  
		  
  		
    <?php
	            		
	            		$loanBalance = array(0,0);
	            		$totalpaid = array(0,0);
	            		
	            	?>
	            	
	            	<?php $paymentCount = 0;?>	
		    	<?php for($x=0; $x < $loan_terms; $x++):?>
		    	
		    	<?PHP $datediff = $x * $loan_type;
			    	$paymentCount++;
		    	?>
		    	<?php if (strtotime("today ") <= strtotime($loandate . ' +' . $datediff . ' Weeks' )){
		    			if($x != 0){
		    			$lastdatediff = ($x-1)* $loan_terms;
		    			$lastpaymentdate = strtotime($loandate . ' +' . $lastdatediff . ' Weeks' );
		    			} else {
			    			$lastpaymentdate = 	strtotime($loandate . ' +' . $datediff . ' Weeks' );
		    			}
		    			
		    			
		    			$totalamountdue = money_format('%(#10n', $payments);
		    			$totalamountdue = money_format('%(#10n', $ngopayments);
		    			
		    			$paymentduedate = strtotime($loandate . ' +' . $datediff . ' Weeks' );
		    			
		    			break;
		    			
		    		}
		    	?>
		    	
		    	
		   
		    	<?php endfor;?>
			  <br />
		    			    	

		    <strong>Last Payment Status</strong>
		     <strong>Payment Due Date</strong> <?=date("m/d/y",$paymentduedate)?><br />
	        <strong>Minimum Payment Due </strong> <?=money_format('%(#10n', $ngopayments) . " " . $ngo_currency;?>  /  $<?=money_format('%(#10n', $payments)?> USD <br />
	        
	        <?php 
	        	$minMustPaid = $paymentCount * $ngopayments ;
	        	$late_balance = $minMustPaid - $loanBalance[0];
	        	
	        if($late_balance <= $ngopayments ):?>
	        
	        <strong>Amount Due</strong>
	        <?php if(($ngopayments-$totalpaid[0]) < 0):?>
	        0.00 <?=$ngo_currency?> / $0.00 USD
	        <?php else :?>
	        	
	         <?=money_format('%(#10n', $ngopayments-$totalpaid[0]) . " " . $ngo_currency;?>  /  $<?=money_format('%(#10n', $payments-$totalpaid[1])?> USD
	         
	         <?php endif;?> <br />
	       
	      
	        <?php else:?>
	        <strong>Past Balance </strong><?=money_format('%(#10n', $late_balance-$ngopayments) . " " . $ngo_currency;?>  /  $<?=money_format('%(#10n', ($late_balance/$rate)-$payments)?> USD<br />
	        
	         <strong>Amount Due</strong><?=money_format('%(#10n', $late_balance) . " " . $ngo_currency;?>  /  $<?=money_format('%(#10n', $late_balance/$rate)?> USD
	       <?php endif;?>
	       <br />
	       
		    <table style="margin-top:20px;">
	            <thead>
	            <tr>
	            	<td>Date</td>
	            	<td><?=$ngo_currency?> Amount</td>
	            	<td>USD Amount</td>
	            	<td>Staff</td>
	            </tr>
	            </thead>
	            <tbody>
	            
			  <?php if($repaymentquery = $this->Ngo_model->view_repayment($groupLoan->id)):?>

	            	
	            		<?php foreach($repaymentquery as $row):?>
	            		
	            		<?php if($row->paymenttype == "Loan"):?>
	            		
	            		
	            		<tr>
	            			<td><?=date('d M y', strtotime($row->date))?></td>
	            			<td><?=money_format('%(#10n', $row->ngoamount) . " " . $ngo_currency;?> </td>
	            			<td>$<?=money_format('%(#10n', $row->amount)?> USD	</td>
	            				<td><?=$this->Ngo_model->get_user_name($row->userid);?> </td>
	            				            			
	            		</tr>
	            		<?php
		            		$loanBalance[0]+= $row->ngoamount;
		            		$loanBalance[1]+= $row->amount;

	            		if ($paymentduedate == $lastpaymentdate ){
		            		if(strtotime($row->date) >= $paymentduedate){
			            		
			            		$totalpaid[0]+= $row->ngoamount;
			            		$totalpaid[1]+= $row->amount;
		            		}
		            		} else {
			            		if(strtotime($row->date) >= $paymentduedate && strtotime($row->date) <= $lastpaymentdate ){
			            		
			            		$totalpaid[0]+= $row->ngoamount;
			            		$totalpaid[1]+= $row->amount;
			            		}	
		            		}
	            		?>
	            		<?php endif;?>
	            		<?php endforeach;?>
	            	
	            			
	            		<?php else:?>
	            		<tr>
	            			<td colspan="3">No Payments Made </td>
	            		</tr>
	            		<?php endif;?>
	            		

	            </tbody>
	        </table>
	        
	        <strong>Loan Balance</strong><?=money_format('%(#10n', (($totalrequested*$rate) *(($loaninterest/100)+1))-$loanBalance[0]) . " " . $ngo_currency;?>  / $<?=money_format('%(#10n', (($totalrequested)*(($loaninterest/100)+1))-$loanBalance[1])?> USD	<br />
	        
<br />
<br />
	       	        
	           	   
				
			    <?php endif;?>
