<?php 
	$ngo_currency = "";
	?>
	
	<?php 
	$groupLoan = $this->Group_model->view_group_loan($groupID);

?>
<style type="text/css">
	.total td{
		font-weight: bold;
	}
	img{ height: 100px; float: left;}
	table tr td{border: 1px solid black; }
	thead tr td{border:2px solid black; font-weight: bold; background-color: yellow; }
	
	table{ border-collapse: collapse; }	
	.client tr td{border: none;}
	.client img{ height: 150px;}
</style>


<h2>Loan Meeting Attendance Sheet</h2>

<?php $query = $this->Group_model->viewGroup($groupID);

    foreach($query as $row):?>
    	<h3><strong>Group Name:</strong> <?=$row->name?></h3>
    <?php endforeach;?>

<h4 style="border-bottom:2px solid black; width:450px;">Date</h4>
<h4 style="border-bottom:2px solid black; width:450px;">Payment Recived</h4>


 <?php if($groupLoan):?>
		    <?php 
	            		$ngo_currency = $groupLoan->currency;
		            	$loan_terms = $groupLoan->loanterms;
		            	$loan_type = $groupLoan->loantermstype;
		            	$loandate = $groupLoan->loandate;	
		            	$loaninterest = $groupLoan->loaninterest;
		            	$justloaninterest = $groupLoan->loaninterest;
		            	$totalrequested = $groupLoan->amount;
		            	$rate= $groupLoan->rate;
		            	$projectID = $groupLoan->id;
		            	
		            	$total = $this->Ngo_model->get_total_funded($projectID);
	            		?>
	            		
	            <?php
		    			$ngoprincipal = ($totalrequested*$rate)/$loan_terms;
			    		$principal = ($totalrequested)/$loan_terms;
			    	
		    		if($loaninterest){
		    			$loaninterest = ($loaninterest/100)+1;
		    			
		    			
		    			
		    			$totalrequested = $totalrequested * $loaninterest;
		    		
		    			$justinterest = $loaninterest -1;	
		    		} else {
			    		$justinterest = 1;
		    		}
			    		$ngopayments = ($totalrequested*$rate)/$loan_terms;
			    		$payments = ($totalrequested)/$loan_terms;
			    		
			    		$ngointerest = $ngoprincipal * $justinterest;
			    		$interest = $principal * $justinterest;
			    		
			    		?>


			    		    <?php
	            		
	            		$loanBalance = array(0,0);
	            		$totalpaid = array(0,0);
	            		
	            	?>
	            	
	            	<?php $paymentCount = 0;?>	
		    	<?php for($x=0; $x < $loan_terms; $x++):?>
		    	
		    	<?PHP $datediff = $x * $loan_type;
			    	$paymentCount++;
		    	?>
		    	<?php if (strtotime("today ") <= strtotime($loandate . ' +' . $datediff . ' Weeks' )){
		    			if($x != 0){
		    			$lastdatediff = ($x-1)* $loan_terms;
		    			$lastpaymentdate = strtotime($loandate . ' +' . $lastdatediff . ' Weeks' );
		    			} else {
			    			$lastpaymentdate = 	strtotime($loandate . ' +' . $datediff . ' Weeks' );
		    			}
		    			
		    			
		    			$totalamountdue = money_format('%(#10n', $payments);
		    			$totalamountdue = money_format('%(#10n', $ngopayments);
		    			
		    			$paymentduedate = strtotime($loandate . ' +' . $datediff . ' Weeks' );
		    			
		    			break;
		    			
		    		}
		    	?>
		    	
		    	
		   
		    	<?php endfor;?>
			  
			  
			  <?php if($repaymentquery = $this->Ngo_model->view_repayment($groupLoan->id)):?>

	            	
	            		<?php foreach($repaymentquery as $row):?>
	            		
	            		
	            		<?php
		            		$loanBalance[0]+= $row->ngoamount;
		            		$loanBalance[1]+= $row->amount;

	            		if ($paymentduedate == $lastpaymentdate ){
		            		if(strtotime($row->date) >= $paymentduedate){
			            		
			            		$totalpaid[0]+= $row->ngoamount;
			            		$totalpaid[1]+= $row->amount;
		            		}
		            		} else {
			            		if(strtotime($row->date) >= $paymentduedate && strtotime($row->date) <= $lastpaymentdate ){
			            		
			            		$totalpaid[0]+= $row->ngoamount;
			            		$totalpaid[1]+= $row->amount;
			            		}	
		            		}
	            		?>
	            		<?php endforeach?>
	        
	        <strong>Loan Balance</strong><?=money_format('%(#10n', (($totalrequested*$rate) *(($loaninterest/100)+1))-$loanBalance[0]) . " " . $ngo_currency;?><br />
	        <strong>Payment Due Date</strong> <?=date("m/d/y",$paymentduedate)?><br />
	        <strong>Minimum Payment Due </strong> <?=money_format('%(#10n', $ngopayments) . " " . $ngo_currency;?>  <br />
	        
	        <?php 
	        	$minMustPaid = $paymentCount * $ngopayments ;
	        	$late_balance = $minMustPaid - $loanBalance[0];
	        	
	        if($late_balance <= $ngopayments ):?>
	        
	        <strong>Amount Due</strong>
	        <?php if(($ngopayments-$totalpaid[0]) < 0):?>
	        0.00 <?=$ngo_currency?>
	        <?php else :?>
	        	
	         <?=money_format('%(#10n', $ngopayments-$totalpaid[0]) . " " . $ngo_currency;?>  	         <?php endif;?> <br />
	       
	      
	        <?php else:?>
	        <strong>Past Balance </strong><?=money_format('%(#10n', $late_balance-$ngopayments) . " " . $ngo_currency;?> <br />
	        
	         <strong>Amount Due</strong><?=money_format('%(#10n', $late_balance) . " " . $ngo_currency;?>  	       <?php endif;?>
	           <?php endif;?>
	           <?php endif;?>
	       
<table>	
	<thead>
		<tr>
			<td>Photo</td>
			<td>Name</td>
			<td>Present/Signature</td>
			<td>Loan Payment Collected</td>
			<td>Current Savings Account Balance </td>
			<td>Savings Withdrawal</td>
			<td>Savings Deposit </td>
		</tr>
	</thead>
	<tbody>			
<?php $query = $this->Group_model->viewGroupMembers($groupID);

    foreach($query as $row):?>
    <tr>
    	<td>
   <img src="<?php if($row->pics != "") { echo $row->pics;} else { echo base_url('images') . "/no_image.jpg";}?>">
   </td>
   	<td><?=$row->name?></td>
   		
   		<td style="width:200px"></td>
   		<td style="width:200px"></td>
   		<td style="width:200px"><?=money_format('%(#10n', $this->Group_model->client_savings_account($row->id)) . " " . $ngo_currency;?></td>  		
   		<td style="width:200px"></td>
   		<td style="width:200px"></td>  
    </tr>
    
  
  <?php endforeach;?>
	</tbody>
	

	</table>			