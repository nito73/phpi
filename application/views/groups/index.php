<?php if($this->tank_auth->get_usertype() != "loan_officer" ):?>
<?php $this->load->view('/dashboard/sidebar_view');?>
<?php endif;?>


   <style type="text/css">
   
   </style>
       <!-- Jumbotron -->
           
      <div style="padding:10px;">
      	<div>
         <a href="<?=site_url("groups/add");?>" class="btn pull-right btn-success">Add Groups</a>

        </div>  
        <br />
        <br />
        
        
            <div class="view_data well">
      <strong>View By</strong>
       Location
        <select name="location" id="location">
        	<option value="All">All</option>
        	
        	
        	
        	<?php $locquery = $this->Group_model->group_location($this->tank_auth->get_orgid());
    if ($locquery):?>
    <?php foreach($locquery as $row):?>
    	<option value="<?=$row->location?>"><?=$row->location?></option>
    <?php endforeach?>
    <?php endif;?>


        </select>
        
 
			
      </div>
        
        <?php $query = $this->Dashboard_model->view_org_projects($this->tank_auth->get_orgid(), $location);
	            	$total = 0;
	            	?>

            <table class="table" id="myProjectsDataTable">
	            <thead>
	            <tr>
	            	<td>ID</td>
	            	<td>Group Name</td>
	            	<td>Location</td>
	            	
	            	<td>View</td>
	            </tr>
	            </thead>
	            <tbody>
	            
	            </tbody>
    </table>
    	            
    	                
      </div>
      
      <script>
      
      $(function()
{
		$("#location").change(function(event){
			
			event.preventDefault();
			
			oTable.fnDraw();
			
		});
	
		
        
 
        oTable = $('#myProjectsDataTable').dataTable( {
                "bProcessing": true,
                "bServerSide": true,
                "bJQueryUI": true,
                "sPaginationType": "bootstrap",
                "sAjaxSource": "<?php echo site_url('groups/myGroupsDatatable')?>",
                "fnServerData": function ( sSource, aoData, fnCallback ) {
                        $.ajax( {
                                "dataType": 'json',
                                "type": "POST",
                                "url": sSource,
                                "data": aoData,
                                "success": fnCallback
                        } );
                },
                "aaSorting": [[0,'DESC']],
                fnServerParams: function ( aoData ) {
                        aoData.push( { name: 'location', value: $('#location').val() } );
                   
                },
                "bLengthChange": false
} );
       
 
});
      </script>
<?php //$this->load->view('/dashboard/footer_view');?>