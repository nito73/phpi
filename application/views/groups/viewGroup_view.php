<?php 
	$groupLoan = $this->Group_model->view_group_loan($groupID);

?>
<style type="text/css">
	.total td{
		font-weight: bold;
	}
</style>
<?php $query = $this->Group_model->viewGroup($groupID);

    foreach($query as $row):?>
  
  <div class="pull-right"style="padding:5px;">
	
	  
	      <div class="btn-group">
    <button class="btn">Client Action</button>
    <button class="btn dropdown-toggle" data-toggle="dropdown">
    <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
	     <li>
  <a href="<?=site_url('groups/printGroup/' . $groupID);?>" target="_blank">Print Group Summery</a>
	  
	  	     </li>

	     <li>
		     <a  href="<?=site_url('groups/printGroupAttendance/' . $groupID);?>" target="_blank">Print Group Attedance Sheet</a>
	  
	  
	     </li>
    </ul>
    </div>
    </div>

  <div style="padding:5px">
	        <a class="btn btn-success" href="<?=site_url('groups/');?>" > <i class="icon-chevron-left"></i> Back</a>

    </div>
  <div style="padding:10px;padding-top:40px;">
  
  	<div class="row">
  	  <div class="span3">
  	  	<a href="#" class="thumbnail">
  	  		<img src="<?php if($row->group_photo != "") { echo $row->group_photo;} else { echo base_url('images') . "/no_image.jpg";}?>">
	  	</a>
	  </div>
    <div class="span5">
			
          
                   <dl class="dl-horizontal">
		  <dt>Group Name</dt>
		  <dd><?=$row->name?></dd>
	
		 
		  <dt>Collection Location</dt>
		  <dd><?=$row->location?></dd>
		 
  		</dl>
          
    </div>
	  	
  </div>
  </div>
    <div class="clearfix"></div>
<?php endforeach;?>
            <?php if($msg = $this->session->flashdata('message')):?>
            	    <div class="alert alert-<?=$msg['type']?>" style="margin-top: 10px">
            	    	<a class="close" data-dismiss="alert" href="#">&times;</a>
            		    <strong class="alert-heading"><?=$msg['body']?></strong>
            	    </div>
            	    <?php endif;?>
            	  
    <div class="tabbable" style="padding:25px;"> <!-- Only required for left/right tabs -->
	    <ul class="nav nav-tabs">
			<li class="active"><a href="#tabmembers" data-toggle="tab">Group Members</a></li>
			<li><a href="#tabloan" data-toggle="tab">Group Loan</a></li>
			<?php if($groupLoan):?>
		    			<li><a href="#tabLoanSchedule" data-toggle="tab">Payment Schedule</a></li>
			<?php endif;?>
			
	    </ul>
	    
	    <div class="tab-content">
	    
	    
	    	
	    	<div class="tab-pane active" id="tabmembers">
			
			<div class="memalert"></div>
			<a class="btn btn-success pull-left " id="addMembers"><i class="icon-plus"></i> Add Members</a>
			
				 <table class="table" id="myGroupMembersDatatable">
	            <thead>
	            <tr>
	            	<td>Name</td>
	            	<td>Role</td>
	            	<td>Phone Number</td>
	            	<td>Address</td>
	            	<td></td>
	            	
	            </tr>
	            </thead>
	            <tbody>
	            
	            </tbody>
    </table>
				
			</div>
			
			
			
			<div class="tab-pane" id="tabloan">
			 <?php if($groupLoan):?>
		    <?php 
	            		$ngo_currency = $groupLoan->currency;
		            	$loan_terms = $groupLoan->loanterms;
		            	$loan_type = $groupLoan->loantermstype;
		            	$loandate = $groupLoan->loandate;	
		            	$loaninterest = $groupLoan->loaninterest;
		            	$justloaninterest = $groupLoan->loaninterest;
		            	$totalrequested = $groupLoan->amount;
		            	$rate= $groupLoan->rate;
		            	$projectID = $groupLoan->id;
		            	
		            	$total = $this->Ngo_model->get_total_funded($projectID);
	            		?>
	            		
	            <?php
		    			$ngoprincipal = ($totalrequested*$rate)/$loan_terms;
			    		$principal = ($totalrequested)/$loan_terms;
			    	
		    		if($loaninterest){
		    			$loaninterest = ($loaninterest/100)+1;
		    			
		    			
		    			
		    			$totalrequested = $totalrequested * $loaninterest;
		    		
		    			$justinterest = $loaninterest -1;	
		    		} else {
			    		$justinterest = 1;
		    		}
			    		$ngopayments = ($totalrequested*$rate)/$loan_terms;
			    		$payments = ($totalrequested)/$loan_terms;
			    		
			    		$ngointerest = $ngoprincipal * $justinterest;
			    		$interest = $principal * $justinterest;
			    		
			    		?>

	            		
		    <div class="row" style="padding:5px;">
    <div class="span3">
    <a href="#" class="thumbnail">
      <img src="<?php if($groupLoan->pics != "") { echo $groupLoan->pics;} else { echo base_url('images') . "/no_image.jpg";}?>">
    </a>
    </div>
    
    <div class="span8">
			
          <h1><?=$groupLoan->title?></h1>
          <h4><?=$groupLoan->location?> | <?=$groupLoan->category?></h4>
         
          
                   <dl class="dl-horizontal">
		
		  <dt>Posted Date</dt>
		  <dd><?=date('d M y' , strtotime($groupLoan->postdate))?></dd>
		  <dt>Exchange Rate</dt>
		  <dd>$ 1 USD -> <?=$rate?> <?=$ngo_currency?></dd>
		  
		  <?php if($repaymenttotal = $this->Ngo_model->total_repayment_amount($projectID)):?>
		  	<dt>Loan </dt>
		  		<dd>$<?=money_format('%(#10n', $groupLoan->amount)?> USD  / <?=money_format('%(#10n', $groupLoan->ngoamount)?> <?=$ngo_currency?>  (Principal)<br />
		  		$<?=money_format('%(#10n', $groupLoan->amount*($loaninterest))?> USD  / <?=money_format('%(#10n', ($groupLoan->amount*$rate)*($loaninterest))?> <?=$ngo_currency?> (Interest <?=$justloaninterest?>%) 
		  		</dd>
		  	<dt>Total Due</dt>
		  	<dd>
		  				  <?php $percenttotal = floor(($repaymenttotal['phptotal']/($groupLoan->amount*($loaninterest)))*100);?>

			  $<?=money_format('%(#10n', $repaymenttotal['phptotal'])?> USD / $<?=money_format('%(#10n', $groupLoan->amount*($loaninterest))?> USD (<?=$percenttotal?>%)<br />
			  <?=money_format('%(#10n', $repaymenttotal['ngototal'])?> <?=$ngo_currency?> / <?=money_format('%(#10n', ($groupLoan->amount*$rate)*($loaninterest))?> <?=$ngo_currency?> (<?=$percenttotal?>%)
		


			   <div class="progress">
       
				<div class="bar" style="width: <?=$percenttotal?>%;"></div>
				   <label><?=$percenttotal?>% Returned</label>
				</div>
            </div>
            
			  </dd>
		  <?php else:?>
			  <?php if ($groupLoan->fundingsource == "PHP"):?>
			  <?php $percenttotal = floor(($total/$groupLoan->amount)*100);?>
				  <dt>Amount</dt>
				  <dd>$<?=$total?>/$<?=$groupLoan->amount?> USD
			  <div class="progress">
	       
					<div class="bar" style="width: <?=$percenttotal?>%;"></div>
					   <label><?=$percenttotal?>% Funded</label>
					</div>
	            </div>
	            </dd>
	          <?php else:?>
	          	  <dt>Repayment Amount</dt>
			  <dd>$<?=money_format('%(#10n', $repaymenttotal['phptotal'])?> USD / $<?=$groupLoan->amount?> USD <br />
			  <?=money_format('%(#10n', $repaymenttotal['ngototal'])?> <?=$ngo_currency?> / <?=$groupLoan->amount*$rate?> <?=$ngo_currency?>
			  <?php $percenttotal = floor(($repaymenttotal['phptotal']/$groupLoan->amount)*100);?>
		


			   <div class="progress">
       
				<div class="bar" style="width: 0%;"></div>
				   <label>0% Returned</label>
				</div>
            </div>
	          <?php endif;?>
	          
		   
		  <?php endif;?>
		  
		  
  		</dl>
          
         
  </div>
    <?php
	            		
	            		$loanBalance = array(0,0);
	            		$totalpaid = array(0,0);
	            		
	            	?>
	            	
	            	<?php $paymentCount = 0;?>	
		    	<?php for($x=0; $x < $loan_terms; $x++):?>
		    	
		    	<?PHP $datediff = $x * $loan_type;
			    	$paymentCount++;
		    	?>
		    	<?php if (strtotime("today ") <= strtotime($loandate . ' +' . $datediff . ' Weeks' )){
		    			if($x != 0){
		    			$lastdatediff = ($x-1)* $loan_terms;
		    			$lastpaymentdate = strtotime($loandate . ' +' . $lastdatediff . ' Weeks' );
		    			} else {
			    			$lastpaymentdate = 	strtotime($loandate . ' +' . $datediff . ' Weeks' );
		    			}
		    			
		    			
		    			$totalamountdue = money_format('%(#10n', $payments);
		    			$totalamountdue = money_format('%(#10n', $ngopayments);
		    			
		    			$paymentduedate = strtotime($loandate . ' +' . $datediff . ' Weeks' );
		    			
		    			break;
		    			
		    		}
		    	?>
		    	
		    	
		   
		    	<?php endfor;?>
			  
		    			    	

		    <strong> Loan Payment</strong><br />
		    <table class="table table-hover">
	            <thead>
	            <tr>
	            	<td>Date</td>
	            	<td><?=$ngo_currency?> Amount</td>
	            	<td>USD Amount</td>
	            	<td>Staff</td>
	            </tr>
	            </thead>
	            <tbody>
	            
			  <?php if($repaymentquery = $this->Ngo_model->view_repayment($groupLoan->id)):?>

	            	
	            		<?php foreach($repaymentquery as $row):?>
	            		
	            		<?php if($row->paymenttype == "Loan"):?>
	            		
	            		
	            		<tr>
	            			<td><?=date('d M y', strtotime($row->date))?></td>
	            			<td><?=money_format('%(#10n', $row->ngoamount) . " " . $ngo_currency;?> </td>
	            			<td>$<?=money_format('%(#10n', $row->amount)?> USD	</td>
	            				<td><?=$this->Ngo_model->get_user_name($row->userid);?> </td>
	            				            			
	            		</tr>
	            		<?php
		            		$loanBalance[0]+= $row->ngoamount;
		            		$loanBalance[1]+= $row->amount;

	            		if ($paymentduedate == $lastpaymentdate ){
		            		if(strtotime($row->date) >= $paymentduedate){
			            		
			            		$totalpaid[0]+= $row->ngoamount;
			            		$totalpaid[1]+= $row->amount;
		            		}
		            		} else {
			            		if(strtotime($row->date) >= $paymentduedate && strtotime($row->date) <= $lastpaymentdate ){
			            		
			            		$totalpaid[0]+= $row->ngoamount;
			            		$totalpaid[1]+= $row->amount;
			            		}	
		            		}
	            		?>
	            		<?php endif;?>
	            		<?php endforeach;?>
	            	
	            			
	            		<?php else:?>
	            		<tr>
	            			<td colspan="3">No Payments Made </td>
	            		</tr>
	            		<?php endif;?>
	            		

	            </tbody>
	        </table>
	        
	        <strong>Loan Balance</strong><?=money_format('%(#10n', (($totalrequested*$rate) *(($loaninterest/100)+1))-$loanBalance[0]) . " " . $ngo_currency;?>  / $<?=money_format('%(#10n', (($totalrequested)*(($loaninterest/100)+1))-$loanBalance[1])?> USD	<br />
	        
<br />
<br />
	        <strong>Payment Due Date</strong> <?=date("m/d/y",$paymentduedate)?><br />
	        <strong>Minimum Payment Due </strong> <?=money_format('%(#10n', $ngopayments) . " " . $ngo_currency;?>  /  $<?=money_format('%(#10n', $payments)?> USD <br />
	        
	        <?php 
	        	$minMustPaid = $paymentCount * $ngopayments ;
	        	$late_balance = $minMustPaid - $loanBalance[0];
	        	
	        if($late_balance <= $ngopayments ):?>
	        
	        <strong>Amount Due</strong>
	        <?php if(($ngopayments-$totalpaid[0]) < 0):?>
	        0.00 <?=$ngo_currency?> / $0.00 USD
	        <?php else :?>
	        	
	         <?=money_format('%(#10n', $ngopayments-$totalpaid[0]) . " " . $ngo_currency;?>  /  $<?=money_format('%(#10n', $payments-$totalpaid[1])?> USD
	         
	         <?php endif;?> <br />
	       
	      
	        <?php else:?>
	        <strong>Past Balance </strong><?=money_format('%(#10n', $late_balance-$ngopayments) . " " . $ngo_currency;?>  /  $<?=money_format('%(#10n', ($late_balance/$rate)-$payments)?> USD<br />
	        
	         <strong>Amount Due</strong><?=money_format('%(#10n', $late_balance) . " " . $ngo_currency;?>  /  $<?=money_format('%(#10n', $late_balance/$rate)?> USD
	       <?php endif;?>
	        
	            <div class="well">
				    <p>By entering a payment,  Please enter monetary value in <?=$ngo_currency?></p>
				    <?php 
				    	$projectStatus = $this->Ngo_model->get_project_status($projectID);
				    if($projectStatus != 'Pending Closing' && $projectStatus != 'Closed' ):?>
				    <form method="post" action="<?=site_url('ngo/repaymentProcess')?>">
					    <label>Payment Date </label><input type="text" name="date" class="datepicker" value="<?=date('d-m-Y')?>"/>
						<input type="hidden" class="input-xxlarge" name="paymenttype" id="paymenttype" value="Loan" />
						<label>Amount <?=$ngo_currency?></label><input type="text" name="amount" />
					    <input type="hidden" name="projectid" value="<?=$projectID?>" />
					    <input type="hidden" name="groupid" value="<?=$groupID?>" />
					    <input type="hidden" name="rate" value="<?=$rate?>" />
					    <input class="btn btn-success"type="submit" value="Send" />
					</form>
				    
				    <?php else:?>
				    <h2>This Loan is <?=$projectStatus?></h2>
				    <?php endif;?>
				    <a href="<?=site_url('ngo/closeProject/' . $projectID)?>" class="btn btn-warning" > Close Loan</a>
			    </div>
		    
	   
				<?php else:?>			
						
								<?php $ngo_currency = $organization->currency;?>
				<a class="btn btn-primary btn-large" href="<?=site_url('groups/addLoan/' . $groupID)?>" > Add Loan </a>
                	<p style="padding-top:20px;"><ul>
    <li>
    <span style="font-size: 13px;"><span style="font-weight: bold;">Food Industry Business Sector</span></span>- Businesses that produce or sell packaged manufactured food out of a store such as Restaurants, Bakery, Ice Cream, etc.</li>
    <li>
    <span style="font-size: 13px;"><span style="font-weight: bold;">Agriculture Business Sector</span></span>- Businesses that grow crops, raise animals, etc. off the land</li>
    <li>
    <span style="font-size: 13px;"><span style="font-weight: bold;">Service Industry Business Sector</span></span> - Businesses that provide services such as repairs, hair dressers, communication centers, etc</li>
    <li>
    <span style="font-size: 13px;"><span style="font-weight: bold;">Small Trade Industry Business Sector</span></span> - Any type of product one a singular to one basis such as Beans, T-shirts, Yams, Jewelry, etc.</li>
    <li>
    <span style="font-size: 13px;"><span style="font-weight: bold;">Manufacturing Business Sector</span></span>- These businesses are defined as capable of producing a product such as cement block makers, seamstress, furniture, etc.</li>
</ul>
</p> 

<br />

<a class="btn btn-primary btn-large" href="<?=site_url('groups/addNonLoan/' . $groupID)?>" > Add Non-PHP Loan </a>
                	<p style="padding-top:20px;">
                	
               
                		Do you have a Loan that is funded by another organization that you want our system to track payments.
                	
</p> 
	<?php endif;?>

			</div>
			
			<?php if($groupLoan):?>
			 <div class="tab-pane" id="tabLoanSchedule">
					    Payment Schedule<br />
		    <table class="table table-hover">
		    <thead>
		    	<tr>
		    		<td>Date</td>
		    		<td>Principal</td>
					<td>Interest (<?=$justinterest*100?>%)</td>
		    		<td>Payment</td>
		    	</tr>
		    </thead>
		    <tbody>	
		    	
		    	<?php for($x=0; $x < $loan_terms; $x++):?>
		    	<?PHP $datediff = $x * $loan_type;?>
		    	<tr>
		    		<td><?=date("m/d/y", strtotime($loandate . ' +' . $datediff . ' Weeks' ));?></td>
					<td><?=money_format('%(#10n', $ngoprincipal) . " " . $ngo_currency;?> <br />
						$ <?=money_format('%(#10n', $principal);?> USD</td>
			    		<td><?=money_format('%(#10n', $ngointerest) . " " . $ngo_currency;?> <br />
						$ <?=money_format('%(#10n', $interest);?> USD</td>
			    		
			    		<td><?=money_format('%(#10n', $ngopayments) . " " . $ngo_currency;?> <br /> $<?=money_format('%(#10n', $payments)?> USD</td>
		    	</tr>
		    	<?php endfor;?>
		    	</tbody>
		    </table>
		    
		    
		   
		    </div>
		    <?php endif;?>


	    </div>
    </div>

    
<div class="addMembersModal modal fade hide">
  <div class="modal-body">
     <div class="span3">
     <form id="addmembersform" method="post" action="<?=base_url();?>groups/addMemberProcess/HTML">
     <legend>Add Existing Client</legend>
	     <label>Client Name</label>
	     	<select name="client" id="clientID">
	     	<option value="">Select </option>
	     <?php $query = $this->Group_model->get_org_clients();
		     foreach($query as $row):?>
				<option value="<?=$row->id?>" ><?=$row->name?></option>      
		     <?php endforeach;?>
	    
	     
	     	</select>
	     <label>Role</label>
	     
	     <select name="role" id="memrole" >
	     	<option value="Member">Member</option>
	     	<option value="Leader">Leader</option>
	     	<option value="Community Leader">Community Leader</option>
	     	
	     </select>
	     <br /> 
	     <input type="hidden" id="groupID" name="groupID" value="<?=$groupID?>" />
		<input type="submit" id="addmembtn"	class="btn btn-success" value="Add Member" />     
     </form>
    </div>
    
    <div class="span3" style="border-left:2px solid #E5E5E5; padding-left:20px;">
    
     <legend>Add New Client</legend>
     <form id="addnewmembersform" method="post" action="<?=base_url();?>groups/addNewMemberProcess">
         <label>Client Name</label>
	     	<input type="text" name="clientName" id="clientName" placeholder="Client Name">
	     <label>Mobile Phone</label>
	     	<input type="text" name="cell"  id="cell" placeholder="Mobile Phone">
	     <label>Role</label>
	     	<select name="role" id="memrole" >
	     	<option value="Member">Member</option>
	     	<option value="Leader">Leader</option>
	     	<option value="Community Leader">Community Leader</option>
	     	
	     </select>
	     
	      <input type="hidden" id="groupID" name="groupID" value="<?=$groupID?>" />
		<input type="submit" id="addmembtn"	class="btn btn-success" value="Add New Client/Member" />     
     </form>

         </div>
  </div>
    
  <div class="modal-footer">
    <a href="#" class="btn" data-dismiss="modal">Close</a>
  </div>
</div>
    
    </div>
    
    
    




    
    
 <script>
      
      $(function()
{
		$("#addMembers").click(function(event){
			
			event.preventDefault();
			
			
			$('.addMembersModal').modal('show');
			
		});
		
		$("body").on("click", ".removeMember", function(event){
			var urlremove = $(this).attr("href");
			event.preventDefault();
			
			$.ajax({
	  type: "POST",
	  dataType: "JSON",
	  url: urlremove,
	   success: function(data) { 
	  	
	 		
	 		oTable.fnDraw();
	 		
		 		$('.memalert').html('<div class="alert alert-success" style="margin-top: 10px"><a class="close" data-dismiss="alert" href="#">&times;</a><strong>This person has been successfully removed from the group</strong></div>');
	 		
	 		
	   },
	});
		
		
		
			
		});
	
		
        
        $("#addmembtn").click(function(event){
			
			event.preventDefault();
			
			var groupID = '<?=$groupID;?>';
			var clientID = $("#clientID").val();
			var role = $('#memrole').val();
	 $.ajax({
	  type: "POST",
	  dataType: "JSON",
	  url: '<?=base_url();?>groups/addMemberProcess',
	  data: { clientID : clientID, role:role, groupID : groupID },
	  success: function(data) { 
	  	
	 		$("#clientID").val('');
			$('#memrole').val('Member');
	 		$('.addMembersModal').modal('hide');
	 		oTable.fnDraw();
	 		if(data.error == "TRUE"){
		 		$('.memalert').html('<div class="alert alert-warning" style="margin-top: 10px"><a class="close" data-dismiss="alert" href="#">&times;</a><strong>This person is already a member of this group</strong></div>');
	 		} else {
		 		$('.memalert').html('<div class="alert alert-success" style="margin-top: 10px"><a class="close" data-dismiss="alert" href="#">&times;</a><strong>This person has been successfully added to the group</strong></div>');
	 		}
	 		
	   },
	});
			
			
		});
	
        
 
        oTable = $('#myGroupMembersDatatable').dataTable( {
                "bProcessing": true,
                "bServerSide": true,
                "bJQueryUI": true,
                "sPaginationType": "bootstrap",
                
                "bFilter"   : false,
                "sAjaxSource": "<?php echo site_url('groups/myGroupsMembersDatatable/' . $groupID)?>",
                "fnServerData": function ( sSource, aoData, fnCallback ) {
                        $.ajax( {
                                "dataType": 'json',
                                "type": "POST",
                                "url": sSource,
                                "data": aoData,
                                "success": fnCallback
                        } );
                },
                "aaSorting": [[0,'DESC']],
                
                "bLengthChange": false
} );
       
 
});
      </script>


    
                
      
    
<?php $this->load->view('/dashboard/footer_view');?>