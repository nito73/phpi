<?php //$this->load->view('/dashboard/sidebar_view');?>
   <?php $stats = $this->Dashboard_model->get_user_stats($this->tank_auth->get_user_id());?>
       <!-- Jumbotron -->
      <div style="padding:10px;">
        <h1>Welcome Back <?=$this->tank_auth->get_name();?></h1>
      <div class="account-summary">
      <strong>Username: </strong><?=$this->tank_auth->get_username();?><br />
      <strong>Email: </strong><?=$this->tank_auth->get_email();?><br />
      <?php 
      $seedquery = $this->Dashboard_model->view_seed_account($this->tank_auth->get_user_id());
      $total = 0;     	
      if($seedquery):?>
	            	<?php foreach($seedquery as $row):?>
	            		<?php $total = $total + $row->amount;?>
	            		<?php endforeach;?>
	<strong>My Seed Account: </strong>$<?=money_format('%(#10n', $total);?> USD<br />
	<?php endif;?>
	   
	   <strong>Outstanding Loans: </strong> $<?=money_format('%(#10n', $stats['outstanding']);?> USD <br />
	            
	  <?php
	  	      $total = 0; 
	   if($fundedquery = $this->Dashboard_model->get_user_projects($this->tank_auth->get_user_id())):?>          
	           <?php foreach($fundedquery as $row):?>
	            		<?php $total = $total + $row->amount;?>
	            		<?php endforeach;?>
	            	
	<strong> Total Amount Planted: </strong>$<?=money_format('%(#10n', $total);?> USD<br /> 		
	      <?php endif;?>      		
	            		
      </div>
        
       
        	            	
	            <?php if($seedquery):?>
	           
         <h3>Mustard Seed Account</h3>
            <table class="table">
	            <thead>
	            <tr>
	            	
	            	<td>Date</td>
	            	<td>Reason</td>
	            	<td>Amount</td>
	            </tr>
	            </thead>
	            <tbody>
	            
	            	
	            	
	            		<?php 
	            		      $total = 0; 
	            		foreach($seedquery as $row):?>
	            			<?php 
	            				$total+=$row->amount;
	            			?>
	            		<tr>
	            		<td><?=date('m/d/y', strtotime($row->timestamp))?></td>
	            			<td><?=$row->reason?> </td>
	            		
	            			<td>$ <?=money_format('%(#10n', $row->amount)?> USD</td>
	            			
	            		</tr>
	            		<?php endforeach;?>
	            		
	            		<tr>
	            		<td colspan="3" style="text-align:right;"><strong>My Seed Account Total</strong> $ <?=money_format('%(#10n', $total)?> USD</td>
	            		</tr>
	            	
	            </tbody>
    </table>
    	            	<?php endif;?>
    	            	
    	            	<?php if($fundedquery):?>
    	            	
    	            	         <h3>My Planted Seed</h3>


    	            	<table class="table table-hover">
	            <thead>
	            <tr>
	            	<td>Date</td>
	            	<td>Application Name</td>
	            	<td>Payment Method</td>
	            	<td>MY % of the Loan</td>
	            	<td>My Donation</td>
	            	
	            	
	            </tr>
	            </thead>
	            <tbody>
	            
	            	
	            		<?php 
	            			 $total = 0; 
	            		foreach($fundedquery as $row):?>
	            			<?php $total = $total + $row->amount;?>

	            		
	            		<tr>
	            			<td><?=date('m/d/y', strtotime($row->timestamp))?></td>
	            			<td><a href="/give/view/<?=$row->id?>"> <?=$row->title?></a></td>
	            			<td><?=$row->method?></td>
	            			<td><?=floor(($row->amount/$row->app_amount)*100) . '%'?></td>
	            			<td>$ <?=money_format('%(#10n', $row->amount)?> USD	</td>
	            			
	            			
	            		</tr>
	            		<?php endforeach;?>
	            		
	            		<tr>
	            		<td colspan="6" style="text-align:right;"><strong>Total Amount donated</strong> $ <?=money_format('%(#10n', $total)?> USD</td>
	            		</tr>
	            </tbody>
    </table>
<?php endif;?>
        
      </div>
<?php //$this->load->view('/dashboard/footer_view');?>