<?php $this->load->view('/dashboard/sidebar_view');?>
   <style type="text/css">
   
   </style>
       <!-- Jumbotron -->
      <div style="padding:10px;">
      	<a target="_blank" href="<?=site_url("ngo/print_collection");?>" class="btn pull-right" id="print">Print Collection Sheet</a>
        <h1>Applications</h1>
      
      <div class="view_data well">
      <strong>View By</strong>
       Location
        <select name="view" id="view">
        	<option value="All">All</option>
        	<?php $locquery = $this->Dashboard_model->view_org_projects_location($this->tank_auth->get_orgid());
    if ($locquery):?>
    <?php foreach($locquery as $row):?>
    	<option value="<?=$row->location?>"><?=$row->location?></option>
    <?php endforeach?>
    <?php endif;?>

        </select>
        
      Industry
			<select class="input-large" name="category" id="category">
				<option value="All"> All </option>
			  <option value="Food">Food</option>
			  <option value="Agriculture">Agriculture</option>
			  <option value="Service">Service</option>
			  <option value="Small Trade">Small Trade</option>
			  <option value="Manufacturing">Manufacturing</option>
			</select>		
			
      </div>
        
        <?php $query = $this->Dashboard_model->view_org_projects($this->tank_auth->get_orgid(), $location);
	            	$total = 0;
	            	?>
            <table class="table" id="myProjectsDataTable">
	            <thead>
	            <tr>
	            	<td>ID</td>
	            	<td>Type</td>
	            	<td>Name</td>
	            	<td>Use</td>
	            	
	            	<td>Amount Requested</td>
	            	<td>Location</td>
	            	<td>Category</td>
	            	<td>View Activity</td>
	            </tr>
	            </thead>
	            <tbody>
	            
	            </tbody>
    </table>

    	     
    	     <form method="post" action="<?=site_url("ngo/print_collection");?>" target="_blank" id="printForm" >
    	     <input type="hidden" name="view" value="All" id="printView"/>
    	     </form>             
      </div>
      
      <script>
      
      $(function()
{
		$("#view").change(function(event){
			
			event.preventDefault();
			
			var viewLocation = $(this).val();
			$("#printView").val(viewLocation);
			oTable.fnDraw();
			
		});
		$("#category").change(function(event){
			
			event.preventDefault();
			
					oTable.fnDraw();
			
		});
		
		$("#print").click(function(event){
			
			event.preventDefault();
			$('#printForm').submit();
			
		});
		
		
        
 
        oTable = $('#myProjectsDataTable').dataTable( {
                "bProcessing": true,
                "bJQueryUI": true,
                "sPaginationType": "bootstrap",
                "bServerSide": true,
                "sAjaxSource": "<?php echo site_url('ngo/myProjectsDataTable')?>",
                "fnServerData": function ( sSource, aoData, fnCallback ) {
                        $.ajax( {
                                "dataType": 'json',
                                "type": "POST",
                                "url": sSource,
                                "data": aoData,
                                "success": fnCallback
                        } );
                },
                "aaSorting": [[0,'DESC']],
                fnServerParams: function ( aoData ) {
                        aoData.push( { name: 'view', value: $('#view').val() } );
                        aoData.push( { name: 'category', value: $('#category').val() } );
                },
                "bLengthChange": false
} );
       
 
});
      </script>
<?php //$this->load->view('/dashboard/footer_view');?>