<style type="text/css">
	#editor{
		overflow:scroll;
		max-height:300px;
	}
</style>

<link rel="stylesheet" href="/imageupload/css/jquery.fileupload-ui.css">


<div style="padding:10px;">
        <h1>Adding New Project</h1>
        <p class="lead">Please remember all applications for Projects are subject to review</p>
        <div>
        <form id="form">
        <label>Name of the Project</label>
		<input class="input-xxlarge" type="text" name="title" id="title">
		<label>Amount Requested</label>
		<input class="input-xxlarge" type="text" name="amount" id="amount" placeholder="Currency in USD">
		
	

  	
		<label>Name of the Project Leader</label>
		<input class="input-xxlarge" type="text" name="name" id="name">
		<input type="hidden" value="Project" name="type" id="type">
		<input type="hidden" value="N/A" name="category" id="category">
		<label>Project Location</label>
		<input class="input-xlarge" type="text" name="location" id="location" placeholder="Location">
		<input class="input-xlarge" type="text" name="country" id="country" placeholder="Country"><br />
		<input class="input-xlarge" type="text" name="region" id="region" placeholder="Region">
		<input class="input-xlarge" type="text" name="postcode" id="postcode" placeholder="Postcode"><br />
		<label>Contact Information</label>
		<input class="input-xlarge" type="text" name="tel" id="tel" placeholder="Telephone">
		<input class="input-xlarge" type="text" name="mobile" id="mobile" placeholder="Mobile">
		<label>Project Photo</label>
		 <img src="" class="uploadimage"/>
	    
	    
		<span class="btn btn-success fileinput-button">
	        <i class="icon-plus icon-white"></i>
	        <span>Select files...</span>
		    <input id="fileupload" type="file" name="files[]" multiple>
	    </span>
	    
	   	    <div id="progress" class="progress progress-success progress-striped">
	        <div class="bar"></div>
	    </div>
	   
		<input class="input-xxlarge" type="hidden" name="pic" id="pic" placeholder="URL of Picture">
		
		<label>Project Video</label>
		<input class="input-xxlarge" type="text" name="video" id="video" placeholder="Youtube ID">
		
		<label>Brief Project Use</label>
		<input class="input-xxlarge" type="text" name="use" id="use">
		
		<label>Brief Project Description</label>
		<div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor">
      
	      <div class="btn-group">
	        <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="icon-text-height"></i>&nbsp;<b class="caret"></b></a>
	          <ul class="dropdown-menu">
	          <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
	          <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
	          <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
	          </ul>
	      </div>
	      
	      <div class="btn-group">
	        <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="icon-bold"></i></a>
	        <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="icon-italic"></i></a>
	      </div>
      
      </div>
      
    

    <div id="editor" name="description">
      
    </div>
    <input type="submit" class="btn" value="Submit" />
  </div>
 </div>
  
  </form>      
        
		
        </div>
<script src="/imageupload/js/vendor/jquery.ui.widget.js"></script>
<script src="/imageupload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="/imageupload/js/jquery.fileupload.js"></script>

<script>
$(function(){
	   	   'use strict';
    // Change this to the location of your server-side upload handler:
   var url = (window.location.hostname === 'blueimp.github.com' ||
                window.location.hostname === 'blueimp.github.io') ?
                '//jquery-file-upload.appspot.com/' : '<?=site_url()?>/imageupload/server/php/';

    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
              	
                $('#pic').val(file.url);
                $('.uploadimage').attr('src', file.url);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .bar').css(
                'width',
                progress + '%'
            );
        }
    });
$("#form").submit(function(event) {
 var projectdesc = $('#editor').html();
    
	 $.ajax({
	  type: "POST",
	  url: '<?=base_url();?>project/process',
	  dataType: 'JSON',
	  data: { title: $("#title").val(), name: $("#name").val(), amount: $("#amount").val(),   country: $("#country").val(),  location: $("#location").val(),   use: $("#use").val(),  category: $("#category").val(),    description: projectdesc, type: $("#type").val(), pic: $("#pic").val(), video: $("#video").val()  },
	  success: function(data) { 
	  	window.location = "<?=base_url();?>/project/success";
	   },
	});
	
	 event.preventDefault();
});

  });
  
  


</script>
