<style type="text/css">
	#editor{
		overflow:scroll;
		max-height:300px;
	}
</style>

<link rel="stylesheet" href="/imageupload/css/jquery.fileupload-ui.css">


<div style="padding:10px;">
        <h1>New Application</h1>
        <p class="lead">Please remember all applications for Loans are subject to review</p>
        <div>
        <form id="form">
        <label>Name of the Loan</label>
		<input class="input-xxlarge" type="text" name="title" id="title">
			<label>Type of Loan</label>
			<select class="input-xxlarge" name="type" id="type">
				<option value=""> --- Select One ---</option>
			  <option value="Micro">Micro</option>
			  <option value="Macro">Macro/Meso</option>
			  <option value="Group Micro">Group Micro</option>
			</select>
		<div class="amountgroup" style="display:none;">
			<label>Number of Members in Micro Group</label>
			<input class="input-xxlarge" type="text" name="amountgroup" id="amountgroup" placeholder="Members in Group">
		
		</div>
		<label>Amount Requested</label>
		<input class="input-xxlarge" type="text" name="amount" id="amount" placeholder="Currency in USD">
		
		
	
		
		<label>Type of Industry</label>
			<select class="input-xxlarge" name="category" id="category">
				<option value=""> --- Select One ---</option>
			  <option value="Food">Food</option>
			  <option value="Agriculture">Agriculture</option>
			  <option value="Service">Service</option>
			  <option value="Small Trade">Small Trade</option>
			  <option value="Manufacturing">Manufacturing</option>
			</select>		
			
  	
		<label>Borrower Information</label>
		<input class="input-xxlarge" type="text" name="name" id="name" placeholder="Barrow Name">
		
		<input class="input-xxlarge" type="text" name="address" id="address" placeholder="Address">
		<br />
		<input class="input-large" type="text" name="city" id="city" placeholder="City">
		<input class="input-large" type="text" name="Region" id="Region" placeholder="Region">
		<input class="input-large" type="text" name="postalcode" id="Postal Code" placeholder="Postal Code">
		<input class="input-xxlarge" type="text" name="cell" id="cell" placeholder="Mobile Phone">
		
		<h2>Loan Terms</h2>

		<select class="input-xxlarge" name="loanterms" id="loanterms" placeholder="Re-Payment Terms">
			  <option value=""> --- Re-Payment Terms ---</option>
			  <option value="4">One Month (4 Payments)</option>
			  <option value="8">Two Months (8 Payments)</option>
			  <option value="12">Three Months (12 Payments)</option>
			  <option value="24">Six Months (24 Payments)</option>
			  <option value="48">One Year (48 Payments)</option>
			</select>
		<select name="currency" id="currency" class="currency">
			<option></option>
			<option value="USD">(USD) US Dollar</option>
			<option value="AED">(AED) UAE Dirham</option>
			<option value="AFN">(AFN) Afghani</option>
			<option value="ALL">(ALL) Lek</option>
			<option value="AMD">(AMD) Armenian Dram</option>
			<option value="ANG">(ANG) Netherlands Antillean Guilder</option>
			<option value="AOA">(AOA) Kwanza</option>
			<option value="ARS">(ARS) Argentine Peso</option>
			<option value="AUD">(AUD) Australian Dollar</option>
			<option value="AWG">(AWG) Aruban Florin</option>
			<option value="AZN">(AZN) Azerbaijanian Manat</option>
			<option value="BAM">(BAM) Convertible Mark</option>
			<option value="BBD">(BBD) Barbados Dollar</option>
			<option value="BDT">(BDT) Taka</option>
			<option value="BGN">(BGN) Bulgarian Lev</option>
			<option value="BHD">(BHD) Bahraini Dinar</option>
			<option value="BIF">(BIF) Burundi Franc</option>
			<option value="BMD">(BMD) Bermudian Dollar</option>
			<option value="BND">(BND) Brunei Dollar</option>
			<option value="BOB">(BOB) Boliviano</option>
			<option value="BOV">(BOV) Mvdol</option>
			<option value="BRL">(BRL) Brazilian Real</option>
			<option value="BSD">(BSD) Bahamian Dollar</option>
			<option value="BTN">(BTN) Ngultrum</option>
			<option value="BWP">(BWP) Pula</option>
			<option value="BYR">(BYR) Belarussian Ruble</option>
			<option value="BZD">(BZD) Belize Dollar</option>
			<option value="CAD">(CAD) Canadian Dollar</option>
			<option value="CDF">(CDF) Congolese Franc</option>
			<option value="CHE">(CHE) WIR Euro</option>
			<option value="CHF">(CHF) Swiss Franc</option>
			<option value="CHF">(CHF) Swiss Franc</option>
			<option value="CHW">(CHW) WIR Franc</option>
			<option value="CLF">(CLF) Unidades de fomento</option>
			<option value="CLP">(CLP) Chilean Peso</option>
			<option value="CNY">(CNY) Yuan Renminbi</option>
			<option value="COP">(COP) Colombian Peso</option>
			<option value="COU">(COU) Unidad de Valor Real</option>
			<option value="CRC">(CRC) Costa Rican Colon</option>
			<option value="CUC">(CUC) Peso Convertible</option>
			<option value="CUP">(CUP) Cuban Peso</option>
			<option value="CVE">(CVE) Cape Verde Escudo</option>
			<option value="CZK">(CZK) Czech Koruna</option>
			<option value="DJF">(DJF) Djibouti Franc</option>
			<option value="DKK">(DKK) Danish Krone</option>
			<option value="DOP">(DOP) Dominican Peso</option>
			<option value="DZD">(DZD) Algerian Dinar</option>
			<option value="EGP">(EGP) Egyptian Pound</option>
			<option value="ERN">(ERN) Nakfa</option>
			<option value="ETB">(ETB) Ethiopian Birr</option>
			<option value="EUR">(EUR) Euro</option>
			<option value="EUR">(EUR) Euro</option>
			<option value="FJD">(FJD) Fiji Dollar</option>
			<option value="FKP">(FKP) Falkland Islands Pound</option>
			<option value="GBP">(GBP) Pound Sterling</option>
			<option value="GEL">(GEL) Lari</option>
			<option value="GHS">(GHS) Ghana Cedi</option>
			<option value="GIP">(GIP) Gibraltar Pound</option>
			<option value="GMD">(GMD) Dalasi</option>
			<option value="GNF">(GNF) Guinea Franc</option>
			<option value="GTQ">(GTQ) Quetzal</option>
			<option value="GYD">(GYD) Guyana Dollar</option>
			<option value="HKD">(HKD) Hong Kong Dollar</option>
			<option value="HNL">(HNL) Lempira</option>
			<option value="HRK">(HRK) Croatian Kuna</option>
			<option value="HTG">(HTG) Gourde</option>
			<option value="HUF">(HUF) Forint</option>
			<option value="IDR">(IDR) Rupiah</option>
			<option value="ILS">(ILS) New Israeli Sheqel</option>
			<option value="INR">(INR) Indian Rupee</option>
			<option value="IQD">(IQD) Iraqi Dinar</option>
			<option value="IRR">(IRR) Iranian Rial</option>
			<option value="ISK">(ISK) Iceland Krona</option>
			<option value="JMD">(JMD) Jamaican Dollar</option>
			<option value="JOD">(JOD) Jordanian Dinar</option>
			<option value="JPY">(JPY) Yen</option>
			<option value="KES">(KES) Kenyan Shilling</option>
			<option value="KGS">(KGS) Som</option>
			<option value="KHR">(KHR) Riel</option>
			<option value="KMF">(KMF) Comoro Franc</option>
			<option value="KPW">(KPW) North Korean Won</option>
			<option value="KRW">(KRW) Won</option>
			<option value="KWD">(KWD) Kuwaiti Dinar</option>
			<option value="KYD">(KYD) Cayman Islands Dollar</option>
			<option value="KZT">(KZT) Tenge</option>
			<option value="LAK">(LAK) Kip</option>
			<option value="LBP">(LBP) Lebanese Pound</option>
			<option value="LKR">(LKR) Sri Lanka Rupee</option>
			<option value="LRD">(LRD) Liberian Dollar</option>
			<option value="LSL">(LSL) Loti</option>
			<option value="LTL">(LTL) Lithuanian Litas</option>
			<option value="LVL">(LVL) Latvian Lats</option>
			<option value="LYD">(LYD) Libyan Dinar</option>
			<option value="MAD">(MAD) Moroccan Dirham</option>
			<option value="MAD">(MAD) Moroccan Dirham</option>
			<option value="MDL">(MDL) Moldovan Leu</option>
			<option value="MGA">(MGA) Malagasy Ariary</option>
			<option value="MKD">(MKD) Denar</option>
			<option value="MMK">(MMK) Kyat</option>
			<option value="MNT">(MNT) Tugrik</option>
			<option value="MOP">(MOP) Pataca</option>
			<option value="MRO">(MRO) Ouguiya</option>
			<option value="MUR">(MUR) Mauritius Rupee</option>
			<option value="MVR">(MVR) Rufiyaa</option>
			<option value="MWK">(MWK) Kwacha</option>
			<option value="MXN">(MXN) Mexican Peso</option>
			<option value="MYR">(MYR) Malaysian Ringgit</option>
			<option value="MZN">(MZN) Mozambique Metical</option>
			<option value="NAD">(NAD) Namibia Dollar</option>
			<option value="NGN">(NGN) Naira</option>
			<option value="NIO">(NIO) Cordoba Oro</option>
			<option value="NOK">(NOK) Norwegian Krone</option>
			<option value="NPR">(NPR) Nepalese Rupee</option>
			<option value="NZD">(NZD) New Zealand Dollar</option>
			<option value="OMR">(OMR) Rial Omani</option>
			<option value="PAB">(PAB) Balboa</option>
			<option value="PEN">(PEN) Nuevo Sol</option>
			<option value="PGK">(PGK) Kina</option>
			<option value="PHP">(PHP) Philippine Peso</option>
			<option value="PKR">(PKR) Pakistan Rupee</option>
			<option value="PLN">(PLN) Zloty</option>
			<option value="PYG">(PYG) Guarani</option>
			<option value="QAR">(QAR) Qatari Rial</option>
			<option value="RON">(RON) New Romanian Leu</option>
			<option value="RSD">(RSD) Serbian Dinar</option>
			<option value="RUB">(RUB) Russian Ruble</option>
			<option value="RWF">(RWF) Rwanda Franc</option>
			<option value="SAR">(SAR) Saudi Riyal</option>
			<option value="SBD">(SBD) Solomon Islands Dollar</option>
			<option value="SCR">(SCR) Seychelles Rupee</option>
			<option value="SDG">(SDG) Sudanese Pound</option>
			<option value="SEK">(SEK) Swedish Krona</option>
			<option value="SGD">(SGD) Singapore Dollar</option>
			<option value="SHP">(SHP) Saint Helena Pound</option>
			<option value="SLL">(SLL) Leone</option>
			<option value="SOS">(SOS) Somali Shilling</option>
			<option value="SRD">(SRD) Surinam Dollar</option>
			<option value="SSP">(SSP) South Sudanese Pound</option>
			<option value="STD">(STD) Dobra</option>
			<option value="SVC">(SVC) El Salvador Colon</option>
			<option value="SYP">(SYP) Syrian Pound</option>
			<option value="SZL">(SZL) Lilangeni</option>
			<option value="THB">(THB) Baht</option>
			<option value="TJS">(TJS) Somoni</option>
			<option value="TMT">(TMT) Turkmenistan New Manat</option>
			<option value="TND">(TND) Tunisian Dinar</option>
			<option value="TOP">(TOP) Pa?anga</option>
			<option value="TRY">(TRY) Turkish Lira</option>
			<option value="TTD">(TTD) Trinidad and Tobago Dollar</option>
			<option value="TWD">(TWD) New Taiwan Dollar</option>
			<option value="TZS">(TZS) Tanzanian Shilling</option>
			<option value="UAH">(UAH) Hryvnia</option>
			<option value="UGX">(UGX) Uganda Shilling</option>
			<option value="UYU">(UYU) Peso Uruguayo</option>
			<option value="UZS">(UZS) Uzbekistan Sum</option>
			<option value="VEF">(VEF) Bolivar </option>
			<option value="VND">(VND) Dong</option>
			<option value="VUV">(VUV) Vatu</option>
			<option value="WST">(WST) Tala</option>
			<option value="XAF">(XAF) CFA Franc BEAC</option>
			<option value="XCD">(XCD) East Caribbean Dollar</option>
			<option value="XDR">(XDR) SDR (Special Drawing Right)</option>
			<option value="XPF">(XPF) CFP Franc</option>
			<option value="XSU">(XSU) Sucre</option>
			<option value="XUA">(XUA) ADB Unit of Account</option>
			<option value="YER">(YER) Yemeni Rial</option>
			<option value="ZAR">(ZAR) Rand</option>
			<option value="ZMW">(ZMW) Zambian Kwacha</option>
			<option value="ZWL">(ZWL) Zimbabwe Dollar</option>
		</select>
		<br />
			<input class="input-xlarge" type="text" name="saving" id="saving" placeholder="Inital Savings">
			<input class="input-xlarge datepicker" type="text" name="startdate" id="startdate" placeholder="Loan Request Start Date">
			<input class="input-xlarge datepicker" type="text" name="loandate" id="loandate" placeholder="Loan Repayment Start Date">
			<select name="loaninterest" id="loaninterest">
			<option value="">Loan Repayment Interest</option>
			<option value="">No Interest</option>
			<?php for($x= 1; $x <100; $x++):?>
				<option value="<?=$x;?>"> <?=$x;?>%</option>
			<?php endfor;?>
			</select>

		<a class="calculate btn btn-success" href="#">Calculate</a>
		<br />
		<br />
		<label>Loan Calculator</label>
		<input class="loanCalculator input-xxlarge" type="text" value="Please Fill out Payment Terms" disabled="disabled">
		<br />
		<h2>Loan Information</h2>
		<label>Loan Location</label>
		<input class="input-xlarge" type="text" name="location" id="location" placeholder="Location">
		<input class="input-xlarge" type="text" name="country" id="country" placeholder="Country">
		
		<label>Loan Photo</label>
		 <img src="" class="uploadimage"/>
	    
	    
		<span class="btn btn-success fileinput-button">
	        <i class="icon-plus icon-white"></i>
	        <span>Select files...</span>
		    <input id="fileupload" type="file" name="files[]" multiple>
	    </span>
	    
	   	    <div id="progress" class="progress progress-success progress-striped">
	        <div class="bar"></div>
	    </div>
	   
		<input class="input-xxlarge" type="hidden" name="pic" id="pic" placeholder="URL of Picture">
		
		<label>Loan Video</label>
		<input class="input-xxlarge" type="text" name="video" id="video" placeholder="Youtube ID">
		
		<label>Loan Use</label>
		<input class="input-xxlarge" type="text" name="use" id="use">
		
		<label>Loan Description</label>
		<div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor">
      
	      <div class="btn-group">
	        <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="icon-text-height"></i>&nbsp;<b class="caret"></b></a>
	          <ul class="dropdown-menu">
	          <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
	          <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
	          <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
	          </ul>
	      </div>
	      
	      <div class="btn-group">
	        <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="icon-bold"></i></a>
	        <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="icon-italic"></i></a>
	      </div>
      
      </div>
      
    

    <div id="editor" name="description">
      
    </div>
    <input type="submit" class="btn" value="Submit" />
  </div>
 </div>
  
  </form>      
        
		
        </div>
<script src="/imageupload/js/vendor/jquery.ui.widget.js"></script>
<script src="/imageupload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="/imageupload/js/jquery.fileupload.js"></script>

<script>
$(function(){
	   	   'use strict';
    // Change this to the location of your server-side upload handler:
   var url = (window.location.hostname === 'blueimp.github.com' ||
                window.location.hostname === 'blueimp.github.io') ?
                '//jquery-file-upload.appspot.com/' : '<?=base_url();?>/imageupload/server/php/';

    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
              	
                $('#pic').val(file.url);
                $('.uploadimage').attr('src', file.url);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .bar').css(
                'width',
                progress + '%'
            );
        }
    });
    
    
    $('.calculate').click(function(event){
	    event.preventDefault();
	    var currency = $('#currency').val();
	    
	     $.ajax({
	  type: "POST",
	  url: '<?=base_url();?>project/get_rate',
	  dataType: 'JSON',
	  data: { currency: currency },
	  success: function(data) { 
	  	if(data.rate == 0){
		  	var rate = 1;
	  	} else {
		  	var rate = data.rate;
	  	}
	  		
	  		var amount = $("#amount").val();
	  		var loanterms = $("#loanterms").val();
	  		var loaninterest = ($("#loaninterest").val()/100)+1;
	  		
	  		var payment = ((amount*loaninterest)/loanterms)*rate;
	  		$(".loanCalculator").val(accounting.formatMoney(payment, { symbol: currency,  format: "%v %s" }));
	  	
	   },
	});
	
	 

    });
    
    
    
    $('#type').change(function(event) { 
    	var loantype = $(this).val();
    	if(loantype == "Group Micro"){
	    	$('.amountgroup').show();
    	} else{
	    	$('.amountgroup').hide();
    	}
     });
$("#form").submit(function(event) {
 var projectdesc = $('#editor').html();
    
	 $.ajax({
	  type: "POST",
	  url: '<?=base_url();?>project/processLoan',
	  dataType: 'JSON',
	  data: { title: $("#title").val(), name: $("#name").val(), amount: $("#amount").val(),   country: $("#country").val(),  location: $("#location").val(),   use: $("#use").val(),  category: $("#category").val(),    description: projectdesc, type: $("#type").val(), pic: $("#pic").val(), video: $("#video").val(), currency: $("#currency").val(), loanterms: $("#loanterms").val(), startdate: $("#startdate").val(), loandate: $("#loandate").val(), loaninterest : $("#loaninterest").val(), saving: $("#saving").val() },
	  success: function(data) { 
	  	window.location = "<?=base_url();?>/project/success";
	   },
	});
	
	 event.preventDefault();
});

  });
  
  


</script>
