<style type="text/css">
	.total td{
		font-weight: bold;
	}
</style>
<?php $query = $this->Ngo_model->viewProject($projectID);
  $total = $this->Ngo_model->get_total_funded($projectID);
    foreach($query as $row):?>
    <?php 
	            		$ngo_currency = $row->currency;
		            	$loan_terms = $row->loanterms;
		            	$loandate = $row->loandate;	
		            	$loaninterest = $row->loaninterest;
		            	$totalrequested = $row->amount;
		            	$rate= $row->rate;
	            		?>
<?php $percenttotal = floor(($total/$row->amount)*100);?>
  <div class="row" style="padding:5px;">
    <div class="span3">
    <a href="#" class="thumbnail">
      <img src="<?php if($row->pics != "") { echo $row->pics;} else { echo base_url('images') . "/no_image.jpg";}?>">
    </a>
    </div>
    <div class="span8">
			<a class="btn pull-right" href="<?=site_url('ngo/printProject/' . $projectID);?>" target="_blank">Print Application Summery</a>
          <h1><?=$row->title?></h1>
          <h4><?=$row->location?> | <?=$row->category?></h4>
         
          
                   <dl class="dl-horizontal">
		  <dt>Name</dt>
		  <dd><?=$row->name?></dd>
		  <dt>Posted Date</dt>
		  <dd><?=date('d M y' , strtotime($row->postdate))?></dd>
		 
		  <?php if($repaymenttotal = $this->Ngo_model->total_repayment_amount($projectID)):?>
		  	<dt>Loan </dt>
		  		<dd>$<?=money_format('%(#10n', $row->amount)?> USD  / <?=money_format('%(#10n', $row->amount*$rate)?> <?=$ngo_currency?>  (Principal)<br />
		  		$<?=money_format('%(#10n', $row->amount*($loaninterest/100))?> USD  / <?=money_format('%(#10n', ($row->amount*$rate)*($loaninterest/100))?> <?=$ngo_currency?> (Interest <?=$loaninterest?>%) 
		  		</dd>
		  	<dt>Total Due</dt>
		  	<dd>
			  $<?=money_format('%(#10n', $repaymenttotal['phptotal'])?> USD / $<?=money_format('%(#10n', $row->amount*(($loaninterest/100)+1))?> USD <br />
			  <?=money_format('%(#10n', $repaymenttotal['ngototal'])?> <?=$ngo_currency?> / <?=money_format('%(#10n', ($row->amount*$rate)*(($loaninterest/100)+1))?> <?=$ngo_currency?> 
			  <?php $percenttotal = floor(($repaymenttotal['phptotal']/$row->amount)*100);?>
		


			   <div class="progress">
       
				<div class="bar" style="width: <?=$percenttotal?>%;"></div>
				   <label><?=$percenttotal?>% Returned</label>
				</div>
            </div>
            
			  </dd>
		  <?php else:?>
			  <?php if ($row->fundingsource == "PHP"):?>
			  
				  <dt>Amount</dt>
				  <dd>$<?=$row->amount?> USD
			  <div class="progress">
	       
					<div class="bar" style="width: <?=$percenttotal?>%;"></div>
					   <label><?=$percenttotal?>% Funded</label>
					</div>
	            </div>
	            </dd>
	          <?php else:?>
	          	  <dt>Repayment Amount</dt>
			  <dd>$<?=money_format('%(#10n', $repaymenttotal['phptotal'])?> USD / $<?=$row->amount?> USD <br />
			  <?=money_format('%(#10n', $repaymenttotal['ngototal'])?> <?=$ngo_currency?> / <?=$row->amount*$rate?> <?=$ngo_currency?>
			  <?php $percenttotal = floor(($repaymenttotal['phptotal']/$row->amount)*100);?>
		


			   <div class="progress">
       
				<div class="bar" style="width: 0%;"></div>
				   <label>0% Returned</label>
				</div>
            </div>
	          <?php endif;?>
	          
		   
		  <?php endif;?>
		  
		  
  		</dl>
          
         
  </div>
    <hr>
<?php endforeach;?>
                   
      
      <script>

</script>

<?php $this->load->view('/dashboard/footer_view');?>