<style type="text/css">
	#editor{
		overflow:scroll;
		max-height:300px;
	}
</style>

<link rel="stylesheet" href="/imageupload/css/jquery.fileupload-ui.css">


<div style="padding:10px;">
        <h1>Adding Application</h1>
        <p class="lead">Please remember all applications for Loans and Projects are subject to review excluding Non-PHP loans</p>
        <div class="row-fluid">
                
                <div class="span4">
                	<a class="btn btn-primary btn-large" href="<?=site_url('project/add/loan')?>" > Add Loan </a>
                	<p style="padding-top:20px;"><ul>
    <li>
    <span style="font-size: 13px;"><span style="font-weight: bold;">Food Industry Business Sector</span></span>- Businesses that produce or sell packaged manufactured food out of a store such as Restaurants, Bakery, Ice Cream, etc.</li>
    <li>
    <span style="font-size: 13px;"><span style="font-weight: bold;">Agriculture Business Sector</span></span>- Businesses that grow crops, raise animals, etc. off the land</li>
    <li>
    <span style="font-size: 13px;"><span style="font-weight: bold;">Service Industry Business Sector</span></span> - Businesses that provide services such as repairs, hair dressers, communication centers, etc</li>
    <li>
    <span style="font-size: 13px;"><span style="font-weight: bold;">Small Trade Industry Business Sector</span></span> - Any type of product one a singular to one basis such as Beans, T-shirts, Yams, Jewelry, etc.</li>
    <li>
    <span style="font-size: 13px;"><span style="font-weight: bold;">Manufacturing Business Sector</span></span>- These businesses are defined as capable of producing a product such as cement block makers, seamstress, furniture, etc.</li>
</ul>
</p> 
                </div>
                <div class="span4">
                	<a class="btn btn-primary btn-large" href="<?=site_url('project/add/nonloan')?>" > Add Non-PHP Loan</a>
                	<br />
                	<br />
                		Do you have a Loan that is funded by another organization that you want our system to track payments.
                	
</p> 
                </div>
                <div class="span4">
                <a class="btn btn-primary btn-large" href="<?=site_url('project/add/project')?>" > Add Project </a>
                	<p style="padding-top:20px;">Tax deductible donations to "PROJECTS" will be in the form of gifts with no expectations of any monetary return to your Founders Mustard Seed Account. </p>
                </div>
			
		
        </div>
