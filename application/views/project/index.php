<?php if($this->tank_auth->get_usertype() != "loan_officer" ):?>
<?php $this->load->view('/dashboard/sidebar_view');?>
<?php endif;?>


   <style type="text/css">
   
   </style>
       <!-- Jumbotron -->
           
      <div style="padding:10px;">
      	<div>

         <a href="<?=site_url("project/add");?>" class="btn pull-right btn-success">Add Project</a>

        </div>  
        <br />
        <br />
        <?php if($msg = $this->session->flashdata('message')):?>
        	    <div class="alert alert-<?=$msg['type']?>" style="margin-top: 10px">
        	    	<a class="close" data-dismiss="alert" href="#">&times;</a>
        		    <h4 class="alert-heading"><?=$msg['body']?></h4>
        	    </div>
        	    <?php endif;?>
        	  
        
             


            <table class="table" id="myProjectsDataTable">
	            <thead>
	            <tr>
	            	<td>Project ID</td>
	            	<td>Titie</td>
	            	<td>Name</td>
	            	<td>Amount</td>
	            	<td>Location</td>
	            	<td>View Project</td>
	            </tr>
	            </thead>
	            <tbody>
	            
	            </tbody>
    </table>
    	            
    	     
    	     <form method="post" action="<?=site_url("ngo/print_collection");?>" target="_blank" id="printForm" >
    	     <input type="hidden" name="view" value="All" id="printView"/>
    	     </form>             
      </div>
      
      <script>
      
      $(function()
{
		$("#city").change(function(event){
			
			event.preventDefault();
			
			var viewLocation = $(this).val();
			$("#printView").val(viewLocation);
			oTable.fnDraw();
			
		});
		$("#region").change(function(event){
			
			event.preventDefault();
			
					oTable.fnDraw();
			
		});
		$("#status").change(function(event){
			
			event.preventDefault();
			
					oTable.fnDraw();
			
		});
		
		$("#print").click(function(event){
			
			event.preventDefault();
			$('#printForm').submit();
			
		});
		
		
        
 
        oTable = $('#myProjectsDataTable').dataTable( {
                "bProcessing": true,
                "bServerSide": true,
                "bJQueryUI": true,
                "sPaginationType": "bootstrap",
                "sAjaxSource": "<?php echo site_url('project/myProjectDatatable')?>",
                "fnServerData": function ( sSource, aoData, fnCallback ) {
                        $.ajax( {
                                "dataType": 'json',
                                "type": "POST",
                                "url": sSource,
                                "data": aoData,
                                "success": fnCallback
                        } );
                },
                "aaSorting": [[0,'DESC']],
                fnServerParams: function ( aoData ) {
                        aoData.push( { name: 'city', value: $('#city').val() } );
                        aoData.push( { name: 'region', value: $('#region').val() } );
                        aoData.push( { name: 'status', value: $('#status').val() } );
                },
                "bLengthChange": false
} );
       
 
});
      </script>
<?php //$this->load->view('/dashboard/footer_view');?>