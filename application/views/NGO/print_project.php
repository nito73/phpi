<style type="text/css">
	.total td{
		font-weight: bold;
	}
	img{ height: 200px; float: left;}
	table tr td{border: 1px solid black; }
	thead tr td{border:2px solid black; font-weight: bold; background-color: yellow; }
	
	table{ border-collapse: collapse; }	
</style>
<?php $query = $this->Ngo_model->viewProject($projectID);
  $total = $this->Ngo_model->get_total_funded($projectID);
    foreach($query as $row):?>
    <?php 
	            		$ngo_currency = $row->currency;
		            	$loan_terms = $row->loanterms;
		            	$loandate = $row->loandate;	
		            	$loaninterest = $row->loaninterest;
		            	$totalrequested = $row->amount;
		            	$rate= $row->rate;
	            		?>
<?php $percenttotal = floor(($total/$row->amount)*100);?>
  <div class="row" style="padding:5px;">
      <img src="<?php if($row->pics != "") { echo $row->pics;} else { echo base_url('images') . "/no_image.jpg";}?>">
      <div style="height:200px; padding-left:300px;">
          <strong><?=$row->title?></strong><br />
		<strong><?=$row->location?> | <?=$row->category?></strong><br />
		  <strong>Name</strong> <?=$row->name?><br />
		  <strong>Posted Date</strong> <?=date('d M y' , strtotime($row->postdate))?> <br />
		 <?php if($rate != 1):?> <strong>Exchange Rate</strong> $ 1 USD -> <?=$rate?> <?=$ngo_currency?><br /><?php endif;?>
		 
		  	<strong>Loan </strong> $<?=money_format('%(#10n', $row->amount)?> USD  / <?=money_format('%(#10n', $row->amount*$rate)?> <?=$ngo_currency?>  (Principal)<br />
		  		$<?=money_format('%(#10n', $row->amount*($loaninterest/100))?> USD  / <?=money_format('%(#10n', ($row->amount*$rate)*($loaninterest/100))?> <?=$ngo_currency?> (Interest <?=$loaninterest?>%) <br />

		  <?php if($repaymenttotal = $this->Ngo_model->total_repayment_amount($projectID)):?>
		  
		  			  <?php $percenttotal = floor(($repaymenttotal['phptotal']/$row->amount)*100);?>

			  <strong>Repayment</strong> <?=$percenttotal?>% Returned<br />
			  $<?=money_format('%(#10n', $repaymenttotal['phptotal'])?> USD / $<?=money_format('%(#10n', $row->amount*(($loaninterest/100)+1))?> USD 
			  
			  (<?=money_format('%(#10n', $repaymenttotal['ngototal'])?> <?=$ngo_currency?> / <?=money_format('%(#10n', ($row->amount*$rate)*(($loaninterest/100)+1))?> <?=$ngo_currency?>) 
			  
		  <?php else:?>
		  
			  <strong>Amount Requested</strong>  <?=$percenttotal?>% Funded <br />
			  $<?=money_format('%(#10n', $total)?>/$<?=money_format('%(#10n', $row->amount)?> USD
			  <br />
				   
				  
				   
		  <?php endif;?>
		 <br />
		  	
				    	<?php
		    		if($loaninterest){
		    			$loaninterest = ($loaninterest/100)+1;
		    			$totalrequested = $totalrequested * $loaninterest;
		    			
		    		} 
			    		$ngopayments = ($totalrequested*$rate)/$loan_terms;
			    		$payments = ($totalrequested)/$loan_terms;
			    		
			    		?>
		    	<strong>Loan Re-Payment Schedule </strong> <?=date("d M y", strtotime($loandate));?> to <?=date("d M y", strtotime($loandate . ' +' . $loan_terms . ' Weeks' ));?><br />
		    	<strong>Each Payment is </strong> <?=money_format('%(#10n', $ngopayments) . " " . $ngo_currency;?> / $<?=money_format('%(#10n', $payments)?> USD
          
         
  </div>
<?php endforeach;?>
		    
		    		    
		    
		    
		    <?php if($repaymentquery = $this->Ngo_model->view_repayment($projectID)):?>
		    <h2>Loans Transaction</h2>
		    <table class="table table-hover">
	            <thead>
	            <tr>
	            	<td>Date</td>
	            	<td>Payment Type</td>
					<td><?=$ngo_currency?> Amount</td>
	            	<td>USD Amount</td>
	            	
	            </tr>
	            </thead>
	            <tbody>
	            	<?php
	            		$savingTotal = array(0,0);
	            		$loanBalance = array(0,0);
	            	?>
	            		<?php foreach($repaymentquery as $row):?>
	            		<?php if($row->paymenttype == "Loan"):?>
	            		
	            		
	            		<tr>
	            			<td><?=date('d M y', strtotime($row->date))?></td>
	            			<td><?=$row->paymenttype?></td>
	            			<td><?=money_format('%(#10n', $row->ngoamount) . " " . $ngo_currency;?> </td>
	            			<td>$<?=money_format('%(#10n', $row->amount)?> USD	</td>
	            				            			
	            		</tr>
	            		<?php
		            		$loanBalance[0]+= $row->ngoamount;
		            		$loanBalance[1]+= $row->amount;

	            		
	            		?>
	            		<?php endif;?>
	            		<?php endforeach;?>
	            	
	            			            		<tr class="total">
	            			<td colspan="2" style="text-align:right;">Loan Balance</td>
	            			<td><?=money_format('%(#10n', ($totalrequested*$rate)-$loanBalance[0]) . " " . $ngo_currency;?> </td>
	            			<td>$<?=money_format('%(#10n', $totalrequested-$loanBalance[1])?> USD	</td>
	            				            			
	            		</tr>

	            </tbody>
	        </table>
	   

			    <?php endif;?>
			   		    
		    
		    
		    
		    
		    <?php if($repaymentquery = $this->Ngo_model->view_savings($projectID)):?>
		    <h2>Savings </h2>
		    
		    <table class="table table-hover">
	            <thead>
	            <tr>
	            	<td>Date</td>
	            	<td>Payment Type</td>
					<td><?=$ngo_currency?> Amount</td>
	            	<td>USD Amount</td>
	            	
	            </tr>
	            </thead>
	            <tbody>
	            	<?php
	            		$savingTotal = array(0,0);
	            		$loanBalance = array(0,0);
	            	?>
	            		<?php foreach($repaymentquery as $row):?>
	            		<?php if($row->paymenttype != "Loan"):?>
		            		
		            	
	            		
	            		<tr>
	            			<td><?=date('d M y', strtotime($row->date))?></td>
	            			<td><?=$row->paymenttype?></td>
	            			<td><?=money_format('%(#10n', $row->ngoamount) . " " . $ngo_currency;?> </td>
	            			<td>$<?=money_format('%(#10n', $row->amount)?> USD	</td>
	            				            			
	            		</tr>
	            		
	            		<?php 
	            			$savingTotal[0]+= $row->ngoamount;
		            		$savingTotal[1]+= $row->amount;
	            		 
	            		?>
	            		<?php endif; ?>
	            		<?php endforeach;?>
	            		<tr class="total">
	            			<td colspan="2" style="text-align:right;">Saving Total</td>
	            			<td><?=money_format('%(#10n', $savingTotal[0]) . " " . $ngo_currency;?> </td>
	            			<td>$<?=money_format('%(#10n', $savingTotal[1])?> USD	</td>
	            				            			
	            		</tr>
	            			            

	            </tbody>
	        </table>
	   

		    <?php endif;?>
		    
		      
    
                
      
      <script>

</script>

<?php $this->load->view('/dashboard/footer_view');?>