<style type="text/css">
	.total td{
		font-weight: bold;
	}
</style>
<?php $query = $this->Ngo_model->viewProject($projectID);
  $total = $this->Ngo_model->get_total_funded($projectID);
    foreach($query as $row):?>
    <?php 
	            		$ngo_currency = $row->currency;
		            	$loan_terms = $row->loanterms;
		            	$loandate = $row->loandate;	
		            	$loaninterest = $row->loaninterest;
		            	$totalrequested = $row->amount;
		            	$rate= $row->rate;
	            		?>
<?php $percenttotal = floor(($total/$row->amount)*100);?>
  <div class="row" style="padding:5px;">
    <div class="span3">
    <a href="#" class="thumbnail">
      <img src="<?php if($row->pics != "") { echo $row->pics;} else { echo base_url('images') . "/no_image.jpg";}?>">
    </a>
    </div>
    <div class="span8">
			<a class="btn pull-right" href="<?=site_url('ngo/printProject/' . $projectID);?>" target="_blank">Print Application Summery</a>
          <h1><?=$row->title?></h1>
          <h4><?=$row->location?> | <?=$row->category?></h4>
         
          
                   <dl class="dl-horizontal">
		  <dt>Name</dt>
		  <dd><?=$row->name?></dd>
		  <dt>Posted Date</dt>
		  <dd><?=date('d M y' , strtotime($row->postdate))?></dd>
		  <dt>Exchange Rate</dt>
		  <dd>$ 1 USD -> <?=$rate?> <?=$ngo_currency?></dd>
		  
		  <?php if($repaymenttotal = $this->Ngo_model->total_repayment_amount($projectID)):?>
		  	<dt>Loan </dt>
		  		<dd>$<?=money_format('%(#10n', $row->amount)?> USD  / <?=money_format('%(#10n', $row->amount*$rate)?> <?=$ngo_currency?>  (Principal)<br />
		  		$<?=money_format('%(#10n', $row->amount*($loaninterest/100))?> USD  / <?=money_format('%(#10n', ($row->amount*$rate)*($loaninterest/100))?> <?=$ngo_currency?> (Interest <?=$loaninterest?>%) 
		  		</dd>
		  	<dt>Total Due</dt>
		  	<dd>
			  $<?=money_format('%(#10n', $repaymenttotal['phptotal'])?> USD / $<?=money_format('%(#10n', $row->amount*(($loaninterest/100)+1))?> USD <br />
			  <?=money_format('%(#10n', $repaymenttotal['ngototal'])?> <?=$ngo_currency?> / <?=money_format('%(#10n', ($row->amount*$rate)*(($loaninterest/100)+1))?> <?=$ngo_currency?> 
			  <?php $percenttotal = floor(($repaymenttotal['phptotal']/$row->amount)*100);?>
		


			   <div class="progress">
       
				<div class="bar" style="width: <?=$percenttotal?>%;"></div>
				   <label><?=$percenttotal?>% Returned</label>
				</div>
            </div>
            
			  </dd>
		  <?php else:?>
			  <?php if ($row->fundingsource == "PHP"):?>
			  
				  <dt>Amount</dt>
				  <dd>$<?=$total?>/$<?=$row->amount?> USD
			  <div class="progress">
	       
					<div class="bar" style="width: <?=$percenttotal?>%;"></div>
					   <label><?=$percenttotal?>% Funded</label>
					</div>
	            </div>
	            </dd>
	          <?php else:?>
	          	  <dt>Repayment Amount</dt>
			  <dd>$<?=money_format('%(#10n', $repaymenttotal['phptotal'])?> USD / $<?=$row->amount?> USD <br />
			  <?=money_format('%(#10n', $repaymenttotal['ngototal'])?> <?=$ngo_currency?> / <?=$row->amount*$rate?> <?=$ngo_currency?>
			  <?php $percenttotal = floor(($repaymenttotal['phptotal']/$row->amount)*100);?>
		


			   <div class="progress">
       
				<div class="bar" style="width: 0%;"></div>
				   <label>0% Returned</label>
				</div>
            </div>
	          <?php endif;?>
	          
		   
		  <?php endif;?>
		  
		  
  		</dl>
          
         
  </div>
    <hr>
<?php endforeach;?>
            
    <div class="tabbable" style="padding:10px;"> <!-- Only required for left/right tabs -->
	    <ul class="nav nav-tabs">
		    <li class="active"><a href="#tab2" data-toggle="tab">Repayment</a></li>
		    <li><a href="#tab3" data-toggle="tab">Savings</a></li>
		    <li><a href="#tab4" data-toggle="tab">Payment Schedule</a></li>
	    </ul>
	    <div class="tab-content">
			
		    <div class="tab-pane active" id="tab2">
		    
		    
		    
		    
		    <?php if($repaymentquery = $this->Ngo_model->view_repayment($projectID)):?>
		    Payment<br />
		    <table class="table table-hover">
	            <thead>
	            <tr>
	            	<td>Date</td>
	            	<td>Payment Type</td>
					<td><?=$ngo_currency?> Amount</td>
	            	<td>USD Amount</td>
	            	
	            </tr>
	            </thead>
	            <tbody>
	            	<?php
	            		$savingTotal = array(0,0);
	            		$loanBalance = array(0,0);
	            	?>
	            		<?php foreach($repaymentquery as $row):?>
	            		<?php if($row->paymenttype == "Loan"):?>
	            		
	            		
	            		<tr>
	            			<td><?=date('d M y', strtotime($row->date))?></td>
	            			<td><?=$row->paymenttype?></td>
	            			<td><?=money_format('%(#10n', $row->ngoamount) . " " . $ngo_currency;?> </td>
	            			<td>$<?=money_format('%(#10n', $row->amount)?> USD	</td>
	            				            			
	            		</tr>
	            		<?php
		            		$loanBalance[0]+= $row->ngoamount;
		            		$loanBalance[1]+= $row->amount;

	            		
	            		?>
	            		<?php endif;?>
	            		<?php endforeach;?>
	            	
	            			            		<tr class="total">
	            			<td colspan="2" style="text-align:right;">Loan Balance</td>
	            			<td><?=money_format('%(#10n', (($totalrequested*$rate) *(($loaninterest/100)+1))-$loanBalance[0]) . " " . $ngo_currency;?> </td>
	            			<td>$<?=money_format('%(#10n', (($totalrequested)*(($loaninterest/100)+1))-$loanBalance[1])?> USD	</td>
	            				            			
	            		</tr>

	            </tbody>
	        </table>
	   

			    <?php endif;?>
			    <div class="well">
				    <p>By entering a payment,  Please enter monetary value in <?=$ngo_currency?></p>
				    <?php 
				    	$projectStatus = $this->Ngo_model->get_project_status($projectID);
				    if($projectStatus != 'Pending Closing' && $projectStatus != 'Closed' ):?>
				    <form method="post" action="<?=site_url('ngo/repaymentProcess')?>">
					    <label>Payment Date </label><input type="text" name="date" class="datepicker"/>
						<input type="hidden" class="input-xxlarge" name="paymenttype" id="paymenttype" value="Loan" />
						<label>Amount <?=$ngo_currency?></label><input type="text" name="amount" />
					    <input type="hidden" name="projectid" value="<?=$projectID?>" />
					    <input type="hidden" name="rate" value="<?=$rate?>" />
					    <input class="btn btn-success"type="submit" value="Send" />
					</form>
				    
				    <?php else:?>
				    <h2>This Loan is <?=$projectStatus?></h2>
				    <?php endif;?>
				    <a href="<?=site_url('ngo/closeProject/' . $projectID)?>" class="btn btn-warning" > Close Loan</a>
			    </div>
		    </div>
		    
		    <div class="tab-pane" id="tab3">
		    Savings<br />
		    
		    
		    
		    <?php if($repaymentquery = $this->Ngo_model->view_savings($projectID)):?>
		    
		    <table class="table table-hover">
	            <thead>
	            <tr>
	            	<td>Date</td>
	            	<td>Payment Type</td>
					<td><?=$ngo_currency?> Amount</td>
	            	<td>USD Amount</td>
	            	
	            </tr>
	            </thead>
	            <tbody>
	            	<?php
	            		$savingTotal = array(0,0);
	            		$loanBalance = array(0,0);
	            	?>
	            		<?php foreach($repaymentquery as $row):?>
	            		<?php if($row->paymenttype != "Loan"):?>
		            		
		            	
	            		
	            		<tr>
	            			<td><?=date('d M y', strtotime($row->date))?></td>
	            			<td><?=$row->paymenttype?></td>
	            			<td><?=money_format('%(#10n', $row->ngoamount) . " " . $ngo_currency;?> </td>
	            			<td>$<?=money_format('%(#10n', $row->amount)?> USD	</td>
	            				            			
	            		</tr>
	            		
	            		<?php 
	            			$savingTotal[0]+= $row->ngoamount;
		            		$savingTotal[1]+= $row->amount;
	            		 
	            		?>
	            		<?php endif; ?>
	            		<?php endforeach;?>
	            		<tr class="total">
	            			<td colspan="2" style="text-align:right;">Saving Total</td>
	            			<td><?=money_format('%(#10n', $savingTotal[0]) . " " . $ngo_currency;?> </td>
	            			<td>$<?=money_format('%(#10n', $savingTotal[1])?> USD	</td>
	            				            			
	            		</tr>
	            			            

	            </tbody>
	        </table>
	   

		    <?php endif;?>
		    <div class="well">
			    <form method="post" action="<?=site_url('ngo/repaymentProcess')?>">
				    <label>Transaction Date </label><input type="text" name="date" class="datepicker"/>
					<input type="hidden" class="input-xxlarge" name="paymenttype" id="paymenttype" value="Saving" />
					<label>Amount <?=$ngo_currency?></label><input type="text" name="amount" />
				    <input type="hidden" name="projectid" value="<?=$projectID?>" />
				    <input type="hidden" name="rate" value="<?=$rate?>" />
				    
				    <input class="btn btn-success"type="submit" value="Send" />
				</form>
	
		    </div>		
		    </div>
			 <div class="tab-pane" id="tab4">
			 <?php
		    		if($loaninterest){
		    			$loaninterest = ($loaninterest/100)+1;
		    			
		    			$ngoprincipal = ($totalrequested*$rate)/$loan_terms;
			    		$principal = ($totalrequested)/$loan_terms;
			    		
		    			
		    			$totalrequested = $totalrequested * $loaninterest;
		    		
		    			$justinterest = $loaninterest -1;	
		    		} 
			    		$ngopayments = ($totalrequested*$rate)/$loan_terms;
			    		$payments = ($totalrequested)/$loan_terms;
			    		
			    		$ngointerest = $ngoprincipal * $justinterest;
			    		$interest = $principal * $justinterest;
			    		
			    		?>
		    Payment Schedule<br />
		    <table class="table table-hover">
		    <thead>
		    	<tr>
		    		<td>Date</td>
		    		<td>Principal</td>
					<td>Interest (<?=$justinterest*100?>%)</td>
		    		<td>Payment</td>
		    	</tr>
		    </thead>
		    <tbody>	
		    	
		    	<?php for($x=0; $x < $loan_terms; $x++):?>
		    	<tr>
		    		<td><?=date("m/d/y", strtotime($loandate . ' +' . $x . ' Weeks' ));?></td>
					<td><?=money_format('%(#10n', $ngoprincipal) . " " . $ngo_currency;?> <br />
						$ <?=money_format('%(#10n', $principal);?> USD</td>
			    		<td><?=money_format('%(#10n', $ngointerest) . " " . $ngo_currency;?> <br />
						$ <?=money_format('%(#10n', $interest);?> USD</td>
			    		
			    		<td><?=money_format('%(#10n', $ngopayments) . " " . $ngo_currency;?> <br /> $<?=money_format('%(#10n', $payments)?> USD</td>
		    	</tr>
		    	<?php endfor;?>
		    	</tbody>
		    </table>
		    
		    
		   
		    </div>

	    </div>
    </div>
    
    
                
      
      <script>

</script>

<?php $this->load->view('/dashboard/footer_view');?>