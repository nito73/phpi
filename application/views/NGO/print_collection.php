<style type="text/css">
table{
	
	border-spacing: none;
	border-collapse: collapse;
}
td{
	border: 1px solid black;
}
thead td{
	border: 3px solid black;
	background-color: yellow;
	font-weight: bold;
	
}
label{
	font-weight: bold;
}
</style>
<h1>Print Collection Sheet <?php if($view != "All"){echo $view;}?></h1>
<div>
	<label>NGO :</label><br />
	<label>Officer: </label> <?=$this->tank_auth->get_orgname();?><br />
	<label>Location: </label><?php if($view != "All"){echo $view;}?><br />
	<label>Date: </label> <?=date('d/m/y')?><br />
</div>


<table>
	<thead>
		<tr>
			<td>Client ID</td>
			<td>Loan Number</td>
			<td>Type</td>
			<td>Industry</td>
			<td>Principal</td>
			<td>Intrest</td>
			<td>Standard Pmt</td>
			<td>Amount Due</td>
			<td>Amount Collected</td>
			<td>Past Due</td>
			<td>Amount Due Next Payment</td>
			<td>Client Signature</td>
			<td>Officer Signature</td>
			<td>Date</td>
		</tr>
	</thead>
<tbody>
<?php if ($query = $this->Ngo_model->get_org_funded_projects($view)):?>
<?php foreach($query as $fundedrow):?>
	 <?php $query = $this->Ngo_model->viewProject($fundedrow->id);
  $total = $this->Ngo_model->get_total_funded($fundedrow->id);
    foreach($query as $row):?>
    <?php 
	            		$ngo_currency = $row->currency;
		            	$loan_terms = $row->loanterms;
		            	$loandate = $row->loandate;	
		            	$loaninterest = $row->loaninterest;
		            	$totalrequested = $row->amount;
		            	$rate= $row->rate;
	            		?>
	            		
	     
		    	 <?php
		    		if($loaninterest){
		    			$loaninterest = ($loaninterest/100)+1;
		    			
		    			$ngoprincipal = ($totalrequested*$rate)/$loan_terms;
			    		$principal = ($totalrequested)/$loan_terms;
			    		
		    			
		    			$totalrequested = $totalrequested * $loaninterest;
		    		
		    			$justinterest = $loaninterest -1;	
		    		} 
			    		$ngopayments = ($totalrequested*$rate)/$loan_terms;
			    		$payments = ($totalrequested)/$loan_terms;
			    		
			    		$ngointerest = $ngoprincipal * $justinterest;
			    		$interest = $principal * $justinterest;
			    		
			    		?>
		    	<?php for($x=1; $x < $loan_terms+1; $x++):?>
		    	<?php $payment = date("m/d/y", strtotime($loandate . ' +' . $x . ' Weeks' ));?>
		    	<?php if(strtotime($payment) > strtotime('today')):?>
		    	
		    	<?php $lastpayment = $this->Ngo_model->last_payment_made($fundedrow->id);
			    	$pastdue = 0;
			    	if($lastpayment != 0 && $lastpayment < $ngopayments){
						$pastdue = $ngopayments - $lastpayment;
						}
					?>

		    	<tr>
		    	
		    		<td>Client ID</td>
			<td><?=$row->phpid?></td>
			<td><?=$row->type?></td>
			<td><?=$row->category?></td>
			<td><?=money_format('%(#10n', $ngoprincipal) . " " . $ngo_currency;?></td>
			<td><?=money_format('%(#10n', $ngointerest) . " " . $ngo_currency;?></td>
			<td><?=money_format('%(#10n', $ngopayments) . " " . $ngo_currency;?></td>
			<td><?=money_format('%(#10n', $pastdue + $ngopayments) . " " . $ngo_currency;?></td>
			<td>&nbsp;</td>
			<td><?php if($pastdue != 0){
				echo money_format('%(#10n', $pastdue) . " " . $ngo_currency;
				}
				?>			
			</td>
			<td><?=$payment?></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		    		
		    	
			    	
		    	</tr>
		    	<?php break;
		    	endif;?>
		    	<?php endfor;?>

		  
	<?php endforeach;?>	            		
<?php endforeach;?>
</tbody>
</table>
<?php endif;?>