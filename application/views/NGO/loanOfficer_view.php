<?php $this->load->view('/dashboard/sidebar_view');?>
<link rel="stylesheet" href="/imageupload/css/jquery.fileupload-ui.css">

    <div style="padding:10px;">
        <p><a class="btn pull-right btn-success addLoanOfficer" href="#addLoanOfficer" >Add Loan Officer</a></p>
        <h1>Loan Officers</h1>
		<?php if($msg = $this->session->flashdata('message')):?>
			    <div class="alert alert-<?=$msg['type']?>" style="margin-top: 10px">
			    	<a class="close" data-dismiss="alert" href="#">&times;</a>
				    <h4 class="alert-heading"><?=$msg['body']?></h4>
			    </div>
			    <?php endif;?>
			  
      

            <table class="table">
	            <thead>
	            <tr>
	            	<td>Name</td>
	            	
	            	<td>Username</td>
	            	<td>Email</td>
	            	
	            	<td>View Loan Officer</td>
	            		            </tr>
	            </thead>
	            <tbody class="userTable">
	            	<?php if($query = $this->Ngo_model->get_loanOfficers($this->tank_auth->get_orgid())):?>
	            		<?php foreach($query as $row):?> 
							<tr>
								<td><?=$row->firstname?> <?=$row->lastname?></td>
								<td><?=$row->username?></td>
								<td><?=$row->email?></td>
								
								<td><a href="<?=base_url()?>settings/viewLoanOfficer/<?=$row->id?>" class="btn btn-success" >View User</a></td>
								
							</tr>	            	
	            		<?php endforeach;?>
	            			
	            	<?php endif;?>
	            </tbody>
    </table>
    	                
      </div>

    <div id="addLoanOfficer" class="modal hide fade" >
					<form id="addLoanOfficerForm" method="post">

				<div class="modal-header">
					<a href="#" class="close" data-dismiss="modal">&times;</a>
					<h3>Add Loan Officer</h3>
					</div>
				<div class="modal-body">
					<div class="divDialogElements">
						<label>Photo</label>
						<img src="" class="uploadimage"/>
	    
	    
						<span class="btn btn-success fileinput-button">
					        <i class="icon-plus icon-white"></i>
					        <span>Select files...</span>
						    <input id="fileupload" type="file" name="files[]" multiple>
					    </span>
					    
					   	    <div id="progress" style="width:75%;" class="progress progress-success progress-striped">
					        <div class="bar" ></div>
					    </div>
					    	<input type="hidden" name="pic" id="pic" placeholder="URL of Picture">
							<label>Name</label>
							<input class="large" id="firstname" name="firstname" type="text" placeholder="First Name"/>
							<input class="large" id="lastname" name="lastname" type="text" placeholder="Last Name"/>
							<label>Username</label>
							<input class="xlarge" id="username" name="username" type="text" />
							<label>Password</label>
							<input class="xlarge" id="password" name="password" type="password" />
							<label>Email</label>
							<input class="xlarge" id="email" name="email" type="text" />
							
						
						</div>
					</div>
				<div class="modal-footer">
					<a href="#" class="btn close-modal" >Cancel</a>
					<input type="submit" href="#" class="btn btn-success addLoanOfficerFormBtn" value="Add" />
					</div>
					</form>
				</div>
 <script src="/imageupload/js/vendor/jquery.ui.widget.js"></script>
<script src="/imageupload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="/imageupload/js/jquery.fileupload.js"></script>
<script src="/imageupload/js/jquery.fileupload-image.js"></script>

   
      
      <script>
		  $(function(){
		  
		  	   	   'use strict';
    // Change this to the location of your server-side upload handler:
   var url = (window.location.hostname === 'blueimp.github.com' ||
                window.location.hostname === 'blueimp.github.io') ?
                '//jquery-file-upload.appspot.com/' : '<?=base_url();?>/imageupload/server/php/';

    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        disableImageResize: /Android(?!.*Chrome)|Opera/
        .test(window.navigator && navigator.userAgent),
    imageMaxWidth: 250,
    imageMaxHeight: 250,
    imageCrop: true, // Force cropped images
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
              	
                $('#pic').val(file.url);
                $('.uploadimage').attr('src', file.url);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .bar').css(
                'width',
                progress + '%'
            );
        }
    });
    
			  $('#addLoanOfficer').modal('hide');
			  $('.close-modal').click(function(event){
					$('#addLoanOfficer').modal('hide');
					event.preventDefault();	  
				  
			  });
			  $('.addLoanOfficer').click(function(event){
					$('#addLoanOfficer').modal('show');
					event.preventDefault();	  
				  
			  });
			  
			   $('.addLoanOfficerFormBtn').click(function(event){
						 $.ajax({
							  type: "POST",
							  url: '<?=base_url();?>ngo/addLoanOfficerProcess',
							  dataType: 'JSON',
							  data: {  firstname: $("#firstname").val(), lastname: $("#lastname").val(), username: $("#username").val(),  email: $("#email").val(),   phone: $("#phone").val(),  password: $("#password").val(), pic:$("#pic").val() },
							  success: function(data) { 
							  	console.log('hi');
							  	$('.userTable').append("<tr><td>" + $("#firstname").val() + " " + $("#lastname").val() + "</td><td>" + $("#username").val() + "</td><td>" + $("#email").val() + "</td><td> <a href=\"<?=base_url()?>settings/viewLoanOfficer/" + data.userid + "\" class=\"btn btn-success \" >View User</a></td></tr>");
							  	$('#addLoanOfficer').modal('hide');
							  	
							  								   },
							});
							
							 event.preventDefault();
				  
			  });
		  });
</script>

<?php $this->load->view('/dashboard/footer_view');?>