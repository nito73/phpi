<?php $this->load->view('/dashboard/sidebar_view');?>

    <div style="padding:10px;">
        <h1>Reserve Account</h1>
		<?php if($msg = $this->session->flashdata('message')):?>
			    <div class="alert alert-<?=$msg['type']?>" style="margin-top: 10px">
			    	<a class="close" data-dismiss="alert" href="#">&times;</a>
				    <h4 class="alert-heading"><?=$msg['body']?></h4>
			    </div>
			    <?php endif;?>
			  
      

            <table class="table">
	            <thead>
	            <tr>
	            	<td>Date</td>
	            	
	            	<td>Type</td>
	            	<td>Rate</td>
	            	<td>PHP Amount</td>
	            	<td>NGO Amount</td>
	            	<td>Diffrence</td>
	            	<td>View Report</td>
	            		            </tr>
	            </thead>
	            <tbody class="userTable">
	            	<?php 
	            		$reserveAccount = 0;
	            		$currency = "";
	            	if($query = $this->Ngo_model->get_reserveAccount($this->tank_auth->get_orgid())):?>
	            		<?php foreach($query as $row):?> 
							<tr>
								<td><?=date("d M Y", strtotime($row->timestamp))?></td>
								<td><?=$row->type?></td>
								<td><?=money_format('%(!#10n', $row->rate) . ' ' . $row->currency?> -> $1</td>
								<td>$<?=money_format('%(!#10n', $row->php_amount)?></td>
								<td><?=money_format('%(!#10n', $row->ngo_amount) . ' ' . $row->currency?></td>
								<td><?=money_format('%(!#10n', $row->difference) . ' ' . $row->currency?></td>
								
								<td><?php if($row->data):?><a href="<?=base_url()?>ngo/reserveAccountReport/<?=$row->id?>" target="_blank" class="btn btn-success" >View Report</a><?php endif;?></td>
								
							</tr>	            	
							<?php $reserveAccount = $reserveAccount +  $row->difference;
								$currency = $row->currency;
							?>
	            		<?php endforeach;?>
	            			
	            	<?php endif;?>
	            	
	            	<tr style="font-size:16px; font-weight:bold;">
		            	<td colspan="5" style="text-align:right;">Total Reserve Account</td>
		            	<td colspan="2"><?=money_format('%(!#10n', $reserveAccount) . ' ' . $currency?></td>
	            	</tr>
	            </tbody>
    </table>
    	                
      </div>

 <script src="/imageupload/js/vendor/jquery.ui.widget.js"></script>
<script src="/imageupload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="/imageupload/js/jquery.fileupload.js"></script>
<script src="/imageupload/js/jquery.fileupload-image.js"></script>

   
      
      <script>
		  $(function(){
		  
		  	   	   'use strict';
    // Change this to the location of your server-side upload handler:
   var url = (window.location.hostname === 'blueimp.github.com' ||
                window.location.hostname === 'blueimp.github.io') ?
                '//jquery-file-upload.appspot.com/' : '<?=base_url();?>/imageupload/server/php/';

    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        disableImageResize: /Android(?!.*Chrome)|Opera/
        .test(window.navigator && navigator.userAgent),
    imageMaxWidth: 250,
    imageMaxHeight: 250,
    imageCrop: true, // Force cropped images
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
              	
                $('#pic').val(file.url);
                $('.uploadimage').attr('src', file.url);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .bar').css(
                'width',
                progress + '%'
            );
        }
    });
    
			  $('#addLoanOfficer').modal('hide');
			  $('.close-modal').click(function(event){
					$('#addLoanOfficer').modal('hide');
					event.preventDefault();	  
				  
			  });
			  $('.addLoanOfficer').click(function(event){
					$('#addLoanOfficer').modal('show');
					event.preventDefault();	  
				  
			  });
			  
			   $('.addLoanOfficerFormBtn').click(function(event){
						 $.ajax({
							  type: "POST",
							  url: '<?=base_url();?>ngo/addLoanOfficerProcess',
							  dataType: 'JSON',
							  data: {  firstname: $("#firstname").val(), lastname: $("#lastname").val(), username: $("#username").val(),  email: $("#email").val(),   phone: $("#phone").val(),  password: $("#password").val(), pic:$("#pic").val() },
							  success: function(data) { 
							  	console.log('hi');
							  	$('.userTable').append("<tr><td>" + $("#firstname").val() + " " + $("#lastname").val() + "</td><td>" + $("#username").val() + "</td><td>" + $("#email").val() + "</td><td> <a href=\"<?=base_url()?>settings/viewLoanOfficer/" + data.userid + "\" class=\"btn btn-success \" >View User</a></td></tr>");
							  	$('#addLoanOfficer').modal('hide');
							  	
							  								   },
							});
							
							 event.preventDefault();
				  
			  });
		  });
</script>

<?php $this->load->view('/dashboard/footer_view');?>