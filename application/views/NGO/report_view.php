<?php $this->load->view('/dashboard/sidebar_view');?>
   <style type="text/css">
   
   </style>
       <!-- Jumbotron -->
      <div style="padding:10px;">
		 <h1>Reports</h1> 

      </div>
      <div class="row-fluid">
      	<div class="span4">
	  <h3>Print Loan Summary Report</h3>
	  <br />
	  <form action="<?=site_url('/reports/dateProcess')?>" method="POST" target="_blank">
	  	<label>Funding Source</label>
	  	<select name="fundingsource">
		  	<option value="All">All</option>
		  	<option value="PHP">People Helping People</option>
		<?php $fundrow = $this->Ngo_model->get_funding_source();
    if ($fundrow):?>
    <?php foreach($fundrow as $row):?>
    	<option value="<?=$row->fundingsource?>"><?=$row->fundingsource?></option>
    <?php endforeach?>
    <?php endif;?>
		  	
	  	</select>
	  	<label for="startdate">Start Date</label>
	  	<input class="input-xlarge" type="text" name="startdate" id="startdate" placeholder="Start Date">
	  	<label for="endate">End Date</label>
	  	<input class="input-xlarge" type="text" name="enddate" id="enddate" placeholder="End Date">
	  	<input type="submit" value="Print Report" class="btn btn-primary" />
	  </form>
      	</div>
      </div>
 
 
 <script>
 	$(function(){
	

    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    
	    var checkin = $('#startdate').datepicker().on('changeDate', function(ev) {
		    	if (ev.date.valueOf() > checkout.date.valueOf()) {
					var newDate = new Date(ev.date)
					newDate.setDate(newDate.getDate());
					checkout.setValue(newDate);
				}
			checkin.hide();
			$('#enddate')[0].focus();
			}).data('datepicker');
			
			var checkout = $('#enddate').datepicker({
				onRender: function(date) {
				return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
				}
			}).on('changeDate', function(ev) {
				checkout.hide();
			}).data('datepicker');

	});
 </script>