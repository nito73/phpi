<?php $this->load->view('/admin/sidebar_view');?>
   
        <h1>NGOs</h1>
    
            <table class="table table-hover">
	            <thead>
	            <tr>
	            	
	            	<td>Name</td>
	            	<td>Organization</td>
	            	<td>Email</td>
	            	<td>Address</td>
	            	<td>View Applications</td>
	            </tr>
	            </thead>
	            <tbody>
	            
	            	<?php $query = $this->Admin_model->get_all_NGOS();
	            		foreach($query as $row):?>
	            		
	            		
	            		<tr>
	            			<td><?=$row->firstname?> <?=$row->lastname?></td>
	            			<td><?=$row->organization?></td>
	            			<td><?=$row->email?></td>
	            			<td><?=$row->address?><br />
	            				<?=$row->city?>, <?=$row->state?> <?=$row->zip?><br />
	            				<?=$row->country?></td>
	            		<td><a class="btn-success btn" href="/admin/viewNGOsProjects/<?=$row->id?>" > View <?=$row->organization?> Application(s)</a></td>
	            		</tr>
	            		<?php endforeach;?>
	            </tbody>
    </table>
    

      
      <script>

</script>

<?php $this->load->view('/dashboard/footer_view');?>