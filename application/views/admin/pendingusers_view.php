<?php $this->load->view('/admin/sidebar_view');?>
   
      
            <table class="table table-hover">
	            <thead>
	            <tr>
	            	<td>Name</td>
	            	<td>Email</td>
	            	<td>Organization</td>
	           
	            	<td>Status</td>
	            </tr>
	            </thead>
	            <tbody>
	            
	            	<?php if ($query = $this->Admin_model->get_pending_users()):?>
	            		<?php foreach($query as $row):?>
	            		
	            		
	            		<tr id="user<?=$row->id?>">
	            		
	            			<td><?=$row->firstname?> <?=$row->lastname?></td>
	            			<td><?=$row->email?></td>
	            			<td><?=$row->organization?></td>
	            		
	            			<td>  <a href="<?=base_url();?>admin/deleteuser/<?=$row->id?>" rel="<?=$row->id?>" class="deleteuser btn btn-large btn-danger pull-right" style="margin-left:10px;"> Delete User </a><a href="<?=base_url();?>admin/approveuser/<?=$row->id?>" rel="<?=$row->id?>" class="approveuser btn btn-large btn-success pull-right"> Approve User </a></td>
	            		</tr>
	            		<?php endforeach;?>
	            		<?php else:?>
	            		<tr>
	            			<td colspan="5"> No Pending Users</td>
	            		</tr>
	            		<?php endif;?>
	            </tbody>
    </table>
    
   
      
 
      <script>
$(function(){
	   	  
$(".approveuser").click(function(event) {
	var userid = $(this).attr('rel');
	 $.ajax({
	  type: "POST",
	  url: '<?=base_url();?>admin/approveuser',
	  data: { userid : userid },
	  success: function(data) { 
	  	$("#user" + userid).hide();
	   },
	});
	
	
		 event.preventDefault();
});

$(".deleteuser").click(function(event) {
	var userid = $(this).attr('rel');
	 $.ajax({
	  type: "POST",
	  url: '<?=base_url();?>admin/deleteuser',
	  data: { userid : userid },
	  success: function(data) { 
	  	$("#user" + userid).hide();
	   },
	});
	
	
		 event.preventDefault();
});

  });
  
  


</script>


<?php $this->load->view('/dashboard/footer_view');?>