<?php $this->load->view('/admin/sidebar_view');?>

   
       <!-- Jumbotron -->

            <table class="table table-hover dataTables">
	            <thead>
	            <tr>
	            	<td>PHP ID</td>
	            	<td>Name</td>
	            	<td>Type</td>
	            	<td>NGO</td>
	            	<td>Amount Funded /Amount Requested</td>
	            	<td>View More</td>
	            </tr>
	            </thead>
	            <tbody>
	            
	            	<?php $query = $this->Admin_model->get_all_projects();
	            		foreach($query as $row):?>
	            		
	            		
	            		<tr>
	            			<td><?=$row->projectid?></td>
	            			<td><?=$row->title?></td>
	            			<td><?=$row->apptype?></td>
	            			<td><?=$row->organization?></td>
	            			<td>Amount Requested  <br />$<?=money_format('%(!#10n', $row->amount)?> <br /><?php if ($funded = $this->Admin_model->get_total_funded($row->projectid)) echo "<br />Amount Funded:$" . money_format('%(!#10n', $funded);?> </td>
	            			<td><a class="btn-success btn" href="/admin/viewClient/<?=$row->client_id?>" > View Application</a>
	            			<a class="btn-danger btn" href="/project/edit/<?=$row->projectid?>" > Edit Application</a></td>
	            		</tr>
	            		<?php endforeach;?>
	            </tbody>
    </table>
    
      
      <script>
	      $(function(){
		      
		      $('.dataTables').dataTable({
			       "sDom": "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
			         "bJQueryUI": true,
                "sPaginationType": "bootstrap",
              
                "bLengthChange": false
			      
			      
			      
		      });
		      
		      $.extend( $.fn.dataTableExt.oStdClasses, {
				    "sWrapper": "dataTables_wrapper form-inline"
				} );
		      
		 });
</script>

<?php $this->load->view('/dashboard/footer_view');?>