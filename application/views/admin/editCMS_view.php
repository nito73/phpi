<style type="text/css">
	#editor{overflow:scroll;max-height:300px;}
</style>
<?php $query = $this->Admin_model->getCMSrow($cmsid);?>
<?php foreach($query as $row):?>
<div style="padding:10px;">
	<h1>Edit Text Area</h1>
	<div>
		<form id="form">
			<label>Area Page: <?=$row->cms_page?></label>
			<label>Area Title: <?=$row->cms_title?></label>
			<label>Brief Application Description</label>
			<div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor">
				<div class="btn-group">
					<a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="icon-text-height"></i>&nbsp;<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
						<li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
						<li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
					</ul>
				</div>
				<div class="btn-group">
					<a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="icon-bold"></i></a>
					<a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="icon-italic"></i></a>
				</div>
			</div>
			<div id="editor" name="contentdesc">
				<?=$row->content?>
			</div>
			<?php endforeach;?>
			<input type="submit" class="btn" value="Submit" />
		</form> 
	</div>
</div>
<script src="/imageupload/js/vendor/jquery.ui.widget.js"></script>
<script src="/imageupload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="/imageupload/js/jquery.fileupload.js"></script>
<script>
$(function(){
	'use strict';
	$("#form").submit(function(event) {
		var contentdesc = $('#editor').html();
		$.ajax({
			type: "POST",
			url: '<?=base_url();?>admin/updateCMS',
			dataType: 'JSON',
			data: { newcontent: contentdesc, edittime: "<?=time()?>", editby: "<?=$this->tank_auth->get_user_id()?>", cmsID: "<?=$cmsid?>" },
			success: function(data) {
				window.location = "<?=base_url();?>/admin";
			},
		});
		event.preventDefault();
	});
});
</script>