<?php $this->load->view('/admin/sidebar_view');?>
   
       <!-- Jumbotron -->
      <div class="jumbotron">
        <h1>Welcome Back <?=$this->tank_auth->get_name();?></h1>
        <?php if ($query=$this->Admin_model->get_pending_projects()):?>

        <p class="lead">Application Pending Approval</p>
        
                	<?php foreach($query as $row):?>
        	<?php $ngo = $this->Admin_model->getOrgByID($row->organization_id);?>
        	<div class="row" id="project<?=$row->id?>">
    <div class="span4">
    <a href="#" class="thumbnail">
      <img src="<?php if($row->pics != "") { echo $row->pics;} else { echo base_url('images') . "/no_image.jpg";}?>">
    </a>
    </div>
    <div class="span8">
          <h1><?=$row->title?> (<?=$row->type?>)</h1>
          <form id="form">
        
        <?php if($row->type == 'Micro'){
	        $phpid = "M$row->id";
        } else if($row->type == "Macro"){
	        $phpid = "MM$row->id";
        } else {
	        $phpid = "P$row->id";
        }
        ?>
		<input class="input-large" type="text" name="phpid<?=$row->id?>" id="phpid<?=$row->id?>" value="<?=$phpid?>" placeholder="PHP ID" >
          <h4><?=$row->location?> | <?=$row->category?></h4>
      <dl class="dl-horizontal">
		  <dt>Name</dt>
		  <dd><?=$row->name?></dd>
		  <dt>Posted Date</dt>
		  <dd><?=date('m/d/y' , strtotime($row->postdate))?></dd>
		  
		  <dt>Amount</dt>
		  <dd>$<?=$row->amount?> USD</dd>
		  
		  
  		</dl>
        <p class="lead">Posted By <?=$ngo['name'];?> (<?=$ngo['organization']?>) <br />
    <a href="#" rel="<?=$row->id?>" class="post btn btn-large btn-success pull-right"> Post </a>
         <a href="#" rel="<?=$row->id?>" class="remove btn btn-large btn-danger pull-right"> Delete </a>
         
          </form>
    </div>
  </div>
    <hr>

        	<?php endforeach;?>
   <?php endif;?>
      </div>
      
      <script>
$(function(){
	   	  
$(".post").click(function(event) {
	var projectid = $(this).attr('rel');
	var phpid = $('#phpid' + projectid).val();
	 $.ajax({
	  type: "POST",
	  url: '<?=base_url();?>admin/approve',
	  data: { id : projectid, phpid : phpid },
	  success: function(data) { 
	  	$("#project" + projectid).hide();
	   },
	});

	
	
		 event.preventDefault();
});


	
	$(".remove").click(function(event) {
	var projectid = $(this).attr('rel');
	var phpid = $('#phpid' + projectid).val();
	 $.ajax({
	  type: "POST",
	  url: '<?=base_url();?>admin/decline',
	  data: { id : projectid, phpid : phpid },
	  success: function(data) { 
	  	$("#project" + projectid).hide();
	   },
	});
		 event.preventDefault();
});
	
	
  });
  
  


</script>

<?php $this->load->view('/dashboard/footer_view');?>