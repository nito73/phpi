<?php $this->load->view('/admin/sidebar_view');?>
   
   <?php if($msg = $this->session->flashdata('message')):?>
   	    <div class="alert alert-<?=$msg['type']?>" style="margin-top: 10px">
   	    	<a class="close" data-dismiss="alert" href="#">&times;</a>
   		    <h4 class="alert-heading"><?=$msg['body']?></h4>
   	    </div>
   	    <?php endif;?>
   	  
	<a href="<?=site_url("admin/viewNGOSettings/")?>/<?=$this->Admin_model->get_NGO_userid($ngoid)?>" class="btn pull-right btn-success"> <i class="icon-pencil"></i> NGO account Settings</a>
	   
	<a href="<?=site_url("admin/ngoUpdates/$ngoid")?>" class="btn pull-right btn-success"> <i class="icon-pencil"></i> New NGO Updates</a>

        <h1><?php $org = $this->Admin_model->getOrgByID($ngoid);
	       echo $org['organization'];
        ?> Application</h1>
    
            <table class="table table-hover">
	            <thead>
	            <tr>
	            	<td>PHP ID</td>
	            	<td>Name</td>
	            	<td>Type</td>
	            	<td>NGO</td>
	            	<td>Amount Funded /Amount Requested</td>
	            	<td>View More</td>
	            </tr>
	            </thead>
	            <tbody>
	            
	            	<?php $query = $this->Admin_model->get_all_NGOs_projects($ngoid);?>
	            	<?php if($query):?>
	            		<?php foreach($query as $row):?>
	            		
	            		
	            		<tr>
	            			<td><?=$row->phpid?></td>
	            			<td><?=$row->title?></td>
	            			<td><?=$row->type?></td>
	            			<td><?=$row->organization?></td>
	            			<td>$<?php if ($funded = $this->Admin_model->get_total_funded($row->projectid)) echo $funded; else echo '0';?> / $<?=$row->amount?></td>
	            			<td><a class="btn-success btn" href="/admin/project/<?=$row->projectid?>" > View Application</a></td>
	            		</tr>
	            		<?php endforeach;?>
	            	<?php else:?>
	            		<tr>
	            			<td colspan="6"> NGO has not submitted any Applications yet</td>
	            		</tr>
	            	<?php endif;?>
	            </tbody>
    </table>
    
    
    
    
    
         
      <script>

</script>

<?php $this->load->view('/dashboard/footer_view');?>

