<?php 
	$ngo_currency = "";
	$clientLoan = $this->Client_model->view_client_loan($clientID);
	?>
<style type="text/css">
	.total td{
		font-weight: bold;
	}
	img{ height: 200px; float: left;}
	table tr td{border: 1px solid black; }
	thead tr td{border:2px solid black; font-weight: bold; background-color: yellow; }
	
	table{ border-collapse: collapse; }	
	.client tr td{border: none;}
	.client img{ height: 150px;}
</style>
<?php $query = $this->Admin_model->viewClient($clientID);

    foreach($query as $row):?>
    <table class="client">
    <tr style="padding-top:10px;">
    	<td>
   <img src="<?php if($row->pics != "") { echo $row->pics;} else { echo base_url('images') . "/no_image.jpg";}?>">
   <br />
   
  
     <strong>Collection Location</strong> <?=$row->collectionlocation?> <br />
     <strong>Name</strong> <?=$row->name?><br />
     <strong>Address </strong> <br />
		  <?=$row->address?> <br/>
		  <?=$row->city?> <?=$row->region?>, <?=$row->postalcode?>
    </td>
	<?php if($row->coname == "" || $row->coaddress != ""):?>
  	<td>
  	     <img src="<?php if($row->copics != "") { echo $row->copics;} else { echo base_url('images') . "/no_image.jpg";}?>"> <br />
   
   		<strong>Co-Signer Name</strong> <?=$row->coname?><br />
     <strong>Address </strong> <br />
		  <?=$row->coaddress?> <br/>
		  <?=$row->cocity?> <?=$row->coregion?>, <?=$row->copostalcode?>
  	</td>
          
         
  
  	<?php endif;?>
    </tr>
    </table>
  
    <div class="clearfix"></div>
<?php endforeach;?>

		    
		    <?php if($clientLoan):?>
		    <?php 
	            		$ngo_currency = $clientLoan->currency;
		            	$loan_terms = $clientLoan->loanterms;
		            	$loan_type = $clientLoan->loantermstype;
		            	$loandate = $clientLoan->loandate;	
		            	$loaninterest = $clientLoan->loaninterest;
		            	$totalrequested = $clientLoan->ngoamount;
		            	$rate= $clientLoan->rate;
		            	$projectID = $clientLoan->id;
		            	
		            	$total = $this->Ngo_model->get_total_funded($projectID);
	            		?>

	
			
          <strong>Loan Name </strong><?=$clientLoan->title?><br />
          <strong>Posted Date</strong> <?=date('d M y' , strtotime($clientLoan->postdate))?> <br />
		  <strong>Requested for funds Exchange Rate</strong> $ 1 USD -> <?=$rate?> <?=$ngo_currency?><br />
		  <strong>Amount requested</strong> <?=money_format('%(!#10n',$totalrequested)?> <?=$ngo_currency?> <br />
		  <br />
		  <strong>Exchange Rate of transfer</strong> $ 1 USD -> <?=$newrate?> <?=$ngo_currency?><br />
		  <strong>Amount Provided</strong> <?=money_format('%(!#10n',$newngoamount)?> <?=$ngo_currency?> <br />
		  <strong>Amount Diffrence</strong> <?=money_format('%(!#10n',$newngoamount-$totalrequested)?> <?=$ngo_currency?> <br />
		 
		  
						
			    <?php endif;?>
		