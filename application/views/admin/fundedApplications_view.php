<?php $this->load->view('/admin/sidebar_view');?>
   
        <h1>Funded Application</h1>
        	
            <table class="table table-hover">
	            <thead>
	            <tr>
	            	<td>Project</td>
	            	<td>NGO</td>
	            	
	            	
	            	<td>Country</td>
	            	<td>Amount</td>
	            	<td>Status</td>
	            </tr>
	            </thead>
	            <tbody>
	            
	            	<?php if($query = $this->Admin_model->get_fundedApplications()):?>
	            		<?php foreach($query as $row):?>
	            		
	            		
	            		<tr>
	            		
	            			<td><?=$row->title?></td>
	            				<td><?=$row->organization?></td>
	            			<td><?=$row->country?></td>
	            			<td>$ <?=money_format('%(!#10n', $row->amount)?></td>
	            			<td>
	            				<?php if(!$row->php_fund_status):?>
	            					<a class="btn btn-success" href="/admin/depositToNGO/<?=$row->id?>">Deposit To NGO</a>
	            				<?php else:?>
	            				<a class="btn btn-success" disabled="disabled"><?=$row->php_fund_status?> </a>
	            				<?php endif;?>
	            			</td>
	            		
	            		</tr>
	            		<?php endforeach;?>
	            	<?php else:?>
	            		<tr>
	            			<td>There is no funded applications</td>
	            		</tr>
	            	<?php endif;?>
	            </tbody>
    </table>
    

      
      <script>

</script>

<?php $this->load->view('/dashboard/footer_view');?>