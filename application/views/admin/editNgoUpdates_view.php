<?php $this->load->view('/admin/sidebar_view');?>
	<a class="btn btn-danger pull-right removeBtn" href="<?=site_url("admin/removeUpdate/$updateID")?>"> <i class="icon-remove"></i> Delete</a>
  <h2>Edit NGO Update</h2>
  <form id="form"method="post" action="<?=site_url("admin/ngoEditUpdatesProcess")?>">
  <?php $query = $this->Ngo_model->get_ngo_update($updateID);  ?>
  <input type="hidden" name="updateID" value="<?=$query->id?>" />
  <legend>Date</legend>
  <input type="text" class="datepicker" name="date" value=" <?=date("d-m-Y", strtotime($query->date))?>" />
  <legend>Update</legend>
		
		
		<div data-target="#editor" data-role="editor-toolbar" class="btn-toolbar">
      <div class="btn-group">
        <a title="" data-toggle="dropdown" class="btn dropdown-toggle" data-original-title="Font"><i class="icon-font"></i><b class="caret"></b></a>
          <ul class="dropdown-menu">
          <li><a style="font-family:'Serif'" data-edit="fontName Serif">Serif</a></li><li><a style="font-family:'Sans'" data-edit="fontName Sans">Sans</a></li><li><a style="font-family:'Arial'" data-edit="fontName Arial">Arial</a></li><li><a style="font-family:'Arial Black'" data-edit="fontName Arial Black">Arial Black</a></li><li><a style="font-family:'Courier'" data-edit="fontName Courier">Courier</a></li><li><a style="font-family:'Courier New'" data-edit="fontName Courier New">Courier New</a></li><li><a style="font-family:'Comic Sans MS'" data-edit="fontName Comic Sans MS">Comic Sans MS</a></li><li><a style="font-family:'Helvetica'" data-edit="fontName Helvetica">Helvetica</a></li><li><a style="font-family:'Impact'" data-edit="fontName Impact">Impact</a></li><li><a style="font-family:'Lucida Grande'" data-edit="fontName Lucida Grande">Lucida Grande</a></li><li><a style="font-family:'Lucida Sans'" data-edit="fontName Lucida Sans">Lucida Sans</a></li><li><a style="font-family:'Tahoma'" data-edit="fontName Tahoma">Tahoma</a></li><li><a style="font-family:'Times'" data-edit="fontName Times">Times</a></li><li><a style="font-family:'Times New Roman'" data-edit="fontName Times New Roman">Times New Roman</a></li><li><a style="font-family:'Verdana'" data-edit="fontName Verdana">Verdana</a></li></ul>
        </div>
      <div class="btn-group">
        <a title="" data-toggle="dropdown" class="btn dropdown-toggle" data-original-title="Font Size"><i class="icon-text-height"></i>&nbsp;<b class="caret"></b></a>
          <ul class="dropdown-menu">
          <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
          <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
          <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
          </ul>
      </div>
      <div class="btn-group">
        <a title="" data-edit="bold" class="btn" data-original-title="Bold (Ctrl/Cmd+B)"><i class="icon-bold"></i></a>
        <a title="" data-edit="italic" class="btn" data-original-title="Italic (Ctrl/Cmd+I)"><i class="icon-italic"></i></a>
      </div>
   
      <div class="btn-group">
        <a title="" data-edit="justifyleft" class="btn" data-original-title="Align Left (Ctrl/Cmd+L)"><i class="icon-align-left"></i></a>
        <a title="" data-edit="justifycenter" class="btn" data-original-title="Center (Ctrl/Cmd+E)"><i class="icon-align-center"></i></a>
        <a title="" data-edit="justifyright" class="btn" data-original-title="Align Right (Ctrl/Cmd+R)"><i class="icon-align-right"></i></a>
        <a title="" data-edit="justifyfull" class="btn" data-original-title="Justify (Ctrl/Cmd+J)"><i class="icon-align-justify"></i></a>
      </div>
      </div>
      
    

    <div id="editor" name="description">
      <?=$query->notes?>
    </div>
    
    <textarea id="update" style="display:none" name="notes"></textarea>
    <a class="btn subbtn" href="" > Submit</a>

  </form>         
      <script>


$(function(){
	
	
	$(".subbtn").click(function(event) {
		event.preventDefault();
 var projectdesc = $('#editor').html();
 
 		$("#update").val(projectdesc);
 		$("#form").submit();
    });
    
    $('.removeBtn').click(function(event) { 
	    var note=confirm("Are you sure that you want to delete this");

		if(!note){
			event.preventDefault();
		}

   
    });
});
</script>

<?php $this->load->view('/dashboard/footer_view');?>