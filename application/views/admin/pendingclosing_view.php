<?php $this->load->view('/admin/sidebar_view');?>
   
      
            <table class="table table-hover">
	            <thead>
	            <tr>
	            	<td>PHP ID</td>
	            	<td>Name</td>
	            	<td>NGO</td>
	            	<td>Repayment Amount/Total</td>
	            	<td>Approve Application</td>
	            </tr>
	            </thead>
	            <tbody>
	            
	            	<?php if ($query = $this->Admin_model->get_pending_closed_projects()):?>
	            		<?php foreach($query as $row):?>
	            		
	            		
	            		<tr>
	            			<td><?=$row->phpid?></td>
	            			<td><?=$row->title?></td>
	            			<td><?=$row->organization?></td>
	            		  <?php if($repaymenttotal = $this->Admin_model->total_repayment_amount($row->projectid)):?>
	            		  <?php $totalowe = $row->amount*(($row->loaninterest/100)+1);?>
							<td>$<?=money_format('%(#10n', $repaymenttotal)?>/$<?=money_format('%(#10n', $totalowe)?>USD <br />
							  <?php $percenttotal = floor(($repaymenttotal/$totalowe)*100);?>
							  (<?=$percenttotal?>% Returned)</td>
						  <?php endif;?>

	            			
	            			<td><a class="btn-success btn" href="/admin/approveClosure/<?=$row->projectid?>" > Approve</a>
	            				<a class="btn-danger btn" href="/admin/declineClosure/<?=$row->projectid?>" > Decline</a>
	            			</td>
	            		</tr>
	            		<?php endforeach;?>
	            		<?php else:?>
	            		<tr>
	            			<td colspan="5"> No Pending Payments</td>
	            		</tr>
	            		<?php endif;?>
	            </tbody>
    </table>
    
   
      
      <script>

</script>

<?php $this->load->view('/dashboard/footer_view');?>