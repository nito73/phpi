<?php $this->load->view('/admin/sidebar_view');?>
   

        <h1><?php $org = $this->Admin_model->getOrgByID($ngoid);
	       echo $org['organization'];
        ?> Applications</h1>
    
            <table class="table table-hover">
	            <thead>
	            <tr>
	            	<td>PHP ID</td>
	            	<td>Name</td>
	            	<td>Type</td>
	            	<td>NGO</td>
	            	<td>Amount Funded /Amount Requested</td>
	            	<td>View More</td>
	            </tr>
	            </thead>
	            <tbody>
	            
	            	<?php $query = $this->Admin_model->get_all_NGOs_projects($ngoid);?>
	            	<?php if($query):?>
	            		<?php foreach($query as $row):?>
	            		
	            		
	            		<tr>
	            			<td><?=$row->phpid?></td>
	            			<td><?=$row->title?></td>
	            			<td><?=$row->type?></td>
	            			<td><?=$row->organization?></td>
	            			<td>$<?php if ($funded = $this->Admin_model->get_total_funded($row->projectid)) echo $funded; else echo '0';?> / $<?=$row->amount?></td>
	            			<td><a class="btn-success btn" href="/admin/project/<?=$row->projectid?>" > View Application</a></td>
	            		</tr>
	            		<?php endforeach;?>
	            	<?php else:?>
	            		<tr>
	            			<td colspan="6"> NGO has not submitted any Applications yet</td>
	            		</tr>
	            	<?php endif;?>
	            </tbody>
    </table>
    
      <h1>Transaction</h1>
    
            <table class="table table-hover">
	            <thead>
	            <tr>
	            	<td>Date</td>
	            	<td>User</td>
	            	
	            	
	            	<td>Application</td>
	            	<td>Special NGO Amount</td>
	            	<td>Order</td>
	            </tr>
	            </thead>
	            <tbody>
	            
	            	<?php $query = $this->Admin_model->get_specialNGO_transaction($ngoid);
	            		foreach($query as $row):?>
	            		
	            		
	            		<tr>
	            			<td><?=date('M d,y', strtotime($row->date));?></td>
	            			
	            			<td><?=$row->firstname?> <?=$row->lastname?></td>
	            			
	            			<td>
		            			<?php if($query = $this->Admin_model->get_order_items($row->orderid)):?>
					        		<ul>
					        		<?php foreach($query as $rowproject):?>
					        			<li><?=$rowproject->title?> </li>
					        		<?php endforeach;?>
					        		</ul>
					        	<?php endif;?>
		            			
	            			</td>
	            			<td>$<?=$row->specialngoamount?></td>
	            			<td><a class="btn btn-success" href="/admin/viewOrder/<?=$row->orderid?>">View Order</a></td>
	            		
	            		</tr>
	            		<?php endforeach;?>
	            </tbody>
    </table>
    
         
      <script>

</script>

<?php $this->load->view('/dashboard/footer_view');?>