<div class="container-fluid" style="padding:15px;">
	<div class="row-fluid">
		<div class="span2">
			<ul class="nav nav-list">
				<li class="nav-header">PHP Admin</li>
				<li <?php if($page == "Admin") echo 'class="active"';?>><a href="/admin">Home</a></li>
				<li <?php if($page == "CMS") echo 'class="active"';?>><a href="/admin/CMS">CMS</a></li>
				<li <?php if($page == "Projects") echo 'class="active"';?>><a href="/admin/projects">Applications</a></li>
				<li <?php if($page == "Funded Applications") echo 'class="active"';?>><a href="/admin/fundedApplications">Funded Applications</a></li>
				<li  <?php if($page == "NGOs") echo 'class="active"';?>><a href="/admin/NGOs">NGOs</a></li>
				<li  <?php if($page == "Special NGOs") echo 'class="active"';?>><a href="/admin/Special_NGOs">Special NGOs</a></li>
				<li <?php if($page == "Donors") echo 'class="active"';?>><a href="/admin/donors/">Donors</a></li>
				<li <?php if($page == "Pending_Payment") echo 'class="active"';?> ><a href="/admin/pendingpayment">Pending Payments</a></li>
				<li <?php if($page == "Pending_Closing") echo 'class="active"';?> ><a href="/admin/pendingClosing">Pending Closing</a></li>
				<li <?php if($page == "Pending_Users") echo 'class="active"';?> ><a href="/admin/pendingusers">Pending Users</a></li>
				<li <?php if($page == "Transaction") echo 'class="active"';?> ><a href="/admin/transaction">PHP Funding</a></li>
				<li><a href="#">Reports</a></li>
			</ul>
		</div>
		<div class="span10">