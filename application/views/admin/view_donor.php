<?php $this->load->view('/admin/sidebar_view');?>
   <?php if($msg = $this->session->flashdata('message')):?>
   	    <div class="alert alert-<?=$msg['type']?>" style="margin-top: 10px">
   	    	<a class="close" data-dismiss="alert" href="#">&times;</a>
   		    <h4 class="alert-heading"><?=$msg['body']?></h4>
   	    </div>
   	    <?php endif;?>
   	  
   <a href="#addMoreSeed" class="addSeed pull-right btn btn-success"><i class="icon-plus"></i> Add More Seed</a>
    
       <?php $query = $this->Admin_model->view_donors($donorid);
	            		foreach($query as $row):?>
  <div class="row">
  
    <div>
          <h2>Name <?=$row->firstname?> <?=$row->lastname?></21>
          <h4></h4>
         
          
                   <dl class="dl-horizontal">
		  <dt>Email</dt>
		  <dd><?=$row->email?></dd>
		  
		  <dt>City</dt>
		  <dd><?=$row->city?></dd>
		  
				  
		  
  		</dl>
          
  </div>
  </div>
<?php endforeach;?>
	<h3>Orders</h3>
            <table class="table table-hover">
	            <thead>
	            <tr>
	            	
	            	<td>Order ID</td>
	            	<td>Date</td>

	            	<td>Total</td>
	            	<td>Payment Method</td>
	            	<td>Payment Status</td>

	            	<td>View Transaction</td>
	            </tr>
	            </thead>
	            <tbody>
	            
	            	<?php $query = $this->Admin_model->view_orders_by_userID($donorid);?>
	            		<?php if($query):?>
	            		<?php foreach($query as $row):?>
	            		
	            		
	            		<tr>
	            			<td><?=$row->id?></td>
	            		
	            			<td><?=date('m/d/y', strtotime($row->timestamp))?></td>
	            			<td><?php 
	            				$total = $row->total+$row->seed;
	            				
	            					echo '$' . $total . " USD";
	            				?></td>
	            			<td><?=$row->method?></td>
	            			<td><?=$row->status?></td>
	            		<td><a class="btn-success btn" href="/admin/viewOrder/<?=$row->id?>" > View Transaction</a></td>
	            		</tr>
	            		<?php endforeach;?>
	            	<?php else:?>
	            		<tr>
	            			<td colspan="6">User has not made any donations yet</td>
	            		</tr>
	            	<?php endif;?>
	            </tbody>
    </table>
    
    
    <h3>Seed Account History</h3>
            <table class="table table-hover">
	            <thead>
	            <tr>
	            	
	            	<td>Date</td>
	            	
	            	<td>Reason</td>
	            	<td>Amount</td>
	            	<td>View Application / Transaction</td>
	            </tr>
	            </thead>
	            <tbody>
	            
	            	<?php $query = $this->Admin_model->view_seed_account($donorid);
	            	$seedtotal = "";
	            	
	            	if($query):?>
	            	<?php foreach($query as $row):?>
	            		<?php $seedtotal = $seedtotal + $row->amount;?>
	            		
	            		<tr>
	            		<td><?=date('m/d/y', strtotime($row->timestamp))?></td>
	            			<td><?=$row->reason?> </td>
	            		
	            			<td>$<?=$row->amount?> USD</td>
	            			
	            		<td>
	            			<?php if($row->projectid != 0 && $row->orderid != 0) :?>
	            				<?php if($row->projectid == 0) :?>
		            				<a class="btn-success btn" href="/admin/project/<?=$row->projectid?>" > View Application</a>
		            			<?php else: ?>
		            			<a class="btn-success btn" href="/admin/viewOrder/<?=$row->orderid?>" > View Transaction</a>
			            		<?php endif;?>
			            		<?php else:?>
			            		PHP Admin Added
		            		<?php endif;?>
	            			
	           		</td>
	            		</tr>
	            		<?php endforeach;?>
	            		
	            		<tr>
	            		<td colspan="3" style="text-align:right;">User Seed Account Currently $<?=$seedtotal?> USD</td>
	            		<td></td>
	            		</tr>
	            	<?php else:?>
	            		<tr>
	            			<td colspan="4">User does not have a seed account</td>
	            		</tr>
	            	<?php endif;?>
	            </tbody>
    </table>



    
    <div class="modal fade hide addMoreSeed">
  <div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <h3>Add Funds to Donor Seed Account</h3>
  </div>
  <div class="modal-body">
  <form id="addDonorSeed"method="post" action="<?=base_url('admin/addDonorSeedProcess/') ?>" >
    <legend>Amount</legend> 
    <input type="text" name="amount" value="" placeholder="Numerical Amount" />
    <?php echo form_error('amount'); ?>
    <legend>Reason</legend>
    <textarea style="width:500px; height:150px;" name="reason" placeholder="Reason for adding seed">
    <?php echo form_error('reason'); ?>
    </textarea>
    <input type="hidden" name="donorID" value="<?=$donorid?>" />
  </form>
  </div>
  <div class="modal-footer">
    <a href="#" class="btn" data-dismiss="modal">Close</a>
    <a href="#" class="addMoreSeedBtn btn btn-success" rel=""><i class="icon-plus"></i> Seed</a>
  </div>
</div>



      
      <script>
	      $(function(){
		      $(".addSeed").click(function(event){
			      event.preventDefault();
			      $('.addMoreSeed').modal("show");
			      
		      });
		      
		      $(".addMoreSeedBtn").click(function(event){
			      event.preventDefault();
			      $('#addDonorSeed').submit();
			      
		      });
		      
	      });
</script>

<?php $this->load->view('/dashboard/footer_view');?>