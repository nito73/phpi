<?php 
	$ngo_currency = "";
	$clientLoan = $this->Client_model->view_client_loan($clientID);
	?>
<style type="text/css">
	.total td{
		font-weight: bold;
	}
	img{ height: 200px; float: left;}
	table tr td{border: 1px solid black; }
	thead tr td{border:2px solid black; font-weight: bold; background-color: yellow; }
	
	table{ border-collapse: collapse; }	
	.client tr td{border: none;}
	.client img{ height: 150px;}
</style>
<?php $query = $this->Admin_model->viewClient($clientID);

    foreach($query as $row):?>
    <table class="client">
    <tr style="padding-top:10px;">
    	<td>
   <img src="<?php if($row->pics != "") { echo $row->pics;} else { echo base_url('images') . "/no_image.jpg";}?>">
   <br />
   
  
     <strong>Collection Location</strong> <?=$row->collectionlocation?> <br />
     <strong>Name</strong> <?=$row->name?><br />
     <strong>Address </strong> <br />
		  <?=$row->address?> <br/>
		  <?=$row->city?> <?=$row->region?>, <?=$row->postalcode?>
    </td>
	<?php if($row->coname == "" || $row->coaddress != ""):?>
  	<td>
  	     <img src="<?php if($row->copics != "") { echo $row->copics;} else { echo base_url('images') . "/no_image.jpg";}?>"> <br />
   
   		<strong>Co-Signer Name</strong> <?=$row->coname?><br />
     <strong>Address </strong> <br />
		  <?=$row->coaddress?> <br/>
		  <?=$row->cocity?> <?=$row->coregion?>, <?=$row->copostalcode?>
  	</td>
          
         
  
  	<?php endif;?>
    </tr>
    </table>
  
    <div class="clearfix"></div>
<?php endforeach;?>

		    
		    <?php if($clientLoan):?>
		    <?php 
	            		$ngo_currency = $clientLoan->currency;
		            	$loan_terms = $clientLoan->loanterms;
		            	$loan_type = $clientLoan->loantermstype;
		            	$loandate = $clientLoan->loandate;
						printf($loandate);
		            	$loaninterest = $clientLoan->loaninterest;
		            	$totalrequested = $clientLoan->amount;
		            	$rate= $clientLoan->rate;
		            	$projectID = $clientLoan->id;
		            	
		            	$total = $this->Ngo_model->get_total_funded($projectID);
	            		?>
	            		
	            <?php
		    		if($loaninterest){
		    			$loaninterest = ($loaninterest/100)+1;
		    			
		    			$ngoprincipal = ($totalrequested*$rate)/$loan_terms;
			    		$principal = ($totalrequested)/$loan_terms;
			    		
		    			
		    			$totalrequested = $totalrequested * $loaninterest;
		    		
		    			$justinterest = $loaninterest -1;	
		    		} 
			    		$ngopayments = ($totalrequested*$rate)/$loan_terms;
			    		$payments = ($totalrequested)/$loan_terms;
			    		
			    		$ngointerest = $ngoprincipal * $justinterest;
			    		$interest = $principal * $justinterest;
			    		
			    		?>

	            		
	
			
             <strong>Loan Name </strong><?=$clientLoan->title?><br />
          <strong>Location</strong> <?=$clientLoan->location?> <br />
          <strong>Catergoy</strong> <?=$clientLoan->category?><br />
		  <strong>Posted Date</strong> <?=date('d M y' , strtotime($clientLoan->postdate))?> <br />
		  <strong>Exchange Rate</strong> $ 1 USD -> <?=$rate?> <?=$ngo_currency?><br />
		  
		  <?php if($repaymenttotal = $this->Ngo_model->total_repayment_amount($projectID)):?>
		  	<strong> Loan </strong> <br />
		  	 $<?=money_format('%(#10n', $clientLoan->amount)?> USD  / <?=money_format('%(#10n', $clientLoan->amount*$rate)?> <?=$ngo_currency?>  (Principal)<br />
		  	 $<?=money_format('%(#10n', $clientLoan->amount*($loaninterest/100))?> USD  / <?=money_format('%(#10n', ($clientLoan->amount*$rate)*($loaninterest/100))?> <?=$ngo_currency?> (Interest <?=$loaninterest?>%) <br />		
		  	   <strong>Total Due</strong>
		  <br />
		  <?php $percenttotal = floor(($repaymenttotal['phptotal']/$clientLoan->amount)*100);?>

			  $<?=money_format('%(#10n', $repaymenttotal['phptotal'])?> USD / $<?=money_format('%(#10n', $clientLoan->amount*(($loaninterest/100)+1))?> USD (<?=$percenttotal?>%)<br />
			  <?=money_format('%(#10n', $repaymenttotal['ngototal'])?> <?=$ngo_currency?> / <?=money_format('%(#10n', ($clientLoan->amount*$rate)*(($loaninterest/100)+1))?> <?=$ngo_currency?> (<?=$percenttotal?>%)
			 <br />
		  <?php else:?>
			  <?php if ($clientLoan->fundingsource == "PHP"):?>
			  <?php $percenttotal = floor(($total/$clientLoan->amount)*100);?>
				  <strong>Amount</strong> $<?=$total?>/$<?=$clientLoan->amount?> USD  (<?=$percenttotal?>% Funded) <br />
	          <?php else:?>
	          	  <strong>Repayment Amount</strong>
			  <br />
			  $<?=money_format('%(#10n', $repaymenttotal['phptotal'])?> USD / $<?=$clientLoan->amount?> USD <br />
			  <?=money_format('%(#10n', $repaymenttotal['ngototal'])?> <?=$ngo_currency?> / <?=$clientLoan->amount*$rate?> <?=$ngo_currency?>
			  <?php $percenttotal = floor(($repaymenttotal['phptotal']/$clientLoan->amount)*100);?>
		


	          <?php endif;?>
	          
		   
		  <?php endif;?>
		  
		  
  		
    <?php
	            		
	            		$loanBalance = array(0,0);
	            		$totalpaid = array(0,0);
	            		
	            	?>
	            	
	            	<?php $paymentCount = 0;?>	
		    	<?php for($x=0; $x < $loan_terms; $x++):?>
		    	
		    	<?php $datediff = $x * $loan_type; // Need error catch for non-int return
			    	$paymentCount++;
		    	?>
		    	<?php if (strtotime("today ") <= strtotime($loandate . ' +' . $datediff . ' Weeks' )){
		    			if($x != 0){
		    			$lastdatediff = ($x-1)* $loan_terms;
		    			$lastpaymentdate = strtotime($loandate . ' +' . $lastdatediff . ' Weeks' );
		    			} else {
			    			$lastpaymentdate = 	strtotime($loandate . ' +' . $datediff . ' Weeks' );
		    			}
		    			
		    			
		    			$totalamountdue = money_format('%(#10n', $payments);
		    			$totalamountdue = money_format('%(#10n', $ngopayments);
		    			
		    			$paymentduedate = strtotime($loandate . ' +' . $datediff . ' Weeks' );
		    			
		    			break;
		    			
		    		}
		    	?>
		    	
		    	
		   
		    	<?php endfor;?>
			  <br />
		    			    	

		    <strong>Last Payment Status</strong>
		     <strong>Payment Due Date</strong> <?=date("m/d/y",$paymentduedate)?><br />
	        <strong>Minimum Payment Due </strong> <?=money_format('%(#10n', $ngopayments) . " " . $ngo_currency;?>  /  $<?=money_format('%(#10n', $payments)?> USD <br />
	        
	        <?php 
	        	$minMustPaid = $paymentCount * $ngopayments ;
	        	$late_balance = $minMustPaid - $loanBalance[0];
	        	
	        if($late_balance <= $ngopayments ):?>
	        
	        <strong>Amount Due</strong>
	        <?php if(($ngopayments-$totalpaid[0]) < 0):?>
	        0.00 <?=$ngo_currency?> / $0.00 USD
	        <?php else :?>
	        	
	         <?=money_format('%(#10n', $ngopayments-$totalpaid[0]) . " " . $ngo_currency;?>  /  $<?=money_format('%(#10n', $payments-$totalpaid[1])?> USD
	         
	         <?php endif;?> <br />
	       
	      
	        <?php else:?>
	        <strong>Past Balance </strong><?=money_format('%(#10n', $late_balance-$ngopayments) . " " . $ngo_currency;?>  /  $<?=money_format('%(#10n', ($late_balance/$rate)-$payments)?> USD<br />
	        
	         <strong>Amount Due</strong><?=money_format('%(#10n', $late_balance) . " " . $ngo_currency;?>  /  $<?=money_format('%(#10n', $late_balance/$rate)?> USD
	       <?php endif;?>
	       <br />
	       
		    <table style="margin-top:20px;">
	            <thead>
	            <tr>
	            	<td>Date</td>
	            	<td><?=$ngo_currency?> Amount</td>
	            	<td>USD Amount</td>
	            	<td>Staff</td>
	            </tr>
	            </thead>
	            <tbody>
	            
			  <?php if($repaymentquery = $this->Ngo_model->view_repayment($clientLoan->id)):?>

	            	
	            		<?php foreach($repaymentquery as $row):?>
	            		
	            		<?php if($row->paymenttype == "Loan"):?>
	            		
	            		
	            		<tr>
	            			<td><?=date('d M y', strtotime($row->date))?></td>
	            			<td><?=money_format('%(#10n', $row->ngoamount) . " " . $ngo_currency;?> </td>
	            			<td>$<?=money_format('%(#10n', $row->amount)?> USD	</td>
	            				<td><?=$this->Ngo_model->get_user_name($row->userid);?> </td>
	            				            			
	            		</tr>
	            		<?php
		            		$loanBalance[0]+= $row->ngoamount;
		            		$loanBalance[1]+= $row->amount;

	            		if ($paymentduedate == $lastpaymentdate ){
		            		if(strtotime($row->date) >= $paymentduedate){
			            		
			            		$totalpaid[0]+= $row->ngoamount;
			            		$totalpaid[1]+= $row->amount;
		            		}
		            		} else {
			            		if(strtotime($row->date) >= $paymentduedate && strtotime($row->date) <= $lastpaymentdate ){
			            		
			            		$totalpaid[0]+= $row->ngoamount;
			            		$totalpaid[1]+= $row->amount;
			            		}	
		            		}
	            		?>
	            		<?php endif;?>
	            		<?php endforeach;?>
	            	
	            			
	            		<?php else:?>
	            		<tr>
	            			<td colspan="3">No Payments Made </td>
	            		</tr>
	            		<?php endif;?>
	            		

	            </tbody>
	        </table>
	        
	        <strong>Loan Balance</strong><?=money_format('%(#10n', (($totalrequested*$rate) *(($loaninterest/100)+1))-$loanBalance[0]) . " " . $ngo_currency;?>  / $<?=money_format('%(#10n', (($totalrequested)*(($loaninterest/100)+1))-$loanBalance[1])?> USD	<br />
	        
<br />
<br />
	       	        
	           	   
				
			    <?php endif;?>
			    <strong>Savings Account</strong><br />
		    
		    
		    
		    <?php if($repaymentquery = $this->Client_model->client_savings_account($clientID)):?>
		    
		    <table class="table table-hover">
	            <thead>
	            <tr>
	            	<td>Date</td>
	            	<td>Type</td>
	            	<td><?=$ngo_currency?> Amount</td>
	            	<td>Staff</td>
	            	
	            </tr>
	            </thead>
	            <tbody>
	            	<?php
	            		$savingTotal = array(0);
	            		
	            	?>
	            		<?php foreach($repaymentquery as $row):?>
	            			
		            	
	            		
	            		<tr>
	            			<td><?=date('d M y', strtotime($row->date))?></td>
	            			<td><?php if($row->amount > 0):?>Deposit<?php else:?> Withdraw<?php endif;?></td>
	            			<td><?=money_format('%(#10n', $row->amount) . " " . $ngo_currency;?> </td>
	            			<td><?=$this->Ngo_model->get_user_name($row->userid);?> </td>
	            			
	            		</tr>
	            		
	            		<?php 
	            			$savingTotal[0]+= $row->amount;
		            	 
	            		?>
	            		<?php endforeach;?>
	            		<tr class="total">
	            			<td colspan="2"style="text-align:right;">Saving Total</td>
	            			<td colspan="2"><?=money_format('%(#10n', $savingTotal[0]) . " " . $ngo_currency;?> </td>
	            				            			
	            		</tr>
	            			            

	            </tbody>
	        </table>
	   

		    <?php endif;?>
		    
		