<?php $this->load->view('/admin/sidebar_view');?>
   
      
            <table class="table table-hover">
	            <thead>
	            <tr>
	            	<td>Order ID</td>
	            	<td>Date Submitted</td>
	            	<td>Name</td>
	            	<td>Amount</td>
	            	<td>Status</td>
	            </tr>
	            </thead>
	            <tbody>
	            
	            	<?php if ($query = $this->Admin_model->get_pending_payments()):?>
	            		<?php foreach($query as $row):?>
	            		
	            		
	            		<tr>
	            			<td><?=$row->orderid?></td>
	            			<td><?=date('m/d/y', strtotime($row->timestamp))?></td>
	            			<td><?=$row->firstname?> <?=$row->lastname?></td>
	            			<td><?=$row->total?></td>
	            		
	            			<td><a class="btn-success btn" href="/admin/approvePayment/<?=$row->orderid?>" > Approve</a> <a class="btn-danger btn" href="/admin/declinePayment/<?=$row->orderid?>" > Decline</a></td>
	            		</tr>
	            		<?php endforeach;?>
	            		<?php else:?>
	            		<tr>
	            			<td colspan="5"> No Pending Payments</td>
	            		</tr>
	            		<?php endif;?>
	            </tbody>
    </table>
    
   
      
      <script>

</script>

<?php $this->load->view('/dashboard/footer_view');?>