<?php
/**********************************
	Edited: 03SEPT2015
	By: Billy
	Notes:
		Code Clean Up
		Error catching:
			-
			-
**********************************/
	$ngo_currency = "";
	$clientLoan = $this->Client_model->view_client_loan($clientID);
	//$paymentduedate='Initial Load';
?>
<style type="text/css">
	.total td{font-weight:bold;}
</style>
<?php
	$query = $this->Admin_model->viewClient($clientID);
	foreach($query as $row):
?>
		<div class="pull-right"style="padding:5px;">
			<a class="btn" href="<?=site_url('admin/printClient/' . $clientID);?>" target="_blank">Print Client Summery</a>
		</div>
		<?php if($clientLoan):?>
		<div class="pull-right"style="padding:5px;">
			<a class="btn" href="<?=site_url('/reports/repaymentReport/' . $clientLoan->id);?>" target="_blank">Print Client Loan Report</a>
		</div>
		<?php endif ;?>
		<div class="row" style="padding:10px;padding-top:40px;">
			<div class="row">
				<div class="span5">
					<div class="span3">
						<a href="#" class="thumbnail">
							<img src="<?php if($row->pics != "") { echo $row->pics;} else { echo base_url('images') . "/no_image.jpg";}?>">
						</a>
					</div>
					<div class="span8">
						<dl class="dl-horizontal">
							<dt>Collection Location</dt>
							<dd><?=$row->collectionlocation?></dd>
							<dd><?=$row->name?></dd>
							<dt>Address</dt>
							<dd><?=$row->address?> <br/>
								<?=$row->city?> <?=$row->region?>, <?=$row->postalcode?>
							</dd>
						</dl>
					</div>
				</div>
				<?php if($row->coname != "" && $row->coaddress != ""):?>
				<div class="span6">
					<div class="span3">
						<a href="#" class="thumbnail">
							<img src="<?php if($row->copics != "") { echo $row->copics;} else { echo base_url('images') . "/no_image.jpg";}?>">
						</a>
					</div>
					<div class="span8">
						<dl class="dl-horizontal">
							<dt>Co-Signer Name</dt>
							<dd><?=$row->coname?></dd>
							<dt>Address</dt>
							<dd><?=$row->coaddress?><br/>
							<?=$row->cocity?> <?=$row->coregion?>, <?=$row->copostalcode?></dd>
						</dl>
					</div>
				</div>
				<?php endif;?>
			</div>
			<div class="clearfix"></div>
			<?php 
				endforeach;
				if($msg = $this->session->flashdata('message')):
			?>
			<div class="alert alert-<?=$msg['type']?>" style="margin-top: 10px">
				<a class="close" data-dismiss="alert" href="#">&times;</a>
				<strong class="alert-heading"><?=$msg['body']?></strong>
			</div>
			<?php endif;?>
			<div class="tabbable" style="padding:25px;"> <!-- Only required for left/right tabs -->
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tab3" data-toggle="tab">Savings</a></li>
					<li><a href="#tab5" data-toggle="tab">Collateral Savings</a></li>
					<li><a href="#tab2" data-toggle="tab">Loans</a></li>
					<?php if($clientLoan):?>
					<li><a href="#tab4" data-toggle="tab">Payment Schedule</a></li>
					<?php endif;?>
				</ul>
				<div class="tab-content" style="padding:10px;">
					<div class="tab-pane" id="tab2">
					<?php if($clientLoan):?>
					<?php
						$typeisnil=false;
	            		$ngo_currency = $clientLoan->currency;
		            	$loan_terms = $clientLoan->loanterms;
						$loan_type = $clientLoan->loantermstype;
						// Check to make sure that the loan type is a integer
						if(is_numeric($loan_type)){$typeisnil=false;}else{$typeisnil=true;}
						$loandate = $clientLoan->loandate;
						$loaninterest = $clientLoan->loaninterest;
		            	$justloaninterest = $clientLoan->loaninterest;
		            	$totalrequested = $clientLoan->amount;
		            	$rate= $clientLoan->rate;
		            	$projectID = $clientLoan->id;
		            	$total = $this->Ngo_model->get_total_funded($projectID);
						$ngoprincipal = ($totalrequested*$rate)/$loan_terms;
			    		$principal = ($totalrequested)/$loan_terms;
						
						if($loaninterest){
							$loaninterest = ($loaninterest/100)+1;
							$totalrequested = $totalrequested * $loaninterest;
							$justinterest = $loaninterest -1;
						} else {
							$justinterest = 1;
						}
						// NGO Payments
			    		$ngopayments = ($totalrequested*$rate)/$loan_terms;
			    		$payments = ($totalrequested)/$loan_terms;
						// NGO Interest
						$ngointerest = $ngoprincipal * $justinterest;
			    		$interest = $principal * $justinterest;
					?>
					<div class="row" style="padding:5px;">
						<div class="span3">
							<a href="#" class="thumbnail">
								<img src="<?php if($clientLoan->pics != "") { echo $clientLoan->pics;} else { echo base_url('images') . "/no_image.jpg";}?>">
							</a>
						</div>
						<div class="span8">
							<h1><?=$clientLoan->title?></h1>
							<h4><?=$clientLoan->location?> | <?=$clientLoan->category?></h4>
							<dl class="dl-horizontal">
								<dt>Posted Date</dt>
								<dd><?=date('d M y' , strtotime($clientLoan->postdate))?></dd>
								<dt>Exchange Rate</dt>
								<dd>$ 1 USD -> <?=$rate?> <?=$ngo_currency?></dd>
								<?php if($repaymenttotal = $this->Ngo_model->total_repayment_amount($projectID)):?>
								<dt>Loan </dt>
								<dd>
									$<?=money_format('%(#10n', $clientLoan->amount)?> USD  / <?=money_format('%(#10n', $clientLoan->ngoamount)?> <?=$ngo_currency?>  (Principal)<br />
									$<?=money_format('%(#10n', $clientLoan->amount*($loaninterest))?> USD  / <?=money_format('%(#10n', ($clientLoan->amount*$rate)*($loaninterest))?> <?=$ngo_currency?> (Interest <?=$justloaninterest?>%) 
								</dd>
								<dt>Total Due</dt>
								<dd>
									<?php $percenttotal = floor(($repaymenttotal['phptotal']/($clientLoan->amount*($loaninterest)))*100);?>
									$<?=money_format('%(#10n', $repaymenttotal['phptotal'])?> USD / $<?=money_format('%(#10n', $clientLoan->amount*($loaninterest))?> USD (<?=$percenttotal?>%)<br />
									<?=money_format('%(#10n', $repaymenttotal['ngototal'])?> <?=$ngo_currency?> / <?=money_format('%(#10n', ($clientLoan->amount*$rate)*($loaninterest))?> <?=$ngo_currency?> (<?=$percenttotal?>%)
									<div class="progress">
										<div class="bar" style="width: <?=$percenttotal?>%;"></div>
										<label><?=$percenttotal?>% Returned</label>
									</div>
								</dd>
						</div>
								<?php else:?>
								<?php if ($clientLoan->fundingsource == "PHP"):?>
								<?php $percenttotal = floor(($total/$clientLoan->amount)*100);?>
								<dt>Amount</dt>
								<dd>$<?=$total?>/$<?=$clientLoan->amount?> USD
								<div class="progress">
									<div class="bar" style="width: <?=$percenttotal?>%;"></div>
									<label><?=$percenttotal?>% Funded</label>
								</div>
								</div>
								</dd>
	          <?php else:?>
	          	  <dt>Repayment Amount</dt>
			  <dd>$<?=money_format('%(#10n', $repaymenttotal['phptotal'])?> USD / $<?=$clientLoan->amount?> USD <br />
			  <?=money_format('%(#10n', $repaymenttotal['ngototal'])?> <?=$ngo_currency?> / <?=$clientLoan->amount*$rate?> <?=$ngo_currency?>
			  <?php $percenttotal = floor(($repaymenttotal['phptotal']/$clientLoan->amount)*100);?>
		


			   <div class="progress">
       
				<div class="bar" style="width: 0%;"></div>
				   <label>0% Returned</label>
				</div>
            </div>
	          <?php endif;?>
	          
		   
		  <?php endif;?>
		  
		  
  		</dl>
          
         
  </div>
    <?php
	            		
	            		$loanBalance = array(0,0);
	            		$totalpaid = array(0,0);
	            		
	            	?>
	            	
	            	<?php ?>	
		    	<?php ?>
		    	
		    	<?php
					if(!$typeisnil){ // If $loan_type returns a non-int, push to error catching
						$paymentCount = 0;
						for($x=0; $x < $loan_terms; $x++):
							$datediff = $x * $loan_type; //Need to isolate NULL returns for loan_type
							$paymentCount++;
							if (strtotime("today ") <= strtotime($loandate . ' +' . $datediff . ' Weeks')){
								if($x != 0){
									$lastdatediff = ($x-1)* $loan_terms;
									$lastpaymentdate = strtotime($loandate . ' +' . $lastdatediff . ' Weeks');
								} else {
									$lastpaymentdate = 	strtotime($loandate . ' +' . $datediff . ' Weeks');
								}
							break;
							} else { // Error catch for undefined variable
								$lastpaymentdate='Invalid Date Specified';
							}
							$totalamountdue = money_format('%(#10n', $payments);
							$totalamountdue = money_format('%(#10n', $ngopayments);
							$paymentduedate = strtotime($loandate . ' +' . $datediff . ' Weeks');
						endfor;
					} else { // Isolate error(s) from non-int variable
						$paymentduedate='Invalid Date Specified';
					}
				?>
			  
		    			    	

		    <strong> Loan Payment</strong><br />
		    <table class="table table-hover">
	            <thead>
	            <tr>
	            	<td>Date</td>
	            	<td><?=$ngo_currency?> Amount</td>
	            	<td>USD Amount</td>
	            	<td>Staff</td>
	            </tr>
	            </thead>
	            <tbody>
	            
			  <?php if($repaymentquery = $this->Ngo_model->view_repayment($clientLoan->id)):?>

	            	
	            		<?php foreach($repaymentquery as $row):?>
	            		
	            		<?php if($row->paymenttype == "Loan" && !$typeisnil):?>
	            		
	            		
	            		<tr>
	            			<td><?=date('d M y', strtotime($row->date))?></td>
	            			<td><?=money_format('%(#10n', $row->ngoamount) . " " . $ngo_currency;?> </td>
	            			<td>$<?=money_format('%(#10n', $row->amount)?> USD	</td>
	            				<td><?=$this->Ngo_model->get_user_name($row->userid);?> </td>
	            				            			
	            		</tr>
	            		<?php
		            		$loanBalance[0]+= $row->ngoamount;
		            		$loanBalance[1]+= $row->amount;
							
							if ($paymentduedate == $lastpaymentdate ){
								if(strtotime($row->date) >= $paymentduedate){
									$totalpaid[0]+= $row->ngoamount;
									$totalpaid[1]+= $row->amount;
								}
							} else {
								if(strtotime($row->date) >= $paymentduedate && strtotime($row->date) <= $lastpaymentdate ){
									$totalpaid[0]+= $row->ngoamount;
									$totalpaid[1]+= $row->amount;
								}	
							}
						?>
	            		<?php endif;?>
	            		<?php endforeach;?>
	            	
	            			
	            		<?php else:?>
	            		<tr>
	            			<td colspan="3">No Payments Made </td>
	            		</tr>
	            		<?php endif;?>
	            		

	            </tbody>
	        </table>
	        
	        <strong>Loan Balance</strong><?=money_format('%(#10n', (($totalrequested*$rate) *(($loaninterest/100)+1))-$loanBalance[0]) . " " . $ngo_currency;?>  / $<?=money_format('%(#10n', (($totalrequested)*(($loaninterest/100)+1))-$loanBalance[1])?> USD	<br />
	        
<br />
<br />
	        <strong>Payment Due Date</strong> <?=!$typeisnil ? date("m/d/y",$paymentduedate) : $paymentduedate; // Converted to if/else for error catching ?><br />
	        <strong>Minimum Payment Due </strong> <?=money_format('%(#10n', $ngopayments) . " " . $ngo_currency;?>  /  $<?=money_format('%(#10n', $payments)?> USD <br />
	        
	        <?php 
	        	$minMustPaid = $paymentCount * $ngopayments ;
	        	$late_balance = $minMustPaid - $loanBalance[0];
	        	
	        if($late_balance <= $ngopayments ):?>
	        
	        <strong>Amount Due</strong>
	        <?php if(($ngopayments-$totalpaid[0]) < 0):?>
	        0.00 <?=$ngo_currency?> / $0.00 USD
	        <?php else :?>
	        	
	         <?=money_format('%(#10n', $ngopayments-$totalpaid[0]) . " " . $ngo_currency;?>  /  $<?=money_format('%(#10n', $payments-$totalpaid[1])?> USD
	         
	         <?php endif;?> <br />
	       
	      
	        <?php else:?>
	        <strong>Past Balance </strong><?=money_format('%(#10n', $late_balance-$ngopayments) . " " . $ngo_currency;?>  /  $<?=money_format('%(#10n', ($late_balance/$rate)-$payments)?> USD<br />
	        
	         <strong>Amount Due</strong><?=money_format('%(#10n', $late_balance) . " " . $ngo_currency;?>  /  $<?=money_format('%(#10n', $late_balance/$rate)?> USD
	       <?php endif;?>
	        
	            <div class="well">
				    <p>By entering a payment,  Please enter monetary value in <?=$ngo_currency?></p>
				    <?php 
				    	$projectStatus = $this->Ngo_model->get_project_status($projectID);
				    if($projectStatus != 'Pending Closing' && $projectStatus != 'Closed' ):?>
				    <form method="post" action="<?=site_url('ngo/repaymentProcess')?>">
					    <label>Payment Date </label><input type="text" name="date" class="datepicker" value="<?=date('d-m-Y')?>"/>
						<input type="hidden" class="input-xxlarge" name="paymenttype" id="paymenttype" value="Loan" />
						<label>Amount <?=$ngo_currency?></label><input type="text" name="amount" />
					    <input type="hidden" name="projectid" value="<?=$projectID?>" />
					    <input type="hidden" name="clientid" value="<?=$clientID?>" />
					    <input type="hidden" name="rate" value="<?=$rate?>" />
					    <input class="btn btn-success"type="submit" value="Send" />
					</form>
				    
				    <?php else:?>
				    <h2>This Loan is <?=$projectStatus?></h2>
				    <?php endif;?>
				    <a href="<?=site_url('ngo/closeProject/' . $projectID)?>" class="btn btn-warning" > Close Loan</a>
			    </div>
		    
	   
				<?php else:?>
				<?php $ngo_currency = $organization->currency;?>
				<a class="btn btn-primary btn-large" href="<?=site_url('clients/addLoan/' . $clientID)?>" > Add Loan </a>
                	<p style="padding-top:20px;"><ul>
    <li>
    <span style="font-size: 13px;"><span style="font-weight: bold;">Food Industry Business Sector</span></span>- Businesses that produce or sell packaged manufactured food out of a store such as Restaurants, Bakery, Ice Cream, etc.</li>
    <li>
    <span style="font-size: 13px;"><span style="font-weight: bold;">Agriculture Business Sector</span></span>- Businesses that grow crops, raise animals, etc. off the land</li>
    <li>
    <span style="font-size: 13px;"><span style="font-weight: bold;">Service Industry Business Sector</span></span> - Businesses that provide services such as repairs, hair dressers, communication centers, etc</li>
    <li>
    <span style="font-size: 13px;"><span style="font-weight: bold;">Small Trade Industry Business Sector</span></span> - Any type of product one a singular to one basis such as Beans, T-shirts, Yams, Jewelry, etc.</li>
    <li>
    <span style="font-size: 13px;"><span style="font-weight: bold;">Manufacturing Business Sector</span></span>- These businesses are defined as capable of producing a product such as cement block makers, seamstress, furniture, etc.</li>
</ul>
</p> 

<br />

<a class="btn btn-primary btn-large" href="<?=site_url('clients/addNonLoan/' . $clientID)?>" > Add Non-PHP Loan </a>
                	<p style="padding-top:20px;">
                	
               
                		Do you have a Loan that is funded by another organization that you want our system to track payments.
                	
</p> 

			    <?php endif;?>
			
		    </div>
		    
		    <div class="tab-pane active" id="tab3">
		    Savings<br />
		    
		    
		    
		    <?php if($repaymentquery = $this->Client_model->client_savings_account($clientID)):?>
		    
		    <table class="table table-hover">
	            <thead>
	            <tr>
	            	<td>Date</td>
	            	<td>Type</td>
	            	<td><?=$ngo_currency?> Amount</td>
	            	<td>Staff</td>
	            	
	            </tr>
	            </thead>
	            <tbody>
	            	<?php
	            		$savingTotal = array(0);
	            		
	            	?>
	            		<?php foreach($repaymentquery as $row):?>
	            			
		            	
	            		
	            		<tr>
	            			<td><?=date('d M y', strtotime($row->date))?></td>
	            			<td><?php if($row->amount > 0):?>Deposit<?php else:?> Withdraw<?php endif;?></td>
	            			<td><?=money_format('%(#10n', $row->amount) . " " . $ngo_currency;?> </td>
	            			<td><?=$this->Ngo_model->get_user_name($row->userid);?> </td>
	            			
	            		</tr>
	            		
	            		<?php 
	            			$savingTotal[0]+= $row->amount;
		            	 
	            		?>
	            		<?php endforeach;?>
	            		<tr class="total">
	            			<td colspan="2"style="text-align:right;">Saving Total</td>
	            			<td colspan="2"><?=money_format('%(#10n', $savingTotal[0]) . " " . $ngo_currency;?> </td>
	            				            			
	            		</tr>
	            			            

	            </tbody>
	        </table>
	   

		    <?php endif;?>
		    <div class="well">
			    <form method="post" action="<?=site_url('clients/savingProcess')?>">
				    <label>Transaction Date </label><input type="text" name="date" class="datepicker"/>
					<input type="hidden" class="input-xxlarge" name="clientID" id="clientID" value="<?=$clientID?>" />
					<input type="hidden" class="input-xxlarge" name="currency" id="currency" value="<?=$ngo_currency?>" />
					
					<label>Amount <?=$ngo_currency?></label><input type="text" name="amount" />
				    
				    <input class="btn btn-success"type="submit" value="Send" />
				</form>
	
		    </div>		
		    </div>
  <div class="tab-pane" id="tab5">
		   Collateral Account<br />
		    This account is only for Deposit to establish credit to apply for a loan
		    
		    
		    <?php if($repaymentquery = $this->Client_model->client_collateral_account($clientID)):?>
		    
		    <table class="table table-hover">
	            <thead>
	            <tr>
	            	<td>Date</td>
	            	<td>Type</td>
	            	<td><?=$ngo_currency?> Amount</td>
	            	<td>Staff</td>
	            	
	            </tr>
	            </thead>
	            <tbody>
	            	<?php
	            		$savingTotal = array(0);
	            		
	            	?>
	            		<?php foreach($repaymentquery as $row):?>
	            			
		            	
	            		
	            		<tr>
	            			<td><?=date('d M y', strtotime($row->date))?></td>
	            			<td><?php if($row->amount > 0):?>Deposit<?php else:?> Withdraw<?php endif;?></td>
	            			<td><?=money_format('%(#10n', $row->amount) . " " . $ngo_currency;?> </td>
	            			<td><?=$this->Ngo_model->get_user_name($row->userid);?> </td>
	            			
	            		</tr>
	            		
	            		<?php 
	            			$savingTotal[0]+= $row->amount;
		            	 
	            		?>
	            		<?php endforeach;?>
	            		<tr class="total">
	            			<td colspan="2"style="text-align:right;">Saving Total</td>
	            			<td colspan="2"><?=money_format('%(#10n', $savingTotal[0]) . " " . $ngo_currency;?> </td>
	            				            			
	            		</tr>
	            			            

	            </tbody>
	        </table>
	   

		    <?php endif;?>
		    <div class="well">
			    <form method="post" action="<?=site_url('clients/collateralProcess')?>">
				    <label>Transaction Date </label><input type="text" name="date" class="datepicker"/>
					<input type="hidden" class="input-xxlarge" name="clientID" id="clientID" value="<?=$clientID?>" />
					<input type="hidden" class="input-xxlarge" name="currency" id="currency" value="<?=$ngo_currency?>" />
					
					<label>Amount <?=$ngo_currency?></label><input type="text" name="amount" />
				    
				    <input class="btn btn-success"type="submit" value="Send" />
				</form>
	
		    </div>		
		    </div>

			<?php if($clientLoan = $this->Client_model->view_client_loan($clientID)):?>
			 <div class="tab-pane" id="tab4">
					    Payment Schedule<br />
		    <table class="table table-hover">
		    <thead>
		    	<tr>
		    		<td>Date</td>
		    		<td>Principal</td>
					<td>Interest (<?=$justinterest*100?>%)</td>
		    		<td>Payment</td>
		    	</tr>
		    </thead>
		    <tbody>	
		    	
		    	<?php for($x=0; $x < $loan_terms; $x++):?>
		    	<?PHP $datediff = $x * $loan_type;?>
		    	<tr>
		    		<td><?=date("m/d/y", strtotime($loandate . ' +' . $datediff . ' Weeks' ));?></td>
					<td><?=money_format('%(#10n', $ngoprincipal) . " " . $ngo_currency;?> <br />
						$ <?=money_format('%(#10n', $principal);?> USD</td>
			    		<td><?=money_format('%(#10n', $ngointerest) . " " . $ngo_currency;?> <br />
						$ <?=money_format('%(#10n', $interest);?> USD</td>
			    		
			    		<td><?=money_format('%(#10n', $ngopayments) . " " . $ngo_currency;?> <br /> $<?=money_format('%(#10n', $payments)?> USD</td>
		    	</tr>
		    	<?php endfor;?>
		    	</tbody>
		    </table>
		    
		    
		   
		    </div>
		    <?php endif;?>

	    </div>
    </div>
    
    
                
      
      <script>

</script>

<?php $this->load->view('/dashboard/footer_view');?>