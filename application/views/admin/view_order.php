<?php $this->load->view('/admin/sidebar_view');?>
   
  
    
    <?php $query = $this->Admin_model->viewOrder($orderid);
    foreach($query as $row):?>
  <div class="row">
  
    <div>
          <h1>Order #ID <?=$row->id?></h1>
          <h4>Total $<?=$row->total?>USD | <?=$row->method?> | <?=$row->status?> </h4>
         
          
                   <dl class="dl-horizontal">
		  <dt>Donor Name</dt>
		  <dd><?php $userid = $this->Admin_model->getDonor($row->userid)?>
		  		<?=$userid['name']?></dd>
		  <dt>Posted Date</dt>
		  <dd><?=date('m/d/y' , strtotime($row->timestamp))?></dd>
		  
		  <dt>Total Amount</dt>
		  <dd>$<?=$row->total+$row->seed?> USD</dd>
		 
		 <?php if($row->seed != 0):?>
		  <dt>Total Used from Seed Account</dt>
		  <dd>$<?=$row->seed?> USD</dd> 
		  <?php endif;?>
		  
		  
  		</dl>
          
  </div>
  </div>
    <hr>
<?php endforeach;?>
            <table class="table table-hover">
	            <thead>
	            <tr>
	            	<td>Application</td>
	            	<td>Amount</td>
	    
	            	<td>View Application</td>
	            	
	            </tr>
	            </thead>
	            <tbody>
	            
	            	<?php if($query = $this->Admin_model->get_order_items($orderid)):?>
	            		<?php foreach($query as $row):?>
	            		
	            		
	            		<tr>
	            			
	            			<td><?=$row->title?></td>
	            			<td><?=$row->oiamount?></td>
	            			<td><a class="btn-success btn" href="/admin/project/<?=$row->projectid?>"> View Application </a></td>
	            			
	            		</tr>
	            		<?php endforeach;?>
	            		<?php else:?>
	            		<tr>
	            			<td colspan="5"> No Payments Made to Application</td>
	            		</tr>
	            		<?php endif;?>
	            </tbody>
    </table>
    
      </div>
      
      <script>

</script>

<?php $this->load->view('/dashboard/footer_view');?>