<?php $this->load->view('/admin/sidebar_view');?>
   
       <!-- Jumbotron -->

    
    <?php foreach($query as $row):?>
<?php $percenttotal = floor(($total/$row->amount)*100);?>
  <div class="row">
    <div class="span3">
    <a href="#" class="thumbnail">
      <img src="<?php if($row->pics != "") { echo $row->pics;} else { echo base_url('images') . "/no_image.jpg";}?>">
    </a>
    </div>
    <div class="span6">
          <h1><?=$row->title?></h1>
          <h4><?=$row->location?> | <?=$row->category?></h4>
         
          
                   <dl class="dl-horizontal">
		  <dt>Name</dt>
		  <dd><?=$row->name?></dd>
		  <dt>Posted Date</dt>
		  <dd><?=date('m/d/y' , strtotime($row->postdate))?></dd>
		  
		   
		  <?php if($repaymenttotal = $this->Admin_model->total_repayment_amount($projectid)):?>
			  <dt>Repayment Amount</dt>
			  <dd>$<?=$repaymenttotal?>/$<?=$row->amount?> USD
			  <?php $percenttotal = floor(($repaymenttotal/$row->amount)*100);?>
		


			   <div class="progress">
       
				<div class="bar" style="width: <?=$percenttotal?>%;"></div>
				   <label><?=$percenttotal?>% Returned</label>
				</div>
            </div>
            
			  </dd>
		  <?php else:?>
			  <dt>Amount</dt>
			  <dd>Amount Requsted $<?=$row->amount?> <br /> Amount Funded: $<?=$total?>
		  <div class="progress">
       
				<div class="bar" style="width: <?=$percenttotal?>%;"></div>
				   <label><?=$percenttotal?>% Funded</label>
				</div>
            </div>
            </dd>
		  <?php endif;?>
		  
		  
  		</dl>
          
           </div>
    <hr>
<?php endforeach;?>
            <table class="table table-hover">
	            <thead>
	            <tr>
	            	<td>Date</td>
	            	<td>User</td>
	            	<td>Amount</td>
	            	<td>View Payment</td>
	            	
	            </tr>
	            </thead>
	            <tbody>
	            
	            	<?php if($query = $this->Admin_model->get_funding($projectid)):?>
	            		<?php foreach($query as $row):?>
	            		
	            		
	            		<tr>
	            			<td><?=date('m/d/y', strtotime($row->timestamp))?></td>
	            			<td><?=$row->firstname?> <?=$row->lastname?></td>
	            			<td>$<?=$row->amount?> USD	</td>
	            			<td><a class="btn btn-success" href="/admin/viewOrder/<?=$row->orderid?>">View Order</a></td>
	            			
	            		</tr>
	            		<?php endforeach;?>
	            		<?php else:?>
	            		<tr>
	            			<td colspan="5"> No Payments Made to Application</td>
	            		</tr>
	            		<?php endif;?>
	            </tbody>
    </table>
    
      
      <script>

</script>

<?php $this->load->view('/dashboard/footer_view');?>