<?php
$this->load->view('/admin/sidebar_view');
?>
<table class="table table-hover dataTables">
	<thead>
		<tr>
			<td>Content ID</td>
			<td>Title Name</td>
			<td>Content Overview</td>
			<td>Last Edited</td>
			<td>Last Edited By</td>
			<td>View More</td>
		</tr>
	</thead>
	<tbody>
	<?php
		$query = $this->Admin_model->get_all_cms();
		if($query){ // If query successful, let's go with it
			foreach($query as $row):
	?>
		<tr>
			<td><?=$row->cms_id?></td>
			<td><?=$row->cms_title?></td>
			<td><?=$this->Admin_model->limit_the_output($row->content)?></td>
			<td><?=$this->Admin_model->relativeTime($row->edit_date)?></td>
			<td><?=$row->username?></td>
			<td>
				<a class="btn-danger btn" href="/admin/edit_cms/<?=$row->cms_id?>" > Edit Content</a>
			</td>
		</tr>
	<?php
			endforeach;
		} else { // If query returns false because there's no fucking rows....
			echo '<tr><td>There was a database error. Please contact your server administrator.</td></tr>';
		}
		// This is how you properly error catch.. Anticipate disaster, so you can be prepared. ~Billy
	?>
	</tbody>
</table>
<script>
	$(function(){
		$('.dataTables').dataTable({
			"sDom": "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
			"bJQueryUI": true,
			"sPaginationType": "bootstrap",
			"bLengthChange": false
		});
		$.extend( $.fn.dataTableExt.oStdClasses, {
			"sWrapper": "dataTables_wrapper form-inline"
		} );
	});
</script>
<?php
$this->load->view('/dashboard/footer_view');
?>