<?php $this->load->view('/admin/sidebar_view');?>
   
        <h1>Donors</h1>
        	<ul class="nav nav-pills">
			  <li <?php if($day == 'All'):?> class="active" <?php endif;?> ><a href="<?=site_url('admin/transaction/')?>">View All</a> </li>
			  <li <?php if($day == '30'):?> class="active" <?php endif;?> ><a href="<?=site_url('admin/transaction/30')?>">30 Days</a></li>
			  <li <?php if($day == '60'):?> class="active" <?php endif;?> ><a href="<?=site_url('admin/transaction/60')?>">60 Days</a></li>
			  <li <?php if($day == '90'):?> class="active" <?php endif;?> ><a href="<?=site_url('admin/transaction/90')?>">90 Days</a></li>
			</ul>
            <table class="table table-hover">
	            <thead>
	            <tr>
	            	<td>Date</td>
	            	<td>User</td>
	            	
	            	
	            	<td>Application</td>
	            	<td>Amount</td>
	            	<td>Order</td>
	            </tr>
	            </thead>
	            <tbody>
	            
	            	<?php $query = $this->Admin_model->get_all_transaction($day);
	            		foreach($query as $row):?>
	            		
	            		
	            		<tr>
	            			<td><?=date('M d,y', strtotime($row->date));?></td>
	            			
	            			<td><?=$row->firstname?> <?=$row->lastname?></td>
	            			
	            			<td>
		            			<?php if($query = $this->Admin_model->get_order_items($row->orderid)):?>
					        		<ul>
					        		<?php foreach($query as $rowproject):?>
					        			<li><?=$rowproject->title?> </li>
					        		<?php endforeach;?>
					        		</ul>
					        	<?php endif;?>
		            			
	            			</td>
	            			<td>$<?=$row->amount?> <?php if($row->specialngo == 'Yes'):?> (Special NGO) <?php endif;?></td>
	            			<td><a class="btn btn-success" href="/admin/viewOrder/<?=$row->orderid?>">View Order</a></td>
	            		
	            		</tr>
	            		<?php endforeach;?>
	            </tbody>
    </table>
    

      
      <script>

</script>

<?php $this->load->view('/dashboard/footer_view');?>