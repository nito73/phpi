<?php $this->load->view('/admin/sidebar_view');?>
   
        <h1>Donors</h1>
    
            <table class="table table-hover">
	            <thead>
	            <tr>
	            	
	            	<td>Name</td>
	            	
	            	<td>Email</td>
	            	<td>Address</td>
	            	<td>View Account</td>
	            </tr>
	            </thead>
	            <tbody>
	            
	            	<?php $query = $this->Admin_model->get_all_donors();
	            		foreach($query as $row):?>
	            		
	            		
	            		<tr>
	            			<td><?=$row->firstname?> <?=$row->lastname?></td>
	            		
	            			<td><?=$row->email?></td>
	            			<td><?=$row->city?></td>
	            		<td><a class="btn-success btn" href="/admin/viewdonor/<?=$row->user_id?>" > View Account</a></td>
	            		</tr>
	            		<?php endforeach;?>
	            </tbody>
    </table>
    

      
      <script>

</script>

<?php $this->load->view('/dashboard/footer_view');?>