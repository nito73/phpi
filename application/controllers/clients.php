<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clients extends CI_Controller {
	
	function __construct()
    {
    
    	parent::__construct();
    	
    	$this->load->helper(array('form', 'url'));
		$this->load->library('security');
		$this->load->library('tank_auth');
		$this->load->model('Ngo_model');
		$this->lang->load('tank_auth');
		//$this->output->enable_profiler(TRUE);
			$this->load->model('Dashboard_model');
	$this->load->model('Client_model');
		
    	if($this->tank_auth->get_usertype() != "NGO" && $this->tank_auth->get_usertype() != "sNGO" && $this->tank_auth->get_usertype() != "loan_officer" ){
	    	redirect('/dashboard/');	
	    	
    	}
        
    }
    
	public function index()
	{
		$data['page'] = "Clients";
		$data['organization'] = $this->tank_auth->get_org();
		$data['location'] = "All";

		$this->load->view('header',$data);
		
		$data['organization'] = $this->Ngo_model->get_org_userID($this->tank_auth->get_user_id());
		$this->load->view('clients/index', $data);	
		$this->load->view('footer');
	}
	public function add()
	{
		$this->load->model('Ngo_model');
		$data['page'] = "Add Clients";
		$data['organization'] = $this->tank_auth->get_org();


		$this->load->view('header',$data);
		
		$data['organization'] = $this->Ngo_model->get_org_userID($this->tank_auth->get_user_id());
		$this->load->view('clients/add_client', $data);	
		$this->load->view('footer');
	}
	
	public function addProcess(){
	
		$data['organization'] = $this->tank_auth->get_org();
		$data = array(
			'organization_id' => $data['organization']->organization_id,
			'name' => $_POST['name'],
			'gov_id' => $_POST['gov_id'],
			'birthdate'  => ($_POST['birthday']) ? date("Y-m-d", strtotime($_POST['birthday'])) :'' ,
			'birthplace'  => $_POST['birthplace'],
			'martial_status'  => $_POST['martial_status'],
			'gender'  => (isset($_POST['gender'])) ? $_POST['gender'] : '',
			'job'  => $_POST['job'],
			'address' => $_POST['address'],
			'city' => $_POST['city'],
			'region' => $_POST['region'],
			'postalcode' => $_POST['postalcode'],
			'collectionlocation' => $_POST['collection_location'],
			'cell' => $_POST['cell'],
			'pics' => $_POST['pic'],
			'currency' => $_POST['currency'],
			'coname' => $_POST['coname'],
			'coaddress' => $_POST['coaddress'],
			'cocity' => $_POST['cocity'],
			'coregion' => $_POST['coregion'],
			'copostalcode' => $_POST['copostalcode'],
			'cocell' => $_POST['cocell'],
			'copics' => $_POST['copic'],
			'cogov_id'  => $_POST['cogov_id'],
			'cobirthdate'  => ($_POST['cobirthday']) ? date("Y-m-d", strtotime($_POST['cobirthday'])) :'' ,
			'cobirthplace' => $_POST['cobirthplace'],
			'comartial_status'  => $_POST['comartial_status'],
			'cogender' => (isset($_POST['cogender'])) ? $_POST['cogender'] : '',
			'cojob'  => $_POST['cojob'],
			'status' => 'Complete'
			);
			
		$this->db->insert('clients', $data);
		$id = $this->db->insert_id();
		if(isset($_POST['saving']) && $_POST['saving'] > 0 ){
			$rate = $this->Client_model->get_exchange_rate($_POST['currency']);
			$savingamount = $_POST['saving']/$rate;
		
			$savingsdata = array(
				'client_id' => $id,	
				'amount' => $_POST['saving'],
				'date' => date('Y-m-d'),
				'currency' => $_POST['currency'],
				'type' => 'Deposit',
				'userid' => $this->tank_auth->get_user_id()
			);
			
			$this->db->insert('savings', $savingsdata);
		}
		
		redirect('/clients/viewClient/' . $id . '/loan/', "refresh");
	}
	
	public function editProcess(){
		$data['organization'] = $this->tank_auth->get_org();
		$id = $_POST['clientID'];
		
		
	$data = array(
			'name' => $_POST['name'],
			'gov_id' => $_POST['gov_id'],
			'birthdate'  => ($_POST['birthday']) ? date("Y-m-d", strtotime($_POST['birthday'])) :'' ,
			'birthplace'  => $_POST['birthplace'],
			'martial_status'  => $_POST['martial_status'],
			'gender'  => (isset($_POST['gender'])) ? $_POST['gender'] : '',
			'job'  => $_POST['job'],
			'address' => $_POST['address'],
			'city' => $_POST['city'],
			'region' => $_POST['region'],
			'postalcode' => $_POST['postalcode'],
			'cell' => $_POST['cell'],
			'pics' => $_POST['pic'],
			'coname' => $_POST['coname'],
			'coaddress' => $_POST['coaddress'],
			'cocity' => $_POST['cocity'],
			'coregion' => $_POST['coregion'],
			'copostalcode' => $_POST['copostalcode'],
			'cocell' => $_POST['cocell'],
			'copics' => $_POST['copic'],
			'cogov_id'  => $_POST['cogov_id'],
			'cobirthdate'  => ($_POST['cobirthday']) ? date("Y-m-d", strtotime($_POST['cobirthday'])) :'' ,
			'cobirthplace' => $_POST['cobirthplace'],
			'comartial_status'  => $_POST['comartial_status'],
			'cogender' => (isset($_POST['cogender'])) ? $_POST['cogender'] : '',
			'cojob'  => $_POST['cojob'],
			'status' => 'Complete'
			);
		
			
		$this->db->where('id', $id );
		$this->db->update('clients', $data);
		
	
		
		redirect('/clients/viewClient/' . $id . '/loan/', "refresh");
	}

	public function addLoan($clientID)
	{
		$this->load->model('Ngo_model');
		$data['page'] = "Add Project";

		$data['clientID'] = $clientID;
		$this->load->view('header',$data);
		
		$data['organization'] = $this->tank_auth->get_org();
		$this->load->view('clients/add_loan_client', $data);	
		
		$this->load->view('footer');
	}
	
	public function addNonLoan($clientID)
	{
		$this->load->model('Ngo_model');
		$data['page'] = "Add Project";

		$data['clientID'] = $clientID;
		$this->load->view('header',$data);
		
		$data['organization'] = $this->tank_auth->get_org();
		$this->load->view('clients/add_nonloan_client', $data);	
		
		$this->load->view('footer');
	}
	
	public function viewClient($clientID,$view_type = "savings")
	{
		$data['page'] = "Clients";
		$data['organization'] = $this->tank_auth->get_org();
	
		$data['clientID'] = $clientID;

		$this->load->view('header',$data);
		
		$data['organization'] = $this->Ngo_model->get_org_userID($this->tank_auth->get_user_id());
		
		$data['view_type'] = $view_type;

		$this->load->view('clients/viewClient_view', $data);	
		$this->load->view('clients/viewClient_' . $view_type , $data);	
		$this->load->view('footer');
	}
	
		public function editClient($clientID)
	{
		$data['page'] = "Clients";
		$data['organization'] = $this->tank_auth->get_org();
	
		$data['clientID'] = $clientID;

		$this->load->view('header',$data);
		
		$data['organization'] = $this->Ngo_model->get_org_userID($this->tank_auth->get_user_id());
		$this->load->view('clients/editClient_view', $data);	
		$this->load->view('footer');
	}
	
		public function removeClient($clientID)
	{
	
		if($this->tank_auth->get_usertype() != "loan_officer"){
			$this->db->where('id', $clientID);
			$this->db->update('clients', array('status' => 'inActive'));
			
			$this->db->where('client_id', $clientID);
			$this->db->delete('group_members');
			 $this->session->set_flashdata('message', array(
			 	'type' => 'success',
			 	'body' => 'The client is successfully removed'
			 ));
		} else {
			$this->session->set_flashdata('message', array(
			 	'type' => 'warning',
			 	'body' => 'You are not able to remove this client'
			 ));
			
		}
		
			redirect('/clients/', "refresh");
	
		
	}
	
	
			public function activateClient($clientID)
	{
	
		if($this->tank_auth->get_usertype() != "loan_officer"){
			$this->db->where('id', $clientID);
			$this->db->update('clients', array('status' => 'Complete'));
			
			 $this->session->set_flashdata('message', array(
			 	'type' => 'success',
			 	'body' => 'The client is successfully Activated'
			 ));
		} else {
			$this->session->set_flashdata('message', array(
			 	'type' => 'warning',
			 	'body' => 'You are not able to remove this client'
			 ));
			
		}
		
			redirect('/clients/', "refresh");
	
		
	}
	
	
	public function printClient($clientID)
	{
		$data['page'] = "Clients";
		$data['organization'] = $this->tank_auth->get_org();
	
		$data['clientID'] = $clientID;
		$data['organization'] = $this->Ngo_model->get_org_userID($this->tank_auth->get_user_id());
		$this->load->view('clients/printClient_view', $data);	
	}
	
		public function printSavingsTransaction($transID)
	{
		$data['page'] = "Clients";
		$data['transaction'] = $this->Client_model->client_savings_trans($transID);
	
		if ($data['transaction'] && $this->Client_model->client_check($data['transaction']->client_id)){
		$data['clientID'] = $data['transaction']->client_id;
		$data['organization'] = $this->tank_auth->get_org();
		
		$data['organization'] = $this->Ngo_model->get_org_userID($this->tank_auth->get_user_id());
		
		
		$this->load->view('clients/printSavingsTransaction_view', $data);	
		} else {
			echo "You are not Authorized to view this Page";
		}
	}
	
	public function printCollateralTransaction($transID)
	{
		$data['page'] = "Clients";
		$data['transaction'] = $this->Client_model->client_collateral_trans($transID);
	
		if ($data['transaction'] && $this->Client_model->client_check($data['transaction']->client_id)){
		$data['clientID'] = $data['transaction']->client_id;
		$data['organization'] = $this->tank_auth->get_org();
		
		$data['organization'] = $this->Ngo_model->get_org_userID($this->tank_auth->get_user_id());
		
		
		$this->load->view('clients/printCollateralTransaction_view', $data);	
		} else {
			echo "You are not Authorized to view this Page";
		}
	}
	
	
	public function printLoanTransaction($transID)
	{
		$data['page'] = "Clients";
		$data['transaction'] = $this->Client_model->client_loan_trans($transID);
	
		if ($data['transaction'] && $this->Client_model->client_check($data['transaction']->clientid)){
		$data['clientID'] = $data['transaction']->clientid;
		$data['organization'] = $this->tank_auth->get_org();
		
		$data['organization'] = $this->Ngo_model->get_org_userID($this->tank_auth->get_user_id());
		
		
		$this->load->view('clients/printLoanTransaction_view', $data);	
		} else {
			echo "You are not Authorized to view this Page";
		}
	}
	public function savingProcess(){
		
		if($_POST['amount'] && $_POST['date']){
			if((int)$_POST['amount'] > 1){
				
				$type = "Deposit";
			} else {
				$type = "Withdraw";
			}
		$data = array(
			'date' => date('Y-m-d', strtotime($_POST['date'])),
			'amount' => $_POST['amount'],
			'type' => $type,
			'client_id' => $_POST['clientID'],
			'currency' => $_POST['currency'],
			'userid' => $this->tank_auth->get_user_id()
		);
		$this->db->insert('savings', $data);
		} else {
			$this->session->set_flashdata('message', array(
		 	'type' => 'warning',
		 	'body' => 'Please enter a valid Date and Amount Deposit/Withdraw'
		 ));
		}
		redirect($_SERVER['HTTP_REFERER'], 'refresh');
	}
	
	public function collateralProcess(){
		 $amount = (int)$_POST['amount'];
		 
		 if ( $amount > 0) {

		$data = array(
			'date' => date('Y-m-d', strtotime($_POST['date'])),
			'amount' => $_POST['amount'],
			'client_id' => $_POST['clientID'],
			'currency' => $_POST['currency'],
			'userid' => $this->tank_auth->get_user_id()
		);
		$this->db->insert('collateral', $data);
		 $this->session->set_flashdata('message', array(
		 	'type' => 'success',
		 	'body' => 'Collateral Deposit is successful',
		 	'step' => 'Collateral'
		 ));
		} else {
		 $this->session->set_flashdata('message', array(
		 	'type' => 'warning',
		 	'body' => 'Client is not able to make a withdraw from this account'
		 ));						
		}
		
				redirect($_SERVER['HTTP_REFERER'], 'refresh');

	}
	
/* 	
 *Edit Loan for the NGO 
 */
 
 
 	public function editLoan($progectID)
	
	
	{
		$this->load->model('Give_model');
		if($this->tank_auth->get_usertype() == "NGO"){
			$data['page'] = "Edit Project";
			$data['projectID'] = $progectID;
			$this->load->view('header',$data);
			$this->load->view('clients/editloan_view', $data);
			$this->load->view('footer');
		
		} else {
		
		
	    	redirect('/dashboard/');	
	    	
    	}
	
	
	}
	
	public function updateProcess()
	{
	
			 
			 $this->session->set_flashdata('message', array(
			 	'type' => 'success',
			 	'body' => 'Loan Information Updated',
			 	'step' => 'Loan'
			 ));
			 
			 
	$projectdata = array(
		'title' => $_POST['title'],
		'name' => $_POST['name'],
		'type' => $_POST['type'],
		'use' => $_POST['use'],
		'description' => $_POST['description'],
		'pics' => $_POST['pic'],
		'video' => $_POST['video']
	);
		$this->db->where('id', $_POST['projectID']);
		$this->db->update('projects', $projectdata);
		
		
		echo json_encode(array('status' => 'success'));
		
	}
	
	   public function myClientDatatable()
        {
        
        	function clientstatus($id, $status){
	        	if($status == "NEW"){
		        	return '<a class="btn btn-warning" href="/clients/editClient/' . $id . '">Finish Client Application</a>';
	        	} else {
		        	return '<a class="btn btn-success" href="/clients/viewClient/' . $id . '">View Client</a>';
	        	}
        	}
 
                $this->load->library('datatables');
                $this->datatables->select('gov_id, name, address, collectionlocation, cell,currency, status,id ');
                $this->datatables->from('clients');
               
               
               if(isset($_POST['status']) && $_POST['status'] == "Active"){
                   $this->datatables->where("status !=", "inActive" );
                } else {
	                $this->datatables->where("status =", $_POST['status'] );
                }
                if(isset($_POST['city']) && $_POST['city'] != "All"){
	        		$city = $_POST['city'];
	        		if(isset($_POST['region']) && $_POST['region'] != "All"){
		        		$region = $_POST['region'];
		        		$this->datatables->where("organization_id =" . $this->tank_auth->get_orgid() . " AND region = '" . $region . "'" . " AND city = '" . $city . "'");
	        		} else {
		        		
		        	 $this->datatables->where("organization_id =" . $this->tank_auth->get_orgid() . " AND city = '" . $city . "'");	
	        		} 
	        		
	        	} else if(isset($_POST['region']) && $_POST['region'] != "All"){
		        		$region = $_POST['region'];
		        		$this->datatables->where("organization_id =" . $this->tank_auth->get_orgid() . " AND region = '" . $region . "'");
	        	}
	        	else {
		        	$this->datatables->where("organization_id =" . $this->tank_auth->get_orgid() );	
	        	}
	        	
	        	 $this->datatables->edit_column('currency', '$1', 'clientstatus(id, status)');
                
                echo $this->datatables->generate();
 
        }
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */