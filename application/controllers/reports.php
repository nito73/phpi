<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct()
    {
    
    	parent::__construct();
    	
    	$this->load->helper(array('form', 'url'));
		$this->load->library('security');
		$this->load->library('tank_auth');
		$this->load->model('Ngo_model');
		$this->load->model('Report_model');
		$this->load->model('Client_model');

			$this->lang->load('tank_auth');
		//$this->output->enable_profiler(TRUE);
		
    
        
    }
	
	public function quarter()
	{
		$data['fundingsource'] = $_POST['fundingsource'];
		$data['qtr'] = $_POST['qtr'];
		$data['page'] = "Quarter Report";
		
		$this->load->view('report/quarterReport_view', $data);
	}
	//old method
	public function dateProcess($type ="")
	{
		if($type == "daily"){
			$data['startdate'] = date("Y-m-d" , strtotime("today"));
			$data['enddate'] = date("Y-m-d" , strtotime("today"));
			$data['fundingsource'] = "ALL";
		} else {
			$data['startdate'] = date("Y-m-d" , strtotime($_POST['startdate']));
			$data['enddate'] = date("Y-m-d" , strtotime($_POST['enddate']));
			$data['fundingsource'] = $_POST['fundingsource'];
		}
		
		
		$data['page'] = "Date Report";
		$data['query'] = $this->Report_model->get_date_report($data['startdate'], $data['enddate'], $data['fundingsource']);
		$data['intial_query'] = $this->Report_model->get_intial_deposit($data['startdate'] . " 00:00:00", $data['enddate'] . " 23:59:00");
		$data['savingsquery'] = $this->Report_model->get_date_report_savings($data['startdate'], $data['enddate']);
	
		$data['closequery'] = $this->Report_model->get_closed_loan($data['startdate'], $data['enddate'], $data['fundingsource']);
		$data['YTDQuery'] = $this->Report_model->get_YTD_data(date("Y", strtotime($data['startdate'])));
		$this->load->view('report/dateReport_view', $data);
	}


	
	public function deposit(){
		$data['fundingsource'] = "All";
		$data['startdate'] = $_POST['startdate'];
		$data['enddate'] = $_POST['enddate'];
		
		$data['page'] = "Date Report";
		$data['query'] = $this->Report_model->get_date_report($data['startdate'], $data['enddate'], $data['fundingsource']);
	
		$data['savingsquery'] = $this->Report_model->get_date_report_savings($data['startdate'], $data['enddate']);
	
		$data['closequery'] = $this->Report_model->get_closed_loan($data['startdate'], $data['enddate'], $data['fundingsource']);
		$data['YTDQuery'] = $this->Report_model->get_YTD_data(date("Y", strtotime($data['startdate'])));
		
		$SQLdata = array(
			'org_id' => $this->tank_auth->get_orgid(),
			'currency'=> $_POST['currency'],
			'type'=> $_POST['type'],
			'php_amount'=> $_POST['phpamount']/$_POST['rate'],
			'ngo_amount'=> $_POST['ngoamount'],
			'difference'=> $_POST['ratedifferance'],
			'rate'=> $_POST['rate'],
			'data'=> $this->load->view('report/dateReport_html', $data, true)

		);
		
		$this->db->insert('ngo_reserve', $SQLdata);
		echo "Done";
	}


	//new method
		public function fullReport($type ="")
	{
		if($type == "daily"){
			$data['startdate'] = date("Y-m-d" , strtotime("today"));
			$data['enddate'] = date("Y-m-d" , strtotime("today"));
			$data['fundingsource'] = "ALL";
		} else {
			$data['startdate'] = date("Y-m-d" , strtotime("2014-01-02"));
			$data['enddate'] = date("Y-m-d" , strtotime("2014-07-31"));
			$data['fundingsource'] = 'All';
		}
		
		
		$data['page'] = "Date Report";
		$data['query'] = $this->Report_model->get_transaction_report($data['startdate'], $data['enddate']);
		$data['intial_query'] = $this->Report_model->get_intial_deposit($data['startdate'] . " 00:00:00", $data['enddate'] . " 23:59:00");
		$data['savingsquery'] = $this->Report_model->get_date_report_savings($data['startdate'], $data['enddate']);
	
		$data['closequery'] = $this->Report_model->get_closed_loan($data['startdate'], $data['enddate'], $data['fundingsource']);
		$data['YTDQuery'] = $this->Report_model->get_YTD_data(date("Y", strtotime($data['startdate'])));
		$this->load->view('report/transactionReport_view', $data);
	}


	
	public function depositReport(){
		$data['fundingsource'] = "All";
		$data['startdate'] = $_POST['startdate'];
		$data['enddate'] = $_POST['enddate'];
		
		$data['page'] = "Date Report";
		$data['query'] = $this->Report_model->get_transaction_report($data['startdate'], $data['enddate']);
	
		$data['savingsquery'] = $this->Report_model->get_date_report_savings($data['startdate'], $data['enddate']);
	
		$data['closequery'] = $this->Report_model->get_closed_loan($data['startdate'], $data['enddate'], $data['fundingsource']);
		$data['YTDQuery'] = $this->Report_model->get_YTD_data(date("Y", strtotime($data['startdate'])));
		
		$SQLdata = array(
			'org_id' => $this->tank_auth->get_orgid(),
			'currency'=> $_POST['currency'],
			'type'=> $_POST['type'],
			'php_amount'=> $_POST['phpamount']/$_POST['rate'],
			'ngo_amount'=> $_POST['ngoamount'],
			'difference'=> $_POST['ratedifferance'],
			'rate'=> $_POST['rate'],
			'data'=> $this->load->view('report/dateReport_html', $data, true)

		);
		
		$this->db->insert('ngo_reserve', $SQLdata);
		echo "Done";
	}
	
	

	//new method
		public function repaymentReport($projectID)
	{
		
		
		$data['page'] = "Date Report";
		$data['projectid'] = $projectID;
		$this->load->view('report/repaymentReport_view', $data);
	}

	//new loan process
	public function newLoan()
	{
		$data['title'] = 'hi';
		
		
		$this->load->view('report/newLoanReport_view', $data);
	}


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */