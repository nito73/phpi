<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{


	$data = array(
		'email' => $_POST['email'],
		'password' => sha1($_POST['password']),
		'usertype' => $_POST['usertype']
	);
	
	$this->db->insert('users', $data);
	
	$userid = $this->db->insert_id();

	$people = array(
		'userid' => $userid,
		'firstname' => $_POST['firstname'],
		'lastname' => $_POST['lastname']

	);
	
	$this->db->insert('people', $people);
	redirect('/', 'refresh');
	
	
	}
	
}
