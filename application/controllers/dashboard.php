<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('Dashboard_model');
		
		if ($this->tank_auth->is_logged_in()) {									// logged in
			$data['page'] = "Dashboard";
			$this->load->view('header',$data);
			 if($this->tank_auth->get_usertype() == "NGO" || $this->tank_auth->get_usertype() == "sNGO" || $this->tank_auth->get_usertype() == "loan_officer" ){
				 $data['location'] = "All";
				 $this->load->model('Ngo_model');
				 $this->load->view('dashboard/index_view_NGO', $data);
			 } else {
			 	$this->load->model('Give_model');
				$this->load->view('dashboard/index_view_user');	 
				 
			 }
			
			$this->load->view('footer');
			
		} else {
			
			$this->session->set_flashdata('warning', 'Please Login or Register to view this page');
				redirect('/auth/login', 'refresh');

		}
	}
	
	public function Ngo($location = "All")
	{
		//$this->output->enable_profiler(TRUE);
		$this->load->model('Dashboard_model');
		
		if ($this->tank_auth->is_logged_in()) {									// logged in
			$data['page'] = "Dashboard";
			$this->load->view('header',$data);
			 if($this->tank_auth->get_usertype() == "NGO" || $this->tank_auth->get_usertype() == "sNGO"){
				 $data['location'] = $location;
				 $this->load->model('Ngo_model');
				 $this->load->view('dashboard/index_view_NGO', $data);
			 } else {
				$this->session->set_flashdata('warning', 'Please Login or Register to view this page');
				redirect('/auth/login', 'refresh');
				 
			 }
			
			$this->load->view('footer');
			
		} else {
			
			$this->session->set_flashdata('warning', 'Please Login or Register to view this page');
				redirect('/auth/login', 'refresh');

		}
	}
	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */