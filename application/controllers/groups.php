<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Groups extends CI_Controller {
	
	function __construct()
    {
    
    	parent::__construct();
    	
    	$this->load->helper(array('form', 'url'));
		$this->load->library('security');
		$this->load->library('tank_auth');
		$this->load->model('Ngo_model');
		$this->lang->load('tank_auth');
		//$this->output->enable_profiler(TRUE);
			$this->load->model('Dashboard_model');
	$this->load->model('Client_model');
	$this->load->model('Group_model');
		
    	if($this->tank_auth->get_usertype() != "NGO" && $this->tank_auth->get_usertype() != "sNGO" && $this->tank_auth->get_usertype() != "loan_officer" ){
	    	redirect('/dashboard/');	
	    	
    	}
        
    }
    
	public function index()
	{
		$data['page'] = "Groups";
		$data['organization'] = $this->tank_auth->get_org();
		$data['location'] = "All";

		$this->load->view('header',$data);
		
		$data['organization'] = $this->Ngo_model->get_org_userID($this->tank_auth->get_user_id());
		$this->load->view('groups/index', $data);	
		$this->load->view('footer');
	}
	public function add()
	{
		$this->load->model('Ngo_model');
		$data['page'] = "Groups";
		$data['organization'] = $this->tank_auth->get_org();


		$this->load->view('header',$data);
		
		$data['organization'] = $this->Ngo_model->get_org_userID($this->tank_auth->get_user_id());
		$this->load->view('groups/add_group', $data);	
		$this->load->view('footer');
	}
	
	public function addProcess(){
		$data['organization'] = $this->tank_auth->get_org();
		$data = array(
			'organization_id' => $data['organization']->organization_id,
			'name' => $_POST['name'],
			'location' => $_POST['location'],
			'group_photo' => $_POST['pic'],
			);
			
		$this->db->insert('groups', $data);
		$id = $this->db->insert_id();
		
		redirect('/groups/viewGroup/' . $id, "refresh");
	}
	
		public function viewGroup($groupID)
	{
		$data['page'] = "Groups";
		$data['organization'] = $this->tank_auth->get_org();
	
		$data['groupID'] = $groupID;

		$this->load->view('header',$data);
		
		$data['organization'] = $this->Ngo_model->get_org_userID($this->tank_auth->get_user_id());
		$this->load->view('groups/viewGroup_view', $data);	
		$this->load->view('footer');
	}
	
	
		
	public function addMemberProcess($type ='JSON'){
		
		
		$SQL = "SELECT * FROM group_members WHERE group_id=? AND client_id=?";
		$query = $this->db->query($SQL, array($_POST['groupID'], $_POST['clientID']));
		
		if($query->num_rows() == 0){
			$groupmemberdata = array(
				'group_id' => $_POST['groupID'],
				'client_id' => $_POST['clientID'],
				'role' => $_POST['role']
			);
			
			$this->db->insert('group_members',  $groupmemberdata);
					if($type == "JSON"){
						echo json_encode(array('status' => 'success', 'error' => 'FALSE'));	 
					} else {
						$this->session->set_flashdata('message', array(
							 	'type' => 'Success',
							 	'body' => 'This person is added to the group'
							 ));
						redirect($_SERVER['HTTP_REFERER'], 'refresh');
					}
		} else{
					if($type == "JSON") {
						echo json_encode(array('status' => 'success', 'error' => 'TRUE'));	 
					} else {
					
							 $this->session->set_flashdata('message', array(
							 	'type' => 'warning',
							 	'body' => 'This person is already a member of the Group'
							 ));
						redirect($_SERVER['HTTP_REFERER'], 'refresh');
					}
		}
		
	}
	
		public function addNewMemberProcess(){
			
		$data['organization'] = $this->tank_auth->get_org();
		$data = array(
			'organization_id' => $data['organization']->organization_id,
			'name' => $_POST['clientName'],
			'cell' => $_POST['cell'],
			'status' => 'NEW'
		);
			
		$this->db->insert('clients', $data);
		$clientID = $this->db->insert_id();
		
		
			$groupmemberdata = array(
				'group_id' => $_POST['groupID'],
				'client_id' => $clientID,
				'role' => $_POST['role']
			);
			
			$this->db->insert('group_members',  $groupmemberdata);
					
				$this->session->set_flashdata('message', array(
					 	'type' => 'success',
					 	'body' => 'This person has been added to the group'
					 ));
				redirect($_SERVER['HTTP_REFERER'], 'refresh');
				
		
	}
	
	
	
	public function removeMember($memberID){
		$this->db->where('id', $memberID);
		$this->db->delete('group_members');
		echo json_encode(array('status' => 'success'));	 
		
		
	}

	
	
	public function printGroup($groupID)
	{
		$data['page'] = "Groups";
		$data['organization'] = $this->tank_auth->get_org();
	
		$data['groupID'] = $groupID;
		$data['organization'] = $this->Ngo_model->get_org_userID($this->tank_auth->get_user_id());
		$this->load->view('groups/printGroup_view', $data);	
	}
	
		public function printGroupAttendance($groupID)
	{
		$data['page'] = "Groups";
		$data['organization'] = $this->tank_auth->get_org();
	
		$data['groupID'] = $groupID;
		$data['organization'] = $this->Ngo_model->get_org_userID($this->tank_auth->get_user_id());
		$this->load->view('groups/printGroup_attendanceSheet', $data);	
	}
	
	
	
		public function addLoan($groupID)
	{
		$this->load->model('Ngo_model');
		$data['page'] = "Add Project";

		$data['groupID'] = $groupID;
		$this->load->view('header',$data);
		
		$data['organization'] = $this->tank_auth->get_org();
		$this->load->view('groups/add_loan_group', $data);	
		
		$this->load->view('footer');
	}
	
	public function addNonLoan($clientID)
	{
		$this->load->model('Ngo_model');
		$data['page'] = "Add Project";

		$data['clientID'] = $clientID;
		$this->load->view('header',$data);
		
		$data['organization'] = $this->tank_auth->get_org();
		$this->load->view('clients/add_nonloan_client', $data);	
		
		$this->load->view('footer');
	}
	
	
		   public function myGroupsDatatable()
        {
        
        	
 
                $this->load->library('datatables');
                $this->datatables->select('id, name,location,id');
                $this->datatables->from('groups');
               
                if(isset($_POST['location']) && $_POST['location'] != "All"){
	        		$location = $_POST['location'];
	        		
		        	 $this->datatables->where("organization_id =" . $this->tank_auth->get_orgid() . " AND location = '" . $location . "'");	
	        	} else {
		        	$this->datatables->where("organization_id =" . $this->tank_auth->get_orgid() );	
	        	}
	        	
	        
                $this->datatables->add_column('id', '<a href="/groups/viewGroup/$1">View Group</a>', 'id');
                
                echo $this->datatables->generate();
 
        }
        
        public function myGroupsMembersDatatable($groupID){
	         $this->load->library('datatables');
                $this->datatables->select('c.name, gm.role, c.cell, c.address, gm.id, c.pics');
                $this->datatables->from('group_members gm');
                $this->datatables->join('clients c', 'gm.client_id = c.id');
                
               	$this->datatables->where("group_id", $groupID );	
	        	
	        	
	        
                $this->datatables->edit_column('c.name', '<img style="width:100px;" src="$2" /><br /><strong>$1</strong>', 'c.name, c.pics');
                
                $this->datatables->edit_column('gm.id', '<a class="removeMember btn btn-danger" href="/groups/removeMember/$1"><i class="icon-remove"></i>Remove</a>', 'gm.id');
                
                echo $this->datatables->generate();
	        
	        
        }
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */