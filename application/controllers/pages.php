<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	
	public function view($page)
	{
	
		$this->load->library('cart');
		$this->load->helper('form');
		$data['page'] = $page;
		$this->load->view('header', $data);
		$this->load->view("pages/$page");
		$this->load->view('footer');
	}
	
	
	public function readMore($ngoID){
	
		$this->load->model('Ngo_model');
		
		$this->load->library('cart');
		$this->load->helper('form');
		$data['page'] = "Partner Page";
		$data['ngoinfo'] = $this->Ngo_model->get_ngo_info($ngoID);
		$data['ngoID'] = $ngoID;
		$this->load->view('header', $data);
		$this->load->view("partner/viewPartner");
		$this->load->view('footer');
		
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */