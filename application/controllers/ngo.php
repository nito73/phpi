<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ngo extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct()
    {
    
    	parent::__construct();
    	
    	$this->load->helper(array('form', 'url'));
		$this->load->library('security');
		$this->load->library('tank_auth');
		$this->load->model('Ngo_model');
		$this->lang->load('tank_auth');
		//$this->output->enable_profiler(TRUE);
		
    	if($this->tank_auth->get_usertype() != "NGO" && $this->tank_auth->get_usertype() != "sNGO"){
	    	redirect('/dashboard/');	
	    	
    	}
        
    }
	
	public function loanOfficer()
	{
		$data['page'] = "Loan Officer";
		$this->load->view('header',$data);
		$this->load->view('NGO/loanOfficer_view', $data);
		$this->load->view('footer');
	}
	
	public function reserveAccount()
	{
		$data['page'] = "Reserve Account";
		$this->load->view('header',$data);
		$this->load->view('NGO/reserveAccount_view', $data);
		$this->load->view('footer');
	}
	
	public function reserveAccountReport($id){
	
		$SQL = "SELECT data from ngo_reserve WHERE id=? and org_id= ?";
		$query = $this->db->query($SQL, array($id,$this->tank_auth->get_orgid()));
		echo $query->row()->data;
	}
	
	public function addLoanOfficerProcess(){
		
	$usertype = "loan_officer";		
	$use_username = $this->config->item('use_username', 'tank_auth');
	$email_activation = $this->config->item('email_activation', 'tank_auth');
					// validation ok
				if (!is_null($data = $this->tank_auth->create_user(
						$use_username ? $_POST['username'] : '',
						$_POST['email'],
						$_POST['password'],
						$usertype,
						$email_activation))) {									// success

						$user_id = $data['user_id'];

						$updata = array(
							'user_id' => $user_id,
							'firstname' => $_POST['firstname'],
							'lastname' => $_POST['lastname'],
							'pic' => $_POST['pic']
						);
						$this->db->where('user_id', $user_id);
						$this->db->update('user_profiles', $updata);
						
						
						
						
						$orgid = $this->tank_auth->get_orgid();
						
						$userdata = array(
							'user_id' => $user_id,
							'organization_id' => $orgid,
							
						);
							$this->db->insert('user_organization', $userdata);
							
													
							
							

						
					echo json_encode(array('status' => 'success', 'userid' => $user_id));	
				}  else {
					echo json_encode(array('status' => 'fail'));	
				}
				
	}
	
	public function phpApproval($projectid){
		$data = array('public' =>'pending');
		$this->db->where('id', $projectid);
		$this->db->update('projects', $data);
		redirect($_SERVER['HTTP_REFERER'], 'refresh');
		
	}
	
	
		public function nonphp()
	{
		$this->load->model('Dashboard_model');
		
		if ($this->tank_auth->is_logged_in()) {									// logged in
				$data['page'] = "Non-PHP Loans";
			$this->load->view('header',$data);
			 if($this->tank_auth->get_usertype() == "NGO" || $this->tank_auth->get_usertype() == "sNGO"){
				 $data['location'] = "All";
				 $this->load->model('Ngo_model');
				 $this->load->view('NGO/nonphp_view', $data);
			 } 
			
			$this->load->view('footer');
			
		} else {
			
			$this->session->set_flashdata('warning', 'Please Login or Register to view this page');
				redirect('/auth/login', 'refresh');

		}
	}
    
    	public function report()
	{
		
		if ($this->tank_auth->is_logged_in()) {									// logged in
				$data['page'] = "Reports";
			$this->load->view('header',$data);
			 if($this->tank_auth->get_usertype() == "NGO" || $this->tank_auth->get_usertype() == "sNGO"){
				
				 $this->load->model('Ngo_model');
				 $this->load->view('NGO/report_view', $data);
			 } 
			
			$this->load->view('footer');
			
		} else {
			
			$this->session->set_flashdata('warning', 'Please Login or Register to view this page');
				redirect('/auth/login', 'refresh');

		}
	}
    
    public function myProjectsDataTable()
        {
        
        	function formatmoney($data){
        		return money_format('%(!#10n', $data );
        	}
        	
 
                $this->load->library('datatables');
                $this->datatables->select('clients.id, type,clients.name,use,amount,location,projects.category, phpid, projects.currency,');
                $this->datatables->from('projects');
                $this->datatables->join('clients' , 'projects.client_id = clients.id ');
                
               
                if(isset($_POST['view']) && $_POST['view'] != "All"){
	        		$view = $_POST['view'];
	        		if(isset($_POST['category']) && $_POST['category'] != "All"){
		        		$category = $_POST['category'];
		        		$this->datatables->where('projects.organization_id =' . $this->tank_auth->get_orgid() . " AND projects.location = '" . $view . "'" . " AND projects.category = '" . $category . "'");
	        		} else {
		        		
		        	 $this->datatables->where("projects.organization_id =" . $this->tank_auth->get_orgid() . " AND 	projects.location = '" . $view . "'");	
	        		} 
	        		
	        	} else {
	        	
	        		if(isset($_POST['category']) && $_POST['category'] != "All"){
		        		$category = $_POST['category'];
		        		$this->datatables->where("projects.organization_id =" . $this->tank_auth->get_orgid() . " AND projects.category = '" . $category . "'");
	        		} else {
		        	 $this->datatables->where("projects.organization_id =" . $this->tank_auth->get_orgid());
					 }
	        	}
	        	
	        	if(isset($_POST['fundingsource'])){
		        	if($_POST['fundingsource'] == "All"){
			        	$this->datatables->where("projects.fundingsource != 'PHP'");	
			        } else {
				        $this->datatables->where("projects.fundingsource = '" .  $_POST['fundingsource'] . "'");
			        }
	        	} else {
		        	$this->datatables->where("projects.fundingsource = 'PHP'");
	        	}
                $this->datatables->edit_column('amount', '$1 $2', 'formatmoney(amount), projects.currency');
                $this->datatables->edit_column('phpid', '<a href="/clients/viewClient/$1">View Client</a>', 'clients.id');
                
                echo $this->datatables->generate();
 
        }
        
        
        
	public function viewProject($projectID)
	{
		$data['page'] = "My Projects";
		$data['projectID'] = $projectID;
		$this->load->view('header',$data);
		$this->load->view('NGO/view_project', $data);
		$this->load->view('footer');
	}
	
	public function printProject($projectID)
	{
		$data['page'] = "My Projects";
		$data['projectID'] = $projectID;
		$this->load->view('NGO/print_project', $data);
	}
	
		public function print_collection()
	{
		if(isset($_POST['view']) && $_POST['view'] != "All"){
	   		$data['view'] = $_POST['view'];
       	} else {
        	$data['view'] = 'All';
    	}
		$this->load->view('NGO/print_collection', $data);
	}
	
	
	public function repaymentProcess(){
		$projectid = $_POST['projectid'];
		
		if((int)$_POST['amount'] == 0){
			
		 $this->session->set_flashdata('message', array(
		 	'type' => 'warning',
		 	'body' => 'Please enter an amount to deposit'
		 ));						
		

		} else {
			$paymenttype = "Loan";
			 $this->session->set_flashdata('message', array(
		 	'type' => 'success',
		 	'body' => 'Loan payment processed successfully',
		 	'step' => 'Loan'
		 ));	
			
			if($paymenttype == 'Loan'){
		$SQL = "SELECT * FROM repayment WHERE projectid=? AND paymenttype=?";
		$query = $this->db->query($SQL, array($projectid, 'Loan'));
			if($query->num_rows() == 0){
				$this->db->where('id', $projectid);
				$this->db->update('projects', array('loandate' => date('Y-m-d')));
			} 
		
		
		}
		if(isset($_POST['clientid']) && $_POST['clientid'] != ""){
		$datarepayment = array(
 			'userid' => $this->tank_auth->get_user_id(),
			'paymenttype' => $paymenttype,
			'projectid' => $projectid,
			'clientid' => $_POST['clientid'],
			'ngoamount' => $_POST['amount'],
			'amount' => $_POST['amount']/$_POST['rate'],
			'date' => date('Y-m-d' , strtotime($_POST['date']))
		);
		
		} else {
			$datarepayment = array(
				'userid' => $this->tank_auth->get_user_id(),
				'paymenttype' => $paymenttype,
				'projectid' => $projectid,
				'groupid' => $_POST['groupid'],
				'ngoamount' => $_POST['amount'],
				'amount' => $_POST['amount']/$_POST['rate'],
				'date' => date('Y-m-d' , strtotime($_POST['date']))
			);
		
		}
		$this->db->insert('repayment', $datarepayment);

		//check status of project
		
		$SQL = "SELECT * FROM projects WHERE id=?";
		$query = $this->db->query($SQL, $projectid);
		
		$row= $query->row();
		
		$status = $row->public;
		
		if($status == "Funded"){
			
			$dataproject = array('public'=> 'Re-Payment');
			$this->db->where('id', $projectid);
			$this->db->update('projects', $dataproject);
		}
		}
		
		redirect($_SERVER['HTTP_REFERER'], 'refresh');
		
	}
	
	function closeProject($projectid){
		
			$SQL = "SELECT * FROM projects WHERE id=?";
			$query = $this->db->query($SQL, $projectid);
			
			$row = $query->row();
			if ($row->fundingsource == "PHP"){
				$dataproject = array('public'=> 'Pending Closing');
			} else {
				$dataproject = array('public'=> 'Closed');

			}
			$this->db->where('id', $projectid);
			$this->db->update('projects', $dataproject);
		
		redirect($_SERVER['HTTP_REFERER'], 'refresh');
	}
	
		public function deposit(){
		
	echo "<pre>";
	print_r($_POST);
	echo "</pre>";
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */