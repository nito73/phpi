<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Project extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 
	 function __construct()
	{
		parent::__construct();

		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->library('security');
		$this->load->library('tank_auth');
		$this->lang->load('tank_auth');
		
		$this->load->model('Ngo_model');


	}
	
		public function index()
	{
		$data['page'] = "Projects";
		$data['organization'] = $this->tank_auth->get_org();
		$data['location'] = "All";

		$this->load->view('header',$data);
		
		$data['organization'] = $this->Ngo_model->get_org_userID($this->tank_auth->get_user_id());
		$this->load->view('project/index', $data);	
		$this->load->view('footer');
	}
	public function add($page = 'splash')
	{
			$data['page'] = "Add Project";


		$this->load->view('header',$data);
		
		$data['organization'] = $this->Ngo_model->get_org_userID($this->tank_auth->get_user_id());
		$this->load->view('project/add_project', $data);	
	
		
		$this->load->view('footer');
	}
	
	public function edit($progectID)
	
	
	{
		$this->load->model('Give_model');
		if($this->tank_auth->get_usertype() == "Admin"){
			$data['page'] = "Edit Project";
			$data['projectID'] = $progectID;
			$this->load->view('header',$data);
			$this->load->view('project/edit_view', $data);
			$this->load->view('footer');
		
		} else {
		
		
	    	redirect('/dashboard/');	
	    	
    	}
	
	}
	
	public function get_rate()
	{
			$this->load->model('Report_model');
			$rate = $this->Report_model->get_rate($_POST['currency']);	

			  echo json_encode(array('status' => 'success', 'rate' => $rate));

	}
	
	public function process()
	{
	
	$this->load->model('Ngo_model');
	$this->load->model('Report_model');
	$organization = $this->Ngo_model->get_org_userID($this->tank_auth->get_user_id());
	if(isset($_POST['currency']) && $_POST['currency']){
	$rate = $this->Report_model->get_rate($_POST['currency']);
	  } else {
		  $rate = 1;
	  }
	  
	  
	  
	  $amount = $_POST['amount'];

	if($this->tank_auth->get_usertype() == "loan_officer"){
		$pending = "NGO_pending";
	} else {
		$pending = "pending";
	}
	
	$projectdata = array(
		'organization_id' => $this->tank_auth->get_orgid(),
		'title' => $_POST['title'],
		'name' => $_POST['name'],
		'amount' => $amount,
		'country' => $_POST['country'],
		'type' => $_POST['type'],
		'category' => $_POST['category'],
		'location' => $_POST['location'],
		'use' => $_POST['use'],
		'description' => $_POST['description'],
		'pics' => $_POST['pic'],
		'video' => $_POST['video'],
		'rate' => $rate,
		'public' => $pending,
		'fundingsource' => 'PHP',
		'organization_id' => $this->tank_auth->get_orgid() 	
	);
	
		$this->db->insert('projects', $projectdata);
		
		
		$id = $this->db->insert_id();
				
		echo json_encode(array('status' => 'success'));
		
	}
	
	public function processLoan()
	{
	
	$this->load->model('Ngo_model');
	$this->load->model('Report_model');
	$organization = $this->Ngo_model->get_org_userID($this->tank_auth->get_user_id());
	
	$ngorate = 1;
	$rate = 1;
	if(isset($_POST['usd']) && $_POST['usd'] == "YES"){
		$ngorate = $this->Report_model->get_rate($_POST['currency']);
	 }else {
		$rate = $this->Report_model->get_rate($_POST['currency']);

	 }
 	

	  $amount = $_POST['amount'];
	
	
	if($this->tank_auth->get_usertype() == "loan_officer"){
		$pending = "NGO_pending";
	} else {
		$pending = "pending";
	}
	if(isset($_POST['clientID']) && $_POST['clientID'] != ""){
	$projectdata = array(
		'organization_id' => $this->tank_auth->get_orgid(),
		'title' => $_POST['title'],
		'amount' => $amount/$rate,
		'actualamount' => $amount/$rate,
		'ngoamount' => $amount*$ngorate,
		'country' => $_POST['country'],
		'type' => $_POST['type'],
		'category' => $_POST['category'],
		'location' => $_POST['location'],
		'use' => $_POST['use'],
		'description' => $_POST['description'],
		'pics' => $_POST['pic'],
		'video' => $_POST['video'],
		'rate' => $rate*$ngorate,
		'currency' => $_POST['currency'],
		'loanterms' => $_POST['loanterms'],
		'loantermstype' => $_POST['loantermstype'],
		'lateterms' => $_POST['lateterms'],
		'lateamount' => $_POST['lateamount'],
		'startdate' => date('Y-m-d', strtotime($_POST['startdate'])),
		'loandate' => date('Y-m-d', strtotime($_POST['loandate'])),
		'loaninterest' => ($_POST['loaninterest']) ? $_POST['loaninterest'] : 0,
		'public' => $pending,
		'fundingsource' => 'PHP',
		'client_id' => $_POST['clientID']	
	);
	} else if(isset($_POST['groupID']) && $_POST['groupID'] != ""){
	$projectdata = array(
		'organization_id' => $this->tank_auth->get_orgid(),
		'title' => $_POST['title'],
		'amount' => round($amount/$rate),
		'actualamount' => $amount/$rate,
		'ngoamount' => $amount,
		'country' => $_POST['country'],
		'type' => $_POST['type'],
		'category' => $_POST['category'],
		'location' => $_POST['location'],
		'use' => $_POST['use'],
		'description' => $_POST['description'],
		'pics' => $_POST['pic'],
		'video' => $_POST['video'],
		'rate' => $rate,
		'currency' => $_POST['currency'],
		'loanterms' => $_POST['loanterms'],
		'loantermstype' => $_POST['loantermstype'],
		'lateterms' => $_POST['lateterms'],
		'lateamount' => $_POST['lateamount'],
		'startdate' => date('Y-m-d', strtotime($_POST['startdate'])),
		'loandate' => date('Y-m-d', strtotime($_POST['loandate'])),
		'loaninterest' => ($_POST['loaninterest']) ? $_POST['loaninterest'] : 0,
		'public' => $pending,
		'fundingsource' => 'PHP',
		'group_id' => $_POST['groupID']	
	);
	} else {
		$projectdata = array(
		'organization_id' => $this->tank_auth->get_orgid(),
		'title' => $_POST['title'],
		'name' => $_POST['name'],
		'amount' => $amount,
		'country' => $_POST['country'],
		'type' => $_POST['type'],
		'category' => $_POST['category'],
		'location' => $_POST['location'],
		'use' => $_POST['use'],
		'description' => $_POST['description'],
		'pics' => $_POST['pic'],
		'video' => $_POST['video'],
		'rate' => $rate,
		'currency' => $_POST['currency'],
		'loanterms' => $_POST['loanterms'],
		'startdate' => date('Y-m-d', strtotime($_POST['startdate'])),
		'loandate' => date('Y-m-d', strtotime($_POST['loandate'])),
		'loaninterest' => $_POST['loaninterest'],
		'public' => $pending,
		'fundingsource' => 'PHP'
	);	
	}
		$this->db->insert('projects', $projectdata);
		
		$id = $this->db->insert_id();
		if(isset($_POST['saving']) && $_POST['saving'] != ""){
			$savingamount = $_POST['saving']/$rate;
		
			$savingsdata = array(
				'projectid' => $id,
				'paymenttype' => 'Saving',
				'amount' => $savingamount,
				'ngoamount' => $_POST['saving'],
				'date' => date('Y-m-d'),
				'userid' => $this->tank_auth->get_user_id()
			);
			
			$this->db->insert('repayment', $savingsdata);
		}
		
		if(isset($_POST['clientID']) && $_POST['clientID'] != ""){
			echo json_encode(array('status' => 'success', 'clientID' => $_POST['clientID']));
		} else if(isset($_POST['groupID']) && $_POST['groupID'] != ""){
			echo json_encode(array('status' => 'success', 'groupID' => $_POST['groupID']));
		} else {
			echo json_encode(array('status' => 'success'));	
		}
		
		
	}
	
	public function processNonLoan(){
	
	$this->load->model('Ngo_model');
	$this->load->model('Report_model');
	$organization = $this->Ngo_model->get_org_userID($this->tank_auth->get_user_id());
	$rate = $this->Report_model->get_rate($_POST['currency']);
	 	
		if(isset($_POST['clientID']) && $_POST['clientID'] !=""){
		
			$projectdata = array(
		
		'client_id' => $_POST['clientID']	,
		'organization_id' => $this->tank_auth->get_orgid(),
		'title' => $_POST['title'],
		'amount' => $amount,
		'country' => $_POST['country'],
		'type' => $_POST['type'],
		'category' => $_POST['category'],
		'location' => $_POST['location'],
		'use' => $_POST['use'],
		'rate' => $rate,
		'currency' => $_POST['currency'],
		'loanterms' => $_POST['loanterms'],
		'loantermstype' => $_POST['loantermstype'],
			'lateterms' => $_POST['lateterms'],
		'lateamount' => $_POST['lateamount'],
		'startdate' => date('Y-m-d', strtotime($_POST['startdate'])),
		'loandate' => date('Y-m-d', strtotime($_POST['loandate'])),
		'loaninterest' => $_POST['loaninterest'],
		'public' => 'NONPHP',
		'fundingsource' => $_POST['fundingsource']	
	);	
		} else {
			$projectdata = array(
		
		'group_id' => $_POST['groupID']	,
		'organization_id' => $this->tank_auth->get_orgid(),
		'title' => $_POST['title'],
		'amount' => $amount,
		'country' => $_POST['country'],
		'type' => $_POST['type'],
		'category' => $_POST['category'],
		'location' => $_POST['location'],
		'use' => $_POST['use'],
		'rate' => $rate,
		'currency' => $_POST['currency'],
		'loanterms' => $_POST['loanterms'],
		'loantermstype' => $_POST['loantermstype'],
			'lateterms' => $_POST['lateterms'],
		'lateamount' => $_POST['lateamount'],
		'startdate' => date('Y-m-d', strtotime($_POST['startdate'])),
		'loandate' => date('Y-m-d', strtotime($_POST['loandate'])),
		'loaninterest' => $_POST['loaninterest'],
		'public' => 'NONPHP',
		'fundingsource' => $_POST['fundingsource']	
	);
		}
	
		$this->db->insert('projects', $projectdata);
		
		$id = $this->db->insert_id();
				
		if(isset($_POST['clientID']) && $_POST['clientID'] != ""){
			echo json_encode(array('status' => 'success', 'clientID' => $_POST['clientID']));
		} else if(isset($_POST['groupID']) && $_POST['groupID'] != ""){
			echo json_encode(array('status' => 'success', 'groupID' => $_POST['groupID']));
		
		} else {
			echo json_encode(array('status' => 'success'));	
		}
		
	}
	
	public function updateProcess()
	{
	
	$projectdata = array(
		'title' => $_POST['title'],
		'name' => $_POST['name'],
		'amount' => $_POST['amount'],
		'country' => $_POST['country'],
		'type' => $_POST['type'],
		'category' => $_POST['category'],
		'location' => $_POST['location'],
		'use' => $_POST['use'],
		'description' => $_POST['description'],
		'pics' => $_POST['pic'],
		'video' => $_POST['video']
	);
		$this->db->where('id', $_POST['projectID']);
		$this->db->update('projects', $projectdata);
		
		
		echo json_encode(array('status' => 'success'));
		
	}
	
	public function success(){
	
		$data['page'] = "Add Project";

		$this->load->view('header',$data);
		$this->load->view('project/success_view', $data);
		$this->load->view('footer');
		
	}
	
		public function viewProject($projectID)
	{
		$data['page'] = "My Projects";
		$data['projectID'] = $projectID;
		$this->load->view('header',$data);
		$this->load->view('project/view_project', $data);
		$this->load->view('footer');
	}


 public function myProjectDatatable()
        {
        function formatMoney($data){
	        
        
        	setlocale(LC_MONETARY, 'en_US');
        	return money_format('%(#10n', $data);
        }
        	
 
                $this->load->library('datatables');
                $this->datatables->select('phpid, title, name, amount, location, id ');
                $this->datatables->from('projects');
               
               
          
	                $this->datatables->where("type", 'project' );
	               $this->datatables->where("organization_id =" . $this->tank_auth->get_orgid() );	
	        	 $this->datatables->edit_column('amount', '$1', 'formatMoney(amount)');
	        	 $this->datatables->edit_column('id', '<a href="' . site_url() . 'project/viewProject/$1" class="btn btn-success" > View Project </a> ', 'id');
	        	                
                echo $this->datatables->generate();
 
        }

	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */