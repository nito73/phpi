<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 
	 function __construct()
    {
    
    	parent::__construct();
    	
    	$this->load->helper(array('form', 'url'));
		$this->load->library('security');
		$this->load->library('tank_auth');
		$this->load->model('Ngo_model');
		$this->lang->load('tank_auth');
		//$this->output->enable_profiler(TRUE);
		
    	
        
    }
	
	
	public function index()
	{
		
		
		
		if (!$this->tank_auth->is_logged_in()) {									// logged in
				redirect('/auth/login', 'refresh');			
		} else {
			$data['page'] = "Home";
			$data['user'] = $this->users->get_user_by_id($this->tank_auth->get_user_id());	
			if($this->tank_auth->get_usertype() == "NGO"){
				$data['organization'] = $this->Ngo_model->get_org_userID($this->tank_auth->get_user_id());
			} else {
				$data['organization'] = "";	
			}
			$this->load->view('header',$data);
			$this->load->view('/settings/settings_index');
			$this->load->view('footer');	

		}
	}
	
	public function viewLoanOfficer($userid)
	{
		
		
		
		if (!$this->tank_auth->is_logged_in()) {									// logged in
				redirect('/auth/login', 'refresh');			
		} else {
			$data['page'] = "Home";
			$data['user'] = $this->users->get_user_by_id($userid);	
			
			
			if($this->Ngo_model->check_loanOfficer($this->tank_auth->get_orgid(),$userid)){
				
				$this->load->view('header',$data);
				$this->load->view('/settings/settings_loanOfficer');
				$this->load->view('footer');		
			} else {
				$this->session->set_flashdata('message', array(
						 	'type' => 'warning',
						 	'body' => 'The account that you are trying to view is disabled or not allowed'
						 ));
			
				redirect('/ngo/loanOfficer', 'refresh');
				
			}
			
				
			

		}
	}
	
	public function update(){
				$userid = $this->tank_auth->get_user_id();		
			if($_POST['oldpassword'] != ""){
				
				if(sha1($_POST['password']) == sha1($_POST['confirmPassword'])){
					
					if($this->tank_auth->change_password($_POST['oldpassword'], $_POST['password'])){
						$this->session->set_flashdata('password', 'alart-success');
						$this->session->set_flashdata('password-msg', 'Your Password Has been Updated');
					} else {
						$this->session->set_flashdata('password', 'alert-warning');
						$this->session->set_flashdata('password-msg', 'Your current password is incorrect');
					}
					
				} else {
					$this->session->set_flashdata('password', 'alart-warning');
					$this->session->set_flashdata('password-msg', 'Your new password does not match with your confirmation');
					
					//nomatch
				}
			}
			
			$userprofiledata = array(
				
				'firstname' => $_POST['firstname'],
				'lastname' => $_POST['lastname'],
				'phone' => $_POST['phone']

			);
			$this->db->where('user_id', $userid);
			$this->db->update('user_profiles', $userprofiledata);
			
			$this->db->where('id', $userid);
			$this->db->update('users', array('email' => $_POST['email']));
			
			
			redirect($_SERVER['HTTP_REFERER'], 'refresh');

	
	}
	
	public function updateNGO(){
			$userid = $this->tank_auth->get_user_id();		
			if (!empty($_FILES['userfile'])){
		
				$config['upload_path'] = './uploads/org_logos/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
			
				
				$this->load->library('upload', $config);
				
				
					
				if ( $this->upload->do_upload()){
						$upload_data = $this->upload->data();
						$data = array (
							'logo' => '/uploads/org_logos/' . $upload_data['file_name'],
							'organization' => $_POST['orgname']
						);
						
					 } else {
				
						 $data = array (
								'organization' => $_POST['orgname']
								);
						}
						
				} else {
					$data = array (
						'organization' => $_POST['orgname']
					);
				
				
				}
					$this->db->where('id', $_POST['orgID']);
					$this->db->update('organization', $data);
				
			
			$this->session->set_flashdata('message', array(
						 	'type' => 'success',
						 	'body' => 'NGO settings has been successfully updated'
						 ));
			
			redirect($_SERVER['HTTP_REFERER'], 'refresh');

	
	}
	
	
	public function updateLoanOfficer(){
			if (isset($_POST['userID'])){
				
				$userid = $_POST['userID'];
			} else {
				$userid = $this->tank_auth->get_user_id();
				
			}
			
			if($_POST['password'] != ""){
				
				if(sha1($_POST['password']) == sha1($_POST['confirmPassword'])){
					
					if($this->tank_auth->change_password_phu($userid, $_POST['password'])){
						$this->session->set_flashdata('password', 'alart-success');
						$this->session->set_flashdata('password-msg', 'Your Password Has been Updated');
					} else {
						$this->session->set_flashdata('password', 'alert-warning');
						$this->session->set_flashdata('password-msg', 'Your current password is incorrect');
					}
					
				} else {
					$this->session->set_flashdata('password', 'alart-warning');
					$this->session->set_flashdata('password-msg', 'Your new password does not match with your confirmation');
					
					//nomatch
				}
			}
			
			$userprofiledata = array(
				
				'firstname' => $_POST['firstname'],
				'lastname' => $_POST['lastname'],
				'phone' => $_POST['phone']

			);
			$this->db->where('user_id', $userid);
			$this->db->update('user_profiles', $userprofiledata);
			
			$this->db->where('id', $userid);
			$this->db->update('users', array('email' => $_POST['email']));
			
			
			redirect($_SERVER['HTTP_REFERER'], 'refresh');

	
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */