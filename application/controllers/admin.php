<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct()
    {
    
    	parent::__construct();
    	
    	$this->load->helper(array('form', 'url'));
		$this->load->library('security');
		$this->load->library('tank_auth');
		$this->load->model('Admin_model');
		$this->load->model('Ngo_model');
		$this->lang->load('tank_auth');
		//$this->output->enable_profiler(TRUE);
		
    	if($this->tank_auth->get_usertype() != "Admin"){
	    	redirect('/dashboard/');	
	    	
    	}
        
    }
	public function index()
	{
		$data['page'] = "Admin";
		$this->load->view('header',$data);
		$this->load->view('admin/index_view', $data);
		$this->load->view('footer');
	}
	/*
		START: CMS ENTRIES
	*/
	// Call the CMS overview list (Still need to finish actual page)
	public function cms()
	{
		$data['page'] = "CMS";
		$this->load->view('header',$data);
		$this->load->view('admin/viewCMS_view', $data);
		$this->load->view('footer');
	}
	
	// TODO: Call specific editable text areas of the website
	public function edit_cms($cid){
		$data['page'] = "Edit CMS";
		$data['cmsid'] = $cid;
		$this->load->view('header',$data);
		$this->load->view('admin/editCMS_view', $data);
		$this->load->view('footer');
	}
	// Attempt to update the CMS entry
	public function updateCMS(){
		$cmsdata = array(
			'content' => $_POST['newcontent'],
			'edit_date' => $_POST['edittime'],
			'edit_by' => $_POST['editby'],
		);
		$this->db->where('cms_id', $_POST['cmsID']);
		$this->db->update('site_cms', $cmsdata);
		echo json_encode(array('status' => 'success'));
	}
	// Pass successful update to user
	public function success(){
		$data['page'] = "Edit Content";
		$this->load->view('header',$data);
		$this->load->view('admin/cms_success', $data);
		$this->load->view('footer');
	}
	/*
		END: CMS ENTRIES
	*/
	public function projects()
	{
		$data['page'] = "Projects";
		$this->load->view('header',$data);
		$this->load->view('admin/projects_view', $data);
		$this->load->view('footer');
	}
	
	public function project($projectid)
	{
		$data['page'] = "Projects";
		$data['projectid'] = $projectid;
		$this->load->view('header',$data);
		$this->load->view('admin/view_project_view', $data);
		$this->load->view('footer');
	}	
	
	public function viewClient($clientID)
	{
		$this->load->model('Client_model');
		$this->load->model('Ngo_model');
	
		$data['page'] = "Clients";
		$data['organization'] = $this->tank_auth->get_org();
	
		$data['clientID'] = $clientID;

		$this->load->view('header',$data);
		
		$this->load->view('admin/viewClient_view', $data);	
		$this->load->view('footer');
	}
	
		public function printClient($clientID)
	{
		$this->load->model('Client_model');
		$this->load->model('Ngo_model');
	
		$data['page'] = "Clients";
		$data['organization'] = $this->tank_auth->get_org();
	
		$data['clientID'] = $clientID;
		$this->load->view('admin/printClient_view', $data);	
	}
	
	public function viewOrder($orderid)
	{
		$data['page'] = "Payment";
		$data['orderid'] = $orderid;
		$this->load->view('header',$data);
		$this->load->view('admin/view_order', $data);
		$this->load->view('footer');
	}
	public function pendingpayment()
	{
		$data['page'] = "Pending_Payment";
		$this->load->view('header',$data);
		$this->load->view('admin/pendingpayment_view', $data);
		$this->load->view('footer');
	}
	
	public function pendingClosing()
	{
		$data['page'] = "Pending_Closing";
		$this->load->view('header',$data);
		$this->load->view('admin/pendingclosing_view', $data);
		$this->load->view('footer');
	}
	
	function declineClosure($projectid){
		
			
			$dataproject = array('public'=> 'Re-Payment');
			$this->db->where('id', $projectid);
			$this->db->update('projects', $dataproject);
		
		redirect($_SERVER['HTTP_REFERER'], 'refresh');
	}
	
	function approveClosure($projectid){
		
			//closing the loan
			$dataproject = array('public'=> 'Closed');
			$this->db->where('id', $projectid);
			$this->db->update('projects', $dataproject);
			
			
			
			//geting the loan info
			$SQL = "SELECT * FROM projects WHERE id=?";
			$query = $this->db->query($SQL, $projectid);
			$row = $query->row();
			$name = $row->title;
			$repaymenttotal = $this->Admin_model->total_repayment_amount($projectid);
			$repaymenttotalprinciple = (1 - ($row->loaninterest/100)) * $this->Admin_model->total_repayment_amountNGO($projectid);
			$percenttotal = $repaymenttotal/($row->amount*(($row->loaninterest/100)+1));
		
		
			//getting the closing rate
			
			$from = 'USD';
			$to = $row->currency;
			$url = 'http://finance.yahoo.com/d/quotes.csv?f=l1d1t1&s='.$from.$to.'=X';
			$handle = fopen($url, 'r');
			 
			if ($handle) {
			    $result = fgetcsv($handle);
			    fclose($handle);
			  }
			  
	
			  $closingrate = $result[0];	
			  
			  
			$closedata =array(
				'userid'    => $this->tank_auth->get_user_id(),
				'projectid' => $projectid,
				'clientid'  => $row->client_id,
				'amount' => $percenttotal*$row->amount,
				'rate' => $closingrate,
				'returned' => $percenttotal,
				'differencerate' => (($row->ngoamount/$row->rate)-($row->ngoamount/$closingrate ))*$closingrate,
				'differenceclose' => $row->ngoamount - $repaymenttotalprinciple
			);
			
		
			$this->db->insert('projects_closing', $closedata);
			
			  
		
			//returning to the seed account
			$SQL = "SELECT o.userid, funding.amount, o.id as orderid FROM funding JOIN `order` o ON funding.orderid = o.id  WHERE funding.projectid=?";
			$query = $this->db->query($SQL, $projectid);
			
			foreach($query->result() as $row){
				$orderid = $row->orderid;
				$userid = $row->userid;
				$amount = $row->amount;
				$totalamount =  $amount * $percenttotal;
				
				$phpamount = $totalamount *.08;
				
				$seedamount = $totalamount * 0.92;
				
				$seeddata = array(
				
					'userid'    => $userid,
					'projectid' => $projectid,
					'orderid' => $orderid,
					'reason' => "$name Paid off " . floor($percenttotal*100) . "% of the loan, Thank You",
					'amount' => $seedamount
				);
				
				$this->db->insert('seed_account', $seeddata);
				
				if( $orgid = $this->Admin_model->is_sDonor($userid)){
					
						$phpamount = $totalamount *.06; //6% for both
						$sngoamount = $totalamount *.02; //2% for both
						
						$transactiondata = array(
						
							'userid'    => $userid,
							'projectid' => $projectid,
							'orderid' => $orderid,
							'date' => date('Y-m-d'),
							'type' => "Re-Payment",
							'amount' => $sngoamount,
							'specialngo' => $orgid,
							'specialngoamount' => $sngoamount
							
						);	
						$this->db->insert('transaction', $transactiondata);

					} else {
				$transactiondata = array(
				
					'userid'    => $userid,
					'projectid' => $projectid,
					'orderid' => $orderid,
					'date' => date('Y-m-d'),
					'type' => "Re-Payment",
					'amount' => $phpamount
					
				);
				
				$this->db->insert('transaction', $transactiondata);
					}
			
			}


			$SQL = "SELECT * FROM repayment WHERE projectid=? ORDER By date DESC LIMIT 1";
			$query = $this->db->query($SQL);
			$row = $query->row();
					$this->db->where('id', $row->id);
					$this->db->update('repayment', array('closePayment' => 1 ));	 
		redirect($_SERVER['HTTP_REFERER'], 'refresh');
		
	}
	
	
	public function pendingusers()
	{
		$data['page'] = "Pending_Users";
		$this->load->view('header',$data);
		$this->load->view('admin/pendingusers_view', $data);
		$this->load->view('footer');
	}
	
	public function NGOs()
	{
		$data['page'] = "NGOs";
		$this->load->view('header',$data);
		$this->load->view('admin/NGOs_view', $data);
		$this->load->view('footer');
	}
	
	
	public function Special_NGOs()
	{
		$data['page'] = "Special NGOs";
		$this->load->view('header',$data);
		$this->load->view('admin/Special_NGOs_view', $data);
		$this->load->view('footer');
	}
	
	
	
	public function viewNGOsProjects($ngoid)
	{
		$data['page'] = "NGOs";
		$data['ngoid'] = $ngoid;
		$this->load->view('header',$data);
		$this->load->view('admin/view_NGOs_projects', $data);
		$this->load->view('footer');
	}
	
	
	public function ngoUpdates($ngoid)
	{
		$data['page'] = "NGOs";
		$data['ngoid'] = $ngoid;
		$this->load->view('header',$data);
		$this->load->view('admin/ngoUpdates_view', $data);
		$this->load->view('footer');
	}
	
	
	public function deleteApp($project_id){
		$this->db->where('id', $project_id);
		$this->db->delete('projects');
		redirect('admin', 'refresh');
	}
	

	
	public function ngoUpdatesProcess()
	{
		
		
		$data = array(
			'organization_id' => $_POST['orgID'],
			'date' => date("Y-m-d", strtotime($_POST['date'])),
			'notes' => $_POST['notes']
		);
		
		$orgID = $_POST['orgID'];
		$this->db->insert('organization_updates', $data);

				 $this->session->set_flashdata('message', array(
				 	'type' => 'success',
				 	'body' => 'Organization Updates has been added'
				 ));
		redirect('/pages/readMore/' . $orgID, 'refresh');
	}
	
	
	public function editNgoUpdates($updateID){
		$data['page'] = "NGOs";
		$data['updateID'] = $updateID;
		$this->load->view('header',$data);
		$this->load->view('admin/editNgoUpdates_view', $data);
		$this->load->view('footer');
	}
	
	
	
	public function ngoEditUpdatesProcess(){
		
		
		$data = array(
			'date' => date("Y-m-d", strtotime($_POST['date'])),
			'notes' => $_POST['notes']
		);
		$this->db->where('id', $_POST['updateID']);
		$this->db->update('organization_updates', $data);


		$SQL = "SELECT * FROM organization_updates WHERE id=?";
		$query = $this->db->query($SQL, $_POST['updateID']);
		$row = $query->row();
		
		$orgID = $row->organization_id;
		
		
				 $this->session->set_flashdata('message', array(
				 	'type' => 'success',
				 	'body' => 'Organization Updates has been updated'
				 ));
		redirect('/pages/readMore/' . $orgID, 'refresh');
	}
	
	
	
	public function viewNGOSettings($userid)
	{
		
		
		
		if (!$this->tank_auth->is_logged_in()) {									// logged in
				redirect('/auth/login', 'refresh');			
		} else {
			$data['page'] = "Home";
			$data['user'] = $this->Admin_model->get_user_by_id($userid);
			$data['userID'] = $userid;	
			$this->load->view('header',$data);
			$this->load->view('/admin/settings_NGO');
			$this->load->view('footer');		
	
		}
	}
	
	public function updateSettings(){
			$userid = $_POST['userID'];
			
			if($_POST['password'] != ""){
				
				if(sha1($_POST['password']) == sha1($_POST['confirmPassword'])){
					
					if($this->tank_auth->change_password_phu($userid, $_POST['password'])){
						$this->session->set_flashdata('password', 'alart-success');
						$this->session->set_flashdata('password-msg', 'Your Password Has been Updated');
					} else {
						$this->session->set_flashdata('password', 'alert-warning');
						$this->session->set_flashdata('password-msg', 'Your current password is incorrect');
					}
					
				} else {
					$this->session->set_flashdata('password', 'alart-warning');
					$this->session->set_flashdata('password-msg', 'Your new password does not match with your confirmation');
					
					//nomatch
				}
			}
			
			
			$userprofiledata = array(
				
				'firstname' => $_POST['firstname'],
				'lastname' => $_POST['lastname'],
				'phone' => $_POST['phone']
			

			);
			$this->db->where('user_id', $userid);
			
			
			$this->db->update('user_profiles', $userprofiledata);
			
			$this->db->where('id', $userid);
			$this->db->update('users', array('email' => $_POST['email']));
			redirect($_SERVER['HTTP_REFERER'], 'refresh');

	}
	

	public function viewspecialNGOsProjects($ngoid)
	{
		$data['page'] = "Special NGOs";
		$data['ngoid'] = $ngoid;
		$this->load->view('header',$data);
		$this->load->view('admin/view_specialNGOs_projects', $data);
		$this->load->view('footer');
	}
	
	public function donors()
	{
		$data['page'] = "Donors";
		$this->load->view('header',$data);
		$this->load->view('admin/donors_view', $data);
		$this->load->view('footer');
	}
	
	public function viewDonor($donorid)
	{
		$data['page'] = "Donors";
		$data['donorid'] = $donorid;
		$this->load->view('header',$data);
		$this->load->view('admin/view_donor', $data);
		$this->load->view('footer');
	}
	
	public function addDonorSeedProcess(){
		
			$this->load->library('form_validation');

			$this->form_validation->set_rules('reason', 'Reason', 'required');
			$this->form_validation->set_rules('amount', 'Amount', 'required|numeric');
		if ($this->form_validation->run() == FALSE)
		{
		
				 $this->session->set_flashdata('message', array(
				 	'type' => 'Warning',
				 	'body' => 'Seed was not added to account, Please fill out the form correctly'
				 ));
		} else {
		$data = array(
			'userid' => $_POST['donorID'],
			'reason' => $_POST['reason'],
			'amount' => $_POST['amount']
		);
		
		$this->db->insert('seed_account', $data);
		
		}
		redirect($_SERVER['HTTP_REFERER'], 'refresh');
	}
	
	public function transaction($day = 'All')
	{
		$data['page'] = "Transaction";
		$data['day'] = $day;
		$this->load->view('header',$data);
		$this->load->view('admin/transaction_view', $data);
		$this->load->view('footer');
	}
	
	public function fundedApplications()
	{
		$data['page'] = "Funded Applications";
		
		$this->load->view('header',$data);
		$this->load->view('admin/fundedApplications_view', $data);
		$this->load->view('footer');
	}
	
	public function depositToNGO($id)
	{
		$this->load->model('Report_model');
		$this->load->model('Client_model');
		$this->load->model('Ngo_model');
			
		$SQL = "SELECT * FROM projects WHERE id=?";
		$query = $this->db->query($SQL, $id);
		
		$row = $query->row();
		
		$data['newrate'] = $this->Report_model->get_rate($row->currency);
		$data['newngoamount'] = $row->amount * $data['newrate'];
		
		
		

		$data['page'] = "Clients";
		$data['organization'] = $row->organization_id;
	
		$data['clientID'] = $row->client_id;
		
		
		
		$SQLdata = array(
			'org_id' => $row->organization_id,
			'project_id' => $id,
			'currency'=> $row->currency,
			'type'=> "Intial Deposit for " . $row->title,
			'php_amount'=> $row->amount,
			'ngo_amount'=> $data['newngoamount'],
			'difference'=> $data['newngoamount'] - $row->ngoamount,
			'rate'=> $data['newrate'], 
			'data' => $this->load->view('admin/printClient_html', $data, TRUE)

		);
		
		$this->db->insert('ngo_reserve', $SQLdata);
		
		
		$this->db->where("id", $id);
		$this->db->update('projects', array("php_fund_status" => "Deposited on " . date("M d,Y") ));
		redirect($_SERVER['HTTP_REFERER'], 'refresh');
	}
	public function approvePayment($orderid){
		//update order
		$orderdata = array(
			'status' => "Approve"
		); 
		$this->db->where('id', $orderid);
		$this->db->update('order', $orderdata);
		
		//getuserid
		$userSQL = "SELECT userid FROM `order` WHERE id=? ";
		$userQuery = $this->db->query($userSQL, $orderid);
		
		$userrow = $userQuery->row();
		
		$userid = $userrow->userid;
		
		//Check if project is fully funded
		$SQL = "SELECT oi.*, oi.amount as oiamount, projects.public, projects.amount as projectamount FROM order_items oi JOIN projects ON oi.projectid = projects.id WHERE oi.orderid=?";
		$query = $this->db->query($SQL, $orderid);
		$total = 0;
		$project_id = '';
		foreach ($query->result() as $row){
			
			$project_id = $row->projectid;
			$status = $row->public;
			$total = $total + $row->oiamount;
			$projectamount = $row->projectamount;
			
			//if not funded
			if($status == "Approved"){
				
				$funddata = array(
					'projectid' => $project_id,
					'orderid' => $orderid,
					'amount' => $row->oiamount
					
					
				);
				
				$this->db->insert('funding', $funddata);
				
				$totalSQL = "SELECT sum(amount) as projectTotal FROM order_items WHERE projectid =?";
				$totalQuery = $this->db->query($totalSQL, $project_id);
				$totalRow = $totalQuery->row();
				$projectTotal = $totalRow->projectTotal;
				
				
				//check if its fully funded now
				if($projectamount <= $projectTotal ){
					$projectdata = array(
						'public' => 'Funded'
					);
					
					$this->db->where('id', $project_id);
					$this->db->update('projects', $projectdata);					
				
				
				}
				
			} else {
				
				$seeddata = array(
					'userid' => $userid,
					'projectid' => $project_id,
					'orderid' => $orderid,
					'reason' => 'Project has been Fully Funded when check was received',
					'amount' => $row->oiamount
				);
				
				$this->db->insert('seed_account', $seeddata);
			}
			
		}
		
		//php fund
		$php = $total*.08;
		$specialngo = "No";
		
		if($orgid = $this->Admin_model->is_sDonor($userid)){
			$specialngo = TRUE;
			
			
			$phpdata = array(
				'userid' => $userid,
				'orderid' => $orderid,
				'projectid' => $project_id,
				'date' => date('Y-m-d'),
				'type' => 'Seed',
				'amount' => '',
				'specialngo' => $orgid,
				'specialngoamount' => $php	
			);
			
			$this->db->insert('transaction', $phpdata);

		} else {
			$phpdata = array(
				'userid' => $userid,
				'orderid' => $orderid,
				'projectid' => $project_id,
				'date' => date('Y-m-d'),
				'type' => 'Seed',
				'amount' => $php
			);
			
			$this->db->insert('transaction', $phpdata);
		}	
			
		redirect($_SERVER['HTTP_REFERER'], 'refresh');
		
	}
	
	public function declinePayment($orderid){
		//update order
		$orderdata = array(
			'status' => "Decline"
		); 
		$this->db->where('id', $orderid);
		$this->db->update('order', $orderdata);
		
		//$this->db->delete('seed_account', array('orderid' => $orderdata));
		redirect($_SERVER['HTTP_REFERER'], 'refresh');
		

	}	
	
	//approving the project
	public function approve(){
		
		

		$projectdata = array(
			'phpid' => $_POST['phpid'],
			'public' => 'Approved'
		);
		
		$this->db->where('id', $_POST['id']);
		$this->db->update('projects', $projectdata);
		echo json_encode(array('status' => 'success'));	 
		
	}
	
		//approving the project
	public function decline(){
		
		

		
		$this->db->where('id', $_POST['id']);
		$this->db->delete('projects', $projectdata);
		echo json_encode(array('status' => 'success'));	 
		
	}

	//approving the user
	public function approveuser($userid = ""){
		if(isset($_POST['userid'])){
			$userid = $_POST['userid'];
		}
		

		$userdata = array('activated' => '1');
		
		$this->db->where('id', $userid);
		$this->db->update('users', $userdata);
		if(isset($_POST['userid'])){
		echo json_encode(array('status' => 'success'));	 
		} else {
			redirect($_SERVER['HTTP_REFERER'], 'refresh');
		}
		
	}	
	
	public function deleteuser($userid = ""){
		if(isset($_POST['userid'])){
			$userid = $_POST['userid'];
		}
		

		$userdata = array('activated' => '1');
		
		$this->db->where('id', $userid);
		$this->db->delete('users', $userdata);
		if(isset($_POST['userid'])){
		echo json_encode(array('status' => 'success'));	 
		} else {
			redirect($_SERVER['HTTP_REFERER'], 'refresh');
		}
		
	}	
	
	
	public function projectpublic()
	{
	
		$projectdata = array(
			'public' => $_POST['projectpublic']
		);
		
		$this->db->where('id', $_POST['project_id']);
		$this->db->update('projects', $projectdata);
		echo json_encode(array('status' => 'success'));	    	
	}
	
	public function projectprivate($project_id)
	{
		$projectdata = array(
			'public' => "NO"
		);
		
		$this->db->where('id', $project_id);
		$this->db->update('projects', $projectdata);
		redirect($_SERVER['HTTP_REFERER'], 'refresh');	
	    	
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */