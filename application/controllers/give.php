<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Give extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 
	 function __construct()
	{
		parent::__construct();

		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->library('security');
		$this->load->library('tank_auth');
		$this->lang->load('tank_auth');
		$this->load->model('Give_model');
		//$this->output->enable_profiler(TRUE);
	}
	public function index()
	{
		$data['page'] = "Give";
		$data['pagenum'] = 0;
		
		if ($this->session->userdata('viewLoanType')){
			$data['viewLoanType'] = $this->session->userdata('viewLoanType');
		} else {
			$data['viewLoanType'] = 'ALL';
		}
		
		if ($this->session->userdata('viewLoanCategory')){
			$data['viewLoanCategory'] = $this->session->userdata('viewLoanCategory');
		} else {
			$data['viewLoanCategory'] = 'ALL';
		}

		if ($this->session->userdata('viewOrgCategory')){
			$data['orgID'] = $this->session->userdata('viewOrgCategory');
		} else {
			$data['orgID'] = 0;
		}
		
		$this->load->view('header',$data);
		$orgid = $data['orgID'];
		if(($this->session->userdata('usertype') && ($this->tank_auth->get_usertype() == "sNGO" || $this->tank_auth->get_usertype() == "sIndivisual") ) || $this->session->userdata('SpecialNGO')){
			
			if($this->session->userdata('SpecialNGO')) {
				
				$orgid = $this->session->userdata('SpecialNGO');
			} else {
				$orgid = $this->tank_auth->get_orgid();
			}
		}
		
		$data['query'] = $this->Give_model->view($data['viewLoanType'],$data['viewLoanCategory'], 0, $orgid);	
		$this->load->view('give/sidebar_view', $data);
		$this->load->view('give/index_view', $data);
		$this->load->view('footer');
	}
	
	public function moreLoans($pagenum = 0)
	{
		$data['page'] = "Give";
		$data['pagenum'] = $pagenum;
		if ($this->session->userdata('viewLoanType')){
			$data['viewLoanType'] = $this->session->userdata('viewLoanType');
		} else {
			$data['viewLoanType'] = 'ALL';
		}
		
		if ($this->session->userdata('viewLoanCategory')){
			$data['viewLoanCategory'] = $this->session->userdata('viewLoanCategory');
		} else {
			$data['viewLoanCategory'] = 'ALL';
		}
		
		if ($this->session->userdata('viewOrgCategory')){
			$data['orgID'] = $this->session->userdata('viewOrgCategory');
		} else {
			$data['orgID'] = 0;
		}
		
		$this->load->view('header',$data);
		
		$orgid = 0;
		
		if(($this->session->userdata('usertype') && ($this->tank_auth->get_usertype() == "sNGO" || $this->tank_auth->get_usertype() == "sIndivisual") ) || $this->session->userdata('SpecialNGO')){
			
			if($this->session->userdata('SpecialNGO')) {
				
				$orgid = $this->session->userdata('SpecialNGO');
			} else {
				$orgid = $data['orgID'];
			}

		}	
			
		$data['query'] = $this->Give_model->view($data['viewLoanType'],$data['viewLoanCategory'], $pagenum, $orgid);	
		$this->load->view('give/sidebar_view', $data);
		$this->load->view('give/index_view', $data);
		$this->load->view('footer');
	}
	
	
	
	public function funded()
	{
		$data['page'] = "Funded";
		if ($this->session->userdata('viewLoanType')){
			$data['viewLoanType'] = $this->session->userdata('viewLoanType');
		} else {
			$data['viewLoanType'] = 'ALL';
		}
		
		if ($this->session->userdata('viewLoanCategory')){
			$data['viewLoanCategory'] = $this->session->userdata('viewLoanCategory');
		} else {
			$data['viewLoanCategory'] = 'ALL';
		}
		
		if ($this->session->userdata('viewOrgCategory')){
			$data['orgID'] = $this->session->userdata('viewOrgCategory');
		} else {
			$data['orgID'] = 0;
		}

		$this->load->view('header',$data);
		if(($this->session->userdata('usertype') && ($this->tank_auth->get_usertype() == "sNGO" || $this->tank_auth->get_usertype() == "sIndivisual") ) || $this->session->userdata('SpecialNGO')){
			
			if($this->session->userdata('SpecialNGO')) {
				
				$orgid = $this->session->userdata('SpecialNGO');
			} else {
				$orgid = $data['orgID'];
			}
			$data['query'] = $this->Give_model->sNGOViewFunded($data['viewLoanType'],$data['viewLoanCategory'],$orgid);
		} else {
			$data['query'] = $this->Give_model->viewFunded($data['viewLoanType'],$data['viewLoanCategory']);	
		}
		$this->load->view('give/sidebar_view', $data);
		$this->load->view('give/funded_view', $data);
		$this->load->view('footer');
	}
	
	
	function viewLoanType($type){
		$this->session->set_userdata('viewLoanType', $type);
		if($type == "Projects"){
				$this->session->set_userdata('viewLoanCategory', 'ALL');
			
		}
		redirect('/give/');	
			
		
		
	}
	function viewLoanCategory($type){
		$this->session->set_userdata('viewLoanCategory', $type);
		redirect('/give/');	
			
	}
	
	function viewOrg($type){
		$this->session->set_userdata('viewOrgCategory', $type);
		redirect('/give/');	
			
	}
	
	function plantSeed(){
		$id = $_POST['id'];
		$projectname = $this->Give_model->getProjectName($id);
		
		$query = $this->Give_model->viewProject($id);
		$total = $this->Give_model->getTotalFunded($id);

		$amount =0;
		foreach($query as $row){
			
			$amount = $row->amount;
		}		

		$remain = $amount - $total - $_POST['amount'];
		if($remain < 0 ){
		
				$remaining = $amount - $total;
				$message = "The application only need $$remaining to be fully funded";
			echo json_encode(array('status' => 'error',  'projectname' => $projectname, 'msg' => $message ));
		} else {
		$data = array(
               'id'      => $id,
               'qty'     => 1,
               'price'   => $_POST['amount'] . ".00",
               'name'	=> $projectname
            );
		
		$this->cart->insert($data);
		echo json_encode(array('status' => 'success', 'projectname' => $projectname ));
		}
		
	}
	function removeSeed(){
		$data = array(
               'rowid'      => $_POST['rowid'],
               'qty'     => 0
               
            );
		
		$this->cart->update($data);
		echo json_encode(array('status' => 'success' ));
		
	}
	public function viewNGO($ngoURL){
		
		$SQL = "SELECT * FROM organization WHERE url=?";
		$query = $this->db->query($SQL, $ngoURL );
		 if ($query->num_rows() > 0){
			$row = $query->row();
			$orgid = $row->id;
			$this->session->set_userdata('SpecialNGO', $orgid);
		}
		
		redirect('/give/', 'refresh');	
		
	}
	public function view($id)
	{
		$data['page'] = "Give";
	
		$this->load->view('header',$data);
		$data['query'] = $this->Give_model->viewProject($id);
		$data['total'] = $this->Give_model->getTotalFunded($id);
		$this->load->view('give/project_view', $data);
		$this->load->view('footer');
	}
	
	public function cart($step = "step1"){
		
	
		$data['page'] = "Cart";
		$this->load->helper('form');
		if( $step == 'step2' && !$this->tank_auth->is_logged_in()) {									// logged in
				$this->session->set_flashdata('warning', 'Please Login or Register to continue the checkout process');
				redirect('/auth/login', 'refresh');

			
		} else {
			$this->load->view('header',$data);
			$this->load->view('give/cart_' . $step);
			$this->load->view('footer');
		}
		
	}
	
	public function cartProcess(){
		
		$adjustment = "";
	for($i = 1; $i < $_POST['itemcount']; $i++){
	
			$id = $_POST["id$i"];
		
			$query = $this->Give_model->viewProject($id);
			$total = $this->Give_model->getTotalFunded($id);

			$amount =0;
			foreach($query as $row){
			
				$amount = $row->amount;
			}		

			$remain = $amount - $total - $_POST['price'.$i];
				
		$data = array(
               'rowid' => $_POST["rowid$i"],
               'qty'   => 0
            );

		$this->cart->update($data);
		
		$projectname = $this->Give_model->getProjectName($_POST["id$i"]);
		if($remain < 0 ){

		$data = array(
               'id'      => $_POST["id$i"],
               'qty'     => 1,
               'price'   => $amount - $total,
               'name'	=> $projectname
            );
         } else {
	         
	         $data = array(
               'id'      => $_POST["id$i"],
               'qty'     => 1,
               'price'   => $_POST["price$i"],
               'name'	=> $projectname
            );
            
            $adjustment = "Your Seed Cart has been adjusted to meet the need of the client";
           
            		
         }
		$this->cart->insert($data);
		if($adjustment){
			
			
		}
		
	}
	redirect('/give/cart/step2', 'refresh');	
	
	}
	
	public function complete(){
		
	$php = $this->cart->total()*.08;
	$data['grandtotal'] = $this->cart->total();
	$seedaccount = $this->Give_model->get_seed_account();
	
	$seed = 0;
	
	if($seedaccount > $data['grandtotal']){
		$seed = $data['grandtotal'];
		$data['grandtotal'] = 0;
		
	} else {
		$seed = $seedaccount;	
		$data['grandtotal'] = $data['grandtotal'] - $seedaccount; 
		$data['grandtotal'] = $data['grandtotal'] * 1.08;
	}
	
	
	if($data['grandtotal']  > 500) {
		$method = "Check";
		$status = "Pending";
		
	} elseif ($data['grandtotal'] == 0) {
		$method = "Seed";
		$status = "Approve";
		
		
	} else  {
		$method = "Paypal";
		$status = "Approve";
		
	}
	
	if(!$seed){
		$seed = 0;
	}
	
	
	
	$orderdata = array(
		'userid' => $this->tank_auth->get_user_id(),
		'total' => $data['grandtotal'],
		'method' => $method,
		'seed' => $seed,
		'status' => $status
	);
	
	$this->db->insert('order', $orderdata);
	
	$data['orderid'] = $this->db->insert_id();
		foreach ($this->cart->contents() as $items){
			$itemdata = array(
				'projectid' => $items['id'],
				'orderid' => $data['orderid'],
				'amount' => $items['price']
			);	
			$this->db->insert('order_items', $itemdata);
			
			if($status == "Approve"){
				$this->db->insert('funding', $itemdata);
			
			}

		}
		if($status == "Approve"){
			$phpdata = array(
				'userid' => $this->tank_auth->get_user_id(),
				'orderid' => $data['orderid'],
				'date' => date('Y-m-d'),
				'type' => 'Seed',
				'amount' => $php,
				'specialngo' => 'No'	
			);
			
			$this->db->insert('transaction', $phpdata);
		}
		if($seedaccount != 0){
			
			$seeddata = array(
					'userid' => $this->tank_auth->get_user_id(),
					'orderid' => $data['orderid'],
					'reason' => 'Used to plant Seed',
					'amount' => "-$seed"
				);
				
				$this->db->insert('seed_account', $seeddata);

		}
		
		
		$this->cart->destroy();
	
		
		$data['page'] = "Cart - Complete";
		$this->load->helper('form');
		$this->load->view('header',$data);
		$this->load->view('give/complete', $data);
		$this->load->view('footer');
	
		
	}
	
	public function completePaypal(){
		
	$php = $this->cart->total()*.08;
	$data['grandtotal'] = $this->cart->total()+$php;
	$seedaccount = $this->Give_model->get_seed_account();
	
	$seed = 0;
	
	if($seedaccount > $data['grandtotal']){
		$seed = $data['grandtotal'];
		$data['grandtotal'] = 0;
		
	} else {
		$seed = $seedaccount;	
		$data['grandtotal'] = $data['grandtotal'] - $seedaccount; 
	}
	
	
	if($data['grandtotal']  > 500) {
		$method = "Check";
		$status = "Pending";
		
	} elseif ($data['grandtotal'] == 0) {
		$method = "Seed";
		$status = "Approve";
		
		
	} else  {
		$method = "Paypal";
		$status = "Pending";
		
	}
	
	
	
	$orderdata = array(
		'userid' => $this->tank_auth->get_user_id(),
		'total' => $data['grandtotal'],
		'method' => $method,
		'seed' => $seed,
		'status' => $status
	);
	
	$this->db->insert('order', $orderdata);
	
	$data['orderid'] = $this->db->insert_id();
		foreach ($this->cart->contents() as $items){
			$itemdata = array(
				'projectid' => $items['id'],
				'orderid' => $data['orderid'],
				'amount' => $items['price']
			);	
			$this->db->insert('order_items', $itemdata);
			
			if($status == "Approve"){
				$this->db->insert('funding', $itemdata);
			
			}

		}
		if($status == "Approve"){
			$phpdata = array(
				'userid' => $this->tank_auth->get_user_id(),
				'orderid' => $data['orderid'],
				'date' => date('Y-m-d'),
				'type' => 'Seed',
				'amount' => $php,
				'specialngo' => 'No'	
			);
			
			$this->db->insert('transaction', $phpdata);
		}
		if($seedaccount != 0){
			
			$seeddata = array(
					'userid' => $this->tank_auth->get_user_id(),
					'orderid' => $data['orderid'],
					'reason' => 'Used to plant Seed',
					'amount' => "-$seed"
				);
				
				$this->db->insert('seed_account', $seeddata);

		}
		
		
		$this->cart->destroy();
	
		
		$data['page'] = "Cart - Complete";
		$this->load->helper('form');
		$this->load->view('header',$data);
		$this->load->view('give/complete_paypal', $data);
		$this->load->view('footer');
	
		
	}
	
	
	
	
	
	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */