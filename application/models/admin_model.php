<?php
class Admin_model extends CI_Model {
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function get_pending_projects(){
		$SQL = "SELECT * FROM projects WHERE public=?";
	    $query = $this->db->query($SQL, 'pending');
		if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	} 
	
	function get_fundedApplications(){
	    $SQL = "SELECT projects.*, o.organization  FROM projects join organization o ON projects.organization_id = o.id WHERE public=? AND fundingsource = ?";
	    $query = $this->db->query($SQL, array('funded', 'PHP'));
		if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	}
	
	function get_pending_payments(){
	    $SQL = "SELECT o.id as orderid, o.total, o.timestamp, up.firstname, up.lastname FROM `order` o JOIN user_profiles up ON o.userid = up.user_id WHERE o.status=?";
	    $query = $this->db->query($SQL, 'Pending');	    
	    if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	}
	
	function get_pending_closed_projects(){
	    $SQL = "SELECT p.id as projectid, p.*, o.* FROM projects p join organization o ON p.organization_id = o.id WHERE p.public=?";
	    $query = $this->db->query($SQL, 'Pending Closing');	    
	    if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	}
	
	function total_repayment_amount($id){
		$SQL = "SELECT sum(amount) as total FROM repayment WHERE projectid =?";
	    $query = $this->db->query($SQL, $id);
		if($query->num_rows() > 0){
			$row = $query->row();
		    return $row->total;
	    } else {
	    	return 0;
	    }
	}
	
	function total_repayment_amountNGO($id){
		$SQL = "SELECT sum(ngoamount) as total FROM repayment WHERE projectid =?";
	    $query = $this->db->query($SQL, $id);
		if($query->num_rows() > 0){
			$row = $query->row();
		    return $row->total;
	    } else {
	    	return 0;
	    }
	}
	function get_pending_users(){
	    $SQL = "SELECT u.id, up.firstname, up.lastname, o.organization, u.email FROM users u JOIN user_profiles up ON u.id = up.user_id JOIN user_organization uo ON u.id = uo.user_id JOIN organization o ON uo.organization_id = o.id WHERE  u.activated =? ";
	    $query = $this->db->query($SQL, '0');	    
	    if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	} 
    
    function get_user_by_id($userid){
		$SQL = "SELECT * FROM users JOIN user_profiles up on up.user_id =users.id WHERE users.id=?";
	    $query = $this->db->query($SQL, $userid);
	   	if($query->num_rows() > 0){
			return $query->row();
	    } else {
	    	return FALSE;
	    } 
    }
	
	function viewClient($clientID){
     	$SQL = "SELECT * FROM clients WHERE id=?  ";
	    $query = $this->db->query($SQL, array($clientID));
		if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	}
	
	function get_all_projects(){
	    $SQL = "SELECT p.id as projectid, p.type as apptype, p.*, o.* FROM projects p join organization o ON p.organization_id = o.id";
	    $query = $this->db->query($SQL);
		if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	} 
    
    function get_all_transaction($day){
    	if($day == 'All') {
		    $SQL = "SELECT t.*, up.firstname, up.lastname  FROM transaction t JOIN user_profiles up ON t.userid = up.user_id WHERE t.amount != ?";
		    $query = $this->db->query($SQL, array(0));
	    } else {
		  	$SQL = "SELECT t.*, up.firstname, up.lastname  FROM transaction t JOIN user_profiles up ON t.userid = up.user_id WHERE t.amount != ? AND datediff(curdate(), date) < ?";
		    $query = $this->db->query($SQL, array(0, $day));

	    }
	    if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	} 
    
	function get_specialNGO_transaction($ngoid){
	    $SQL = "SELECT t.*, up.firstname, up.lastname  FROM transaction t JOIN user_profiles up ON t.userid = up.user_id WHERE specialngo=?";
	    $query = $this->db->query($SQL, $ngoid);
		if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	}
	
	function get_all_NGOs_projects($ngoid){
	    $SQL = "SELECT p.id as projectid, p.type, p.*, o.* FROM projects p join organization o ON p.organization_id = o.id WHERE o.id=?";
	    $query = $this->db->query($SQL, $ngoid);
		if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	}
	
	function get_NGO_userid($orgID){
	    $SQL = "SELECT uo.user_id FROM user_organization uo JOIN users ON uo.user_id = users.id WHERE uo.organization_id=? AND (users.usertype = ? OR users.usertype = ?)";
	    $query = $this->db->query($SQL, array($orgID, "NGO", "sNGO"));
		if($query->num_rows() > 0){
			$row = $query->row();
		    return $row->user_id;
	    } else {
	    	return FALSE;
	    }
	} 
    
    function get_all_donors(){
		$SQL = "SELECT * FROM users JOIN user_profiles ON users.id = user_profiles.user_id where usertype=? OR usertype=?";
	    $query = $this->db->query($SQL, array('indivisual','Special Donor'));
		if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	}
	
	function view_donors($user_id){
		$SQL = "SELECT * FROM users JOIN user_profiles ON users.id = user_profiles.user_id where user_profiles.user_id=?";
	    $query = $this->db->query($SQL, array($user_id));
		if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	}
	
	function view_orders_by_userID($user_id){
		$SQL = "SELECT * FROM `order` WHERE userid = ?";
	    $query = $this->db->query($SQL, array($user_id));
		if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	}
    
    function view_seed_account($user_id){
		$SQL = "SELECT * FROM seed_account WHERE userid = ?";
	    $query = $this->db->query($SQL, array($user_id));
		if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	}
	
	function get_total_funded($projectid){
		$SQL = "SELECT sum(amount) as total FROM funding WHERE projectid =?";
	    $query = $this->db->query($SQL, $projectid);
		if($query->num_rows() > 0){
	      	$row = $query->row();
		    return $row->total;
	    } else {
	    	return '0';
	    }
	}
	
	function get_funding($projectid){
		$SQL = "SELECT funding.*, up.firstname, up.lastname, o.timestamp FROM funding JOIN `order` o  ON funding.orderid = o.id join user_profiles up ON o.userid = up.user_id WHERE funding.projectid =?";
	    $query = $this->db->query($SQL, $projectid);
		if($query->num_rows() > 0){
			return $query->result();
	    } else {
	    	return false;
	    }
	}
	
	function getOrgByID($orgID){
		$SQL = "SELECT organization.organization, user_profiles.firstname, user_profiles.lastname FROM organization JOIN user_organization on organization.id = user_organization.organization_id JOIN user_profiles ON user_organization.user_id = user_profiles.user_id WHERE organization.id =? LIMIT 1";
	    $query = $this->db->query($SQL, $orgID);
		$row = $query->row();
	    if($query->num_rows){
	    $data['organization'] = $row->organization;
	    $data['name'] = $row->firstname . ' ' . $row->lastname;
	   return $data; 
	   } else {
		   return false;
	   }
    }
	
	function getDonor($user_id){
		$SQL = "SELECT * FROM user_profiles WHERE user_id=? LIMIT 1";
	    $query = $this->db->query($SQL, $user_id);
		$row = $query->row();
	      $data['name'] = $row->firstname . ' ' . $row->lastname;
	   return $data; 
    }
	
	function get_all_NGOS(){
	    $SQL = "SELECT users.email, o.*, up.firstname, up.lastname FROM organization o JOIN user_organization uo on  uo.organization_id = o.id JOIN users on uo.user_id = users.id JOIN user_profiles up on users.id = up.user_id  WHERE users.usertype !=? OR users.usertype !=?";
	    $query = $this->db->query($SQL, array("indivisual", "sindivisual"));
		if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	}
	// Special NGO
	function get_all_Special_NGOS(){
	    $SQL = "SELECT users.email, o.*, up.firstname, up.lastname FROM organization o JOIN user_organization uo on  uo.organization_id = o.id JOIN users on uo.user_id = users.id JOIN user_profiles up on users.id = up.user_id  WHERE users.usertype =?";
	    $query = $this->db->query($SQL, array("sNGO"));
		if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	}
	
	function is_sDonor($user_id){
		$SQL = "SELECT * FROM users JOIN user_organization uo ON users.id = uo.user_id WHERE users.id=? LIMIT 1";
	    $query = $this->db->query($SQL, $user_id);
		$row = $query->row();
		if($query->num_rows() > 0 && $row->usertype == "sIndivisual"){
			return $row->organization_id;
		} else {
			return false;
		}
	}
	
	function is_sDonor_firsttime($user_id){
		$SQL = "SELECT * FROM `order` WHERE userid=? AND status=? LIMIT 1";
	    $query = $this->db->query($SQL, array($user_id,'Approve'));
		if($query->num_rows() == 0){
			return true;
		} else {
			return false;
		}
    }
	
	function viewProject($id){
		$SQL = "SELECT * FROM projects where id=?";
		$query = $this->db->query($SQL, $id);
        return $query->result();
    }
	
	function viewOrder($id) {
		$SQL = "SELECT * FROM `order` WHERE id = ?";
        $query = $this->db->query($SQL, $id);
        return $query->result();
    }
	
	function get_order_items($id) {
		$SQL = "SELECT order_items.amount as oiamount, order_items.*, projects.title FROM order_items Join projects on order_items.projectid = projects.id where order_items.orderid=?";
		$query = $this->db->query($SQL, $id);
        return $query->result();
    }
	/***************************************
		For:	CMS Addition
		Date:	05SEPT2015
		Dev:	William Essig
	****************************************
		TABLE:
			SITE_CMS
		COLUMNS:
			CMS_ID
				AUTO-INCREMENTING INT
			CMS_TITLE
				TITLE FOR AREA
			CONTENT
				TEXT AREA CONTENT
			EDIT_DATE
				UNIX_TIMESTAMP
			EDIT_BY
				TABLE->USER.ID
		**Should probably add a page column
	***************************************/
	
	// Grab all content from SITE_CMS, and convert SITE_CMS.EDIT_BY to USER.USERNAME per row
	function get_all_cms(){
		$sql = "SELECT cms.cms_id, cms.cms_title, cms.content, cms.edit_date, user.username FROM site_cms cms, users user WHERE cms.edit_by = user.id";
	    $query = $this->db->query($sql);
		if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	}
	// Get specific CMS row
	function getCMSrow($cmsid){
		$SQL = "SELECT * FROM site_cms where cms_id=?";
		$query = $this->db->query($SQL, $cmsid);
        return $query->result();
	}
	// Readable time (WP Style)
	function relativeTime($dt,$precision=2) {
		$times=array(	365*24*60*60	=> "year",
						30*24*60*60		=> "month",
						7*24*60*60		=> "week",
						24*60*60		=> "day",
						60*60			=> "hour",
						60				=> "minute",
						1				=> "second");

		$passed=time()-$dt;
		if($passed<5) {
			$output='less than 5 seconds ago';
		}
		else {
			$output=array();
			$exit=0;
			foreach($times as $period=>$name) {
				if($exit>=$precision || ($exit>0 && $period<60)) 	break;
				$result = floor($passed/$period);
				if($result>0) {
					$output[]=$result.' '.$name.($result==1?'':'s');
					$passed-=$result*$period;
					$exit++;
				}
				else if($exit>0) $exit++;
			}
			$output=implode(' and ',$output).' ago';
		}
		return $output;
	}
	// Limit amount of data shown for viewCMS overview
	function limit_the_output($str){
		if(strlen($str) > 150){ // Check if greater than set length (150)
			$newstr = substr($str,0,150);
			$str = substr($newstr,0,strrpos($newstr,' ')).'...';
		} else {
			$str = $str; // If str is less than 150 already, there's no need to truncate
		}
		return $str;
	}
}
?>