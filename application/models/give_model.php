<?php
class Give_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function view($type = "ALL", $category = "ALL", $pageNum = 0, $orgid)
    {
		if($this->tank_auth->get_usertype() == "Admin") {    	
		 	$this->db->where('public', 'Pending');
		 	$this->db->or_where('public',  'Approved');
		} else {
	    	$this->db->where('public',  'Approved');	
		}
		if($orgid != 0){
			$this->db->where('organization_id',  $orgid);
		}

		if ($category != "ALL") { $this->db->where('category', $category);} 
	    
	    if ($type != 'ALL') {$this->db->where('type', $type);}
	    
		$this->db->where('fundingsource', 'PHP');
		
		$numrow = $this->db->get('projects');	
	    $data['total'] = $numrow->num_rows(); 
		
		if($this->tank_auth->get_usertype() == "Admin") {    	
		 	$this->db->where('public', 'Pending');
		 	$this->db->or_where('public',  'Approved');
		} else {
	    	$this->db->where('public',  'Approved');	
		}
			if($orgid != 0){
			$this->db->where('organization_id',  $orgid);
		}

		if ($category != "ALL") { $this->db->where('category', $category);} 
	    
	    if ($type != 'ALL') {$this->db->where('type', $type);}
	    
		$this->db->where('fundingsource', 'PHP');
		
		
		if($pageNum == 0){
	    	$this->db->limit(25);
	    } else {
		    $limit = $pageNum*25;
		    $this->db->limit(25, $limit);
	    }	
    	
    	$query = $this->db->get('projects');	
	    	
	    
    	$data['query'] = $query->result();
        
        return $data;
    }
    
    function viewOrgList(){
	 	$SQL = "SELECT DISTINCT p.organization_id, o.* FROM projects p JOIN organization o on p.organization_id = o.id";
	 	$query = $this->db->query($SQL);
	 	
	 	return $query->result();   
	    
    }

 function viewFunded($type = "ALL", $category = "ALL")
    {
		
	    	if ($type == 'ALL') {
	    		if ($category == "ALL") {
			    	$SQL = "SELECT * FROM projects WHERE public=?";  		
	    		} else {
			    	$SQL = "SELECT * FROM projects WHERE public=? AND category=?";
		    	}
		    $query = $this->db->query($SQL, array('Closed', $category));
	    	} else {
		    	if ($category == "ALL") {
			    	$SQL = "SELECT * FROM projects WHERE public=? AND type=?";
			    } else {
				    $SQL = "SELECT * FROM projects WHERE public=? AND type=? AND category=?";
			    }
    	   $query = $this->db->query($SQL, array('Closed', $type, $category));
    	    	
	    	}
	    
    	
        
        return $query->result();
    }
    
    function sNGOViewFunded($type = "ALL", $category = "ALL", $orgid)
    {
	
	    	if ($type == 'ALL') {
	    		if ($category == "ALL") {
			    	$SQL = "SELECT * FROM projects WHERE public=? AND organization_id=?";  		
	    		} else {
			    	$SQL = "SELECT * FROM projects WHERE public=? AND organization_id=? AND category=?";
		    	}
		    $query = $this->db->query($SQL, array('Closed', $orgid, $category));
	    	} else {
		    	if ($category == "ALL") {
			    	$SQL = "SELECT * FROM projects WHERE public=? AND organization_id=? AND type=?";
			    } else {
				    $SQL = "SELECT * FROM projects WHERE public=? AND organization_id=? AND type=? AND category=?";
			    }
    	   $query = $this->db->query($SQL, array('Closed', $orgid, $type, $category));
    	    	
	    	}
	    
    	
        
        return $query->result();
    }

    function viewProject($id)
    {
    
    	$SQL = "SELECT * FROM projects where id=?";
    	
        $query = $this->db->query($SQL, $id);
        return $query->result();
    }
    
    function getOrgByOrgID($orgid){
	    
	    $SQL = "SELECT organization FROM organization WHERE id =? LIMIT 1";
	    $query = $this->db->query($SQL, $orgid);
	    
	    $row = $query->row();
	    $organization = $row->organization;
	   
	   return $organization; 
    }
    
    function getTotalFunded($id){
	    
	    $SQL = "SELECT sum(amount) as total FROM funding WHERE projectid=?";
	    $query = $this->db->query($SQL, $id);
	    
	    if ($query->num_rows() > 0){
		    $row = $query->row();
		    return $row->total;
		} else {
			$seed=0;
			return $seed;
		}
    }
    
    function getProjectName($id){
	    
	    $SQL = "SELECT * FROM projects WHERE id=?";
	    $query = $this->db->query($SQL, $id);
	    
	    if ($query->num_rows() > 0){
		    $row = $query->row();
		    return $row->title;
		} else {
			return 0;
		}
    }
    
    function get_seed_account(){
	    $SQL = "SELECT sum(amount) as seedtotal FROM seed_account WHERE userid =?";
	    
	    $query = $this->db->query($SQL, $this->tank_auth->get_user_id());
	    
	     if ($query->num_rows() > 0){
		    $row = $query->row();
		    return $row->seedtotal;
		} else {
			return 0;
		}
	    
    }
}
?>