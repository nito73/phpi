<?php
class Client_model extends CI_Model {

   

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
   
    function client_check($clientID){
 		$SQL = "SELECT * FROM clients WHERE id=? AND organization_id = ? ";
	    $query = $this->db->query($SQL, array($clientID, $this->tank_auth->get_orgid()));	    
	     	
	    if($query->num_rows() > 0){
		    return TRUE;
	    } else {
	    	return FALSE;
	    }
	    
    }      function viewClient($clientID){
     	$SQL = "SELECT * FROM clients WHERE id=? AND organization_id = ? ";
	    $query = $this->db->query($SQL, array($clientID, $this->tank_auth->get_orgid()));	    
	     	
	    if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	    
    } 
    
    
    function view_client_loan($clientID){
     	$SQL = "SELECT * FROM projects WHERE client_id=?";
	    $query = $this->db->query($SQL, array($clientID));	    
	     	
	    if($query->num_rows() > 0){
		    return $query->row();
	    } else {
	    	return FALSE;
	    }
	    

    }

     function view_client_loan_payment($clientID){
     	$SQL = "SELECT * FROM projects WHERE client_id=?";
	    $query = $this->db->query($SQL, array($clientID));	    
	     	
	    if($query->num_rows() > 0){
		   $clientLoan = $query->row();
		   $ngo_currency = $clientLoan->currency;
    	   $loan_terms = $clientLoan->loanterms;
    	   $loan_type = $clientLoan->loantermstype;
    	   $loandate = $clientLoan->loandate;	
    	   // $justinterest = $clientLoan->loaninterest/100;
    	   $justinterestPayment = (($clientLoan->loaninterest/100)*$clientLoan->ngoamount)/$loan_terms;
    	   $totalrequested = $clientLoan->amount;
    	   $rate= $clientLoan->rate;
    	   $ngoprincipal = ($clientLoan->ngoamount)/$loan_terms;
		   $principal = ($totalrequested)/$loan_terms;
		   $totalDue = $totalrequested;
		   $totalDueNGO = $clientLoan->ngoamount;

		   $loanSchedule = array();
			    $loanSchedule['unformated_total_ngo_principal'] = $totalDueNGO;
			    $loanSchedule['unformated_total_principal'] = $totalrequested;

			    $loanSchedule['unformated_total_ngo_interest'] = 0;
				$loanSchedule['unformated_total_interest'] =0;
			 for($x=0; $x < $loan_terms; $x++){
		    	

		    	if ($loan_type == "4"){
		    		$datediff = $x . " Month";
		    	} else {
		    		$datediff = $x * $loan_type;
		    		$datediff = $datediff . " Weeks";
		    	}
		    	$loanSchedule[$x]['date'] 		   			  = date("m/d/y", strtotime($loandate . ' +' . $datediff));
				$loanSchedule[$x]['ngo_principal']            = money_format('%(#10n', $this->roundUpToAny($ngoprincipal)) . " " . $ngo_currency;
				$loanSchedule[$x]['principal']                = '$' . money_format('%(#10n', $this->roundUpToAny($ngoprincipal)/$rate) . " USD";
				$loanSchedule[$x]['unformated_ngo_principal'] = $this->roundUpToAny($ngoprincipal);
				$loanSchedule[$x]['unformated_principal']     = $this->roundUpToAny($ngoprincipal)/$rate;
				$loanSchedule[$x]['ngo_interest']  = money_format('%(#10n', $this->roundUpToAny($justinterestPayment)) . " " . $ngo_currency;
				$loanSchedule[$x]['interest']      = '$'. money_format('%(#10n', $this->roundUpToAny($justinterestPayment)/$rate) . " USD";
				$loanSchedule[$x]['unformated_ngo_interest']  = $this->roundUpToAny($justinterestPayment);
				$loanSchedule[$x]['unformated_interest']      = $this->roundUpToAny($justinterestPayment)/$rate;
				
				$ngopayments = $this->roundUpToAny($ngoprincipal) + $this->roundUpToAny($justinterestPayment);
			    $payments = $ngopayments/$rate;
			    
			    $loanSchedule[$x]['ngo_payment']             = money_format('%(#10n', $ngopayments) . " " . $ngo_currency;
				$loanSchedule[$x]['payment']      			 = '$'. money_format('%(#10n', $payments) . " USD";
				$loanSchedule[$x]['unformated_ngo_payment']  = $ngopayments;
				$loanSchedule[$x]['unformated_payment']      = $payments;
				
			    $totalDueNGO = $totalDueNGO - $ngoprincipal;
			    $totalDue = $totalDue - $principal;

			    $loanSchedule['unformated_total_ngo_interest'] += $loanSchedule[$x]['unformated_ngo_interest'];
				$loanSchedule['unformated_total_interest'] += $loanSchedule[$x]['unformated_interest'];
				
		    }

		    return $loanSchedule;

	    } else {
	    	return FALSE;
	    }
	    

    }


    
    function client_savings_account($clientID){
     	$SQL = "SELECT * FROM savings WHERE client_id=? ORDER BY date";
	    $query = $this->db->query($SQL, array($clientID));	    
	     	
	    if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	    
    } 
    
     function client_savings_trans($transID){
     	$SQL = "SELECT * FROM savings WHERE id=? ORDER BY date";
	    $query = $this->db->query($SQL, array($transID));	    
	     	
	    if($query->num_rows() > 0){
		    return $query->row();
	    } else {
	    	return FALSE;
	    }
	    
    } 
    
        function client_savings_trans_total($clientID, $transID = 0){
     	if($transID == 0){
			     	$SQL = "SELECT sum(amount) as total FROM savings WHERE client_id=? ORDER BY date";
			     	$query = $this->db->query($SQL, array($clientID));	    
	     	
     	} else {
	     	     	$SQL = "SELECT sum(amount) as total FROM savings WHERE id <= ? AND client_id=? ORDER BY date";
	     	     	$query = $this->db->query($SQL, array($transID, $clientID));	    

     	}
	     	
	    if($query->num_rows() > 0){
		    return $query->row()->total;
	    } else {
	    	return FALSE;
	    }
	    
    } 
    
    
     function client_collateral_trans($transID){
     	$SQL = "SELECT * FROM collateral WHERE id=? ORDER BY date";
	    $query = $this->db->query($SQL, array($transID));	    
	     	
	    if($query->num_rows() > 0){
		    return $query->row();
	    } else {
	    	return FALSE;
	    }
	    
    }
    
    function client_collateral_trans_total($clientID, $transID = 0){
     	if($transID == 0){
			     	$SQL = "SELECT sum(amount) as total FROM collateral WHERE client_id=? ORDER BY date";
			     	$query = $this->db->query($SQL, array($clientID));	    
	     	
     	} else {
	     	     	$SQL = "SELECT sum(amount) as total FROM collateral WHERE id <= ? AND client_id=? ORDER BY date";
	     	     	$query = $this->db->query($SQL, array($transID, $clientID));	    

     	}
	     	
	    if($query->num_rows() > 0){
		    return $query->row()->total;
	    } else {
	    	return FALSE;
	    }
	    
    }       
       function client_loan_trans($transID){
     	$SQL = "SELECT * FROM repayment WHERE id=? ORDER BY date";
	    $query = $this->db->query($SQL, array($transID));	    
	     	
	    if($query->num_rows() > 0){
		    return $query->row();
	    } else {
	    	return FALSE;
	    }
	    
    } 
      
        function client_collateral_account($clientID){
     	$SQL = "SELECT * FROM collateral WHERE client_id=? ORDER BY date";
	    $query = $this->db->query($SQL, array($clientID));	    
	     	
	    if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	    
    } 
      
          /*
     * View org NGO
     */
   
       function client_city($orgid ){
	        $SQL = "SELECT distinct city FROM clients WHERE organization_id = ?";
	    $query = $this->db->query($SQL, array($orgid));
	    
	    if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	    
    }
    
       function client_region($orgid ){
	        $SQL = "SELECT distinct region FROM clients WHERE organization_id = ?";
	    $query = $this->db->query($SQL, array($orgid));
	    
	    if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	    
    }
    
      function get_exchange_rate($currency){
	      
	$from = 'USD';
	$to = $currency;
	$url = 'http://finance.yahoo.com/d/quotes.csv?f=l1d1t1&s='.$from.$to.'=X';
	
	 
	if ($handle = fopen($url, 'r')) {
	    $result = fgetcsv($handle);
	     $rate = $result[0];	
	    fclose($handle);
	  } else{
		   $rate = 1;
	  }
 
	  return $rate;
	  

      }



	function roundUpToAny($n,$x=5) {
    	return (ceil($n)%$x === 0) ? ceil($n) : round(($n+$x/2)/$x)*$x;
	}
    
}
?>
