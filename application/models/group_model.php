<?php
class Group_model extends CI_Model {

   

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
   
    
     function viewGroup($groupID){
     	$SQL = "SELECT * FROM groups WHERE id=? AND organization_id = ? ";
	    $query = $this->db->query($SQL, array($groupID, $this->tank_auth->get_orgid()));	    
	     	
	    if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	    
    } 
    
    
       function view_group_loan($groupID){
     	$SQL = "SELECT * FROM projects WHERE group_id=?";
	    $query = $this->db->query($SQL, array($groupID));	    
	     	
	    if($query->num_rows() > 0){
		    return $query->row();
	    } else {
	    	return FALSE;
	    }
	    
    }
    
     function viewGroupMembers($groupID){
     	$SQL = "SELECT * FROM group_members gm JOIN clients ON gm.client_id = clients.id WHERE gm.group_id=? AND clients.organization_id = ? ";
	    $query = $this->db->query($SQL, array($groupID, $this->tank_auth->get_orgid()));	    
	     	
	    if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	    
    }
    
    function get_org_clients(){
	    
	    $SQL = "SELECT * FROM clients WHERE organization_id = ? ";
	    $query = $this->db->query($SQL, array($this->tank_auth->get_orgid()));	    
	     	
	    if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
    }
    
  
    
    function client_savings_account($clientID){
     	$SQL = "SELECT sum(amount) as total FROM savings WHERE client_id=? ";
	    $query = $this->db->query($SQL, array($clientID));	    
	    
	    
	    if($query->num_rows() > 0){
		    return $query->row()->total;
	    } else {
	    	return FALSE;
	    }
	    
    } 
      
        function client_collateral_account($clientID){
     	$SQL = "SELECT * FROM collateral WHERE client_id=? ORDER BY date";
	    $query = $this->db->query($SQL, array($clientID));	    
	     	
	    if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	    
    } 
      
          /*
     * View org NGO
     */
   
       function group_location($orgid ){
	        $SQL = "SELECT distinct location FROM groups WHERE organization_id = ?";
	    $query = $this->db->query($SQL, array($orgid));
	    
	    if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	    
    }
    
       function client_region($orgid ){
	        $SQL = "SELECT distinct region FROM clients WHERE organization_id = ?";
	    $query = $this->db->query($SQL, array($orgid));
	    
	    if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	    
    }
    
      function get_exchange_rate($currency){
	      
	$from = 'USD';
	$to = $currency;
	$url = 'http://finance.yahoo.com/d/quotes.csv?f=l1d1t1&s='.$from.$to.'=X';
	
	 
	if ($handle = fopen($url, 'r')) {
	    $result = fgetcsv($handle);
	     $rate = $result[0];	
	    fclose($handle);
	  } else{
		   $rate = 1;
	  }
 
	  return $rate;
	  

      }
    
}
?>
