<?php
class Report_model extends CI_Model {

   

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
   
    
     function get_quarterly_report($qtr, $fundingSource){
     	if($fundingSource == "All"){
     	$SQL = "SELECT  c.*, p.id as projectid, p.fundingsource, p.title, paymenttype, sum(re.amount) as total, sum(re.ngoamount) as ngototal, COUNT(re.amount) AS totaltransactions FROM repayment re JOIN clients c on re.clientid = c.id LEFT JOIN projects p on re.projectid = p.id  WHERE QUARTER(re.date) = ? AND c.organization_id = ? GROUP BY re.projectid, re.paymenttype;";
	    $query = $this->db->query($SQL, array($qtr, $this->tank_auth->get_orgid()));	    
	     	
     	} else {
		    $SQL = "SELECT  c.*, p.id as projectid, p.fundingsource, p.title, paymenttype, sum(re.amount) as total, sum(re.ngoamount) as ngototal, COUNT(re.amount) AS totaltransactions FROM repayment re JOIN clients c on re.clientid = c.id LEFT JOIN projects p on re.projectid = p.id  WHERE QUARTER(re.date) = ? AND p.fundingsource=? AND c.organization_id = ? GROUP BY re.projectid, re.paymenttype;";
		    $query = $this->db->query($SQL, array($qtr, $fundingSource, $this->tank_auth->get_orgid()));	    
	    }
	    if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	    
    } 
    
    
       
     function get_date_report($startdate, $enddate, $fundingSource){
     	if($fundingSource == "All"){
     	 if($startdate == $enddate){
     	 	$SQL = "SELECT  c.*, p.id as projectid,  c.name as clientname, p.*, paymenttype,  sum(re.amount) as total, sum(re.ngoamount) as ngototal, COUNT(re.amount) AS totaltransactions, sum(re.depositamount)  as phpdepositamount, sum(re.depositngoamount)  as ngodepositamount  FROM repayment re JOIN clients c on re.clientid = c.id JOIN projects p on re.projectid = p.id  WHERE (date = ?) AND c.organization_id = ? GROUP BY re.projectid, re.paymenttype;";
	    $query = $this->db->query($SQL, array($startdate, $this->tank_auth->get_orgid()));	    
	     	} else {
		     	$SQL = "SELECT  c.*, p.id as projectid, c.name as clientname, p.*, paymenttype, sum(re.amount) as total, sum(re.ngoamount) as ngototal, COUNT(re.amount) AS totaltransactions, sum(re.depositamount)  as phpdepositamount, sum(re.depositngoamount)  as ngodepositamount FROM repayment re JOIN clients c on re.clientid = c.id JOIN projects p on re.projectid = p.id  WHERE (date BETWEEN ? and ?) AND c.organization_id = ? GROUP BY re.projectid, re.paymenttype;";
	    $query = $this->db->query($SQL, array($startdate, $enddate, $this->tank_auth->get_orgid()));	    
	     	
	     	}
     	} else {
     		 if($startdate == $enddate){

		    $SQL = "SELECT  c.*, c.name as clientname, p.*, paymenttype,  sum(re.amount) as total, sum(re.ngoamount) as ngototal, COUNT(re.amount) AS totaltransactions FROM repayment re JOIN clients c on re.clientid = c.id JOIN projects p on re.projectid = p.id  WHERE (date = ?) AND p.fundingsource=? AND c.organization_id = ? GROUP BY re.projectid, re.paymenttype;";
		    $query = $this->db->query($SQL, array($startdate, $fundingSource, $this->tank_auth->get_orgid()));	  
		    } else {
			    $SQL = "SELECT  c.*, c.name as clientname, p.*, paymenttype,  sum(re.amount) as total, sum(re.ngoamount) as ngototal, COUNT(re.amount) AS totaltransactions FROM repayment re JOIN clients c on re.clientid = c.id JOIN projects p on re.projectid = p.id  WHERE (date BETWEEN ? and ?) AND p.fundingsource=? AND c.organization_id = ? GROUP BY re.projectid, re.paymenttype;";
		    $query = $this->db->query($SQL, array($startdate, $enddate, $fundingSource, $this->tank_auth->get_orgid()));	  
		    }  
	    }
	    if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	    
    } 
    

     function get_transaction_report($startdate, $enddate){
     
     	 if($startdate == $enddate){
     	 	$SQL = "SELECT DISTINCT re.projectid  FROM repayment re JOIN clients c on re.clientid = c.id JOIN projects p on re.projectid = p.id  WHERE (date = ?) AND c.organization_id = ? GROUP BY re.projectid, re.paymenttype;";
	    $query = $this->db->query($SQL, array($startdate, $this->tank_auth->get_orgid()));	    
	     	} else {
		     	$SQL = "SELECT DISTINCT re.projectid  FROM repayment re JOIN clients c on re.clientid = c.id JOIN projects p on re.projectid = p.id  WHERE (date BETWEEN ? and ?) AND c.organization_id = ? GROUP BY re.projectid, re.paymenttype;";
	    $query = $this->db->query($SQL, array($startdate, $enddate, $this->tank_auth->get_orgid()));	    
	     	}
	     	
     	
	    if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	    
    } 

     function view_client_loan($projectid){
     	$SQL = "SELECT projects.*, clients.*, clients.name as clientname  FROM projects JOIN clients on projects.client_id = clients.id WHERE projects.id=?";
	    $query = $this->db->query($SQL, array($projectid));	    
	     	
	    if($query->num_rows() > 0){
		    return $query->row();
	    } else {
	    	return FALSE;
	    }
	    
    }

        function view_client_repayment($projectid, $startdate = FALSE, $enddate = FALSE){
     	if($startdate && $enddate){
	     	$SQL = "SELECT * FROM repayment WHERE projectid=? AND (date BETWEEN ? and ?) AND paymenttype=? ORDER BY date";
		    $query = $this->db->query($SQL, array($projectid,$startdate, $enddate,"Loan"));	    
	     } else {
	     	$SQL = "SELECT * FROM repayment WHERE projectid=? AND paymenttype=? ORDER BY date";
		    $query = $this->db->query($SQL, array($projectid,"Loan"));	    
	     }
	    if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	    
    }

    function total_client_repayment($projectid){
     	$SQL = "SELECT * FROM repayment WHERE projectid=? AND paymenttype=? ORDER BY date";
	    $query = $this->db->query($SQL, array($projectid,"Loan"));	    
	     	
	    if($query->num_rows() > 0){
		    return $query->num_rows();
	    } else {
	    	return FALSE;
	    }
	    
    }


         function get_intial_deposit($start, $end){
	         
     		
	     		 $this->db->select('nr.*, c.*, c.name as clientname, p.*, p.rate as intial_rate');
	     		 $this->db->from('ngo_reserve nr');
	     		 $this->db->join('projects p', "nr.project_id = p.id", "Left");
	         	 $this->db->join('clients c', "p.client_id = c.id", "Left");
	         	 $this->db->where('nr.timestamp >=', $start);
	         	 $this->db->where('nr.timestamp <=', $end);
	         	 $this->db->where('nr.org_id', $this->tank_auth->get_orgid());
		 
		    $query = $this->db->get();	  
		   
	    
	    if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	    
    } 
    
        function get_date_report_savings($startdate, $enddate){
        if($startdate == $enddate){
        $SQL = "SELECT  c.*, c.name as clientname, save.type,  count(save.type) AS totaltransactions, sum(save.amount) as total FROM savings save JOIN clients c on save.client_id = c.id  WHERE (date = ? ) AND c.organization_id = ? GROUP BY save.client_id, save.type";
	    $query = $this->db->query($SQL, array($startdate, $this->tank_auth->get_orgid()));	    
	     	} else {
     	$SQL = "SELECT  c.*, c.name as clientname, save.type,  count(save.type) AS totaltransactions, sum(save.amount) as total FROM savings save JOIN clients c on save.client_id = c.id  WHERE (date BETWEEN ? and ?) AND c.organization_id = ? GROUP BY save.client_id, save.type";
	    $query = $this->db->query($SQL, array($startdate, $enddate, $this->tank_auth->get_orgid()));	    
	     	}
     	
	    if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	    
    } 
    
     function get_YTD_data($year){
     	$SQL = "SELECT save.type, sum(save.amount) as total FROM savings save JOIN clients c on save.client_id = c.id  WHERE (date BETWEEN '$year-01-01' and  '$year-12-31' ) AND c.organization_id = ? GROUP BY save.type";
	    $query = $this->db->query($SQL, array($this->tank_auth->get_orgid()));	    
	     	
     	$data['savingQuery'] = $query->result();
     	
     	
     	$SQL = "SELECT p.*, sum(re.ngoamount) as ngototal FROM repayment re JOIN clients c on re.clientid = c.id LEFT JOIN projects p on re.projectid = p.id   WHERE (date BETWEEN '$year-01-01' and  '$year-12-31' ) AND c.organization_id = ? GROUP BY re.clientid";
	    $query = $this->db->query($SQL, array($this->tank_auth->get_orgid()));	    
	     	
     	$data['loanQuery'] = $query->result();
     	
	    if($query->num_rows() > 0){
		    return $data;
	    } else {
	    	return FALSE;
	    }
	    
    } 
    
    
         function get_closed_loan($startdate, $enddate){
     	
     	$SQL = "SELECT pc.*, c.name as clientname, p.rate as openrate, pc.rate as closerate, p.currency FROM projects_closing pc JOIN projects p on pc.projectid=p.id JOIN  clients c on p.client_id = c.id WHERE (pc.date BETWEEN ? and ?) AND p.organization_id = ?;";
	    $query = $this->db->query($SQL, array($startdate, $enddate, $this->tank_auth->get_orgid()));	    
	   
	    if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	    
    } 
    
    
    function get_rate($to)
	{
			
			$rate = "";
			$SQL = "SELECT * FROM rates WHERE currency=?";
			$query = $this->db->query($SQL, $to);
			
			//checks if there is a rate stored
			if($query->num_rows()){
				$row = $query->row();
				if($row->date == date("Y-m-d")){
					$rate = $row->rate;
				} else {
					$rate = "OLD";
					$id = $row->id;
				}
			}  else {
				$rate = "NONE";
			}
			
			
			 if($rate == "OLD" || $rate == "NONE"){
					$from = 'USD';
					$url = 'http://finance.yahoo.com/d/quotes.csv?f=l1d1t1&s='.$from.$to.'=X';
					$handle = fopen($url, 'r');
					 
					if ($handle) {
					    $result = fgetcsv($handle);
					    fclose($handle);
					  }
					  	
					  	
					  	$data = array(
						  		'rate' => $result[0],
						  		'date' => date("Y-m-d"),
						  		'currency' => $to
						  );		
						  
					// if the rate is old			  
					  if($rate == "OLD"){
						  $this->db->where('id', $id);
						  $this->db->update('rates', $data);
					  } else {
					  	//if first time
						  $this->db->insert('rates', $data);
						  
					  }
					  $rate = $result[0];	
					  
					  

			}
			
		return $rate;
		

	}


	//closing totals

	function closingtotals($clientid){
		$SQL = "SELECT sum(ngoamount) as clienttotalpayment, sum(depositamount) as ngototalpayment from repayment where clientid = ? GROUP BY clientid";
		$query = $this->db->query($SQL, $clientid);
		 if($query->num_rows() > 0){
		    return $query->row();
	    } else {
	    	return FALSE;
	    }
	}
function roundUpToAny($n,$x=5) {
    	return (ceil($n)%$x === 0) ? ceil($n) : round(($n+$x/2)/$x)*$x;
	}      
    
      
    
}


