<?php
class Dashboard_model extends CI_Model {

   

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
   
    
     function get_user_projects($user_id){
	    $SQL = "SELECT o.timestamp, o.method, p.title, p.id, p.amount as app_amount, oi.amount FROM order_items oi JOIN `order` o on oi.orderid=o.id JOIN projects p ON oi.projectid = p.id   WHERE o.userid = ? AND o.status = ? ";
	    $query = $this->db->query($SQL, array($user_id, "Approve"));	    
	    if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	    
    } 
    
       function get_user_stats($user_id){
	    $SQL = "SELECT sum(oi.amount) as total FROM order_items oi JOIN `order` o on oi.orderid=o.id JOIN projects p ON oi.projectid = p.id   WHERE o.userid = ? AND o.status != ? ";
	    $query = $this->db->query($SQL, array($user_id, "Closed"));	    
	    if($query->num_rows() > 0){
	    	$row = $query->row();
	    	$data['outstanding'] = $row->total;
		    return $data;
	    } else {
	    	return FALSE;
	    }
	    
    } 
    
        function view_orders_by_userID($user_id){
	        $SQL = "SELECT * FROM `order` WHERE userid = ?";
	    $query = $this->db->query($SQL, array($user_id));
	    
	    if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	    
    }
    
    function view_seed_account($user_id){
	        $SQL = "SELECT * FROM seed_account WHERE userid = ?";
	    $query = $this->db->query($SQL, array($user_id));
	    
	    if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	    
    }
    
    
    /*
     * View org NGO
     */
   
       function view_org_projects_location($orgid ){
	        $SQL = "SELECT distinct location FROM projects WHERE organization_id = ?";
	    $query = $this->db->query($SQL, array($orgid));
	    
	    if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	    
    }
      
       function view_org_projects($orgid, $location){
	      if($location == 'All') {
	        $SQL = "SELECT * FROM projects WHERE organization_id = ?";
	        $query = $this->db->query($SQL, array($orgid));
	    } else {
		    $SQL = "SELECT * FROM projects WHERE organization_id = ? AND location = ?";
	        $query = $this->db->query($SQL, array($orgid, $location));
	    }
	    if($query->num_rows() > 0){
		    return $query->result();
	    } else {
	    	return FALSE;
	    }
	    
    }
    
    function get_total_funded($projectid){
	    
	    $SQL = "SELECT sum(amount) as total FROM funding WHERE projectid =?";
	    $query = $this->db->query($SQL, $projectid);
	    
	      if($query->num_rows() > 0){
	      	$row = $query->row();
		    return $row->total;
	    } else {
	    	return '0';
	    }
	    
    }
    
  
    
     function getDonor($user_id){
	    
	    $SQL = "SELECT * FROM user_profiles WHERE user_id=? LIMIT 1";
	    $query = $this->db->query($SQL, $user_id);
	    
	    $row = $query->row();
	      $data['name'] = $row->firstname . ' ' . $row->lastname;
	   return $data; 
    }
    
      
    
}
?>