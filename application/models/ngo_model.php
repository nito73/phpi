<?php
class Ngo_model extends CI_Model {

   

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	function get_org_userID($userid){
		$SQL = "SELECT o.id as orgid, o.*, uo.* FROM organization o JOIN user_organization uo on o.id = uo.organization_id WHERE uo.user_id =?";
		$query = $this->db->query($SQL, $userid);
		if($query->num_rows()> 0){
			
			return $query->row();
		} else {
			return false;
		}
		
		
	}
	
	
   
    function get_total_funded($projectid){
	    
	    $SQL = "SELECT sum(amount) as total FROM funding WHERE projectid =?";
	    $query = $this->db->query($SQL, $projectid);
	    
	      if($query->num_rows() > 0){
	      	$row = $query->row();
		    return $row->total;
	    } else {
	    	return '0';
	    }
	    
    }
    
    
    
          function get_funding($projectid){
	    
	    $SQL = "SELECT funding.*, up.firstname, up.lastname, o.timestamp FROM funding JOIN `order` o  ON funding.orderid = o.id join user_profiles up ON o.userid = up.user_id WHERE funding.projectid =?";
	    $query = $this->db->query($SQL, $projectid);
	    
	      if($query->num_rows() > 0){

		    return $query->result();
	    } else {
	    	return false;
	    }
	    
    }
    
    function get_funding_source(){
	    
	    $SQL = "SELECT DISTINCT fundingsource FROM projects WHERE fundingsource != ? AND organization_id = ?";
	    $query = $this->db->query($SQL, array('PHP', $this->tank_auth->get_orgid()) );
	    
	    if($query->num_rows() > 0){

		    return $query->result();
	    } else {
	    	return false;
	    }
    }


      function viewProject($id)
    {
    
    	$SQL = "SELECT * FROM projects where id=?";
    	
        $query = $this->db->query($SQL, $id);
        return $query->result();
    }
    
    function total_repayment_amount($id){
	     $SQL = "SELECT sum(amount) as total FROM repayment WHERE projectid =? AND paymenttype = ? ";
	    $query = $this->db->query($SQL, array($id,'Loan'));
	    
	      if($query->num_rows() > 0){

		    	$row = $query->row();
				$total = array();
				$total['phptotal'] = $row->total;
				
				$SQL = "SELECT sum(ngoamount) as total FROM repayment WHERE projectid =? AND paymenttype != ? ";
				$query = $this->db->query($SQL, array($id,'Saving'));
				
				$row = $query->row();
				$total['ngototal'] = $row->total;
				
				if ($total['phptotal']) { return $total; } else {return FALSE;}
				
	    } else {
	    	return FALSE;
	    }

	    
    }
    
     function get_reserveAccount($orgid){
	     $SQL = "SELECT * FROM ngo_reserve WHERE org_id=? order by timestamp";
	    $query = $this->db->query($SQL, array($orgid,));
	    
	      if($query->num_rows() > 0){

		    return $query->result();
	    } else {
	    	return false;
	    }
	    
    }
	    function get_loanOfficers($orgid){
	     $SQL = "SELECT up.firstname, up.lastname, u.username, u.email, u.id FROM user_profiles up JOIN user_organization uo ON up.user_id = uo.user_id JOIN users u on up.user_id=u.id  WHERE uo.organization_id=? and u.usertype=?";
	    $query = $this->db->query($SQL, array($orgid, 'loan_officer'));
	    
	      if($query->num_rows() > 0){

		    return $query->result();
	    } else {
	    	return false;
	    }
	    
    }
    
    function check_loanOfficer($orgid,$userid){
	     $SQL = "SELECT up.firstname, up.lastname, u.username, u.email, u.id FROM user_profiles up JOIN user_organization uo ON up.user_id = uo.user_id JOIN users u on up.user_id=u.id  WHERE uo.organization_id=? and uo.user_id=?";
	    $query = $this->db->query($SQL, array($orgid, $userid));
	    
	      if($query->num_rows() > 0){

		    return true;
	    } else {
	    	return false;
	    }
	  }
    
    function get_user_name($userid){
	    $SQL = "SELECT firstname, lastname FROM user_profiles WHERE user_id = ?";
	    $query = $this->db->query($SQL, array($userid));
	    
	    if($query->num_rows() > 0){
		    $row = $query->row();
		    $name = $row->firstname . ' ' . $row->lastname;
		    return $name;
	    } else {
	    	return "";
	    }
	    
	    
    }
        
    function view_repayment($id, $transactionID = 0){
	    if($transactionID == 0 ){
	     $SQL = "SELECT * FROM repayment WHERE projectid =? AND paymenttype=? ORDER by date";
	    $query = $this->db->query($SQL, array($id,'Loan'));
	    } else {
			 $SQL = "SELECT * FROM repayment WHERE projectid =? AND repayment.id <= ? AND paymenttype=? ORDER by date";
	    $query = $this->db->query($SQL, array($id, $transactionID, 'Loan'));    
	    }
	      if($query->num_rows() > 0){

		    return $query->result();
	    } else {
	    	return false;
	    }
	    
    }
	
	    function view_savings($id){
	     $SQL = "SELECT * FROM repayment WHERE projectid =? AND paymenttype!=? ORDER by date";
	    $query = $this->db->query($SQL, array($id,'Loan'));
	    
	      if($query->num_rows() > 0){

		    return $query->result();
	    } else {
	    	return false;
	    }
	    
    }
    
    function get_project_status($id){
	     $SQL = "SELECT public FROM projects WHERE id =?";
	    $query = $this->db->query($SQL, $id);
	    
	      if($query->num_rows() > 0){

		    $row = $query->row();
		    return $row->public;
	    } else {
	    	return 0;
	    }
	    
    }
    
    function last_payment_made($projectid){
	    $SQL = "SELECT * FROM repayment WHERE projectid=? AND paymenttype=? ORDER BY date DESC LIMIT 1";
	    $query = $this->db->query($SQL, array($projectid, "Loan"));
	     if($query->num_rows() > 0){

		    $row = $query->row();
		    return $row->ngoamount;
	    } else {
	    	return 0;
	    }
    }
    
    
    // Recollection Sheets
  	  function get_org_funded_projects($view){
  	  	if($view == "All"){
	     $SQL = "SELECT * FROM projects WHERE organization_id=? AND (public =? OR public =?) ";
	    $query = $this->db->query($SQL, array($this->tank_auth->get_orgid(), "Funded", "Re-Payment"));
	    } else {
		 $SQL = "SELECT * FROM projects WHERE organization_id=? AND (public =? or public =?) AND location=?";
	    $query = $this->db->query($SQL, array($this->tank_auth->get_orgid(), "Funded" , "Re-Payment", $view));   
	    }
	      if($query->num_rows() > 0){

		   
		    return $query->result();
	    } else {
	    	return 0;
	    }
	    
    }  
    
    
    
    //Partner Pages
    
    function get_ngo_info($ngoID){
	    
	    $SQL = "SELECT * FROM organization WHERE id=?";
	    $query = $this->db->query($SQL, $ngoID);
	    if($query->num_rows() > 0){

		    return $query->row();
	    } else {
	    	return 0;
	    }
    }
    
     function get_ngo_updates($ngoID){
	    
	    $SQL = "SELECT * FROM organization_updates WHERE organization_id=? ORDER BY date DESC";
	    $query = $this->db->query($SQL, $ngoID);
	    if($query->num_rows() > 0){

		    return $query->result();
	    } else {
	    	return 0;
	    }
    }
    
      function get_ngo_update($updateID){
	    
	    $SQL = "SELECT * FROM organization_updates WHERE id=?";
	    $query = $this->db->query($SQL, $updateID);
	    if($query->num_rows() > 0){

		    return $query->row();
	    } else {
	    	return 0;
	    }
    }
    
    
    
    
    
}
?>