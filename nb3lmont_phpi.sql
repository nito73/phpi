-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 08, 2015 at 05:29 PM
-- Server version: 5.5.42-37.1
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `nb3lmont_phpi`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(11) unsigned NOT NULL,
  `organization_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `postalcode` varchar(255) DEFAULT NULL,
  `collectionlocation` varchar(255) DEFAULT '',
  `gov_id` varchar(255) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `birthplace` varchar(255) DEFAULT NULL,
  `martial_status` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `job` varchar(255) DEFAULT NULL,
  `cell` varchar(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `coname` varchar(255) DEFAULT NULL,
  `coaddress` varchar(255) DEFAULT NULL,
  `cocity` varchar(255) DEFAULT NULL,
  `coregion` varchar(255) DEFAULT NULL,
  `copostalcode` varchar(255) DEFAULT NULL,
  `cocell` varchar(255) DEFAULT NULL,
  `pics` varchar(255) DEFAULT NULL,
  `copics` varchar(255) DEFAULT NULL,
  `cojob` varchar(255) DEFAULT NULL,
  `cogender` varchar(255) DEFAULT NULL,
  `comartial_status` varchar(255) DEFAULT NULL,
  `cobirthplace` varchar(255) DEFAULT NULL,
  `cobirthdate` date DEFAULT NULL,
  `cogov_id` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'Complete'
) ENGINE=InnoDB AUTO_INCREMENT=316 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `organization_id`, `name`, `address`, `city`, `region`, `postalcode`, `collectionlocation`, `gov_id`, `birthdate`, `birthplace`, `martial_status`, `gender`, `job`, `cell`, `currency`, `coname`, `coaddress`, `cocity`, `coregion`, `copostalcode`, `cocell`, `pics`, `copics`, `cojob`, `cogender`, `comartial_status`, `cobirthplace`, `cobirthdate`, `cogov_id`, `status`) VALUES
(1, NULL, '', '', '', NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', 'USD', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(2, 11, 'Peter Yim', '19 West ', 'San Francisco', '', '14605', '', NULL, NULL, NULL, NULL, NULL, NULL, '56541', 'GHS', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(3, 11, 'Peter Yim', '19 West ', 'San Francisco', '', '14605', '', NULL, NULL, NULL, NULL, NULL, NULL, '56541', 'GHS', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(4, 11, 'Test User 1', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', 'USD', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(5, 11, 'Test User 2', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', 'USD', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(6, 11, 'Test User 2', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', 'USD', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(7, 11, 'Test User 2', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', 'USD', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(8, 11, 'test user 4', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', 'GHS', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(9, 11, 'test user 5', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', 'USD', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(10, 11, 'test user 5', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', 'USD', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(11, 13, 'Philippe N', 'Kinunu', 'Boneza', 'Rwanda', '', 'Susho', NULL, NULL, NULL, NULL, NULL, NULL, '0788463252', 'RWF', '', '', '', '', '', '', 'http://phpintl.org/imageupload/server/php/files/DSC07350.JPG', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(12, 13, 'john Gasangwa', 'somewhere', 'Kinunu', 'Rwanda', '', 'Susho', NULL, NULL, NULL, NULL, NULL, NULL, '0788464444', 'RWF', 'Gasangwa John', '', 'Kigali', 'Rwanda', '', '02312354564', 'http://phpintl.org/imageupload/server/php/files/mcy%20ntwali_001.jpg', 'http://phpintl.org/imageupload/server/php/files/m%20gsh000_003.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(13, 13, 'TUYISABE Darius', 'Muramba', 'Boneza', 'Rwanda', '', 'Muramba', NULL, NULL, NULL, NULL, NULL, NULL, '0789903126', 'RWF', '', '', '', '', '', '', 'http://phpintl.org/imageupload/server/php/files/IMG00172%20%282%29.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(14, 13, 'MUKANDAYISENGA Marie Chantal', 'Karukamba', 'Boneza', 'Rwanda', '', 'Karukamba', NULL, NULL, NULL, NULL, NULL, NULL, '0725249532', 'RWF', '', '', '', '', '', '', 'http://phpintl.org/imageupload/server/php/files/DSC00148%20%281%29.JPG', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(15, 13, 'Tegerezayesu Ablaham', 'Kinunu', 'Boneza', 'Rwanda', '', 'Kinunu', NULL, NULL, NULL, NULL, NULL, NULL, '0783257404', 'RWF', '', '', '', '', '', '', 'http://phpintl.org/imageupload/server/php/files/DSC00219.JPG', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(16, 14, 'Tom', '23 kigali st', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(17, 15, 'Murorunkwere', 'Gasabo', 'Kigali', 'Kigali City', '89707', 'Bank of Kigali', NULL, NULL, NULL, NULL, NULL, NULL, '0788250163', 'RWF', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(18, 15, 'TONY NGUYEN', '45 ROCK RD', 'RWANDA', 'KIGALI', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(19, 21, 'jason', '12 asdf', 'kigali', 'east', '125343', 'Rwanda', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(20, NULL, 'Barack Obama', '1 white house', 'washington', 'Virgina', '12345', 'Rusororo', NULL, NULL, NULL, NULL, NULL, NULL, '0788250162', '', '', '', '', '', '', '', 'http://phpintl.org/imageupload/server/php/files/bs.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(21, NULL, 'Barack Obama', '1 white house', 'washington', 'Virgina', '12345', 'Rusororo', NULL, NULL, NULL, NULL, NULL, NULL, '0788250162', '', '', '', '', '', '', '', 'http://phpintl.org/imageupload/server/php/files/bs.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(22, NULL, 'Barack Obama', '1 white house', 'washington', 'Virgina', '12345', 'Rusororo', NULL, NULL, NULL, NULL, NULL, NULL, '0788250162', '', '', '', '', '', '', '', 'http://phpintl.org/imageupload/server/php/files/bs.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(23, NULL, 'Obama', '1 washington', 'WashingtonDC', 'Virgina', '12345', 'Rwanda', NULL, NULL, NULL, NULL, NULL, NULL, '0788250112', '', '', '', '', '', '', '', 'http://phpintl.org/imageupload/server/php/files/bs%20%281%29.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(24, 14, 'Tony Nguyen', '78 ARCHER RD', 'rochester', 'New York', '14624', 'United States', NULL, NULL, NULL, NULL, NULL, NULL, '0788250163', 'RWF', '', '', '', '', '', '', 'http://phpintl.org/imageupload/server/php/files/bs%20%282%29.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(25, 21, 'muhire', 'kabuga', 'kigali', 'rwanda', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '0788845020', 'RWF', 'MUGABEKAZI', 'KABUGA', 'KIGALI', 'RWANDA', '', '0786186155', 'http://phpintl.org/imageupload/server/php/files/bs%20%284%29.jpg', 'http://phpintl.org/imageupload/server/php/files/bs%20%285%29.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(26, 21, 'NIYISHOBORA Jean de Dieu', 'KABUGA I', 'KIGALI', 'RUSORORO', '', 'KABUGA  I', NULL, NULL, NULL, NULL, NULL, NULL, '07885653141', 'RWF', '', '', '', '', '', '', 'http://phpintl.org/imageupload/server/php/files/DSC02387.JPG', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(27, 21, 'NIYISHOBORA Jean de Dieu', 'KABUGA I', 'KIGALI', 'RUSORORO', '', 'KABUGA  I', NULL, NULL, NULL, NULL, NULL, NULL, '07885653141', 'RWF', '', '', '', '', '', '', 'http://phpintl.org/imageupload/server/php/files/DSC02387.JPG', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(28, 21, 'RUCYAHANA Jean Baptiste', 'NYAGAHINGA', 'KIGALI', 'RUSORORO', '', 'KINYANA', NULL, NULL, NULL, NULL, NULL, NULL, '0782059298', 'RWF', 'MUKANKUSI Jeanne', 'NYAGAHINGA', 'KIGALI', 'RUSORORO', '', '0782059298', 'http://phpintl.org/imageupload/server/php/files/DSC02399%20%281%29.JPG', 'http://phpintl.org/imageupload/server/php/files/DSC02401.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(29, 21, 'NIYISHOBORA Jean de Dieu', 'KABUGA I', 'KIGALI', 'RUSORORO', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '07885653141', 'RWF', 'MUKABALISA Esperance', 'KABUGA I', 'KIGALI', 'RUSORORO', '', '07885653141', 'http://phpintl.org/imageupload/server/php/files/DSC02387%20%281%29.JPG', 'http://phpintl.org/imageupload/server/php/files/DSC02402%20%281%29.JPG', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(30, 21, 'MUKESHIMANA Seraphine', 'NYAGAHINGA', 'KIGALI', 'RUSORORO', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '0788601983', 'RWF', 'GAHUNZIRE Fabien', 'nyagahinga', 'kigali', 'Rwanda', '125343', '0788497985', 'http://phpintl.org/imageupload/server/php/files/DSC02397%20%281%29.JPG', 'http://phpintl.org/imageupload/server/php/files/DSC02405%20%281%29.JPG', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(31, 21, 'nyiramataro ruth', 'nyagahinga', 'kigali', 'rusororo', '', 'rusororo sacc0', NULL, NULL, NULL, NULL, NULL, NULL, '0789241114', 'RWF', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(32, 21, 'AYABAGABO Froduald', 'KABUGAII', 'KIGALI', 'RUSORORO', '', 'RUSACCO', '1197080007832074', '1970-05-24', 'MATABA', 'Married', 'male', 'SMALL TRADER', '0783866655', 'RWF', 'MUKAMANA Vestine', 'KABUGA II', 'KIGALI', 'RUSORORO', '', '0783866655', 'http://phpintl.org/imageupload/server/php/files/DSC02407.jpg', 'http://phpintl.org/imageupload/server/php/files/DSC02408.jpg', 'Farmer', 'female', 'married', 'KANOMBE', '1972-05-24', '1197270009206000', 'inActive'),
(33, 21, 'nyiramatabaro ruth', 'nyagahinga', 'kigali', 'rusororo', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '0789241114', 'RWF', '', '', '', '', '', '', 'http://phpintl.org/imageupload/server/php/files/client1.JPG', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(34, 21, 'MUKANDERA Salama', 'KABUGAI ', 'KIGALI', 'RUSORORO', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '0784259174', 'RWF', '', '', '', '', '', '', 'http://phpintl.org/imageupload/server/php/files/DSC02414.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(35, 21, 'nyirasekanabo consilde', 'nyagahinga', 'kigali', 'rusororo', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '0783873924', 'RWF', '', '', '', '', '', '', 'http://phpintl.org/imageupload/server/php/files/consilde.JPG', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(36, 21, 'NSENGIMANA Aphrodis', 'NYAGAHINGA', 'KIGALI', 'RUSORORO', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '0788508455', 'RWF', 'iINGABIRE Giselle', 'nyagahinga', 'kigali', 'RUSORORO', '', '078808455', 'http://phpintl.org/imageupload/server/php/files/DSC02417.jpg', 'http://phpintl.org/imageupload/server/php/files/DSC02418.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(37, 21, 'UKWIGIZE Jean Bosco', 'GASAGARA', 'KIGALI', 'RUSORORO', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '0785809998', 'RWF', '', '', '', '', '', '', 'http://phpintl.org/imageupload/server/php/files/DSC02419.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(38, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(39, 21, 'MUKANGANGO AGNES', 'KABUGA2', 'KIGALI', 'rusororo', '', 'ROSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '0787680522', 'RWF', '', '', '', '', '', '', 'http://phpintl.org/imageupload/server/php/files/AGNES%20%281%29.JPG', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(40, 21, 'izabiriza chantal', 'KABUGA II', 'KIGALI', 'RUSORORO', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '0783172969', 'RWF', 'KARASIRA Emmanuel', 'KABUGAII', 'KIGALI', 'RUSORORO', '', '0783172969', 'http://phpintl.org/imageupload/server/php/files/DSC02421.jpg', 'http://phpintl.org/imageupload/server/php/files/DSC02422.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(41, 21, '41', '1', '1', '', '', '1', NULL, NULL, NULL, NULL, NULL, NULL, '1', 'RWF', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'inActive'),
(42, 17, 'teszxtsdsaf', 'd', 'qs', 's', 's', 's', NULL, NULL, NULL, NULL, NULL, NULL, 's', 'USD', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(43, 17, 'tester2', '231', 'saf', 'sa', 's', 'dfsf', NULL, NULL, NULL, NULL, NULL, NULL, '12345435', 'USD', '', '', '', '', '', '', 'http://phpintl.org/imageupload/server/php/files/hoodie-800x600.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(44, 19, 'Phu Bear', '123 Rainbow Rd', 'Rochester', 'Monroe', '14621', 'Over the bridge', NULL, NULL, NULL, NULL, NULL, NULL, '5857480272', 'USD', '', '', '', '', '', '', 'http://phpintl.org/imageupload/server/php/files/100-gallon.jpeg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(45, 21, 'test', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', 'USD', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'inActive'),
(46, 11, 'test', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://phpintl.org/imageupload/server/php/files/vo.jpg', 'http://phpintl.org/imageupload/server/php/files/vo%20%281%29.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(47, 14, 'tt', 'ttt', 'tttt', 'tttt', '', 'tttt', NULL, NULL, NULL, NULL, NULL, NULL, 'ttttt', 'RWF', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(48, 14, 'jason', '1 sesame', 'rochester', 'usa', '14624', 'rochester', NULL, NULL, NULL, NULL, NULL, NULL, '1234567890', 'RWF', '', '', '', '', '', '', 'http://phpintl.org/imageupload/server/php/files/bannerforhomepage.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(49, 22, 'asdf', 'asdf', 'adfa', 'asdf', '', 'asdf', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(50, 8, 'Tiki Tiki Tembo', '123 Liberty St', 'Rochester', 'NY', '14605', 'Rochester', NULL, NULL, NULL, NULL, NULL, NULL, '5857480292', 'VND', 'Bob', '263 Central Ave', 'Rochester', 'NY', '14605', '5857121234', 'http://phpintl.org/imageupload/server/php/files/Screen%20Shot%202014-01-13%20at%201.31.13%20PM.png', 'http://phpintl.org/imageupload/server/php/files/Screen%20Shot%202014-01-13%20at%201.31.19%20PM.png', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(51, 8, 'tesdt', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', 'GHS', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(52, 21, 'tony ', '12 asdf rd', 'rochester', '', '14323', '', NULL, NULL, NULL, NULL, NULL, NULL, '5665677567', 'USD', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'inActive'),
(53, 21, 'Nancy', 'gikundo', 'Kigali', 'Rwanda', '', 'Kigali', NULL, NULL, NULL, NULL, NULL, NULL, '0259785757', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/1520777.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(54, 21, 'Alexis Fidel', 'Gikundo', 'Kigali', 'Rwanda', '14KO4', 'Kigali', NULL, NULL, NULL, NULL, NULL, NULL, '0257898520', 'USD', 'Robert Fidel', 'Gikundo', 'Kigali', 'Rwanda', '14KO4', '02585287989', 'http://www.phpintl.org/imageupload/server/php/files/1520777%20%281%29.jpg', 'http://www.phpintl.org/imageupload/server/php/files/images%20%282%29.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(55, 15, 'UMWALI MURORUNKWERE Joselyne', 'KIGALI-RWANDA', 'KIGALI', 'CENTRAL', '4953', '', NULL, NULL, NULL, NULL, NULL, NULL, '+250788250163', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/UMWALI.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(56, 21, 'CYARUZIMA Felrcite', 'RUSORORO', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '0788861941', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/DSC03541.JPG', '', NULL, NULL, NULL, NULL, NULL, NULL, 'inActive'),
(57, 21, 'Kantengwa Victoire', 'RUSORORO', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '078', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/DSC03545.JPG', 'http://www.phpintl.org/imageupload/server/php/files/DSC03545%20%281%29.JPG', NULL, NULL, NULL, NULL, NULL, NULL, 'inActive'),
(58, 21, 'NTAMUKUNZI Theogene', 'GASAGARA CELL', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '0722648070', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/DSC03547.JPG', '', NULL, NULL, NULL, NULL, NULL, NULL, 'inActive'),
(59, 21, 'AYABAGABO Fordouard', 'KABUGA', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '0783866655', 'RWF', 'MUKAMANA Vestine', 'KANOMBE', 'KIGALI', 'CENTRAL', '', '0784507201', 'http://www.phpintl.org/imageupload/server/php/files/DSC03550.JPG', 'http://www.phpintl.org/imageupload/server/php/files/DSC03551.JPG', NULL, NULL, NULL, NULL, NULL, NULL, 'inActive'),
(60, 21, 'HABIYAKARE Didace', 'NYAGAHINGA', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '0788673237', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/DSC03543.JPG', '', NULL, NULL, NULL, NULL, NULL, NULL, 'inActive'),
(61, 21, 'BATSINZI Bernard', 'GASHARARA', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '0783101568', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/DSC03548.JPG', '', NULL, NULL, NULL, NULL, NULL, NULL, 'inActive'),
(62, 21, 'MUKAKABANO Annonciata', 'KABUGA II', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '0784531526', 'RWF', 'KAYIBANDA Damas', 'KABUGA II', 'KIGALI', 'EAST', '', '0784531526', 'http://www.phpintl.org/imageupload/server/php/files/DSC03552.JPG', 'http://www.phpintl.org/imageupload/server/php/files/DSC03553.JPG', NULL, NULL, NULL, NULL, NULL, NULL, 'inActive'),
(63, 21, 'HABIYAKARE Didas', 'NYAGAHINGA', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', '11998580059527060', '1985-05-24', 'GASABO', 'Single', 'male', 'Civil Servant', '0788673237', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/DSC03543%20%283%29.JPG', '', '', '', '', '', '0000-00-00', '', 'Complete'),
(64, 21, 'NTAMUKUNZI Theogene', 'GASAGARA', 'KIGALI', 'EAST', '', '', '1198380012700069', '1983-05-24', 'GASAGARA', 'Single', 'male', 'SMALL TRADER', '0722648070', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/DSC03547%20%282%29.JPG', '', '', '', '', '', '0000-00-00', '', 'Complete'),
(65, 21, 'KANTENGWA Victoire', 'Nyagahinga', 'Kigali', 'EAST', '', 'RUSORORO SACCO', '1197770010097057', '1977-05-24', 'NYAGAHINGA', 'Single', 'female', 'Farmer', '0725208641', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/DSC03545%20%283%29.JPG', '', '', '', '', '', '0000-00-00', '', 'Complete'),
(66, 21, 'KANTENGWA Victoire', 'Nyagahinga', 'Kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/DSC03545%20%283%29.JPG', '', NULL, NULL, NULL, NULL, NULL, NULL, 'inActive'),
(67, 21, 'NTAGUNGIRA Felicite', 'NYAGAHINGA', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', '1195270087297051', '2014-05-24', 'KABUGA I', 'MARRIED', 'female', 'Farmer', '0788861941', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/DSC03542.JPG', '', '', '', '', '', '0000-00-00', '', 'Complete'),
(68, 21, 'AYABAGABO Frodouard', 'KABUGA II', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '0783866655', 'RWF', 'MUKAMANA Vestine', 'KANOMBE', 'KIGALI', 'CENTRAL', '', '0783866655', 'http://www.phpintl.org/imageupload/server/php/files/DSC03550%20%282%29.JPG', 'http://www.phpintl.org/imageupload/server/php/files/DSC03551%20%281%29.JPG', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(69, 21, 'BATSINZI Bernard', 'GISHARARA', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', '11978580059327091', '1978-03-05', 'GISHARARA', 'Married', 'male', 'policeman', '0783435320', 'RWF', 'UWAMAHORO Florence', 'GISHARARA', 'KIGALI', 'EAST', '', '0722645479', 'http://www.phpintl.org/imageupload/server/php/files/DSC03548%20%282%29.JPG', 'http://www.phpintl.org/imageupload/server/php/files/DSC03549.JPG', 'small trader', 'female', 'married', 'KABUGA', '1981-05-25', '1198180065432875', 'Complete'),
(70, 21, 'MBONIGABA Caritas', 'KABUGA II', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', '1197470008263067', '1974-05-24', 'MBANDAZI', 'Married', 'female', 'Farmer', '0788454552', 'RWF', 'KAYIBANDA Damas', 'KABUGA II', 'KIGALI', 'EAST', '', '0788841832', 'http://www.phpintl.org/imageupload/server/php/files/DSC03552%20%281%29.JPG', 'http://www.phpintl.org/imageupload/server/php/files/DSC03553%20%282%29.JPG', 'Farmer', 'male', 'married', 'RUSORORO', '1962-05-24', '1196280003898058', 'Complete'),
(71, 21, 'mary ', '123df dsf ', 'kigali', 'west', '123435', 'kigali', NULL, NULL, NULL, NULL, NULL, NULL, '65465247', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/Picture1.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(72, 21, 'mukiza yvonne', 'kanombec', 'Kigali', 'east', '', 'rusororo sacco', NULL, NULL, NULL, NULL, NULL, NULL, '021545', 'RWF', 'bosco', 'kanombe', 'Kigali', 'east', '', '0124858', 'http://www.phpintl.org/imageupload/server/php/files/Picture1%20%281%29.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(73, 21, 'UMUHOZA  Ange', 'mbandazi', 'Kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '0124', 'RWF', 'MUGABO Jean', 'mbandazi', 'Kigali', 'east', '', '', 'http://www.phpintl.org/imageupload/server/php/files/Picture1%20%282%29.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(74, 21, 'BIGIRIMANA theoneste', 'nyagahinga', 'kigali', 'east', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'BANKUNDIYE Rachel', 'nyagahinga', 'kigali', 'east', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot2.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(75, 21, 'NARIHAMIJE Ignace', 'nyagahinga', 'kigali', 'east', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'NIYIGENA  Geremene', 'nyagahinga', 'kigali', 'east', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot4.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot5.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(76, 21, 'NARIHAMIJE Ignace', 'nyagahinga', 'kigali', 'east', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'NIYIGENA  Geremene', 'nyagahinga', 'kigali', 'east', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot4.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot5.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(77, 21, 'NARIHAMIJE Ignace', 'nyagahinga', 'kigali', 'east', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'NIYIGENA  Geremene', 'nyagahinga', 'kigali', 'east', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot4.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot5.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(78, 21, 'NIYONSHUTI KALINIJABO J.BOSCO', 'nyagahinga', 'kigali', 'east', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'USD', 'GATETE Rihard', 'nyagahinga', 'kigali', 'east', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot7.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot8.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(79, 21, 'MUKANKUSI  Francne', 'nyagahinga', 'kigali', 'east', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '0783235107', 'RWF', 'NIYITEGEKA Marlene', 'nyagahinga', 'kigali', 'east', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot9.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot10.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(80, 21, 'GAHIZI Dieu Donne', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'USD', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot12.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot13.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(81, 21, 'UWIRINGIYE Marthe', 'kabugaII', 'kigali', 'east', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'KANOBANA boniface', 'kabugaII', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot14.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot15.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(82, 21, 'GAHAMANYI  Francois', 'kabugaII', 'kigali', 'east', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot17.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(83, 21, 'GAHAMANYI  Francois', 'kabugaII', 'kigali', 'east', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot17.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(84, 21, 'GAHAMANYI  Francois', 'kabugaII', 'kigali', 'east', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot17.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(85, 21, 'RUSHEMA Leonard', 'kabugaI', 'kigali', 'east', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot18.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(86, 21, 'KUBWIMANA Dancille', 'kabugaII', 'kigali', 'east', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'IYAKAREMYE jean claude', 'kabugaII', 'kigali', 'east', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot19.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot20.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(87, 21, 'GAHAMANYI jean marie vianney', 'kabugaII', 'kigali', 'east', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MUKAMFIZI  victoire', 'kabugaII', 'kigali', 'east', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot21.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot22.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(88, 21, 'NGIRUWONSANGA theoneste', 'nyagahinga', 'kigali', 'east', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'UWIBUKABABYEYI vedaste', 'nyagahinga', 'kigali', 'east', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot23.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot24.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(89, 21, 'NIYONZIMA Elie', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MUKANTAGWERA Esprance', 'kabugaII', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot25.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot26.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(90, 21, 'TWAGIRAYEZU Deogratias', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'USD', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot27.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(91, 21, 'KAMANZI Gasard', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'HITIYAREMYE Theoneste', 'mbandazi', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot28.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot29.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(92, 21, 'NIRAGIRE Theophile', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot30.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(93, 21, 'HABAKURORA Francois', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MUKAMURIGO Danclle', 'nyagahinga', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot31.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot32.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(94, 21, 'GAHIMA Eric', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot33.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(95, 21, 'RUDAKUBANA Auguste', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot34.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(96, 21, 'UWAMARIYA Agnes', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot36.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(97, 21, 'KAMBALI Auguste', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '0788615175', 'RWF', 'NYIRAMINANI  Goudiose', 'kabugaI', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot37.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot38.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(98, 21, 'MUTBAGIRANA Phinia', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot40.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(99, 21, 'MUGIRANEZA  EMMANUE', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot41.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(100, 21, 'MANIRAFASHA jean Bosco', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '0788584414', 'RWF', 'MUKIZA yvonne', 'kabugaI', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot42.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot43.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(101, 21, 'RUMINA Emmanuel', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MUKANDAYISENGA Claudine', 'kabugaI', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot44.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot45.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(102, 21, 'KABAGANWA Alice', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '0788644973', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot46.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(103, 21, 'NGABONZIZA Theoneste', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot47.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(104, 21, 'MWIZERWA AimeEmanuel', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot48.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(105, 21, 'MUKANDORI Clementine', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'TWAGIRIMANA Xavier', 'nyagahinga', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot49.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot50.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(106, 21, 'MUKAYIRANGA Joys', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MUHONGAYIRE Adeline', 'nyagahinga', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot51.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot52.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(107, 21, 'NDUWAYEZU Emerithe', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '0783141887', 'RWF', 'NYIRAMBARUSHIMANI    ', 'NYAGAHINGA', 'KIGALI', 'EAST', '', '078563112', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot54.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(108, 21, 'KARANGANWA Emmanul', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MUKARUTAMU  Josephine', 'kabugaI', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot57.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot58.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(109, 21, 'MUKANKWAYA Xavera', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'UWAMAHORO Victoire', 'nyagahinga', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot59.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot60.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(110, 21, 'MUKARUGWIZA Penine', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'UWASE jeanne', 'nyagahinga', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot61.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot62.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(111, 21, 'BIZIYAREMYE  Elie', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot63.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(112, 21, 'UMURISA  Signorine', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot64.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(113, 21, 'GAHMA Slas', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot65.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(114, 21, 'MUSABWASONI marie claire', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot67.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(115, 21, 'GATERETSE Gaspard', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot68.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(116, 21, 'MBONIMPA Esperance', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot69.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(117, 21, 'UWIMANA  Benilde', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot70.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(118, 21, 'TWIRINGIYIMANA Enock', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot71.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(119, 21, 'MUKAKIZIMA Agnes', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'NYARUKUNDO jeanne', 'nyagahinga', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot72.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot73.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(120, 21, 'MUKASEKURU Liliane', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'GAKURU Joseph', 'kabugaII', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot75%20%281%29.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot77.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(121, 21, 'NGAMWA Perpetue', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MUNYANA Alice', 'NYAGAHINGA', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot78.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot79.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(122, 21, 'MUKAMUDENGE Liberathe', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '0783180801', 'RWF', 'GATEBUKA Gaspard', 'NYAGAHINGA', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot80.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot81.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(123, 21, 'BYIHORERE Pierre', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MUREKATETE Chrisine', 'NYAGAHINGA', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot82.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot83.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(124, 21, 'KAMASA Pierre Celestin', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'NYIRARWAKA Diane', 'kabugaI', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot84.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot85.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(125, 21, 'KABALISA Jean marie vianney', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '0788562182', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot86.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(126, 21, 'MUKANTARINDWA Julienne', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MUKANDORI  olive', 'mbandazi', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot87.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot88.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(127, 21, 'KABENDEGERI  Emmanuel', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot90.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(128, 21, 'NZABORANKIRA Fidelite', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'BYIBIRO  Livine', 'kabugaII', 'k', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot91.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot92.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(129, 21, 'BAGWANEZA  Laurent', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot94.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(130, 21, 'MWIZERWA  jean paul', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MUKARUKUNDO Christine', 'NYAGAHINGA', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot95.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot96.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(131, 21, 'ABARIKUMWE  Ceredi', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MUKAKIZIMA Agnes', 'NYAGAHINGA', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot97.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot98.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(132, 21, 'UWAMARIYA Lucie', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'NYIRAMANA FATUMA', 'kabugaI', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot99.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot100.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(133, 21, 'NGABO  Richard', 'RUHANGA', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot102.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(134, 21, 'UWAMWEZI  Libelatha', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot103.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(135, 21, 'GATETE Pascal', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MUREBWAYIRE Monique', 'kabugaII', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot104.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot105.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(136, 21, 'BIDAHOMBA Antoine', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'NAGAHWEJE Donatha', 'NYAGAHINGA', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot106.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot107.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(137, 21, 'UWIMANA Florance', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MABANO Igole', 'kabugaII', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot108.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot109.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(138, 21, 'kabasimba  charlotte', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot110.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(139, 21, 'MUKANKUSI DIdacienne', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'TABARO jeande DIEU', 'mbandazi', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot111.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot112.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(140, 21, 'BAVAKURE Benjamin', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MUKANSANGA  Francine', 'mbandazi', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot113.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot114.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(141, 21, 'NIYIBIZI  Assia', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MUNEZERO  Yizid', 'mbandazi', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot115.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot116.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(142, 21, 'NIYIBIZI  Assia', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'USD', 'MUNEZERO  yazid', 'mbandazi', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot115.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot116.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(143, 21, 'UWITONZE  Shakira', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot117.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(144, 21, 'NYIRADADARI Appolinarie', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'INGABIRE Anngelique', 'kabugaII', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot118.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot119.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(145, 21, 'UWAMUTIMA Marie', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot120.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(146, 21, 'KARANGWA Uziel', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot121.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(147, 21, 'NTEZIRYAYO  Emmanuel', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot122.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(148, 21, 'NTIGASHIRA Faustin', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MUTUYIMANA Josephine', 'kabugaII', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot123.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot124.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(149, 21, 'nzanana j Baptiste', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'UWITONZE Alphonsine', 'nyagahinga', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot125%20%281%29.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot126.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(150, 21, 'MUKAKAYUMBA Madaline', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'NSENGIMANA  ISMAIL', 'mbandazi', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot127.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot128.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(151, 21, 'REDEMPTA nema mutagata', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'KAMARIZA salima', 'kabugaI', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot129.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot130.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(152, 21, 'MUKANGAMIJE olive', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot131.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(153, 21, 'KAMANA alphonse', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot132.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(154, 21, 'NYIRABAHASHYI  alphonsine', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'AKIMANA', 'mbandazi', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot134.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot135.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(155, 21, 'MUKANKUNDIYE petronire', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'USD', 'NSHYIZIRUNGU  Clementine', 'mbandazi', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot136.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot137.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(156, 21, 'MUKAKARASI Adela', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'KABAKURANYE janvier', 'mbandazi', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot138.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot139.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete');
INSERT INTO `clients` (`id`, `organization_id`, `name`, `address`, `city`, `region`, `postalcode`, `collectionlocation`, `gov_id`, `birthdate`, `birthplace`, `martial_status`, `gender`, `job`, `cell`, `currency`, `coname`, `coaddress`, `cocity`, `coregion`, `copostalcode`, `cocell`, `pics`, `copics`, `cojob`, `cogender`, `comartial_status`, `cobirthplace`, `cobirthdate`, `cogov_id`, `status`) VALUES
(157, 21, 'TWAGIRAYEZU papiace', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'NYIRANGAYABANZI madlene', 'mbandazi', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot140.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot141.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(158, 21, 'RWIZANGOGA  wellals', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'UWUMUTIMA  marie', 'kabugaII', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot142.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot143.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(159, 21, 'MUKAKABERA Pascasie', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot144.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(160, 21, 'GASHUMBA  Flodourd', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', 'mbandazi', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot145.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot146.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(161, 21, 'SEMAKAMBA  David', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot147.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(162, 21, 'MBONYINSHUTI fidele', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'NIYONSUTI alexia', 'kabugaII', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot148.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot149.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(163, 21, 'MUKASHEMA monique', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MUTESI Gorethe', 'NYAGAHINGA', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot150.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot151.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(164, 21, 'UWIMANA Eugene', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot152.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(165, 21, 'HAVUGIMANA  jean marie  vianney', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'UWAMAHORO christine', 'kabugaII', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot153.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot154.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(166, 21, 'HATANGIMANA   theoneste', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot155.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(167, 21, 'BARAGERAGEZA Gaspard', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot156.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(168, 21, 'HAVUGIMANA Aimable', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot157.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(169, 21, 'MUKANKIKO stephanie', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'USD', 'NIKUZE yvonne', 'mbandazi', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot158.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot159.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(170, 21, 'NYIRABATESI  Patricie', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'UMUTONI Angelique', 'mbandazi', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot160%20%281%29.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot161%20%281%29.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(171, 21, 'MUKANDOLI Immacule', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'NDATIMANA J claude', 'mbandazi', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot162.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot163.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(172, 21, 'UWAMAHORO J claude', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot167.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(173, 21, 'KABARENZI ', 'NYARUGUNGA', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot164.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(174, 21, 'NDAYISHIMIYE Philimin', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MWISENEZA Bligitte', 'kabugaI', '', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot165.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot166.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(175, 21, 'GAKWAYA Emmanuel', 'NYARUGUNGA', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot168.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(176, 21, 'HABYARIMANA j claude', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot169.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(177, 21, 'NARUKUNDO  Jeanne', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'GISARO john', 'NYAGAHINGA', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot170.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot171.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(178, 21, 'NTIRIVAMUNDA   thomas', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'BARACYEKWA  Helene', 'mbandazi', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot172.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot173.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(179, 21, 'NDAGIJIMANA  Felicien', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot174.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(180, 21, 'GASARASI Janvier', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'USD', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot175.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(181, 21, 'MUKASHEMA  Esperance', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '0785633380', 'RWF', 'MUKABAGIRE  Cansilee', 'kabugaI', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot177.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot178.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(182, 21, 'UWIMANA Alphonsine', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'RUSEZERA Cyprien', 'kabugaII', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot180.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot181.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(183, 21, 'NDEREYIMANA  Francoise', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot182.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(184, 21, 'UWIZEYIMANA  Thierry', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'KARANGWA Enock', 'kabugaII', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot183.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot184.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(185, 21, 'MUKAMUKARA  Patricie', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'USD', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot185.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(186, 21, 'MUHORAKEYE hirwa saphine', 'GAKO', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot186.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(187, 21, 'BAZIZANE  Fina', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'USD', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot187.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(188, 21, 'MUKARUSINE  Callitine', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'RWAMIHIGO francois', 'kabugaII', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot188.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot189.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(189, 21, 'KANYANA   Claire', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'NIYONKURU  patrick', 'kabugaI', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot190.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot191.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(190, 21, 'KANAMUGIRE  Gerard', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'NTIVUGURUZWA  Dieudonne', 'kabugaII', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot192.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot193.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(191, 21, 'MUKANYONGA Gerturde', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot194.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(192, 21, 'UMUTONIWASE   Lyliose', 'kayonza', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'BYIRINGIRO   Esdrace', 'kkayonza', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot195.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot196.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(193, 21, 'MUKARUSANGA  Jeanne', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'NIYIGENA Emmanuel', 'kabugaII', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot197.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot198.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(194, 21, 'MUKAREMERA Venancie', 'RUHANGA', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'NGORORABANGA Dieudonne', 'RUHANGA', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot199.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot200.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(195, 21, 'KAGOYIRE  Liberathe', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot201.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(196, 21, 'MUKANKUBANA  Emerthe', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot202.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(197, 21, 'MUKAMURAMBA Odette', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot203.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(198, 21, 'MUKAMURENZI  Judith', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot204.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(199, 21, 'MWISENEZA  Vincent', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot205.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(200, 21, 'MUKANYANGEZI  Selaphine', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MUKAKAYUMBA  Nadine', 'mbandazi', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot206.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot207.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(201, 21, 'NGABOYAMAHINA  Musa', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot208.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(202, 21, 'MUKANDEKEZI     Marie', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'NGARAMBE  Anatanael', 'kabugaII', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot209.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot260%20%281%29.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(203, 21, 'MUTSINDASHYAKA Theoneste', 'BISENGA', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MUNYANEZA janvier', 'BISENGA', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot211.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot212.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(204, 21, 'UWIMPAYE  Aime serge', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'USD', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot213.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(205, 21, 'COOPERATIVE  Abakoranabushake', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'ndayishimiye  philimin', 'kabugaI', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot214%20%281%29.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot215.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(206, 21, 'KOKUSIMA Joyce', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot216.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(207, 21, 'RWAKAGEYO  Francois', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'KANKWANZI  Velenna', 'kabugaI', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot217.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot218.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(208, 21, 'NDARIBITSE  Modeste', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MUKANIYONSENGA   Fabiora', 'mbandazi', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot219.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot220.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(209, 21, 'MURANGIRA Celestin', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MUKABUTERA Domina', 'kabugaII', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot221.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot222.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(210, 21, 'UWIMANA  Valentine', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot223.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(211, 21, 'MUSABYEMARIYA  Byemuje', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot224.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(212, 21, 'MUKAMUSONI  Aisha', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot225.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(213, 21, 'KAREMANGINGO Innocent', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'UMUTESI  Emmanella', 'nyagahinga', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot226.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot227.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(214, 21, 'RUDAHANGARWA  Cyrille', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MUKAKALISA  Joselyae', 'nyagahinga', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot228.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot229.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(215, 21, 'BYE  BYE  Nyakatsi', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'KARAKE  FRANCOIS', 'nyagahinga', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot230.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot231.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(216, 21, 'BYE  BYE  Nyakatsi', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'KARAKE  FRANCOIS', 'nyagahinga', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot230.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot231.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(217, 21, 'MUSOMAYIRE  Belancille', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'RWIGEMA  Honore', 'kabugaI', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot232.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot233.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(218, 21, 'NTAGANIRA  Valens', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot234.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(219, 21, 'MUKASHEMA  Esperance', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'UWITONZE   Marie  Claire', 'nyagahinga', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot235.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot236.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(220, 21, 'NDANGA Jean Bosco', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MURWANASHYAKA  Ferdinand', 'kabugaII', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot237.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot238.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(221, 21, 'SHYAKA Innocent', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MUKANDAGARA  Alphonsine', 'mbandazi', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot239.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot240.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(222, 21, 'BIMENYIMANA   Mathieu', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot241.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(223, 21, 'NYIRAHABIMANA   Jeannette', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MUSAFIRI  Deo', 'nyagahinga', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot242.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot243.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(224, 21, 'MUKAMUSONI  Grace', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'UMUTESI  MWIZA', 'kabugaII', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot244.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot245.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(225, 21, 'RWANDANGA  Boniface', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'NOK', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot246.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(226, 21, 'TWAGIRUMUKIZA  Kassim', 'mbandazi', '', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot247.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(227, 21, 'MUKAKIZIMA  Judith', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot248.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(228, 21, 'BARAMBA   Gad', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot249.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(229, 21, 'KARANGWA  Theoneste', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot250.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(230, 21, 'KAMAGAJU  Margueritte', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'UMUGWANEZA  Calne', 'nyagahinga', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot251.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot253.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(231, 21, 'KARASISI  Erneste', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MUKAKAYONGA  Eugenie', 'kabugaII', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot254.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot255.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(232, 21, 'DUSABE Prisca', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot257.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(233, 21, 'MADAM  Venantie', 'mbandazi', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot258.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(234, 21, 'KIGENZA Erneste', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'USD', 'MUKARUZIGA  Jacqueline', 'nyagahinga', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot259.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot261.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(235, 21, 'KAMANZI  Celestin', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'USD', 'KAMAGAJU  Margaritte', 'nyagahinga', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot262.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot263.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(236, 21, 'RWAMANYWA   Styveen', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot265.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(237, 21, 'NDAGIJIMANA  Faustin', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot266.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(238, 21, 'RWAGITARE  Daniel', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot268.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(239, 21, 'MUSABYEMARIYA  Ninette', 'kabugaI', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot269.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(240, 21, 'NZABAMWITA  Salatie', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'kanzayire', 'kabugaII', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot270.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot271.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(241, 21, 'MUZIBAYIRE  Cricencia', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'KAYITESI Ange', 'nyagahinga', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot272.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot273.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(242, 21, 'BARASA Thadee', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'NDIZEYE  Baraza', 'kabugaII', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot275.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot276.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(243, 21, 'RUVEBANA  Alphonse', 'kabugaII', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot277.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(244, 21, 'MUHAWENIMANA   Safari', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', 'MUGABEKAZI  Christine', 'nyagahinga', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot278.jpg', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot279.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(245, 21, 'UMURERWA  Donavine', 'nyagahinga', 'kigali', 'EAST', '', 'RUSORORO SACCO', NULL, NULL, NULL, NULL, NULL, NULL, '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/My%20Snapshot286.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(246, 14, 'Alexis Fidel', '', 'Kigali', 'Rwanda', '14K04', 'Rusororo ', NULL, NULL, NULL, NULL, NULL, NULL, '0250786541123', 'RWF', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(247, 21, 'Tony Nguyen', '71 springbrook cir', 'rochester', 'NY', '14324', 'Rusororo Sacco', NULL, NULL, NULL, NULL, NULL, NULL, '5857481480', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/Picture5.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(248, 21, 'MUGABO  Emmanuel', 'NYAGAHINGA104', 'Kigali', 'EAST', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '104', 'RWF', 'UWERA  Rutagomya  Anne', 'NYAGAHINGA', 'Kigali', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/Picture19.jpg', 'http://www.phpintl.org/imageupload/server/php/files/Picture18.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(249, 21, 'RYUMUGABE  Ferdinand', 'KABUGAII', 'Kigali', 'EAST', '', 'Rusororo Sacco', NULL, NULL, NULL, NULL, NULL, NULL, '0728804926', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/Picture25.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Complete'),
(250, 24, 'Paulina Ayitigabe', 'Box GP 18169', 'Accra', 'Ghana', '18169', 'Ghana', '', '1991-04-04', 'Sandema, Ghana', 'Single', 'female', 'Student', '', 'GHS', 'David Wilder-Laryea', 'Box GP 18169', 'Accra', 'Ghana', '18169', '0267866388', 'http://www.phpintl.org/imageupload/server/php/files/Snapshot_20140429_4.JPG', '', 'Loan Officer', 'male', 'Single', 'Cape Coast, Ghana', '1984-08-05', '', 'Complete'),
(251, 14, 'Paulina Ayitigabe', '', '', '', '', '', '', '1991-04-04', 'Sandema, Ghana', 'Single', 'female', '', '', 'RWF', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', 'Complete'),
(252, 25, 'Ama Serwaa', 'Konongo', 'Konongo', 'Ashanti', '', 'Konongo', '', '0000-00-00', '', 'Married', 'female', 'Trader', '', 'GHS', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/DSC05304.JPG', '', '', '', '', '', '0000-00-00', '', 'Complete'),
(253, 24, 'Martin Ofori', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box 18169', 'Location of Collection', 'National ID', '1991-04-14', 'Bibiani, Ghana', 'Single', 'male', 'Student', 'Mobile Phone', 'GHS', 'David Wilder', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box 18169', '0267866388', 'http://www.phpintl.org/imageupload/server/php/files/Martin%20Ofori%20%281%29.jpg', '', 'Literacy Program Officer', 'male', 'Single', 'Cape Coast', '1984-08-05', 'National ID', 'Complete'),
(254, 24, 'Augustine Adehenu', '', '', '', '', '', '', '1993-01-06', '', '', '', '', '', 'GHS', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/Augustine%20Adehenu.jpg', '', '', '', '', '', '0000-00-00', '', 'Complete'),
(255, 24, 'Augustine Adehenu', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box 18169', '', '', '1993-01-06', 'Likpe Bala, V/R', 'Single', 'male', 'Student', '', 'GHS', 'David Laryea', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/Augustine%20Adehenu.jpg', '', '', '', '', '', '1984-08-05', '', 'Complete'),
(256, 24, 'Augustine Adehenu', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box 18169', '', '', '1993-01-06', 'Likpe Bala, V/R', 'Single', 'male', 'Student', '', 'GHS', 'David Laryea', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box 18169', '0267866388', 'http://www.phpintl.org/imageupload/server/php/files/Augustine%20Adehenu.jpg', '', 'Literacy Program Officer', 'male', 'Single', 'Cape Coast', '1984-08-05', '', 'Complete'),
(257, 24, 'McAnthony Enchill', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box 18169', '', '', '1986-05-06', 'Saltpond', 'Single', 'male', 'Student', '', 'GHS', 'David Laryea', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box 18169', '0267866388', 'http://www.phpintl.org/imageupload/server/php/files/McAnthony%20Enchil.jpg', '', 'Literacy Program Officer', 'male', 'Single', 'Cape Coast', '1984-08-05', '', 'Complete'),
(258, 24, 'Mary Adubea Banafo', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box 18169', '', 'GRASK032A3880', '1987-01-27', 'Amasaman', 'Single', 'female', 'Student', '', 'GHS', 'David Laryea', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/Mary%20Banafo.jpg', '', '', '', '', 'Cape Coast', '1984-08-05', '', 'Complete'),
(259, 24, 'Mary Adubea Banafo', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box 18169', '', 'GRASK032A3880', '1987-01-27', 'Amasaman', 'Single', 'female', 'Student', '', 'GHS', 'David Laryea', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box 18169', '0267866388', 'http://www.phpintl.org/imageupload/server/php/files/Mary%20Banafo.jpg', '', 'Literacy Program Officer', 'male', 'Single', 'Cape Coast', '1984-08-05', '', 'Complete'),
(260, 24, 'Emmanuel Owusu', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box 18169', '', '', '1994-08-18', 'Berkwai', 'Single', 'male', 'Student', '', 'GHS', 'David Laryea', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box 18169', '0267866388', 'http://www.phpintl.org/imageupload/server/php/files/Emmanuel%20Owusu.jpg', '', 'Literacy Program Officer', 'male', 'Single', 'Cape Coast', '1984-08-05', '', 'Complete'),
(261, 24, 'Richard Benor', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box 18169', '', '', '1997-11-03', 'Lapaz', 'Single', 'male', 'Student', '', 'GHS', 'David Laryea', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box 18169', '0267866388', 'http://www.phpintl.org/imageupload/server/php/files/Richard%20Gbernor.jpg', '', 'Literacy Program Officer', 'male', 'Single', 'Cape Coast', '1984-08-05', '', 'Complete'),
(262, 24, 'Lydia Avoryi', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box 18169', '', '', '1986-09-09', 'Asademe, V/R', 'Single', 'female', 'Student', '', 'GHS', 'David Laryea', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box 18169', '0267866388', 'http://www.phpintl.org/imageupload/server/php/files/Lydia%20Avoryi.jpg', '', 'Literacy Program Officer', 'male', 'Single', '', '1984-08-05', '', 'Complete'),
(263, 24, 'Thomas Nakoja', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box 18169', '', '', '1997-04-14', 'Togo', 'Single', 'male', 'Student', '', 'GHS', 'David Laryea', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box 18169', '0267866388', 'http://www.phpintl.org/imageupload/server/php/files/Thomas%20Nakoja.jpg', '', 'Literacy Program Officer', 'male', 'Single', 'Cape Coast', '1984-08-05', '', 'Complete'),
(264, 24, 'Bernard Tademe', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box 18169', '', '', '1995-06-24', 'Kpassa', 'Single', 'male', 'Student', '', 'GHS', 'David Laryea', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/BErnard%20Tademe.jpg', 'http://www.phpintl.org/imageupload/server/php/files/my%20100.jpg', '', 'male', 'c', 'Cape Coast', '1984-08-05', '', 'Complete'),
(265, 24, 'Bernard Tademe', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box 18169', '', '', '1995-06-24', 'Kpassa', 'Single', 'male', 'Student', '', 'GHS', 'David Laryea', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box 18169', '0267866388', 'http://www.phpintl.org/imageupload/server/php/files/BErnard%20Tademe.jpg', 'http://www.phpintl.org/imageupload/server/php/files/my%20100.jpg', 'Literacy Program Officer', 'male', 'Single', 'Cape Coast', '1984-08-05', '', 'Complete'),
(266, 24, 'Francis Allotey Attaakwa', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box 18169', '', '', '1993-03-25', 'Pokuase', 'Single', 'male', 'Student', '0279747592', 'GHS', 'David Laryea', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box 18169', '0267866388', 'http://www.phpintl.org/imageupload/server/php/files/Francis%20Allotey.jpg', 'http://www.phpintl.org/imageupload/server/php/files/my%20100%20%281%29.jpg', 'Literacy Program Officer', 'male', 'Single', 'Cape Coast', '1984-08-05', '', 'Complete'),
(267, 24, 'Veronica Adjeiwaa', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box 18169', '', '', '0000-00-00', 'Sampa', 'Single', 'female', 'Student', '', 'GHS', 'David Laryea', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP18169', '0267866388', 'http://www.phpintl.org/imageupload/server/php/files/Veronica%20Adjeiwaa.jpg', 'http://www.phpintl.org/imageupload/server/php/files/my%20100%20%282%29.jpg', 'Literacy Program Officer', 'male', 'Single', 'Cape Coast', '1984-08-05', '', 'Complete'),
(268, 24, 'Grace Bediako', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box 18169', '', '', '1992-06-16', 'Dedeiman', 'Single', 'female', 'Student', '', 'GHS', 'David Laryea', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/Grace%20Bediako.jpg', 'http://www.phpintl.org/imageupload/server/php/files/my%20103.jpg', '', '', '', '', '1969-12-31', '', 'Complete'),
(269, 24, 'Grace Bediako', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box 18169', '', '', '1992-06-16', 'Dedeiman', 'Single', 'female', 'Student', '', 'GHS', 'David Laryea', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP18169', '0267866388', 'http://www.phpintl.org/imageupload/server/php/files/Grace%20Bediako.jpg', 'http://www.phpintl.org/imageupload/server/php/files/my%20103.jpg', 'Literacy Program Officer', 'male', 'Single', 'Cape Coast', '1969-12-31', '', 'Complete'),
(270, 24, 'Philemon Teye', 'Hope Training Institute', 'Accra', 'Greater Accra', '', '', '', '1995-03-31', 'Somanya', 'Single', 'male', 'Student', '', 'GHS', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/Philemon%20teye.jpg', '', '', '', '', '', '0000-00-00', '', 'Complete'),
(271, 24, 'Philemon Teye', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP 18169', '', '', '1995-03-31', 'Somanya', 'Single', 'male', 'Student', '', 'GHS', 'David Laryea', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/Philemon%20teye.jpg', 'http://www.phpintl.org/imageupload/server/php/files/my%20103%20%281%29.jpg', '', '', '', '', '1969-12-31', '', 'Complete'),
(272, 24, 'Philemon Teye', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP 18169', '', '', '1995-03-31', 'Somanya', 'Single', 'male', 'Student', '', 'GHS', 'David Laryea', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP18169', '0267866388', 'http://www.phpintl.org/imageupload/server/php/files/Philemon%20teye.jpg', 'http://www.phpintl.org/imageupload/server/php/files/my%20103%20%281%29.jpg', 'Literacy Program Officer', 'male', 'Single', 'Cape Coast', '1984-08-05', '', 'Complete'),
(273, 24, 'Felicia Attah', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP 18169', '', '4485045787', '1993-08-26', 'Hohoe', 'Single', 'female', 'Student', '', 'GHS', 'David Laryea', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP18169', '0267866388', 'http://www.phpintl.org/imageupload/server/php/files/Felicia%20Attah.jpg', 'http://www.phpintl.org/imageupload/server/php/files/my%20103%20%282%29.jpg', 'Literacy Program Officer', 'male', 'Single', 'Cape Coast', '1984-08-05', '', 'Complete'),
(274, 24, 'Clement Aworka', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP 18169', '', '', '1994-04-11', 'Accra', 'Single', 'male', 'Student', '', 'GHS', 'David Laryea', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP18169', '0267866388', 'http://www.phpintl.org/imageupload/server/php/files/Clemet%20Aworka.jpg', 'http://www.phpintl.org/imageupload/server/php/files/my%20103%20%283%29.jpg', 'Literacy Program Officer', 'male', 'Single', 'Cape Coast', '1984-08-05', '', 'Complete'),
(275, 24, 'Patience Siayor', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP 18169', '', '', '1994-04-02', 'Accra', 'Single', 'female', 'Student', '', 'GHS', 'David Laryea', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP18169', '0267866388', 'http://www.phpintl.org/imageupload/server/php/files/Patience%20Siayor.jpg', 'http://www.phpintl.org/imageupload/server/php/files/my%20103%20%284%29.jpg', 'Literacy Program Officer', 'male', 'Single', 'Cape Coast', '1984-08-05', '', 'Complete'),
(276, 24, 'Doris Assaa', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP 18169', '', '', '1993-07-21', 'Drobo', 'Single', 'female', 'Student', '', 'GHS', 'David Laryea', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP18169', '0267866388', 'http://www.phpintl.org/imageupload/server/php/files/Doris%20Assah.jpg', 'http://www.phpintl.org/imageupload/server/php/files/my%20103%20%285%29.jpg', 'Literacy Program Officer', 'male', 'Single', 'Cape Coast', '1984-08-05', '', 'Complete'),
(277, 24, 'Florence K. Oforiwaa', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP 18169', '', '', '1989-06-06', 'Kwahu', 'Single', 'female', 'Student', '', 'GHS', 'David Laryea', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP18169', '0267866388', 'http://www.phpintl.org/imageupload/server/php/files/Florence%20Oforiwaa.jpg', 'http://www.phpintl.org/imageupload/server/php/files/my%20103%20%286%29.jpg', 'Literacy Program Officer', 'male', 'Single', 'Cape Coast', '1984-08-05', '', 'Complete'),
(278, 24, 'Evelyn Moi', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP 18169', '', '', '1994-04-12', 'Dedeiman', 'Single', 'female', 'Student', '', 'GHS', 'David Laryea', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP18169', '0267866388', 'http://www.phpintl.org/imageupload/server/php/files/Evelyn%20Moi.jpg', 'http://www.phpintl.org/imageupload/server/php/files/my%20103%20%287%29.jpg', 'Literacy Program Officer', 'male', 'Single', 'Cape Coast', '1984-08-05', '', 'Complete'),
(279, 24, 'Peter Boakye', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP 18169', '', '2516011497', '1990-04-06', 'Tepa', 'Single', 'male', 'Student', '', 'GHS', 'David Laryea', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP18169', '0267866388', 'http://www.phpintl.org/imageupload/server/php/files/Peter%20Boakye.jpg', 'http://www.phpintl.org/imageupload/server/php/files/my%20103%20%288%29.jpg', 'Literacy Program Officer', 'male', 'Single', 'Cape Coast', '1984-08-05', '', 'Complete'),
(280, 24, 'Paul Amissah', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP 18169', '', '', '1993-01-07', 'Winneba', 'Single', 'male', 'Student', '0548507770', 'GHS', 'David Laryea', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP18169', '0267866388', 'http://www.phpintl.org/imageupload/server/php/files/Paul%20Amissah.jpg', 'http://www.phpintl.org/imageupload/server/php/files/my%20103%20%289%29.jpg', 'Literacy Program Officer', 'male', 'Single', 'Cape Coast', '1984-08-05', '', 'Complete'),
(281, 24, 'Dominic Eshun', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP 18169', '', '', '1992-10-24', 'Cape Coast', 'Single', 'male', 'Student', '0245279735', 'GHS', 'David Laryea', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP18169', '0267866388', 'http://www.phpintl.org/imageupload/server/php/files/Dominic%20Eshun.jpg', 'http://www.phpintl.org/imageupload/server/php/files/my%20103%20%2810%29.jpg', 'Literacy Program Officer', 'male', 'Single', 'Cape Coast', '1984-08-05', '', 'Complete'),
(282, 24, 'Francis P. K. Marworwi', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP 18169', '', '7777009981', '1993-03-06', 'Salaga', 'Single', 'male', 'Student', '', 'GHS', 'David Laryea', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP18169', '0267866388', 'http://www.phpintl.org/imageupload/server/php/files/Francis%20Marworwi.jpg', 'http://www.phpintl.org/imageupload/server/php/files/my%20103%20%2811%29.jpg', 'Literacy Program Officer', 'male', 'Single', 'Cape Coast', '1984-08-05', '', 'Complete'),
(283, 24, 'Ebenezer Acquah', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP 18169', '', '', '1993-04-12', 'Agona Asafo', 'Single', 'male', 'Student', '0232154309', 'GHS', 'David Laryea', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP18169', '0267866388', 'http://www.phpintl.org/imageupload/server/php/files/Ebenezer%20Acquah.jpg', 'http://www.phpintl.org/imageupload/server/php/files/my%20103%20%2812%29.jpg', 'Literacy Program Officer', 'male', 'Single', 'Cape Coast', '1984-08-05', '', 'Complete'),
(284, 24, 'Grace Quaintsil', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP 18169', '', '', '1995-04-04', 'Gomoah Fetteh', 'Single', 'female', 'Student', '', 'GHS', 'David Laryea', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP18169', '0267866388', 'http://www.phpintl.org/imageupload/server/php/files/Grace%20Quaintsil.jpg', 'http://www.phpintl.org/imageupload/server/php/files/my%20103%20%2813%29.jpg', 'Literacy Program Officer', 'male', 'Single', 'Cape Coast', '1984-08-05', '', 'Complete'),
(285, 24, 'Godwin Zigah', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP 18169', '', 'GRGAD464H3780', '1970-01-05', 'mAMPONG', 'Married', 'male', 'Trader', '0244431792', 'GHS', 'David Laryea', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP18169', '0267866388', '', 'http://www.phpintl.org/imageupload/server/php/files/my%20103%20%2814%29.jpg', 'Literacy Program Officer', 'male', 'Single', 'Cape Coast', '1984-08-05', '', 'Complete'),
(286, 24, 'Salomey Adu-Kwao', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP 18169', '', '6391018634', '1980-10-16', 'Kade', 'Married', 'female', 'Caterer', '0243935387', 'GHS', 'David Laryea', 'Hope Training Institute', 'Accra', 'Greater Accra', 'Box GP18169', '0267866388', '', 'http://www.phpintl.org/imageupload/server/php/files/my%20103%20%2815%29.jpg', 'Literacy Program Officer', 'male', 'Single', 'Cape Coast', '1984-08-05', '', 'Complete'),
(287, 21, 'AYABAGABO Flodouard', 'RUSORORO', 'KIGALI', 'EAST', '', 'SACCO RUSORORO', '1197080007832074', '1970-05-24', 'MATABA', 'Married', 'male', 'SMALL TRADER', '0783866655', 'RWF', 'MUKAMANA Vestine', 'RWANDA', 'KIGALI', 'EAST', '', '', 'http://www.phpintl.org/imageupload/server/php/files/DSC03550%20%283%29.JPG', 'http://www.phpintl.org/imageupload/server/php/files/DSC03551%20%282%29.JPG', 'Farmer', 'female', 'married', 'KANOMBE', '1972-05-24', '1197270009206000', 'Complete'),
(288, 27, 'John Doe', '12 me road', 'rochester', 'New York', '14450', 'United States', '123456789', '1989-03-18', 'Oakland California', 'Single', 'male', 'Director', '5857550400', 'USD', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', 'Complete'),
(289, 27, 'Alexis Fidele ', '', '', '', '', 'New York ', '789456213', '1984-06-24', 'New York', 'Married 10', 'male', 'Drug Cartel ', '5857555019', 'USD', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/Fidel-Castro_diaporama_full.jpg', '', '', '', '', '', '0000-00-00', '', 'inActive'),
(290, 27, 'Sam Smith', '', '', '', '', '', '456789123', '0000-00-00', '', 'Single ', 'male', '', '', 'USD', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/Sam%2BSmith%2BCapital%2BFM%2BJingle%2BBell%2BBall%2BDay%2BVHuVimXrXRHl.jpg', '', '', '', '', '', '0000-00-00', '', 'Complete'),
(291, 27, 'Robert Jones ', '', '', '', '', '', '654897321', '1977-12-05', 'Denver Colorado ', 'Swagger', 'male', 'Actor', '', 'USD', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/robert-downey-300.jpg', '', '', '', '', '', '0000-00-00', '', 'Complete');
INSERT INTO `clients` (`id`, `organization_id`, `name`, `address`, `city`, `region`, `postalcode`, `collectionlocation`, `gov_id`, `birthdate`, `birthplace`, `martial_status`, `gender`, `job`, `cell`, `currency`, `coname`, `coaddress`, `cocity`, `coregion`, `copostalcode`, `cocell`, `pics`, `copics`, `cojob`, `cogender`, `comartial_status`, `cobirthplace`, `cobirthdate`, `cogov_id`, `status`) VALUES
(292, 27, 'Jack Skellington', '', '', '', '', 'hell', '321654897', '1996-05-06', 'tv', 'dead', 'male', 'president of pumpkinville', '3215647898', 'USD', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/download.jpeg', '', '', '', '', '', '0000-00-00', '', 'Complete'),
(293, 27, 'Alexis Fidele', '', '', '', '', 'rochester', '8794151423', '1970-11-05', 'cuba', 'married', 'male', 'drug cartel', '', 'USD', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/Fidel-Castro_diaporama_full%20%281%29.jpg', '', '', '', '', '', '0000-00-00', '', 'Complete'),
(294, 28, 'Tony Man', '', '', '', '', 'rochester', '1234123412', '2009-02-09', 'china', 'chillen', 'male', 'boring hero', '12341234234', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/MAD-Just-Us-League-of-Stupid-Heroes-Superman.jpg', '', '', '', '', '', '0000-00-00', '', 'Complete'),
(295, 21, 'Sekamana Janvier', 'Mbandazi', 'KIGALI', 'EAST', '', 'SACCO RUSORORO', '1198380016212046', '1983-07-15', 'MBANDAZI', 'MARRIED', 'male', 'Farmer', '0783588426', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/P1120661%20%282%29.JPG', '', '', '', '', '', '0000-00-00', '', 'Complete'),
(296, 21, 'Mukankima Annonciatta', 'NYAGAHINGA', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', '1196870005936001', '1968-07-03', 'Nyagahinga', 'Married', 'female', 'Farming', '', 'RWF', 'Ntamukunzi Tharcisse', 'KIGALI RWANDA', 'KIGALI', 'EAST', '', '0787651254', 'http://www.phpintl.org/imageupload/server/php/files/Mukankima%20annonciatta.jpg', '', 'Farmer', 'male', 'Married', 'Rusororo', '1960-02-16', '1196080003505048', 'Complete'),
(297, 21, 'Sebagabo Cassien', 'RWANDA', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', '1195080001655001', '1950-01-18', 'Kamashashi', 'Married', 'male', 'Subsistence Agricuture', '0724357601', 'RWF', 'Kamuziga Belancile ', 'Rwanda', 'KIGALI', 'EAST', '', '0721032870', 'http://www.phpintl.org/imageupload/server/php/files/sebagabo.JPG', '', 'Small farmer', 'female', 'Married', 'KABUGA II', '1964-09-10', '1196470004076082 ', 'Complete'),
(298, 21, 'Ndungutse Jean Fabrice', 'NYAKAGOMBE', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', '1197180005340058', '1971-07-09', 'NYAKAGOMBE', 'Married', 'male', 'small farming', '0785397803', 'RWF', 'Mukakarangwa Providence ', 'NYAKAGOMBE', 'KIGALI', 'EAST', '', '0721041793', 'http://www.phpintl.org/imageupload/server/php/files/NDUNGUTSE.JPG', '', 'Farmer', 'female', 'Married', 'KABUGA II', '1981-12-06', '1198170015097033', 'Complete'),
(299, 21, 'Rutabana Venuste', 'BUSENGA', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', '1196680005814066', '1966-08-29', 'BISENGA', 'Married', 'male', 'Farmer', '0788983533', 'RWF', '', 'BISENGA', 'KIGALI', 'EAST', '', '0725421096', 'http://www.phpintl.org/imageupload/server/php/files/kwizera.JPG', '', 'Farmer', 'female', 'Married', 'KABUGA II', '1972-06-24', '1197280003505041', 'Complete'),
(300, 21, 'Kwizera Daniel', 'SAMUDUHA', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', '119908018644202', '1990-12-02', 'MBANDAZI', 'Single', 'male', 'Farmer', '0784558707', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/KWIZERA.JPG', '', '', '', '', '', '0000-00-00', '', 'Complete'),
(301, 21, 'Rubuga Joyce', 'NYAGAHINGA', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', '1195870004152047', '1958-10-19', 'KABUTARE', 'Married', 'female', 'Farming', '', 'RWF', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', 'Complete'),
(302, 21, 'Rubuga Joyce', 'KABUTARE', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', '1195870004152047', '1958-10-19', 'NYAGAHINGA', 'Married', 'female', 'Farming', '', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/rubuga%20joyce%20%281%29.JPG', '', '', '', '', '', '0000-00-00', '', 'Complete'),
(303, 21, 'Mukangango Agnes', 'KABUGA II', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', '1197680007561074', '1976-05-06', 'CYANAMO', 'Married', 'female', 'Small trader', '0787680522', 'RWF', 'Gakunde Eric ', 'GAHINGA II', 'KIGALI', 'EAST', '', '0726453012', 'http://www.phpintl.org/imageupload/server/php/files/mukangango.JPG', '', 'Farmer', 'male', 'Married', 'Rusororo', '1972-07-24', '1197280007561074', 'Complete'),
(304, 21, 'SEKAMANA Janvier', 'NYAGAHINGA', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', '1197380005814111', '1973-01-01', 'NYAKAGOMBE', 'Single', 'male', 'Carpenter', '07898736521', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/janvier.JPG', '', '', '', '', '', '0000-00-00', '', 'Complete'),
(305, 21, 'Mboningabo Celestin', 'NYAGAHINGA', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', '1197180005347632', '1971-06-25', 'Nyagahinga', '', 'male', 'Farmer', '0788453281', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/celestin.JPG', '', '', '', '', '', '0000-00-00', '', 'Complete'),
(306, 21, 'Kiribazayire Justine', 'NYAKAGOMBE', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', '1197480001655001', '1974-08-11', 'NYAKAGOMBE', 'Married', 'female', 'Farmer', '0788479201', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/justine.JPG', '', '', '', '', '', '0000-00-00', '', 'Complete'),
(307, 21, 'Turabumukiza Eugene', 'NYAKAGOMBE', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', '1198270004152030', '1978-03-25', 'NYAKAGOMBE', 'Single', 'male', 'Farmer', '0727625281', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/eugene.jpg', '', '', '', '', '', '0000-00-00', '', 'Complete'),
(308, 21, 'Nteziryayo Emmanuel', 'NYAGAHINGA', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', '119778000165500176', '1977-09-06', 'NYAKAGOMBE', 'Single', 'male', 'Mechanic', '0783674401', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/emmanuel.jpg', '', '', '', '', '', '0000-00-00', '', 'Complete'),
(309, 21, 'Nsengimana Ismael', 'NYAKAGOMBE', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', '1197880005814066', '1978-07-01', 'NYAKAGOMBE', 'Single', 'male', 'Technician', '0782108536', 'RWF', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', 'Complete'),
(310, 21, 'Kayibanda Damas', 'NYAKAGOMBE', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', '1198080001655984', '1980-07-01', 'NYAKAGOMBE', 'Single', 'male', 'TRADER', '0788746354', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/damas.JPG', '', '', '', '', '', '0000-00-00', '', 'Complete'),
(311, 21, 'Nkuranga Augustin', 'NYAGAHINGA', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', '1197970005936001', '1979-02-28', 'NYAGAHINGA', 'Single', 'male', 'Builder', '0729685765', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/augustin.JPG', '', '', '', '', '', '0000-00-00', '', 'Complete'),
(312, 21, 'TUYIZERE J.Damascene', 'RUSORORO', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', '119798000534005', '1979-03-24', 'KABUGA II', 'Single', 'male', 'STUDENT', '0785767097', 'RWF', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', 'inActive'),
(313, 21, 'TUYIZERE J.Damascene', 'KABUTARE', 'KIGALI', 'EAST', '', 'RUSORORO SACCO', '1196790005814066', '1967-07-01', 'NYAGAHINGA', 'Single', 'male', 'STUDENT', '0786745468', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/damascene.jpg', '', '', '', '', '', '0000-00-00', '', 'Complete'),
(314, 14, 'tommy donh', '1 THIRD ST', 'Rochester', 'ny', '14555', 'ROCHESTER', 'ny123214', '1984-11-15', 'cali', 'singlr', 'male', 'something', '343334343', 'RWF', '', '', '', '', '', '', 'http://www.phpintl.org/imageupload/server/php/files/Dock.jpg', '', '', '', '', '', '0000-00-00', '', 'Complete'),
(315, 21, 'Peter Yim', '263 Central Ave', 'Rochester', 'NY', '14605', 'Rochester', '123', '2015-08-27', 'SF', 'Single', 'male', 'Web Dev', '4152405770', 'RWF', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', 'Complete');

-- --------------------------------------------------------

--
-- Table structure for table `collateral`
--

CREATE TABLE IF NOT EXISTS `collateral` (
  `id` int(11) unsigned NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `collateral`
--

INSERT INTO `collateral` (`id`, `client_id`, `date`, `timestamp`, `amount`, `currency`, `userid`) VALUES
(23, 10, '2013-11-01', NULL, 200, 'USD', 44),
(24, 16, '2013-11-28', NULL, 20000, 'RWF', 49),
(25, 53, '2014-01-30', NULL, 100, 'USD', 58),
(26, 54, '2014-01-30', NULL, 100, 'USD', 58),
(27, 52, '2014-01-20', NULL, 68100, 'RWF', 58),
(28, 41, '2014-02-04', NULL, 100, 'USD', 58),
(29, 48, '2014-04-01', NULL, 2000, 'RWF', 49),
(30, 247, '2014-04-08', NULL, 15000, 'USD', 62),
(31, 248, '1969-12-31', NULL, 15000, 'RWF', 62);

-- --------------------------------------------------------

--
-- Table structure for table `funding`
--

CREATE TABLE IF NOT EXISTS `funding` (
  `id` int(11) NOT NULL,
  `projectid` int(11) NOT NULL,
  `orderid` int(11) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=120 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `funding`
--

INSERT INTO `funding` (`id`, `projectid`, `orderid`, `amount`) VALUES
(77, 80, 65, 500),
(76, 51, 64, 250),
(75, 52, 63, 200),
(74, 53, 63, 300),
(73, 52, 62, 300),
(72, 53, 62, 200),
(78, 51, 66, 350),
(79, 104, 67, 520),
(80, 105, 67, 520),
(81, 110, 68, 445),
(82, 50, 70, 1000),
(83, 126, 71, 600),
(85, 49, 84, 250),
(86, 48, 84, 250),
(87, 56, 87, 25),
(91, 127, 90, 500),
(92, 10, 91, 100),
(93, 128, 92, 100),
(113, 10, 97, 500),
(95, 54, 72, 25),
(96, 56, 72, 350),
(97, 71, 72, 25),
(98, 154, 94, 100),
(99, 155, 94, 250),
(100, 153, 94, 100),
(101, 156, 94, 150),
(102, 157, 94, 850),
(103, 154, 95, 200),
(104, 155, 95, 250),
(105, 153, 95, 500),
(106, 156, 95, 125),
(107, 157, 95, 125),
(108, 154, 96, 200),
(109, 155, 96, 300),
(110, 153, 96, 400),
(111, 156, 96, 125),
(112, 157, 96, 225),
(114, 40, 99, 150),
(115, 54, 99, 125),
(116, 46, 99, 150),
(117, 160, 99, 75),
(118, 146, 99, 75),
(119, 48, 102, 25);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) unsigned NOT NULL,
  `organization_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `group_photo` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `group_members`
--

CREATE TABLE IF NOT EXISTS `group_members` (
  `id` int(11) unsigned NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ipn_log`
--

CREATE TABLE IF NOT EXISTS `ipn_log` (
  `id` int(11) NOT NULL,
  `listener_name` varchar(3) DEFAULT NULL COMMENT 'Either IPN or API',
  `transaction_type` varchar(16) DEFAULT NULL COMMENT 'The type of call being made to the listener',
  `transaction_id` varchar(19) DEFAULT NULL COMMENT 'The unique transaction ID generated by PayPal',
  `status` varchar(16) DEFAULT NULL COMMENT 'The status of the call',
  `message` varchar(512) DEFAULT NULL COMMENT 'Explanation of the call status',
  `ipn_data_hash` varchar(32) DEFAULT NULL COMMENT 'MD5 hash of the IPN post data',
  `detail` text COMMENT 'Detail text (potentially JSON) on this call',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ipn_log`
--

INSERT INTO `ipn_log` (`id`, `listener_name`, `transaction_type`, `transaction_id`, `status`, `message`, `ipn_data_hash`, `detail`, `created_at`, `updated_at`) VALUES
(1, 'IPN', NULL, NULL, 'ERROR', 'No POST data and no cached IPN data', '40cd750bba9870f18aada2478b24840a', '{"ipn_data":[],"ipn_response":null}', '2013-06-11 08:45:02', '2013-06-11 08:45:02'),
(2, 'IPN', 'cache', NULL, NULL, NULL, NULL, 'a:39:{s:12:"mc_handling1";s:4:"1.67";s:13:"address_state";s:2:"CA";s:9:"quantity1";s:1:"3";s:6:"txn_id";s:9:"291398141";s:9:"last_name";s:5:"Smith";s:11:"mc_currency";s:3:"USD";s:12:"payer_status";s:8:"verified";s:14:"address_status";s:9:"confirmed";s:3:"tax";s:4:"2.02";s:7:"invoice";s:2:"18";s:14:"address_street";s:15:"123, any street";s:11:"payer_email";s:23:"buyer@paypalsandbox.com";s:9:"mc_gross1";s:5:"12.34";s:11:"mc_shipping";s:4:"3.02";s:10:"first_name";s:4:"John";s:8:"business";s:23:"support@phuconcepts.com";s:11:"verify_sign";s:56:"Aclp20XKmFScyK-mU20wwRqId4-uABaI1kPuHO.hdoG59j-nlVQLkQFh";s:8:"payer_id";s:13:"TESTBUYERID01";s:12:"payment_date";s:24:"08:08:46 11 Jun 2013 PDT";s:15:"address_country";s:13:"United States";s:14:"payment_status";s:9:"Completed";s:14:"receiver_email";s:23:"support@phuconcepts.com";s:12:"payment_type";s:7:"instant";s:11:"address_zip";s:5:"95131";s:12:"address_city";s:8:"San Jose";s:12:"mc_shipping1";s:4:"1.02";s:10:"item_name1";s:2:"54";s:8:"mc_gross";s:5:"15.34";s:12:"item_number1";s:3:"541";s:6:"mc_fee";s:4:"0.44";s:17:"residence_country";s:2:"US";s:20:"address_country_code";s:2:"US";s:14:"notify_version";s:3:"2.4";s:11:"receiver_id";s:23:"support@phuconcepts.com";s:11:"mc_handling";s:4:"2.06";s:8:"txn_type";s:4:"cart";s:6:"custom";s:3:"PHP";s:12:"address_name";s:10:"John Smith";s:8:"test_ipn";s:1:"1";}', '2013-06-11 08:45:18', '2013-06-11 10:10:22'),
(3, 'IPN', 'cart', '990405788', 'ERROR', 'Receiver email seller@paypalsandbox.com does not match merchant''s', 'bff44d7dcaaefdf00a17269ab3dc811c', '{"ipn_data":{"mc_handling1":"1.67","address_state":"CA","txn_id":"990405788","last_name":"Smith","mc_currency":"USD","payer_status":"verified","address_status":"confirmed","tax":"2.02","invoice":"abc1234","address_street":"123, any street","payer_email":"buyer@paypalsandbox.com","mc_gross1":"12.34","mc_shipping":"3.02","first_name":"John","business":"seller@paypalsandbox.com","verify_sign":"AKVXpAX441dZ69zokd3nCEl53JEQAIMUUe6WVUJRAg9fUDddsbqdslVh","payer_id":"TESTBUYERID01","payment_date":"06:43:26 11 Jun 2013 PDT","address_country":"United States","payment_status":"Completed","receiver_email":"seller@paypalsandbox.com","payment_type":"instant","address_zip":"95131","address_city":"San Jose","mc_shipping1":"1.02","item_name1":"something","mc_gross":"15.34","item_number1":"AK-1234","mc_fee":"0.44","residence_country":"US","address_country_code":"US","notify_version":"2.4","receiver_id":"seller@paypalsandbox.com","mc_handling":"2.06","txn_type":"cart","custom":"xyz123","address_name":"John Smith","test_ipn":"1"},"ipn_response":"HTTP\\/1.1 200 OK\\r\\nDate: Tue, 11 Jun 2013 13:45:18 GMT\\r\\nServer: Apache\\r\\nX-Frame-Options: SAMEORIGIN\\r\\nSet-Cookie: c9MWDuvPtT9GIMyPc3jwol1VSlO=xxlwwtnkAPrDmfm6GC2DWsRFpGUnBWDHAf19sbWYTPxODzjFW9sBYaCh-hbLyIKqrf8tYPILKwTOzjIJ9dDyaJ-scB1bjagzX5UNwCViUcYTUfOdAXxaQ_hPrf85Z7Yf3JwcZ0%7cHAFnJ9mhmbAlQPpr6SpXgzIwh-UGBPqOQvg-y7scbUeiBuZbFMbe5QinHq6QKsovevcXYW%7c3XwwQDi-lghBA9QO9jEgy8KMg5cTdfuM0rVaQUrQBu73D1pulKYeMaPmFq5s7BB0vMih-W%7c1370958318; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: cookie_check=yes; expires=Fri, 09-Jun-2023 13:45:18 GMT; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: navcmd=_notify-validate; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: navlns=0.0; expires=Mon, 06-Jun-2033 13:45:18 GMT; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: Apache=10.72.109.11.1370958318384462; path=\\/; expires=Thu, 04-Jun-43 13:45:18 GMT\\r\\nConnection: close\\r\\nSet-Cookie: X-PP-SILOVER=name%3DSANDBOX3.WEB.1%26silo_version%3D880%26app%3Dslingshot%26TIME%3D3995711313; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: Apache=10.72.128.11.1370958318375974; path=\\/; expires=Thu, 04-Jun-43 13:45:18 GMT\\r\\nVary: Accept-Encoding\\r\\nStrict-Transport-Security: max-age=14400\\r\\nTransfer-Encoding: chunked\\r\\nContent-Type: text\\/html; charset=UTF-8\\r\\n\\r\\n8\\r\\nVERIFIED\\r\\n0\\r\\n\\r\\n"}', '2013-06-11 08:45:18', '2013-06-11 08:45:20'),
(4, 'IPN', 'cart', '212915134', 'SUCCESS', 'Parsing, authentication and validation complete', '0bc22c2fa97c372f73671ff08199428c', '{"ipn_data":{"mc_handling1":"1.67","address_state":"CA","txn_id":"212915134","last_name":"Smith","mc_currency":"USD","payer_status":"verified","address_status":"confirmed","tax":"2.02","invoice":"abc1234","address_street":"123, any street","payer_email":"buyer@paypalsandbox.com","mc_gross1":"12.34","mc_shipping":"3.02","first_name":"John","business":"support@phuconcepts.com","verify_sign":"A24nwNvp1GPWAd3LS0v2O.5jNy6EAdQFirl3QkzO91fHgVHDQQMFX2oF","payer_id":"TESTBUYERID01","payment_date":"06:45:16 11 Jun 2013 PDT","address_country":"United States","payment_status":"Completed","receiver_email":"support@phuconcepts.com","payment_type":"instant","address_zip":"95131","address_city":"San Jose","mc_shipping1":"1.02","item_name1":"something","mc_gross":"15.34","item_number1":"AK-1234","mc_fee":"0.44","residence_country":"US","address_country_code":"US","notify_version":"2.4","receiver_id":"support@phuconcepts.com","mc_handling":"2.06","txn_type":"cart","custom":"xyz123","address_name":"John Smith","test_ipn":"1"},"ipn_response":"HTTP\\/1.1 200 OK\\r\\nDate: Tue, 11 Jun 2013 13:46:42 GMT\\r\\nServer: Apache\\r\\nX-Frame-Options: SAMEORIGIN\\r\\nSet-Cookie: c9MWDuvPtT9GIMyPc3jwol1VSlO=t_M0zy37-pbgnXiualwgkOhwh3UVtyLTvdiy84MDuICiwNyUecO_ibF5s3W1L1RLUCOByvSqWL9LJHbmbhOaj8P8kBy51rFhw3GIF6ufCS7xW9Aq-6a9DTTQoT8TsorOU5lPcG%7cknZbo0CdwkkA3CkNAAnK6Enxx1PUsprvAAeUo0ZHLarRp_tZ2jfp6wF5PVuC7_xt2sL8QG%7cC2F_0helaLB6sLX3gWGNijw-iKYo57weUgWulVxP-Ypb41tDtLy3aPmpo2pWzfNnwUi_X0%7c1370958402; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: cookie_check=yes; expires=Fri, 09-Jun-2023 13:46:42 GMT; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: navcmd=_notify-validate; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: navlns=0.0; expires=Mon, 06-Jun-2033 13:46:42 GMT; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: Apache=10.72.109.11.1370958402336113; path=\\/; expires=Thu, 04-Jun-43 13:46:42 GMT\\r\\nConnection: close\\r\\nSet-Cookie: X-PP-SILOVER=name%3DSANDBOX3.WEB.1%26silo_version%3D880%26app%3Dslingshot%26TIME%3D1110095697; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: Apache=10.72.128.11.1370958402327259; path=\\/; expires=Thu, 04-Jun-43 13:46:42 GMT\\r\\nVary: Accept-Encoding\\r\\nStrict-Transport-Security: max-age=14400\\r\\nSet-Cookie: X-PP-SILOVER=name%3DSANDBOX3.WEB.1%26silo_version%3D880%26app%3Dslingshot%26TIME%3D1110095697; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: Apache=10.72.128.11.1370958402316314; path=\\/; expires=Thu, 04-Jun-43 13:46:42 GMT\\r\\nStrict-Transport-Security: max-age=14400\\r\\nTransfer-Encoding: chunked\\r\\nContent-Type: text\\/html; charset=UTF-8\\r\\n\\r\\n8\\r\\nVERIFIED\\r\\n0\\r\\n\\r\\n"}', '2013-06-11 08:46:42', '2013-06-11 08:46:44'),
(7, 'IPN', 'cart', '632748367', 'ERROR', 'Receiver email seller@paypalsandbox.com does not match merchant''s', '206e18deb4e3beacb4ac7e42c752d617', '{"ipn_data":{"mc_handling1":"1.67","address_state":"CA","txn_id":"632748367","last_name":"Smith","mc_currency":"USD","payer_status":"verified","address_status":"confirmed","tax":"2.02","invoice":"19","address_street":"123, any street","payer_email":"buyer@paypalsandbox.com","mc_gross1":"12.34","mc_shipping":"3.02","first_name":"John","business":"seller@paypalsandbox.com","verify_sign":"AEudaBy0OzTT-FSPZxPgDKVRJ-XKA9VBnmetSgKWQwBzlymmoEhWWc4W","payer_id":"TESTBUYERID01","payment_date":"06:49:32 11 Jun 2013 PDT","address_country":"United States","payment_status":"Completed","receiver_email":"seller@paypalsandbox.com","payment_type":"instant","address_zip":"95131","address_city":"San Jose","mc_shipping1":"1.02","item_name1":"something","mc_gross":"15.34","item_number1":"AK-1234","mc_fee":"0.44","residence_country":"US","address_country_code":"US","notify_version":"2.4","receiver_id":"seller@paypalsandbox.com","mc_handling":"2.06","txn_type":"cart","custom":"xyz123","address_name":"John Smith","test_ipn":"1"},"ipn_response":"HTTP\\/1.1 200 OK\\r\\nDate: Tue, 11 Jun 2013 14:56:13 GMT\\r\\nServer: Apache\\r\\nX-Frame-Options: SAMEORIGIN\\r\\nSet-Cookie: c9MWDuvPtT9GIMyPc3jwol1VSlO=OZwcVTRF1TqZKdfM2pdxMqQxxUhzQClysly2eeeSlcUnPkIYyE7FSLTwHoMmkbe-7pNvwOMrnVfCXP9JedTIXab3mFm_v_h86dWs5sC5ncm2ALbYRIqC7btAbbRvqq3gYvN7TW%7cU5yqlUzsacZcBpkrOuMfpoVcY_4bpWfiFwO0mMUlkc-a7v4OkDuZvOisfXlRpBFUjON59G%7cIBNPJT2mNFAJDZp0qNOT-Rbstnph2f17GSUpAwVl0LiY-otG1lmTGuH-moAobgEkzLTSYG%7c1370962573; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: cookie_check=yes; expires=Fri, 09-Jun-2023 14:56:13 GMT; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: navcmd=_notify-validate; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: navlns=0.0; expires=Mon, 06-Jun-2033 14:56:13 GMT; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: Apache=10.72.109.11.1370962573360037; path=\\/; expires=Thu, 04-Jun-43 14:56:13 GMT\\r\\nConnection: close\\r\\nSet-Cookie: X-PP-SILOVER=name%3DSANDBOX3.WEB.1%26silo_version%3D880%26app%3Dslingshot%26TIME%3D2369435473; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: Apache=10.72.128.11.1370962573351065; path=\\/; expires=Thu, 04-Jun-43 14:56:13 GMT\\r\\nVary: Accept-Encoding\\r\\nStrict-Transport-Security: max-age=14400\\r\\nTransfer-Encoding: chunked\\r\\nContent-Type: text\\/html; charset=UTF-8\\r\\n\\r\\n8\\r\\nVERIFIED\\r\\n0\\r\\n\\r\\n"}', '2013-06-11 09:56:14', '2013-06-11 09:56:15'),
(5, 'IPN', 'cart', '502133629', 'ERROR', 'Receiver email seller@paypalsandbox.com does not match merchant''s', '93f4c161d40f6b30d091d1e7e47b9e3e', '{"ipn_data":{"mc_handling1":"1.67","address_state":"CA","quantity1":"1","txn_id":"502133629","last_name":"Smith","mc_currency":"USD","payer_status":"verified","address_status":"confirmed","tax":"2.02","invoice":"abc1234","address_street":"123, any street","payer_email":"buyer@paypalsandbox.com","mc_gross1":"12.34","mc_shipping":"3.02","first_name":"John","business":"seller@paypalsandbox.com","verify_sign":"ArZDinixGnLpbi7QGIsMtSaJWRnNAUrrhEMIhlH4tXLqixwXdTaPQKHU","payer_id":"TESTBUYERID01","payment_date":"06:46:40 11 Jun 2013 PDT","address_country":"United States","payment_status":"Completed","receiver_email":"seller@paypalsandbox.com","payment_type":"instant","address_zip":"95131","address_city":"San Jose","mc_shipping1":"1.02","item_name1":"something","mc_gross":"15.34","item_number1":"AK-1234","mc_fee":"0.44","residence_country":"US","address_country_code":"US","notify_version":"2.4","receiver_id":"seller@paypalsandbox.com","mc_handling":"2.06","txn_type":"cart","custom":"xyz123","address_name":"John Smith","test_ipn":"1"},"ipn_response":"HTTP\\/1.1 200 OK\\r\\nDate: Tue, 11 Jun 2013 13:48:37 GMT\\r\\nServer: Apache\\r\\nX-Frame-Options: SAMEORIGIN\\r\\nSet-Cookie: c9MWDuvPtT9GIMyPc3jwol1VSlO=LmrmRQWd4jMgSUAvwOdQ0EUhpZ3Mnt8s3tcbUbtom4t64YYDBgcP-vGnHwYQxiNQfQvvM_Cv_zXXZVhAKpZo5Ts5VhbHEEkwECUv6MsiHe-MRIBdJl4UBb2GLrCSZgnFr9qecG%7cDEIz254xS-PwCrclAPfDXDJDsfu-BmsmR563gQ_GQViFV94PdXCFv_9P-KaMgQdlA4zOs0%7cMoHHuV8rC4Op_F896yfMVgIcAZMqiz_Ese8mazVkIIU7upnGsSX9pEniagYB_8YEDUbjZG%7c1370958517; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: cookie_check=yes; expires=Fri, 09-Jun-2023 13:48:37 GMT; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: navcmd=_notify-validate; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: navlns=0.0; expires=Mon, 06-Jun-2033 13:48:37 GMT; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: Apache=10.72.109.11.1370958517156612; path=\\/; expires=Thu, 04-Jun-43 13:48:37 GMT\\r\\nConnection: close\\r\\nSet-Cookie: X-PP-SILOVER=name%3DSANDBOX3.WEB.1%26silo_version%3D880%26app%3Dslingshot%26TIME%3D3039475537; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: Apache=10.72.128.11.1370958517146954; path=\\/; expires=Thu, 04-Jun-43 13:48:37 GMT\\r\\nVary: Accept-Encoding\\r\\nStrict-Transport-Security: max-age=14400\\r\\nTransfer-Encoding: chunked\\r\\nContent-Type: text\\/html; charset=UTF-8\\r\\n\\r\\n8\\r\\nVERIFIED\\r\\n0\\r\\n\\r\\n"}', '2013-06-11 08:48:37', '2013-06-11 08:48:38'),
(6, 'IPN', 'cart', '322123679', 'SUCCESS', 'Parsing, authentication and validation complete', '3e392d10a34242f7d7e1abff2d6f3a2c', '{"ipn_data":{"mc_handling1":"1.67","address_state":"CA","quantity1":"1","txn_id":"322123679","last_name":"Smith","mc_currency":"USD","payer_status":"verified","address_status":"confirmed","tax":"2.02","invoice":"abc1234","address_street":"123, any street","payer_email":"buyer@paypalsandbox.com","mc_gross1":"12.34","mc_shipping":"3.02","first_name":"John","business":"support@phuconcepts.com","verify_sign":"AX.7EhUYINHfaTyDCzVntiyTHhceAM-2Vb5GUAFCuj3CW4HJ715Gq2Hc","payer_id":"TESTBUYERID01","payment_date":"06:48:35 11 Jun 2013 PDT","address_country":"United States","payment_status":"Completed","receiver_email":"support@phuconcepts.com","payment_type":"instant","address_zip":"95131","address_city":"San Jose","mc_shipping1":"1.02","item_name1":"something","mc_gross":"15.34","item_number1":"AK-1234","mc_fee":"0.44","residence_country":"US","address_country_code":"US","notify_version":"2.4","receiver_id":"support@phuconcepts.com","mc_handling":"2.06","txn_type":"cart","custom":"xyz123","address_name":"John Smith","test_ipn":"1"},"ipn_response":"HTTP\\/1.1 200 OK\\r\\nDate: Tue, 11 Jun 2013 13:49:33 GMT\\r\\nServer: Apache\\r\\nX-Frame-Options: SAMEORIGIN\\r\\nSet-Cookie: c9MWDuvPtT9GIMyPc3jwol1VSlO=J5pGonyR75vTly8hbbaFpbjvMCDYB94s8hLs7cL0Iq10ptZWd3SjvjQ756Cr3KNxWL8fdaHXiZi-4KVG86-Jj5vaZaQxJdXx2Yt7Vi_1lTATsYPtH5ZTBAghTos2tO9CiuyHoG%7cvBl1-1dBncClLiYpnMrRcCoATL_gztQRwUC7d4sIBIJvm_8GpFtkOlwz5rcMvq8YkkBdOG%7cK5LI4UI646k8A0ObjHY9h_3xxhhK4pUMXlViA1E3YKFNrHqyBthn3375tbHIQkZ1moIBEG%7c1370958573; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: cookie_check=yes; expires=Fri, 09-Jun-2023 13:49:33 GMT; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: navcmd=_notify-validate; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: navlns=0.0; expires=Mon, 06-Jun-2033 13:49:33 GMT; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: Apache=10.72.109.11.1370958573109353; path=\\/; expires=Thu, 04-Jun-43 13:49:33 GMT\\r\\nConnection: close\\r\\nSet-Cookie: X-PP-SILOVER=name%3DSANDBOX3.WEB.1%26silo_version%3D880%26app%3Dslingshot%26TIME%3D3978999633; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: Apache=10.72.128.11.1370958573098193; path=\\/; expires=Thu, 04-Jun-43 13:49:33 GMT\\r\\nVary: Accept-Encoding\\r\\nStrict-Transport-Security: max-age=14400\\r\\nTransfer-Encoding: chunked\\r\\nContent-Type: text\\/html; charset=UTF-8\\r\\n\\r\\n8\\r\\nVERIFIED\\r\\n0\\r\\n\\r\\n"}', '2013-06-11 08:49:33', '2013-06-11 08:49:34'),
(8, 'IPN', 'cart', '1005253049', 'SUCCESS', 'Parsing, authentication and validation complete', '2817bf902bc7d13c99d0c8041068502c', '{"ipn_data":{"mc_handling1":"1.67","address_state":"CA","txn_id":"1005253049","last_name":"Smith","mc_currency":"USD","payer_status":"verified","address_status":"confirmed","tax":"2.02","invoice":"19","address_street":"123, any street","payer_email":"buyer@paypalsandbox.com","mc_gross1":"12.34","mc_shipping":"3.02","first_name":"John","business":"support@phuconcepts.com","verify_sign":"AYRHmQqp21DrZT1UyC7H6LW5OqsQAHNIzJ6.37uuFzLB5sFblCyPI6zn","payer_id":"TESTBUYERID01","payment_date":"07:57:36 11 Jun 2013 PDT","address_country":"United States","payment_status":"Completed","receiver_email":"support@phuconcepts.com","payment_type":"instant","address_zip":"95131","address_city":"San Jose","mc_shipping1":"1.02","item_name1":"something","mc_gross":"15.34","item_number1":"AK-1234","mc_fee":"0.44","residence_country":"US","address_country_code":"US","notify_version":"2.4","receiver_id":"support@phuconcepts.com","mc_handling":"2.06","txn_type":"cart","custom":"php","address_name":"John Smith","test_ipn":"1"},"ipn_response":"HTTP\\/1.1 200 OK\\r\\nDate: Tue, 11 Jun 2013 14:58:21 GMT\\r\\nServer: Apache\\r\\nX-Frame-Options: SAMEORIGIN\\r\\nSet-Cookie: c9MWDuvPtT9GIMyPc3jwol1VSlO=yXKLiZTH2Ogn4lUX69DP1xF51H299e5AWsN7dU6Od3pWWuwWXQ7PGnTNbQuBYCUkHUhRaluHNRQxrEtEelqJUGakpx_qVhCw1NNPkvET5_ARi6PUtLgvQhLiYALlPGvVswqvK0%7chQCW0e_sOIBxKqhI3cs2UPR4txm-7G41F_rx_Rn2xFFBS8l7J5DNHkbexc4wBYygG-wy3W%7clL85VA1HeF0wCrQ64sGIJJRubCwcXVJSbo4Ea_GW-s2N6T2VWWbpKmesx2hlLO1R25l7mW%7c1370962701; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: cookie_check=yes; expires=Fri, 09-Jun-2023 14:58:21 GMT; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: navcmd=_notify-validate; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: navlns=0.0; expires=Mon, 06-Jun-2033 14:58:21 GMT; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: Apache=10.72.109.11.1370962701321294; path=\\/; expires=Thu, 04-Jun-43 14:58:21 GMT\\r\\nConnection: close\\r\\nSet-Cookie: X-PP-SILOVER=name%3DSANDBOX3.WEB.1%26silo_version%3D880%26app%3Dslingshot%26TIME%3D222017361; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: Apache=10.72.128.11.1370962701309291; path=\\/; expires=Thu, 04-Jun-43 14:58:21 GMT\\r\\nVary: Accept-Encoding\\r\\nStrict-Transport-Security: max-age=14400\\r\\nTransfer-Encoding: chunked\\r\\nContent-Type: text\\/html; charset=UTF-8\\r\\n\\r\\n8\\r\\nVERIFIED\\r\\n0\\r\\n\\r\\n"}', '2013-06-11 09:58:22', '2013-06-11 09:58:23'),
(9, 'IPN', 'cart', '321424618', 'SUCCESS', 'Parsing, authentication and validation complete', 'efdb89272a858e07dca4fb1a53f11d70', '{"ipn_data":{"mc_handling1":"1.67","address_state":"CA","txn_id":"321424618","last_name":"Smith","mc_currency":"USD","payer_status":"verified","address_status":"confirmed","tax":"2.02","invoice":"19","address_street":"123, any street","payer_email":"buyer@paypalsandbox.com","mc_gross1":"5.55","mc_shipping":"3.02","first_name":"John","business":"support@phuconcepts.com","verify_sign":"An5ns1Kso7MWUdW4ErQKJJJ4qi4-AbOFregdDmUkRo-CZij96KDWmGhc","payer_id":"TESTBUYERID01","payment_date":"07:58:20 11 Jun 2013 PDT","address_country":"United States","payment_status":"Completed","receiver_email":"support@phuconcepts.com","payment_type":"instant","address_zip":"95131","address_city":"San Jose","mc_shipping1":"1.02","item_name1":"something","mc_gross":"15.34","item_number1":"AK-1234","mc_fee":"0.44","residence_country":"US","address_country_code":"US","notify_version":"2.4","receiver_id":"support@phuconcepts.com","mc_handling":"2.06","txn_type":"cart","custom":"php","address_name":"John Smith","test_ipn":"1"},"ipn_response":"HTTP\\/1.1 200 OK\\r\\nDate: Tue, 11 Jun 2013 14:59:49 GMT\\r\\nServer: Apache\\r\\nX-Frame-Options: SAMEORIGIN\\r\\nSet-Cookie: c9MWDuvPtT9GIMyPc3jwol1VSlO=VlXtZtSPT1hoEFWpYDxAVBDsEIFKLGk2uGjQd0BwO1csGAcojYvG3npEYxVomT0SsYwCQPbRncyrF-bIDl4iGcT9tG0SxrM8M35YQY5IIHl1fRDKfWEYxJtE01xC8Vfw1XAJNW%7c-_2TTYCZy0ZiGtg37M9GXKXIu4I0W86rLLEfXpX8x2vxcRZQTkqwOQJ21el0yyArl5sQHG%7c1KJvkvKx06nUVOZDwllevgg748amYXrAuFaED2Wai5W0aiMKwWs5SxFNrf7jWBC7udqBBG%7c1370962789; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: cookie_check=yes; expires=Fri, 09-Jun-2023 14:59:49 GMT; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: navcmd=_notify-validate; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: navlns=0.0; expires=Mon, 06-Jun-2033 14:59:49 GMT; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: Apache=10.72.109.11.1370962789495594; path=\\/; expires=Thu, 04-Jun-43 14:59:49 GMT\\r\\nConnection: close\\r\\nSet-Cookie: X-PP-SILOVER=name%3DSANDBOX3.WEB.1%26silo_version%3D880%26app%3Dslingshot%26TIME%3D1698412369; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: Apache=10.72.128.11.1370962789483942; path=\\/; expires=Thu, 04-Jun-43 14:59:49 GMT\\r\\nVary: Accept-Encoding\\r\\nStrict-Transport-Security: max-age=14400\\r\\nTransfer-Encoding: chunked\\r\\nContent-Type: text\\/html; charset=UTF-8\\r\\n\\r\\n8\\r\\nVERIFIED\\r\\n0\\r\\n\\r\\n"}', '2013-06-11 09:59:50', '2013-06-11 09:59:51'),
(10, 'IPN', 'cart', '321424618', 'SUCCESS', 'Parsing, authentication and validation complete', 'efdb89272a858e07dca4fb1a53f11d70', '{"ipn_data":{"mc_handling1":"1.67","address_state":"CA","txn_id":"321424618","last_name":"Smith","mc_currency":"USD","payer_status":"verified","address_status":"confirmed","tax":"2.02","invoice":"19","address_street":"123, any street","payer_email":"buyer@paypalsandbox.com","mc_gross1":"5.55","mc_shipping":"3.02","first_name":"John","business":"support@phuconcepts.com","verify_sign":"An5ns1Kso7MWUdW4ErQKJJJ4qi4-AbOFregdDmUkRo-CZij96KDWmGhc","payer_id":"TESTBUYERID01","payment_date":"07:58:20 11 Jun 2013 PDT","address_country":"United States","payment_status":"Completed","receiver_email":"support@phuconcepts.com","payment_type":"instant","address_zip":"95131","address_city":"San Jose","mc_shipping1":"1.02","item_name1":"something","mc_gross":"15.34","item_number1":"AK-1234","mc_fee":"0.44","residence_country":"US","address_country_code":"US","notify_version":"2.4","receiver_id":"support@phuconcepts.com","mc_handling":"2.06","txn_type":"cart","custom":"php","address_name":"John Smith","test_ipn":"1"},"ipn_response":"HTTP\\/1.1 200 OK\\r\\nDate: Tue, 11 Jun 2013 15:07:19 GMT\\r\\nServer: Apache\\r\\nX-Frame-Options: SAMEORIGIN\\r\\nSet-Cookie: c9MWDuvPtT9GIMyPc3jwol1VSlO=Lk1IgGHKHS1fel88MyXRgXH7Qv2Nciat45B-0kT2kJRjntNRwT80z006l3rbXqXbEB_ANn1ndg0Xwv3QxcjIU5ox4HZMsEXMooP7jJ06nR1STd2dbOwgLaJYdWuAbaRTUJ9tTW%7cHjh4uGK_LIg76N6ahu8A-DJ1vI9sBAv81TAQAXrPfsZJoqZ9Mb-AABY4bFPl_BPQbOdo60%7cH6unSyGmaEITenxEaMEN--qManYjp-Si-wN0MMKNQ08rPVze1LFfuNBxw1BQsHJTV6lIjG%7c1370963239; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: cookie_check=yes; expires=Fri, 09-Jun-2023 15:07:19 GMT; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: navcmd=_notify-validate; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: navlns=0.0; expires=Mon, 06-Jun-2033 15:07:19 GMT; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: Apache=10.72.109.11.1370963239134967; path=\\/; expires=Thu, 04-Jun-43 15:07:19 GMT\\r\\nConnection: close\\r\\nSet-Cookie: X-PP-SILOVER=name%3DSANDBOX3.WEB.1%26silo_version%3D880%26app%3Dslingshot%26TIME%3D658356049; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: Apache=10.72.128.11.1370963239125850; path=\\/; expires=Thu, 04-Jun-43 15:07:19 GMT\\r\\nVary: Accept-Encoding\\r\\nStrict-Transport-Security: max-age=14400\\r\\nTransfer-Encoding: chunked\\r\\nContent-Type: text\\/html; charset=UTF-8\\r\\n\\r\\n8\\r\\nVERIFIED\\r\\n0\\r\\n\\r\\n"}', '2013-06-11 10:07:19', '2013-06-11 10:07:21'),
(11, 'IPN', 'cart', '856312822', 'SUCCESS', 'Parsing, authentication and validation complete', '18dd83c83371064c8c2e8dafabdf935a', '{"ipn_data":{"mc_handling1":"1.67","address_state":"CA","txn_id":"856312822","last_name":"Smith","mc_currency":"USD","payer_status":"verified","address_status":"confirmed","tax":"2.02","invoice":"18","address_street":"123, any street","payer_email":"buyer@paypalsandbox.com","mc_gross1":"12.34","mc_shipping":"3.02","first_name":"John","business":"support@phuconcepts.com","verify_sign":"Ai1PaghZh5FmBLCDCTQpwG8jB264AXsTGG8m-fSsTjtp8mqu302mKnt6","payer_id":"TESTBUYERID01","payment_date":"07:59:48 11 Jun 2013 PDT","address_country":"United States","payment_status":"Completed","receiver_email":"support@phuconcepts.com","payment_type":"instant","address_zip":"95131","address_city":"San Jose","mc_shipping1":"1.02","item_name1":"qdfad","mc_gross":"15.34","item_number1":"dqswadq","mc_fee":"0.44","residence_country":"US","address_country_code":"US","notify_version":"2.4","receiver_id":"support@phuconcepts.com","mc_handling":"2.06","txn_type":"cart","custom":"php","address_name":"John Smith","test_ipn":"1"},"ipn_response":"HTTP\\/1.1 200 OK\\r\\nDate: Tue, 11 Jun 2013 15:08:48 GMT\\r\\nServer: Apache\\r\\nX-Frame-Options: SAMEORIGIN\\r\\nSet-Cookie: c9MWDuvPtT9GIMyPc3jwol1VSlO=KY0KpfojZW6VZbdZsigTLwhWDjq8MO-Ta9IxnAd_Lw6naz1Rxpu65hLD5folqxaI9EWQxIPoj4BpSpNXexSuyRNkYrbTWIznRaK4KL5Sw0WS_EYGvPld2eawfjtKUKZHZnt1lW%7csHgbeMta0aZaYag5yDQWa_DSjeQF8fUdVxAs-V8nXsARgq714kLnkOZph1aDGb6PYzBXg0%7cCvJfIRVb_JcYVFF2wfIK9AnIhhNB4ujZJ2w1_6MiYvTiWPykeLPQBZXarveW2FJifpwuqW%7c1370963328; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: cookie_check=yes; expires=Fri, 09-Jun-2023 15:08:48 GMT; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: navcmd=_notify-validate; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: navlns=0.0; expires=Mon, 06-Jun-2033 15:08:48 GMT; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: Apache=10.72.109.11.1370963328476363; path=\\/; expires=Thu, 04-Jun-43 15:08:48 GMT\\r\\nConnection: close\\r\\nSet-Cookie: X-PP-SILOVER=name%3DSANDBOX3.WEB.1%26silo_version%3D880%26app%3Dslingshot%26TIME%3D2151528273; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: Apache=10.72.128.11.1370963328468070; path=\\/; expires=Thu, 04-Jun-43 15:08:48 GMT\\r\\nVary: Accept-Encoding\\r\\nStrict-Transport-Security: max-age=14400\\r\\nTransfer-Encoding: chunked\\r\\nContent-Type: text\\/html; charset=UTF-8\\r\\n\\r\\n8\\r\\nVERIFIED\\r\\n0\\r\\n\\r\\n"}', '2013-06-11 10:08:49', '2013-06-11 10:08:50'),
(12, 'IPN', 'cart', '291398141', 'SUCCESS', 'Parsing, authentication and validation complete', '1fa432bb14e35d6fca87fd33e935dc48', '{"ipn_data":{"mc_handling1":"1.67","address_state":"CA","quantity1":"3","txn_id":"291398141","last_name":"Smith","mc_currency":"USD","payer_status":"verified","address_status":"confirmed","tax":"2.02","invoice":"18","address_street":"123, any street","payer_email":"buyer@paypalsandbox.com","mc_gross1":"12.34","mc_shipping":"3.02","first_name":"John","business":"support@phuconcepts.com","verify_sign":"Aclp20XKmFScyK-mU20wwRqId4-uABaI1kPuHO.hdoG59j-nlVQLkQFh","payer_id":"TESTBUYERID01","payment_date":"08:08:46 11 Jun 2013 PDT","address_country":"United States","payment_status":"Completed","receiver_email":"support@phuconcepts.com","payment_type":"instant","address_zip":"95131","address_city":"San Jose","mc_shipping1":"1.02","item_name1":"54","mc_gross":"15.34","item_number1":"541","mc_fee":"0.44","residence_country":"US","address_country_code":"US","notify_version":"2.4","receiver_id":"support@phuconcepts.com","mc_handling":"2.06","txn_type":"cart","custom":"PHP","address_name":"John Smith","test_ipn":"1"},"ipn_response":"HTTP\\/1.1 200 OK\\r\\nDate: Tue, 11 Jun 2013 15:10:21 GMT\\r\\nServer: Apache\\r\\nX-Frame-Options: SAMEORIGIN\\r\\nSet-Cookie: c9MWDuvPtT9GIMyPc3jwol1VSlO=Es3WZSI_Tbmk_KB1EC8n573vgPfJpFP25BX8fzrXDQo2AS-U6OHlkcd74gxPFy9HRDUQkamCkctOrn7AsUF3uI3p3iVYNXrVHIkZKbqxiv7GmWPu49u5wCAFuIG9K5ZipyOUlW%7crRGRjptJjziv-0mXLUlwh3u0DTdaKz0sYXF6ZXg1sCjfSRD1-w1ELo2lCX0AAawL7ozbPW%7cYl1Yk4VzgdKEtD1QWq26Xe1Ta3FDP4M9OpdBm_xo74qfCZsWvnBEULJD5gyWHE7GSw3ZoG%7c1370963421; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: cookie_check=yes; expires=Fri, 09-Jun-2023 15:10:21 GMT; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: navcmd=_notify-validate; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: navlns=0.0; expires=Mon, 06-Jun-2033 15:10:21 GMT; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: Apache=10.72.109.11.1370963421633780; path=\\/; expires=Thu, 04-Jun-43 15:10:21 GMT\\r\\nConnection: close\\r\\nSet-Cookie: X-PP-SILOVER=name%3DSANDBOX3.WEB.1%26silo_version%3D880%26app%3Dslingshot%26TIME%3D3711809361; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: Apache=10.72.128.11.1370963421625895; path=\\/; expires=Thu, 04-Jun-43 15:10:21 GMT\\r\\nVary: Accept-Encoding\\r\\nStrict-Transport-Security: max-age=14400\\r\\nSet-Cookie: X-PP-SILOVER=name%3DSANDBOX3.WEB.1%26silo_version%3D880%26app%3Dslingshot%26TIME%3D3711809361; domain=.paypal.com; path=\\/; Secure; HttpOnly\\r\\nSet-Cookie: Apache=10.72.128.11.1370963421617141; path=\\/; expires=Thu, 04-Jun-43 15:10:21 GMT\\r\\nStrict-Transport-Security: max-age=14400\\r\\nTransfer-Encoding: chunked\\r\\nContent-Type: text\\/html; charset=UTF-8\\r\\n\\r\\n8\\r\\nVERIFIED\\r\\n0\\r\\n\\r\\n"}', '2013-06-11 10:10:22', '2013-06-11 10:10:23'),
(13, 'IPN', 'web_accept', '3N380822KJ5169234', 'SUCCESS', 'Parsing, authentication and validation complete', '116f40e292b103fe59afa2a4268b1f72', NULL, '2013-08-29 13:48:35', '2013-08-29 13:48:36');

-- --------------------------------------------------------

--
-- Table structure for table `ipn_orders`
--

CREATE TABLE IF NOT EXISTS `ipn_orders` (
  `id` int(11) NOT NULL,
  `notify_version` varchar(64) DEFAULT NULL COMMENT 'IPN Version Number',
  `verify_sign` varchar(127) DEFAULT NULL COMMENT 'Encrypted string used to verify the authenticityof the tansaction',
  `test_ipn` int(11) DEFAULT NULL,
  `protection_eligibility` varchar(24) DEFAULT NULL COMMENT 'Which type of seller protection the buyer is protected by',
  `charset` varchar(127) DEFAULT NULL COMMENT 'Character set used by PayPal',
  `btn_id` varchar(40) DEFAULT NULL COMMENT 'The PayPal buy button clicked',
  `address_city` varchar(40) DEFAULT NULL COMMENT 'City of customers address',
  `address_country` varchar(64) DEFAULT NULL COMMENT 'Country of customers address',
  `address_country_code` varchar(2) DEFAULT NULL COMMENT 'Two character ISO 3166 country code',
  `address_name` varchar(128) DEFAULT NULL COMMENT 'Name used with address (included when customer provides a Gift address)',
  `address_state` varchar(40) DEFAULT NULL COMMENT 'State of customer address',
  `address_status` varchar(20) DEFAULT NULL COMMENT 'confirmed/unconfirmed',
  `address_street` varchar(200) DEFAULT NULL COMMENT 'Customer''s street address',
  `address_zip` varchar(20) DEFAULT NULL COMMENT 'Zip code of customer''s address',
  `first_name` varchar(64) DEFAULT NULL COMMENT 'Customer''s first name',
  `last_name` varchar(64) DEFAULT NULL COMMENT 'Customer''s last name',
  `payer_business_name` varchar(127) DEFAULT NULL COMMENT 'Customer''s company name, if customer represents a business',
  `payer_email` varchar(127) DEFAULT NULL COMMENT 'Customer''s primary email address. Use this email to provide any credits',
  `payer_id` varchar(13) DEFAULT NULL COMMENT 'Unique customer ID.',
  `payer_status` varchar(20) DEFAULT NULL COMMENT 'verified/unverified',
  `contact_phone` varchar(20) DEFAULT NULL COMMENT 'Customer''s telephone number.',
  `residence_country` varchar(2) DEFAULT NULL COMMENT 'Two-Character ISO 3166 country code',
  `business` varchar(127) DEFAULT NULL COMMENT 'Email address or account ID of the payment recipient (that is, the merchant). Equivalent to the values of receiver_email (If payment is sent to primary account) and business set in the Website Payment HTML.',
  `receiver_email` varchar(127) DEFAULT NULL COMMENT 'Primary email address of the payment recipient (that is, the merchant). If the payment is sent to a non-primary email address on your PayPal account, the receiver_email is still your primary email.',
  `receiver_id` varchar(13) DEFAULT NULL COMMENT 'Unique account ID of the payment recipient (i.e., the merchant). This is the same as the recipients referral ID.',
  `custom` varchar(255) DEFAULT NULL COMMENT 'Custom value as passed by you, the merchant. These are pass-through variables that are never presented to your customer.',
  `invoice` varchar(127) DEFAULT NULL COMMENT 'Pass through variable you can use to identify your invoice number for this purchase. If omitted, no variable is passed back.',
  `memo` varchar(255) DEFAULT NULL COMMENT 'Memo as entered by your customer in PayPal Website Payments note field.',
  `tax` decimal(10,2) DEFAULT NULL COMMENT 'Amount of tax charged on payment',
  `auth_id` varchar(19) DEFAULT NULL COMMENT 'Authorization identification number',
  `auth_exp` varchar(28) DEFAULT NULL COMMENT 'Authorization expiration date and time, in the following format: HH:MM:SS DD Mmm YY, YYYY PST',
  `auth_amount` int(11) DEFAULT NULL COMMENT 'Authorization amount',
  `auth_status` varchar(20) DEFAULT NULL COMMENT 'Status of authorization',
  `num_cart_items` int(11) DEFAULT NULL COMMENT 'If this is a PayPal shopping cart transaction, number of items in the cart',
  `parent_txn_id` varchar(19) DEFAULT NULL COMMENT 'In the case of a refund, reversal, or cancelled reversal, this variable contains the txn_id of the original transaction, while txn_id contains a new ID for the new transaction.',
  `payment_date` varchar(28) DEFAULT NULL COMMENT 'Time/date stamp generated by PayPal, in the following format: HH:MM:SS DD Mmm YY, YYYY PST',
  `payment_status` varchar(20) DEFAULT NULL COMMENT 'Payment status of the payment',
  `payment_type` varchar(10) DEFAULT NULL COMMENT 'echeck/instant',
  `pending_reason` varchar(20) DEFAULT NULL COMMENT 'This variable is only set if payment_status=pending',
  `reason_code` varchar(20) DEFAULT NULL COMMENT 'This variable is only set if payment_status=reversed',
  `remaining_settle` int(11) DEFAULT NULL COMMENT 'Remaining amount that can be captured with Authorization and Capture',
  `shipping_method` varchar(64) DEFAULT NULL COMMENT 'The name of a shipping method from the shipping calculations section of the merchants account profile. The buyer selected the named shipping method for this transaction',
  `shipping` decimal(10,2) DEFAULT NULL COMMENT 'Shipping charges associated with this transaction. Format unsigned, no currency symbol, two decimal places',
  `transaction_entity` varchar(20) DEFAULT NULL COMMENT 'Authorization and capture transaction entity',
  `txn_id` varchar(19) DEFAULT NULL COMMENT 'A unique transaction ID generated by PayPal',
  `txn_type` varchar(20) DEFAULT NULL COMMENT 'cart/express_checkout/send-money/virtual-terminal/web-accept',
  `exchange_rate` decimal(10,2) DEFAULT NULL COMMENT 'Exchange rate used if a currency conversion occured',
  `mc_currency` varchar(3) DEFAULT NULL COMMENT 'Three character country code. For payment IPN notifications, this is the currency of the payment, for non-payment subscription IPN notifications, this is the currency of the subscription.',
  `mc_fee` decimal(10,2) DEFAULT NULL COMMENT 'Transaction fee associated with the payment, mc_gross minus mc_fee equals the amount deposited into the receiver_email account. Equivalent to payment_fee for USD payments. If this amount is negative, it signifies a refund or reversal, and either ofthose p',
  `mc_gross` decimal(10,2) DEFAULT NULL COMMENT 'Full amount of the customer''s payment',
  `mc_handling` decimal(10,2) DEFAULT NULL COMMENT 'Total handling charge associated with the transaction',
  `mc_shipping` decimal(10,2) DEFAULT NULL COMMENT 'Total shipping amount associated with the transaction',
  `payment_fee` decimal(10,2) DEFAULT NULL COMMENT 'USD transaction fee associated with the payment',
  `payment_gross` decimal(10,2) DEFAULT NULL COMMENT 'Full USD amount of the customers payment transaction, before payment_fee is subtracted',
  `settle_amount` decimal(10,2) DEFAULT NULL COMMENT 'Amount that is deposited into the account''s primary balance after a currency conversion',
  `settle_currency` varchar(3) DEFAULT NULL COMMENT 'Currency of settle amount. Three digit currency code',
  `auction_buyer_id` varchar(64) DEFAULT NULL COMMENT 'The customer''s auction ID.',
  `auction_closing_date` varchar(28) DEFAULT NULL COMMENT 'The auction''s close date. In the format: HH:MM:SS DD Mmm YY, YYYY PSD',
  `auction_multi_item` int(11) DEFAULT NULL COMMENT 'The number of items purchased in multi-item auction payments',
  `for_auction` varchar(10) DEFAULT NULL COMMENT 'This is an auction payment - payments made using Pay for eBay Items or Smart Logos - as well as send money/money request payments with the type eBay items or Auction Goods(non-eBay)',
  `subscr_date` varchar(28) DEFAULT NULL COMMENT 'Start date or cancellation date depending on whether txn_type is subcr_signup or subscr_cancel',
  `subscr_effective` varchar(28) DEFAULT NULL COMMENT 'Date when a subscription modification becomes effective',
  `period1` varchar(10) DEFAULT NULL COMMENT '(Optional) Trial subscription interval in days, weeks, months, years (example a 4 day interval is 4 D',
  `period2` varchar(10) DEFAULT NULL COMMENT '(Optional) Trial period',
  `period3` varchar(10) DEFAULT NULL COMMENT 'Regular subscription interval in days, weeks, months, years',
  `amount1` decimal(10,2) DEFAULT NULL COMMENT 'Amount of payment for Trial period 1 for USD',
  `amount2` decimal(10,2) DEFAULT NULL COMMENT 'Amount of payment for Trial period 2 for USD',
  `amount3` decimal(10,2) DEFAULT NULL COMMENT 'Amount of payment for regular subscription  period 1 for USD',
  `mc_amount1` decimal(10,2) DEFAULT NULL COMMENT 'Amount of payment for trial period 1 regardless of currency',
  `mc_amount2` decimal(10,2) DEFAULT NULL COMMENT 'Amount of payment for trial period 2 regardless of currency',
  `mc_amount3` decimal(10,2) DEFAULT NULL COMMENT 'Amount of payment for regular subscription period regardless of currency',
  `recurring` varchar(1) DEFAULT NULL COMMENT 'Indicates whether rate recurs (1 is yes, blank is no)',
  `reattempt` varchar(1) DEFAULT NULL COMMENT 'Indicates whether reattempts should occur on payment failure (1 is yes, blank is no)',
  `retry_at` varchar(28) DEFAULT NULL COMMENT 'Date PayPal will retry a failed subscription payment',
  `recur_times` int(11) DEFAULT NULL COMMENT 'The number of payment installations that will occur at the regular rate',
  `username` varchar(64) DEFAULT NULL COMMENT '(Optional) Username generated by PayPal and given to subscriber to access the subscription',
  `password` varchar(24) DEFAULT NULL COMMENT '(Optional) Password generated by PayPal and given to subscriber to access the subscription (Encrypted)',
  `subscr_id` varchar(19) DEFAULT NULL COMMENT 'ID generated by PayPal for the subscriber',
  `case_id` varchar(28) DEFAULT NULL COMMENT 'Case identification number',
  `case_type` varchar(28) DEFAULT NULL COMMENT 'complaint/chargeback',
  `case_creation_date` varchar(28) DEFAULT NULL COMMENT 'Date/Time the case was registered',
  `order_status` enum('PAID','WAITING','REJECTED') DEFAULT NULL COMMENT 'Additional variable to make payment_status more actionable',
  `discount` decimal(10,2) DEFAULT NULL COMMENT 'Additional variable to record the discount made on the order',
  `shipping_discount` decimal(10,2) DEFAULT NULL COMMENT 'Record the discount made on the shipping',
  `ipn_track_id` varchar(127) DEFAULT NULL COMMENT 'Internal tracking variable added in April 2011',
  `transaction_subject` varchar(255) DEFAULT NULL COMMENT 'Describes the product for a button-based purchase',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ipn_orders`
--

INSERT INTO `ipn_orders` (`id`, `notify_version`, `verify_sign`, `test_ipn`, `protection_eligibility`, `charset`, `btn_id`, `address_city`, `address_country`, `address_country_code`, `address_name`, `address_state`, `address_status`, `address_street`, `address_zip`, `first_name`, `last_name`, `payer_business_name`, `payer_email`, `payer_id`, `payer_status`, `contact_phone`, `residence_country`, `business`, `receiver_email`, `receiver_id`, `custom`, `invoice`, `memo`, `tax`, `auth_id`, `auth_exp`, `auth_amount`, `auth_status`, `num_cart_items`, `parent_txn_id`, `payment_date`, `payment_status`, `payment_type`, `pending_reason`, `reason_code`, `remaining_settle`, `shipping_method`, `shipping`, `transaction_entity`, `txn_id`, `txn_type`, `exchange_rate`, `mc_currency`, `mc_fee`, `mc_gross`, `mc_handling`, `mc_shipping`, `payment_fee`, `payment_gross`, `settle_amount`, `settle_currency`, `auction_buyer_id`, `auction_closing_date`, `auction_multi_item`, `for_auction`, `subscr_date`, `subscr_effective`, `period1`, `period2`, `period3`, `amount1`, `amount2`, `amount3`, `mc_amount1`, `mc_amount2`, `mc_amount3`, `recurring`, `reattempt`, `retry_at`, `recur_times`, `username`, `password`, `subscr_id`, `case_id`, `case_type`, `case_creation_date`, `order_status`, `discount`, `shipping_discount`, `ipn_track_id`, `transaction_subject`, `created_at`, `updated_at`) VALUES
(1, '2.4', 'A24nwNvp1GPWAd3LS0v2O.5jNy6EAdQFirl3QkzO91fHgVHDQQMFX2oF', 1, NULL, NULL, NULL, 'San Jose', 'United States', 'US', 'John Smith', 'CA', 'confirmed', '123, any street', '95131', 'John', 'Smith', NULL, 'buyer@paypalsandbox.com', 'TESTBUYERID01', 'verified', NULL, 'US', 'support@phuconcepts.com', 'support@phuconcepts.com', 'support@phuco', 'xyz123', 'abc1234', NULL, '2.02', NULL, NULL, NULL, NULL, NULL, NULL, '06:45:16 11 Jun 2013 PDT', 'Completed', 'instant', NULL, NULL, NULL, NULL, NULL, NULL, '212915134', 'cart', NULL, 'USD', '0.44', '15.34', '2.06', '3.02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAID', '-15.34', NULL, NULL, NULL, '2013-06-11 08:46:44', '2013-06-11 08:46:44'),
(2, '2.4', 'AX.7EhUYINHfaTyDCzVntiyTHhceAM-2Vb5GUAFCuj3CW4HJ715Gq2Hc', 1, NULL, NULL, NULL, 'San Jose', 'United States', 'US', 'John Smith', 'CA', 'confirmed', '123, any street', '95131', 'John', 'Smith', NULL, 'buyer@paypalsandbox.com', 'TESTBUYERID01', 'verified', NULL, 'US', 'support@phuconcepts.com', 'support@phuconcepts.com', 'support@phuco', 'xyz123', 'abc1234', NULL, '2.02', NULL, NULL, NULL, NULL, NULL, NULL, '06:48:35 11 Jun 2013 PDT', 'Completed', 'instant', NULL, NULL, NULL, NULL, NULL, NULL, '322123679', 'cart', NULL, 'USD', '0.44', '15.34', '2.06', '3.02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAID', '-15.34', NULL, NULL, NULL, '2013-06-11 08:49:34', '2013-06-11 08:49:34'),
(3, '2.4', 'AYRHmQqp21DrZT1UyC7H6LW5OqsQAHNIzJ6.37uuFzLB5sFblCyPI6zn', 1, NULL, NULL, NULL, 'San Jose', 'United States', 'US', 'John Smith', 'CA', 'confirmed', '123, any street', '95131', 'John', 'Smith', NULL, 'buyer@paypalsandbox.com', 'TESTBUYERID01', 'verified', NULL, 'US', 'support@phuconcepts.com', 'support@phuconcepts.com', 'support@phuco', 'php', '19', NULL, '2.02', NULL, NULL, NULL, NULL, NULL, NULL, '07:57:36 11 Jun 2013 PDT', 'Completed', 'instant', NULL, NULL, NULL, NULL, NULL, NULL, '1005253049', 'cart', NULL, 'USD', '0.44', '15.34', '2.06', '3.02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAID', '-15.34', NULL, NULL, NULL, '2013-06-11 09:58:23', '2013-06-11 09:58:23'),
(4, '2.4', 'An5ns1Kso7MWUdW4ErQKJJJ4qi4-AbOFregdDmUkRo-CZij96KDWmGhc', 1, NULL, NULL, NULL, 'San Jose', 'United States', 'US', 'John Smith', 'CA', 'confirmed', '123, any street', '95131', 'John', 'Smith', NULL, 'buyer@paypalsandbox.com', 'TESTBUYERID01', 'verified', NULL, 'US', 'support@phuconcepts.com', 'support@phuconcepts.com', 'support@phuco', 'php', '19', NULL, '2.02', NULL, NULL, NULL, NULL, NULL, NULL, '07:58:20 11 Jun 2013 PDT', 'Completed', 'instant', NULL, NULL, NULL, NULL, NULL, NULL, '321424618', 'cart', NULL, 'USD', '0.44', '15.34', '2.06', '3.02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAID', '-15.34', NULL, NULL, NULL, '2013-06-11 09:59:51', '2013-06-11 10:07:21'),
(5, '2.4', 'Ai1PaghZh5FmBLCDCTQpwG8jB264AXsTGG8m-fSsTjtp8mqu302mKnt6', 1, NULL, NULL, NULL, 'San Jose', 'United States', 'US', 'John Smith', 'CA', 'confirmed', '123, any street', '95131', 'John', 'Smith', NULL, 'buyer@paypalsandbox.com', 'TESTBUYERID01', 'verified', NULL, 'US', 'support@phuconcepts.com', 'support@phuconcepts.com', 'support@phuco', 'php', '18', NULL, '2.02', NULL, NULL, NULL, NULL, NULL, NULL, '07:59:48 11 Jun 2013 PDT', 'Completed', 'instant', NULL, NULL, NULL, NULL, NULL, NULL, '856312822', 'cart', NULL, 'USD', '0.44', '15.34', '2.06', '3.02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAID', '-15.34', NULL, NULL, NULL, '2013-06-11 10:08:50', '2013-06-11 10:08:50'),
(6, '2.4', 'Aclp20XKmFScyK-mU20wwRqId4-uABaI1kPuHO.hdoG59j-nlVQLkQFh', 1, NULL, NULL, NULL, 'San Jose', 'United States', 'US', 'John Smith', 'CA', 'confirmed', '123, any street', '95131', 'John', 'Smith', NULL, 'buyer@paypalsandbox.com', 'TESTBUYERID01', 'verified', NULL, 'US', 'support@phuconcepts.com', 'support@phuconcepts.com', 'support@phuco', 'PHP', '18', NULL, '2.02', NULL, NULL, NULL, NULL, NULL, NULL, '08:08:46 11 Jun 2013 PDT', 'Completed', 'instant', NULL, NULL, NULL, NULL, NULL, NULL, '291398141', 'cart', NULL, 'USD', '0.44', '15.34', '2.06', '3.02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAID', '-15.34', NULL, NULL, NULL, '2013-06-11 10:10:23', '2013-06-11 10:10:23'),
(7, '3.7', 'AY.rbRk1STVYHOfJUWt7y3NCLh5yAhuRR8IpuY-sFxwMPYo6h.v9pems', NULL, 'Eligible', 'windows-1252', NULL, 'Hornell', 'United States', 'US', 'robert evans', 'NY', 'confirmed', '9 church street', '14843', 'robert', 'evans', NULL, 'bobevansdct@hotmail.com', 'GZ88W9M8LNH6C', 'verified', NULL, 'US', 'peoplehelpingpeopleintl@gmail.com', 'peoplehelpingpeopleintl@gmail.com', 'XLGPTG4QEDE8L', NULL, '56', NULL, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, '11:48:21 Aug 29, 2013 PDT', 'Completed', 'instant', NULL, NULL, NULL, NULL, '0.00', NULL, '3N380822KJ5169234', 'web_accept', NULL, 'USD', '0.89', '27.00', NULL, NULL, '0.89', '27.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAID', '0.00', NULL, 'bce2c65fb7103', NULL, '2013-08-29 13:48:36', '2013-08-29 13:48:36');

-- --------------------------------------------------------

--
-- Table structure for table `ipn_order_items`
--

CREATE TABLE IF NOT EXISTS `ipn_order_items` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `item_name` varchar(127) DEFAULT NULL COMMENT 'Item name as passed by you, the merchant. Or, if not passed by you, as entered by your customer. If this is a shopping cart transaction, Paypal will append the number of the item (e.g., item_name_1,item_name_2, and so forth).',
  `item_number` varchar(127) DEFAULT NULL COMMENT 'Pass-through variable for you to track purchases. It will get passed back to you at the completion of the payment. If omitted, no variable will be passed back to you.',
  `quantity` varchar(127) DEFAULT NULL COMMENT 'Quantity as entered by your customer or as passed by you, the merchant. If this is a shopping cart transaction, PayPal appends the number of the item (e.g., quantity1,quantity2).',
  `mc_gross` decimal(10,2) DEFAULT NULL COMMENT 'Full amount of the customer''s payment',
  `mc_handling` decimal(10,2) DEFAULT NULL COMMENT 'Total handling charge associated with the transaction',
  `mc_shipping` decimal(10,2) DEFAULT NULL COMMENT 'Total shipping amount associated with the transaction',
  `tax` decimal(10,2) DEFAULT NULL COMMENT 'Amount of tax charged on payment',
  `cost_per_item` decimal(10,2) DEFAULT NULL COMMENT 'Cost of an individual item',
  `option_name_1` varchar(64) DEFAULT NULL COMMENT 'Option 1 name as requested by you',
  `option_selection_1` varchar(200) DEFAULT NULL COMMENT 'Option 1 choice as entered by your customer',
  `option_name_2` varchar(64) DEFAULT NULL COMMENT 'Option 2 name as requested by you',
  `option_selection_2` varchar(200) DEFAULT NULL COMMENT 'Option 2 choice as entered by your customer',
  `option_name_3` varchar(64) DEFAULT NULL COMMENT 'Option 3 name as requested by you',
  `option_selection_3` varchar(200) DEFAULT NULL COMMENT 'Option 3 choice as entered by your customer',
  `option_name_4` varchar(64) DEFAULT NULL COMMENT 'Option 4 name as requested by you',
  `option_selection_4` varchar(200) DEFAULT NULL COMMENT 'Option 4 choice as entered by your customer',
  `option_name_5` varchar(64) DEFAULT NULL COMMENT 'Option 5 name as requested by you',
  `option_selection_5` varchar(200) DEFAULT NULL COMMENT 'Option 5 choice as entered by your customer',
  `option_name_6` varchar(64) DEFAULT NULL COMMENT 'Option 6 name as requested by you',
  `option_selection_6` varchar(200) DEFAULT NULL COMMENT 'Option 6 choice as entered by your customer',
  `option_name_7` varchar(64) DEFAULT NULL COMMENT 'Option 7 name as requested by you',
  `option_selection_7` varchar(200) DEFAULT NULL COMMENT 'Option 7 choice as entered by your customer',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(40) COLLATE utf8_bin NOT NULL,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `ngo_reserve`
--

CREATE TABLE IF NOT EXISTS `ngo_reserve` (
  `id` int(11) unsigned NOT NULL,
  `org_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `currency` varchar(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `php_amount` double DEFAULT NULL,
  `ngo_amount` double DEFAULT NULL,
  `difference` double DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data` text
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ngo_reserve`
--

INSERT INTO `ngo_reserve` (`id`, `org_id`, `project_id`, `currency`, `type`, `php_amount`, `ngo_amount`, `difference`, `rate`, `timestamp`, `data`) VALUES
(13, 8, 50, 'GHS', 'Intial Deposit for God is Good Foodstuff', 550, 1398.375, 1398.375, 2.5425, '2014-03-12 23:06:55', '<style type="text/css">\n	.total td{\n		font-weight: bold;\n	}\n	img{ height: 200px; float: left;}\n	table tr td{border: 1px solid black; }\n	thead tr td{border:2px solid black; font-weight: bold; background-color: yellow; }\n	\n	table{ border-collapse: collapse; }	\n	.client tr td{border: none;}\n	.client img{ height: 150px;}\n</style>\n<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">\n\n<h4>A PHP Error was encountered</h4>\n\n<p>Severity: Warning</p>\n<p>Message:  Invalid argument supplied for foreach()</p>\n<p>Filename: admin/printClient_html.php</p>\n<p>Line Number: 19</p>\n\n</div>\n		    \n		    		'),
(14, 8, 49, 'GHS', 'Intial Deposit for Megyefo Tease Shop', 550, 1398.375, 1398.375, 2.5425, '2014-03-12 23:06:58', '<style type="text/css">\n	.total td{\n		font-weight: bold;\n	}\n	img{ height: 200px; float: left;}\n	table tr td{border: 1px solid black; }\n	thead tr td{border:2px solid black; font-weight: bold; background-color: yellow; }\n	\n	table{ border-collapse: collapse; }	\n	.client tr td{border: none;}\n	.client img{ height: 150px;}\n</style>\n<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">\n\n<h4>A PHP Error was encountered</h4>\n\n<p>Severity: Warning</p>\n<p>Message:  Invalid argument supplied for foreach()</p>\n<p>Filename: admin/printClient_html.php</p>\n<p>Line Number: 19</p>\n\n</div>\n		    \n		    		'),
(15, 27, 154, 'USD', 'Intial Deposit for Buying produce ', 500, 500, 0, 1, '2014-06-24 17:02:22', '<style type="text/css">\n	.total td{\n		font-weight: bold;\n	}\n	img{ height: 200px; float: left;}\n	table tr td{border: 1px solid black; }\n	thead tr td{border:2px solid black; font-weight: bold; background-color: yellow; }\n	\n	table{ border-collapse: collapse; }	\n	.client tr td{border: none;}\n	.client img{ height: 150px;}\n</style>\n    <table class="client">\n    <tr style="padding-top:10px;">\n    	<td>\n   <img src="http://www.phpintl.org/imageupload/server/php/files/Fidel-Castro_diaporama_full.jpg">\n   <br />\n   \n  \n     <strong>Collection Location</strong> New York  <br />\n     <strong>Name</strong> Alexis Fidele <br />\n     <strong>Address </strong> <br />\n		   <br/>\n		   ,     </td>\n	  	<td>\n  	     <img src="http://www.phpintl.org/images/no_image.jpg"> <br />\n   \n   		<strong>Co-Signer Name</strong> <br />\n     <strong>Address </strong> <br />\n		   <br/>\n		   ,   	</td>\n          \n         \n  \n  	    </tr>\n    </table>\n  \n    <div class="clearfix"></div>\n\n		    \n		    		    \n	\n			\n          <strong>Loan Name </strong>Buying produce <br />\n          <strong>Posted Date</strong> 24 Jun 14 <br />\n		  <strong>Requested for funds Exchange Rate</strong> $ 1 USD -> 1 USD<br />\n		  <strong>Amount requested</strong>         500.00 USD <br />\n		  <br />\n		  <strong>Exchange Rate of transfer</strong> $ 1 USD -> 1 USD<br />\n		  <strong>Amount Provided</strong>         500.00 USD <br />\n		  <strong>Amount Diffrence</strong>           0.00 USD <br />\n		 \n		  \n						\n			    		'),
(16, 27, 155, 'USD', 'Intial Deposit for Making Music ', 800, 800, 0, 1, '2014-06-24 17:02:24', '<style type="text/css">\n	.total td{\n		font-weight: bold;\n	}\n	img{ height: 200px; float: left;}\n	table tr td{border: 1px solid black; }\n	thead tr td{border:2px solid black; font-weight: bold; background-color: yellow; }\n	\n	table{ border-collapse: collapse; }	\n	.client tr td{border: none;}\n	.client img{ height: 150px;}\n</style>\n    <table class="client">\n    <tr style="padding-top:10px;">\n    	<td>\n   <img src="http://www.phpintl.org/imageupload/server/php/files/Sam%2BSmith%2BCapital%2BFM%2BJingle%2BBell%2BBall%2BDay%2BVHuVimXrXRHl.jpg">\n   <br />\n   \n  \n     <strong>Collection Location</strong>  <br />\n     <strong>Name</strong> Sam Smith<br />\n     <strong>Address </strong> <br />\n		   <br/>\n		   ,     </td>\n	  	<td>\n  	     <img src="http://www.phpintl.org/images/no_image.jpg"> <br />\n   \n   		<strong>Co-Signer Name</strong> <br />\n     <strong>Address </strong> <br />\n		   <br/>\n		   ,   	</td>\n          \n         \n  \n  	    </tr>\n    </table>\n  \n    <div class="clearfix"></div>\n\n		    \n		    		    \n	\n			\n          <strong>Loan Name </strong>Making Music <br />\n          <strong>Posted Date</strong> 24 Jun 14 <br />\n		  <strong>Requested for funds Exchange Rate</strong> $ 1 USD -> 1 USD<br />\n		  <strong>Amount requested</strong>         800.00 USD <br />\n		  <br />\n		  <strong>Exchange Rate of transfer</strong> $ 1 USD -> 1 USD<br />\n		  <strong>Amount Provided</strong>         800.00 USD <br />\n		  <strong>Amount Diffrence</strong>           0.00 USD <br />\n		 \n		  \n						\n			    		'),
(17, 27, 153, 'USD', 'Intial Deposit for Doe dubby', 1000, 1000, 0, 1, '2014-06-24 17:02:28', '<style type="text/css">\n	.total td{\n		font-weight: bold;\n	}\n	img{ height: 200px; float: left;}\n	table tr td{border: 1px solid black; }\n	thead tr td{border:2px solid black; font-weight: bold; background-color: yellow; }\n	\n	table{ border-collapse: collapse; }	\n	.client tr td{border: none;}\n	.client img{ height: 150px;}\n</style>\n    <table class="client">\n    <tr style="padding-top:10px;">\n    	<td>\n   <img src="http://www.phpintl.org/images/no_image.jpg">\n   <br />\n   \n  \n     <strong>Collection Location</strong> United States <br />\n     <strong>Name</strong> John Doe<br />\n     <strong>Address </strong> <br />\n		  12 me road <br/>\n		  rochester New York, 14450    </td>\n	  	<td>\n  	     <img src="http://www.phpintl.org/images/no_image.jpg"> <br />\n   \n   		<strong>Co-Signer Name</strong> <br />\n     <strong>Address </strong> <br />\n		   <br/>\n		   ,   	</td>\n          \n         \n  \n  	    </tr>\n    </table>\n  \n    <div class="clearfix"></div>\n\n		    \n		    		    \n	\n			\n          <strong>Loan Name </strong>Doe dubby<br />\n          <strong>Posted Date</strong> 24 Jun 14 <br />\n		  <strong>Requested for funds Exchange Rate</strong> $ 1 USD -> 1 USD<br />\n		  <strong>Amount requested</strong>        1000.00 USD <br />\n		  <br />\n		  <strong>Exchange Rate of transfer</strong> $ 1 USD -> 1 USD<br />\n		  <strong>Amount Provided</strong>        1000.00 USD <br />\n		  <strong>Amount Diffrence</strong>           0.00 USD <br />\n		 \n		  \n						\n			    		'),
(18, 27, 156, 'USD', 'Intial Deposit for Making Movies ', 400, 400, 0, 1, '2014-06-24 17:02:30', '<style type="text/css">\n	.total td{\n		font-weight: bold;\n	}\n	img{ height: 200px; float: left;}\n	table tr td{border: 1px solid black; }\n	thead tr td{border:2px solid black; font-weight: bold; background-color: yellow; }\n	\n	table{ border-collapse: collapse; }	\n	.client tr td{border: none;}\n	.client img{ height: 150px;}\n</style>\n    <table class="client">\n    <tr style="padding-top:10px;">\n    	<td>\n   <img src="http://www.phpintl.org/imageupload/server/php/files/robert-downey-300.jpg">\n   <br />\n   \n  \n     <strong>Collection Location</strong>  <br />\n     <strong>Name</strong> Robert Jones <br />\n     <strong>Address </strong> <br />\n		   <br/>\n		   ,     </td>\n	  	<td>\n  	     <img src="http://www.phpintl.org/images/no_image.jpg"> <br />\n   \n   		<strong>Co-Signer Name</strong> <br />\n     <strong>Address </strong> <br />\n		   <br/>\n		   ,   	</td>\n          \n         \n  \n  	    </tr>\n    </table>\n  \n    <div class="clearfix"></div>\n\n		    \n		    		    \n	\n			\n          <strong>Loan Name </strong>Making Movies <br />\n          <strong>Posted Date</strong> 24 Jun 14 <br />\n		  <strong>Requested for funds Exchange Rate</strong> $ 1 USD -> 1 USD<br />\n		  <strong>Amount requested</strong>         400.00 USD <br />\n		  <br />\n		  <strong>Exchange Rate of transfer</strong> $ 1 USD -> 1 USD<br />\n		  <strong>Amount Provided</strong>         400.00 USD <br />\n		  <strong>Amount Diffrence</strong>           0.00 USD <br />\n		 \n		  \n						\n			    		'),
(19, 27, 157, 'USD', 'Intial Deposit for Growing pumpkin', 1200, 1200, 0, 1, '2014-06-24 17:02:31', '<style type="text/css">\n	.total td{\n		font-weight: bold;\n	}\n	img{ height: 200px; float: left;}\n	table tr td{border: 1px solid black; }\n	thead tr td{border:2px solid black; font-weight: bold; background-color: yellow; }\n	\n	table{ border-collapse: collapse; }	\n	.client tr td{border: none;}\n	.client img{ height: 150px;}\n</style>\n    <table class="client">\n    <tr style="padding-top:10px;">\n    	<td>\n   <img src="http://www.phpintl.org/imageupload/server/php/files/download.jpeg">\n   <br />\n   \n  \n     <strong>Collection Location</strong> hell <br />\n     <strong>Name</strong> Jack Skellington<br />\n     <strong>Address </strong> <br />\n		   <br/>\n		   ,     </td>\n	  	<td>\n  	     <img src="http://www.phpintl.org/images/no_image.jpg"> <br />\n   \n   		<strong>Co-Signer Name</strong> <br />\n     <strong>Address </strong> <br />\n		   <br/>\n		   ,   	</td>\n          \n         \n  \n  	    </tr>\n    </table>\n  \n    <div class="clearfix"></div>\n\n		    \n		    		    \n	\n			\n          <strong>Loan Name </strong>Growing pumpkin<br />\n          <strong>Posted Date</strong> 24 Jun 14 <br />\n		  <strong>Requested for funds Exchange Rate</strong> $ 1 USD -> 1 USD<br />\n		  <strong>Amount requested</strong>        1200.00 USD <br />\n		  <br />\n		  <strong>Exchange Rate of transfer</strong> $ 1 USD -> 1 USD<br />\n		  <strong>Amount Provided</strong>        1200.00 USD <br />\n		  <strong>Amount Diffrence</strong>           0.00 USD <br />\n		 \n		  \n						\n			    		'),
(20, 8, 46, 'GHS', 'Intial Deposit for DELICIOUS PORRIDGE', 500, 1608.75, 1608.75, 3.2175, '2014-11-17 14:46:27', '<style type="text/css">\n	.total td{\n		font-weight: bold;\n	}\n	img{ height: 200px; float: left;}\n	table tr td{border: 1px solid black; }\n	thead tr td{border:2px solid black; font-weight: bold; background-color: yellow; }\n	\n	table{ border-collapse: collapse; }	\n	.client tr td{border: none;}\n	.client img{ height: 150px;}\n</style>\n<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">\n\n<h4>A PHP Error was encountered</h4>\n\n<p>Severity: Warning</p>\n<p>Message:  Invalid argument supplied for foreach()</p>\n<p>Filename: admin/printClient_html.php</p>\n<p>Line Number: 19</p>\n\n</div>\n		    \n		    		');

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `total` double NOT NULL,
  `method` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `seed` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=104 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `userid`, `total`, `method`, `seed`, `status`, `timestamp`) VALUES
(65, 47, 540, 'Check', 0, 'Approve', '2013-09-20 13:55:33'),
(64, 39, 0, 'Seed', 250, 'Approve', '2013-09-12 16:59:25'),
(63, 45, 540, 'Check', 0, 'Approve', '2013-09-12 16:41:48'),
(62, 39, 540, 'Check', 0, 'Approve', '2013-09-12 16:39:38'),
(61, 39, 540, 'Check', 0, 'Approve', '2013-09-12 16:34:18'),
(66, 47, 0, 'Seed', 350, 'Approve', '2013-09-26 00:14:15'),
(67, 11, 1123.2, 'Check', 0, 'Approve', '2013-11-07 17:39:32'),
(68, 11, 480.6, 'Paypal', NULL, 'Approve', '2013-12-02 11:38:57'),
(69, 11, 27, 'Paypal', NULL, 'Approve', '2013-12-13 18:00:10'),
(70, 61, 1080, 'Check', 0, 'Approve', '2014-01-13 19:14:19'),
(71, 61, 648, 'Check', 0, 'Approve', '2014-01-13 19:46:27'),
(72, 11, 999, 'Check', 0, 'Approve', '2014-01-15 18:19:26'),
(73, 11, 0, 'Seed', 0, 'Approve', '2014-01-15 18:23:40'),
(74, 11, 0, 'Seed', 0, 'Approve', '2014-01-15 18:26:02'),
(75, 11, 0, 'Seed', 0, 'Approve', '2014-01-15 18:26:52'),
(76, 11, 0, 'Seed', 0, 'Approve', '2014-01-15 18:33:55'),
(77, 11, 0, 'Seed', 0, 'Approve', '2014-01-15 18:43:34'),
(78, 11, 0, 'Seed', 0, 'Approve', '2014-01-15 18:43:43'),
(79, 11, 0, 'Seed', 0, 'Approve', '2014-01-15 18:45:03'),
(80, 11, 0, 'Seed', 0, 'Approve', '2014-01-15 18:45:58'),
(81, 11, 0, 'Seed', 0, 'Approve', '2014-01-15 18:46:58'),
(82, 11, 0, 'Seed', 0, 'Approve', '2014-01-15 18:48:55'),
(83, 11, 0, 'Seed', 0, 'Approve', '2014-01-15 18:50:07'),
(84, 63, 810, 'Check', 0, 'Approve', '2014-01-17 20:24:14'),
(85, 63, 0, 'Seed', 0, 'Approve', '2014-01-17 20:26:26'),
(86, 63, 0, 'Seed', 0, 'Approve', '2014-01-17 20:26:40'),
(87, 63, 27, 'Paypal', NULL, 'Approve', '2014-01-17 20:39:35'),
(91, 65, 108, 'Paypal', NULL, 'Approve', '2014-02-03 16:34:05'),
(90, 64, 540, 'Check', 0, 'Approve', '2014-01-30 18:27:55'),
(92, 65, 108, 'Paypal', NULL, 'Approve', '2014-02-03 16:41:01'),
(93, 11, 27, 'Paypal', NULL, 'Approve', '2014-03-30 19:17:36'),
(94, 75, 1566, 'Check', 0, 'Approve', '2014-06-24 16:51:45'),
(95, 76, 1296, 'Check', 0, 'Approve', '2014-06-24 16:55:19'),
(96, 77, 1350, 'Check', 0, 'Approve', '2014-06-24 16:59:10'),
(97, 37, 0, 'Seed', 500, 'Approve', '2014-08-21 17:03:55'),
(98, 82, 8640, 'Check', 0, 'Approve', '2014-10-23 22:13:10'),
(99, 11, 71, 'Paypal', 550, 'Approve', '2014-11-15 21:20:33'),
(100, 11, 108, 'Paypal', 0, 'Pending', '2014-12-18 19:29:01'),
(101, 56, 189, 'Paypal', NULL, 'Pending', '2015-04-20 16:54:29'),
(102, 56, 27, 'Paypal', NULL, 'Approve', '2015-04-20 16:55:19'),
(103, 90, 27, 'Paypal', NULL, 'Pending', '2015-08-27 16:19:29');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE IF NOT EXISTS `order_items` (
  `id` int(11) NOT NULL,
  `orderid` int(11) NOT NULL,
  `projectid` int(11) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=124 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `orderid`, `projectid`, `amount`) VALUES
(75, 65, 80, 500),
(74, 64, 51, 250),
(73, 63, 53, 300),
(72, 63, 52, 200),
(71, 62, 53, 200),
(70, 62, 52, 300),
(69, 61, 40, 500),
(76, 66, 51, 350),
(77, 67, 104, 520),
(78, 67, 105, 520),
(79, 68, 110, 445),
(80, 69, 51, 25),
(81, 70, 50, 1000),
(82, 71, 126, 600),
(83, 72, 54, 25),
(84, 72, 56, 350),
(85, 72, 71, 25),
(86, 72, 89, 25),
(87, 72, 49, 500),
(88, 84, 40, 250),
(89, 84, 49, 250),
(90, 84, 48, 250),
(91, 87, 56, 25),
(92, 88, 46, 500),
(93, 89, 106, 522),
(94, 90, 127, 500),
(95, 91, 10, 100),
(96, 92, 128, 100),
(97, 93, 40, 25),
(98, 94, 154, 100),
(99, 94, 155, 250),
(100, 94, 153, 100),
(101, 94, 156, 150),
(102, 94, 157, 850),
(103, 95, 154, 200),
(104, 95, 155, 250),
(105, 95, 153, 500),
(106, 95, 156, 125),
(107, 95, 157, 125),
(108, 96, 154, 200),
(109, 96, 155, 300),
(110, 96, 153, 400),
(111, 96, 156, 125),
(112, 96, 157, 225),
(113, 97, 10, 500),
(114, 98, 40, 8000),
(115, 99, 40, 150),
(116, 99, 54, 125),
(117, 99, 46, 150),
(118, 99, 160, 75),
(119, 99, 146, 75),
(120, 100, 55, 100),
(121, 101, 48, 175),
(122, 102, 48, 25),
(123, 103, 48, 25);

-- --------------------------------------------------------

--
-- Table structure for table `organization`
--

CREATE TABLE IF NOT EXISTS `organization` (
  `id` int(11) NOT NULL,
  `organization` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `currency` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `organization`
--

INSERT INTO `organization` (`id`, `organization`, `currency`, `url`, `address`, `city`, `state`, `zip`, `country`, `type`, `logo`) VALUES
(11, 'New NGO', 'USD', NULL, '', '', '', '', '', 'NGO', NULL),
(7, 'People Helping People', 'USD', NULL, '9 church st', 'hornell', 'ny', '14843', 'US', 'NGO', NULL),
(8, 'Kraban Support Foundation', 'GHS', NULL, 'po box kd83', 'accra', 'kanda', '', 'africa', 'NGO', NULL),
(9, 'PNG Suzanne', 'PGK', NULL, '', '', '', '', '', 'NGO', NULL),
(13, 'Sacco Boneza', 'RWF', NULL, '', 'Kinunu', 'Boneza', '', 'Rwanda', 'NGO', NULL),
(14, 'RobNGO', 'RWF', NULL, '', 'boneza', 'kigali', '', 'rwanda', 'NGO', NULL),
(27, 'Make It Happen', 'USD', NULL, '', '', '', '', '', 'NGO', NULL),
(28, 'Make It Happen 2', 'RWF', NULL, '', '', '', '', '', 'NGO', NULL),
(21, 'Rusororo Sacco', 'RWF', NULL, 'Gasabo', 'Kigali', 'Rwanda', '', 'Rwanda', 'NGO', 'http://phpintl.org/imageupload/server/php/files/Ru_sacco_image.png'),
(26, 'Joy to The World ', 'USD', '/j2twproject', '', '', '', '', '', 'sNGO', NULL),
(23, 'growing together africa', 'USD', NULL, '3 Hanover Square Apt 8 k', 'new york', 'new york', '10004', 'United States', 'NGO', NULL),
(24, 'Hope Training Institute', 'GHS', NULL, 'Box GP 18169', 'Accra', 'Ghana', '18169', 'Ghana', 'NGO', NULL),
(25, 'Sinapi Aba', 'GHS', NULL, 'Sinapi Aba', 'Kumasi', 'Ghana', '4911', 'Ghana', 'NGO', NULL),
(29, '', 'USD', '', '', '', '', '', '', 'sNGO', NULL),
(30, 'BINGO', 'USD', NULL, '263 Central', 'Roic', '', '', '', 'NGO', NULL),
(31, 'PMFL', 'USD', NULL, '93 Harper Ave', 'Chautauqua', 'New York', '14722', 'USA', 'NGO', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `organization_updates`
--

CREATE TABLE IF NOT EXISTS `organization_updates` (
  `id` int(11) unsigned NOT NULL,
  `organization_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phpid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` decimal(15,0) NOT NULL,
  `ngoamount` double(15,0) DEFAULT NULL,
  `actualamount` double(15,0) DEFAULT NULL,
  `rate` double NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `currency` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `loanterms` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `loantermstype` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `startdate` date DEFAULT NULL,
  `loandate` date DEFAULT NULL,
  `loaninterest` double DEFAULT NULL,
  `lateterms` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lateamount` double DEFAULT NULL,
  `use` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `pics` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `organization_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `public` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fundingsource` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `php_fund_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=178 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `client_id`, `group_id`, `title`, `type`, `name`, `phpid`, `amount`, `ngoamount`, `actualamount`, `rate`, `category`, `location`, `country`, `currency`, `loanterms`, `loantermstype`, `startdate`, `loandate`, `loaninterest`, `lateterms`, `lateamount`, `use`, `description`, `pics`, `video`, `organization_id`, `postdate`, `public`, `fundingsource`, `php_fund_status`) VALUES
(52, NULL, NULL, 'OCCASION BEADS', 'Micro', 'Victoria Tetteh', 'M52', '500', NULL, NULL, 2.14, 'Small Trade', 'Accra', 'Ghana', 'GHS', '24', NULL, '2013-08-20', '2013-08-20', 20, NULL, NULL, 'Victoria is seeking some help to buy more supplies so that she can expand her production and increase profit. ', '\n      \n      Victoria Tetteh makes beads and other products for local and foreign markets. She is seeking a loan of $500.00 from PHPI to help her expand her production. She has been a client of Kraban Support Foundation and has had several training sessions on simple record keeping and loan management from the organization.\n      \n            ', '/imageupload/server/php/files/Victoria%20Tetteh%20%281%29.jpg', '', '8', '2014-11-13 21:05:17', 'Closed', 'PHP', NULL),
(53, NULL, NULL, 'ABC HOT KENKEY', 'Micro', 'Agnes', 'M53', '500', NULL, NULL, 2.14, 'Food', 'Nkurankan', 'Ghana', 'GHS', '24', NULL, '2013-08-20', '2013-08-20', 20, NULL, NULL, 'Agnes is looking to feed more kids, but She needs to buy more food and supplies.', '\n      \n      Agnes is a mother of four children who lives in Nkurankan, Ghana and has been a client of Kraban Support Foundation for years. She cooks kenkey and fried fish (a local meal) and sells it to school children in her town. She is seeking a loan of $500 from PHPI to help her cook enough for sale. She has had several training sessions on simple record keeping and loan management from the Kraban Support Foundation.&nbsp;        ', '/imageupload/server/php/files/Agnes%20%281%29.JPG', '', '8', '2014-11-13 21:07:30', 'no', 'PHP', NULL),
(51, NULL, NULL, 'HOPE IN GOD FOOD', 'Micro', 'Madame Vida', 'M51', '550', NULL, NULL, 2.14, 'Food', 'Accra', 'Ghana', 'GHS', '24', NULL, '2013-08-20', '2013-08-20', 20, NULL, NULL, 'Mademe Vida sells wakye to the villagers. She wants to open a restaurant.', '\n      \n      Madame Vida sells wakye, a local meal prepared from special rice and beans and served with vegetable stew. She buys her rice and beans directly from the producers in bags. A lot of people enjoy her meals and she has a large number of people who buy from her. She wants to open a mini-restaurant where she can add other local dishes to her meals. She is seeking a loan of 550 to expand her business.&nbsp;        ', '/imageupload/server/php/files/Vida%20%281%29.JPG', '', '8', '2014-11-13 21:04:17', 'Closed', 'PHP', NULL),
(50, NULL, NULL, 'GOD IS GOOD FOODSTUFF', 'Micro', 'Sarah', 'M50', '550', NULL, NULL, 2.14, 'Agriculture', 'Accra', 'Ghana', 'GHS', '24', NULL, '2013-08-20', '2013-08-20', 20, NULL, NULL, 'Sarah is looking for an opportunity to increase her plantain and cassava business. ', '\n      \n      Sarah has been a client of Kraban Support Foundation for years now. She has had several training sessions on simple record keeping and loan management from the organization. She sells agricultural products such as cassava and plantain. She is seeking a loan of $550.00 from PHPI to help her expand her business.            ', '/imageupload/server/php/files/Sarah%20%281%29.JPG', '', '8', '2014-11-13 21:02:53', 'Funded', 'PHP', 'Deposited on Mar 12,2014'),
(40, NULL, NULL, 'TUFI MEDICAL CLINIC WATER STORAGE TANK', 'Project', 'Borna Joyce Lekong', 'P40', '8000', NULL, NULL, 6828, 'Service', 'Tufi', 'Papau New Guinea', 'GNF', '24', NULL, '2013-08-20', '2013-08-20', 20, NULL, NULL, 'PURCHASE A NEW WATER STORAGE TANK', '\n      \n      The Tufi Medical Clinic is in desperate need of $8,000 for a water tank; the existing one has a gaping, one-foot rusted hole in its side. The tank is beyond repair and Wayne Dicker, Operations Manager of the nearby the Tufi Lodge in Papua New Guinea, will arrange for the purchase, shipment and installation with the help of the native villagers. The clinic is staffed by several native nurses who are dedicated to serving the surrounding villages. We observed a number of cases&nbsp;where having clean water storage would help the patient''s prognosis. These included a&nbsp;young girl who had tuberculosis and malaria which kept her from keeping food down and&nbsp;a woman could not produce any milk to feed her new born. Without a water supply it is extremely difficult, especially during the dry period of the year, to provide quality care for these people.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ', '/imageupload/server/php/files/P1020141%20%283%29.JPG', '', '7', '2014-11-16 01:42:41', 'Funded', 'PHP', NULL),
(47, NULL, NULL, 'REGGIE''S CORNER SHOP', 'Micro', 'Regina Asare', 'M47', '550', NULL, NULL, 2.14, 'Small Trade', 'Pokuase', 'Ghana', 'GHS', '24', NULL, '2013-08-20', '2013-08-20', 20, NULL, NULL, 'Regina plans to start her own boutique store where she can make clothing to sell.', '\n      \n      Regina''s dream is to open a modern boutique in her community. She lives in Pokuase, Ghana. She has a shop, but she has no money to buy clothing to sell in her shop. She is seeking a loan of $550.00 from PHPI to enable her to realize her dream. She has &nbsp;been trained in the skills lending program of Kraban Support Foundation.\n      \n                    ', '/imageupload/server/php/files/Regina%20Asare%20%281%29.JPG', '', '8', '2014-11-13 20:59:27', 'Approved', 'PHP', NULL),
(10, NULL, NULL, 'SPORTS FACILITIES', 'Project', 'Kids Play International', 'p1', '12000', NULL, NULL, 2.14, 'Service', 'Gatagara', 'Rwanda', 'GHS', '24', NULL, '2013-08-20', '2013-08-20', 20, NULL, NULL, 'BUILD OR REFURBISH SPORTS FACILITIES', '\n      \n      \n      \n      \n      Through sports activity and interactive discussion, the&nbsp;<span style=''margin: 0px; padding: 0px; outline: 0px; border: 0px currentColor; border-image: none; color: rgb(85, 85, 85); line-height: 21px; font-family: "Helvetica Neue", Helvetica, sans-serif; vertical-align: baseline;''><span style="margin: 0px; padding: 0px; outline: 0px; border: 0px currentColor; border-image: none; vertical-align: baseline; background-color: transparent;"><b style="font-style: italic;">Let''s Play Fair! Community sports&nbsp;</b><a title="Click to Continue > by Supreme Savings" id="_GPLITA_0" style="margin: 0px; padding: 0px; outline: 0px; border: 0px currentColor; border-image: none; color: rgb(240, 95, 34); text-decoration: underline; vertical-align: baseline; background-color: transparent;" href="http://www.kidsplayintl.org/programs/lets-play/#" in_hdr="" in_rurl="http://i.trkjmp.com/click?v=VVM6MzYzODY6MTEyNTplZHVjYXRpb24gcHJvZ3JhbTo2MTEwYWI2OTczOGJiOTcwNmJkMjA3NThkMGY5ZGY3Mjp6LTE0ODgtMTc0MjY0Ond3dy5raWRzcGxheWludGwub3JnOjQwMjU1OjRiMGU2NjIyMjI5NWYwYTNhNjdiMjEyNDA3ODMxNjU3">education program</a></span></span><span style=''color: rgb(85, 85, 85); line-height: 21px; font-family: "Helvetica Neue", Helvetica, sans-serif;''>&nbsp;teaches&nbsp;</span><strong style=''margin: 0px; padding: 0px; outline: 0px; border: 0px currentColor; border-image: none; color: rgb(85, 85, 85); line-height: 21px; font-family: "Helvetica Neue", Helvetica, sans-serif; vertical-align: baseline;''>boys and girls</strong><span style=''color: rgb(85, 85, 85); line-height: 21px; font-family: "Helvetica Neue", Helvetica, sans-serif;''>&nbsp;important life lessons and values embodied in the Olympic ideals, such as; teamwork, good sportsmanship, fair play, gender&nbsp;</span><a title="Click to Continue > by Supreme Savings" id="_GPLITA_2" style=''margin: 0px; padding: 0px; outline: 0px; border: 0px currentColor; border-image: none; color: rgb(240, 95, 34); line-height: 21px; font-family: "Helvetica Neue", Helvetica, sans-serif; text-decoration: underline; vertical-align: baseline;'' href="http://www.kidsplayintl.org/programs/lets-play/#" in_rurl="http://i.trkjmp.com/click?v=VVM6MjEzMTY6MTEyNTplcXVpdHk6N2IzNmExZDZhMjU0OTU2ZThlZTMwOGI4NWQ3Zjc0YWQ6ei0xNDg4LTE3NDI2NDp3d3cua2lkc3BsYXlpbnRsLm9yZzoxNTk3MDo1MjIxYTFmZDE2NDg0NWQ5MmIwNDljNjIwYjFmNjAxZQ">equity</a><span style=''color: rgb(85, 85, 85); line-height: 21px; font-family: "Helvetica Neue", Helvetica, sans-serif;''>, respect, and problem-solving. The objective is to build a sustainable community sports education program that will promote gender equity between both boys and girls. At the moment we are in need of building a facility to store sports equipment and a place for kids to play.</span>                                ', '/imageupload/server/php/files/kids_play%20%281%29.jpg', '', '7', '2014-11-13 17:36:07', 'Approved', 'PHP', NULL),
(49, NULL, NULL, 'MEGYEFO TEASE SHOP', 'Micro', 'Salomey', 'M49', '550', NULL, NULL, 2.14, 'Small Trade', 'Accra', 'Ghana', 'GHS', '24', NULL, '2013-08-20', '2013-08-20', 20, NULL, NULL, 'Salomey wants to increase her sales by carrying a wider variety of products in her store.', '\n      \n      \n      \n      Salomey has been a client of Kraban Support Foundation for several years now. She has had several training sessions on simple record keeping and loan management from the organization. She sells provisions and other items in her shop. She is seeking a loan of $550.00 from PHPI to enable her to expand her sales.\n      \n                    ', '/imageupload/server/php/files/Salomey%20%281%29.JPG', '', '8', '2014-11-13 21:02:00', 'Funded', 'PHP', 'Deposited on Mar 12,2014'),
(48, NULL, NULL, 'GOD IS GREAT SHOP', 'Micro', 'Rose D', 'M48', '500', NULL, NULL, 2.14, 'Food', 'Accra', 'Ghana', 'GHS', '24', NULL, '2013-08-20', '2013-08-20', 20, NULL, NULL, 'Rose needs funding to help her buy more ingredients to build her business. ', '\n      \n      \n      Rose D. sells rice, groundnut (in tins of olonka) and cooking ingredients at the market. She is seeking a loan of $500.00 from PHPI. She has had several training sessions on simple record keeping and loan management from the organization. She has been a client of Kraban Support Foundation for several years now.\n      \n                ', '/imageupload/server/php/files/Rose%20D.%20%281%29.JPG', '', '8', '2014-11-13 21:00:35', 'Approved', 'PHP', NULL),
(54, NULL, NULL, 'ASEDA SHOP', 'Micro', 'Auntie B', 'M54', '550', NULL, NULL, 2.14, 'Food', 'Accra', 'Ghana', 'GHS', '24', NULL, '2013-08-20', '2013-08-20', 20, NULL, NULL, 'Auntie B is selling commodities to the community from a small table top. She is looking to expand and have a mini store front.', '\n      \n      Auntie B, as she is called in her community, sells a variety of food items from a table top shop. Her sales are expanding and she wishes to open a mini-supermarket where she can sell her goods. As a hardworking mother, she is seeking a loan of $550 from PHPI to help expand her business and achieve her dream. She is a client of Kraban Support Foundation.&nbsp;        ', '/imageupload/server/php/files/Auntie%20Beauty.JPG', '', '8', '2014-11-13 21:09:02', 'Approved', 'PHP', NULL),
(46, NULL, NULL, 'DELICIOUS PORRIDGE', 'Micro', 'Patience Odame', 'M46', '500', NULL, NULL, 2.14, 'Food', 'Accra', 'Ghana', 'GHS', '24', NULL, '2013-08-20', '2013-08-20', 20, NULL, NULL, 'Patience Odame is looking to look for some extra funding to help her expand her porridge shop', '\n      \n      \n      Patience Odame is a 42-year-old woman with 3 children. She is a pioneering client of Kraban Support Foundation (KSF) and has gone through several loan management programs with them. She sells porridge and "koose" (made from beans) in a central community location, and her many customers start their day buying breakfast from her. They buy from her because they know the food is very tasty and is prepared with careful attention to hygiene. She requests a loan of $500.00 so she can buy more bags of beans and maize and serve more customers.\n      \n                ', '/imageupload/server/php/files/Patience%20Odame%20%282%29.JPG', '', '8', '2014-11-17 14:46:27', 'Funded', 'PHP', 'Deposited on Nov 17,2014'),
(55, NULL, NULL, 'EASY WEAR SHOP', 'Micro', 'Agnes Owusu', 'M55', '550', NULL, NULL, 2.14, 'Small Trade', 'Accra', 'Ghana', 'GHS', '24', NULL, '2013-08-20', '2013-08-20', 20, NULL, NULL, 'Agnes takes in used shoes and refurbishes them for the community.', '\n      \n      \n      Agnes(the seated lady) sells second-hand footwear in the market. She displays her goods on the gound in the market for sale. She used to receive the goods in several bales but due to the insecure nature of her items, she is not able to receive any more goods even though demand is high. She is in need of a $550 loan because she wants to open a shop where she can keep and sell her footwear. She has been a faithful client of Kraban Support Foundation.&nbsp;            ', '/imageupload/server/php/files/Agnes%20Owusu.JPG', '', '8', '2014-11-13 21:09:54', 'Approved', 'PHP', NULL),
(56, NULL, NULL, 'NYAME BEKYERE CHARCOAL', 'Micro', 'Auntie Lizzy', 'M56', '550', NULL, NULL, 2.14, 'Service', 'Kasoah', 'Ghana', 'GHS', '24', NULL, '2013-08-20', '2013-08-20', 20, NULL, NULL, 'Elizabeth supplies her town with charcoal so that families in the community are able to cook and boil water.', '\n      \n      \n      Auntie Lizzy lives in Amanfrom, Kasoah, Ghana, where she sells charcoal to her community. Since this is the main source of fuel in most homes, demand for her product is very high. She is seeking a loan of $550 from PHPI to enable her to buy more bags of charcoal in order to serve her community. She has been trained by Kraban Support Foundation to use the teach lending tool in keeping simple records of her financial and business reports.&nbsp;            ', '/imageupload/server/php/files/Elizabeth.JPG', '', '8', '2014-11-13 21:11:34', 'Approved', 'PHP', NULL),
(57, NULL, NULL, 'ROTALA FARMS', 'Micro', 'Akosua Takyiwa', 'M57', '500', NULL, NULL, 2.14, 'Agriculture', 'Asuboi', 'Ghana', 'GHS', '24', NULL, '2013-08-20', '2013-08-20', 20, NULL, NULL, 'Akosua Takyiwa produces fruits and vegetables for sale on the local markets and communities.', '\n      \n      Akosua Takiwa grows produce to supply local markets, educational institutions, and a health center. The target clients include children, patients of the local health centers, the 5 surrounding communities at large and 1 major urban center. She has received farm business skills training based on Kraban Support Foundation''s Teach Lending Strategy. This methodology involves providing her the needed skills in farm business record keeping, daily sales, profits and working capital and farm produce marketing skills. She has also acquired training in fruit processing to make fruit juices to sell to school children to help improve their diets to enable them to be better in school.&nbsp;        ', '/imageupload/server/php/files/Akosua%20Koley.jpg', '', '8', '2014-11-13 21:12:42', 'Approved', 'PHP', NULL),
(58, NULL, NULL, 'SPECIAL BREAD', 'Micro', 'Agnes Nota', 'M58', '600', NULL, NULL, 2.14, 'Food', 'Accra', 'Ghana', 'GHS', '24', NULL, '2013-08-20', '2013-08-20', 20, NULL, NULL, 'Agnes is looking for help to buy more supplies to make bread to sell to her community to raise enough money to send her children to school', '\n      \n      Born in 1970, Agnes Nota is married and has four children, She has been very hard working from the day she received her first loan. She explained how good it is for a women to work for herself and care for her children, even if her work is just the buying and selling of bread. Her desire is to work for her children''s education and pay for their apprenticeships so that they may learn a trade. Agnes decided to apply for a loan from Kraban Support Foundation due to her inability to get investment capital that could give her a higher income to meet her children''s needs and those of the family.&nbsp;        ', '/imageupload/server/php/files/Agnes%20Nota.jpg', '', '8', '2014-11-13 21:13:26', 'Approved', 'PHP', NULL),
(59, NULL, NULL, 'BABY''S AFRICAN BEADS', 'Micro', 'Baby', 'M59', '500', NULL, NULL, 2.14, 'Manufacturing', 'Accra', 'Ghana', 'GHS', '24', NULL, '2013-08-20', '2013-08-20', 20, NULL, NULL, 'Baby makes different type of beads for local and foreign markets. This is a hot commodity in the community where she lives.', '\n      \n      \n      Baby makes beads for local and foreign markets. She is seeking a loan of $500 from PHPI to help her expand her production. As a client of Kraban Support Foundation, she has learned several training sessions on simple record keeping and loan management from the organization.&nbsp;            ', '/imageupload/server/php/files/Baby.jpg', '', '8', '2014-11-13 21:14:20', 'Approved', 'PHP', NULL),
(60, NULL, NULL, 'BECKY''S DRINKING SPOT', 'Micro', 'Rebecca', 'M60', '500', NULL, NULL, 2.14, 'Service', 'Accra', 'Ghana', 'GHS', '24', NULL, '2013-08-20', '2013-08-20', 20, NULL, NULL, 'Rebecca is looking for a loan to help her keep a decent-sized inventory for her customers. ', '\n      \n      \n      Rebecca operates a drinking spot in her community. She sells a variety of alcoholic and non-alcoholic beverages ranging from local to foreign. Becky has participated in the Teach Lending Skills training of Kraban Support Foundation and therefore asks for a loan amount of $500 from PHPI to enable her to expand and improve upon her business by purchasing more drinks to sell to her customers.&nbsp;            ', '/imageupload/server/php/files/Rebecca.jpg', '', '8', '2014-11-13 21:15:05', 'Approved', 'PHP', NULL),
(61, NULL, NULL, 'ALPHA AND OMEGA SHOP', 'Micro', 'Philomena', 'M61', '550', NULL, NULL, 2.14, 'Service', 'Accra', 'Ghana', 'GHS', '24', NULL, '2013-08-20', '2013-08-20', 20, NULL, NULL, 'Philomena sells sewing materials to seamstresses in her community.', '\n      \n      \n      \n      Philomena sells stationary, sewing materials and school supplies to the people in her &nbsp;community. She is a client of Kraban Support Foundation and is seeking a loan of $550 so that she can buy more inventory to offer more to her customers and increase her profits.&nbsp;                ', '/imageupload/server/php/files/Philomena.jpg', '', '8', '2014-11-13 21:15:41', 'Approved', 'PHP', NULL),
(62, NULL, NULL, 'DWEN HWE KEN SMOKED FISH', 'Micro', 'Cecilia ', 'M62', '500', NULL, NULL, 2.14, 'Food', 'Accra', 'Ghana', 'GHS', '24', NULL, '2013-08-20', '2013-08-20', 20, NULL, NULL, 'Cecilia wants to provide her community with more flavored smoked fish.', '\n      \n      Cecilia has been a client of Kraban Support Foundation for over 5 years. She has had several training sessions on simple record keeping and loan management from the organization. She sells smoked dry fish in the market in which is the people''s choice of meat. She is seeking a loan of $500 from PHPI to expand her production and sales.&nbsp;        ', '/imageupload/server/php/files/Cecilia.JPG', '', '8', '2014-11-13 21:16:42', 'Approved', 'PHP', NULL),
(63, NULL, NULL, 'ABUNDANT GRACE SPECIAL FOOD', 'Micro', 'Doris Ablorh', 'M63', '500', NULL, NULL, 2.14, 'Food', 'Nkurakan', 'Ghana', 'GHS', '24', NULL, '2013-08-20', '2013-08-20', 20, NULL, NULL, 'Doris is in need of more ingredients to make and sell kenkey, which is one of the community''s favorite meals.', '\n      \n      <span style="color: rgb(0, 0, 0); line-height: normal; font-family: arial, tahoma, sans-serif; background-color: rgb(255, 255, 239);"><font size="3">Nkurankan is a large marking centre for the people of the Yilo Krobo District of Ghana. Doris Ablorh is a resident of Nkurankan and sells "kenkey", which is a fermented food prepared from corn dough. She is a client of Kraban Support Foundation and is seeking a PHP loan of $500.00 to help her improve her business and support her children education</font></span>\n      \n            ', '/imageupload/server/php/files/Doris%20Ablorh.jpg', '', '8', '2014-11-13 21:17:26', 'Approved', 'PHP', NULL),
(64, NULL, NULL, 'EYE ADOM SHOP', 'Micro', 'Auntie Grace ', 'M64', '500', NULL, NULL, 2.14, 'Food', 'Accra', 'Ghana', 'GHS', '24', NULL, '2013-08-20', '2013-08-20', 20, NULL, NULL, 'Auntie Grace wishes to buy more produce so that she will have a greater variety to sell to her community.', '\n      \n      Auntie Grace sells vegetables and other ingredients used in food preparation. She sells tomatoes, pepper, garden eggs, salt, onions and cooking oil from palm fruits. She is unable to provide as much as her community demand because of investment capital to buy the item from wholesalers. She is looking for a loan so that she will be able to buy more from the wholesalers and increase her sales.    ', '/imageupload/server/php/files/Auntie%20Grace.JPG', '', '8', '2014-11-13 21:18:20', 'Approved', 'PHP', NULL),
(65, NULL, NULL, 'GOD''S GRACE SHOP', 'Micro', 'Patience', 'M65', '550', NULL, NULL, 2.14, 'Service', 'Accra', 'Ghana', 'GHS', '24', NULL, '2013-08-20', '2013-08-20', 20, NULL, NULL, 'Auntie Pat sells necklaces, earrings, body and hair pomades, artificial hair and so on. ', '\n      \n      \n      Auntie Pat lives in a community where women predominate and so daily demands for goods by individuals is very high. She is seeking a loan for $550 from PHPI to purchase more products to meet her customers'' demands.&nbsp;            ', '/imageupload/server/php/files/Patience%20%283%29.JPG', '', '8', '2014-11-13 21:18:58', 'Approved', 'PHP', NULL),
(66, NULL, NULL, 'SPECIAL WAKYE', 'Micro', 'Auntie Victoria', 'M66', '500', NULL, NULL, 2.14, 'Food', 'Accra', 'Ghana', 'GHS', '24', NULL, '2013-08-20', '2013-08-20', 20, NULL, NULL, 'Victoria is in need of a loan to help her buy more ingredients to make wakye, a special dish the community enjoys.', '\n      \n      \n      Auntie Victoria (lady sitting) sells "wakye", a meal enjoyed by the locals and prepared from special rice and beans. She is a client of Kraban support Foundation and has completed several training programs that teach good business practices. She is a good manager of loans and knows how to use it to her full advantage to grow her business.&nbsp;            ', '/imageupload/server/php/files/Auntie%20Victoria.JPG', '', '8', '2014-11-13 21:19:41', 'Approved', 'PHP', NULL),
(67, NULL, NULL, 'PATTY''S ONIONS', 'Micro', 'Patience Badu', 'M67', '500', NULL, NULL, 2.14, 'Agriculture', 'Accra', 'Ghana', 'GHS', '24', NULL, '2013-08-20', '2013-08-20', 20, NULL, NULL, 'Patience grows and sells onions to her local market. She is looking to expand her growing operation so that she can provide more onions to the community.', '\n      \n      \n      Patience has been a client of Kraban Support Foundation and has had several training sessions on simple record keeping and loan management from the organization. She grows and sells onions for her local market and operates &nbsp;her own local stand. She is seeking a loan of $500 from PHPI to help her expand her sales and support the family.&nbsp;            ', '/imageupload/server/php/files/Patience%20Badu.JPG', '', '8', '2014-11-13 21:20:13', 'Approved', 'PHP', NULL),
(71, NULL, NULL, 'RAYMOND''S BEETLE NUT BUSINESS', 'Micro', 'Raymond Numdi', 'M71', '4000', NULL, NULL, 6828, 'Agriculture', 'Kingaldurni', 'Papua New Guinea', 'GNF', '24', NULL, '2013-08-20', '2013-08-20', 20, NULL, NULL, 'Raymond seeks a micro loan to help him start a beetle nut retail business. ', '\n      \n      \n      \n      \n      <p style="line-height: 1.15; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="color: rgb(0, 0, 0); font-family: Arial; font-size: 15px; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;">A micro loan of $4000 dollars will enable&nbsp;Raymond Numdi to start a beetle nut business.&nbsp; He will buy local nuts and ship them from the coast of Papua New Guinea via the one inland road to KIngaldurni where there is a vibrant market for them. Raymond speaks excellent English and at school was in the top 10% of his class. He is a member of the Moge Tribe and Agilka clan in Papua New Guinea, located in the Kingaldurni village. He is discouraged as his family has little money and without some financial help he has little opportunity to start up a business and get ahead.&nbsp;Bob Evans,&nbsp;president of PHPI, got to know Raymond while he was in Papua New Guinea and feels he is worthy of receiving a loan which will help increase his income so that he can pay for schooling of his four children.</span></p>\n      \n                        ', '/imageupload/server/php/files/Raymond%20%281%29.jpg', '', '9', '2014-11-13 18:07:59', 'Approved', 'PHP', NULL),
(69, NULL, NULL, 'PRENATAL CARE IN PAPUA NEW GUINEA', 'Project', 'Wayne Dicker', 'P69', '1000', NULL, NULL, 6828, 'Service', 'Port Moresby', 'Papua New Guinea', 'GNF', '24', NULL, '2013-08-20', '2013-08-20', 20, NULL, NULL, 'Women in Papua New Guinea need transportation to major hospitals for prenatal care ', '\n      \n      \n      \n      <span id="docs-internal-guid--f2b1879-9d85-d2b5-7dd8-d0c64cd95dbb"><p style="line-height: 1.15; margin-top: 0pt; margin-bottom: 0pt;" dir="ltr"><span style="color: rgb(0, 0, 0); font-family: Arial; font-size: 15px; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;">The Tufi Medical Clinic is in need of $1000 to provide transportation for prenatal care that is beyond the scope of the clinic''s care. At the Tufi Medical Clinic, Bob Evans, volunteer director of microloans for PHPI, &nbsp;found a young woman about to bear her first child. However, the baby&nbsp;was growing outside of her womb and unless she had an operation she would not survive giving birth to her child. She&nbsp;was &nbsp;in desperate need of transportation by boat to a major hospital in Port Moresby the capital of Papua New Guinea. Wayne Dicker, the operations manager at the nearby Tufi Lodge, arranged transport for her. There are other&nbsp;women who need prenatal care who&nbsp;can be helped through your donation.&nbsp;</span><span style="color: rgb(0, 0, 0); font-family: Arial; font-size: 15px; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;"> </span></p><div><span style="color: rgb(0, 0, 0); font-family: Arial; font-size: 15px; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;"><br></span></div></span>\n      \n                    ', '/imageupload/server/php/files/Untitled-2.jpg', '', '7', '2014-11-20 20:21:50', 'Approved', 'PHP', NULL),
(177, 253, NULL, 'Manufacturing', 'Micro', '', '', '200', 676, 200, 3.38, 'Manufacturing', 'Bibiani', 'Accra', 'GHS', '48', '1', '2015-02-09', '2015-03-09', 10, 'min', 50, 'To expand his BAG PRODUCING BUSINESS', 'Martin is a past trainee of Hope Trainee Institute and has started his his shop after training from the Institute. he needs some capital to expand his business. He is an orphan but with the trainee that he''s received, and with your assistance, he will be able to not only take care of himself but also his siblings as well', '', '', '24', '2015-02-09 17:39:51', 'NGO_pending', 'PHP', NULL),
(104, 13, NULL, 'EXPAND POULTRY FARM', 'Micro', 'Darius Tuyisabe', 'M104', '500', 350000, 520, 673, 'Agriculture', 'Boneza', 'Rwanda', 'RWF', '12', '4', '2013-11-07', '2013-12-14', 19, NULL, NULL, 'A loan to expand my poultry firm business to 100 chickens.', '\n      \n      <p class="MsoNormal">Muraho(hello) from Rwanda! My name is Darius Tuyisabe; I\nam 32 years old and live in a small community of Muramba, Boneza in Rwanda. I\nam married with a son of 2 years old and 3 adopted orphans. I own a poultry farm\nof 30 chickens. The chickens provide eggs to feed the school children in the village.\nI would like to request a loan of $520 dollars to expand my poultry farm\nbusiness to 100 chickens.</p>\n\n<p class="MsoNormal">The profit from my business will be used to pay for medical\ninsurance for my family which is about $5 per person, and &nbsp;pay for school fees for my children. Thank you for supporting my\nbusiness.</p>\n      \n            ', 'http://phpintl.org/imageupload/server/php/files/IMG00167.jpg', '', '13', '2014-11-13 18:13:57', 'Closed', 'PHP', NULL),
(105, 14, NULL, 'EXPAND A SMALL GROCERY STORE', 'Micro', 'Marie Chantel', 'M105', '520', 350000, 520, 673, 'Small Trade', 'Karukamba', 'Rwanda', 'RWF', '12', '4', '2013-11-07', '2013-11-06', 19, NULL, NULL, 'I sell potatoes, beans, cooking oil, salt, and sugar and I would like to expand my grocery store business.', '\n      \n      <p class="MsoListParagraphCxSpFirst">Muraho (hello) from Boneza in Rwanda! My\nname is Marie Chantal. I am 31 years old, married with a husband and 3 children. I\nrun a small grocery store in our neighborhood.&nbsp;\nIn the store, I sell potatoes, beans, cooking oil, salt, and sugar. I\nwould like to request for a loan from People Helping People of $520 to expand\nmy grocery store business.</p>\n\n<p class="MsoListParagraphCxSpLast">The profit from this business will help me to send\nmy children to school, pay for the medical insurance for my family and help\nexpand my business to help the community. Thank you for supporting my business.</p>\n      \n            ', 'http://phpintl.org/imageupload/server/php/files/DSC00149.JPG', '', '13', '2014-11-13 18:26:17', 'Closed', 'PHP', NULL),
(106, 15, NULL, 'RAISE FARM ANIMALS', 'Micro', 'Abraham', 'M106', '522', 350000, 522, 671, 'Small Trade', 'Boneza', 'Rwanda', 'RWF', '12', '4', '2013-11-18', '2013-12-23', 19, NULL, NULL, 'I would like to start raising cows, chickens and goats to provide better food for my family and community.', '\n      \n      \n      <p class="MsoNormal">Muraho (hello) from Boneza in Rwanda! My name is Abraham\nand I am 37 years old. I am married with 3 children. I would like to get a loan of\n$522 to start raising farm animals. I would like to buy cows, chickens\nand goats to be able to provide milk, eggs, and other dairy produce.</p>\n\n<p class="MsoNormal">The profit from my business will help me to send my children\nto school and pay for medical insurance for my family. I would like to provide\nmore and different kinds of service to my community. Thank you and I look forward to your\nsupport.</p>\n      \n                ', 'http://phpintl.org/imageupload/server/php/files/DSC00217.JPG', '', '13', '2014-11-13 18:29:09', 'Approved', 'PHP', NULL),
(125, NULL, NULL, '', '', '', '', '0', NULL, NULL, 0, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '2013-12-09 21:43:39', '', NULL, NULL),
(129, 52, NULL, 'ALEXIS'' PRODUCE FARM', 'Micro', 'Alexis Fidel', '', '500', 340500, 500, 681, 'Agriculture', 'Kigali', 'Rwanda', 'RWF', '6', '4', '2014-02-01', '2014-02-02', 20, NULL, NULL, 'A loan of $500 helps Alexis to purchase fertilizers and support labor costs during maize production.', '\n      \n      Alexis is 34 years old, married, and has five children. She is requesting a loan, which she will use to support labor costs and buy fertilizers during maize production. She needs the fertilizer in order to increase production. With the profits from her farming, she wishes to invest in her business so that in the future she can acquire machines and tractors that will enhance her farming capacity.&nbsp;&nbsp;<div>The agriculture sector accounts for 37% of Rwanda''s gross domestic product, generates 65% of Rwanda''s export revenue, and employs approximately 90% of Rwandans (as of 2009).&nbsp;\nDespite the importance of agriculture to the Rwandan economy, farmers in Rwanda remain under served by financial institutions. Agriculture is viewed as a high-risk proposition because these activities are affected by weather, natural disasters and price fluctuations.\n    </div>    ', 'http://www.phpintl.org/imageupload/server/php/files/images%20%284%29.jpg', '', '21', '2014-11-13 19:35:01', 'Closed', 'PHP', NULL),
(132, 56, NULL, 'HOUSE REHABILITATION FOR RENTAL INCOME', 'Micro', 'Cyaruzima Felicite', '', '500', 340250, 500, 680.5, 'Service', 'RUSORORO', '', 'RWF', '12', '4', '2014-03-05', '2014-05-05', 16, NULL, NULL, 'Felicite needs a loan to rehabilitate her house so she can earn income from rent.', '\n      \n      Cyaruzima Felicite is applying for a $500 loan to refurbish her house so that it will&nbsp; produce from rent. This rent will allow her to pay the&nbsp;&nbsp;chool fees for her children and cover medical insurance as well as other households needs        ', 'http://www.phpintl.org/imageupload/server/php/files/DSC03541%20%281%29.JPG', '', '21', '2014-11-13 19:38:19', 'no', 'PHP', NULL),
(135, 59, NULL, 'GROW MY SMALL SHOP', 'Micro', 'AYABAGABO Froduard', '', '1200', 544400, 800, 680.5, 'Small Trade', 'RUSORORO SECTOR', 'RWANDA', 'RWF', '12', '4', '2014-03-05', '2014-05-05', 16, NULL, NULL, 'I want to expand my shop to provide more goods for my customers.', '<p>\n      \n      \n      \n      \n    AYABAGABO Froduard is requesting $800 to increase his business which he started in 2011.&nbsp;Flodouard sells different items&nbsp;which are in high demand including rice, sugar, oil and beans&nbsp;in his small shop. He has 3 children including the elder daughter who is at the secondary school. &nbsp;With a loan of $1200 he intends to increase the variety of merchandise in his shop and enhance the business. Ayabagabo&nbsp;rents a house&nbsp;for his business&nbsp;and is obliged to also rent another one for the family which affects his monthly income. For him to cover medical insurance, pay school fees and continue the business, he needs to increase the sales from his shop.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>', 'http://www.phpintl.org/imageupload/server/php/files/DSC03550%20%281%29.JPG', '', '21', '2014-11-13 20:23:04', 'no', 'PHP', NULL),
(136, 60, NULL, 'MOTO CYCLE PURCHASE', 'Micro', 'HABIHAKARE Didas ', '', '300', 204150, 300, 680.5, 'Service', 'NYAGAHINGA', '', 'RWF', '12', '4', '2014-03-05', '2014-05-05', 16, NULL, NULL, 'I want to purchase a moto cycle to start a transportation business.', '\n      \n      \n      \n    HABIHAKARE Didas has&nbsp;saved some money that will allow him to buy a&nbsp; moto cycle for a transport business. He needs $300 to add &nbsp;to his initial capita to be able to buy the moto cycle.&nbsp;&nbsp;The moto transport business will help to sustain his family.    ', 'http://www.phpintl.org/imageupload/server/php/files/DSC03543%20%281%29.JPG', '', '21', '2014-11-13 19:46:07', 'no', 'PHP', NULL),
(160, 296, NULL, 'MUSHROOM FARMING PROJECT', 'Micro', 'Mukankima Annonciatta', 'M160', '500', 340250, 500, 680.5, 'Agriculture', 'Nyagahinga', 'Rwanda', 'RWF', '12', '4', '2014-07-21', '2014-09-21', 16, 'balance', 0, 'Annonciatta wants to change from traditional farming to growing mushrooms.', '\n      \n      \n      \n      <p class="MsoNormal" style="line-height: 15pt; margin-bottom: 7.5pt;"><span style="font-family: Helvetica, sans-serif; font-size: 14pt;">Mukankima Annonciatta, aged 46, is the mother\nof 2 boys and 3 daughters, and married to Ntamukunzi Tharcisse, 54.&nbsp; They live under the poverty line because they\nuse traditional farming methods. Having realized that she can''t afford the mechanization\nand irrigation system costs to address this challenge, she would like to invest\n$500 in mushroom farming.<o:p></o:p></span></p><p class="MsoNormal" style="line-height: 15pt; margin-bottom: 7.5pt;"><span style="font-family: Helvetica, sans-serif; font-size: 14pt;">Rich in protein, mushrooms are in high demand\nin the Rusororo sector to fight malnutrition. One kilo sells for between $2.20\nand $2.90. She will be harvesting them 3 times per week and plans to get 36\nkilograms from every round. She plans to harvest them 9 months of the year.<o:p></o:p></span></p><p class="MsoNormal" style="line-height: 15pt; margin-bottom: 7.5pt;"><span style="font-family: Helvetica, sans-serif; font-size: 14pt;">The mushroom agriculture can generate them\nadditional income to cover medical insurance for the children, pay their\ntuition fees, as well as enhancing the family''s quality of life. Mukankima\nadded that her children will benefit from mushrooms because they are rich in\nvitamins and proteins.<o:p></o:p></span></p><p class="MsoNormal">\n\n\n\n\n\n</p><p class="MsoNormal"><span style="font-size: 14pt;">&nbsp;</span></p>\n      \n                    ', 'http://www.phpintl.org/imageupload/server/php/files/Mukankima%20annonciatta%20%281%29.jpg', '', '21', '2014-11-20 20:23:43', 'Approved', 'PHP', NULL),
(142, 250, NULL, 'BUY A SEWING MACHINE FOR FASHION BUSINESS', 'Micro', 'Paulina ', 'M142', '300', 841, 300, 2.8025, 'Service', 'VOH Campus-Ayawaso', 'Ghana', 'GHS', '48', '2', '2015-01-05', '2015-02-02', 5, '0', 0, 'Paulina will be using the funds to help start her fashion business.', '\n      \n      \n      Paulina has been attending Village of Hope Vocational Education learning how to sew fashionable clothing to sell. The funds will help her buy a sewing machine, some sewing materials and pay for a place to start. She wants to make this a successful business to help provide money for her future.&nbsp;            ', '', '', '24', '2014-11-13 20:33:01', 'Approved', 'PHP', NULL),
(143, 252, NULL, 'EXPAND SMALL TABLE TOP FOOD BUSINESS', 'Micro', 'Ama Serwaa', '', '300', 850, 300, 2.8325, 'Small Trade', 'Konongo', 'Ghana', 'GHS', '12', '2', '2014-05-02', '2014-05-16', 20, '', 0, 'Ama wants to buy goods in bulk to expand her food business.', '\n      \n      <div>Ama Serwaa is a client of Sinapi Aba. She sells food and cooking ingredients of all sorts on a table&nbsp;<span style="font-size: 14px;">top at the market centre. She is requesting a loan of $300 to enable her buy goods in bulk to expand her&nbsp;</span><span style="font-size: 14px;">business.</span></div>\n      \n                ', 'http://www.phpintl.org/imageupload/server/php/files/DSC05304%20%281%29.JPG', '', '25', '2014-11-13 19:58:31', 'Approved', 'PHP', NULL),
(145, NULL, NULL, 'NEW CLASSROOM CONSTRUCTION', 'Project', 'Kathleen Gibbs', '', '8000', NULL, NULL, 1, 'N/A', 'Medie', 'Ghana', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'These funds will go directly to the costs of electrical work, plastering, windows, doors, flooring, security fence and paint.  Please support us in growing our school. Your donation will make a huge difference for our kids.', '\n      \n      \n      <div><font color="#666666" face="Georgia, Times New Roman"><span style="font-size: 17.33px; background-color: rgb(246, 246, 246);">Joy2theWorld International Christian Academy is excited to be growing! &nbsp;Because we are growing, we need $8000 to construct new classrooms. Our KG2 will be in Primary 1 in September. &nbsp;All our classes will graduate and in September, we must be ready for the 50 additional children (or more!). &nbsp;These funds are specifically for electrical, windows, doors, plaster, flooring and paint! &nbsp;We need your help to grow our school...making room for more children. &nbsp;Any and all donations will be appreciated. &nbsp;The landlord set up the shell, we need to pay for the renovations in lieu of rent.</span></font></div><div><font color="#666666" face="Georgia, Times New Roman"><span style="font-size: 17.33px; background-color: rgb(246, 246, 246);"><br></span></font></div><div><font color="#666666" face="Georgia, Times New Roman"><span style="font-size: 17.33px; background-color: rgb(246, 246, 246);">We are a non-profit providing education to the "Brilliant But Needy" children who might not be able to attend school without us. &nbsp;Our children live in poverty and the only way to break this cycle of poverty and illiteracy is education.</span></font></div><div><font color="#666666" face="Georgia, Times New Roman"><span style="font-size: 17.33px; background-color: rgb(246, 246, 246);"><br></span></font></div><div><font color="#666666" face="Georgia, Times New Roman"><span style="font-size: 17.33px; background-color: rgb(246, 246, 246);">Help us reach them and change (their) history!</span></font></div>\n      \n                ', 'http://www.phpintl.org/imageupload/server/php/files/1328613_1399733816.5318.jpg', '', '26', '2014-11-13 20:02:40', 'Approved', 'PHP', NULL),
(146, 63, NULL, 'PURCHASE MORE LAND FOR A LARGER MAIZE CROP', 'Micro', 'Habiyakare Didas', 'M146', '400', 271600, 400, 679, 'Agriculture', 'MBANDAZI', 'RWANDA', 'RWF', '12', '4', '2014-05-24', '2014-07-24', 16.25, 'fixed', 0, 'Didas would like to buy more land so he can increase his maize production and sales.', '\n      \n      Habiyakare Didas would like to&nbsp;buy more land to increase the annual productivity of his maize crop. the maize he grows&nbsp;is in demand by the small and medium enterprises. &nbsp;Didas harvests 200 kilos of maize on his small plot of land and would like to grow more maize and meet the demand.&nbsp;Didashas &nbsp;talked with AZAM, the local factory,&nbsp;and they promised him a market for his whole harvest.&nbsp;&nbsp;Habiyaremye Didas wants to continue his studies in a management faculty which will allow him to sustain himself as well as a&nbsp;family. He is single at the moment but wishes to construct his own house.&nbsp;    ', 'http://www.phpintl.org/imageupload/server/php/files/ibigori%20%283%29.JPG', '', '21', '2014-11-13 20:33:04', 'Approved', 'PHP', NULL),
(147, 69, NULL, 'EXPAND A BEAN SELLING BUSINESS', 'Micro', 'BATSINZI Bernard', 'M147', '600', 407400, 600, 679, 'Food', 'KABUGA', 'RWANDA', 'RWF', '12', '4', '2014-05-25', '2014-07-25', 16.25, 'balance', 0, 'Bernard wants to expand his bean  trading business to increase his profits.', '\n      \n      BATSINZI Bernard needs a loan of $600&nbsp;to increase his bean business which will increase his annual income. He wants to expand his business of buying and selling beans in Gisharara cell at the above market.\n\nBatsinzi\nBernard has a family with 5 children, he now buys and sells beans in the\nmarket.&nbsp; At the moment he can only buy 50\nkilos at a time&nbsp;but if he can buy a larger quantity of beans, the more&nbsp; he will earn. With $ 600 he intends to purchase sacks of beans and start up a store where\ntraders will be buying beans. With the expansion of the beans business, Batsinzi\nwill be able to raise his children, pay school fees and medical insurance. Since raising 5 children without owning a house is a hindrance, Batsinzi also hopes to &nbsp;build his own house so he can stop renting.        ', 'http://www.phpintl.org/imageupload/server/php/files/P1120661%20%281%29.JPG', '', '21', '2014-11-13 20:33:07', 'Approved', 'PHP', NULL),
(168, 305, NULL, 'MORE PROFITS FROM SHOP ', 'Micro', 'Mboningabo Celestin', 'M168', '400', 274000, 400, 685, 'Service', 'Kabuga II', 'Rwanda', 'RWF', '12', '4', '2014-04-27', '2014-07-23', 16.25, 'balance', 0, 'Celestin needs to expand his business in order to fix his house.', '\n      \n      <p class="MsoNormal"><span style="font-family: Helvetica, sans-serif; font-size: 10.5pt;">Mboningabo Celestin and his children are at\nhigh risk as one side of their house is about to fall down. He has a small shop\nand needs $400 to buy more stock so that he can increase his profits. With his\nextra profits he can rebuild his house. Currently the profits from Celestin''s\nshop pay for food, medical insurance and other family needs.&nbsp; </span></p><p class="MsoNormal"><br></p><p class="MsoNormal">\n\n\n\n\n\n</p><p class="MsoNormal"><o:p>&nbsp;</o:p></p>\n\n<p class="MsoNormal"><o:p>&nbsp;</o:p></p>        ', 'http://www.phpintl.org/imageupload/server/php/files/celestin%20%281%29.JPG', '', '21', '2014-11-13 20:54:04', 'Approved', 'PHP', NULL),
(169, 306, NULL, 'SELLING VARIETIES OF VEGETALBES', 'Micro', 'Justine ', '', '300', 205500, 300, 685, 'Small Trade', 'NYAGAHINGA', 'RWANDA', 'RWF', '12', '4', '2014-04-27', '2014-07-23', 16.25, 'balance', 0, 'Justine wants to help her community through selling a variety of vegetables.', '\n      \n      Justine &nbsp;plans to sell vegetables which which will help the community to fight malnutrition. She needs $300 to begin selling different types of vegetables and using the profit to feed her daughter and sustain her family. The more families are sensitized on the importance of vegetables, the more they are in high demand, which is why Justine decided to invest in selling them. She added that the small business will not only generate income but also help her in food security.        ', 'http://www.phpintl.org/imageupload/server/php/files/justine%20%281%29.JPG', '', '21', '2014-11-13 20:50:34', 'Approved', 'PHP', NULL),
(170, 307, NULL, 'REHABILITATE HOME FOR RENTAL BUSINESS', 'Micro', 'Turabumukiza Eugene', 'M170', '200', 137000, 200, 685, 'Service', 'NYAGAHINGA', 'RWANDA', 'RWF', '12', '4', '2014-04-27', '2014-07-23', 16.25, 'balance', 0, 'Eugene wants to use his house for rental income.', '\n      Turabumukiza Eugene needs $200 to invest in a house rental business &nbsp;After rehabilitating and extending his house, &nbsp;he will earn about $45 per month to develop and sustain his family as well as&nbsp;increase his monthly income.        ', 'http://www.phpintl.org/imageupload/server/php/files/eugene%20%281%29.jpg', '', '21', '2014-11-13 20:54:09', 'Approved', 'PHP', NULL),
(171, 308, NULL, 'PURCHASE OF BASIC REPAIRING TOOLS', 'Micro', 'Nteziryayo Emmanuel', '', '200', 137000, 200, 685, 'Service', 'KABUGA II', 'RWANDA', 'RWF', '12', '4', '2014-04-27', '2014-07-23', 16.25, 'balance', 0, 'Emmanuell needs to buy his own tools so he can develop his repair business.', '\n      \n      Nteziryayo Emmanuel works with metal repairers but he needs to buy his own set of tools so he can work more. Whenever Nteziryayo is working he has to borrow tools; he is not as productive as he would like to be because he has to wait for others to finish with their tools.        ', 'http://www.phpintl.org/imageupload/server/php/files/emmanuel%20%281%29.jpg', '', '21', '2014-11-13 20:52:42', 'Approved', 'PHP', NULL),
(172, 309, NULL, 'PURCHASE OF CARPENTRY TOOLS', 'Micro', 'Nsengimana Ismael ', '', '500', 205500, 300, 685, 'Manufacturing', 'NYAGAHINGA', 'RWANDA', 'RWF', '12', '', '2014-04-27', '2014-07-23', 16.25, 'balance', 0, 'Ismael needs to buy basic carpentry tools to increase his business.', '\n      \n      \n      To feed his children, Nsengimana Ismael has to borrow tools from the nearest workshop while he can work from home and make money. He needs a loan of $500 &nbsp;to buy basic carpentry tools which will increase his household income and sustain his family. He will also be able to pay school fees, medical insurance &nbsp;as well as food security.            ', 'http://www.phpintl.org/imageupload/server/php/files/ismael.JPG', '', '21', '2014-12-03 02:29:09', 'Approved', 'PHP', NULL),
(173, 310, NULL, 'EXPAND MY RESTAURANT BUSINESS', 'Micro', 'Kayibanda Damas', '', '700', 479500, 700, 685, 'Small Trade', 'KABUGA II', 'RWANDA', 'RWF', '12', '4', '2014-04-27', '2014-07-23', 16.25, 'balance', 0, 'Damas has a small restaurant which he would like to grow to provide more variety for his customers.', '\n      \n      Kayibanda Damas would like to grow his small restaurant into a big one with many customer. He needs $700 to develop his menu to meet the customer demand for more variety. Damas hopes to attract different clients through offering more varieties of food.        ', 'http://www.phpintl.org/imageupload/server/php/files/damas%20%281%29.JPG', '', '21', '2014-11-13 20:56:20', 'Approved', 'PHP', NULL),
(174, 311, NULL, 'PURCHASE OF CONSTRUCTION TOOLS', 'Micro', 'Nkuranga Augustin', '', '400', 274000, 400, 685, 'Service', 'NYAGAHINGA', 'RWANDA', 'RWF', '12', '4', '2014-04-27', '2014-07-23', 16.25, 'balance', 0, 'Augustin needs to buy construction tools to increase his business.', '\n      \n      Nkuranga Augustin &nbsp;is a builder would like to buy the tools he needs to grow his business. He needs $400 to buy the tools and materials that will help him win bids on projects. With this loan, Nkuranga will buy the tools which will help to generate more income and develop himself as well as the district.        ', 'http://www.phpintl.org/imageupload/server/php/files/augustin%20%281%29.JPG', '', '21', '2014-11-13 20:57:20', 'Approved', 'PHP', NULL),
(149, 64, NULL, 'MAKE NEW MERCHANDISE AVAILABLE IN BISENGA', 'Micro', 'Ntamukunzi Theogene', 'M149', '700', 475300, 700, 679, 'Small Trade', 'GASAGARA', 'RWANDA', 'RWF', '12', '4', '2014-05-25', '2014-07-25', 16.25, 'fixed', 475, 'Theogene needs capital to expand the inventory in his shop.', '\n      \n      \n      \n      At the age of 31, Ntamukunzi Theogene has a vision for growing his small business in Bisenga and he needs $700 to make it happen. Theogene wants to use new capital to bring everyday household items like charcoal, beans, cassava flour, and rice, which are in great demand, to the people of Bisenga, Currently some of this merchandise is obtained when people have traveled a long distance. &nbsp;He wants to establish a regular trade route to get this merchandise to Bisenga and to be able to offer a greater variety of merchandise to his neighbors. Ntamukunzi hopes to grow this business so that he can construct his own house and sustain his relatives. Ntamukunzi has a brother who is going&nbsp;to the University and he wishes to continue helping him.<br>\n      \n                  ', 'http://www.phpintl.org/imageupload/server/php/files/THEOGENE.png', '', '21', '2014-11-20 20:27:33', 'Approved', 'PHP', NULL);
INSERT INTO `projects` (`id`, `client_id`, `group_id`, `title`, `type`, `name`, `phpid`, `amount`, `ngoamount`, `actualamount`, `rate`, `category`, `location`, `country`, `currency`, `loanterms`, `loantermstype`, `startdate`, `loandate`, `loaninterest`, `lateterms`, `lateamount`, `use`, `description`, `pics`, `video`, `organization_id`, `postdate`, `public`, `fundingsource`, `php_fund_status`) VALUES
(150, 70, NULL, 'SOY BEAN FARMING', 'Micro', 'Mbonigaba Caritas', 'M150', '1100', 746900, 1100, 679, 'Agriculture', 'KAMASHASHI', 'RWANDA', 'RWF', '12', '4', '2014-05-25', '2014-07-25', 16.25, 'fixed', 746, 'Caritas wants to grow soy beans for the local market.', '\n      \n      \n      \n      \n      \n      Having analyzed the Kabuga market, Mbonigaba Caritas would like to invest in growing soy beans for the local market. She needs $1,100 to cultivate her own land. Caritas is a mother of five and three of them are going to the secondary school; in this school year she has faced challenges including paying school fees for the children.&nbsp;She had banana trees but they did not produce this year. With $1,100 Caritas&nbsp;will&nbsp;replace banana trees with soy beans&nbsp;using selected seeds, fertilizers, agricultural tools, pesticides as well as making organic manure from her farm. &nbsp;Mbonigaba knows that soy beans are in high demand in the area especially by fishermen who are using them for fish food.&nbsp;With this new venture she intends to pay school fees for the children and sustain the orphans in her family.                    ', 'http://www.phpintl.org/imageupload/server/php/files/soya.png', '', '21', '2014-11-13 20:54:20', 'Approved', 'PHP', NULL),
(151, 65, NULL, 'INCREASE RENTAL INCOME', 'Micro', 'Kantengwa Victoire', 'M151', '900', 611100, 900, 679, 'Service', 'NYAGAHINGA', 'RWANDA', 'RWF', '12', '4', '2014-05-25', '2014-07-25', 16.25, 'fixed', 611, 'Victoire wants to refurbish his house to increase the rental value to support his family.', '\n      \n      \n      Kantengwa Victoire would like to refurbish her house in order to increase her monthly income.&nbsp;With $900 she can refurbish and modernize this old house which will attract local tenants.The house is now making about $45 per month.&nbsp;It is located in an area where people like to live, and after the rehabilitation, Kantengwa hopes to rent it at about $65 per unit, which will allow her to sustain her family of 4 children plus two adopted children. With these rental fees she will be able to pay school fees and medical insurance for 6 children, &nbsp;2 at the primary level and 4 at the secondary school. Educating and feeding the family are the most urgent challenges she is facing and she is looking forward to increasing her income and meet the demand for quality housing.&nbsp;        ', 'http://www.phpintl.org/imageupload/server/php/files/DSC03545%20%284%29.JPG', '', '21', '2014-11-20 20:28:45', 'Approved', 'PHP', NULL),
(152, 67, NULL, 'REFURBISH A HOUSE TO RENT', 'Micro', 'Ntagungira Cyaruzima Felicite ', '', '700', 475300, 700, 679, 'Service', 'RUSORORO', 'RWANDA', 'RWF', '12', '4', '2014-05-25', '2014-07-25', 16.25, 'fixed', 475, 'I need to refurbish my house so that it will provide rental income.', '\n      \n      Ntagungira Cyaruzima Felicite is\napplying for a loan of $700 to refurbish her house so that it can be rented for &nbsp;more money on a monthly basis.&nbsp;With $700, she can rehabilitate her house and increase the rent; the house is now empty and doesn''t contribute money to sustain her family.&nbsp;Cyaruzima lives in Nyagahinga where the houses for rent are in high demand because it is near the &nbsp;commercial center called Kabuga. &nbsp;Increased rent will allow her to pay school fees for her children\nand cover medical insurance as well as other households'' needs.&nbsp;Cyaruzima is a farmer\nwhose husband is unemployed; she has a 20-year-old son and &nbsp;two daughters who are now beginning secondary school where the fees are high.&nbsp;    ', 'http://www.phpintl.org/imageupload/server/php/files/DSC03541%20%283%29.JPG', '', '21', '2014-11-13 20:31:11', 'no', 'PHP', NULL),
(161, 297, NULL, 'MAIZE FARMING WITH IMPROVED SEEDS', 'Micro', 'Sebagabo Cassien', 'M161', '400', 272200, 400, 680.5, 'Agriculture', 'KAMASHASHI', 'RWANDA', 'RWF', '12', '4', '2014-07-20', '2014-09-21', 16, 'balance', 0, 'Cassien wants to increase his maize production to sell to local manufacturers.', '\n      <p class="MsoNormalCxSpFirst"><span style="font-family: Helvetica, sans-serif; font-size: 10.5pt;">At the age of 64, Sebagabo Cassien, father of 7\nchildren, is unable to pay the school fees for three of his children due to\ninsufficient income. He is seeking a loan of $400 to buy maize seeds to sell in\nbulk to local manufacturers like Bralirwa and MINIMEX. &nbsp;Sebagabo will buy fertilizers and improved\nseeds as well as rehabilitate and develop part of his farm of 0.8 hectares. Sebagabo\nexpects maize production of 4.5 metric tons to sell to MINIMEX which buys the\nproduce at 33 cents/kilo. <o:p></o:p></span></p>\n\n<p class="MsoNormalCxSpMiddle"><span style="font-family: Helvetica, sans-serif; font-size: 10.5pt;">Although he works with Kamuziga Belancile aged\n50, on a farm on a daily basis, their revenue is not enough to cover their\nhousehold needs. Sebagabo grew bananas for 35 years but he didn''t produce\nenough to sustain his family because he used a hoe to grow only what they could\neat. Sebagabo sees an opportunity in maize to breakthrough this continuous\nscarcity. Last season, when Sebagabo was testing maize, he was able to buy a\ncalf. As his annual income increases he will be able to cover his family basic\nneeds, pay the school fees of his children, save a little in Sacco for future\nuse as well as covering medical insurance.</span><span style="font-size: 14pt;"><o:p></o:p></span></p>    ', 'http://www.phpintl.org/imageupload/server/php/files/sebagabo%20%281%29.JPG', '', '21', '2014-11-13 20:54:33', 'Approved', 'PHP', NULL),
(162, 298, NULL, 'CABBAGE GROWING IN RUGENDE MARSHLAND', 'Micro', 'Ndungutse Jean Fabrice', 'M162', '600', 408300, 600, 680.5, 'Agriculture', 'NYAKAGOMMBE', 'RWANDA', 'RWF', '12', '4', '2014-07-20', '2014-09-20', 16, 'balance', 0, 'Jean Fabrice sees an opportunity to raise cabbages in local marshland to sell locally.', '\n      \n      <p class="MsoNormalCxSpFirst"><span style="font-family: Helvetica, sans-serif; font-size: 10.5pt;">Ndungutse Jean Fabrice, aged 43, needs $600 to begin\nraising cabbages in the Rugende marshlands. &nbsp;Married to Mukakarangwa Providence and blessed\nwith 2 children, Ndungutse would like to see his daughter and son raised in\ngood conditions and educated. He believes that growing cabbages will make this\ndream a reality. &nbsp;&nbsp;<o:p></o:p></span></p>\n\n<p class="MsoNormalCxSpMiddle"><span style="font-family: Helvetica, sans-serif; font-size: 10.5pt;">Ndungutse decided to invest in cabbage farming\nafter realizing that his land, 70 acres, is suitable for it and near water and he can earn enough income every\nsix months. He has already tested this project in Nyakagombe village. Ndungutse\nwill follow modern ways of growing cabbages.\nHe will use fertilizers (NPK) and organic manure, pesticides, pumps and other\nagricultural activities that he will be harvesting every six months.</span> Ndungutse\nhas targeted the local markets, Zinia and Kimironko, which have a growing\ndemand for cabbages. He will sell them at about 30 cents/cabbage.</p>\n\n<p class="MsoNormalCxSpMiddle"><span style="font-family: Helvetica, sans-serif; font-size: 10.5pt;">&nbsp;Cabbage\ngrowing will help the Ndungutse family to fight malnutrition and pay children''s\nschool fees and stationery materials as well as increasing their income.&nbsp;</span><span style="font-size: 14pt;"><o:p></o:p></span></p>        ', 'http://www.phpintl.org/imageupload/server/php/files/NDUNGUTSE%20%281%29.JPG', '', '21', '2014-11-13 20:54:34', 'Approved', 'PHP', NULL),
(163, 299, NULL, 'CASSAVA PLANTING ', 'Micro', 'Rutabana Venuste', 'M163', '1200', 816600, 1200, 680.5, 'Agriculture', 'BISENGA', 'RWANDA', 'RWF', '12', '4', '2014-07-20', '2014-09-20', 16, 'balance', 0, 'Venuste wants to grow cassava for sale to local baking firms.', '\n      <div><p class="MsoNormal" style="line-height: 15pt;"><span style="font-family: Helvetica, sans-serif; font-size: 10.5pt;">Rutabana Venuste, aged 48, needs $1200 to change from growing Irish\npotatoes to growing cassava. &nbsp;He has\nalready contacted one of the local firms which makes biscuits from cassava. He\nneeds fertilizers, improved cassava seeds, irrigation utensils as well as\npesticides to rehabilitate and develop the land. Rutabana and his wife,\nUwamwezi Cecile, have three children. Uwamwezi who is member of the Womenâ€™s Association,\nis ready to venture into cassava planting and is committed to achieving the\nfamily''s vision.</span></p><p class="MsoNormal" style="line-height: 15pt;"><span style="line-height: 15pt; font-family: Helvetica, sans-serif; font-size: 10.5pt;">Last year Uwitonze Pierre, their eldest son, performed well in the primary\nschool leaving examinations but they could not afford the school fees for him\nto continue. They hope that </span><span style="line-height: 15pt; font-family: Helvetica, sans-serif; font-size: 10.5pt;">Uwitonze and the other children will get\naccess to education, medical insurance and food. The family also plans to rehabilitate\ntheir old house and live in better conditions.</span></p><div><br></div></div>        ', 'http://www.phpintl.org/imageupload/server/php/files/RUTABANA.JPG', '', '21', '2014-11-13 20:54:37', 'Approved', 'PHP', NULL),
(164, 300, NULL, 'DISTRIBUTION OF CASSAVA FLOUR', 'Micro', 'Kwizera Daniel', 'M164', '800', 544400, 800, 680.5, 'Service', 'SAMUDUHA', 'RWANDA', 'RWF', '12', '4', '2014-07-20', '2014-09-20', 16, 'balance', 0, 'Daniel wants to buy modern equipment to make cassava flour and sell it at competitive prices.', '\n      <div><div><p class="MsoNormal" style="line-height: 15pt;"><span style=''font-family: "Times New Roman","serif"; font-size: 12pt; mso-fareast-font-family: "Times New Roman";''>Kwizera Daniel is young (24), and business oriented.&nbsp; He could n0t afford to go to school but he\nwould like to become his own boss through preparing cassava flour. </span><span style="font-family: Helvetica, sans-serif; font-size: 10.5pt;">Kwizera needs $800 to buy modern equipment to\nmake his cassava flour more competitive at the local market. He plans to\npackage his flour and brand it &nbsp;to assure\ncleanliness and thereby grow the business.<o:p></o:p></span></p>\n\n<p class="MsoNormal"><span style="line-height: 15pt; font-family: Helvetica, sans-serif; font-size: 10.5pt;">Kwizera, whose name in English means\n"Believe", believes that, though he is an orphan. his future will\nshine from this business. He started his business over a year and a half ago, using\na small amount of capital ($75). He is still using traditional tools which\nmakes his cassava flour not as competitive at the local market. Kwizera wants\nto acquire the technical skills grow his cassava production so he will be\nmaking other products from this plant in the future.</span></p>\n\n<p class="MsoNormal" style="line-height: 15pt;"><span style="font-family: Helvetica, sans-serif; font-size: 10.5pt;">He hopes to construct his own house and\ncontribute to the development of Samuduha which is his motherland.<o:p></o:p></span></p></div><div><br></div><div><br><div><br></div></div></div>        ', 'http://www.phpintl.org/imageupload/server/php/files/KWIZERA%20%281%29.JPG', '', '21', '2014-11-13 20:54:38', 'Approved', 'PHP', NULL),
(165, 302, NULL, 'GROWING AND SELLING SOYBEANS', 'Micro', 'Rubuga', 'M165', '700', 476350, 700, 680.5, 'Agriculture', 'GAHOROMANI', 'RWANDA', 'RWF', '12', '4', '2014-07-20', '2014-09-20', 16, 'balance', 0, 'Rubuga wants to use modern farming methods to increase her soybean crop.', '\n      <div><p class="MsoNormal" style="line-height: 200%;"><span style="line-height: 200%; font-family: Helvetica, sans-serif; font-size: 10.5pt;">Rubuga has been growing\nsoybeans for almost 8 years but realizes she has been growing them for\nsubsistence which didn''t meet her daily needs. &nbsp;She </span><span style="line-height: 200%; font-family: Helvetica, sans-serif; font-size: 10.5pt;">still hopes that the sky is the limit and\nwould like to venture into modern agriculture and sell her soybean harvest at\nher home market commonly known as Gahoromani. <span>&nbsp;Rubuga needs $700 to hire tractors to till her\nland into radical terraces to prevent soil erosion and to buy fertilizers,\ncompost and pesticides. </span><o:p></o:p></span></p>\n\n<p class="MsoNormal" style="line-height: 200%;"><span style="line-height: 200%; font-family: Helvetica, sans-serif; font-size: 10.5pt;">Rubuga hopes to harvest two crops per year,\nsell them at her market as well as providing her family with food rich in\nprotein.&nbsp; She would like to see her\ngrandchildren get an education and to construct a new house for the family.&nbsp;</span></p></div><div><br></div><div><br></div><div><br></div><div><br></div><div><br></div>        ', 'http://www.phpintl.org/imageupload/server/php/files/rubuga%20joyce%20%282%29.JPG', '', '21', '2014-11-13 20:54:41', 'Approved', 'PHP', NULL),
(166, 303, NULL, 'SELLING CASSAVA FLOUR AND DOUGH', 'Micro', 'Mukangango and Kakunde', 'M166', '800', 544400, 800, 680.5, 'Manufacturing', 'CYANAMO', 'RWANDA', 'RWF', '12', '4', '2014-07-21', '2014-09-20', 16, 'balance', 0, 'Mukangango needs to expand her cassava flour business to supply local restaurants.', '\n      <p class="MsoNormal"><span style="font-family: Helvetica, sans-serif; font-size: 10.5pt;">Mukangango\nand her husband Kakunde dream of owning their own home and they have started a\nsmall business making flour out of cassava and then making fried dough with it.\nThey have already put $100 into their business and now they need $800 to expand\nthe business. T</span><span style="font-family: Helvetica, sans-serif; font-size: 10.5pt;">hey need modern tools\nto grind cassava and prepare the flour and dough for the local market. Cassava\nflour is consumed in the area especially with meat soups and the fried dough is\neaten for breakfast. The family has started negotiating with nearest\nrestaurants which will be buying their products.<o:p></o:p></span></p>\n\n<p class="MsoNormal"><span style="font-family: Helvetica, sans-serif; font-size: 10.5pt;">Mukangango and Kakunda\nhave two children. The business will not only generate income but also help the\nfamily to fight malnutrition, pay medical insurance and pay for their eldest\nson to complete his primary education. Mukangango and Kakunde are expecting a\nbetter future from this project,</span></p>\n\n<p class="MsoNormal"><o:p>&nbsp;</o:p></p>        ', 'http://www.phpintl.org/imageupload/server/php/files/mukangango%20%281%29.JPG', '', '21', '2014-11-13 20:54:43', 'Approved', 'PHP', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `projects_closing`
--

CREATE TABLE IF NOT EXISTS `projects_closing` (
  `id` int(11) unsigned NOT NULL,
  `projectid` int(255) DEFAULT NULL,
  `clientid` int(255) unsigned NOT NULL,
  `userid` int(255) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `differencerate` double DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `returned` double DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `differenceclose` double DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projects_closing`
--

INSERT INTO `projects_closing` (`id`, `projectid`, `clientid`, `userid`, `amount`, `differencerate`, `rate`, `returned`, `date`, `differenceclose`) VALUES
(1, 126, 50, 9, 2.3324036711800002, 0, 21065, 0.777467890392, '2014-01-13 19:55:15', 12833.2),
(2, 127, 53, 11, 450, 0, 1, 0.9, '2014-01-30 18:46:58', 68),
(3, 128, 54, 11, 500, 0, 1, 1, '2014-02-03 16:48:10', 20),
(4, 129, 52, 11, 450, -250, 680.5, 0.9, '2014-02-05 06:49:25', 46308),
(5, 109, 24, 11, 0, 2668.64343958, 680.5, 0, '2014-02-05 06:49:28', 300000),
(6, 155, 290, 11, 862.5, 0, 1, 1.078125, '2014-06-25 16:21:06', -28),
(7, 153, 288, 11, 920, 0, 1, 0.92, '2014-06-25 16:21:07', 116.8),
(8, 156, 291, 11, 358.333333333, 0, 1, 0.895833333333, '2014-06-25 16:21:09', 56),
(9, 157, 292, 11, 1080, 0, 1, 0.9, '2014-06-25 16:21:12', 163.2),
(10, 158, 293, 11, 458.333333333, 0, 1, 0.916666666667, '2014-06-25 16:21:14', 60),
(11, 159, 294, 11, 416.666666667, 0, 678, 0.833333333333, '2014-06-25 20:37:48', 67800);

-- --------------------------------------------------------

--
-- Table structure for table `rates`
--

CREATE TABLE IF NOT EXISTS `rates` (
  `id` int(11) unsigned NOT NULL,
  `currency` varchar(5) DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `date` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rates`
--

INSERT INTO `rates` (`id`, `currency`, `rate`, `date`) VALUES
(1, 'USD', 1, '2014-11-15'),
(2, 'RWF', 730.27, '2015-09-04'),
(3, 'PYG', 4426.0249, '2014-03-08'),
(4, 'HKD', 7.7607, '2014-03-08'),
(5, 'EUR', 0.7207, '2014-03-08'),
(6, NULL, 1, '2014-03-12'),
(7, 'GHS', 3.82, '2015-09-03'),
(8, '', 1, '2015-02-09'),
(9, NULL, 1, '2015-09-03'),
(10, NULL, 1, '2015-09-03');

-- --------------------------------------------------------

--
-- Table structure for table `repayment`
--

CREATE TABLE IF NOT EXISTS `repayment` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `projectid` int(11) NOT NULL,
  `clientid` int(11) DEFAULT NULL,
  `paymenttype` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` double NOT NULL,
  `ngoamount` double DEFAULT NULL,
  `date` date NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ngo_reserve_id` int(11) DEFAULT NULL,
  `depositdate` date DEFAULT NULL,
  `depositRate` double DEFAULT NULL,
  `depositamount` double DEFAULT NULL,
  `depositngoamount` double DEFAULT NULL,
  `closepayment` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=145 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `repayment`
--

INSERT INTO `repayment` (`id`, `userid`, `projectid`, `clientid`, `paymenttype`, `amount`, `ngoamount`, `date`, `timestamp`, `ngo_reserve_id`, `depositdate`, `depositRate`, `depositamount`, `depositngoamount`, `closepayment`) VALUES
(22, 46, 80, NULL, NULL, 400, 400, '2013-09-20', '2013-09-20 14:00:03', NULL, NULL, NULL, NULL, NULL, 0),
(21, 46, 80, NULL, NULL, 100, 100, '2013-09-20', '2013-09-20 13:58:58', NULL, NULL, NULL, NULL, NULL, 0),
(19, 36, 52, NULL, NULL, 23.3644859813, 50, '2013-09-12', '2013-09-12 16:50:51', NULL, NULL, NULL, NULL, NULL, 0),
(18, 36, 52, NULL, NULL, 420.560747664, 900, '2013-09-12', '2013-09-12 16:49:28', NULL, NULL, NULL, NULL, NULL, 0),
(17, 36, 52, NULL, NULL, 46.7289719626, 100, '2013-08-30', '2013-08-30 18:48:46', NULL, NULL, NULL, NULL, NULL, 0),
(24, 44, 87, NULL, 'Saving', 138.408304498, 300, '2013-08-30', '2013-09-20 19:17:49', NULL, NULL, NULL, NULL, NULL, 0),
(38, 44, 79, NULL, 'Loan', 100, 100, '2013-09-21', '2013-09-21 19:15:19', NULL, NULL, NULL, NULL, NULL, 0),
(26, 44, 87, NULL, 'Loan', 230.680507497, 500, '2013-09-20', '2013-09-20 19:33:04', NULL, NULL, NULL, NULL, NULL, 0),
(27, 44, 87, NULL, 'Loan', 230.680507497, 500, '2013-09-20', '2013-09-20 20:02:11', NULL, NULL, NULL, NULL, NULL, 0),
(28, 44, 87, NULL, 'Saving', 138.408304498, 300, '2013-09-20', '2013-09-20 20:02:51', NULL, NULL, NULL, NULL, NULL, 0),
(34, 44, 87, NULL, 'Loan', 230.680507497, 500, '2013-09-20', '2013-09-20 20:40:51', NULL, NULL, NULL, NULL, NULL, 0),
(59, 44, 89, NULL, 'Loan', 46.1361014994, 100, '2013-10-03', '2013-10-01 08:52:50', NULL, NULL, NULL, NULL, NULL, 0),
(60, 44, 96, NULL, 'Loan', 450, 301725, '2013-10-01', '2013-10-01 09:50:52', NULL, NULL, NULL, NULL, NULL, 0),
(61, 44, 89, NULL, 'Loan', 46.1361014994, 100, '2013-10-04', '2013-10-02 10:42:31', NULL, NULL, NULL, NULL, NULL, 0),
(39, 44, 79, NULL, 'Loan', 200, 200, '2013-09-22', '2013-09-21 19:17:22', NULL, NULL, NULL, NULL, NULL, 0),
(40, 44, 88, NULL, 'Saving', 0, 0, '2013-09-24', '2013-09-24 10:25:21', NULL, NULL, NULL, NULL, NULL, 0),
(41, 44, 89, NULL, 'Saving', 115.340253749, 250, '2013-09-24', '2013-09-24 10:37:30', NULL, NULL, NULL, NULL, NULL, 0),
(42, 44, 89, NULL, 'Loan', 138.408304498, 300, '2013-09-01', '2013-09-24 10:55:21', NULL, NULL, NULL, NULL, NULL, 0),
(43, 44, 89, NULL, 'Loan', 92.2722029988, 200, '2013-09-08', '2013-09-24 10:55:46', NULL, NULL, NULL, NULL, NULL, 0),
(44, 44, 89, NULL, 'Saving', 230.680507497, 500, '2013-09-30', '2013-09-24 10:56:33', NULL, NULL, NULL, NULL, NULL, 0),
(45, 44, 77, NULL, 'Saving', 130, 130, '2013-09-19', '2013-09-24 11:46:43', NULL, NULL, NULL, NULL, NULL, 0),
(46, 44, 89, NULL, 'Loan', 46.1361014994, 100, '2013-09-25', '2013-09-26 00:04:49', NULL, NULL, NULL, NULL, NULL, 0),
(47, 44, 89, NULL, 'Loan', 46.1361014994, 100, '2013-09-26', '2013-09-26 18:08:38', NULL, NULL, NULL, NULL, NULL, 0),
(48, 44, 89, NULL, 'Saving', 92.2722029988, 200, '2013-09-28', '2013-09-27 22:18:07', NULL, NULL, NULL, NULL, NULL, 0),
(49, 44, 90, NULL, 'Saving', 92.2722029988, 200, '2013-09-30', '2013-09-30 12:53:12', NULL, NULL, NULL, NULL, NULL, 0),
(50, 44, 91, NULL, 'Saving', 0, 0, '2013-09-30', '2013-09-30 17:32:36', NULL, NULL, NULL, NULL, NULL, 0),
(51, 44, 92, NULL, 'Saving', 0, 0, '2013-09-30', '2013-09-30 17:34:33', NULL, NULL, NULL, NULL, NULL, 0),
(52, 44, 93, NULL, 'Saving', 0, 0, '2013-09-30', '2013-09-30 17:35:17', NULL, NULL, NULL, NULL, NULL, 0),
(53, 44, 94, NULL, 'Saving', 0, 0, '2013-09-30', '2013-09-30 17:35:31', NULL, NULL, NULL, NULL, NULL, 0),
(54, 44, 90, NULL, 'Loan', 138.408304498, 300, '2013-09-20', '2013-09-30 17:51:00', NULL, NULL, NULL, NULL, NULL, 0),
(55, 44, 89, NULL, 'Loan', 46.1361014994, 100, '2013-10-02', '2013-09-30 18:44:21', NULL, NULL, NULL, NULL, NULL, 0),
(56, 44, 95, NULL, 'Saving', 0.298284862043, 200, '2013-09-30', '2013-09-30 23:14:19', NULL, NULL, NULL, NULL, NULL, 0),
(57, 44, 95, NULL, 'Loan', 300, 201150, '2013-09-01', '2013-09-30 23:14:55', NULL, NULL, NULL, NULL, NULL, 0),
(58, 44, 95, NULL, 'Loan', 300, 201150, '2013-09-08', '2013-09-30 23:15:30', NULL, NULL, NULL, NULL, NULL, 0),
(62, 44, 89, NULL, 'Loan', 23.0680507497, 50, '2013-10-20', '2013-10-06 12:43:55', NULL, NULL, NULL, NULL, NULL, 0),
(63, 44, 89, NULL, 'Loan', 46.1361014994, 100, '2013-10-16', '2013-10-15 21:49:15', NULL, NULL, NULL, NULL, NULL, 0),
(64, 44, 97, NULL, 'Loan', 200, 200, '2013-10-18', '2013-10-17 15:20:52', NULL, NULL, NULL, NULL, NULL, 0),
(65, 44, 0, 7, 'Saving', 500, 500, '2013-10-20', '2013-10-20 23:41:27', NULL, NULL, NULL, NULL, NULL, 0),
(66, 44, 0, 8, 'Saving', 229.621125144, 500, '2013-10-20', '2013-10-20 23:43:51', NULL, NULL, NULL, NULL, NULL, 0),
(67, 44, 100, NULL, 'Loan', 100, 100, '2013-10-20', '2013-10-21 00:13:42', NULL, NULL, NULL, NULL, NULL, 0),
(68, 44, 100, NULL, 'Loan', 37.5, 37.5, '2013-10-21', '2013-10-21 00:46:44', NULL, NULL, NULL, NULL, NULL, 0),
(69, 44, 100, NULL, 'Loan', 40, 40, '2013-10-21', '2013-10-21 00:49:12', NULL, NULL, NULL, NULL, NULL, 0),
(70, 48, 102, NULL, 'Loan', 42.5, 28687.5, '2013-11-06', '2013-11-06 10:49:59', NULL, NULL, NULL, NULL, NULL, 0),
(71, 48, 105, NULL, 'Loan', 51.5666716196, 34704.37, '2013-11-06', '2013-11-06 11:01:17', NULL, NULL, NULL, NULL, NULL, 0),
(72, 49, 108, NULL, 'Loan', 51.8122205663, 34766, '2013-12-29', '2013-11-29 08:49:55', NULL, NULL, NULL, NULL, NULL, 0),
(73, 49, 110, NULL, 'Loan', 43.3875018532, 29264.87, '2013-12-02', '2013-12-02 11:43:00', NULL, NULL, NULL, NULL, NULL, 0),
(74, 36, 126, 50, 'Loan', 0.474721101353, 10000, '2014-01-13', '2014-01-13 19:50:49', NULL, NULL, NULL, NULL, NULL, 0),
(75, 36, 126, 50, 'Loan', 0.425278898647, 8958.5, '2014-01-13', '2014-01-13 19:52:11', NULL, NULL, NULL, NULL, NULL, 0),
(76, 36, 126, 50, 'Loan', 1.8988844054099998, 40000, '2014-01-13', '2014-01-13 19:53:10', NULL, NULL, NULL, NULL, NULL, 0),
(77, 58, 127, 53, 'Loan', 100, 100, '2014-01-31', '2014-01-30 18:41:28', NULL, NULL, NULL, NULL, NULL, 0),
(78, 58, 127, 53, 'Loan', 100, 100, '2014-02-27', '2014-01-30 18:41:59', NULL, NULL, NULL, NULL, NULL, 0),
(79, 58, 127, 53, 'Loan', 100, 100, '2014-03-27', '2014-01-30 18:42:19', NULL, NULL, NULL, NULL, NULL, 0),
(80, 58, 127, 53, 'Loan', 100, 100, '2014-04-24', '2014-01-30 18:42:41', NULL, NULL, NULL, NULL, NULL, 0),
(81, 58, 127, 53, 'Loan', 100, 100, '2014-05-22', '2014-01-30 18:43:01', NULL, NULL, NULL, NULL, NULL, 0),
(82, 58, 127, 53, 'Loan', 40, 40, '2014-06-26', '2014-01-30 18:43:25', NULL, NULL, NULL, NULL, NULL, 0),
(83, 58, 129, 52, 'Loan', 100, 68100, '2014-02-02', '2014-02-02 17:52:57', NULL, NULL, NULL, NULL, NULL, 0),
(84, 58, 129, 52, 'Loan', 100, 68100, '2014-02-02', '2014-02-02 17:53:07', NULL, NULL, NULL, NULL, NULL, 0),
(85, 58, 129, 52, 'Loan', 100, 68100, '2014-03-02', '2014-02-02 17:53:23', NULL, NULL, NULL, NULL, NULL, 0),
(86, 58, 129, 52, 'Loan', 100, 68100, '2014-04-06', '2014-02-02 17:53:35', NULL, NULL, NULL, NULL, NULL, 0),
(87, 58, 129, 52, 'Loan', 100, 68100, '2014-05-04', '2014-02-02 17:53:55', NULL, NULL, NULL, NULL, NULL, 0),
(88, 58, 129, 52, 'Loan', 40, 27240, '2014-06-01', '2014-02-02 17:54:30', NULL, NULL, NULL, NULL, NULL, 0),
(89, 58, 128, 54, 'Loan', 100, 100, '2014-02-03', '2014-02-03 16:46:44', NULL, NULL, NULL, NULL, NULL, 0),
(90, 58, 128, 54, 'Loan', 100, 100, '2014-02-03', '2014-02-03 16:47:10', NULL, NULL, NULL, NULL, NULL, 0),
(91, 58, 128, 54, 'Loan', 400, 400, '2014-02-03', '2014-02-03 16:47:23', NULL, NULL, NULL, NULL, NULL, 0),
(92, 49, 139, 246, 'Loan', 100.294985251, 68000, '2014-01-31', '2014-04-11 21:09:39', NULL, NULL, NULL, NULL, NULL, 0),
(93, 49, 139, 246, 'Loan', 100.294985251, 68000, '2014-02-26', '2014-04-11 21:10:01', NULL, NULL, NULL, NULL, NULL, 0),
(94, 49, 139, 246, 'Loan', 100.294985251, 68000, '2014-03-31', '2014-04-11 21:10:16', NULL, NULL, NULL, NULL, NULL, 0),
(95, 49, 139, 246, 'Loan', 100.294985251, 68000, '2014-04-30', '2014-04-11 21:10:31', NULL, NULL, NULL, NULL, NULL, 0),
(96, 49, 139, 246, 'Loan', 100.294985251, 68000, '2014-05-31', '2014-04-11 21:11:05', NULL, NULL, NULL, NULL, NULL, 0),
(97, 74, 153, 288, 'Loan', 153, 153, '2013-02-04', '2014-06-24 17:06:08', NULL, NULL, NULL, NULL, NULL, 0),
(98, 74, 153, 288, 'Loan', 153, 153, '2013-03-04', '2014-06-24 17:06:48', NULL, NULL, NULL, NULL, NULL, 0),
(99, 74, 153, 288, 'Loan', 154, 154, '2013-04-04', '2014-06-24 17:07:05', NULL, NULL, NULL, NULL, NULL, 0),
(100, 74, 153, 288, 'Loan', 154, 154, '2013-05-04', '2014-06-24 17:07:22', NULL, NULL, NULL, NULL, NULL, 0),
(101, 74, 153, 288, 'Loan', 153, 153, '2013-06-04', '2014-06-24 17:07:46', NULL, NULL, NULL, NULL, NULL, 0),
(102, 74, 153, 288, 'Loan', 154, 154, '2013-07-04', '2014-06-24 17:11:05', NULL, NULL, NULL, NULL, NULL, 0),
(103, 74, 153, 288, 'Loan', 183, 183, '2013-07-24', '2014-06-24 17:12:45', NULL, NULL, NULL, NULL, NULL, 0),
(104, 74, 158, 293, 'Loan', 50, 50, '2013-02-02', '2014-06-24 17:16:00', NULL, NULL, NULL, NULL, NULL, 0),
(105, 74, 158, 293, 'Loan', 100, 100, '2013-03-03', '2014-06-24 17:17:24', NULL, NULL, NULL, NULL, NULL, 0),
(106, 74, 158, 293, 'Loan', 100, 100, '2013-04-04', '2014-06-24 17:19:27', NULL, NULL, NULL, NULL, NULL, 0),
(107, 74, 158, 293, 'Loan', 100, 100, '2013-04-05', '2014-06-24 17:19:43', NULL, NULL, NULL, NULL, NULL, 0),
(108, 74, 158, 293, 'Loan', 100, 100, '2013-04-06', '2014-06-24 17:20:04', NULL, NULL, NULL, NULL, NULL, 0),
(109, 74, 158, 293, 'Loan', 50, 50, '2013-05-24', '2014-06-24 17:20:55', NULL, NULL, NULL, NULL, NULL, 0),
(110, 74, 158, 293, 'Loan', 50, 50, '2013-06-24', '2014-06-24 17:21:07', NULL, NULL, NULL, NULL, NULL, 0),
(111, 74, 155, 290, 'Loan', 175, 175, '2013-04-04', '2014-06-24 17:22:30', NULL, NULL, NULL, NULL, NULL, 0),
(112, 74, 155, 290, 'Loan', 175, 175, '2013-05-05', '2014-06-24 17:22:50', NULL, NULL, NULL, NULL, NULL, 0),
(113, 74, 155, 290, 'Loan', 175, 175, '2013-06-06', '2014-06-24 17:23:03', NULL, NULL, NULL, NULL, NULL, 0),
(114, 74, 155, 290, 'Loan', 175, 175, '2013-07-07', '2014-06-24 17:23:22', NULL, NULL, NULL, NULL, NULL, 0),
(115, 74, 155, 290, 'Loan', 175, 175, '2013-08-08', '2014-06-24 17:23:39', NULL, NULL, NULL, NULL, NULL, 0),
(116, 74, 155, 290, 'Loan', 160, 160, '2013-09-07', '2014-06-24 17:24:07', NULL, NULL, NULL, NULL, NULL, 0),
(117, 74, 156, 291, 'Loan', 60, 60, '2013-05-05', '2014-06-24 17:25:17', NULL, NULL, NULL, NULL, NULL, 0),
(118, 74, 156, 291, 'Loan', 60, 60, '2013-06-06', '2014-06-24 17:25:27', NULL, NULL, NULL, NULL, NULL, 0),
(119, 74, 156, 291, 'Loan', 60, 60, '2013-07-07', '2014-06-24 17:25:37', NULL, NULL, NULL, NULL, NULL, 0),
(120, 74, 156, 291, 'Loan', 60, 60, '2013-08-08', '2014-06-24 17:25:48', NULL, NULL, NULL, NULL, NULL, 0),
(121, 74, 156, 291, 'Loan', 60, 60, '2013-09-09', '2014-06-24 17:26:17', NULL, NULL, NULL, NULL, NULL, 0),
(122, 74, 156, 291, 'Loan', 130, 130, '2013-10-01', '2014-06-24 17:27:31', NULL, NULL, NULL, NULL, NULL, 0),
(123, 74, 157, 292, 'Loan', 216, 216, '2013-06-03', '2014-06-24 17:28:55', NULL, NULL, NULL, NULL, NULL, 0),
(124, 74, 157, 292, 'Loan', 216, 216, '2013-07-01', '2014-06-24 17:29:17', NULL, NULL, NULL, NULL, NULL, 0),
(125, 74, 157, 292, 'Loan', 216, 216, '2014-06-24', '2014-06-24 17:29:27', NULL, NULL, NULL, NULL, NULL, 0),
(126, 74, 157, 292, 'Loan', 216, 216, '2013-08-01', '2014-06-24 17:30:01', NULL, NULL, NULL, NULL, NULL, 0),
(127, 74, 157, 292, 'Loan', 200, 200, '2013-09-01', '2014-06-24 17:30:22', NULL, NULL, NULL, NULL, NULL, 0),
(128, 74, 157, 292, 'Loan', 16, 16, '2013-11-01', '2014-06-24 17:30:38', NULL, NULL, NULL, NULL, NULL, 0),
(129, 74, 157, 292, 'Loan', 216, 216, '2013-10-01', '2014-06-24 17:31:12', NULL, NULL, NULL, NULL, NULL, 0),
(130, 78, 159, 294, 'Loan', 100, 67800, '2013-03-04', '2014-06-25 20:30:43', NULL, NULL, NULL, NULL, NULL, 0),
(131, 78, 159, 294, 'Loan', 100, 67800, '2013-04-01', '2014-06-25 20:31:10', NULL, NULL, NULL, NULL, NULL, 0),
(132, 78, 159, 294, 'Loan', 100, 67800, '2013-05-01', '2014-06-25 20:35:57', NULL, NULL, NULL, NULL, NULL, 0),
(133, 78, 159, 294, 'Loan', 100, 67800, '2013-06-05', '2014-06-25 20:36:26', NULL, NULL, NULL, NULL, NULL, 0),
(134, 78, 159, 294, 'Loan', 100, 67800, '2013-06-03', '2014-06-25 20:36:41', NULL, NULL, NULL, NULL, NULL, 0),
(135, 58, 167, 304, 'Loan', 48.4364963504, 33179, '2014-05-20', '2014-07-23 15:12:12', NULL, NULL, NULL, NULL, NULL, 0),
(136, 58, 167, 304, 'Loan', 48.4364963504, 33179, '2014-06-20', '2014-07-23 15:15:11', NULL, NULL, NULL, NULL, NULL, 0),
(137, 58, 168, 305, 'Loan', 38.75, 26543.75, '2014-06-27', '2014-07-23 15:45:41', NULL, NULL, NULL, NULL, NULL, 0),
(138, 58, 169, 306, 'Loan', 29.0624963504, 19907.81, '2014-05-27', '2014-07-23 16:21:15', NULL, NULL, NULL, NULL, NULL, 0),
(139, 58, 170, 307, 'Loan', 19.375007299299998, 13271.88, '2014-06-27', '2014-07-23 16:31:49', NULL, NULL, NULL, NULL, NULL, 0),
(140, 58, 171, 308, 'Loan', 19.375007299299998, 13271.88, '2014-06-27', '2014-07-23 16:47:17', NULL, NULL, NULL, NULL, NULL, 0),
(141, 58, 172, 309, 'Loan', 29.0624963504, 19907.81, '2014-06-27', '2014-07-23 17:56:23', NULL, NULL, NULL, NULL, NULL, 0),
(142, 58, 173, 310, 'Loan', 67.8124963504, 46451.56, '2014-06-27', '2014-07-23 18:12:34', NULL, NULL, NULL, NULL, NULL, 0),
(143, 58, 174, 311, 'Loan', 38.75, 26543.75, '2014-06-27', '2014-07-23 18:25:25', NULL, NULL, NULL, NULL, NULL, 0),
(144, 58, 175, 313, 'Loan', 38.75, 26543.75, '2014-06-27', '2014-07-23 18:40:52', NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `savings`
--

CREATE TABLE IF NOT EXISTS `savings` (
  `id` int(11) unsigned NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `type` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=265 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `savings`
--

INSERT INTO `savings` (`id`, `client_id`, `date`, `timestamp`, `type`, `amount`, `currency`, `userid`) VALUES
(1, 3, '2013-10-09', NULL, NULL, 100, 'USD', NULL),
(2, 3, '2013-10-06', NULL, NULL, -50, 'USD', NULL),
(3, 3, '2013-10-11', NULL, NULL, 200, 'USD', NULL),
(4, 3, '2013-10-15', NULL, NULL, 100, 'USD', NULL),
(5, 3, '2013-10-15', NULL, NULL, -200, 'USD', NULL),
(6, 10, '2013-10-20', NULL, NULL, 500, 'USD', 44),
(7, 10, '2013-10-21', NULL, NULL, 200, 'USD', NULL),
(8, 11, '2013-11-05', NULL, NULL, 1000, 'RWF', 48),
(9, 11, '2013-11-05', NULL, NULL, 1000, 'RWF', 48),
(10, 11, '2013-11-05', NULL, NULL, -1000, 'RWF', 48),
(11, 12, '2013-11-05', NULL, NULL, 5000, 'RWF', 48),
(12, 16, '2013-11-26', NULL, NULL, 10000, 'RWF', 49),
(13, 16, '2013-11-26', NULL, NULL, -2000, 'RWF', 49),
(14, 17, '2013-11-26', NULL, NULL, 10, 'RWF', 50),
(15, 17, '2013-11-26', NULL, NULL, 100000, 'RWF', 50),
(16, 17, '2013-11-26', NULL, NULL, 0, 'RWF', 50),
(17, 17, '2013-11-26', NULL, NULL, -2000, 'RWF', 50),
(18, 18, '1969-12-31', NULL, NULL, 10000, 'RWF', 50),
(19, 18, '1969-12-31', NULL, NULL, -2000, 'RWF', 50),
(20, 19, '2013-11-29', NULL, NULL, 10000, 'RWF', 49),
(21, 20, '2013-12-02', NULL, NULL, 10, '', 52),
(22, 21, '2013-12-02', NULL, NULL, 10, '', 52),
(23, 22, '2013-12-02', NULL, NULL, 10, '', 52),
(24, 23, '2013-12-02', NULL, NULL, 10000, '', 52),
(25, 24, '2013-12-02', NULL, NULL, 10000, 'RWF', 49),
(26, 24, '2013-12-02', NULL, NULL, -2000, 'RWF', 49),
(27, 25, '2013-12-02', NULL, NULL, 40000, 'RWF', 49),
(28, 25, '2013-12-02', NULL, NULL, 0, 'RWF', 49),
(29, 25, '2013-12-02', NULL, NULL, 5000, 'RWF', 49),
(30, 25, '2013-12-02', NULL, NULL, -10000, 'RWF', 49),
(31, 28, '2013-12-03', NULL, NULL, 16200, 'RWF', 49),
(32, 29, '2013-12-03', NULL, NULL, 46275, 'RWF', 49),
(33, 30, '2013-12-03', NULL, NULL, 45815, 'RWF', 49),
(34, 31, '2013-12-03', NULL, NULL, 40825, 'RWF', 49),
(35, 32, '2013-12-03', NULL, NULL, 60000, 'RWF', 49),
(36, 33, '2013-12-03', NULL, NULL, 40825, 'RWF', 49),
(37, 34, '2013-12-03', NULL, NULL, 16325, 'RWF', 49),
(38, 35, '2013-12-03', NULL, NULL, 92610, 'RWF', 49),
(39, 36, '2013-12-03', NULL, NULL, 1440, 'RWF', 49),
(40, 37, '2013-12-03', NULL, NULL, 56000, 'RWF', 49),
(41, 39, '2013-12-03', NULL, NULL, 20630, 'RWF', 49),
(42, 40, '2013-12-03', NULL, NULL, 16625, 'RWF', 49),
(43, 41, '2013-12-03', NULL, NULL, 1, 'RWF', 49),
(44, 42, '2013-12-03', NULL, NULL, 50, 'USD', 54),
(45, 43, '2013-12-03', NULL, NULL, 241, 'USD', 54),
(46, 44, '2013-12-04', NULL, NULL, 10, 'USD', 56),
(47, 44, '2013-12-04', NULL, NULL, 103, 'USD', 56),
(48, 44, '2013-12-04', NULL, NULL, 0, 'USD', 56),
(49, 44, '1969-12-31', NULL, NULL, -123, 'USD', 56),
(50, 46, '2013-12-09', NULL, NULL, 300, 'RWF', 44),
(51, 47, '2013-12-16', NULL, NULL, 1111, 'RWF', 59),
(52, 48, '2013-12-16', NULL, NULL, 1000, 'RWF', 49),
(53, 49, '2013-12-16', NULL, NULL, 1000, 'RWF', 60),
(54, 50, '2014-01-13', '2014-01-13 19:32:07', NULL, 20000, 'VND', 36),
(55, 53, '2014-01-30', '2014-01-30 18:31:18', 'Deposit', 200, 'USD', 58),
(56, 54, '2014-01-30', '2014-01-30 19:24:49', 'Deposit', 200, 'USD', 58),
(57, 54, '2014-01-30', '2014-01-30 19:26:27', 'Withdraw', -50, 'USD', 58),
(58, 52, '2014-01-20', '2014-02-04 19:14:33', 'Deposit', 136200, 'RWF', 58),
(59, 52, '2014-02-01', '2014-02-04 19:15:45', 'Withdraw', -34050, 'RWF', 58),
(60, 55, '2014-03-17', '2014-03-17 13:50:34', 'Deposit', 1000, 'RWF', 50),
(61, 55, '2014-03-17', '2014-03-17 13:51:32', 'Deposit', 500, 'RWF', 50),
(62, 55, '2014-03-17', '2014-03-17 13:52:14', 'Withdraw', -500, 'RWF', 50),
(63, 48, '2014-03-31', '2014-03-31 10:22:36', 'Deposit', 40000, 'RWF', 49),
(64, 48, '2014-03-30', '2014-03-31 10:22:52', 'Withdraw', -500, 'RWF', 49),
(65, 48, '2014-03-31', '2014-03-31 10:29:35', 'Deposit', 5000, 'RWF', 49),
(66, 48, '2014-03-31', '2014-03-31 10:30:52', 'Withdraw', -1000, 'RWF', 49),
(67, 71, '2014-03-31', '2014-03-31 11:19:05', 'Deposit', 26800, 'RWF', 62),
(68, 72, '2014-03-31', '2014-03-31 11:33:21', 'Deposit', 2000, 'RWF', 62),
(69, 72, '2014-03-31', '2014-03-31 11:34:32', 'Deposit', 2500, 'USD', 62),
(70, 72, '2014-03-31', '2014-03-31 11:35:55', 'Withdraw', -500, 'USD', 62),
(71, 73, '2014-03-31', '2014-03-31 11:42:34', 'Deposit', 5000, 'RWF', 62),
(72, 73, '2014-03-31', '2014-03-31 11:43:06', 'Withdraw', -3000, 'USD', 62),
(73, 74, '2014-03-31', '2014-03-31 12:19:13', 'Deposit', 9570, 'RWF', 62),
(74, 75, '2014-03-31', '2014-03-31 12:51:16', 'Deposit', 800, 'RWF', 62),
(75, 76, '2014-03-31', '2014-03-31 12:51:17', 'Deposit', 800, 'RWF', 62),
(76, 77, '2014-03-31', '2014-03-31 12:51:17', 'Deposit', 800, 'RWF', 62),
(77, 78, '2014-03-31', '2014-03-31 12:56:27', 'Deposit', 5800, 'USD', 62),
(78, 79, '2014-03-31', '2014-03-31 13:01:48', 'Deposit', 14100, 'RWF', 62),
(79, 81, '2014-03-31', '2014-03-31 13:15:34', 'Deposit', 96000, 'RWF', 62),
(80, 82, '2014-03-31', '2014-03-31 13:22:01', 'Deposit', 425800, 'RWF', 62),
(81, 83, '2014-03-31', '2014-03-31 13:22:02', 'Deposit', 425800, 'RWF', 62),
(82, 84, '2014-03-31', '2014-03-31 13:22:02', 'Deposit', 425800, 'RWF', 62),
(83, 85, '2014-03-31', '2014-03-31 13:29:05', 'Deposit', 800, 'RWF', 62),
(84, 86, '2014-03-31', '2014-03-31 13:34:32', 'Deposit', 4800, 'RWF', 62),
(85, 87, '2014-03-31', '2014-03-31 13:39:36', 'Deposit', 1400, 'RWF', 62),
(86, 72, '2014-03-31', '2014-03-31 13:41:46', 'Withdraw', -1000, 'USD', 62),
(87, 88, '2014-03-31', '2014-03-31 13:50:56', 'Deposit', 300, 'RWF', 62),
(88, 89, '2014-03-31', '2014-03-31 13:55:14', 'Deposit', 800, 'RWF', 62),
(89, 91, '2014-03-31', '2014-03-31 14:02:59', 'Deposit', 800, 'RWF', 62),
(90, 92, '2014-03-31', '2014-03-31 14:07:12', 'Deposit', 8800, 'RWF', 62),
(91, 93, '2014-03-31', '2014-03-31 14:12:36', 'Deposit', 800, 'RWF', 62),
(92, 95, '2014-03-31', '2014-03-31 14:17:47', 'Deposit', 200, 'RWF', 62),
(93, 96, '2014-03-31', '2014-03-31 14:20:55', 'Deposit', 800, 'RWF', 62),
(94, 97, '2014-03-31', '2014-03-31 14:27:21', 'Deposit', 2450, 'RWF', 62),
(95, 98, '2014-03-31', '2014-03-31 14:30:04', 'Deposit', 800, 'RWF', 62),
(96, 99, '2014-03-31', '2014-03-31 14:32:27', 'Deposit', 800, 'RWF', 62),
(97, 100, '2014-03-31', '2014-03-31 14:38:12', 'Deposit', 1000, 'RWF', 62),
(98, 101, '2014-03-31', '2014-03-31 14:43:11', 'Deposit', 1800, 'RWF', 62),
(99, 102, '2014-03-31', '2014-03-31 14:49:32', 'Deposit', 50, 'RWF', 62),
(100, 103, '2014-03-31', '2014-03-31 14:52:24', 'Deposit', 800, 'RWF', 62),
(101, 105, '2014-03-31', '2014-03-31 15:03:01', 'Deposit', 640, 'RWF', 62),
(102, 106, '2014-03-31', '2014-03-31 15:08:07', 'Deposit', 800, 'RWF', 62),
(103, 107, '2014-03-31', '2014-03-31 15:13:23', 'Deposit', 392400, 'RWF', 62),
(104, 108, '2014-03-31', '2014-03-31 15:20:05', 'Deposit', 5, 'RWF', 62),
(105, 109, '2014-03-31', '2014-03-31 15:24:59', 'Deposit', 350240, 'RWF', 62),
(106, 110, '2014-03-31', '2014-03-31 15:29:28', 'Deposit', 1425, 'RWF', 62),
(107, 111, '2014-03-31', '2014-03-31 15:41:14', 'Deposit', 400, 'RWF', 62),
(108, 113, '2014-03-31', '2014-03-31 15:47:18', 'Deposit', 100720, 'RWF', 62),
(109, 114, '2014-03-31', '2014-03-31 15:53:15', 'Deposit', 4800, 'RWF', 62),
(110, 115, '2014-03-31', '2014-03-31 15:57:59', 'Deposit', 1500, 'RWF', 62),
(111, 116, '2014-03-31', '2014-03-31 16:03:28', 'Deposit', 50800, 'RWF', 62),
(112, 117, '2014-03-31', '2014-03-31 16:07:03', 'Deposit', 1100, 'RWF', 62),
(113, 118, '2014-03-31', '2014-03-31 16:12:23', 'Deposit', 800, 'RWF', 62),
(114, 119, '2014-03-31', '2014-03-31 16:16:55', 'Deposit', 400, 'RWF', 62),
(115, 120, '2014-03-31', '2014-03-31 16:24:20', 'Deposit', 62200, 'RWF', 62),
(116, 121, '2014-03-31', '2014-03-31 16:28:02', 'Deposit', 9400, 'RWF', 62),
(117, 122, '2014-03-31', '2014-03-31 16:33:31', 'Deposit', 168325, 'RWF', 62),
(118, 123, '2014-03-31', '2014-03-31 16:38:02', 'Deposit', 20000, 'RWF', 62),
(119, 124, '2014-03-31', '2014-03-31 16:41:11', 'Deposit', 4200, 'RWF', 62),
(120, 125, '2014-03-31', '2014-03-31 16:45:42', 'Deposit', 1000, 'RWF', 62),
(121, 126, '2014-03-31', '2014-03-31 16:51:53', 'Deposit', 15800, 'RWF', 62),
(122, 127, '2014-03-31', '2014-03-31 16:56:35', 'Deposit', 800, 'RWF', 62),
(123, 128, '2014-03-31', '2014-03-31 17:03:46', 'Deposit', 270, 'RWF', 62),
(124, 129, '2014-03-31', '2014-03-31 17:06:48', 'Deposit', 800, 'RWF', 62),
(125, 130, '2014-03-31', '2014-03-31 17:11:47', 'Deposit', 24567, 'RWF', 62),
(126, 131, '2014-03-31', '2014-03-31 17:15:42', 'Deposit', 68750, 'RWF', 62),
(127, 132, '2014-03-31', '2014-03-31 17:19:13', 'Deposit', 32000, 'RWF', 62),
(128, 133, '2014-03-31', '2014-03-31 17:21:50', 'Deposit', 800, 'RWF', 62),
(129, 134, '2014-03-31', '2014-03-31 17:25:09', 'Deposit', 10800, 'RWF', 62),
(130, 135, '2014-03-31', '2014-03-31 17:29:32', 'Deposit', 1233, 'RWF', 62),
(131, 136, '2014-03-31', '2014-03-31 17:33:24', 'Deposit', 800, 'RWF', 62),
(132, 137, '2014-03-31', '2014-03-31 17:37:36', 'Deposit', 9300, 'RWF', 62),
(133, 138, '2014-03-31', '2014-03-31 17:43:19', 'Deposit', 5800, 'RWF', 62),
(134, 139, '2014-03-31', '2014-03-31 17:51:44', 'Deposit', 19800, 'RWF', 62),
(135, 131, '2014-03-31', '2014-03-31 17:53:32', 'Withdraw', -20000, 'USD', 62),
(136, 141, '2014-03-31', '2014-03-31 18:05:43', 'Deposit', 3800, 'RWF', 62),
(137, 142, '2014-03-31', '2014-03-31 18:06:36', 'Deposit', 3800, 'USD', 62),
(138, 143, '2014-03-31', '2014-03-31 18:09:09', 'Deposit', 720, 'RWF', 62),
(139, 144, '2014-03-31', '2014-03-31 18:13:09', 'Deposit', 750, 'RWF', 62),
(140, 145, '2014-03-31', '2014-03-31 18:16:17', 'Deposit', 1800, 'RWF', 62),
(141, 146, '2014-03-31', '2014-03-31 18:18:28', 'Deposit', 44800, 'RWF', 62),
(142, 147, '2014-03-31', '2014-03-31 18:21:52', 'Deposit', 1000, 'RWF', 62),
(143, 148, '2014-03-31', '2014-03-31 18:25:28', 'Deposit', 1900, 'RWF', 62),
(144, 149, '2014-03-31', '2014-03-31 18:38:11', 'Deposit', 800, 'RWF', 62),
(145, 150, '2014-03-31', '2014-03-31 18:42:42', 'Deposit', 3000, 'RWF', 62),
(146, 151, '2014-04-03', '2014-04-03 09:01:41', 'Deposit', 800, 'RWF', 62),
(147, 153, '2014-04-03', '2014-04-03 09:08:07', 'Deposit', 1800, 'RWF', 62),
(148, 154, '2014-04-03', '2014-04-03 09:11:33', 'Deposit', 1000, 'RWF', 62),
(149, 155, '2014-04-03', '2014-04-03 09:20:48', 'Deposit', 3800, 'USD', 62),
(150, 156, '2014-04-03', '2014-04-03 09:41:05', 'Deposit', 800, 'RWF', 62),
(151, 157, '2014-04-03', '2014-04-03 09:44:10', 'Deposit', 1800, 'RWF', 62),
(152, 158, '2014-04-03', '2014-04-03 09:46:50', 'Deposit', 1100, 'RWF', 62),
(153, 159, '2014-04-03', '2014-04-03 09:48:32', 'Deposit', 800, 'RWF', 62),
(154, 160, '2014-04-03', '2014-04-03 09:51:40', 'Deposit', 649000, 'RWF', 62),
(155, 161, '2014-04-03', '2014-04-03 09:53:37', 'Deposit', 4800, 'RWF', 62),
(156, 162, '2014-04-03', '2014-04-03 09:58:03', 'Deposit', 10600, 'RWF', 62),
(157, 163, '2014-04-03', '2014-04-03 10:00:50', 'Deposit', 205, 'RWF', 62),
(158, 164, '2014-04-03', '2014-04-03 10:04:33', 'Deposit', 4800, 'RWF', 62),
(159, 165, '2014-04-03', '2014-04-03 10:09:01', 'Deposit', 800, 'RWF', 62),
(160, 169, '2014-04-03', '2014-04-03 10:18:05', 'Deposit', 1400, 'USD', 62),
(161, 170, '2014-04-03', '2014-04-03 10:23:21', 'Deposit', 1000, 'RWF', 62),
(162, 171, '2014-04-03', '2014-04-03 10:27:23', 'Deposit', 820, 'RWF', 62),
(163, 174, '2014-04-03', '2014-04-03 11:47:25', 'Deposit', 363600, 'RWF', 62),
(164, 175, '2014-04-03', '2014-04-03 11:50:29', 'Deposit', 1400, 'RWF', 62),
(165, 176, '2014-04-03', '2014-04-03 11:53:30', 'Deposit', 800, 'RWF', 62),
(166, 177, '2014-04-03', '2014-04-03 11:57:04', 'Deposit', 459490, 'RWF', 62),
(167, 178, '2014-04-03', '2014-04-03 12:00:19', 'Deposit', 180600, 'RWF', 62),
(168, 179, '2014-04-03', '2014-04-03 12:03:54', 'Deposit', 800, 'RWF', 62),
(169, 180, '2014-04-03', '2014-04-03 12:06:09', 'Deposit', 800, 'USD', 62),
(170, 181, '2014-04-03', '2014-04-03 12:10:08', 'Deposit', 13340, 'RWF', 62),
(171, 182, '2014-04-03', '2014-04-03 12:16:32', 'Deposit', 600, 'RWF', 62),
(172, 183, '2014-04-03', '2014-04-03 12:18:29', 'Deposit', 800, 'RWF', 62),
(173, 184, '2014-04-03', '2014-04-03 12:21:02', 'Deposit', 800, 'RWF', 62),
(174, 185, '2014-04-03', '2014-04-03 12:23:55', 'Deposit', 800, 'USD', 62),
(175, 187, '2014-04-03', '2014-04-03 12:28:51', 'Deposit', 1000, 'USD', 62),
(176, 188, '2014-04-03', '2014-04-03 12:34:45', 'Deposit', 1198800, 'RWF', 62),
(177, 189, '2014-04-03', '2014-04-03 13:15:28', 'Deposit', 800, 'RWF', 62),
(178, 191, '2014-04-03', '2014-04-03 13:22:20', 'Deposit', 800, 'RWF', 62),
(179, 192, '2014-04-03', '2014-04-03 13:30:02', 'Deposit', 34800, 'RWF', 62),
(180, 193, '2014-04-03', '2014-04-03 13:33:20', 'Deposit', 6800, 'RWF', 62),
(181, 194, '2014-04-03', '2014-04-03 13:38:34', 'Deposit', 200, 'RWF', 62),
(182, 195, '2014-04-03', '2014-04-03 13:41:54', 'Deposit', 800, 'RWF', 62),
(183, 196, '2014-04-03', '2014-04-03 13:49:56', 'Deposit', 800, 'RWF', 62),
(184, 197, '2014-04-03', '2014-04-03 13:56:07', 'Deposit', 800, 'RWF', 62),
(185, 198, '2014-04-03', '2014-04-03 13:58:38', 'Deposit', 1000, 'RWF', 62),
(186, 199, '2014-04-03', '2014-04-03 14:01:12', 'Deposit', 800, 'RWF', 62),
(187, 200, '2014-04-03', '2014-04-03 14:06:52', 'Deposit', 96800, 'RWF', 62),
(188, 201, '2014-04-03', '2014-04-03 14:09:34', 'Deposit', 800, 'RWF', 62),
(189, 202, '2014-04-03', '2014-04-03 14:20:19', 'Deposit', 1000, 'RWF', 62),
(190, 203, '2014-04-03', '2014-04-03 14:24:55', 'Deposit', 6021, 'RWF', 62),
(191, 204, '2014-04-03', '2014-04-03 14:27:19', 'Deposit', 800, 'USD', 62),
(192, 205, '2014-04-03', '2014-04-03 15:15:05', 'Deposit', 1928400, 'RWF', 62),
(193, 206, '2014-04-03', '2014-04-03 15:18:46', 'Deposit', 4800, 'RWF', 62),
(194, 207, '2014-04-03', '2014-04-03 15:22:39', 'Deposit', 800, 'RWF', 62),
(195, 208, '2014-04-03', '2014-04-03 15:25:59', 'Deposit', 200, 'RWF', 62),
(196, 209, '2014-04-03', '2014-04-03 15:28:38', 'Deposit', 2150, 'RWF', 62),
(197, 210, '2014-04-03', '2014-04-03 15:30:21', 'Deposit', 1825, 'RWF', 62),
(198, 212, '2014-04-03', '2014-04-03 15:35:14', 'Deposit', 10200, 'RWF', 62),
(199, 213, '2014-04-03', '2014-04-03 15:38:09', 'Deposit', 1000, 'RWF', 62),
(200, 214, '2014-04-03', '2014-04-03 15:41:39', 'Deposit', 9800, 'RWF', 62),
(201, 216, '2014-04-03', '2014-04-03 15:45:57', 'Deposit', 497700, 'RWF', 62),
(202, 215, '2014-04-03', '2014-04-03 15:45:57', 'Deposit', 497700, 'RWF', 62),
(203, 217, '2014-04-03', '2014-04-03 15:50:08', 'Deposit', 509, 'RWF', 62),
(204, 218, '2014-04-03', '2014-04-03 15:51:49', 'Deposit', 62900, 'RWF', 62),
(205, 219, '2014-04-03', '2014-04-03 15:54:55', 'Deposit', 300800, 'RWF', 62),
(206, 221, '2014-04-03', '2014-04-03 16:01:41', 'Deposit', 600, 'RWF', 62),
(207, 222, '2014-04-03', '2014-04-03 16:04:10', 'Deposit', 4400, 'RWF', 62),
(208, 223, '2014-04-03', '2014-04-03 16:06:57', 'Deposit', 400, 'RWF', 62),
(209, 224, '2014-04-03', '2014-04-03 16:09:48', 'Deposit', 3800, 'RWF', 62),
(210, 225, '2014-04-03', '2014-04-03 16:11:36', 'Deposit', 105300, 'NOK', 62),
(211, 226, '2014-04-03', '2014-04-03 16:13:26', 'Deposit', 80, 'RWF', 62),
(212, 227, '2014-04-03', '2014-04-03 16:16:55', 'Deposit', 1000, 'RWF', 62),
(213, 231, '2014-04-03', '2014-04-03 16:34:52', 'Deposit', 3600, 'RWF', 62),
(214, 232, '2014-04-03', '2014-04-03 16:37:39', 'Deposit', 600, 'RWF', 62),
(215, 233, '2014-04-03', '2014-04-03 16:39:21', 'Deposit', 800, 'RWF', 62),
(216, 234, '2014-04-03', '2014-04-03 16:42:15', 'Deposit', 3060, 'USD', 62),
(217, 237, '2014-04-03', '2014-04-03 17:06:08', 'Deposit', 230, 'RWF', 62),
(218, 238, '2014-04-03', '2014-04-03 17:08:33', 'Deposit', 9000, 'RWF', 62),
(219, 239, '2014-04-03', '2014-04-03 17:23:55', 'Deposit', 800, 'RWF', 62),
(220, 240, '2014-04-03', '2014-04-03 17:28:04', 'Deposit', 20000, 'RWF', 62),
(221, 241, '2014-04-03', '2014-04-03 17:34:18', 'Deposit', 4600, 'RWF', 62),
(222, 243, '2014-04-03', '2014-04-03 17:38:16', 'Deposit', 800, 'RWF', 62),
(223, 244, '2014-04-03', '2014-04-03 17:41:11', 'Deposit', 800, 'RWF', 62),
(224, 247, '2014-04-08', '2014-04-08 12:49:36', 'Deposit', 1000, 'RWF', 62),
(225, 247, '2014-04-08', '2014-04-08 12:59:23', 'Withdraw', -500, 'USD', 62),
(226, 247, '2014-04-08', '2014-04-08 13:27:48', 'Deposit', 500, 'USD', 62),
(227, 246, '2014-04-08', '2014-04-08 13:58:27', 'Deposit', 2000, 'RWF', 49),
(228, 246, '2014-04-08', '2014-04-08 14:19:18', 'Withdraw', -1000, 'RWF', 49),
(229, 246, '2014-04-08', '2014-04-08 14:19:22', 'Withdraw', -1000, 'RWF', 49),
(230, 246, '2014-04-08', '2014-04-08 14:19:39', 'Deposit', 500, 'RWF', 49),
(231, 198, '2014-04-09', '2014-04-09 08:54:20', 'Deposit', 45000, 'RWF', 62),
(232, 198, '2014-04-09', '2014-04-09 08:54:44', 'Deposit', 44000, 'RWF', 62),
(233, 198, '2014-04-09', '2014-04-09 08:56:34', 'Withdraw', -44000, 'RWF', 62),
(234, 198, '2014-04-09', '2014-04-09 09:08:45', 'Withdraw', -44500, 'RWF', 62),
(235, 247, '2014-04-09', '2014-04-10 06:45:35', 'Deposit', 1000, 'RWF', 62),
(236, 247, '2014-04-10', '2014-04-10 07:02:28', 'Withdraw', -1000, 'RWF', 62),
(237, 39, '2014-04-10', '2014-04-10 09:58:57', 'Deposit', 56635, 'RWF', 62),
(238, 39, '2014-04-10', '2014-04-10 10:00:17', 'Deposit', 4000, 'RWF', 62),
(239, 39, '2014-04-10', '2014-04-10 10:00:53', 'Deposit', 80000, 'RWF', 62),
(240, 39, '2014-04-10', '2014-04-10 10:02:02', 'Withdraw', -160000, 'RWF', 62),
(241, 248, '2014-04-10', '2014-04-10 10:33:09', 'Deposit', 71300, 'RWF', 62),
(242, 248, '2014-04-10', '2014-04-10 10:33:36', 'Withdraw', -30000, 'RWF', 62),
(243, 81, '2014-03-27', '2014-04-10 11:26:03', 'Withdraw', -40000, 'RWF', 62),
(244, 81, '2014-04-10', '2014-04-10 11:27:58', 'Withdraw', -40000, 'RWF', 62),
(245, 144, '2014-04-09', '2014-04-10 11:51:40', 'Deposit', 55000, 'RWF', 62),
(246, 249, '2014-04-10', '2014-04-10 12:27:57', 'Deposit', 4840, 'RWF', 62),
(247, 26, '2014-04-17', '2014-04-17 14:39:02', 'Deposit', 15000, 'RWF', 62),
(248, 26, '2014-04-17', '2014-04-17 14:39:03', 'Deposit', 15000, 'RWF', 62),
(249, 30, '2014-04-17', '2014-04-17 14:47:57', 'Withdraw', -40000, 'RWF', 62),
(250, 32, '2014-04-17', '2014-04-17 14:49:52', 'Withdraw', -1000, 'RWF', 62),
(251, 32, '2014-04-17', '2014-04-17 14:50:47', 'Withdraw', -40000, 'RWF', 62),
(252, 36, '2014-04-17', '2014-04-17 15:08:32', 'Withdraw', -40000, 'RWF', 62),
(253, 288, '2014-06-24', '2014-06-24 15:22:43', 'Deposit', 200, 'USD', 74),
(254, 289, '2014-06-24', '2014-06-24 15:40:46', 'Deposit', 100, 'USD', 74),
(255, 291, '2014-06-24', '2014-06-24 16:00:23', 'Deposit', 50, 'USD', 74),
(256, 292, '2014-06-24', '2014-06-24 16:06:28', 'Deposit', 100, 'USD', 74),
(257, 293, '2014-06-24', '2014-06-24 16:44:38', 'Deposit', 200, 'USD', 74),
(258, 314, '2014-11-15', '2014-11-15 22:47:39', 'Deposit', 100000, 'RWF', 49),
(259, 258, '2015-02-09', '2015-02-09 17:29:04', 'Deposit', 500, 'GHS', 70),
(260, 315, '2015-08-27', '2015-08-27 22:26:05', 'Deposit', 200, 'RWF', 58),
(261, 315, '2015-08-27', '2015-08-27 22:26:45', 'Deposit', 500, 'RWF', 58),
(262, 315, '2015-08-27', '2015-08-27 22:27:28', 'Withdraw', -100, 'RWF', 58),
(263, 315, '2015-09-02', '2015-09-02 07:45:20', 'Deposit', 500, 'RWF', 58),
(264, 315, '2015-09-02', '2015-09-02 07:45:28', 'Withdraw', -200, 'RWF', 58);

-- --------------------------------------------------------

--
-- Table structure for table `scheduled_payment`
--

CREATE TABLE IF NOT EXISTS `scheduled_payment` (
  `id` int(11) NOT NULL,
  `projectid` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `enddate` date NOT NULL,
  `plan` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `seed_account`
--

CREATE TABLE IF NOT EXISTS `seed_account` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `projectid` int(11) NOT NULL,
  `orderid` int(11) NOT NULL,
  `reason` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=85 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `seed_account`
--

INSERT INTO `seed_account` (`id`, `userid`, `projectid`, `orderid`, `reason`, `amount`, `timestamp`) VALUES
(55, 45, 52, 63, 'Occasion Beads Paid off 98.1308411216% the loan, Thank You', 181, '2013-09-12 16:51:11'),
(58, 47, 80, 65, 'Peter Food Paid off 100% the loan, Thank You', 460, '2013-09-20 14:03:30'),
(56, 39, 52, 62, 'Occasion Beads Paid off 98.1308411216% the loan, Thank You', 271, '2013-09-12 16:51:11'),
(57, 39, 0, 64, 'Used to plant Seed', -250, '2013-09-12 16:59:25'),
(59, 47, 0, 66, 'Used to plant Seed', -350, '2013-09-26 00:14:15'),
(60, 61, 126, 71, 'My water tank Paid off 77.7467890392% the loan, Thank You', 429, '2014-01-13 19:55:15'),
(61, 64, 127, 90, 'Alexis Produce Farm Paid off 90% the loan, Thank You', 414, '2014-01-30 18:46:58'),
(62, 65, 128, 92, 'Alexis Produce Farm Paid off 100% the loan, Thank You', 92, '2014-02-03 16:48:10'),
(63, 11, 51, 69, 'Project has been Fully Funded when check was received', 25, '2014-05-02 14:04:13'),
(64, 11, 51, 69, 'Project has been Fully Funded when check was received', 25, '2014-05-02 14:04:13'),
(65, 11, 49, 72, 'Project has been Fully Funded when check was received', 500, '2014-05-02 14:04:19'),
(66, 75, 155, 94, 'Making Music  Paid off 107% of the loan, Thank You', 248, '2014-06-25 16:21:06'),
(67, 76, 155, 95, 'Making Music  Paid off 107% of the loan, Thank You', 248, '2014-06-25 16:21:06'),
(68, 77, 155, 96, 'Making Music  Paid off 107% of the loan, Thank You', 298, '2014-06-25 16:21:06'),
(69, 75, 153, 94, 'Doe dubby Paid off 92% of the loan, Thank You', 85, '2014-06-25 16:21:07'),
(70, 76, 153, 95, 'Doe dubby Paid off 92% of the loan, Thank You', 423, '2014-06-25 16:21:07'),
(71, 77, 153, 96, 'Doe dubby Paid off 92% of the loan, Thank You', 339, '2014-06-25 16:21:07'),
(72, 75, 156, 94, 'Making Movies  Paid off 89% of the loan, Thank You', 124, '2014-06-25 16:21:09'),
(73, 76, 156, 95, 'Making Movies  Paid off 89% of the loan, Thank You', 103, '2014-06-25 16:21:09'),
(74, 77, 156, 96, 'Making Movies  Paid off 89% of the loan, Thank You', 103, '2014-06-25 16:21:09'),
(75, 75, 157, 94, 'Growing pumpkin Paid off 90% of the loan, Thank You', 704, '2014-06-25 16:21:12'),
(76, 76, 157, 95, 'Growing pumpkin Paid off 90% of the loan, Thank You', 104, '2014-06-25 16:21:12'),
(77, 77, 157, 96, 'Growing pumpkin Paid off 90% of the loan, Thank You', 186, '2014-06-25 16:21:12'),
(78, 37, 0, 0, 'Per Bobs Request. email: 12/13/12\n\nMike Land\nmland71@gmail.com\nPassword: mikepass\n$500.00 to a n  agriculture loan\n$500.00 to Kids Play Project\n1317 Ptarmigan Loop\nPark City\nUtah 84098\ncell 858 382 4575        ', 1000, '2014-08-20 17:43:28'),
(79, 38, 0, 0, 'Per Bobs Email: 12/13/12', 3000, '2014-08-20 17:47:06'),
(80, 40, 0, 0, 'Per Bobs request: 12/24/12', 1000, '2014-08-20 17:51:11'),
(81, 80, 0, 0, 'Per Bobs Request: 12/24/12', 2000, '2014-08-20 17:53:24'),
(82, 37, 0, 97, 'Used to plant Seed', -500, '2014-08-21 17:03:55'),
(83, 11, 0, 99, 'Used to plant Seed', -550, '2014-11-15 21:20:33'),
(84, 82, 40, 98, 'Project has been Fully Funded when check was received', 8000, '2014-11-18 20:10:19');

-- --------------------------------------------------------

--
-- Table structure for table `site_cms`
--

CREATE TABLE IF NOT EXISTS `site_cms` (
  `cms_id` int(3) NOT NULL,
  `cms_page` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `cms_title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `edit_date` int(11) NOT NULL COMMENT 'Will be set to epoch',
  `edit_by` int(5) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_cms`
--

INSERT INTO `site_cms` (`cms_id`, `cms_page`, `cms_title`, `content`, `edit_date`, `edit_by`) VALUES
(1, 'welcome_message', 'Who We Are', 'People Helping People International (PHPI) is a non-profit organization that provides aid directly to people around the world who make two dollars or less per day through microseed loans. For them, help is not a welfare program but a loan which, when paid back, provides dignity and empowerment and can enable the poor to double their two-dollar-a-day income.<br /> </p><p>When you give through PHPI?s microseed loan program, you know your donation reaches the person or community you want to help. We work with non-governmental organizations (NGOs) to identify people in need. We post a picture of the person or group and a description of how the loan will be used on our website. You choose which project you want to support. Your donation, which is tax deductible, is directed to that person or group. When it is repaid, you redirect the principal to another person or group.</p>\r\n<p><br />When you become a supporter of PHPI, your helping hands join together with many others to bring hope into the lives of the poor. It is through your faith, if only the size of a mustard seed, that you will help us move mountains of need to become mountains of joy. People Helping People International encourages you to explore our site and determine how you, too, can sow microseed loans of unconditional love.<br />', 1441470740, 91);

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE IF NOT EXISTS `transaction` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `orderid` int(11) DEFAULT NULL,
  `projectid` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` double NOT NULL,
  `specialngo` int(11) DEFAULT NULL,
  `specialngoamount` double NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=139 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`id`, `userid`, `orderid`, `projectid`, `date`, `timestamp`, `type`, `amount`, `specialngo`, `specialngoamount`) VALUES
(87, 47, 65, 80, '2013-09-20', '2013-09-20 14:03:30', 'Re-Payment', 40, NULL, 0),
(86, 47, 65, 80, '2013-09-20', '2013-09-20 13:56:10', 'Seed', 40, NULL, 0),
(85, 39, 64, NULL, '2013-09-12', '2013-09-12 16:59:25', 'Seed', 20, 0, 0),
(84, 39, 62, 52, '2013-09-12', '2013-09-12 16:51:11', 'Re-Payment', 23.5514018692, NULL, 0),
(83, 45, 63, 52, '2013-09-12', '2013-09-12 16:51:11', 'Re-Payment', 15.700934579500002, NULL, 0),
(82, 45, 63, 52, '2013-09-12', '2013-09-12 16:42:07', 'Seed', 40, NULL, 0),
(81, 39, 62, 52, '2013-09-12', '2013-09-12 16:42:05', 'Seed', 40, NULL, 0),
(79, 39, 59, 52, '2013-08-30', '2013-08-30 18:25:06', 'Re-Payment', 16.4485981308, NULL, 0),
(78, 38, 60, 52, '2013-08-30', '2013-08-30 18:25:06', 'Re-Payment', 24.6728971962, NULL, 0),
(77, 39, 59, NULL, '2013-08-30', '2013-08-30 18:09:57', 'Seed', 16, 0, 0),
(76, 24, 58, 16, '2013-08-29', '2013-08-29 22:39:51', 'Seed', 34, NULL, 0),
(75, 23, 57, 16, '2013-08-29', '2013-08-29 22:38:02', 'Seed', 6, NULL, 0),
(74, 42, 52, 16, '2013-08-29', '2013-08-29 22:31:53', 'Seed', 2, NULL, 0),
(73, 39, 55, NULL, '2013-08-29', '2013-08-29 20:42:18', 'Seed', 2, 0, 0),
(72, 39, 51, NULL, '2013-08-23', '2013-08-23 18:56:23', 'Seed', 4, 0, 0),
(71, 39, 50, NULL, '2013-08-21', '2013-08-21 13:19:18', 'Seed', 48, 0, 0),
(70, 39, 49, NULL, '2013-08-21', '2013-08-21 12:54:14', 'Seed', 2, 0, 0),
(69, 39, 48, NULL, '2013-08-21', '2013-08-21 12:53:28', 'Seed', 2, 0, 0),
(68, 39, 47, 53, '2013-08-20', '2013-08-21 06:34:16', 'Seed', 48, NULL, 0),
(67, 39, 47, 53, '2013-08-20', '2013-08-21 06:28:02', 'Seed', 48, NULL, 0),
(66, 37, 46, 20, '2013-08-20', '2013-08-20 20:30:14', 'Seed', 44, NULL, 0),
(65, 37, 46, 20, '2013-08-20', '2013-08-20 20:30:10', 'Seed', 44, NULL, 0),
(64, 37, 46, 20, '2013-08-20', '2013-08-20 20:28:06', 'Seed', 44, NULL, 0),
(63, 37, 46, 20, '2013-08-20', '2013-08-20 20:27:59', 'Seed', 44, NULL, 0),
(88, 47, 66, NULL, '2013-09-25', '2013-09-26 00:14:15', 'Seed', 28, 0, 0),
(89, 11, 67, 105, '2013-11-07', '2013-11-07 17:39:59', 'Seed', 83.2, NULL, 0),
(90, 11, 68, 110, '2013-12-02', '2013-12-02 11:39:15', 'Seed', 35.6, NULL, 0),
(91, 61, 70, 50, '2014-01-13', '2014-01-13 19:15:26', 'Seed', 80, NULL, 0),
(92, 61, 71, 126, '2014-01-13', '2014-01-13 19:48:38', 'Seed', 48, NULL, 0),
(93, 61, 71, 126, '2014-01-13', '2014-01-13 19:55:15', 'Re-Payment', 37.318458738800004, NULL, 0),
(94, 11, 73, NULL, '2014-01-15', '2014-01-15 18:23:40', 'Seed', 0, 0, 0),
(95, 11, 74, NULL, '2014-01-15', '2014-01-15 18:26:02', 'Seed', 0, 0, 0),
(96, 11, 75, NULL, '2014-01-15', '2014-01-15 18:26:52', 'Seed', 0, 0, 0),
(97, 11, 76, NULL, '2014-01-15', '2014-01-15 18:33:55', 'Seed', 0, 0, 0),
(98, 11, 77, NULL, '2014-01-15', '2014-01-15 18:43:34', 'Seed', 0, 0, 0),
(99, 11, 78, NULL, '2014-01-15', '2014-01-15 18:43:43', 'Seed', 0, 0, 0),
(100, 11, 79, NULL, '2014-01-15', '2014-01-15 18:45:03', 'Seed', 0, 0, 0),
(101, 11, 80, NULL, '2014-01-15', '2014-01-15 18:45:58', 'Seed', 0, 0, 0),
(102, 11, 81, NULL, '2014-01-15', '2014-01-15 18:46:58', 'Seed', 0, 0, 0),
(103, 11, 82, NULL, '2014-01-15', '2014-01-15 18:48:55', 'Seed', 0, 0, 0),
(104, 11, 83, NULL, '2014-01-15', '2014-01-15 18:50:07', 'Seed', 0, 0, 0),
(105, 63, 85, NULL, '2014-01-17', '2014-01-17 20:26:26', 'Seed', 0, 0, 0),
(106, 63, 86, NULL, '2014-01-17', '2014-01-17 20:26:40', 'Seed', 0, 0, 0),
(107, 63, 84, 48, '2014-01-17', '2014-01-17 20:34:11', 'Seed', 60, NULL, 0),
(108, 63, 87, 56, '2014-01-17', '2014-01-17 20:41:31', 'Seed', 2, NULL, 0),
(109, 64, 88, 46, '2014-01-30', '2014-01-30 17:37:47', 'Seed', 40, NULL, 0),
(110, 64, 89, 106, '2014-01-30', '2014-01-30 18:00:32', 'Seed', 41.76, NULL, 0),
(111, 64, 90, 127, '2014-01-30', '2014-01-30 18:28:23', 'Seed', 40, NULL, 0),
(112, 64, 90, 127, '2014-01-30', '2014-01-30 18:46:58', 'Re-Payment', 36, NULL, 0),
(113, 65, 91, 10, '2014-02-03', '2014-02-03 16:34:46', 'Seed', 8, NULL, 0),
(114, 65, 92, 128, '2014-02-03', '2014-02-03 16:41:12', 'Seed', 8, NULL, 0),
(115, 65, 92, 128, '2014-02-03', '2014-02-03 16:48:10', 'Re-Payment', 8, NULL, 0),
(116, 11, 69, 51, '2014-05-02', '2014-05-02 14:04:13', 'Seed', 2, NULL, 0),
(117, 11, 69, 51, '2014-05-02', '2014-05-02 14:04:13', 'Seed', 2, NULL, 0),
(135, 37, 97, NULL, '2014-08-21', '2014-08-21 17:03:55', 'Seed', 40, 0, 0),
(119, 11, 72, 49, '2014-05-02', '2014-05-02 14:04:19', 'Seed', 72, NULL, 0),
(120, 75, 94, 157, '2014-06-24', '2014-06-24 16:56:04', 'Seed', 116, NULL, 0),
(121, 76, 95, 157, '2014-06-24', '2014-06-24 16:56:05', 'Seed', 96, NULL, 0),
(122, 77, 96, 157, '2014-06-24', '2014-06-24 16:59:50', 'Seed', 100, NULL, 0),
(123, 75, 94, 155, '2014-06-25', '2014-06-25 16:21:06', 'Re-Payment', 21.5625, NULL, 0),
(124, 76, 95, 155, '2014-06-25', '2014-06-25 16:21:06', 'Re-Payment', 21.5625, NULL, 0),
(125, 77, 96, 155, '2014-06-25', '2014-06-25 16:21:06', 'Re-Payment', 25.875, NULL, 0),
(126, 75, 94, 153, '2014-06-25', '2014-06-25 16:21:07', 'Re-Payment', 7.36, NULL, 0),
(127, 76, 95, 153, '2014-06-25', '2014-06-25 16:21:07', 'Re-Payment', 36.8, NULL, 0),
(128, 77, 96, 153, '2014-06-25', '2014-06-25 16:21:07', 'Re-Payment', 29.44, NULL, 0),
(129, 75, 94, 156, '2014-06-25', '2014-06-25 16:21:09', 'Re-Payment', 10.75, NULL, 0),
(130, 76, 95, 156, '2014-06-25', '2014-06-25 16:21:09', 'Re-Payment', 8.95833333333, NULL, 0),
(131, 77, 96, 156, '2014-06-25', '2014-06-25 16:21:09', 'Re-Payment', 8.95833333333, NULL, 0),
(132, 75, 94, 157, '2014-06-25', '2014-06-25 16:21:12', 'Re-Payment', 61.2, NULL, 0),
(133, 76, 95, 157, '2014-06-25', '2014-06-25 16:21:12', 'Re-Payment', 9, NULL, 0),
(134, 77, 96, 157, '2014-06-25', '2014-06-25 16:21:12', 'Re-Payment', 16.2, NULL, 0),
(136, 11, 99, 146, '2014-11-15', '2014-11-16 01:42:41', 'Seed', 46, NULL, 0),
(137, 82, 98, 40, '2014-11-18', '2014-11-18 20:10:19', 'Seed', 640, NULL, 0),
(138, 56, 102, 48, '2015-04-20', '2015-04-20 16:58:09', 'Seed', 2, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `usertype` varchar(255) COLLATE utf8_bin NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_reason` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `new_password_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `new_password_requested` datetime DEFAULT NULL,
  `new_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `new_email_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `usertype`, `activated`, `banned`, `ban_reason`, `new_password_key`, `new_password_requested`, `new_email`, `new_email_key`, `last_ip`, `last_login`, `created`, `modified`) VALUES
(9, 'support', '$2a$08$SuU80qOLJSgbQouv71KpVOWTg1EKgXvgmHCK4F2mwLApiI1ZXWq7e', 'yim+ngo@chalk.org', 'Admin', 1, 0, NULL, NULL, NULL, NULL, NULL, '209.49.68.5', '2015-08-27 17:20:53', '2013-04-17 17:00:00', '2015-08-27 22:20:53'),
(11, 'txn5473', '$2a$08$Gu16qlVykpQHxExCEnls4OT783r5Ymn/h5PG6mDfma7ZVN28URlMO', 'txn5473@gmail.com', 'Admin', 1, 0, NULL, NULL, NULL, NULL, NULL, '74.74.202.136', '2015-03-24 22:02:39', '2013-04-23 20:02:28', '2015-03-25 03:02:39'),
(20, 'bobevansdct', '$2a$08$Ur5ob1..yDBioWPngSrQpONVxXwX2OfYoztG5Fh0.XWudLLYhOqUO', 'bobevansdct@hotmail.com', 'Admin', 1, 0, NULL, '6c60be5e871c4d7cd72be87a9a5f8513', '2015-05-27 10:10:17', NULL, NULL, '184.74.38.237', '2013-06-03 13:04:17', '2013-06-03 13:02:33', '2015-05-27 15:10:17'),
(35, 'bobevans', '$2a$08$mrKvlKPoTVbNiIxHX3gvgu/PZENJFdjYMfts5vKHaqZs0zMaCh4wG', 'bobevansdct@gmail.cim', 'indivisual', 1, 0, NULL, NULL, NULL, NULL, NULL, '71.161.70.229', '2013-08-28 11:43:04', '2013-08-19 15:28:44', '2013-08-28 18:43:04'),
(36, 'nanaopare1962', '$2a$08$fIuW0muCg6taNg6eXmEjZOkapUt4jOdZtijx7p4/XG0gJAqJ6bk4C', 'nanaopare1962@yahoo.co.uk', 'NGO', 1, 0, NULL, NULL, NULL, NULL, NULL, '173.87.20.158', '2014-03-12 17:08:08', '2013-08-20 08:42:36', '2014-03-12 23:08:08'),
(37, 'mland71', '$2a$08$h5VhvXLBPHuCVi2fgMJ4dO3bfEvWVBbsrtRpLdQrlGBvUM5Df2Aku', 'mland71@gmail.com', 'indivisual', 1, 0, NULL, NULL, NULL, NULL, NULL, '74.74.202.136', '2015-03-24 22:17:51', '2013-08-20 11:35:07', '2015-03-25 03:17:51'),
(38, 'Tracy', '$2a$08$s5bQV7IIdHCC2x/srz6Zxu6Nsb5.NfSCzXCYiQUxuoneSK//i.EsC', 'tracy@athletesourcecasting.com', 'indivisual', 1, 0, NULL, NULL, NULL, NULL, NULL, '74.74.202.136', '2015-03-24 22:18:32', '2013-08-20 11:36:14', '2015-03-25 03:18:32'),
(39, 'bobby', '$2a$08$TBzO8aI6.fIXGWoAePHmGeoZ1bS8CtY6.5b1A8PJJLvQ8IT41tXsy', 'bobby@mealtracker.com', 'indivisual', 1, 0, NULL, NULL, NULL, NULL, NULL, '74.74.202.136', '2015-03-24 22:12:16', '2013-08-20 11:37:13', '2015-03-25 03:12:16'),
(40, 'lollydolly848', '$2a$08$354kuVuxB8nHyFd7w.VZI.MAAiTsvheroYZqBCnFNkAaBz3fraiH6', 'lollydolly848@gmail.com', 'indivisual', 1, 0, NULL, NULL, NULL, NULL, NULL, '74.74.202.136', '2015-03-24 22:11:11', '2013-08-20 11:48:36', '2015-03-25 03:11:11'),
(41, 'pngsuzanne', '$2a$08$GFXNK.kIfUhXdSId9G5A.O506Q/D4mZbRNDjJts3Hn2QIgpE6JA3a', 'suzanne@pacificasiatourism.org', 'NGO', 1, 0, NULL, NULL, NULL, NULL, NULL, '209.105.135.247', '2013-08-20 16:23:49', '2013-08-20 16:21:58', '2013-08-20 23:23:49'),
(44, 'yim1', '$2a$08$Si5Nzobp0jiyhfmKX5wJ9uunY0ykiTpJITCMQvqmTNKQ3P39Edip.', 'yim@chalk.org', 'NGO', 1, 0, NULL, NULL, NULL, NULL, NULL, '74.46.239.75', '2013-12-09 14:40:24', '2013-09-10 00:19:23', '2013-12-09 21:40:24'),
(45, 'petertest', '$2a$08$8IDCXOIuvVqfTdkNhO/T9.2/oJwqGxuqvS2CvSEmzOSAf9GNvKIwC', 'testpeter@asda.com', 'indivisual', 1, 0, NULL, NULL, NULL, NULL, NULL, '74.34.187.246', '2013-09-12 09:41:10', '2013-09-12 09:40:57', '2013-09-12 16:41:10'),
(46, 'nana1', '$2a$08$HD7Ortd3GTRKo7xQUDsoROWdsUOdzTSS3cDgUr3E6K6Ydr..ORauW', 'nana.oparedjan@gmail.com', 'NGO', 1, 0, NULL, NULL, NULL, NULL, NULL, '41.66.207.131', '2013-09-20 06:56:39', '2013-09-20 06:20:02', '2014-02-01 01:43:22'),
(47, 'daniel', '$2a$08$xXfWtCEbOqfc.NEgXLP7tulpNIfl5y.68D/.3Cox6vaUi1DNKNIs2', 'daniel@chalk.org', 'indivisual', 1, 0, NULL, NULL, NULL, NULL, NULL, '127.0.0.1', '2013-09-25 22:12:51', '2013-09-20 06:51:12', '2013-09-26 00:12:51'),
(48, 'PhilippeSacco', '$2a$08$.gTOmLzhIHJYcaZfJ6J6vuTU1NMWmmFkcfpI.xBn4W.57ww9RQNqG', 'philippeni2030@yahoo.fr', 'NGO', 1, 0, NULL, NULL, NULL, NULL, NULL, '197.243.37.218', '2013-12-15 05:18:45', '2013-11-05 02:50:00', '2013-12-15 12:18:45'),
(49, 'RobNGO', '$2a$08$kL7CyElsWygtRjtX91nXfe0pleXxvMsFr.ykUst1XnixwnP81Pufa', 'robngo@gmail.com', 'NGO', 1, 0, NULL, NULL, NULL, NULL, NULL, '67.246.191.209', '2015-03-03 10:25:34', '2013-11-26 05:12:18', '2015-03-03 16:25:34'),
(50, 'fidelnsenga', '$2a$08$mZOMxA7DoOfgiqz1NFfNYu1I2SkFtJ0zrvqHgz9SK/EzCcUKgYel6', 'fidelnsengiyumva@yahoo.fr', 'NGO', 1, 0, NULL, NULL, NULL, NULL, NULL, '41.215.249.135', '2014-03-20 06:11:08', '2013-11-26 05:29:20', '2014-03-20 12:11:08'),
(51, 'txn1', '$2a$08$a713G5yQpjX8jPEc6CXwouedRWtUJGoD384jn9EBrU.k2K8KBXzHi', 'txn5473@hotmail.com', 'sNGO', 1, 0, NULL, NULL, NULL, NULL, NULL, '41.138.87.45', '2013-12-02 02:56:29', '2013-12-02 02:51:46', '2013-12-02 09:56:29'),
(53, 'MukizaSacco', '$2a$08$NuqnvEyJH95KpapEQaQ4RuADQYtXNKW8OzEjMY0OrTQuLvSorZge6', 'rusacco@yahoo.com', 'loan_officer', 1, 0, NULL, NULL, NULL, NULL, NULL, '41.138.85.45', '0000-00-00 00:00:00', '2013-12-02 03:42:47', '2013-12-02 10:42:47'),
(54, 'testing', '$2a$08$CPsXr8aDgFMRk9wmH6zTz.HJrr3SLDKvLmb9WtCPJrZgZCvdKunoC', 'yim+test@chalk.org', 'NGO', 1, 0, NULL, NULL, NULL, NULL, NULL, '74.46.239.75', '2013-12-03 08:59:34', '2013-12-03 08:56:03', '2013-12-03 15:59:34'),
(55, 'testing2', '$2a$08$gJCQuWNS.mGoCp7hGWDqiuV.EjP5/MqALvHlZHfT2XnJZXgTE8YFq', 'yim+test2@chalk.org', 'NGO', 1, 0, NULL, NULL, NULL, NULL, NULL, '74.46.239.75', '0000-00-00 00:00:00', '2013-12-03 09:01:14', '2013-12-03 16:02:15'),
(56, 'vietphunguyen', '$2a$08$keoajPXQ0s73sRLkH8QCuOX0yPyeVqQxN8Xgz8W3H8U9B6zKbzULG', 'designer@phuconcepts.com', 'indivisual', 1, 0, NULL, NULL, NULL, NULL, NULL, '50.48.230.26', '2015-04-20 12:16:15', '2013-12-04 06:23:04', '2015-04-20 17:16:15'),
(57, 'latrell', '$2a$08$fJ1HxEq8CyLwE/cqK2iHcOtykDTWvQyd8PR9rU/6djSegNGtbKOfy', 'management@phuconepts.com', 'NGO', 1, 0, NULL, NULL, NULL, NULL, NULL, '50.122.191.81', '0000-00-00 00:00:00', '2013-12-04 08:31:56', '2014-04-09 18:34:50'),
(58, 'DonavineSacco', '$2a$08$kpX.QO.80GpOMsnN/znw1eo4HmsxDc25ROyFOQp.zEONrqkw/5FIu', 'ashimwedodo@yahoo.fr', 'NGO', 1, 0, NULL, NULL, NULL, NULL, NULL, '104.9.123.131', '2015-09-02 02:43:52', '2013-12-09 14:32:37', '2015-09-02 07:43:52'),
(59, 'tntn', '$2a$08$rhWrMr7NWyAKKBrY896G3.BfQHhlUp99wt.Uln7kRgXqaYRcPdhXi', 'asdf', 'loan_officer', 1, 0, NULL, NULL, NULL, NULL, NULL, '74.46.239.75', '2013-12-16 11:24:05', '2013-12-16 11:23:53', '2013-12-16 18:24:05'),
(60, 'tonytestme', '$2a$08$VBgJ7W9ctTeZXDw6e/pE3OvPWJ.sZtYiUl5FIT4BY.CPc2oDAihTq', 'tonytestme@gmail.com', 'sNGO', 1, 0, NULL, NULL, NULL, NULL, NULL, '74.46.239.75', '2013-12-16 12:26:28', '2013-12-16 12:25:12', '2013-12-16 19:26:28'),
(61, 'liptrot', '$2a$08$2bhnUuDQgMePG7TZ5F07v.VhlhAWiUXMq5LwOYA5njHl2Pl98FWDe', 'management@phuconcepts.com', 'indivisual', 1, 0, NULL, NULL, NULL, NULL, NULL, '173.84.85.169', '2014-01-13 13:07:43', '2014-01-13 12:13:19', '2014-01-13 20:07:43'),
(62, 'testuser1', '$2a$08$fPNMT3FR21rmqeH8ortwC.NHS91M7scrV3wVTdEvIfCmnub6jUcCu', 'madidi.ferdinand@yahoo.fr', 'loan_officer', 1, 0, NULL, NULL, NULL, NULL, NULL, '41.138.87.43', '2014-04-17 07:59:25', '2014-01-17 11:34:29', '2014-04-17 13:59:25'),
(63, 'PHPDirector', '$2a$08$YpkIaIyXVT5lf.zx3m5lDu1Cj4PXFOv5SrJBGvXRmIbAMuRGsFPdq', 'tnguyen8485@gmail.com', 'indivisual', 1, 0, NULL, NULL, NULL, NULL, NULL, '173.84.85.169', '2014-01-17 13:41:51', '2014-01-17 13:02:36', '2014-01-17 20:41:51'),
(64, 'johnsmith', '$2a$08$aBsKmucjMAUEn9BnfGGxou7nBTmlYklT0jT6qXACGWgkoPAeNM.TS', 'johnsmith@gmail.com', 'indivisual', 1, 0, NULL, NULL, NULL, NULL, NULL, '74.69.115.235', '2014-02-03 09:31:31', '2014-01-30 10:31:47', '2014-02-03 16:31:31'),
(65, 'smithjohn', '$2a$08$sYcJnCIkjkK/OvAUeSW1g.bDvoJZ0ovf7iMN/UIbyo5e926lJmpSu', 'smithjohn@gmail.com', 'indivisual', 1, 0, NULL, NULL, NULL, NULL, NULL, '173.84.87.58', '2014-02-03 12:42:12', '2014-02-03 09:32:49', '2014-02-03 19:42:12'),
(66, 'lolamapuelos', '$2a$08$//.YtOh.Cg5LYuPLdG0OieDlfo9xif1jnS1pFqNK5.vYWJO951LkC', 'growingtogetherafrica@gmail.com', 'NGO', 1, 0, NULL, NULL, NULL, NULL, NULL, '209.150.57.196', '0000-00-00 00:00:00', '2014-03-07 11:03:25', '2014-03-11 17:13:41'),
(67, 'mltalbot', '$2a$08$/Pn63sq3WrTSfXiVWuVSx.lk7ihHFhWwU3PLhRFTjZvAUwQoc6.ee', 'mltalbot@aol.com', 'Admin', 1, 0, NULL, NULL, NULL, NULL, NULL, '67.246.191.209', '2015-07-28 13:33:43', '2014-03-13 11:16:39', '2015-07-28 18:33:43'),
(68, 'udasilas', '$2a$08$EYOOkxikbQsfqi4b4IqahuKG1Pz6Va24iqjFYpagAECujQJmVzEVW', 'udasilas@yahoo.com', 'indivisual', 1, 0, NULL, NULL, NULL, NULL, NULL, '41.138.87.44', '2014-04-18 04:15:11', '2014-04-18 04:14:48', '2014-04-18 10:15:11'),
(69, 'amasukari', '$2a$08$dXyK5VX14TcFWAmn0Te/KObDlPXKNAOCLYJmocdvPhbMFyGcdhZ.m', 'amasukari@yahoo.com', 'NGO', 1, 0, NULL, NULL, NULL, NULL, NULL, '41.190.90.75', '2014-04-29 07:37:08', '2014-04-29 07:36:13', '2014-04-29 13:38:47'),
(70, 'wilderlaryea09', '$2a$08$BUOG8ZGeE.vMrqUhLdUWBuSkd.NGyFqtKqm7JTqIeFP0FlmZN/pIG', 'wilderlaryea09@gmail.com', 'loan_officer', 1, 0, NULL, NULL, NULL, NULL, NULL, '41.190.89.128', '2015-02-09 11:23:25', '2014-04-29 07:45:39', '2015-02-09 17:23:25'),
(71, 'samson', '$2a$08$2GxyyKwTZ8k0pto3CnNl/eg0L1OJ7dRsY0fZAe1R74Q.q/LlYpaYu', 'samsonb82@yahoo.fr', 'indivisual', 1, 0, NULL, NULL, NULL, NULL, NULL, '41.138.85.44', '2014-05-02 01:31:39', '2014-05-02 01:31:30', '2014-05-02 07:31:39'),
(72, 'tsowah', '$2a$08$e398znK9zd4sFVVGHZHGcuxMl5BDIzAxh7UxyEdZg3eu5kaup8aAK', 'tsowah@sinapiaba.com', 'NGO', 1, 0, NULL, NULL, NULL, NULL, NULL, '197.220.163.74', '2014-05-02 08:30:47', '2014-05-02 08:30:11', '2014-05-02 14:30:47'),
(73, 'hmkathleen', '$2a$08$vhxdGY1q26xqyttj4kM2b.GPfKmEnYIM91XH16ND6aI2zOaVuNNO2', 'hmkathleen@joy2theworld.org', 'sNGO', 1, 0, NULL, NULL, NULL, NULL, NULL, '41.189.162.0', '2014-06-19 21:54:42', '2014-05-20 08:48:33', '2014-06-20 03:54:42'),
(74, 'makeithappen', '$2a$08$8C6vytNoGXsucbohwcdsbOjQq08egfEbyu6iUb87eMji1cXXAgvFe', 'makeithappen@gmail.com', 'NGO', 1, 0, NULL, NULL, NULL, NULL, NULL, '173.87.22.254', '2014-06-25 14:33:05', '2014-06-24 09:10:18', '2014-06-25 20:33:05'),
(75, 'phantom1', '$2a$08$HDdtOGg/v7wjU6xhE/mAxeWjZn1ACWVRAyY6TgiR4IWst5p29ksqW', 'phantom1@gmail.com', 'indivisual', 1, 0, NULL, NULL, NULL, NULL, NULL, '173.87.22.254', '2014-06-24 10:47:31', '2014-06-24 10:16:34', '2014-06-24 16:47:31'),
(76, 'phantom2', '$2a$08$DyizGuR2AX5VAgDXULAfwuqX8ac92cZsXnsWchhPH1XxcwY7XWDVm', 'phantom2@gmail.com', 'indivisual', 1, 0, NULL, NULL, NULL, NULL, NULL, '173.87.22.254', '2014-06-24 10:52:03', '2014-06-24 10:17:49', '2014-06-24 16:52:03'),
(77, 'phantom3', '$2a$08$xn0aG57.x.yGpwgurwpmfetMaUOKJlVcYNGKllLA8M0PpfqpfQMZ6', 'phantom3@gmail.com', 'indivisual', 1, 0, NULL, NULL, NULL, NULL, NULL, '173.87.22.254', '2014-06-24 10:56:24', '2014-06-24 10:19:20', '2014-06-24 16:56:24'),
(78, 'makeithappen2', '$2a$08$jDJiufB45gX7Ezn9nn/wXu1jp1ApL0TvzTHagVayq9GfftKPnTZ3S', 'makeithappen2@gmail.com', 'NGO', 1, 0, NULL, NULL, NULL, NULL, NULL, '173.87.22.254', '2014-06-25 13:44:31', '2014-06-25 13:42:20', '2014-06-25 19:44:31'),
(79, 'johnndoolittle', '$2a$08$5p1s4.LcqCGHlCaCq2XJT.YCsex7W9p3RgO7TUDzbydVLnD2uG.TK', 'John.n.doolittle@gmail.com', 'Admin', 1, 0, NULL, NULL, NULL, NULL, NULL, '197.157.131.18', '2014-10-15 10:18:43', '2014-08-08 05:56:36', '2014-10-15 15:18:43'),
(80, 'snevans222', '$2a$08$/zjQrBZdhszhPxUsBaXiFOXDc4SqHuFIt6KLzjmc5XHsLD937OJAa', 'snevans222@gmail.com', 'indivisual', 1, 0, NULL, NULL, NULL, NULL, NULL, '74.74.202.136', '2015-03-24 22:11:40', '2014-08-20 11:52:34', '2015-03-25 03:11:40'),
(81, 'PeopleHelpingPeople', '$2a$08$ogpdeh1hCSvkkj0bJk08LOJ//EYKHKLEfeZjLnfuy/35gHIVzvmhe', 'bobevansphp@gmail.com', 'indivisual', 1, 0, NULL, NULL, NULL, NULL, NULL, '67.246.191.209', '2014-11-25 11:31:04', '2014-10-23 09:47:29', '2014-11-25 17:31:04'),
(82, 'hudson585', '$2a$08$gOZe.QoXXp17m4gieWZKPeJJzULMGaQpohj8JKnH8WncWMKHqmpKG', 'mark@phuconcepts.com', 'indivisual', 1, 0, NULL, NULL, NULL, NULL, NULL, '50.48.202.220', '2014-10-23 17:11:26', '2014-10-23 16:47:44', '2014-10-23 22:11:26'),
(83, 'CashDollar', '$2a$08$lluS7s0oa5wxodGH9o5/G.NVl5MN/thwIKuNVoT/nDoaGZ8M7Z/Uy', 'mltalbot33@gmail.com', 'sNGO', 1, 0, NULL, NULL, NULL, NULL, NULL, '67.246.191.209', '0000-00-00 00:00:00', '2014-11-25 11:45:09', '2015-08-11 18:18:12'),
(84, 'johndevito', '$2a$08$zJr.ggc/1OGWVLqOF4vGF.36.zA.57.NP9.OVPuwx7hdysoz8rHGK', 'guyinthebluehouse@gmail.com', 'sNGO', 1, 0, NULL, NULL, NULL, NULL, NULL, '50.48.230.26', '0000-00-00 00:00:00', '2015-04-20 11:38:06', '2015-04-20 16:38:06'),
(85, 'johndevito309', '$2a$08$kJCQTAx0CuUx9rxQxPjLUeXA3zHgBHaMqcYbvtmjOWwnKQsiNKtby', 'johndevito309@gmail.com', 'sNGO', 1, 0, NULL, NULL, NULL, NULL, NULL, '73.170.114.193', '2015-04-20 11:47:03', '2015-04-20 11:43:03', '2015-04-20 16:47:03'),
(86, 'vietbingo', '$2a$08$L9phmqqlXs.W6szbb4AFH.q.z64iP3K6P2KRWjl6mCToeuh8rto8S', 'designer+ngo@phuconcepts.com', 'NGO', 0, 0, NULL, NULL, NULL, NULL, NULL, '50.48.230.26', '0000-00-00 00:00:00', '2015-04-23 12:04:13', '2015-04-23 17:04:13'),
(87, 'JohnnyOptimo', '$2a$08$9L8ZK7fxzmEAHyWKe07TlOTJfnz1cq2gV3eK/o4YN.VEGR3gBcBN.', 'john@phuconcepts.com', 'indivisual', 1, 0, NULL, NULL, NULL, NULL, NULL, '50.49.151.238', '2015-05-21 11:49:18', '2015-05-19 15:23:31', '2015-05-21 16:49:18'),
(88, 'PMFL2015', '$2a$08$CF3vZVYfdPvd1za31t1MD.VUEbjlEAHgcXkK1JdGM3IqZaeCGMebK', 'bobevansdct@stny.rr.com', 'NGO', 1, 0, NULL, 'e8ca4de7a865dd98910516d15f91ba49', '2015-08-11 13:30:25', NULL, NULL, '67.246.191.209', '0000-00-00 00:00:00', '2015-08-11 13:21:01', '2015-08-27 22:21:20'),
(89, 'johntaves', '$2a$08$Jgf7ez79YKwujC9nQC86v.UEqyLxurLdDcDkrymXBv9qtd1.Mj46W', 'john.taves@pnwsoft.com', 'indivisual', 1, 0, NULL, NULL, NULL, NULL, NULL, '107.77.70.21', '2015-08-13 13:02:07', '2015-08-13 13:01:36', '2015-08-13 18:02:07'),
(90, 'alexgerberukrnet', '$2a$08$vqrsy8oQlyhKh8RI3.FBges4WL33ehJZAaISu3C.wurHVSFkcZ0cW', 'alexgerber@ukr.net', 'indivisual', 1, 0, NULL, NULL, NULL, NULL, NULL, '93.171.172.30', '2015-08-27 11:17:25', '2015-08-27 11:17:02', '2015-08-27 16:17:25'),
(91, 'nb3lmont', '$2a$08$8lnR58UaY.e9L6abFLeqfemx15UWmWF1he6NkC5c7kwguJrITwSrG', 'billy@phuconcepts.com', 'Admin', 1, 0, NULL, NULL, NULL, NULL, NULL, '50.48.231.67', '2015-09-08 11:18:13', '2015-08-27 15:39:07', '2015-09-08 16:18:13'),
(92, 'Nito', '$2a$08$TR6LfhABDCe8yBjChkHISe1bDVHGspWVPIIX4iHu4G.47/pz.bwQW', 'info@fragrepairs.com', 'indivisual', 1, 0, NULL, NULL, NULL, NULL, NULL, '50.48.231.67', '2015-08-27 16:36:42', '2015-08-27 16:36:31', '2015-08-27 21:36:42');

-- --------------------------------------------------------

--
-- Table structure for table `user_autologin`
--

CREATE TABLE IF NOT EXISTS `user_autologin` (
  `key_id` char(32) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `user_autologin`
--

INSERT INTO `user_autologin` (`key_id`, `user_id`, `user_agent`, `last_ip`, `last_login`) VALUES
('1fa220dac4c33e2bd01052d366bf4ee5', 11, 'Mozilla/5.0 (Windows NT 5.1; rv:21.0) Gecko/20100101 Firefox/21.0', '184.74.38.237', '2013-07-01 20:03:12'),
('39688853feff4d10be60d5c861a34bb5', 35, 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)', '71.161.70.229', '2013-08-28 16:59:15');

-- --------------------------------------------------------

--
-- Table structure for table `user_organization`
--

CREATE TABLE IF NOT EXISTS `user_organization` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `organization_id` int(11) NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_organization`
--

INSERT INTO `user_organization` (`id`, `user_id`, `organization_id`, `url`, `address`, `city`, `state`, `zip`, `country`) VALUES
(2, 6, 0, '', 'sdfghj', 'qfghj', 'wsdfghj', 'fdgh', 'ftrghjkghfdsrg'),
(3, 7, 0, 'b', 'tess', 'tess', 'tess', 'tess', 'tess'),
(4, 9, 0, 'c', '123 test', 'Rochester', 'NY', '14623', 'United States'),
(5, 10, 2, 'd', '263 Central Ave', 'Rochester', 'New York', '14605', 'United States'),
(6, 12, 0, 'e', '263 Central Ave', 'Rochester', 'New York', '14605', 'United States'),
(7, 13, 0, 'f', '263 Central Ave', 'Rochester', 'New York', '14605', 'United States'),
(8, 11, 1, 'g', '', '', '', '', ''),
(9, 14, 2, 'peoplehp', '263 Central Ave', 'Rochester', 'NY', '14623', 'USA'),
(10, 17, 3, NULL, '', '', '', '', ''),
(11, 18, 4, 'phpspecial', '', '', '', '', ''),
(12, 19, 4, NULL, '', '', '', '', ''),
(13, 21, 5, NULL, '', '', '', '', ''),
(14, 22, 6, NULL, '', '', '', '', ''),
(15, 29, 4, NULL, '', '', '', '', ''),
(16, 30, 7, NULL, '', '', '', '', ''),
(17, 31, 2, NULL, '', '', '', '', ''),
(18, 32, 2, NULL, '', '', '', '', ''),
(19, 33, 2, NULL, '', '', '', '', ''),
(20, 34, 2, NULL, '', '', '', '', ''),
(21, 36, 8, NULL, '', '', '', '', ''),
(22, 41, 9, NULL, '', '', '', '', ''),
(23, 43, 10, NULL, '', '', '', '', ''),
(24, 44, 11, NULL, '', '', '', '', ''),
(25, 46, 12, NULL, '', '', '', '', ''),
(26, 48, 13, NULL, '', '', '', '', ''),
(27, 49, 14, NULL, '', '', '', '', ''),
(28, 50, 15, NULL, '', '', '', '', ''),
(29, 51, 16, NULL, '', '', '', '', ''),
(30, 54, 17, NULL, '', '', '', '', ''),
(31, 55, 18, NULL, '', '', '', '', ''),
(32, 56, 19, NULL, '', '', '', '', ''),
(33, 57, 20, NULL, '', '', '', '', ''),
(34, 58, 21, NULL, '', '', '', '', ''),
(35, 59, 14, NULL, '', '', '', '', ''),
(36, 60, 22, NULL, '', '', '', '', ''),
(37, 62, 21, NULL, '', '', '', '', ''),
(38, 66, 23, NULL, '', '', '', '', ''),
(39, 69, 24, NULL, '', '', '', '', ''),
(40, 70, 24, NULL, '', '', '', '', ''),
(41, 72, 25, NULL, '', '', '', '', ''),
(42, 73, 26, NULL, '', '', '', '', ''),
(43, 74, 27, NULL, '', '', '', '', ''),
(44, 78, 28, NULL, '', '', '', '', ''),
(45, 79, 0, NULL, '', '', '', '', ''),
(46, 83, 29, NULL, '', '', '', '', ''),
(47, 86, 30, NULL, '', '', '', '', ''),
(48, 88, 31, NULL, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_profiles`
--

CREATE TABLE IF NOT EXISTS `user_profiles` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `firstname` varchar(255) COLLATE utf8_bin NOT NULL,
  `lastname` varchar(255) COLLATE utf8_bin NOT NULL,
  `pic` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_bin NOT NULL,
  `country` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `user_profiles`
--

INSERT INTO `user_profiles` (`id`, `user_id`, `firstname`, `lastname`, `pic`, `city`, `country`, `website`, `phone`) VALUES
(4, 7, 'tess', 'tess', NULL, 'tess', NULL, NULL, NULL),
(5, 8, 'Peter', 'Yim', NULL, 'San Francisco', NULL, NULL, NULL),
(7, 9, 'Peter', 'Yim', NULL, 'Rochester', NULL, NULL, ''),
(9, 10, 'Peter K', 'Yim', NULL, 'Rochester', NULL, NULL, NULL),
(11, 11, 'Tony', 'Nguyen', NULL, 'rochester', NULL, NULL, ''),
(13, 12, 'Peter', 'Kin', NULL, 'Rochester', NULL, NULL, NULL),
(14, 13, 'Peter', 'Kin', NULL, 'Rochester', NULL, NULL, NULL),
(16, 14, 'peter', 'yim', NULL, 'Rochester', NULL, NULL, NULL),
(19, 17, 'peter', 'yim', NULL, 'q', 'q', NULL, NULL),
(20, 18, 'peter', 'yim', NULL, 'Rochester', 'USA', NULL, NULL),
(21, 19, 'bob', 'e', NULL, 'rochester', NULL, NULL, NULL),
(22, 20, 'Bob', 'Evans', NULL, 'Chautawaga', 'United States', NULL, ''),
(23, 21, 'Tony ', 'Ng', NULL, 'tony', 'tony', NULL, NULL),
(24, 22, 'tony', 'Ngo', NULL, '', '', NULL, NULL),
(25, 23, 'php', 'donor1', NULL, 'rochester', 'usa', NULL, NULL),
(26, 24, 'php', 'donor2', NULL, 'rochester', 'usa', NULL, NULL),
(27, 25, 'php', 'doner3', NULL, 'rochester', 'usa', NULL, NULL),
(28, 26, 'php', 'doner4', NULL, 'rochester', 'usa', NULL, NULL),
(29, 27, 'php', 'donor5', NULL, 'rochester', 'usa', NULL, NULL),
(30, 28, 'php', 'donor6', NULL, 'rochester', 'usa', NULL, NULL),
(31, 29, 'special peter', 'yim', NULL, 'buf', 'usa', NULL, NULL),
(32, 30, 'Bob', 'Evans', NULL, 'hornell', 'US', NULL, NULL),
(33, 31, 'Peter', 'Yim', NULL, '', NULL, NULL, NULL),
(34, 32, 'tes', 'test', NULL, '', NULL, NULL, NULL),
(35, 33, 'one', 'moretest', NULL, '', NULL, NULL, NULL),
(36, 34, 'bob', 'testing', NULL, '', NULL, NULL, NULL),
(37, 35, 'Bob', 'Evans', NULL, 'Chautauqua', 'U.S.A.', NULL, NULL),
(38, 36, 'Nana', 'Opare Djan', NULL, 'accra', 'africa', NULL, ''),
(39, 37, 'mike', 'land', NULL, 'park city', 'usa', NULL, NULL),
(40, 38, 'Tracy ', 'Evans', NULL, 'Park city', 'USA', NULL, NULL),
(41, 39, 'Bob', 'Evans JR', NULL, 'Hornell', 'USA', NULL, NULL),
(42, 40, 'Brittney', 'Behling', NULL, 'Hornell', 'USA', NULL, NULL),
(43, 41, 'Suzanne', '', NULL, '', '', NULL, NULL),
(44, 42, 'tony', 'nguyen', NULL, 'rochester', 'usa', NULL, NULL),
(45, 43, 'tony', 'ngo', NULL, 'rochester', '', NULL, NULL),
(46, 44, '', '', NULL, '', '', NULL, NULL),
(47, 45, 'petertest', 'test', NULL, 'San Francisco', 'USA', NULL, NULL),
(48, 46, 'Nana', 'Opare-Djan', NULL, 'Taifa', 'Ghana', NULL, NULL),
(49, 47, 'Daniel', 'Yim', NULL, 'San Francisco', 'USA', NULL, NULL),
(50, 48, 'Philippe', 'Niyonsenga', NULL, 'Kinunu', 'Rwanda', NULL, NULL),
(51, 49, 'Robert', 'NGO', NULL, 'boneza', 'rwanda', NULL, NULL),
(52, 50, 'nsengiyumva', 'Fidele', NULL, 'Kigali', 'Rwanda', NULL, NULL),
(53, 51, '', '', NULL, '', '', NULL, NULL),
(54, 52, 'Donavine', 'UMURERWA', NULL, 'Kigali', 'Africa', NULL, NULL),
(55, 53, 'Mukiza', 'Yvonne', NULL, '', NULL, NULL, NULL),
(56, 54, 'Peter ', 'Yim', NULL, '1', '1', NULL, NULL),
(57, 55, 'peter2', 'yim', NULL, 'sdf', 'usa', NULL, NULL),
(58, 56, 'Viet', 'Nguyen', NULL, 'Rochester', 'United States', NULL, ''),
(59, 57, 'Latrell', 'Liptrot', NULL, '', '', NULL, NULL),
(60, 58, 'Donavine', 'Umurerwa', NULL, 'Kigali', 'Rwanda', NULL, 'donavinesacco'),
(61, 59, 'tony', 'nguyen', NULL, '', NULL, NULL, NULL),
(62, 60, 'tony', 'testme', NULL, 'rochester', '', NULL, NULL),
(63, 61, 'Latrell', 'Liptrot', NULL, 'Rochester', 'US', NULL, NULL),
(64, 62, 'Ferdinand', 'RYUMUGABE', NULL, '', NULL, NULL, '0728804926'),
(65, 63, 'Tony', 'Nguyen', NULL, 'Rochester', 'US', NULL, NULL),
(66, 64, 'john', 'smith', NULL, 'rochester', 'usa', NULL, NULL),
(67, 65, 'John', 'Smith', NULL, 'Kigali', 'Rwanda', NULL, NULL),
(68, 66, 'dolores', 'martin', NULL, 'new york', 'United States', NULL, NULL),
(69, 67, 'Mary Lee', 'Talbot', NULL, 'NY', 'USA', NULL, ''),
(70, 68, 'Silas', 'Jean Munana', NULL, 'Kigali', 'Rwanda', NULL, NULL),
(71, 69, 'Amos', 'Asuma-Karikari', NULL, 'Accra', 'Ghana', NULL, '0208192645'),
(72, 70, 'David', 'Wilder-Laryea', 'http://www.phpintl.org/imageupload/server/php/files/Snapshot_20140429_1.JPG', '', NULL, NULL, '0267866388'),
(73, 71, 'Samson', 'Nsengiyumva', NULL, 'Kigali', 'Rwanda', NULL, NULL),
(74, 72, 'Thomas', 'Sowah', NULL, 'Kumasi', 'Ghana', NULL, NULL),
(75, 73, 'Kathleen', 'Gibbs', NULL, '', '', NULL, NULL),
(76, 74, 'Bob', 'Evans', NULL, '', '', NULL, NULL),
(77, 75, 'Phantom', 'One', NULL, 'Rochester', 'NY', NULL, NULL),
(78, 76, 'Phantom', 'Two', NULL, '', '', NULL, ''),
(79, 77, 'Phantom', 'Three', NULL, 'Rochester', 'NY', NULL, NULL),
(80, 78, 'Bob', 'Evans', NULL, '', '', NULL, NULL),
(81, 79, 'John', 'Doolittle', NULL, '', 'USA', NULL, NULL),
(82, 80, 'Stacy ', 'Evans', NULL, 'Hornell', 'USA', NULL, NULL),
(83, 81, 'Robert', 'Evans Sr.', NULL, 'Chautauqua', 'United States', NULL, NULL),
(84, 82, 'mark', 'werner', NULL, 'rochester', 'USA', NULL, NULL),
(85, 83, '', '', NULL, '', '', NULL, NULL),
(86, 84, 'John', 'Devito', NULL, '', '', NULL, NULL),
(87, 85, 'John', 'Devito', NULL, 'Rochester', 'United States', NULL, ''),
(88, 86, 'Viet', 'Nguyen', NULL, 'Roic', '', NULL, NULL),
(89, 87, 'John', 'Kalinowski', NULL, 'Rochester', 'United States', NULL, NULL),
(90, 88, 'Robert ', 'Evans', NULL, 'Chautauqua', 'USA', NULL, NULL),
(91, 89, 'John', 'Taves', NULL, 'Bellevue', 'United States', NULL, NULL),
(92, 90, '', '', NULL, '', '', NULL, NULL),
(93, 91, 'William', 'Essig', NULL, 'Rochester', 'US', NULL, NULL),
(94, 92, 'William', 'Essig', NULL, 'Rochester', 'US', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `collateral`
--
ALTER TABLE `collateral`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `funding`
--
ALTER TABLE `funding`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_members`
--
ALTER TABLE `group_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipn_log`
--
ALTER TABLE `ipn_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipn_orders`
--
ALTER TABLE `ipn_orders`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UniqueTransactionID` (`txn_id`);

--
-- Indexes for table `ipn_order_items`
--
ALTER TABLE `ipn_order_items`
  ADD PRIMARY KEY (`id`), ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ngo_reserve`
--
ALTER TABLE `ngo_reserve`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organization`
--
ALTER TABLE `organization`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `url` (`url`);

--
-- Indexes for table `organization_updates`
--
ALTER TABLE `organization_updates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects_closing`
--
ALTER TABLE `projects_closing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rates`
--
ALTER TABLE `rates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `repayment`
--
ALTER TABLE `repayment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `savings`
--
ALTER TABLE `savings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scheduled_payment`
--
ALTER TABLE `scheduled_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seed_account`
--
ALTER TABLE `seed_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_cms`
--
ALTER TABLE `site_cms`
  ADD PRIMARY KEY (`cms_id`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_autologin`
--
ALTER TABLE `user_autologin`
  ADD PRIMARY KEY (`key_id`,`user_id`);

--
-- Indexes for table `user_organization`
--
ALTER TABLE `user_organization`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `url` (`url`);

--
-- Indexes for table `user_profiles`
--
ALTER TABLE `user_profiles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=316;
--
-- AUTO_INCREMENT for table `collateral`
--
ALTER TABLE `collateral`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `funding`
--
ALTER TABLE `funding`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=120;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `group_members`
--
ALTER TABLE `group_members`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipn_log`
--
ALTER TABLE `ipn_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `ipn_orders`
--
ALTER TABLE `ipn_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ipn_order_items`
--
ALTER TABLE `ipn_order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ngo_reserve`
--
ALTER TABLE `ngo_reserve`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=104;
--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=124;
--
-- AUTO_INCREMENT for table `organization`
--
ALTER TABLE `organization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `organization_updates`
--
ALTER TABLE `organization_updates`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=178;
--
-- AUTO_INCREMENT for table `projects_closing`
--
ALTER TABLE `projects_closing`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `rates`
--
ALTER TABLE `rates`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `repayment`
--
ALTER TABLE `repayment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=145;
--
-- AUTO_INCREMENT for table `savings`
--
ALTER TABLE `savings`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=265;
--
-- AUTO_INCREMENT for table `scheduled_payment`
--
ALTER TABLE `scheduled_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `seed_account`
--
ALTER TABLE `seed_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT for table `site_cms`
--
ALTER TABLE `site_cms`
  MODIFY `cms_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=139;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=93;
--
-- AUTO_INCREMENT for table `user_organization`
--
ALTER TABLE `user_organization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `user_profiles`
--
ALTER TABLE `user_profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=95;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
